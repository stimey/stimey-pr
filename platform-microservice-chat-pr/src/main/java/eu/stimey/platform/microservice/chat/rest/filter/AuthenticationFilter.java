package eu.stimey.platform.microservice.chat.rest.filter;

import javax.ws.rs.ext.Provider;

import eu.stimey.platform.library.rest.filter.DefaultAuthenticationFilter;

@Provider
public class AuthenticationFilter extends DefaultAuthenticationFilter {

}
