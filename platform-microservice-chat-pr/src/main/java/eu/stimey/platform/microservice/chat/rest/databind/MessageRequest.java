package eu.stimey.platform.microservice.chat.rest.databind;

public class MessageRequest {
	public String from;
	public String targetId;
	public String targetName;
	public String message;

	public MessageRequest() {
	}
}