package eu.stimey.platform.microservice.chat.databind.filter;

public enum ChatListFilter {
	SHOW_ALL, ONLY_GROUP, UNREAD_MESSAGES, MESSAGE_REQUEST, CONTACT_REQUEST;

	public static ChatListFilter fromString(String value) {
		return (value.equals("1") ? SHOW_ALL
				: (value.equals("2") ? ONLY_GROUP
						: (value.equals("3") ? UNREAD_MESSAGES
								: (value.equals("4") ? MESSAGE_REQUEST
										: (value.equals("5")) ? CONTACT_REQUEST : SHOW_ALL))));
	}
}
