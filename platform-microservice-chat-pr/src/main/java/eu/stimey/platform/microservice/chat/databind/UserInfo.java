package eu.stimey.platform.microservice.chat.databind;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import eu.stimey.platform.library.utils.databind.DateDeserializer;
import eu.stimey.platform.library.utils.databind.DateSerializer;

public class UserInfo {

	protected String userId;
	protected boolean showConversationValue;
	protected String membersName;
	protected String avatarImageId;

	@JsonSerialize(using = DateSerializer.class)
	@JsonDeserialize(using = DateDeserializer.class)
	protected Date startConversationTime;

	public UserInfo(String userId, boolean showConversationValue, String membersName, String avatarImageId) {
		this.userId = userId;
		this.showConversationValue = showConversationValue;
		this.membersName = membersName;
		this.avatarImageId = avatarImageId;
	}

	public String getMembersName() {
		return membersName;
	}

	public void setMembersName(String membersName) {
		this.membersName = membersName;
	}

	public String getAvatarImageId() {
		return avatarImageId;
	}

	public void setAvatarImageId(String avatarImageId) {
		this.avatarImageId = avatarImageId;
	}

	public UserInfo() {
		this(null, false);
	}

	public UserInfo(String userId, boolean showConversationValue) {
		this.userId = userId;
		this.showConversationValue = showConversationValue;
		startConversationTime = null;
	}

	public UserInfo(String userId, boolean showConversationValue, Date startConversationTime) {
		this.userId = userId;
		this.showConversationValue = showConversationValue;
		this.startConversationTime = startConversationTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public boolean isShowConversationValue() {
		return showConversationValue;
	}

	public void setShowConversationValue(boolean showConversationValue) {
		this.showConversationValue = showConversationValue;
	}

	public Date getStartConversationTime() {
		return startConversationTime;
	}

	public void setStartConversationTime(Date startConversationTime) {
		this.startConversationTime = startConversationTime;
	}

	@Override
	public String toString() {
		return "UserInfo [userId=" + userId + ", showConversationValue=" + showConversationValue
				+ ", startConversationTime=" + startConversationTime + ", username=" + getMembersName() + "]";
	}
}