package eu.stimey.platform.microservice.chat.rest.databind;

public class MessageRequestResponse {
	public String targetId;
	public boolean accepted;
	public boolean delete;

	public MessageRequestResponse(String targetId, boolean accepted) {
		this.targetId = targetId;
		this.accepted = accepted;

		if (accepted)
			this.delete = false;
		else
			this.delete = true;
	}

	public MessageRequestResponse() {
	}

	@Override
	public String toString() {
		return "targetId: " + targetId + ", accepted: " + accepted;
	}
}
