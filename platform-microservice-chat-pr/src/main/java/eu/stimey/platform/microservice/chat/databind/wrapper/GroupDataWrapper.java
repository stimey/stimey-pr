package eu.stimey.platform.microservice.chat.databind.wrapper;

import java.util.ArrayList;

public class GroupDataWrapper {
	public String topic;
	public String groupName;
	public ArrayList<String> memberList;
	public String groupImageId;

	GroupDataWrapper() {
	}
}
