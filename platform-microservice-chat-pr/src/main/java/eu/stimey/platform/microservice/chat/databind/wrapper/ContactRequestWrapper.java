package eu.stimey.platform.microservice.chat.databind.wrapper;

public class ContactRequestWrapper {
	public String targetId;
	public String targetName;
	public String title;
	public String message;

	public ContactRequestWrapper() {
	}
}
