package eu.stimey.platform.microservice.chat.rest;

import static eu.stimey.platform.library.utils.StimeyLogger.*;

import javax.annotation.PreDestroy;
import javax.ws.rs.ApplicationPath;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import eu.stimey.platform.microservice.chat.beans.ChatService;

/**
 * 
 * REST configuration
 *
 */
@ApplicationPath("api")
public class RESTConfig extends ResourceConfig {
	protected final MongoDBService mongoDBService;
	protected final RedisAMQPService redisAMQPService;

	public RESTConfig() {
		super();

		mongoDBService = MongoDBService.getService();
		redisAMQPService = RedisAMQPService.getService();

		logger().info("REST-Service started...");

		packages("eu.stimey.platform.microservice.chat.rest");

		register(new AbstractBinder() {
			protected void configure() {
				bind(mongoDBService).to(MongoDBService.class);
				bind(redisAMQPService).to(RedisAMQPService.class);
				UserSharedService userSharedService = new UserSharedService();
				ChatService chatService = new ChatService();
				chatService.setUserSharedService(userSharedService);

				bind(userSharedService).to(UserSharedService.class);
				bind(chatService).to(ChatService.class);
			}
		});

		register(new JacksonJsonProvider().configure(SerializationFeature.INDENT_OUTPUT, true));
	}

	@PreDestroy
	public void shutdown() {
		logger().info("REST-Service stopped...");
	}
}
