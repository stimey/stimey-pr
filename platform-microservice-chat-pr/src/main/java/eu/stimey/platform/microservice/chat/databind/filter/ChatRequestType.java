package eu.stimey.platform.microservice.chat.databind.filter;

public enum ChatRequestType {
	MESSAGE_REQUEST, CONTACT_REQUEST;

	// default ist message request
	public static ChatRequestType fromString(String type) {
		return (type.toLowerCase().equals("message") ? MESSAGE_REQUEST
				: (type.toLowerCase().equals("contact")) ? CONTACT_REQUEST : MESSAGE_REQUEST);
	}
}
