package eu.stimey.platform.microservice.chat.rest.databind;

import java.util.LinkedList;
import java.util.List;

public class Friend {

	protected String userid;
	protected String username;
	protected String usertype;
	protected List<String> friendRequests;
	protected String avatarImageId;
	protected String avatarImage;
	protected String rank;

	public Friend() {
	}

	public Friend(String userid, String username, String usertype, String avatarImageId) {
		this.userid = userid;
		this.username = username;
		this.usertype = usertype;
		this.avatarImageId = avatarImageId;
		this.rank = "";
		this.avatarImage = null;
		this.friendRequests = new LinkedList<>();
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public String getAvatarImageId() {
		return avatarImageId;
	}

	public void setAvatarImageId(String avatarImageId) {
		this.avatarImageId = avatarImageId;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getAvatarImage() {
		return avatarImage;
	}

	public void setAvatarImage(String avatarImage) {
		this.avatarImage = avatarImage;
	}

	public List<String> getFriendRequests() {
		return friendRequests;
	}

	public void setFriendRequests(List<String> friendRequests) {
		this.friendRequests = friendRequests;
	}

	public Friend(String userid, String username, String usertype, String avatarImageId, String avatarImage,
			String rank) {
		this.userid = userid;
		this.username = username;
		this.usertype = usertype;
		this.avatarImageId = avatarImageId;
		this.avatarImage = avatarImage;
		this.rank = rank;
	}

	@Override
	public String toString() {
		return "Friend{" + "userid='" + userid + '\'' + ", username='" + username + '\'' + ", usertype='" + usertype
				+ '\'' + ", friendRequests=" + friendRequests + ", avatarImageId='" + avatarImageId + '\''
				+ ", avatarImage='" + avatarImage + '\'' + ", rank='" + rank + '\'' + '}';
	}
}
