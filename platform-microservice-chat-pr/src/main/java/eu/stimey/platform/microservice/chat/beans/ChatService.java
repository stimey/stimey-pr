package eu.stimey.platform.microservice.chat.beans;

import java.io.IOException;
import java.util.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.data.access.databind.Message;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.microservice.chat.databind.ChatObject;
import eu.stimey.platform.microservice.chat.databind.filter.ChatListFilter;
import eu.stimey.platform.microservice.chat.databind.filter.ChatRequestType;
import eu.stimey.platform.microservice.chat.databind.wrapper.UnreadMessageWrapper;
import eu.stimey.platform.microservice.chat.rest.databind.Friend;
import eu.stimey.platform.microservice.chat.rest.databind.MessageRequestResponse;
import eu.stimey.platform.microservice.chat.startup.Global;
import org.bson.Document;

import eu.stimey.platform.library.data.access.CachingConfiguration;
import eu.stimey.platform.library.data.access.Messaging;
import eu.stimey.platform.microservice.chat.databind.ChatMessage;
import eu.stimey.platform.microservice.chat.databind.UserInfo;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;

public class ChatService {
	UserSharedService userSharedService;

	public UserSharedService getUserSharedService() {
		return userSharedService;
	}

	public void setUserSharedService(UserSharedService userSharedService) {
		this.userSharedService = userSharedService;
	}

	protected Messaging<ChatObject> chats;

	public ChatService() {
		CachingConfiguration configuration = new CachingConfiguration(RedisAMQPService.getService().getJedisPool(),
				RedisAMQPService.getService().getRabbitmqConnection(), MongoDBService.getService().getClient(),
				MongoDBService.getService().getDatabaseName());

		chats = new Messaging<>("ms_chat", ChatObject.class, "chat", configuration);
		chats.clear(); // clear cache
	}

	public Messaging<ChatObject> getChats() {
		return chats;
	}

	public ChatObject getChatObject(String topic) {
		return chats.get(topic);
	}

	public ChatMessage getChatMessage(String topic, String messageId) {
		return (ChatMessage) chats.getMessage(topic, messageId);
	}

	public boolean setMessageReadStatus(String userId, String topic, String messageId, boolean status) {
		ChatMessage msg = getChatMessage(topic, messageId);
		msg.setMessageUnread(status);

		if (msg.getUnreadGroupMessages() != null)
			msg.getUnreadGroupMessages().replace(userId, status);

		// System.out.println(userId + " has update the message");

		chats.setMessage(topic, messageId, msg);
		return true;
	}

	/*
	 * Creates a new message or contact request
	 */
	public boolean createRequestType(ChatRequestType type, String userId, String targetUserId, String username,
			String targetName, String requestMessage, String title) {
		Set<String> members = new HashSet<>();

		for (ChatObject messages : getFriendsList(sortWithAlphabeticalOrder(userId), userId)) {
			members.addAll(messages.getMembers());
		}

		if (!members.contains(targetUserId)) {
			String topic = UUID.randomUUID().toString();
			String creatorName = targetName;

			List<UserInfo> userInfo = new LinkedList<>();
			userInfo.add(new UserInfo(targetUserId, false, creatorName, null));
			userInfo.add(new UserInfo(userId, false, username, null));

			ChatObject msg = null;

			if (type == ChatRequestType.MESSAGE_REQUEST)
				msg = ChatObject.createMessageRequestInstance(userId, username, topic,
						Arrays.asList(userId, targetUserId), Arrays.asList(username, creatorName), userInfo, false);
			else if (type == ChatRequestType.CONTACT_REQUEST)
				msg = ChatObject.createContactRequestInstance(userId, username, topic,
						Arrays.asList(userId, targetUserId), Arrays.asList(username, creatorName), userInfo, false,
						title);

			if (msg != null) {
				getChats().create(topic, msg);
				getChats().publish(topic, new ChatMessage(userId, username, requestMessage, true));
				return true;
			}
		}

		return false;
	}

	public boolean updateRequestStatus(ChatRequestType type, String topic, boolean status, String userId,
			String username, Cookie cookie) {
		ChatObject chat = this.getChatObject(topic);

		boolean success = false;

		if (chat != null) {
			if (type == ChatRequestType.MESSAGE_REQUEST) {
				if (status) {
					chat.setMessageRequest(false);
					chat.setMessageRequestAccepted(true);
					chats.set(topic, chat);
				} else {
					chats.delMessageAll(topic);
					chats.del(topic);
				}

				String targetId = "";

				for (String theUserId : chat.getMembers()) {
					if (!theUserId.equals(userId)) {
						targetId = theUserId;
					}
				}

				MessageRequestResponse answer = new MessageRequestResponse(userId, status);
				ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

				String json = "{}";

				try {
					json = ow.writeValueAsString(answer);
				} catch (JsonProcessingException e) {
					System.out.println(e.toString());
				}

				// update status on profile
				Client client = StimeyClient.create();
				Response response = StimeyClient.post(client, "dashboard", Global.DOCKER,
						"api/profiles/" + targetId + "/messageRequests", cookie, json);

				if (response.getStatus() == 200) {
					success = true;
				} else {
					success = false;
				}
			} else if (type == ChatRequestType.CONTACT_REQUEST) {
				if (status) {
					chat.setContactRequest(true);
					chat.setContactRequestAccepted(true);
					chat.setContactRequestDeclined(false);
					chats.set(topic, chat);
					success = true;
				} else {
					chat.setContactRequest(true);
					chat.setContactRequestAccepted(false);
					chat.setContactRequestDeclined(true);

					// ChatMessage deniedMsg = new ChatMessage(userId, username, username + "
					// declined your contact request.", true);
					// chats.publish(topic, deniedMsg);

					chats.set(topic, chat);
					success = true;
				}
			} else {
				success = false;
			}
		}

		return success;
	}

	public List<ChatObject> getUserChats(String userId, ChatListFilter filter, String searchMessage) {
		if (filter == null) {
			filter = ChatListFilter.SHOW_ALL;
		}

		LinkedHashSet<ChatObject> result = new LinkedHashSet<>();
		List<String> topics = chats.findAll(
				new Document("members", new Document("$elemMatch", new Document("$eq", userId))),
				new Document("lastAccess", -1), true);

		for (String topic : topics) {
			ChatObject msg = getChatObject(topic);
			if (filter == ChatListFilter.SHOW_ALL) {
				if (searchTextExists(searchMessage, msg)) {
					result.add(msg);
				}
			} else if (filter == ChatListFilter.ONLY_GROUP) {
				if (msg.isGroupValue()) {
					if (searchTextExists(searchMessage, msg))
						result.add(msg);
				}
			} else if (filter == ChatListFilter.UNREAD_MESSAGES) {
				for (Message message : msg.getMessages()) {
					ChatMessage chatMessage = getChatMessage(topic, message.getId());
					if (!chatMessage.getUserId().equals(userId)) {
						if (chatMessage.isMessageUnread()) {
							if (searchTextExists(searchMessage, msg))
								result.add(msg);
						}
					}
				}
			} else if (filter == ChatListFilter.MESSAGE_REQUEST) {
				if (msg.isMessageRequest() && !msg.isMessageRequestAccepted() && !msg.getCreator().equals(userId)) {
					if (searchTextExists(searchMessage, msg))
						result.add(msg);
				}
			} else if (filter == ChatListFilter.CONTACT_REQUEST) {
				if (msg.isContactRequest() && !msg.isContactRequestAccepted() && !msg.getCreator().equals(userId)) {
					if (searchTextExists(searchMessage, msg))
						result.add(msg);
				}
			}
		}

		return new ArrayList<>(result);
	}

	public int getUnreadMessagesCount(final String ownUserId) {
		Set<String> unreadMessages = new HashSet<>();
		List<String> topics = chats.findAll(
				new Document("members", new Document("$elemMatch", new Document("$eq", ownUserId))),
				new Document("lastAccess", -1), true);

		for (String topic : topics) {
			ChatObject msg = getChatObject(topic);
			for (Message obj : msg.getMessages()) {
				ChatMessage chatMessage = getChatMessage(topic, obj.getId());

				boolean check = (msg.isGroupValue() ? chatMessage.getUnreadGroupMessages().get(ownUserId)
						: chatMessage.isMessageUnread()) && !chatMessage.getUserId().equals(ownUserId);

				if (check) {
					unreadMessages.add(chatMessage.getTopic());
					break;
				}
			}
		}

		return unreadMessages.size();
	}

	public List<UnreadMessageWrapper> getLastUnreadMessages(final String ownUserId, final Cookie cookie) {
		final int maxMessageLength = 30;
		Set<UnreadMessageWrapper> unreadMessages = new HashSet<>();
		List<String> topics = chats.findAll(
				new Document("members", new Document("$elemMatch", new Document("$eq", ownUserId))),
				new Document("lastAccess", -1), true);

		Client client = StimeyClient.create();

		for (String topic : topics) {
			ChatObject msg = getChatObject(topic);
			if (msg.getMessages().size() > 0) {
				Message lastMsg = msg.getMessages().get(msg.getMessages().size() - 1);
				ChatMessage chatMessage = getChatMessage(topic, lastMsg.getId());

				boolean check = (msg.isGroupValue() ? chatMessage.getUnreadGroupMessages().get(ownUserId)
						: chatMessage.isMessageUnread()) && !chatMessage.getUserId().equals(ownUserId);

				if (check) {
					String message = chatMessage.getValue().toString();

					Friend friend = getFriendsProfile(client, chatMessage.getUserId(), cookie);

					String displayMessage = trimUnreadMessage(message, maxMessageLength);

					UnreadMessageWrapper umw = new UnreadMessageWrapper(
							(msg.isGroupValue()) ? msg.getMsgTopic() : chatMessage.getUserId(),
							(friend != null) ? friend.getAvatarImageId() : "", msg.getCreator(),
							chatMessage.getKey().toString(), displayMessage, chatMessage.getTimestamp(),
							msg.isGroupValue());

					unreadMessages.add(umw);
				}
			}
		}

		// sort last unread messages by timestamp
		List<UnreadMessageWrapper> sorted = new ArrayList<>(unreadMessages);
		Collections.sort(sorted);

		return sorted;
	}

	private Friend getFriendsProfile(final Client client, final String friendId, final Cookie cookie) {
		Response response = StimeyClient.get(client, "dashboard", Global.DOCKER, "api/profile/friend/" + friendId,
				cookie);
		Friend friend = new Friend();

		if (response != null && response.getStatus() == 200) {
			try {
				friend = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
						.readValue(response.readEntity(String.class), new TypeReference<Friend>() {
						});
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return friend;
	}

	/*
	 * Trim the message to max length and filter HTML tags (e.g mentions)
	 */
	private String trimUnreadMessage(String message, int maxLength) {
		// replace html tags for preview
		String withoutHtmlTags = message.replaceAll("<[^>]*>", "");

		if (withoutHtmlTags.length() <= maxLength) {
			return withoutHtmlTags;
		} else {
			return withoutHtmlTags.substring(0, Math.min(withoutHtmlTags.length(), maxLength)) + "...";
		}
	}

	/*
	 * Search for a specific message in your chats
	 */
	private boolean searchTextExists(String searchText, ChatObject msg) {
		boolean exists = false;

		if (searchText.length() < 3)
			return true;

		if (msg.getCreator().toLowerCase().equals(searchText.toLowerCase())) {
			exists = true;
		}

		for (String user : msg.getMembersName()) {
			if (user.toLowerCase().equals(searchText.toLowerCase())) {
				exists = true;
				break;
			}
		}

		for (Message message : msg.getMessages()) {
			if (message.getValue().toString().toLowerCase().contains(searchText.toLowerCase())) {
				exists = true;
				break;
			}
		}

		return exists;
	}

	public List<ChatObject> sortWithAlphabeticalOrder(String userid) {
		List<ChatObject> result = new LinkedList<>();
		List<String> topics = chats.findAll(
				new Document("members", new Document("$elemMatch", new Document("$eq", userid))),
				new Document("creator", 1), true);
		for (String topic : topics)
			result.add(getChatObject(topic));
		return result;
	}

	public List<ChatObject> getChatList(List<ChatObject> listMessages, String userId, ChatListFilter filter) {
		List<ChatObject> chatList = new LinkedList<>();

		for (ChatObject messages : listMessages) {
			if (!messages.isGroupValue()) {
				if (filter == ChatListFilter.SHOW_ALL) {
					for (UserInfo tempUserInfo : messages.getUserInfo()) {
						if (tempUserInfo.getUserId().equals(userId) && tempUserInfo.isShowConversationValue()) {
							if (messages.getMembers().get(0).equals(userId)) {
								messages.setCreator(messages.getMembersName().get(1));
							}
							chatList.add(messages);
							break;
						}
					}
				}
			} else {
				for (UserInfo tempUserInfo : messages.getUserInfo()) {
					if (tempUserInfo.getUserId().equals(userId) && tempUserInfo.isShowConversationValue()) {
						chatList.add(messages);
						break;
					}
				}
			}
		}
		return chatList;
	}

	public List<ChatObject> getChatSearchList(List<ChatObject> listMessages, String userId, String searchLabel) {
		List<ChatObject> chatSearchList = new LinkedList<>();
		for (ChatObject messages : listMessages) {
			List<String> membersNoMe;
			membersNoMe = messages.getMembers();
			membersNoMe.remove(userId);
			for (UserInfo tempUserInfo : messages.getUserInfo()) {
				if (tempUserInfo.getUserId().equals(userId) && tempUserInfo.isShowConversationValue()) {
					if (containsCaseInsenstive(searchLabel, membersNoMe)
							|| messages.getCreator().toLowerCase().contains(searchLabel.toLowerCase())) {
						chatSearchList.add(messages);
						break;
					}
				}
			}
		}
		return chatSearchList;
	}

	public List<ChatObject> getFriendsList(List<ChatObject> listMessages, String userId) {
		List<ChatObject> friendsList = new LinkedList<>();
		for (ChatObject messages : listMessages) {
			if (!messages.isGroupValue()) {
				if (messages.getMembers().get(0).equals(userId)) {
					messages.setCreator(messages.getMembersName().get(1));
				}
				friendsList.add(messages);
			}
		}
		return friendsList;
	}

	public List<ChatObject> getFriendsSearchList(List<ChatObject> listMessages, String userId, String searchLabel) {
		List<ChatObject> friendsSearchList = new LinkedList<>();
		for (ChatObject messages : listMessages) {
			List<String> membersNoMe = new LinkedList<>();
			membersNoMe = messages.getMembers();
			membersNoMe.remove(userId);
			if (!messages.isGroupValue()) {
				if (containsCaseInsenstive(searchLabel, membersNoMe)
						|| messages.getCreator().toLowerCase().contains(searchLabel.toLowerCase()))
					friendsSearchList.add(messages);
			}
		}
		return friendsSearchList;
	}

	public List<ChatObject> getGroupsList(List<ChatObject> listMessages, String userId) {
		List<ChatObject> groupsList = new LinkedList<>();
		for (ChatObject messages : listMessages)
			if (messages.isGroupValue())
				groupsList.add(messages);
		return groupsList;
	}

	public List<ChatObject> getGroupsSearchList(List<ChatObject> listMessages, String userId, String searchLabel) {
		List<ChatObject> groupsSearchList = new LinkedList<>();
		for (ChatObject messages : listMessages) {
			List<String> membersNoMe = new LinkedList<>();
			membersNoMe = messages.getMembers();
			membersNoMe.remove(userId);
			if (messages.isGroupValue()) {
				if (containsCaseInsenstive(searchLabel, membersNoMe)
						|| messages.getCreator().toLowerCase().contains(searchLabel.toLowerCase()))
					groupsSearchList.add(messages);
			}
		}
		return groupsSearchList;
	}

	public boolean containsCaseInsenstive(String searchLabel, List<String> userIdMembers) {
		for (String userId : userIdMembers) {
			UserSharedCacheObject userSharedCacheObject = userSharedService.getUser(userId);
			if (userSharedCacheObject != null
					&& userSharedCacheObject.username.toLowerCase().contains(searchLabel.toLowerCase())) {
				return true;
			}
		}
		return false;
	}

	public boolean userInfoContainsUserId(List<UserInfo> userInfos, String userId) {
		for (UserInfo userInfo : userInfos) {
			if (userInfo.getUserId().equals(userId)) {
				userInfos.add(userInfo);
				return true;
			}
		}
		return false;
	}

	public UserInfo getContainsUserInfo(List<UserInfo> userInfos, String userId) {
		for (UserInfo userInfo : userInfos) {
			if (userInfo.getUserId().equals(userId)) {
				if (!userInfo.isShowConversationValue()) {
					userInfo.setShowConversationValue(true);
					return userInfo;
				} else {
					return userInfo;
				}
			}
		}
		return null;
	}
}
