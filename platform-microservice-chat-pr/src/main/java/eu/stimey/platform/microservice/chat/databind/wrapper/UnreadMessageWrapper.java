package eu.stimey.platform.microservice.chat.databind.wrapper;

import java.util.Date;

public class UnreadMessageWrapper implements Comparable<UnreadMessageWrapper> {
	public String userId;
	public String avatarId;
	public String groupname;
	public String username;
	public String shortMessage;
	public Date timestamp;
	public boolean isGroupChat;

	public UnreadMessageWrapper() {
	}

	public UnreadMessageWrapper(String userId, String avatarId, String groupname, String username, String shortMessage,
			Date timestamp, boolean isGroupChat) {
		this.userId = userId;
		this.avatarId = avatarId;
		this.groupname = groupname;
		this.username = username;
		this.shortMessage = shortMessage;
		this.timestamp = timestamp;
		this.isGroupChat = isGroupChat;
	}

	@Override
	public int compareTo(UnreadMessageWrapper o) {
		return o.timestamp.compareTo(timestamp);
	}
}
