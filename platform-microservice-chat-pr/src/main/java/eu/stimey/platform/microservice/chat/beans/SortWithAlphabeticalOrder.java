package eu.stimey.platform.microservice.chat.beans;

import java.util.Comparator;

import eu.stimey.platform.microservice.chat.databind.ChatObject;

public class SortWithAlphabeticalOrder implements Comparator<ChatObject> {

	@Override
	public int compare(ChatObject a, ChatObject b) {
		return a.getCreator().compareToIgnoreCase(b.getCreator());
	}

}
