package eu.stimey.platform.microservice.chat.rest.resources;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.microservice.chat.databind.ChatObject;
import eu.stimey.platform.microservice.chat.databind.wrapper.ContactRequestWrapper;
import eu.stimey.platform.microservice.chat.databind.wrapper.GroupDataWrapper;
import eu.stimey.platform.microservice.chat.databind.wrapper.UnreadMessageWrapper;
import eu.stimey.platform.microservice.chat.databind.wrapper.UserMessageWrapper;
import eu.stimey.platform.microservice.chat.databind.filter.ChatListFilter;
import eu.stimey.platform.microservice.chat.databind.filter.ChatRequestType;
import eu.stimey.platform.microservice.chat.beans.ChatService;
import eu.stimey.platform.microservice.chat.beans.SortWithAlphabeticalOrder;
import eu.stimey.platform.microservice.chat.databind.ChatMessage;
import eu.stimey.platform.microservice.chat.databind.UserInfo;
import eu.stimey.platform.microservice.chat.rest.databind.Friend;
import eu.stimey.platform.microservice.chat.rest.databind.MessageRequest;
import eu.stimey.platform.microservice.chat.startup.Global;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.server.ContainerRequest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Path("/chat")
public class ChatResource {
	@Inject
	UserSharedService userSharedService;
	@Inject
	ChatService service;

	@GET
	@Path("/chatpage/{filter}/{searchMessage}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response showChatPage(@Context ContainerRequestContext requestContext,
			@PathParam("filter") final String chatListFilter, @PathParam("searchMessage") final String searchMessage) {
		String userId = (String) requestContext.getProperty("userid");

		boolean emptyChatList = false; // can be removed!

		// added chatlistfilter
		ChatListFilter filter = ChatListFilter.fromString(chatListFilter);

		Object[] responseObjects = new Object[2];
		responseObjects[0] = service.getUserChats(userId, filter, searchMessage);
		responseObjects[1] = emptyChatList;

		return Response.status(200).entity(responseObjects).build();
	}

	/*
	 * REQUEST HANDLING
	 */
	@POST
	@Path("/send-message-request")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response sendMessageRequest(@Context ContainerRequestContext requestContext,
			final MessageRequest messageRequest) {
		final String userId = (String) requestContext.getProperty("userid");
		final String username = (String) requestContext.getProperty("username");

		if (messageRequest == null)
			return Response.status(401).build();

		if (!userId.equals(messageRequest.from))
			return Response.status(401).build();

		boolean status = service.createRequestType(ChatRequestType.MESSAGE_REQUEST, userId, messageRequest.targetId,
				username, messageRequest.targetName, messageRequest.message, "");

		if (status)
			return Response.status(200).entity(true).build();
		else
			return Response.status(204).build();
	}

	@POST
	@Path("/send-contact-request")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response sendContactRequest(@Context ContainerRequestContext requestContext,
			final ContactRequestWrapper contactRequestWrapper) {
		final String userId = (String) requestContext.getProperty("userid");
		final String username = (String) requestContext.getProperty("username");

		boolean status = service.createRequestType(ChatRequestType.CONTACT_REQUEST, userId,
				contactRequestWrapper.targetId, username, contactRequestWrapper.targetName,
				contactRequestWrapper.message, contactRequestWrapper.title);

		if (status)
			return Response.status(200).entity(true).build();
		else
			return Response.status(404).build();
	}

	@PUT
	@Path("/update-message-request/{topic}/{status}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response updateMessageRequest(@Context ContainerRequestContext requestContext,
			@PathParam("topic") final String topic, @PathParam("status") final boolean status,
			@CookieParam("access_token") final Cookie cookie) {
		final String userId = (String) requestContext.getProperty("userid");
		final String username = (String) requestContext.getProperty("username");

		boolean valid = service.updateRequestStatus(ChatRequestType.MESSAGE_REQUEST, topic, status, userId, username,
				cookie);

		if (valid) {
			return Response.status(200).build();
		} else {
			return Response.status(404).build();
		}
	}

	@GET
	@Path("/get-last-unread-messages")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLastUnreadMessages(@Context ContainerRequestContext requestContext,
			@CookieParam("access_token") final Cookie cookie) {
		String userId = (String) requestContext.getProperty("userid");

		if (userId == null || userId.isEmpty()) {
			return Response.status(405).build();
		}

		List<UnreadMessageWrapper> unreadMessages = service.getLastUnreadMessages(userId, cookie);

		return Response.status(200).entity(unreadMessages).build();
	}

	@PUT
	@Path("/update-contact-request/{topic}/{status}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response updateContactRequest(@Context ContainerRequestContext requestContext,
			@PathParam("topic") final String topic, @PathParam("status") final boolean status,
			@CookieParam("access_token") final Cookie cookie) {
		final String userId = (String) requestContext.getProperty("userid");
		final String username = (String) requestContext.getProperty("username");

		boolean valid = service.updateRequestStatus(ChatRequestType.CONTACT_REQUEST, topic, status, userId, username,
				cookie);

		if (valid)
			return Response.status(200).build();
		else
			return Response.status(404).build();
	}

	@POST
	@Path("/msg-status/read/{topic}/{messageId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateMessageStatus(@Context ContainerRequestContext requestContext,
			@PathParam("topic") String topic, @PathParam("messageId") String messageId) {
		String userId = (String) requestContext.getProperty("userid");

		service.setMessageReadStatus(userId, topic, messageId, false);

		Object[] responseObject = new Object[1];
		responseObject[0] = true;

		return Response.status(200).entity(responseObject).build();
	}

	@GET
	@Path("/get-unread-messages-count")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUnreadUserMessages(@Context ContainerRequestContext requestContext) {
		String userId = (String) requestContext.getProperty("userid");

		int unreadMessagesCounter = service.getUnreadMessagesCount(userId);

		return Response.status(200).entity(unreadMessagesCounter).build();
	}

	@GET
	@Path("/chat-delete/{topic}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response chatDelete(@Context ContainerRequestContext requestContext, @PathParam("topic") String topic) {
		String userId = (String) requestContext.getProperty("userid");

		ChatObject currentMessage = service.getChatObject(topic);
		List<UserInfo> userInfos = new LinkedList<>();
		List<ChatObject> chatList;

		// System.out.println("test1" + currentMessage);

		for (UserInfo tempUserInfo : currentMessage.getUserInfo()) {
			if (tempUserInfo.getUserId().equals(userId) && tempUserInfo.isShowConversationValue()) {
				tempUserInfo.setShowConversationValue(false);
				tempUserInfo.setStartConversationTime(new Date());
			}
			userInfos.add(tempUserInfo);
		}

		currentMessage.setUserInfo(userInfos);
		service.getChats().set(topic, currentMessage);

		// alle nachrichten aus dem chat löschen
		service.getChats().delMessageAll(topic);
		// den gesamten Chat löschen
		service.getChats().del(topic);

		chatList = service.getChatList(service.getUserChats(userId, null, ""), userId, ChatListFilter.SHOW_ALL);

		return Response.status(200).entity(chatList).build();
	}

	@Context
	private HttpServletRequest servletRequest;

	@POST
	@Path("/chat-messages-post/{topic}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postChatMessagePage(@Context ContainerRequestContext requestContext,
			@PathParam("topic") String topic, UserMessageWrapper userMessageWrapper) {

		String userId = (String) requestContext.getProperty("userid");
		String username = (String) requestContext.getProperty("username");

		Pattern p = Pattern.compile("\\B@[a-z0-9_-]+");
		Matcher m = p.matcher(userMessageWrapper.message);
		String parsedMessage = userMessageWrapper.message;

		ArrayList<String> matchedNames = new ArrayList<>();

		while (m.find()) {
			String matchedName = m.group();
			matchedNames.add(matchedName.split("@")[1]);
		}

		ChatObject currentMessage = service.getChatObject(topic);

		// update UserInfos
		List<UserInfo> userInfos = new LinkedList<>();
		List<String> userIds = new ArrayList<>();

		for (UserInfo tempUserInfo : currentMessage.getUserInfo()) {
			if (!tempUserInfo.getUserId().equals(userId) && !tempUserInfo.isShowConversationValue()) {
				tempUserInfo.setShowConversationValue(true);
				tempUserInfo.setStartConversationTime(new Date());
			}

			userIds.add(tempUserInfo.getUserId());
			userInfos.add(tempUserInfo);

			if (matchedNames.contains(tempUserInfo.getMembersName())) {
				String replaceUrl = "<a href='/profiles/" + tempUserInfo.getUserId() + "' target='_blank'>@"
						+ tempUserInfo.getMembersName() + "</a>";
				parsedMessage = parsedMessage.replace("@" + tempUserInfo.getMembersName(), replaceUrl);
			}
		}

		currentMessage.setTimestamp(new Date());
		currentMessage.setUserInfo(userInfos);
		currentMessage.setLastAccess(new Date());

		service.getChats().set(topic, currentMessage);

		ChatMessage childMessage;

		if (currentMessage.isGroupValue()) {
			childMessage = new ChatMessage(userId, userIds, username, parsedMessage, true);
		} else {
			childMessage = new ChatMessage(userId, username, parsedMessage, true);
		}

		if (userMessageWrapper.photoFileId != null)
			childMessage.addPhoto(userMessageWrapper.photoFileId, "");

		if (userMessageWrapper.fileId != null) {
			childMessage.addFile(userMessageWrapper.fileId, userMessageWrapper.fileName, "");
		}

		service.getChats().publish(topic, childMessage);

		return Response.status(200).build();
	}

	@POST
	@Path("/get-message-photo")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public void getPhoto(@Context HttpServletResponse response, @QueryParam("photoId") String photoId) {
		Client client = ClientBuilder.newClient();
		String target = "http://ms_files:8080/api";
		if (!Global.DOCKER)
			target = "http://localhost:8080/platform-microservice-files/api";
		InputStream inputStream = client.target(target).path("files/" + photoId)
				.request(MediaType.APPLICATION_OCTET_STREAM).headers(null).get(InputStream.class);

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			IOUtils.copy(inputStream, outputStream);
			inputStream.close();

			response.getOutputStream().write(outputStream.toByteArray());
			response.getOutputStream().flush();
			response.getOutputStream().close();
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@GET
	@Path("/friends-list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response showFriendsList(@Context ContainerRequestContext requestContext,
			@CookieParam("access_token") Cookie cookie) {
		String userId = (String) requestContext.getProperty("userid");

		Client client = StimeyClient.create();
		Response response = StimeyClient.get(client, "dashboard", Global.DOCKER, "api/profiles/" + userId + "/friends",
				cookie);

		List<Friend> friends = null;

		if (response != null && response.getStatus() == 200) {
			try {
				friends = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
						.readValue(response.readEntity(String.class), new TypeReference<List<Friend>>() {
						});
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		Object[] responseObjects = new Object[2];
		responseObjects[0] = friends;
		responseObjects[1] = userId;

		return Response.status(200).entity(responseObjects).build();
	}

	@POST
	@Path("/start-chat-go/{stringfriend}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response startChat(@Context ContainerRequestContext requestContext,
			@PathParam("stringfriend") String friendid, @CookieParam("access_token") Cookie cookie) {
		String userId = (String) requestContext.getProperty("userid");
		String username = (String) requestContext.getProperty("username");

		Client client = StimeyClient.create();

		/*
		 * Daten des Freundes anfragen per REST JSON response dann in eine Klasse
		 * konvertieren
		 */
		Response response = StimeyClient.get(client, "dashboard", Global.DOCKER, "api/profile/friend/" + friendid,
				cookie);
		Friend friend = new Friend();

		if (response != null && response.getStatus() == 200) {
			try {
				friend = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
						.readValue(response.readEntity(String.class), new TypeReference<Friend>() {
						});
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		/*
		 * Daten vom eigenen Profil abrufen per REST JSON response dann in eine Klasse
		 * konvertieren
		 */
		response = StimeyClient.get(client, "dashboard", Global.DOCKER, "api/profile/friend/" + userId, cookie);
		Friend self = new Friend();

		if (response != null && response.getStatus() == 200) {
			try {
				self = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
						.readValue(response.readEntity(String.class), new TypeReference<Friend>() {
						});
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		Set<String> set = new HashSet<>();

		// alle vergangenen nachrichten auslesen?
		for (ChatObject messages : service.getFriendsList(service.sortWithAlphabeticalOrder(userId), userId))
			set.addAll(messages.getMembers());

		if (!set.contains(friendid)) {
			String topic = UUID.randomUUID().toString();
			String creatorName = friend.getUsername();
			String creatorType = friend.getUsertype();
			List<UserInfo> userInfo = new LinkedList<>();

			userInfo.add(new UserInfo(friendid, false, creatorName, friend.getAvatarImageId()));
			userInfo.add(new UserInfo(userId, false, username, self.getAvatarImageId()));

			ChatObject msg = ChatObject.createUserChatInstance(userId, username, topic, Arrays.asList(userId, friendid),
					Arrays.asList(username, creatorName), userInfo);
			service.getChats().create(topic, msg);

			return Response.status(200).entity(msg).build();
		}

		if (set.contains(friendid)) {
			ChatObject theChat = null;

			for (ChatObject obj : service.getFriendsList(service.sortWithAlphabeticalOrder(userId), userId)) {
				if (obj.getMembers().contains(friendid)) {
					theChat = obj;
				}
			}

			if (theChat != null) {
				return Response.status(200).entity(theChat).build();
			} else {
				return Response.status(204).build();
			}
		}

		return Response.status(204).build();
	}

	/*
	 * Group List Functions
	 */

	@GET
	@Path("/groups-list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response showGroupsList(@Context ContainerRequestContext requestContext) {
//		response.setHeader("Content-Disposition", "attachment");
//		response.setHeader("Content-Type", "text/html");

		String userId = (String) requestContext.getProperty("userid");

		List<ChatObject> groupsList = new LinkedList<>();
		groupsList = service.getGroupsList(service.sortWithAlphabeticalOrder(userId), userId);

		// System.out.println("groups-list" + groupsList);

		return Response.status(200).entity(groupsList).build();
	}

	@GET
	@Path("/groups-search/{groupSerachLable}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response showGroupsSerach(@Context ContainerRequestContext requestContext,
			@PathParam("groupSerachLable") String searchLabel) {

		String userId = (String) requestContext.getProperty("userid");

//		response.setHeader("Content-Disposition", "attachment");
//		response.setHeader("Content-Type", "text/html");
		// System.out.println(searchLabel);

		List<ChatObject> groupsSearchList = new LinkedList<>();
		groupsSearchList = service.getGroupsSearchList(service.getUserChats(userId, null, ""), userId, searchLabel);
		// System.out.println("groups-search" + groupsSearchList);
		return Response.status(200).entity(groupsSearchList).build();
	}

	@GET
	@Path("/team-page/{topic}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response showTeamPage(@Context ContainerRequestContext requestContext, @PathParam("topic") String topic) {
//		response.setHeader("Content-Disposition", "attachment");
//		response.setHeader("Content-Type", "text/html");

		String userId = (String) requestContext.getProperty("userid");
		String username = (String) requestContext.getProperty("username");

		ChatObject teamMessages = service.getChatObject(topic);
		// System.out.println("team-page" + teamMessages);
		String groupAdminUsername = userSharedService.getUser(teamMessages.getGroupAdminID()).username;

		Object[] responseobjects = new Object[4];
		responseobjects[0] = groupAdminUsername;
		responseobjects[1] = teamMessages;
		responseobjects[2] = userId;
		responseobjects[3] = username;

		return Response.status(200).entity(responseobjects).build();
	}

	@GET
	@Path("/start-group")
	@Produces(MediaType.APPLICATION_JSON)
	public Response showStartGroup(@Context ContainerRequestContext requestContext) {
//		response.setHeader("Content-Disposition", "attachment");
//		response.setHeader("Content-Type", "text/html");

		String userId = (String) requestContext.getProperty("userid");

		List<ChatObject> friendsList = new LinkedList<>();
		List<ChatObject> firstTenFriendMembers = new LinkedList<>();

		friendsList = service.getFriendsList(service.sortWithAlphabeticalOrder(userId), userId);
		Collections.sort(friendsList, new SortWithAlphabeticalOrder());

		Object[] responseobjects = new Object[3];

		if (friendsList.size() <= 10) {
			responseobjects[0] = friendsList;
		} else {
			firstTenFriendMembers = friendsList.subList(0, 10);
			responseobjects[0] = firstTenFriendMembers;
		}
		// System.out.println("start-group" + friendsList);

		responseobjects[1] = userId;
		responseobjects[2] = friendsList;

		return Response.status(200).entity(responseobjects).build();
	}

	@POST
	@Path("/start-group-go/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response startGroupGo(@Context ContainerRequestContext requestContext, final GroupDataWrapper wrapper) {

		String userId = (String) requestContext.getProperty("userid");
		String username = (String) requestContext.getProperty("username");

		List<UserInfo> userInfos = new LinkedList<>();
		List<String> groupMemberIds = new LinkedList<>();

		groupMemberIds.add(userId);
		if (wrapper.memberList!=null) {
			wrapper.memberList.add(username);
			userInfos.add(new UserInfo(userId, true, username, ""));
		
			for (String groupUsername : wrapper.memberList) {
				String groupUserId = userSharedService.getUserId(groupUsername);
				groupMemberIds.add(groupUserId);
				userInfos.add(new UserInfo(groupUserId, true, groupUsername, ""));
			}
		}

		String topic = UUID.randomUUID().toString();

		ChatObject msg = ChatObject.createUserGroupInstance(userId, wrapper.groupName, userId, username, topic,
				groupMemberIds, wrapper.memberList, userInfos);
		service.getChats().create(topic, msg);

		return Response.status(200).build();
	}

	@GET
	@Path("/edit-group/{topic}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response editGroup(@Context ContainerRequestContext requestContext, @PathParam("topic") String topic) {

		String userId = (String) requestContext.getProperty("userid");
		String username = (String) requestContext.getProperty("username");

		List<ChatObject> friendsList = new LinkedList<>();
		List<String> groupMembersUsername = new LinkedList<>();
		groupMembersUsername = service.getChatObject(topic).getMembersName();
		groupMembersUsername.remove(username);
		friendsList = service.getFriendsList(service.sortWithAlphabeticalOrder(userId), userId);
		Collections.sort(friendsList, new SortWithAlphabeticalOrder());

		Object[] responseobjects = new Object[3];
		responseobjects[0] = service.getChatObject(topic);
		responseobjects[1] = friendsList;
		responseobjects[2] = groupMembersUsername;
		return Response.status(200).entity(responseobjects).build();
	}

	@POST
	@Path("/edit-group-go")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response editGroupGo(@Context ContainerRequestContext requestContext, final GroupDataWrapper groupData) {
		String userId = (String) requestContext.getProperty("userid");
		String username = (String) requestContext.getProperty("username");

		ChatObject currentMessage = service.getChatObject(groupData.topic);

		List<String> groupMemberIds = new LinkedList<>();
		List<UserInfo> userInfos = new LinkedList<>();

		groupMemberIds.add(userId);
		userInfos.add(new UserInfo(userId, true, username, ""));
		if (groupData.memberList!=null) {
			groupData.memberList.add(username);

			for (String memberName : groupData.memberList) {
				String memberID = userSharedService.getUserId(memberName);
				groupMemberIds.add(memberID);
	
				userInfos.add(new UserInfo(memberID, true, memberName, ""));
			}
		}

		currentMessage.setCreator(groupData.groupName);
		currentMessage.setMembers(groupMemberIds);
		currentMessage.setMembersName(groupData.memberList);
		currentMessage.setUserInfo(userInfos);

		if (groupData.groupImageId!=null && !groupData.groupImageId.equals("xxx"))
			currentMessage.setGroupImageId(groupData.groupImageId);
		else
			currentMessage.setGroupImageId("");

		service.getChats().set(groupData.topic, currentMessage);

		return Response.status(200).build();
	}

	@GET
	@Path("/{groupId}/group-members")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupMemberList(@Context ContainerRequestContext requestContext,
			@PathParam("groupId") final String groupId) {
		ChatObject currentMessage = service.getChatObject(groupId);

		if (currentMessage != null) {
			List<String> memberList = currentMessage.getMembers();
			return Response.status(200).entity(memberList).build();
		}

		return Response.status(404).build();
	}

	@GET
	@Path("/leave-group/{topic}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response leaveGroup(@Context ContainerRequestContext requestContext, @PathParam("topic") String topic) {
		String userId = (String) requestContext.getProperty("userid");
		String username = (String) requestContext.getProperty("username");

		ChatObject currentMessage = service.getChatObject(topic);

		if (currentMessage.getMembers().size() == 1 || currentMessage.getGroupAdminID().equals(userId)) {
			service.getChats().del(topic);
		} else {
			List<String> members = currentMessage.getMembers();
			List<String> membersName = currentMessage.getMembersName();
			List<UserInfo> userInfos = new LinkedList<>();

			members.remove(userId);
			membersName.remove(username);

			for (UserInfo userInfo : currentMessage.getUserInfo())
				if (!userInfo.getUserId().equals(userId))
					userInfos.add(userInfo);

			currentMessage.setMembers(members);
			currentMessage.setMembersName(membersName);
			currentMessage.setUserInfo(userInfos);

			// System.out.println("members:");
			// System.out.println(members.toString());

			service.getChats().set(topic, currentMessage);

			// send message to remaining group members that this user left the group
			ChatMessage msg = new ChatMessage(userId, username, username + " has left the group.", false);
			service.getChats().publish(topic, msg);
		}

		List<ChatObject> groupsList = new LinkedList<>();
		for (ChatObject message : service.sortWithAlphabeticalOrder(userId)) {
			if (message.isGroupValue() == true) {
				groupsList.add(message);
			}
		}

		return Response.status(200).entity(groupsList).build();
	}

	@GET
	@Path("/admin-leave-group")
	@Produces(MediaType.APPLICATION_JSON)
	public Response adminLeaveGroup(@Context ContainerRequestContext requestContext,
			@QueryParam("topic") String topic) {
		String username = (String) requestContext.getProperty("username");

		List<String> groupMembersUsername;
		groupMembersUsername = service.getChatObject(topic).getMembersName();
		groupMembersUsername.remove(username);

		Object[] responseObjects = new Object[2];
		responseObjects[0] = topic;
		responseObjects[1] = groupMembersUsername;
		return Response.status(200).entity(responseObjects).build();
	}

	@POST
	@Path("/admin-leave-group-go")
	@Produces(MediaType.APPLICATION_JSON)
	public Response adminLeaveGroup(@Context ContainerRequestContext requestContext,
			@QueryParam("newAdmin") String newAdmin, @QueryParam("topic") String topic) {

		String userId = (String) requestContext.getProperty("userid");
		String username = (String) requestContext.getProperty("username");

		ChatObject currentMessage = service.getChatObject(topic);
		if (currentMessage.getMembers().size() == 1) {
			service.getChats().del(topic);
		} else {
			List<String> members = currentMessage.getMembers();
			List<String> membersName = currentMessage.getMembersName();
			List<UserInfo> userInfos = new LinkedList<>();
			members.remove(userId);
			membersName.remove(username);
			for (UserInfo userInfo : currentMessage.getUserInfo())
				if (!userInfo.getUserId().equals(userId))
					userInfos.add(userInfo);

			currentMessage.setMembers(members);
			currentMessage.setMembersName(membersName);
			currentMessage.setUserInfo(userInfos);
			currentMessage.setGroupAdminID(userSharedService.getUserId(newAdmin));
			service.getChats().set(topic, currentMessage);

		}

		return Response.status(200).build();
	}

}
