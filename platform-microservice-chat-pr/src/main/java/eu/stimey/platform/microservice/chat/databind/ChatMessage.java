package eu.stimey.platform.microservice.chat.databind;

import java.util.*;

import eu.stimey.platform.library.data.access.databind.Message;

public class ChatMessage extends Message {
	private List<String> images;
	private List<String> files;
	private String userId;

	private boolean messageUnread;
	private Map<String, Boolean> unreadGroupMessages;

	public ChatMessage() {
		this("", null, null, false);
	}

	public ChatMessage(String userId, String username, String value, boolean status) {
		super(username, value);

		this.userId = userId;
		this.images = new LinkedList<>();
		this.files = new LinkedList<>();
		this.messageUnread = status;
	}

	public ChatMessage(String userId, List<String> userIds, String username, String value, boolean status) {
		super(username, value);

		this.userId = userId;
		this.unreadGroupMessages = new HashMap<>();

		for (String uid : userIds) {
			this.unreadGroupMessages.put(uid, status);
		}

		this.images = new LinkedList<>();
		this.files = new LinkedList<>();
		this.messageUnread = status;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

	public List<String> getFiles() {
		return files;
	}

	public void setFiles(List<String> files) {
		this.files = files;
	}

	public void addPhoto(String photoId, String contentType) {
		images.add(photoId);
	}

	public void addFile(String fileId, String fileName, String contentType) {
		files.add(fileId);
		files.add(fileName);
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public boolean isMessageUnread() {
		return messageUnread;
	}

	public void setMessageUnread(boolean messageUnread) {
		this.messageUnread = messageUnread;
	}

	public Map<String, Boolean> getUnreadGroupMessages() {
		return unreadGroupMessages;
	}

	public void setUnreadGroupMessages(Map<String, Boolean> unreadGroupMessages) {
		this.unreadGroupMessages = unreadGroupMessages;
	}

	public void setUnreadGroupMessage(String userId, boolean status) {
		this.unreadGroupMessages.replace(userId, status);
	}

	@Override
	public String toString() {
		return "ChatMessage [key=" + key + ", value=" + value + ", timestamp=" + timestamp + ",images=" + images
				+ ", files=" + files + ", userId=" + userId + ", messageUnread=" + messageUnread + "]";
	}
}