package eu.stimey.platform.microservice.chat.databind;

import java.util.List;

import eu.stimey.platform.library.data.access.databind.Messages;

public class ChatObject extends Messages {
	protected String groupAdminID;
	protected String creatorType;
	protected List<String> members;
	protected List<String> membersName;
	protected List<UserInfo> UserInfo;

	protected boolean groupValue;

	protected boolean messageRequest; // is this a chat messageRequest?
	protected boolean messageRequestAccepted; // chat messageRequest messageRequestAccepted?

	protected boolean contactRequest;
	protected boolean contactRequestAccepted;
	protected boolean contactRequestDeclined;

	protected String contactRequestTitle;

	protected String groupImageId;
	protected String msgTopic;

	// for deserialisation
	public ChatObject() {
		super(null, null);
	}

	private ChatObject(String groupAdminID, String creator, String creatorType, String topic, List<String> members,
			List<String> membersName, List<UserInfo> UserInfo, boolean groupValue, boolean messageRequestAccepted,
			boolean messageRequest, boolean contactRequest, boolean contactRequestAccepted,
			String contactRequestTitle) {
		super(creator, topic);
		this.msgTopic = topic;
		this.groupAdminID = groupAdminID;
		this.creatorType = creatorType;
		this.members = members;
		this.membersName = membersName;
		this.UserInfo = UserInfo;
		this.groupValue = groupValue;

		this.messageRequestAccepted = messageRequestAccepted;
		this.messageRequest = messageRequest;

		this.contactRequest = contactRequest;
		this.contactRequestAccepted = contactRequestAccepted;
		this.contactRequestDeclined = false;
		this.contactRequestTitle = contactRequestTitle;
		this.groupImageId = "";
	}

	/*
	 * Create a chat instance with a friend
	 */
	public static ChatObject createUserChatInstance(String userId, String username, String topic,
			List<String> memberIds, List<String> memberNames, List<UserInfo> userInfos) {
		return new ChatObject(userId, userId, username, topic, memberIds, memberNames, userInfos, false, false, false,
				false, false, "");
	}

	/*
	 * Create a new group chat with friends
	 */
	public static ChatObject createUserGroupInstance(String groupAdminID, String groupname, String userId,
			String username, String topic, List<String> memberIds, List<String> memberNames, List<UserInfo> userInfos) {
		return new ChatObject(groupAdminID, groupname, username, topic, memberIds, memberNames, userInfos, true, false,
				false, false, false, "");
	}

	/*
	 * Create new message request to users (user follows user, but isn't a friend)
	 */
	public static ChatObject createMessageRequestInstance(String userId, String username, String topic,
			List<String> memberIds, List<String> memberNames, List<UserInfo> userInfos, boolean accepted) {
		return new ChatObject(userId, userId, username, topic, memberIds, memberNames, userInfos, false, accepted, true,
				false, false, "");
	}

	/*
	 * Create new contact request (users can contact organizations)
	 */
	public static ChatObject createContactRequestInstance(String userId, String username, String topic,
			List<String> memberIds, List<String> memberNames, List<UserInfo> userInfos, boolean accepted,
			String title) {
		return new ChatObject(userId, userId, username, topic, memberIds, memberNames, userInfos, false, false, false,
				true, accepted, title);
	}

	public String getGroupAdminID() {
		return groupAdminID;
	}

	public void setGroupAdminID(String groupAdminID) {
		this.groupAdminID = groupAdminID;
	}

	public String getCreatorType() {
		return creatorType;
	}

	public void setCreatorType(String creatorType) {
		this.creatorType = creatorType;
	}

	public List<String> getMembers() {
		return members;
	}

	public void setMembers(List<String> members) {
		this.members = members;
	}

	public List<String> getMembersName() {
		return membersName;
	}

	public void setMembersName(List<String> membersName) {
		this.membersName = membersName;
	}

	public List<UserInfo> getUserInfo() {
		return UserInfo;
	}

	public void setUserInfo(List<UserInfo> userInfo) {
		UserInfo = userInfo;
	}

	public boolean isGroupValue() {
		return groupValue;
	}

	public void setGroupValue(boolean groupValue) {
		this.groupValue = groupValue;
	}

	public boolean isMessageRequestAccepted() {
		return messageRequestAccepted;
	}

	public void setMessageRequestAccepted(boolean messageRequestAccepted) {
		this.messageRequestAccepted = messageRequestAccepted;
	}

	public boolean isMessageRequest() {
		return messageRequest;
	}

	public void setMessageRequest(boolean messageRequest) {
		this.messageRequest = messageRequest;
	}

	public String getMsgTopic() {
		return msgTopic;
	}

	public void setMsgTopic(String msgTopic) {
		this.msgTopic = msgTopic;
	}

	public boolean isContactRequest() {
		return contactRequest;
	}

	public void setContactRequest(boolean contactRequest) {
		this.contactRequest = contactRequest;
	}

	public boolean isContactRequestAccepted() {
		return contactRequestAccepted;
	}

	public void setContactRequestAccepted(boolean contactRequestAccepted) {
		this.contactRequestAccepted = contactRequestAccepted;
	}

	public boolean isContactRequestDeclined() {
		return contactRequestDeclined;
	}

	public void setContactRequestDeclined(boolean contactRequestDeclined) {
		this.contactRequestDeclined = contactRequestDeclined;
	}

	public String getContactRequestTitle() {
		return contactRequestTitle;
	}

	public void setContactRequestTitle(String contactRequestTitle) {
		this.contactRequestTitle = contactRequestTitle;
	}

	public String getGroupImageId() {
		return groupImageId;
	}

	public void setGroupImageId(String groupImageId) {
		this.groupImageId = groupImageId;
	}

	@Override
	public String toString() {
		return "ChatObject [groupAdminID=" + groupAdminID + ", creatorType=" + creatorType + ", members=" + members
				+ ", membersName=" + membersName + ", UserInfo=" + UserInfo + ", groupValue=" + groupValue
				+ ", creator=" + creator + ", topic=" + topic + ", messages=" + messages + ", lastAccess=" + lastAccess
				+ "]";
	}
}
