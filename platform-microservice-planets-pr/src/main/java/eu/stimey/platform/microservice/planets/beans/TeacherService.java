package eu.stimey.platform.microservice.planets.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.planets.beans.Global.GlobalVariables;
import eu.stimey.platform.microservice.planets.databind.Teacher;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

public class TeacherService {
	private Caching<String, Teacher> cache;
	private Teacher teacher;

	public TeacherService() {
		this.cache = new Caching<>("ms_planets_teacher", "_id", Teacher.class, "teacher", GlobalVariables.CACHE_CONFIG);
		this.cache.clear();
		this.cache.setWorkaround_keyMapper((key) -> new ObjectId(key));
	}

	public TeacherService(String id) {
		this();

		this.teacher = cache.get((id));
	}

	public Teacher getTeacher(String userId) {
		String cacheUserId = findCacheIdByUserId(userId);
		return cache.get(cacheUserId);
	}

	/**
	 * addWorld() Add a new World to specific userId
	 *
	 * @param userId
	 * @param worldId
	 */
	public void addSavedWorld(String userId, String worldId) {
		String cacheUserId = findCacheIdByUserId(userId);

		if (cacheUserId.equals("")) {
			this.addTeacher(userId);
			cacheUserId = this.teacher.get_id().toHexString();
		} else {
			this.teacher = cache.get(cacheUserId);
		}

		this.teacher.addSavedWorld(worldId);
		this.cache.set(cacheUserId, this.teacher);
	}

	public void moveArchivedToDeletedWorld(String userId, String worldId) {
		String cacheUserId = findCacheIdByUserId(userId);

		this.teacher = cache.get(cacheUserId);
		this.teacher.removeOneArchivedWorld(worldId);
		this.teacher.addDeletedWorld(worldId);
		this.cache.set(cacheUserId, this.teacher);
	}

	public void moveToArchivedWorld(String userId, String worldId, String oldState) {
		String cacheUserId = findCacheIdByUserId(userId);
		this.teacher = cache.get(cacheUserId);

		if (oldState.equals("archived"))
			this.teacher.removeOneSavedWorld(worldId);
		else if (oldState.equals("active"))
			this.teacher.removeOneActiveWorld(worldId);

		this.teacher.addArchivedWorld(worldId);
		this.cache.set(cacheUserId, this.teacher);
		this.cache.flush();
	}

	public void moveArchivedToSavedWorld(String userId, String worldId) {
		String cacheUserId = findCacheIdByUserId(userId);
		this.teacher = cache.get(cacheUserId);
		this.teacher.removeOneArchivedWorld(worldId);
		this.teacher.addSavedWorld(worldId);
		this.cache.set(cacheUserId, this.teacher);
		this.cache.flush();
	}

	public void moveSavedToActiveWorld(String userId, String worldId) {
		String cacheUserId = findCacheIdByUserId(userId);
		this.teacher = cache.get(cacheUserId);

		if (!this.teacher.removeOneSavedWorld(worldId))
			return;

		this.teacher.addActiveWorld(worldId);
		this.cache.set(cacheUserId, this.teacher);
		this.cache.flush();
	}

	public void addModeratedWorld(String userId, String worldId) {
		String cacheUserId = findCacheIdByUserId(userId);
		this.teacher = cache.get(cacheUserId);
		this.teacher.addModeratedWorld(worldId);
		this.cache.set(cacheUserId, this.teacher);
	}

	public void addInvitationToWorld(String userId, String worldId) {
		String cacheUserId = findCacheIdByUserId(userId);

		if (cacheUserId.equals("")) {
			this.addTeacher(userId);
			cacheUserId = findCacheIdByUserId(userId);
		}

		this.teacher = cache.get(cacheUserId);
		this.teacher.addInvitationToWorld(worldId);
		this.cache.set(cacheUserId, this.teacher);
	}

	public void removeInvitationToWorld(String userId, String worldId) {
		String cacheUserId = findCacheIdByUserId(userId);
		this.teacher = cache.get(cacheUserId);
		this.teacher.removeInvitationToWorld(worldId);
		this.cache.set(cacheUserId, this.teacher);
	}

	/**
	 * findCacheIdByUserId()
	 *
	 * @param userId Searched userId
	 * @return CacheId for specific UserId as String
	 */
	private String findCacheIdByUserId(String userId) {

		List<String> ids = this.cache.find(new Document("userId", userId), null, 0, 1,
				(result) -> result.get_id().toHexString(), true);

		if (ids.size() == 0)
			return "";
		else
			return ids.get(0);
	}

	/**
	 * addTeacher() Creates the Database-Entry for the relation between Teachers &
	 * Worlds
	 *
	 * @param userId Teacher-Id
	 */
	public Teacher addTeacher(String userId) {
		this.teacher = new Teacher();
		this.teacher.setUserId(userId);
		this.cache.set(this.teacher.get_id().toHexString(), this.teacher);

		return this.teacher;
	}

	public void setHelp(String userId, boolean newState) {
		String cacheUserId = findCacheIdByUserId(userId);
		this.teacher = cache.get(cacheUserId);
		this.teacher.setShowHelp(newState);
		this.cache.set(cacheUserId, this.teacher);
	}

	public Document getAllWorldsForSingleTeacher(String userId) {
		Document serviceData = new Document();

		String cacheId = findCacheIdByUserId(userId);
		Teacher t = new Teacher();

		try {
			t = this.cache.get(cacheId);
		} catch (Exception e) {
			this.addTeacher(userId);
		}

		List<String> moderatedWorlds = new ArrayList<>(t.moderatedWorlds);
		List<String> activeWorlds = t.activeWorlds;
		List<String> savedWorlds = t.savedWorlds;
		moderatedWorlds.removeAll(activeWorlds);
		moderatedWorlds.removeAll(savedWorlds);

		WorldService ws = new WorldService();
		serviceData.append("archived", ws.getWorldsFromIdList(t.archivedWorlds));
		serviceData.append("active", ws.getWorldsFromIdList(t.activeWorlds));
		serviceData.append("saved", ws.getWorldsFromIdList(t.savedWorlds));
		serviceData.append("invited", ws.getWorldsFromIdList(t.invitationToModerateWorld));
		serviceData.append("moderated", ws.getWorldsFromIdList(moderatedWorlds));
		serviceData.append("showHelp", t.showHelp);

		return serviceData;
	}
}
