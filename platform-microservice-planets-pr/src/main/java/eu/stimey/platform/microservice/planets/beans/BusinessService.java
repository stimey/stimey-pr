package eu.stimey.platform.microservice.planets.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.planets.beans.Global.GlobalVariables;
import eu.stimey.platform.microservice.planets.databind.Incubator;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

public class BusinessService {
	private Caching<String, Incubator> cache;

	public BusinessService() {
		super();

		this.cache = new Caching<>("ms_planets_business", "_id", Incubator.class, "business",
				GlobalVariables.CACHE_CONFIG);

		this.cache.clear();
		this.cache.setWorkaround_keyMapper((key) -> new ObjectId(key));
	}

	public Incubator get(String key) {
		return cache.get(key);
	}

	public List<Incubator> getAll() {
		List<String> ids = this.cache.find(null, null, 0, 0, (result) -> result.get_id().toHexString(), true);

		List<Incubator> incubatorList = new ArrayList<>();

		for (String id : ids) {
			incubatorList.add(this.cache.get(id));
		}

		return incubatorList;
	}

	private String findCacheIdByUserId(String userId) {

		List<String> ids = this.cache.find(new Document("userId", userId), null, 0, 1,
				(result) -> result.get_id().toHexString(), true);

		if (ids.size() == 0)
			return "";
		else
			return ids.get(0);
	}

	public Incubator addIncubator(String userId) {
		Incubator businessUser;

		businessUser = new Incubator();
		businessUser.setUserId(userId);
		cache.set(businessUser.get_id().toHexString(), businessUser);

		return businessUser;
	}

	public Document getAllWorlds(String userId) {

		Document serviceData = new Document();

		String cacheId = findCacheIdByUserId(userId);
		Incubator t = new Incubator();

		try {
			t = this.cache.get(cacheId);
		} catch (Exception e) {
			this.addIncubator(userId);
		}

		List<String> moderatedWorlds = new ArrayList<>(t.moderatedWorlds);
		List<String> activeWorlds = t.activeWorlds;
		List<String> savedWorlds = t.savedWorlds;
		moderatedWorlds.removeAll(activeWorlds);
		moderatedWorlds.removeAll(savedWorlds);

		WorldService ws = new WorldService();
		serviceData.append("archived", ws.getWorldsFromIdList(t.archivedWorlds));
		serviceData.append("active", ws.getWorldsFromIdList(t.activeWorlds));
		serviceData.append("saved", ws.getWorldsFromIdList(t.savedWorlds));
		serviceData.append("invited", ws.getWorldsFromIdList(t.invitationToModerateWorld));
		serviceData.append("moderated", ws.getWorldsFromIdList(moderatedWorlds));
		serviceData.append("showHelp", t.showHelp);

		return serviceData;

	}
}