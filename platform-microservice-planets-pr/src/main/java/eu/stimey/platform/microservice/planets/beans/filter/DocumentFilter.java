package eu.stimey.platform.microservice.planets.beans.filter;

import org.bson.Document;

public interface DocumentFilter {
	Document toDocument();
}
