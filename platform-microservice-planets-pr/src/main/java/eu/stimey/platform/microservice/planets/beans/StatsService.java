package eu.stimey.platform.microservice.planets.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.planets.beans.Global.GlobalVariables;
import eu.stimey.platform.microservice.planets.databind.Stats;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

public class StatsService {
	private Caching<String, Stats> cache;

	public StatsService() {
		super();

		this.cache = new Caching<>("ms_planets_stats", "_id", Stats.class, "stats", GlobalVariables.CACHE_CONFIG);

		this.cache.clear();
		this.cache.setWorkaround_keyMapper((key) -> new ObjectId(key));
	}

	public Stats get(String key) {
		return cache.get(key);
	}

	public List<Stats> getAll() {
		List<String> ids = this.cache.find(null, null, 0, 0, (result) -> result.get_id().toHexString(), true);

		List<Stats> historyList = new ArrayList<>();

		for (String id : ids) {
			historyList.add(this.cache.get(id));
		}

		return historyList;
	}
}
