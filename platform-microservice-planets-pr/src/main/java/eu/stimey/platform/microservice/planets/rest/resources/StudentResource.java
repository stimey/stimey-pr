package eu.stimey.platform.microservice.planets.rest.resources;

import eu.stimey.platform.microservice.planets.beans.StudentService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/students")
public class StudentResource {
	private StudentService studServ = new StudentService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllStudents() {
		return Response.ok().entity(studServ.getAll()).build();
	}
	
	@GET
	@Path("/{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response student(@PathParam("userid") String userid) {
		return Response.ok().entity(studServ.get(userid)).build();
	}
}