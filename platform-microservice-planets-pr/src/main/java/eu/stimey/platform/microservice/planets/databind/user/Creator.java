package eu.stimey.platform.microservice.planets.databind.user;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Creator extends Moderator {
	public List<String> savedWorlds;
	public boolean showHelp;

	public Creator() {
		super();
		this.savedWorlds = new ArrayList<>();
		this.showHelp = true;
	}

	public boolean removeOneSavedWorld(String worldId) {
		if (this.savedWorlds.contains(worldId)) {
			this.savedWorlds.remove(worldId);
			return true;
		}

		return false;
	}

	public void addSavedWorld(String worldId) {
		this.savedWorlds.add(worldId);
	}

	public List<String> getSavedWorlds() {
		return this.savedWorlds;
	}

	public void setShowHelp(boolean newState) {
		this.showHelp = newState;
	}

	@JsonIgnore
	public List<String> getAllWorlds() {
		Set<String> allWorlds = new HashSet<>();
		allWorlds.addAll(this.savedWorlds);
		allWorlds.addAll(this.activeWorlds);
		allWorlds.addAll(this.moderatedWorlds);
		allWorlds.addAll(this.archivedWorlds);
		return new ArrayList<>(allWorlds);
	}
}
