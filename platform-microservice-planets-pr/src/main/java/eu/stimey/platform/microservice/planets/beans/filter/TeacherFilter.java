package eu.stimey.platform.microservice.planets.beans.filter;

import org.bson.Document;

import java.util.LinkedList;
import java.util.List;

public class TeacherFilter implements DocumentFilter {

	public static Document userId(String userId) {
		if (userId == null || userId.contains("any"))
			return new Document();
		List<Document> result = new LinkedList<>();
		for (String part : userId.split(" ")) {
			result.add(new Document("userId", part.toUpperCase()));
		}
		if (result.size() == 0)
			return new Document();
		if (result.size() == 1)
			return result.get(0);
		return FilterBuilder.or(result.toArray(new Document[result.size()]));
	}

	@Override
	public Document toDocument() {
		return null;
	}
}
