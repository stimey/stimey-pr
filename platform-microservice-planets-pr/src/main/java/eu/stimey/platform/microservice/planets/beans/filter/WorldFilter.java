package eu.stimey.platform.microservice.planets.beans.filter;

import org.bson.Document;

import java.util.List;

public class WorldFilter {
	private Document filter;

	public WorldFilter() {
		this.filter = new Document();
	}

	public WorldFilter title(String query) {
		Document titleFilter = new Document("title", new Document("$regex", query).append("$options", "i"));

		if (this.filter.isEmpty())
			this.filter = titleFilter;
		else
			this.filter = FilterBuilder.and(this.filter, titleFilter);

		return this;
	}

	public WorldFilter statePublish() {
		Document stateFilter = new Document("state", "publish");

		if (this.filter.isEmpty())
			this.filter = stateFilter;
		else
			this.filter = FilterBuilder.and(this.filter, stateFilter);

		return this;
	}

	public WorldFilter language(String language) {
		if (language.equals("Any"))
			return this;

		Document stateFilter = new Document("language", language);

		if (this.filter.isEmpty())
			this.filter = stateFilter;
		else
			this.filter = FilterBuilder.and(this.filter, stateFilter);

		return this;
	}

	public WorldFilter difficulty(List<String> difficulties) {
		return this.filterList(difficulties, "difficulty");
	}

	public WorldFilter stages(List<String> stages) {
		return this.filterList(stages, "stage");
	}

	public WorldFilter topics(List<String> topics) {
		return this.filterList(topics, "topics");
	}

	private WorldFilter filterList(List<String> list, String keyword) {
		Document result = new Document();

		for (String str : list) {
			Document stateFilter = new Document(keyword, str);

			if (!result.isEmpty())
				result = FilterBuilder.or(result, stateFilter);
			else
				result = stateFilter;
		}

		if (this.filter.isEmpty())
			this.filter = result;
		else
			this.filter = FilterBuilder.and(this.filter, result);

		return this;
	}

	public WorldFilter privacyPublic() {
		return this.privacy("public");
	}

	public WorldFilter privacyPrivate() {
		return this.privacy("private");
	}

	private WorldFilter privacy(String option) {
		Document f = new Document("privacy", option);

		if (this.filter.isEmpty())
			this.filter = f;
		else
			this.filter = FilterBuilder.and(this.filter, f);

		return this;
	}

	public Document getFilter() {
		return this.filter;
	}
}