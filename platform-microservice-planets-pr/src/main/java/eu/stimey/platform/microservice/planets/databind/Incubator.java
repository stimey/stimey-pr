package eu.stimey.platform.microservice.planets.databind;

import eu.stimey.platform.microservice.planets.databind.user.Creator;

public class Incubator extends Creator {
	public Incubator() {
		super();
	}

	public Incubator(String userId) {
		super();

		this.userId = userId;
	}
}
