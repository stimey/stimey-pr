package eu.stimey.platform.microservice.planets.databind;

import eu.stimey.platform.microservice.planets.databind.user.User;

import java.util.ArrayList;
import java.util.List;

public class Moderator extends User {
	// Responsibilities

	public List<String> responsibilities;

	public Moderator() {
		super();
	}

	public List<World> getWorlds() {
		return new ArrayList<>();
	}

	public boolean isModerator() {
		return false;
	}

	// moderator permissions:
	// missions, resources, communities, evaluation, users)
}
