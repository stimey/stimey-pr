package eu.stimey.platform.microservice.planets.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.planets.beans.Global.GlobalVariables;
import eu.stimey.platform.microservice.planets.databind.Student;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

public class StudentService {
	private Caching<String, Student> cache;
	private Student student;

	public StudentService() {
		super();

		this.cache = new Caching<>("ms_planets_students", "_id", Student.class, "students",
				GlobalVariables.CACHE_CONFIG);

		this.cache.clear();
		this.cache.setWorkaround_keyMapper((key) -> new ObjectId(key));
	}

	public List<Student> getAll() {
		List<String> ids = this.cache.find(null, null, 0, 0, (result) -> result.get_id().toHexString(), true);

		List<Student> allStudents = new ArrayList<>();

		for (String id : ids) {
			allStudents.add(this.cache.get(id));
		}

		return allStudents;
	}
	
	public Student get(String userId) {
		String cacheUserId = findCacheIdByUserId(userId);
		
		if (cacheUserId.length() == 0) {
			return null;
		}
		
		return cache.get(cacheUserId);
	}

	/**
	 * addStudent() Creates the Database-Entry for the relation between Student &
	 * Worlds
	 *
	 * @param userId
	 */
	public Student addStudent(String userId) {
		String id = this.findCacheIdByUserId(userId);

		if (id.length() != 0) {
			return null;
		}

		this.student = new Student();
		this.student.setUserId(userId);

		this.cache.set(this.student.get_id().toHexString(), this.student);
		return this.student;
	}

	public void addActiveWorld(String userId, String worldId) {
		String cacheUserId = findCacheIdByUserId(userId);
		this.student = cache.get(cacheUserId);
		this.student.addActiveWorld(worldId);
		this.cache.set(cacheUserId, this.student);
	}

	public void moveActiveToExploredWorld(String userId, String worldId) {
		String cacheUserId = findCacheIdByUserId(userId);
		this.student = cache.get(cacheUserId);
		this.student.removeOneActiveWorld(worldId);
		this.student.addArchivedWorld(worldId);
		this.cache.set(cacheUserId, this.student);
	}

	/**
	 * findCacheIdByUserId()
	 *
	 * @param userId Searched userId
	 * @return CacheId for specific UserId as String
	 */
	private String findCacheIdByUserId(String userId) {

		List<String> ids = this.cache.find(new Document("userId", userId), null, 0, 1,
				(result) -> result.get_id().toHexString(), true);

		if (ids.size() == 0)
			return "";
		else
			return ids.get(0);
	}

	public void addInvitationToWorld(String userId, String worldId) {
		String cacheUserId = findCacheIdByUserId(userId);
		if (cacheUserId.equals("")) {
			this.addStudent(userId);
			cacheUserId = findCacheIdByUserId(userId);
		}

		this.student = cache.get(cacheUserId);
		this.student.addInvitationToWorld(worldId);
		this.cache.set(cacheUserId, this.student);
	}

	public void removeInvitationToWorld(String userId, String worldId) {
		String cacheUserId = findCacheIdByUserId(userId);
		this.student = cache.get(cacheUserId);
		this.student.removeInvitationToWorld(worldId);
		this.cache.set(cacheUserId, this.student);
	}

	public void quitWorld(String userId, String worldId) {
		String cacheUserId = findCacheIdByUserId(userId);
		this.student = cache.get(cacheUserId);
		this.student.removeOneActiveWorld(worldId);
		this.cache.set(cacheUserId, this.student);
	}

	public Document getAllWorlds(String userId) {
		Document serviceData = new Document();

		String cacheId = findCacheIdByUserId(userId);
		Student s = new Student();

		try {
			s = this.cache.get(cacheId);
		} catch (Exception e) {
			this.addStudent(userId);
		}
		WorldService ws = new WorldService();
		serviceData.append("archived", ws.getWorldsFromIdList(s.archivedWorlds));
		serviceData.append("active", ws.getWorldsFromIdList(s.activeWorlds));
		serviceData.append("invited", ws.getWorldsFromIdList(s.invitationToBeMember));

		return serviceData;
	}

}
