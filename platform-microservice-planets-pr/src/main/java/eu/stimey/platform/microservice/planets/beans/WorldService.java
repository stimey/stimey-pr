package eu.stimey.platform.microservice.planets.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.library.data.access.databind.Message;
import eu.stimey.platform.library.utils.StimeyLogger;
import eu.stimey.platform.microservice.planets.beans.Global.GlobalVariables;
import eu.stimey.platform.microservice.planets.beans.filter.WorldFilter;
import eu.stimey.platform.microservice.planets.databind.World;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

public class WorldService {
	private Caching<String, World> cache;

	public WorldService() {
		this.cache = new Caching<>("ms_planets_worlds", "_id", World.class, "worlds", GlobalVariables.CACHE_CONFIG);

		this.cache.clear();
		this.cache.setWorkaround_keyMapper((key) -> new ObjectId(key));
	}

	public World get(String key) {
		try {
			World searchedWorld = cache.get(key);
			return searchedWorld;
		} catch (Exception e) {
			return null;
		}
	}

	public void getAndSet(String key, Function<World, World> function) {
		cache.getAndSet(key, function);
	}

	public List<HashMap<String, Object>> search(WorldFilter filter, int limit, int offset) {
		List<String> ids = this.getWorldIDsFromFilter(filter.getFilter(), null, offset, limit);

		return this.getWorldsFromIdList(ids);
	}

	private List<String> getWorldIDsFromFilter(Document filter, Document sort, int skip, int limit) {
		return this.cache.find(filter, sort, skip, limit, (result) -> result.get_id().toHexString(), true);
	}

	public boolean isTitleAvailable(String title) {
		List<String> titles = this.cache.find(null, null, 0, 0, (result) -> result.getTitle(), true);

		for (String t : titles) {
			if (t.equals(title))
				return false;
		}

		return true;
	}

	public String setWorld(World world) {
		this.cache.set(world.get_id().toHexString(), world);
		return world.get_id().toHexString();
	}

	public void addKeywords(ArrayList<String> keywords) {
		StimeyLogger.logger().info("WorldService: sending keywords to api-gateway-storage:" + keywords);
		this.cache.publish("addKeywords", new Message(null, keywords));
	}

	/**
	 * Collect needed Attributes for Overview
	 */
	public List<HashMap<String, Object>> getWorldsFromIdList(List<String> worldIds) {
		List<HashMap<String, Object>> worldList = new ArrayList<>();
		HashMap<String, Object> world;

		for (String worldId : worldIds) {
			world = this.getWorld(worldId);

			if (!world.get("state").toString().equals("DELETED"))
				worldList.add(this.getWorld(worldId));
		}

		return worldList;
	}

	public List<HashMap<String, Object>> getWorldsDTO(List<World> worlds) {
		List<HashMap<String, Object>> worldList = new ArrayList<>();

		for (World world : worlds) {
			HashMap<String, Object> worldDTO = this.getWorld(world);

			if (!worldDTO.get("state").toString().equals("DELETED"))
				worldList.add(this.getWorld(world));
		}

		return worldList;
	}

	public HashMap<String, Object> getWorld(World world) {
		HashMap<String, Object> worldData = new HashMap<>();

		int memberCount = world.getMembers().size() + world.getModerators().size();

		worldData.put("id", world.get_id().toHexString());
		worldData.put("name", world.getTitle());
		worldData.put("creationDate", String.valueOf(world.getCreationDate()));
		worldData.put("theme", world.getTheme());
		worldData.put("character", world.getCharacter());
		worldData.put("missions", String.valueOf(world.getMissions().size()));
		worldData.put("missionIDs", world.getMissions());
		worldData.put("challenges", String.valueOf(world.getChallenges().size()));
		worldData.put("members", String.valueOf(memberCount));
		worldData.put("communities", String.valueOf(world.getCommunities().size()));
		worldData.put("resources", String.valueOf(world.getLabResources().size()));
		worldData.put("brainPoints", String.valueOf(world.getBrainPoints()));
		worldData.put("heartPoints", String.valueOf(world.getHeartPoints()));
		worldData.put("spiritPoints", String.valueOf(world.getSpiritPoints()));
		worldData.put("owner", String.valueOf(world.getOwner()));
		worldData.put("keywords", world.getKeywords());
		worldData.put("difficulty", world.getDifficulty());
		worldData.put("state", world.getState());

		return worldData;
	}

	public HashMap<String, Object> getWorld(String worldId) {
		World world = this.cache.get(worldId);
		return this.getWorld(world);
	}

	public List<World> getWorlds(List<String> worldIds) {
		List<World> worlds = new ArrayList<>();
		for (String worldId : worldIds) {
			worlds.add(this.get(worldId));
		}
		return worlds;
	}

	public Caching<String, World> getCache() {
		return cache;
	}
}