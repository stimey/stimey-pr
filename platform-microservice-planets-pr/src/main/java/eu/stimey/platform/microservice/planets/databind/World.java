package eu.stimey.platform.microservice.planets.databind;

import eu.stimey.platform.microservice.planets.databind.serializer.DocumentWithId2;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

public class World extends DocumentWithId2 {
	private String title, description, creationDate, theme, difficulty, stage, character, owner, privacy, language;
	private int brainPoints, heartPoints, spiritPoints;
	private List<String> members, communities, memberInvitations, moderatorInvitations, moderators, topics;
	private List<String> missions, challenges, keywords, labResources;
	private String state;

	public World() {
		this.brainPoints = 0;
		this.spiritPoints = 0;
		this.heartPoints = 0;
		this.members = new ArrayList<>();
		this.communities = new ArrayList<>();
		this.labResources = new ArrayList<>();
		this.memberInvitations = new ArrayList<>();
		this.moderatorInvitations = new ArrayList<>();
		this.moderators = new ArrayList<>();
		this.missions = new ArrayList<>();
		this.challenges = new ArrayList<>();
		this.keywords = new ArrayList<>();
		this.topics = new ArrayList<>();
		this.privacy = "private";
		this.state = "save"; // publish, archive, DELETED
	}

	/**
	 * Only getter & setter for data
	 */
	public void setTitle(String name) {
		this.title = name;
	}

	public String getTitle() {
		return this.title;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public void setActualDate() {
		this.creationDate = Long.toString(new Date().getTime());
	}

	public String getCreationDate() {
		return this.creationDate;
	}

	public void addModerator(String userId) {
		this.moderators.add(userId);
	}

	public void removeModerationInvitation(String userId) {
		this.moderatorInvitations.remove(userId);
	}

	public void removeMemberInvitation(String userId) {
		this.memberInvitations.remove(userId);
	}

	public void addMember(String userId) {
		this.members.add(userId);
	}

	public List<String> getModerators() {
		return this.moderators;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return this.description;
	}

	public List<String> getLabResources() {
		return this.labResources;
	}

	public void setLabResource(ArrayList<String> resource) {
		this.labResources = resource;
	}

	public List<String> getCommunities() {
		return this.communities;
	}

	public void addCommunity(String community) {
		this.communities.add(community);
	}

	public void setCommunities(ArrayList<String> communities) {
		this.communities = communities;
	}

	public List<String> getMissions() {
		return this.missions;
	}

	public void addMission(String mission) {
		this.missions.add(mission);
	}

	public void setMissions(ArrayList<String> mission) {
		this.missions = mission;
	}

	public List<String> getChallenges() {
		return this.challenges;
	}

	public void addChallenge(String challenge) {
		this.challenges.add(challenge);
	}

	public String getPrivacy() {
		return this.privacy;
	}

	public void setPrivacy(String privacy) {
		this.privacy = privacy;
	}

	public String getCharacter() {
		return character;
	}

	public void setCharacter(String inhabitantsImg) {
		this.character = inhabitantsImg;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	public int getBrainPoints() {
		return brainPoints;
	}

	public void setBrainPoints(int brainPoints) {
		this.brainPoints = brainPoints;
	}

	public int getHeartPoints() {
		return heartPoints;
	}

	public void setHeartPoints(int heartPoints) {
		this.heartPoints = heartPoints;
	}

	public int getSpiritPoints() {
		return spiritPoints;
	}

	public void setSpiritPoints(int spiritPoints) {
		this.spiritPoints = spiritPoints;
	}

	public List<String> getMembers() {
		return members;
	}

	public void removeMember(String userId) {
		this.members.remove(userId);
	}

	public void setMembers(List<String> members) {
		this.members = members;
	}

	public void setModerators(List<String> moderators) {
		this.moderators = moderators;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public List<String> getMemberInvitations() {
		return memberInvitations;
	}

	public void setMemberInvitations(List<String> memberInvitations) {
		this.memberInvitations = memberInvitations;
	}

	public List<String> getModeratorInvitations() {
		return moderatorInvitations;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public List<String> getTopics() {
		return topics;
	}

	public void setTopics(List<String> topics) {
		this.topics = topics;
	}

	public void setModeratorInvitations(List<String> moderatorInvitations) {
		this.moderatorInvitations = moderatorInvitations;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

}
