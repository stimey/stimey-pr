package eu.stimey.platform.microservice.planets.databind;

import eu.stimey.platform.microservice.planets.databind.user.Creator;

public class Teacher extends Creator {
	public Teacher() {
		super();
	}

	public Teacher(String userId) {
		super();

		this.userId = userId;
	}
}