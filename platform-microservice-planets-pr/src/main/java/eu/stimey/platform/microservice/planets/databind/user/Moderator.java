package eu.stimey.platform.microservice.planets.databind.user;

import java.util.ArrayList;
import java.util.List;

public class Moderator extends User {
	public List<String> moderatedWorlds;
	public List<String> invitationToModerateWorld;

	public Moderator() {
		super();
		this.moderatedWorlds = new ArrayList<>();
		this.invitationToModerateWorld = new ArrayList<>();
	}

	public void addModeratedWorld(String worldId) {
		this.moderatedWorlds.add(worldId);
	}

	public List<String> getModeratedWorlds() {
		return this.moderatedWorlds;
	}

	public void addInvitationToWorld(String worldId) {
		this.invitationToModerateWorld.add(worldId);
	}

	public void removeInvitationToWorld(String worldId) {
		this.invitationToModerateWorld.remove(worldId);
	}
}