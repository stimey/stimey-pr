package eu.stimey.platform.microservice.planets.rest.resources;

import eu.stimey.platform.microservice.planets.beans.StatsService;
import eu.stimey.platform.microservice.planets.beans.StudentService;
import org.bson.Document;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("/stats")
public class StatsResource {
	private StatsService service;

	public StatsResource() {
		this.service = new StatsService();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/worlds/{userId}")
	public Response updateWorld(@Context ContainerRequestContext context, @PathParam("userId") String userId) {
		if (!context.getProperty("usertype").equals("student"))
			return Response.serverError().build();

		StudentService ss = new StudentService();
		Document stats = new Document();

		stats.append("userId", userId);
		Document allWorlds = ss.getAllWorlds(userId);
		stats.append("activeWorlds", ((ArrayList<String>) allWorlds.get("active")).size());
		stats.append("archivedWorlds", ((ArrayList<String>) allWorlds.get("archived")).size());

		try {
			return Response.ok().entity(stats).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllStudents(@Context ContainerRequestContext context) {

		return Response.ok().entity(service.getAll()).build();
	}
}
