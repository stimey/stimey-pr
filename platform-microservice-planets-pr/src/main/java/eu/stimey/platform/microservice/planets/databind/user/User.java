package eu.stimey.platform.microservice.planets.databind.user;

import eu.stimey.platform.microservice.planets.databind.serializer.DocumentWithId2;

import java.util.ArrayList;
import java.util.List;

public class User extends DocumentWithId2 {
	public String userId;
	public List<String> activeWorlds;
	public List<String> archivedWorlds;
	public List<String> deletedWorlds;
	public List<String> invitationToBeMember;

	public User() {
		this.archivedWorlds = new ArrayList<>();
		this.activeWorlds = new ArrayList<>();
		this.invitationToBeMember = new ArrayList<>();
		this.deletedWorlds = new ArrayList<>();
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserId() {
		return this.userId;
	}

	public void addActiveWorld(String worldId) {
		this.activeWorlds.add(worldId);
	}

	public void removeOneActiveWorld(String worldId) {
		this.activeWorlds.remove(worldId);
	}

	public List<String> getActiveWorlds() {
		return this.activeWorlds;
	}

	public void addArchivedWorld(String worldId) {
		this.archivedWorlds.add(worldId);
	}

	public void removeOneArchivedWorld(String worldId) {
		this.archivedWorlds.remove(worldId);
	}

	public void addDeletedWorld(String worldId) {
		try {
			this.deletedWorlds.add(worldId);
		} catch (NullPointerException e) {
			// Fix for old database-struct
			this.deletedWorlds = new ArrayList<>();
			this.deletedWorlds.add(worldId);
		}
	}

	public List<String> getArchivedWorlds() {
		return this.archivedWorlds;
	}

	public void addInvitationToWorld(String worldId) {
		this.invitationToBeMember.add(worldId);
	}

	public void removeInvitationToWorld(String worldId) {
		this.invitationToBeMember.remove(worldId);
	}

}