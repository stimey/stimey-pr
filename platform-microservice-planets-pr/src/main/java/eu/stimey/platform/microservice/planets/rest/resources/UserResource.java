package eu.stimey.platform.microservice.planets.rest.resources;

import eu.stimey.platform.microservice.planets.beans.BusinessService;
import eu.stimey.platform.microservice.planets.beans.StudentService;
import eu.stimey.platform.microservice.planets.beans.TeacherService;
import eu.stimey.platform.microservice.planets.beans.WorldService;
import eu.stimey.platform.microservice.planets.databind.Incubator;
import eu.stimey.platform.microservice.planets.databind.World;
import eu.stimey.platform.microservice.planets.databind.user.Creator;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Path("/user")
public class UserResource {

	@Inject
	WorldService worldService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAllWorlds")
	public Response user(@Context ContainerRequestContext context) {
		String userType = (String) context.getProperty("usertype");
		String userId = (String) context.getProperty("userid");

		switch (userType) {
		case "incubator":
			BusinessService bs = new BusinessService();
			return Response.ok().entity(bs.getAllWorlds(userId)).build();
		case "teacher":
			TeacherService ts = new TeacherService(userId);
			return Response.ok().entity(ts.getAllWorldsForSingleTeacher(userId)).build();
		case "student":
			StudentService ss = new StudentService();
			return Response.ok().entity(ss.getAllWorlds(userId)).build();
		default:
			return Response.serverError().entity("No " + userType + " allowed").build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/recentWorlds")
	public Response getRecentWorlds(@Context ContainerRequestContext context) {
		String userType = (String) context.getProperty("usertype");
		String userId = (String) context.getProperty("userid");

		Creator creator = null;

		switch (userType) {
		case "incubator":
			BusinessService businessService = new BusinessService();
			creator = businessService.get(userId);
			break;
		case "teacher":
			TeacherService teacherService = new TeacherService(userId);
			creator = teacherService.getTeacher(userId);
			break;
		}

		if (creator == null) {
			return Response.status(Response.Status.NO_CONTENT).build();
		}

		List<String> allWorlds = creator.getAllWorlds();
		List<World> worlds = this.worldService.getWorlds(allWorlds);

		Comparator<World> worldComparatorByCreationDate = (w1,
				w2) -> (int) (Long.parseLong(w1.getCreationDate()) - Long.parseLong(w2.getCreationDate()));
		Collections.sort(worlds, worldComparatorByCreationDate);

		List<World> recent;
		if (worlds.size() < 5) {
			recent = worlds.subList(0, worlds.size());
		} else {
			recent = worlds.subList(0, 5);
		}

		return Response.status(Response.Status.OK).entity(this.worldService.getWorldsDTO(recent)).build();
		// return Response.status(Response.Status.OK).entity(allWorlds).build();
	}
}