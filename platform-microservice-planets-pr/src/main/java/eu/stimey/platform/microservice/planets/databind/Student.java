package eu.stimey.platform.microservice.planets.databind;

import eu.stimey.platform.microservice.planets.databind.user.User;

import java.util.ArrayList;
import java.util.List;

public class Student extends User {
	private List<String> enrolledWorlds;

	public Student() {
		super();

		this.enrolledWorlds = new ArrayList<>();
	}

	public Student(String userId) {
		super();

		this.userId = userId;
	}

	public void enrollingWorld(String worldId) {
		this.enrolledWorlds.add(worldId);
	}

	public List<String> getEnrolledWorlds() {
		return this.enrolledWorlds;
	}
}