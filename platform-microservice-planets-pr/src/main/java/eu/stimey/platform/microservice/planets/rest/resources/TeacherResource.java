package eu.stimey.platform.microservice.planets.rest.resources;

import eu.stimey.platform.microservice.planets.beans.TeacherService;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/teacher")
public class TeacherResource {
	private TeacherService service = new TeacherService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllStudents(@Context ContainerRequestContext context) {
		String userType = (String) context.getProperty("usertype");
		String userId = (String) context.getProperty("userid");

		if (userType.equals("teacher")) {
			return Response.ok().entity(service.getAllWorldsForSingleTeacher(userId)).build();
		} else {
			return Response.serverError().entity("No " + userType + " allowed").build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/showHelp/{state}")
	public void showHelp(@Context ContainerRequestContext context,
			@PathParam("state") @DefaultValue("true") boolean state) {

		if (!context.getProperty("usertype").equals("teacher"))
			return;

		String userId = (String) context.getProperty("userid");

		try {
			service.setHelp(userId, state);
		} catch (ClassCastException e) {
			e.printStackTrace();
		}
	}
}