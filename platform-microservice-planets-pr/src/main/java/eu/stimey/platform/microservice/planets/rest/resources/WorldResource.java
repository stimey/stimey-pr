package eu.stimey.platform.microservice.planets.rest.resources;

import eu.stimey.platform.microservice.planets.beans.StudentService;
import eu.stimey.platform.microservice.planets.beans.TeacherService;
import eu.stimey.platform.microservice.planets.beans.WorldService;
import eu.stimey.platform.microservice.planets.beans.filter.WorldFilter;
import eu.stimey.platform.microservice.planets.databind.World;
import org.bson.Document;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

@Path("/worlds")
public class WorldResource {
	@Inject
	private WorldService worldService;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/create")
	public Response createWorld(@Context ContainerRequestContext context, Document json) {
		String title = json.get("title").toString();
		String userName = context.getProperty("username").toString();
		String userId = context.getProperty("userid").toString();

		if (!context.getProperty("usertype").equals("teacher"))
			return Response.serverError().build();

		World newWorld = new World();
		newWorld.setTitle(title);
		newWorld.setDescription(json.get("description").toString());
		newWorld.setTheme(json.get("theme").toString());
		newWorld.setCharacter(json.get("character").toString());
		newWorld.setTopics(json.get("topics", arrayListType()));
		newWorld.setKeywords(json.get("keywords", arrayListType()));
		newWorld.setPrivacy(json.get("privacy").toString());
		newWorld.setLanguage(json.get("language").toString());
		newWorld.setStage(json.get("stage").toString());
		newWorld.setDifficulty(json.get("difficulty").toString());
		newWorld.addModerator(userId);
		newWorld.setOwner(userName);
		newWorld.setActualDate();
		worldService.setWorld(newWorld);

		TeacherService ts = new TeacherService();
		ts.addSavedWorld(userId, newWorld.get_id().toHexString());
		ts.addModeratedWorld(userId, newWorld.get_id().toHexString());

		return Response.ok().entity(newWorld.get_id().toHexString()).build();
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/get/{worldId}")
	public Response get(@Context ContainerRequestContext context, @PathParam("worldId") String worldId) {
		World world = new WorldService().get(worldId);

		Document worldData = new Document();
		worldData.append("id", worldId);
		worldData.append("name", world.getTitle());
		worldData.append("creationDate", world.getCreationDate());
		worldData.append("theme", world.getTheme());
		worldData.append("character", world.getCharacter());
		worldData.append("stage", world.getStage());
		worldData.append("difficulty", world.getDifficulty());
		worldData.append("language", world.getLanguage());
		worldData.append("topics", world.getTopics());
		worldData.append("missions", world.getMissions());
		worldData.append("challenges", world.getChallenges());
		worldData.append("communities", world.getCommunities());
		worldData.append("resources", world.getLabResources());
		worldData.append("brainPoints", world.getBrainPoints());
		worldData.append("heartPoints", world.getHeartPoints());
		worldData.append("spiritPoints", world.getSpiritPoints());
		worldData.append("owner", world.getOwner());
		worldData.append("description", world.getDescription());
		worldData.append("keywords", world.getKeywords());
		worldData.append("privacy", world.getPrivacy());
		worldData.append("moderators", world.getModerators());
		worldData.append("members", world.getMembers());
		worldData.append("memberInvitations", world.getMemberInvitations());
		worldData.append("moderatorInvitations", world.getModeratorInvitations());
		worldData.append("state", world.getState());

		return Response.ok().entity(worldData).build();
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/get/{worldId}/{area}")
	public Response getWorldByArea(@Context ContainerRequestContext context, @PathParam("worldId") String worldId,
			@PathParam("area") String area) throws IOException {
		Response response = this.get(context, worldId);
		Document doc = (Document) response.getEntity();
		doc.append(area, true);

		return Response.ok().entity(doc).build();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/archive/{worldId}")
	public Response archiveWorld(@Context ContainerRequestContext context, @PathParam("worldId") String worldId) {
		String userId = context.getProperty("userid").toString();

		if (!context.getProperty("usertype").equals("teacher"))
			return Response.serverError().build();

		try {
			TeacherService ts = new TeacherService();

			if (ts.getTeacher(userId).getActiveWorlds().contains(worldId))
				ts.moveToArchivedWorld(userId, worldId, "active");
			else if (ts.getTeacher(userId).getSavedWorlds().contains(worldId))
				ts.moveToArchivedWorld(userId, worldId, "saved");

			return Response.ok().entity(this.setWorldState(worldId, "archive")).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	private HashMap<String, Object> setWorldState(String worldId, String newState) {
		WorldService ws = new WorldService();
		World world = ws.get(worldId);

		world.setState(newState);
		ws.setWorld(world);

		return ws.getWorld(worldId);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/unarchive/{worldId}")
	public Response unarchiveWorld(@Context ContainerRequestContext context, @PathParam("worldId") String worldId) {
		String userId = context.getProperty("userid").toString();

		if (!context.getProperty("usertype").equals("teacher"))
			return Response.serverError().build();

		try {
			TeacherService ts = new TeacherService();
			ts.moveArchivedToSavedWorld(userId, worldId);
			return Response.ok().entity(this.setWorldState(worldId, "save")).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/publish/{worldId}")
	public Response createWorld(@Context ContainerRequestContext context, @PathParam("worldId") String worldId,
			LinkedHashMap<String, Object> doc) {
		String userId = context.getProperty("userid").toString();

		if (!context.getProperty("usertype").equals("teacher"))
			return Response.serverError().build();

		try {
			TeacherService ts = new TeacherService();
			ts.moveSavedToActiveWorld(userId, worldId);

			return Response.ok().entity(this.setWorldState(worldId, "publish")).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/updateDescription/{worldId}")
	public Response updateDescription(@Context ContainerRequestContext context, @PathParam("worldId") String worldId,
			LinkedHashMap<String, Object> doc) {
		if (!context.getProperty("usertype").equals("teacher"))
			return Response.serverError().build();

		try {
			WorldService ws = new WorldService();
			World world = ws.get(worldId);
			world.setDescription((String) doc.get("description"));

			ws.setWorld(world);

			return Response.ok().entity(world).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/update/{worldId}")
	public Response updateWorld(@Context ContainerRequestContext context, @PathParam("worldId") String worldId,
			LinkedHashMap<String, Object> doc) {
		if (!context.getProperty("usertype").equals("teacher"))
			return Response.serverError().build();

		try {
			WorldService ws = new WorldService();
			World world = ws.get(worldId);
			world.setDescription((String) doc.get("description"));
			world.setTheme((String) doc.get("theme"));
			world.setCharacter((String) doc.get("character"));
			world.setPrivacy(((String) doc.get("privacy")));
			world.setDifficulty((String) doc.get("difficulty"));
			world.setStage((String) doc.get("stage"));
			world.setLanguage((String) doc.get("language"));
			world.setTitle((String) doc.get("title"));
			world.setTopics((ArrayList<String>) doc.get("topics"));
			world.setKeywords((ArrayList<String>) doc.get("keywords"));
			ws.addKeywords((ArrayList<String>) doc.get("keywords"));

			ws.setWorld(world);

			return Response.ok().entity(world).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	private Response saveInvitations(String worldId, String usertype, LinkedHashMap<String, Object> doc) {
		WorldService ws = new WorldService();
		World world = ws.get(worldId);
		TeacherService ts = new TeacherService();

		LinkedHashMap<String, Object> newDoc = new LinkedHashMap<>();
		ArrayList<String> memberInvitations = (ArrayList<String>) doc.get("memberInvitations");
		ArrayList<String> moderatorInvitations = (ArrayList<String>) doc.get("moderatorInvitations");
		ArrayList<String> communities = (ArrayList<String>) doc.get("communities");

		if (usertype.equals("business")) {
			newDoc.put("memberInvitations", doc.get("memberInvitations"));
			newDoc.put("moderatorInvitations", doc.get("moderatorInvitations"));
		}

		List<String> oldList = world.getModeratorInvitations();
		if (moderatorInvitations.size() > 0) {
			for (String userId : moderatorInvitations) {
				if (oldList.size() == 0 || !oldList.contains(userId)) {
					ts.addInvitationToWorld(userId, worldId);
				}
			}
			for (String userId : oldList) {
				if (!moderatorInvitations.contains(userId)) {
					ts.removeInvitationToWorld(userId, worldId);
				}
			}

		} else if (oldList.size() != 0 && (moderatorInvitations.size() == 0)) {
			for (String userId : oldList) {
				ts.removeInvitationToWorld(userId, worldId);
			}
		}
		world.setModeratorInvitations(moderatorInvitations);

		StudentService ss = new StudentService();
		List<String> oldMemberList = world.getMemberInvitations();
		if (memberInvitations.size() > 0) {
			for (String userId : memberInvitations) {
				if (oldMemberList.size() == 0 || !oldMemberList.contains(userId)) {
					ss.addInvitationToWorld(userId, worldId);
				}
			}
			for (String userId : oldMemberList) {
				if (!memberInvitations.contains(userId)) {
					ss.removeInvitationToWorld(userId, worldId);
				}
			}
		}
		if (oldMemberList.size() != 0 && memberInvitations.size() == 0) {
			for (String userId : oldMemberList) {
				ss.removeInvitationToWorld(userId, worldId);
			}
		}
		world.setMemberInvitations(memberInvitations);

		if (communities.size() > 0)
			world.setCommunities(communities);
		else
			world.setCommunities(new ArrayList<>());

		ws.setWorld(world);

		return Response.ok().entity(world).build();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/save/{worldId}")
	public Response saveWorld(@Context ContainerRequestContext context, @PathParam("worldId") String worldId,
			LinkedHashMap<String, Object> doc) {
		if (!context.getProperty("usertype").equals("teacher")) {
			return this.saveInvitations(worldId, context.getProperty("usertype").toString(), doc);
		}

		try {
			WorldService ws = new WorldService();

			ArrayList<String> resources = (ArrayList<String>) doc.get("resources");
			ArrayList<String> missions = (ArrayList<String>) doc.get("missions");
			ArrayList<String> members = (ArrayList<String>) doc.get("members");
			ArrayList<String> moderators = (ArrayList<String>) doc.get("moderators");
			ArrayList<String> communities = (ArrayList<String>) doc.get("communities");

			ws.getAndSet(worldId, (world) -> {
				if (communities.size() > 0)
					world.setCommunities(communities);
				else
					world.setCommunities(new ArrayList<>());

				if (resources.size() > 0)
					world.setLabResource(resources);
				else
					world.setLabResource(new ArrayList<>());

				if (communities.size() > 0)
					world.setCommunities(communities);
				else
					world.setCommunities(new ArrayList<>());

				if (missions.size() > 0)
					world.setMissions(missions);
				else
					world.setMissions(new ArrayList<>());

				if (members.size() > 0)
					world.setMembers(members);

				if (world.getMembers().size() != 0 && members.size() == 0)
					world.setMembers(members);

				if (moderators.size() > 0)
					world.setModerators(moderators);

				if (world.getModerators().size() != 0 && moderators.size() == 0)
					world.setMembers(moderators);

				return world;
			});

			return this.saveInvitations(worldId, context.getProperty("usertype").toString(), doc);
		} catch (ClassCastException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/agreeModerate/{worldId}")
	public Response agreeModerate(@Context ContainerRequestContext context, @PathParam("worldId") String worldId) {
		if (!context.getProperty("usertype").equals("teacher"))
			return Response.serverError().build();
		String userId = (String) context.getProperty("userid");

		try {
			TeacherService ts = new TeacherService();
			WorldService ws = new WorldService();
			World world = ws.get(worldId);
			List<String> moderatorInvitations = world.getModeratorInvitations();

			if (!moderatorInvitations.contains(userId))
				return Response.serverError().build();

			world.addModerator(userId);
			world.removeModerationInvitation(userId);
			ws.setWorld(world);

			ts.removeInvitationToWorld(userId, worldId);
			ts.addModeratedWorld(userId, worldId);

			return Response.ok().entity(world).build();
		} catch (ClassCastException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/agreeMember/{worldId}")
	public Response agreeMember(@Context ContainerRequestContext context, @PathParam("worldId") String worldId) {
		return this.enroll(context, worldId, true);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/enroll/{worldId}")
	public Response enrollWithoutInvitation(@Context ContainerRequestContext context,
			@PathParam("worldId") String worldId) {
		return this.enroll(context, worldId, false);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/declineModerate/{worldId}")
	public Response declineModerate(@Context ContainerRequestContext context, @PathParam("worldId") String worldId) {
		if (!context.getProperty("usertype").equals("teacher"))
			return Response.serverError().build();

		String userId = (String) context.getProperty("userid");

		try {
			TeacherService ts = new TeacherService();
			WorldService ws = new WorldService();
			World world = ws.get(worldId);
			List<String> moderatorInvitations = world.getModeratorInvitations();

			if (!moderatorInvitations.contains(userId))
				return Response.serverError().build();

			world.removeModerationInvitation(userId);
			ws.setWorld(world);

			ts.removeInvitationToWorld(userId, worldId);

			return Response.ok().entity(world).build();
		} catch (ClassCastException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/declineEnroll/{worldId}")
	public Response declineEnroll(@Context ContainerRequestContext context, @PathParam("worldId") String worldId) {
		if (!context.getProperty("usertype").equals("student"))
			return Response.serverError().build();

		String userId = (String) context.getProperty("userid");

		try {
			StudentService ss = new StudentService();
			WorldService ws = new WorldService();
			World world = ws.get(worldId);

			List<String> invitations = world.getMemberInvitations();

			if (!invitations.contains(userId))
				return Response.serverError().build();

			world.removeMemberInvitation(userId);
			ss.removeInvitationToWorld(userId, worldId);

			ws.setWorld(world);

			return Response.ok().entity(world).build();
		} catch (ClassCastException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/quitWorld/{worldId}")
	public Response quitWorld(@Context ContainerRequestContext context, @PathParam("worldId") String worldId) {
		if (!context.getProperty("usertype").equals("student"))
			return Response.serverError().build();

		String userId = (String) context.getProperty("userid");

		try {
			StudentService ss = new StudentService();
			WorldService ws = new WorldService();
			World world = ws.get(worldId);

			world.removeMember(userId);
			ss.quitWorld(userId, worldId);

			ws.setWorld(world);

			return Response.ok().entity(world).build();
		} catch (ClassCastException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/delete/{worldId}")
	public Response deleteWorld(@Context ContainerRequestContext context, @PathParam("worldId") String worldId) {
		String userId = (String) context.getProperty("userid");
		if (context.getProperty("usertype").equals("student"))
			return Response.serverError().build();

		try {
			TeacherService ts = new TeacherService();
			WorldService ws = new WorldService();
			World world = ws.get(worldId);

			// only owner can delete a world!
			if (!context.getProperty("username").toString().equals(world.getOwner())) {
				return Response.serverError().build();
			}

			ts.moveArchivedToDeletedWorld(userId, worldId);
			world.setState("DELETED");
			ws.setWorld(world);

			return Response.ok().entity(world).build();
		} catch (ClassCastException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	private Response enroll(ContainerRequestContext context, String worldId, boolean invited) {
		if (!context.getProperty("usertype").equals("student"))
			return Response.serverError().build();

		String userId = (String) context.getProperty("userid");

		try {
			StudentService ss = new StudentService();
			WorldService ws = new WorldService();
			World world = ws.get(worldId);

			if (invited) {
				List<String> invitations = world.getMemberInvitations();

				if (!invitations.contains(userId))
					return Response.serverError().build();

				world.removeMemberInvitation(userId);
				ss.removeInvitationToWorld(userId, worldId);
			}

			world.addMember(userId);
			ws.setWorld(world);

			ss.addActiveWorld(userId, worldId);

			return Response.ok().entity(world).build();
		} catch (ClassCastException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/search")
	public Response search(@Context ContainerRequestContext context, @QueryParam("o") @DefaultValue("0") int offset,
			@QueryParam("l") @DefaultValue("256") int limit, @QueryParam("language") String language,
			@QueryParam("difficulties[]") List<String> difficulties, @QueryParam("topics[]") List<String> topics,
			@QueryParam("stages[]") List<String> stages, @QueryParam("searchString") String query) {

		WorldFilter filter = new WorldFilter();
		filter.statePublish().privacyPublic().title(query).language(language).difficulty(difficulties).stages(stages)
				.topics(topics);

		WorldService ws = new WorldService();
		return Response.status(Response.Status.OK).entity(ws.search(filter, limit, offset)).build();
	}

	/**
	 * Returns Class-Type for ArrayLists
	 *
	 * @return class-type
	 */
	private Class<ArrayList<String>> arrayListType() {
		return (Class<ArrayList<String>>) ((Class) ArrayList.class);
	}
}
