package eu.stimey.platform.microservice.api.gateway.storage.startup;

import static eu.stimey.platform.library.utils.StimeyLogger.logger;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;

public class KafkaService {
	protected static KafkaService service;

	protected final String host;
	protected final int port;
	protected final String topic;

	protected Producer<String, String> producer;

	public KafkaService(String host, int port, String topic) {
		super();
		this.host = host;
		this.port = port;
		this.topic = topic;
	}

	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public String getTopic() {
		return topic;
	}

	public Producer<String, String> getProducer() {
		return producer;
	}

	public void postConstrcut() {
		Properties props = new Properties();
		props.put("bootstrap.servers", host + ":" + String.valueOf(port));
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 16384);
		props.put("linger.ms", 1);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

		producer = new KafkaProducer<>(props);
	}

	public void preDestroy() {
		producer.close();
	}

	public static void start() {
		logger().info(String.format("Kafka - Service started..."));
		if (Global.DOCKER)
			service = new KafkaService("kafka_stimey", 9092, "stimey");
		else
			service = new KafkaService("localhost", 9092, "stimey");
		service.postConstrcut();
	}

	public static void stop() {
		service.preDestroy();
		logger().info(String.format("Kafka - Service stopped..."));
	}

	public static KafkaService getService() {
		return service;
	}
}
