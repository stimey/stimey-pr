package eu.stimey.platform.microservice.api.gateway.storage.rest.databind;

public class MonitoringResponse {
	/**
	 * The number of client-side errors captured in a specified period.
	 */
	public long error4XX;
	/**
	 * The number of server-side errors captured in a given period.
	 */
	public long error5XX;
	/**
	 * used_memory_rss: Number of bytes that Redis allocated as seen by the
	 * operating system (a.k.a resident set size).
	 */
	public long cacheUsedMemory;
	/**
	 * The cache hit ratio represents the efficiency of cache usage
	 * cacheHits/(cacheHits + cacheMisses).
	 */
	public double cacheHitRatio;
	/**
	 * keyspace_hits: Number of successful lookup of keys in the main dictionary
	 */
	public long cacheHits;
	/**
	 * keyspace_misses: Number of failed lookup of keys in the main dictionary
	 */
	public long cacheMisses;
	/**
	 * The total number API requests in a given period.
	 */
	public long count;
	/**
	 * The time between when API Gateway receives a request from a client and when
	 * it returns a response to the client.
	 */
	public long latency;

	public MonitoringResponse() {
		super();
	}

	public MonitoringResponse(long error4xx, long error5xx, long count, long latency) {
		super();

		this.error4XX = error4xx;
		this.error5XX = error5xx;
		this.count = count;
		this.latency = latency;
	}

	public MonitoringResponse(long error4xx, long error5xx, long cacheUsedMemory, double cacheHitRatio, long count,
			long latency) {
		super();

		this.error4XX = error4xx;
		this.error5XX = error5xx;
		this.cacheUsedMemory = cacheUsedMemory;
		this.cacheHitRatio = cacheHitRatio;
		this.count = count;
		this.latency = latency;
	}

	public MonitoringResponse(long error4xx, long error5xx, long cacheUsedMemory, double cacheHitRatio, long cacheHits,
			long cacheMisses, long count, long latency) {
		super();

		this.error4XX = error4xx;
		this.error5XX = error5xx;
		this.cacheUsedMemory = cacheUsedMemory;
		this.cacheHitRatio = cacheHitRatio;
		this.cacheHits = cacheHits;
		this.cacheMisses = cacheMisses;
		this.count = count;
		this.latency = latency;
	}
}
