package eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.stimey.platform.library.utils.databind.DateDeserializer;
import eu.stimey.platform.library.utils.databind.DateSerializer;
import eu.stimey.platform.library.utils.databind.DocumentWithId2;

import java.util.*;

public class StatusUpdate extends DocumentWithId2 implements Comparable<StatusUpdate> {

	// TODO: implements sortable für sortieren nach timestamp
	private String creatorId;

	private String text;
	@JsonSerialize(using = DateSerializer.class)
	@JsonDeserialize(using = DateDeserializer.class)
	private Date timestamp;
	private List<String> likesByUserId;
	private List<String> sharesByUserId;
	private String photoId;
	private String fileId;
	private String fileName;
	private String fileDescription;

	// TODO Mentions and hashtags List of userids so it can be retrieved via rest
	// api through filter

	private List<SocialFeedback> socialfeedbacks;
	private Map<String, String> userIdsWhoInteractedWithPost;

	private StatusUpdateCategory category;
	// TODO provide the id of mission, community etc here so while viewing a status
	// update, so you can link it to the
	// corresponding resource (mission, community). For now it just states e.g.
	// Posted in community,
	// Posted in mission without beeing able to click a link to the corresponding
	// community or mission

	public StatusUpdate() {
		this(null, null, StatusUpdateCategory.MY_WALL);
	}

	public StatusUpdate(String creatorId, String text, StatusUpdateCategory category) {
		this.creatorId = creatorId;
		this.text = text;
		this.likesByUserId = new LinkedList<String>();
		this.sharesByUserId = new LinkedList<String>();
		this.timestamp = new Date();
		this.socialfeedbacks = new LinkedList<>();
		this.userIdsWhoInteractedWithPost = new HashMap<>();
		this.category = category;
	}

	// TODO: Discussion mit File oder Bild

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creator) {
		this.creatorId = creator;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public List<String> getLikesByUserId() {
		return likesByUserId;
	}

	public void setLikesByUserId(List<String> likesByUserId) {
		this.likesByUserId = likesByUserId;
	}

	public String getPhotoId() {
		return photoId;
	}

	public void setPhotoId(String photoId) {
		this.photoId = photoId;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public SocialFeedback getSocialFeedback(String id) {
		return socialfeedbacks.stream().filter(d -> d.get_id().toHexString().equals(id)).findAny().orElse(null);
	}

	public List<SocialFeedback> getSocialFeedbacks() {
		return socialfeedbacks;
	}

	public void setSocialFeedbacks(List<SocialFeedback> socialfeedbacks) {
		this.socialfeedbacks = socialfeedbacks;
	}

	public String getFileDescription() {
		return fileDescription;
	}

	public void setFileDescription(String fileDescription) {
		this.fileDescription = fileDescription;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<String> getSharesByUserId() {
		return sharesByUserId;
	}

	public void setSharesByUserId(List<String> sharesByUserId) {
		this.sharesByUserId = sharesByUserId;
	}

	public Map<String, String> getUserIdsWhoInteractedWithPost() {
		return userIdsWhoInteractedWithPost;
	}

	public void setUserIdsWhoInteractedWithPost(Map<String, String> userIdsWhoInteractedWithPost) {
		this.userIdsWhoInteractedWithPost = userIdsWhoInteractedWithPost;
	}

	public StatusUpdateCategory getCategory() {
		return category;
	}

	public void setCategory(StatusUpdateCategory category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "StatusUpdate [statusUpdateID=" + this.get_id() + ", creator=" + creatorId + ", text=" + text
				+ ", timestamp=" + timestamp + "Category=" + category + ", likesByUserId=" + likesByUserId.toString()
				+ ", sharesByUserId=" + sharesByUserId.toString() + ", photoId=" + photoId + ", fileId=" + fileId
				+ ", fileDescription=" + fileDescription + ", socialfeedbacks=" + socialfeedbacks
				+ ", userIdsWhoInteractedWithPost=" + userIdsWhoInteractedWithPost + "]";
	}

	@Override
	public int compareTo(StatusUpdate other) {
		// sort for newest timestamp on first position in List
		return -this.getTimestamp().compareTo(other.getTimestamp());
	}

}