package eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.stimey.platform.library.utils.databind.DateDeserializer;
import eu.stimey.platform.library.utils.databind.DateSerializer;
import eu.stimey.platform.library.utils.databind.DocumentWithId2;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class SocialFeedback extends DocumentWithId2 {

	private String creatorId;
	private String text;
	@JsonSerialize(using = DateSerializer.class)
	@JsonDeserialize(using = DateDeserializer.class)
	private Date timestamp;
	private List<String> likesByUserId;
	private List<SocialFeedback> socialFeedbacks;

	// TODO Mentions and hashtags List of userids so it can be retrieved via rest
	// api through filter

	public SocialFeedback() {
		this(null, null);
	}

	public SocialFeedback(String creatorId, String text) {
		this.creatorId = creatorId;
		this.text = text;
		this.timestamp = new Date();
		this.likesByUserId = new LinkedList<>();
		this.socialFeedbacks = new LinkedList<>();
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public List<String> getLikesByUserId() {
		return likesByUserId;
	}

	public void setLikesByUserId(List<String> likesByUserId) {
		this.likesByUserId = likesByUserId;
	}

	public List<SocialFeedback> getSocialFeedbacks() {
		return socialFeedbacks;
	}

	public void setSocialFeedbacks(List<SocialFeedback> socialFeedbacks) {
		this.socialFeedbacks = socialFeedbacks;
	}

	@Override
	public String toString() {
		return "SocialFeedback [_id=" + get_id().toHexString() + ", creatorId=" + creatorId + ", text=" + text
				+ ", timestamp=" + timestamp + ", likesByUserId=" + likesByUserId + ", socialFeedbacks="
				+ socialFeedbacks + "]";
	}
}
