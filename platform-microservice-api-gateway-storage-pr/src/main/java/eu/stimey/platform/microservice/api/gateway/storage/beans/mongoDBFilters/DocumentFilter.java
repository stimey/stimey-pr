package eu.stimey.platform.microservice.api.gateway.storage.beans.mongoDBFilters;

import org.bson.Document;

import java.util.Arrays;

/**
 * DocumentFilter.java
 *
 * @author Sascha M. Schumacher
 * @version 2018.04.13.0000
 */
public class DocumentFilter {

	/** case insensitivity to match upper and lower cases */
	public static final String CASE_INSENSITIVITY = "i";

	/** for patterns that include anchors */
	public static final String MULTILINE_MATCH = "m";

	/** to ignore all white space characters */
	public static final String EXTENDED = "x";

	/** to match all characters including newline characters */
	public static final String MATCH_NEW_LINE = "s";

	/**
	 * builds a filter with the $and for the searching in the database
	 * 
	 * @param values the list of documents to and together
	 * @return a document with the $and filter for the searching in the database
	 */
	public static Document and(Document... values) {
		if (values.length == 0)
			return new Document();
		if (values.length == 1)
			return values[0];
		return new Document("$and", Arrays.asList(values));
	}

	/**
	 * builds a filter with the $or for the searching in the database
	 * 
	 * @param values the list of documents to or together
	 * @return a document with the $or filter for the searching in the database
	 */
	public static Document or(Document... values) {
		if (values.length == 0)
			return new Document();
		if (values.length == 1)
			return values[0];
		return new Document("$or", Arrays.asList(values));
	}

	/**
	 * builds a filter with the $elemMatch for the searching in the database
	 * 
	 * @param value the document to match
	 * @return a document with the $elemMatch filter for the searching in the
	 *         database
	 */
	public static Document elemMatch(Document value) {
		if (value == null)
			return new Document();
		return new Document("$elemMatch", value);
	}

	/**
	 * builds a filter with the $regex and $options for the searching in the
	 * database
	 * 
	 * @param value   the value for the use of regex
	 * @param options the options of regex
	 * @return a document with the $regex and $options filter for the searching in
	 *         the database
	 */
	public static Document regex(String value, String options) {
		if (value == null || value.length() <= 0)
			return new Document();
		if (options == null || !options.matches("^[imxs]+$"))
			return new Document("$regex", value);
		return new Document("$regex", value).append("$options", options);
	}

}
