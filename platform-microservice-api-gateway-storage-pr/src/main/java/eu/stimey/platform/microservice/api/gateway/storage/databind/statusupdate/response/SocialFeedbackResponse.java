package eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate.response;

import eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate.SocialFeedback;

import java.util.Date;
import java.util.List;

public class SocialFeedbackResponse {
	public String socialFeedbackId;
	public String creatorId;
	public String creatorName;
	public String socialFeedbackText;
	public Date timestamp;
	public List<UserResponse> likesByUser;
	public List<SocialFeedbackResponse> socialFeedbacks;

	public SocialFeedbackResponse(SocialFeedback socialFeedback, String creatorName, List<UserResponse> likesByUser,
			List<SocialFeedbackResponse> socialFeedbacks) {
		this.socialFeedbackId = socialFeedback.get_id().toHexString();
		this.creatorId = socialFeedback.getCreatorId();
		this.creatorName = creatorName;
		this.socialFeedbackText = socialFeedback.getText();
		this.timestamp = socialFeedback.getTimestamp();
		this.likesByUser = likesByUser;
		this.socialFeedbacks = socialFeedbacks;
	}
}
