package eu.stimey.platform.microservice.api.gateway.storage.rest;

import static eu.stimey.platform.library.utils.StimeyLogger.*;

import javax.annotation.PreDestroy;
import javax.ws.rs.ApplicationPath;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.microservice.api.gateway.storage.beans.KeywordService;
import eu.stimey.platform.microservice.api.gateway.storage.beans.StatusUpdateService;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import eu.stimey.platform.microservice.api.gateway.storage.beans.MonitoringService;

/**
 * 
 * REST configuration
 *
 */
@ApplicationPath("api")
public class RESTConfig extends ResourceConfig {
	private UserSharedService userSharedService = new UserSharedService();
	private /* static */ KeywordService keywordService = new KeywordService();
	private StatusUpdateService statusUpdateService = new StatusUpdateService(userSharedService);

	protected final MongoDBService mongoDBService;
	protected final RedisAMQPService redisAMQPService;

	public RESTConfig() {
		super();

		mongoDBService = MongoDBService.getService();
		redisAMQPService = RedisAMQPService.getService();

		logger().info("REST-Service started...");

		packages("eu.stimey.platform.microservice.api.gateway.storage.rest");

		register(new AbstractBinder() {
			protected void configure() {
				// bind(mongoDBService).to(MongoDBService.class);
				bind(redisAMQPService).to(RedisAMQPService.class);
				bind(keywordService).to(KeywordService.class);
				bind(MonitoringService.getService()).to(MonitoringService.class);
				bind(userSharedService).to(UserSharedService.class);
				bind(statusUpdateService).to(StatusUpdateService.class);
			}
		});

		register(new JacksonJsonProvider().configure(SerializationFeature.INDENT_OUTPUT, true));
	}

	/*
	 * public static void subscribe() { keywordService.subscribe(); }
	 */

	@PreDestroy
	public void shutdown() {
		logger().info("REST-Service stopped...");
	}
}
