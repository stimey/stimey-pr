package eu.stimey.platform.microservice.api.gateway.storage.databind.caching;

public class GlobalCachingObject {
	public int index;
	public int lastIndex;

	public GlobalCachingObject() {
		super();
	}

	public GlobalCachingObject(int index, int lastIndex) {
		super();
		this.index = index;
		this.lastIndex = lastIndex;
	}
}
