package eu.stimey.platform.microservice.api.gateway.storage.startup;

import com.mongodb.MongoClient;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import eu.stimey.platform.library.gamification.communication.GamificationCommunication;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import static eu.stimey.platform.library.utils.StimeyLogger.logger;

public class GamificationCommunicationService {

	protected static GamificationCommunicationService service;

	protected final String rabbitmqHost;
	protected final int rabbitmqPort;

	protected final String rabbitmqUsername;
	protected final String rabbitmqPassword;

	protected Connection rabbitmqConnection;

	protected final String mongodbHost;
	protected final int mongodbPort;
	protected final String mongodbDatabaseName;

	protected MongoClient mongodbClient;

	protected GamificationCommunication gamificationCommunication;

	public GamificationCommunicationService(String rabbitmqHost, int rabbitmqPort, String mongodbHost, int mongodbPort,
			String mongodbDatabaseName) {
		super();
		this.rabbitmqHost = rabbitmqHost;
		this.rabbitmqPort = rabbitmqPort;
		this.rabbitmqUsername = null;
		this.rabbitmqPassword = null;
		this.mongodbHost = mongodbHost;
		this.mongodbPort = mongodbPort;
		this.mongodbDatabaseName = mongodbDatabaseName;
	}

	public GamificationCommunicationService(String rabbitmqHost, int rabbitmqPort, String rabbitmqUsername,
			String rabbitmqPassword, String mongodbHost, int mongodbPort, String mongodbDatabaseName) {
		super();
		this.rabbitmqHost = rabbitmqHost;
		this.rabbitmqPort = rabbitmqPort;
		this.rabbitmqUsername = rabbitmqUsername;
		this.rabbitmqPassword = rabbitmqPassword;
		this.mongodbHost = mongodbHost;
		this.mongodbPort = mongodbPort;
		this.mongodbDatabaseName = mongodbDatabaseName;
	}

	public Connection getRabbitmqConnection() {
		return rabbitmqConnection;
	}

	public GamificationCommunication getGamificationCommunication() {
		return gamificationCommunication;
	}

	public String getRabbitmqHost() {
		return rabbitmqHost;
	}

	public int getRabbitmqPort() {
		return rabbitmqPort;
	}

	public MongoClient getMongodbClient() {
		return mongodbClient;
	}

	public String getMongodbHost() {
		return mongodbHost;
	}

	public int getMongodbPort() {
		return mongodbPort;
	}

	public String getMongodbDatabaseName() {
		return mongodbDatabaseName;
	}

	public void postConstrcut() {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(rabbitmqHost);
		if (rabbitmqPort != 0)
			factory.setPort(rabbitmqPort);
		// if (Global.DOCKER) {
		// factory.setUsername(rabbitmqUsername);
		// factory.setPassword(rabbitmqPassword);
		// }

		mongodbClient = new MongoClient(mongodbHost, mongodbPort);
		try {
			rabbitmqConnection = factory.newConnection();
			gamificationCommunication = new GamificationCommunication(rabbitmqConnection, mongodbClient);
		} catch (IOException | TimeoutException e) {
			e.printStackTrace();
		}
	}

	public void preDestroy() {
		try {
			rabbitmqConnection.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void start() {
		logger().info(String.format("GamificationCommunication - Service started..."));
		if (Global.DOCKER)
			service = new GamificationCommunicationService("rabbitmq", 5672, "stimey", "MTEY5B7ORbpHR0jWN2MvauoZi3xaoF",
					"mongo", 27017, "ms_achievement");
		// service = new GamificationCommunicationService("rabbitmq", 5672, "mongo",
		// 27017, "ms_achievement");
		else
			service = new GamificationCommunicationService("localhost", 5672, "localhost", 27017, "ms_achievement");
		service.postConstrcut();
	}

	public static void stop() {
		service.preDestroy();
		logger().info(String.format("GamificationCommunication - Service stopped..."));
	}

	public static GamificationCommunicationService getService() {
		return service;
	}

}
