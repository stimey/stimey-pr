package eu.stimey.platform.microservice.api.gateway.storage.startup;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.utils.StimeyLogger;
import eu.stimey.platform.microservice.api.gateway.storage.beans.MonitoringService;

/**
 * Global web application settings
 * 
 */
@WebListener
public class ServiceServletContextListener implements ServletContextListener {
	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		Global.startup();

		StimeyLogger.setApplicationName(servletContextEvent.getServletContext().getContextPath());

		MongoDBService.start("ms_api_gateway_storage");
		RedisAMQPService.start();
		if (Global.KAFKA)
			KafkaService.start();
		MonitoringService.start();
		GamificationCommunicationService.start();
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		MonitoringService.stop();
		RedisAMQPService.stop();
		MongoDBService.stop();
		if (Global.KAFKA)
			KafkaService.stop();
		MonitoringService.stop();
		GamificationCommunicationService.stop();
	}
}
