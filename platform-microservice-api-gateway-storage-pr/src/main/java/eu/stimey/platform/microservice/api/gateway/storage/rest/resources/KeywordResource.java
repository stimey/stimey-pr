package eu.stimey.platform.microservice.api.gateway.storage.rest.resources;

import eu.stimey.platform.library.fuzzy.search.FuzzySearchUtils;
import eu.stimey.platform.library.utils.StimeyLogger;
import eu.stimey.platform.microservice.api.gateway.storage.beans.KeywordService;
import eu.stimey.platform.microservice.api.gateway.storage.databind.keyword.Keyword;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Path("/keywords")
public class KeywordResource {

	@Inject
	KeywordService keywordService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getKeywordList(@QueryParam("searchQuery") @DefaultValue("") String searchQuery,
			@QueryParam("keywordTag[]") List<String> keywordTags) {

		StimeyLogger.logger().info("KeywordResource: searchQuery=" + searchQuery);

		if (searchQuery.isEmpty() && keywordTags.size() == 0)
			return Response.status(Response.Status.BAD_REQUEST).build();

		if (keywordTags.size() > 0)
			return Response.status(Response.Status.OK).entity(this.keywordService.getKeywordsByTags(keywordTags))
					.build();

		List<Keyword> keywords = keywordService.findKeywords(searchQuery);
		StimeyLogger.logger().info("KeywordResource: found keywords=" + keywords);
		
		if (keywords.size() <= 10) {
			return Response.status(Response.Status.OK).entity(keywords).build();
		}
		
		/*
		// Erroneous code
		List<Keyword> result = new ArrayList<>();
		Iterator<Keyword> iterator = keywords.iterator();
		while (iterator.hasNext() && result.size() < 10) {
			Keyword cursor = iterator.next();
			if (FuzzySearchUtils.validLevenshteinDistance50(searchQuery, cursor.tag)) {
				result.add(keywords.remove(keywords.indexOf(cursor)));
			}
		}

		for (int i = result.size(); i <= 10;) {
			result.add(keywords.remove(0));
		}
		
		return Response.status(Response.Status.OK).entity(keywords).build();
		*/
		
		List<Keyword> result = new ArrayList<>();
		Iterator<Keyword> iterator = keywords.iterator();
		int i=0;
		while (iterator.hasNext() && i<10) {
			Keyword keyword = iterator.next();
			if (FuzzySearchUtils.validLevenshteinDistance50(searchQuery, keyword.tag)) {
				result.add(keyword);
				iterator.remove();
				i++;
			}
		}
		
		int size = result.size();
		if (size<10)
			for (int j=0; j<10-size; j++) {
				result.add(keywords.get(j));
			}
		
		return Response.status(Response.Status.OK).entity(result).build();
	}
}
