package eu.stimey.platform.microservice.api.gateway.storage.utils;

import static servlet.web.proxy.ProxyLogger.logger;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.tuple.Pair;
import servlet.web.proxy.HTTPProxyClient;
import servlet.web.proxy.ProxyServlet;

import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.utils.security.JsonWebToken;
import eu.stimey.platform.microservice.api.gateway.storage.beans.MonitoringService;
import eu.stimey.platform.microservice.api.gateway.storage.data.collection.DataCollection;
import eu.stimey.platform.microservice.api.gateway.storage.startup.Global;
import eu.stimey.platform.microservice.api.gateway.storage.startup.KafkaService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import redis.clients.jedis.Jedis;

/**
 * 
 * Reverse Proxy Servlet (API Gateway for the STIMEY platform)
 *
 */
public class StimeyApiProxyServlet extends ProxyServlet {
	protected static final long serialVersionUID = 6794852852578211665L;

	protected static Map<String, String> serviceList;

	static {
		serviceList = new HashMap<>();
		// serviceList.put("", "..."); // match_url = null
		if (Global.DOCKER) {
			serviceList.put("auth", "http://ms_auth:8080");
			serviceList.put("config", "http://ms_config:8080");
			serviceList.put("dashboard", "http://ms_dashboard:8080");
			serviceList.put("courses", "http://ms_courses:8080");
			serviceList.put("chat", "http://ms_chat:8080");
			// serviceList.put("files", "http://ms_files:8080"); use global.js in storage
			serviceList.put("activitystream", "http://ms_activitystream:8080");
			serviceList.put("achievements", "http://ms_achievement:8080");
			serviceList.put("communities", "http://ms_communities:8080");
			serviceList.put("lab", "http://ms_lab:8080");
			serviceList.put("badges", "http://ms_badges:8080");
			serviceList.put("planets", "http://ms_planets:8080");
			serviceList.put("collector", "http://ms_collector:8080");
			serviceList.put("scientix", "http://resources.scientix.eu");
			serviceList.put("evaluation", "http://ms_evaluation:8080");
			serviceList.put("notification", "http://ms_notification:8080");
			serviceList.put("robot", "http://ms_robot:8080");
			serviceList.put("radio", "http://ms_radio:8080");
		} else {
			serviceList.put("auth", "http://localhost:8080/platform-microservice-auth");
			serviceList.put("config", "http://localhost:8080/platform-microservice-admin-configuration");
			serviceList.put("dashboard", "http://localhost:8080/platform-microservice-dashboard");
			serviceList.put("courses", "http://localhost:8080/platform-microservice-courses");
			serviceList.put("chat", "http://localhost:8080/platform-microservice-chat");
			// serviceList.put("files",
			// "http://localhost:8080/platform-microservice-files"); use global.js in
			// storage
			serviceList.put("activitystream", "http://localhost:8080/platform-microservice-activitystream");
			serviceList.put("template", "http://localhost:8080/prototype2-microservice-web-template");
			serviceList.put("achievements", "http://localhost:8080/platform-microservice-achievement");
			serviceList.put("communities", "http://localhost:8080/platform-microservice-communities");
			serviceList.put("lab", "http://localhost:8080/platform-microservice-lab");
			serviceList.put("badges", "http://localhost:8080/platform-microservice-badges");
			serviceList.put("planets", "http://localhost:8080/platform-microservice-planets");
			serviceList.put("collector", "http://localhost:8080/platform-microservice-collector");
			serviceList.put("scientix", "http://resources.scientix.eu");
			serviceList.put("evaluation", "http://localhost:8080/platform-microservice-evaluation");
			serviceList.put("notification", "http://localhost:8080/platform-microservic-notification");
			serviceList.put("robot", "http://localhost:8080/platform-microservice-robot");
			serviceList.put("radio", "http://localhost:8080/platform-microservice-radio");
		}
	}

	protected Pair<String, String> matchingService(HttpServletRequest servletRequest) {
		Pair<String, String> result = null;

		logger().debug("getContextPath: " + servletRequest.getContextPath());
		logger().debug("getRequestURI: " + servletRequest.getRequestURI());
		logger().debug("getServletPath: " + servletRequest.getServletPath());
		logger().debug("getPathInfo: " + servletRequest.getPathInfo());

		String match_url = servletRequest.getPathInfo();
		if (match_url == null)
			match_url = "/";
		logger().debug(match_url);
		String[] match_array = match_url.split("/");

		if (match_array.length > 1)
			for (String prefix : serviceList.keySet()) {
				if (!prefix.isEmpty()) {
					if (match_array[1].equals(prefix)) {
						result = Pair.of(prefix, serviceList.get(prefix));
						break;
					}
				} else if (match_url.equals("/")) {
					result = Pair.of(prefix, serviceList.get(prefix));
					break;
				}
			}

		return result;
	}

	@Override
	protected void config(HTTPProxyClient proxyClient) {
		proxyClient.setConnectionRequestTimeout(5000);
		proxyClient.setConnectTimeout(5000);
		proxyClient.setReadTimeout(30000);

		proxyClient.setMaxTotalConnections(200);
		proxyClient.setMaxConnectionsPerRoute(20);
	}

	@Override
	protected void service(HttpServletRequest servletRequest, HttpServletResponse servletResponse)
			throws ServletException, IOException {
		Pair<String, String> target = matchingService(servletRequest);
		if ((target != null && (target.getKey().equals("auth") || target.getKey().equals("config")))
				|| authenticated(servletRequest)) {
			if (target != null) {
				URI targetObj = getTargetObj(target.getValue());
				long start = System.currentTimeMillis();
				doService(servletRequest, servletResponse, target.getValue(), targetObj, null, new MutableBoolean(true),
						null, true, "/" + target.getKey(), null);
				long stop = System.currentTimeMillis();
				MonitoringService.getService().update(servletResponse.getStatus(), start, stop);
				dataCollection(servletRequest);
			} else
				servletResponse.sendError(404);
		} else
			handleNotAuthenticated(servletResponse);
	}

	private static volatile String PRIVATE_KEY = null;
	protected static final Object lock = new Object();

	// uses Double-Check-Idiom a la Bloch
	public static String getPrivateKey() {
		String temp = PRIVATE_KEY;
		if (temp == null) {
			synchronized (lock) {
				temp = PRIVATE_KEY;
				if (temp == null) {
					Jedis jedis = RedisAMQPService.getService().getJedisPool().getResource();
					PRIVATE_KEY = temp = jedis.get("shared_global-private_key");
					jedis.close();
				}
			}
		}

		return temp;
	}

	protected boolean authenticated(HttpServletRequest servletRequest) {
		boolean result = false;

		try {
			String compactJws = JsonWebToken.getJWT(servletRequest);
			Jws<Claims> claims = null;
			result = compactJws != null
					&& (claims = JsonWebToken.verify(JsonWebToken.transformKey(getPrivateKey()), compactJws)) != null;
			if (result) {
				servletRequest.setAttribute("userid", claims.getBody().getSubject());
				servletRequest.setAttribute("username", (String) claims.getBody().get("username"));
				servletRequest.setAttribute("usertype", (String) claims.getBody().get("usertype"));
				if (claims.getBody().get("usersubtype") != null)
					servletRequest.setAttribute("usersubtype", (String) claims.getBody().get("usersubtype"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	protected void handleNotAuthenticated(HttpServletResponse servletResponse) throws IOException {
		servletResponse.sendError(401);
	}

	protected void dataCollection(HttpServletRequest servletRequest) {
		if (Global.KAFKA) {
			String userid = (String) servletRequest.getAttribute("userid");
			String username = (String) servletRequest.getAttribute("username");
			String usertype = (String) servletRequest.getAttribute("usertype");
			servletRequest.setAttribute("userid", userid);
			servletRequest.setAttribute("username", username);
			servletRequest.setAttribute("usertype", usertype);
			DataCollection.send(KafkaService.getService().getProducer(), KafkaService.getService().getTopic(), userid,
					username, usertype, servletRequest);
		}
	}
}
