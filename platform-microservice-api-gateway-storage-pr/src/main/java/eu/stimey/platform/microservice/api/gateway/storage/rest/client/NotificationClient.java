package eu.stimey.platform.microservice.api.gateway.storage.rest.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.microservice.api.gateway.storage.startup.Global;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;
import java.util.Collection;

public class NotificationClient {

	/**
	 * Sends a notification request to the notification service
	 *
	 * @param receiverUserID  The ID of the User who is to receive the notification
	 *
	 * @param userIDs         The ID of the person mentioned in the notification
	 * @param postContentText Text of a content link
	 * @param postContentRef  The reference of a context link
	 * @param text            The text showing inside the notification
	 * @return The response of the request
	 */
	public static final Response sendNotification(String receiverUserID, String origin, Collection<String> userIDs,
			String postContentText, String postContentRef, String text, String icon, Cookie cookie) {

		return StimeyClient.post(StimeyClient.create(), "notification", Global.DOCKER,
				"api/notification/send/" + receiverUserID, cookie,
				notificationToJsonObject(origin, userIDs, postContentText, postContentRef, text, icon).toString());
	}

	/**
	 * Sends a notification request to broadcast the notification
	 *
	 * @param distributorID   The ID of the User who is to receive the notification
	 *
	 * @param userIDs         The ID of the person mentioned in the notification
	 * @param postContentText Text of a content link
	 * @param postContentRef  The reference of a context link
	 * @param text            The text showing inside the notification
	 * @return The response of the request
	 */
	public static final Response broadcastNotification(String distributorID, String origin, Collection<String> userIDs,
			String postContentText, String postContentRef, String text, String icon, Cookie cookie) {

		return StimeyClient.post(StimeyClient.create(), "notification", Global.DOCKER,
				"api/notification/distribute/" + distributorID, cookie,
				notificationToJsonObject(origin, userIDs, postContentText, postContentRef, text, icon));
	}

	/**
	 * Sends a notification request to broadcast the notification
	 *
	 *
	 * @param userIDList      The IDs of the users to receive the notification
	 * @param userIDs         The ID of the person mentioned in the notification
	 * @param postContentText Text of a content link
	 * @param postContentRef  The reference of a context link
	 * @param text            The text showing inside the notification
	 * @return The response of the request
	 */
	public static final Response broadcastNotification(Collection<String> userIDList, String origin,
			Collection<String> userIDs, String postContentText, String postContentRef, String text, String icon,
			Cookie cookie) {

		return StimeyClient.post(StimeyClient.create(), "notification", Global.DOCKER, "api/notification/broadcast",
				cookie, broadcastToJsonString(userIDList,
						notificationToJsonObject(origin, userIDs, postContentText, postContentRef, text, icon)));
	}

	/**
	 * Json parsing did not work, this is a workaround to parse a normal
	 * notification to Json
	 *
	 * @param userIDs         @see Notification
	 * @param postContentText @see Notification
	 * @param postContentRef  @see Notification
	 * @param text            @see Notification
	 * @return Json representation of the Notification as String
	 */
	public static final ObjectNode notificationToJsonObject(String origin, Collection<String> userIDs,
			String postContentText, String postContentRef, String text, String icon) {

		ObjectMapper mapper = new ObjectMapper();
		ObjectNode node = mapper.createObjectNode();

		node.put("origin", origin);
		node.putArray("userIDs").addAll((ArrayNode) mapper.valueToTree(userIDs));
		node.put("postContentText", postContentText);
		node.put("postContentRef", postContentRef);
		node.put("text", text);
		node.put("icon", icon);

		return node;
	}

	/**
	 * Json parsing did not work, this is a workaround to parse a BroadcastRequest
	 * to Json
	 *
	 * @param userIDList List of userIDs
	 * @param node       A ObjectNode of the Json representation of a notification
	 * @return Json representation of the BroadCastRequest as String
	 */
	public static final String broadcastToJsonString(Collection<String> userIDList, ObjectNode node) {
		node.putArray("userIDList").addAll((ArrayNode) new ObjectMapper().valueToTree(userIDList));
		return node.toString();
	}
}
