package eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate.requests;

public class StatusUpdateRequest {

	public String text;
	public String photoId;
	public String fileId;
	public String fileName;
	public String fileDescription;
	public String category;

	public StatusUpdateRequest() {

	}

}
