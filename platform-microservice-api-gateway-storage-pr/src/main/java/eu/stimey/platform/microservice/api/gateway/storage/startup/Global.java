package eu.stimey.platform.microservice.api.gateway.storage.startup;

import java.util.concurrent.TimeUnit;

import eu.stimey.platform.library.core.startup.DefaultGlobal;

/**
 * 
 * Global settings (environment variables)
 *
 */
public final class Global extends DefaultGlobal {
	public static final boolean KAFKA = false; // DON'T CHANGE THIS VALUE, KAFKA IS DISABLED

	public static final long MONITORING_PERIOD = 1;
	public static final TimeUnit MONITORING_UNIT = TimeUnit.HOURS;

	public static void startup() {
		DefaultGlobal.startup();
	}
}
