/**
 * Copyright (c) 2018, Stimey
 */
package eu.stimey.platform.microservice.api.gateway.storage.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.library.data.access.databind.Message;
import eu.stimey.platform.library.fuzzy.search.FuzzySearch;
import eu.stimey.platform.library.utils.StimeyLogger;
import eu.stimey.platform.microservice.api.gateway.storage.databind.keyword.Keyword;
import org.bson.Document;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class KeywordService {
	public Caching<String, Keyword> keywordCache;

	public KeywordService() {
		super();
		this.keywordCache = new Caching<>("ms_api_gateway_storage_keywords", "tag", Keyword.class, "keywords",
				GlobalVariables.CACHE_CONFIG);
		this.keywordCache.clear();
		this.subscribe();
	}

	/**
	 * can be called via: Caching.publish("addKeywords", new Message(null,
	 * List<Keywords>));
	 */
	public void subscribe() {
		this.keywordCache.subscribe("addKeywords", (message) -> {
			StimeyLogger.logger().info("KeywordService: received keywords" + message.getValue());
			((List<String>) message.getValue()).forEach((keyword) -> this.setKeyword(new Keyword(keyword)));
		});
		StimeyLogger.logger().info("KeywordService subscribed to topic 'addKeywords'");
	}

	/**
	 * Gets the keyword with the given tag
	 *
	 * @param keywordTag the tag of the keyword
	 * @return the keyword with the given tag
	 */
	public Keyword getKeywordByTag(String keywordTag) {
		if (keywordTag == null || keywordTag.isEmpty())
			throw new IllegalArgumentException("the keyword tag cannot be null.");
		this.keywordCache.find(new Document("tag", keywordTag), null, 0, 0, (keyword) -> keyword.tag, true);
		Keyword keyword = this.keywordCache.get(keywordTag);
		return keyword;
	}

	/**
	 * Gets the keywords with the given tags
	 *
	 * @param keywordTags the tags of the keywords
	 * @return the keywords with the given tags
	 */
	public List<Keyword> getKeywordsByTags(List<String> keywordTags) {
		List<Keyword> keywords = new LinkedList<>();
		for (String keywordTag : keywordTags) {
			if (keywordTag != null)
				keywords.add(this.getKeywordByTag(keywordTag));
		}
		return keywords;
	}

	/**
	 * Finds keywords which contains the given filter
	 *
	 * @param filter used to find the specific keywords in the cache
	 * @param offset indicates with which document from the database will started
	 * @param limit  indicates how many documents are retrieved from the database
	 * @return a list with keywords which contains the given filter
	 */
	public List<Keyword> findKeywords(Document filter, Document sort, int offset, int limit) {
		List<Keyword> keywords = new LinkedList<>();
		for (String tag : findTagsOfKeywords(filter, sort, offset, limit)) {
			keywords.add(this.keywordCache.get(tag));
		}
		return keywords;
	}

	public List<Keyword> findKeywords(String query) {
		return this.findKeywords(soundex(query), new Document("matches", -1), 0, 200);
	}

	private Document soundex(String query) {
		if (query == null || query.isEmpty())
			return new Document();
		List<Document> result = new LinkedList<>();
		for (String part : query.split(" ")) {
			String soundex = FuzzySearch.soundexTrimZeros(FuzzySearch.soundex(part));
			result.add(new Document("soundex", FilterBuilder.regex(soundex, FilterBuilder.CASE_INSENSITIVITY)));
		}
		return FilterBuilder.or(result.toArray(new Document[result.size()]));
	}

	/**
	 * Finds the tags of keywords which contains the given filter
	 *
	 * @param filter used to find the specific tags of keywords in the cache
	 * @param offset indicates with which document from the database will started
	 * @param limit  indicates how many documents are retrieved from the database
	 * @return a list with tags of keywords which contains the given filter
	 */
	public List<String> findTagsOfKeywords(Document filter, Document sort, int offset, int limit) {
		return this.keywordCache.find(filter, sort, offset, limit, (keyword) -> keyword.tag, true);
	}

	/**
	 * Persists a keyword in the database. If the keyword already exists, matches is
	 * incremented.
	 *
	 * @param keyword the keyword to post in the database
	 */
	public void setKeyword(Keyword keyword) {
		if (keyword == null)
			StimeyLogger.logger().error("the keyword cannot be null.",
					new IllegalArgumentException("the keyword cannot be null."));
		Keyword existingKeyword = getKeywordByTag(keyword.tag);
		if (existingKeyword != null) {
			StimeyLogger.logger().info("found keyword=" + keyword);
			existingKeyword.matches++;
			StimeyLogger.logger().info("matches increased");
			this.keywordCache.set(existingKeyword.tag, existingKeyword);
		} else {
			this.keywordCache.set(keyword.tag, keyword);
			StimeyLogger.logger().info("set keyword");
		}
	}

}
