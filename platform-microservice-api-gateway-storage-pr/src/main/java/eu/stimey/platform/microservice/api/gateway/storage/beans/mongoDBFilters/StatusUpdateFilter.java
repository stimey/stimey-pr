package eu.stimey.platform.microservice.api.gateway.storage.beans.mongoDBFilters;

import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.LinkedList;
import java.util.List;

public class StatusUpdateFilter extends DocumentFilter {
	/**
	 * builds a filter with the keywords, the topics, the stages and the
	 * difficulties for the searching in the database
	 *
	 * @return a document with the keywords filter, the topics filter, the stage
	 *         filter and the difficulty filter for the searching in the database
	 */
	public static Document filter(List<String> statusUpdateIds, List<String> userId, List<String> categories) {
		List<Document> result = new LinkedList<>();
		Document buffer;
		if (!(buffer = statusUpdateIds(statusUpdateIds)).isEmpty())
			result.add(buffer);
		if (!(buffer = userIds(userId)).isEmpty())
			result.add(buffer);
		if (!(buffer = category(categories)).isEmpty())
			result.add(buffer);

		return and(result.toArray(new Document[result.size()]));
	}

	/**
	 * builds a filter with the userIds for searching in the database
	 *
	 * @param statusUpdateIds the filter for the statusUpdateIds
	 * @return a document with the statusUpdateIds filter for searching in the
	 *         database
	 */
	public static Document statusUpdateIds(List<String> statusUpdateIds) {
		if (statusUpdateIds == null || statusUpdateIds.isEmpty()) {
			return new Document();
		}
		List<Document> result = new LinkedList<>();
		for (String statusUpdateId : statusUpdateIds) {
			result.add(new Document("_id", new ObjectId(statusUpdateId)));
		}
		if (result.size() == 0)
			return new Document();
		if (result.size() == 1)
			return result.get(0);
		return or(result.toArray(new Document[result.size()]));
	}

//    private static Document getIdsFilter(String ids)
//    {
//
//    }

	/**
	 * builds a filter with the userIds for searching in the database
	 *
	 * @param userIds the filter for the userIds
	 * @return a document with the userIds filter for searching in the database
	 */
	public static Document userIds(List<String> userIds) {
		if (userIds == null || userIds.isEmpty()) {
			return new Document();
		}
		List<Document> result = new LinkedList<>();
		for (String userId : userIds) {
			result.add(new Document("creatorId", userId));
		}
		if (result.size() == 0)
			return new Document();
		if (result.size() == 1)
			return result.get(0);
		return or(result.toArray(new Document[result.size()]));
	}

	/**
	 * builds a filter with the userIds for searching in the database
	 *
	 * @param categories the filter for the categories
	 * @return a document with the statusUpdateIds filter for searching in the
	 *         database
	 */
	public static Document category(List<String> categories) {
		if (categories == null || categories.isEmpty()) {
			return new Document();
		}
		List<Document> result = new LinkedList<>();
		for (String category : categories) {
			result.add(new Document("category", category));
		}
		if (result.size() == 0)
			return new Document();
		if (result.size() == 1)
			return result.get(0);
		return or(result.toArray(new Document[result.size()]));
	}

}
