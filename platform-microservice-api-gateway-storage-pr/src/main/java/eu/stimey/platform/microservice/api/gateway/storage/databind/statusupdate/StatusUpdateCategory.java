package eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate;

import com.fasterxml.jackson.annotation.JsonValue;

public enum StatusUpdateCategory {
	MY_WALL("myWall"), FOREIGN_WALL("foreignWall"), MISSION("mission"), COMMUNITY("community"),
	EPORTFOLIO_ALBUM("eportfolioAlbum");

	private String text;

	StatusUpdateCategory(String text) {
		this.text = text;
	}

	public static StatusUpdateCategory fromString(String text) {
		for (StatusUpdateCategory d : StatusUpdateCategory.values()) {
			if (d.text.equals(text)) {
				return d;
			}
		}
		throw new IllegalArgumentException("no such category " + text);
	}

	@JsonValue
	public String getText() {
		return text;
	}
}
