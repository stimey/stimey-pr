package eu.stimey.platform.microservice.api.gateway.storage.data.collection;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataCollection {
	public static void send(Producer<String, String> producer, String topic, String key, String username,
			String usertype, HttpServletRequest servletRequest) {
		Map<String, List<String>> headers = new HashMap<>();
		Enumeration<String> headerNames = servletRequest.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();

			List<String> values = new LinkedList<>();
			Enumeration<String> headerValues = servletRequest.getHeaders(headerName);
			while (headerValues.hasMoreElements())
				values.add(headerValues.nextElement());

			headers.put(headerName, values);
		}

		RequestData requestData = new RequestData(username, usertype, servletRequest.getMethod(),
				servletRequest.getRequestURI(), headers, servletRequest.getParameterMap(), "");

		String value = null;
		try {
			value = new ObjectMapper().writeValueAsString(requestData);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		if (value != null)
			producer.send(new ProducerRecord<String, String>(topic, key, value));
	}
}
