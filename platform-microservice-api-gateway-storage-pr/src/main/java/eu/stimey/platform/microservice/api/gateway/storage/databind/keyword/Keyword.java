
package eu.stimey.platform.microservice.api.gateway.storage.databind.keyword;

import eu.stimey.platform.library.fuzzy.search.FuzzySearch;

public class Keyword {
	public String soundex;
	public String tag;
	public long matches;
	public String displayText;

	public Keyword() {
		super();
		this.soundex = "";
		this.tag = "";
		this.matches = 0;
	}

	public Keyword(String tag) {
		super();
		this.tag = tag;
		this.soundex = FuzzySearch.soundex(tag);
		this.matches = 0;
	}

	public String getDisplayText() {
		return '#' + tag;
	}

	public String get_id() {
		return this.tag;
	}

	public void set_id(String _id) {
		this.tag = _id;
	}

	@Override
	public String toString() {
		return "Keyword [soundex=" + soundex + ", tag=" + tag + ", matches=" + matches + "]";
	}

}
