package eu.stimey.platform.microservice.api.gateway.storage.beans;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate.StatusUpdate;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StatusUpdateService {

	// @Inject
	private UserSharedService userSharedService;

	private Caching<String, StatusUpdate> statusUpdateCache;

	/*
	 * public StatusUpdateService() { this.statusUpdateCache = new
	 * Caching<>("ms_api_gateway_storage_statusupdate", "_id", StatusUpdate.class,
	 * "statusupdate", GlobalVariables.CACHE_CONFIG);
	 * this.statusUpdateCache.setWorkaround_keyMapper((key) -> new ObjectId(key)); }
	 */

	public StatusUpdateService(UserSharedService userSharedService) {
		this.statusUpdateCache = new Caching<>("ms_api_gateway_storage_statusupdate", "_id", StatusUpdate.class,
				"statusupdate", GlobalVariables.CACHE_CONFIG);
		this.statusUpdateCache.setWorkaround_keyMapper((key) -> new ObjectId(key));
		this.userSharedService = userSharedService;
	}

	public void setStatusUpdate(StatusUpdate statusupdate) {
		statusUpdateCache.set(statusupdate.get_id().toHexString(), statusupdate);
	}

	public void setStatusUpdate(String statusUpdateID, StatusUpdate statusupdate) {
		statusUpdateCache.set(statusUpdateID, statusupdate);
	}

	public void delStatusUpdate(String statusUpdateID) {
		this.statusUpdateCache.del(statusUpdateID);
	}

	public StatusUpdate getStatusUpdateById(String statusUpdateID) {
		statusUpdateCache.find(new Document("_id", new ObjectId(statusUpdateID)), null, 0, 0,
				(statusupdate) -> statusupdate.get_id().toHexString(), true);
		return statusUpdateCache.get(statusUpdateID);
	}

	public List<StatusUpdate> getStatusUpdateList(Document filter, int offset, int limit, String order) {
		return this.getStatusUpdateByIds(this.getStatusUpdateIds(filter, offset, limit, order));
	}

	public List<String> getStatusUpdateIds(Document filter, int offset, int limit, String order) {
		Document sort;
		if (order.equals("dec")) {
			sort = new Document("timestamp", -1);
		} else {
			sort = null;
		}
		List<String> result = statusUpdateCache.find(filter, sort, offset, limit,
				(statusupdate) -> statusupdate.get_id().toHexString(), true);

		return result;
	}

	public List<StatusUpdate> getStatusUpdateByIds(List<String> statusUpdateIDs) {
		List<StatusUpdate> statusupdates = new LinkedList<>();
		for (String statusUpdateID : statusUpdateIDs) {
			statusupdates.add(this.getStatusUpdateById(statusUpdateID));
		}
		return statusupdates;
	}

	public Caching<String, StatusUpdate> getCache() {
		return statusUpdateCache;
	}

	public String parseMentionsInTextToUrl(String text) {

		String regex = "((?:^|(?<=\\s))\\@\\w+\\b)";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		String textResult = text;

		while (matcher.find()) {
			String found = matcher.group(0);
			String username = found.replaceFirst("@", "");
			String mentionedUserId = userSharedService.getUserId(username); // "5a156e3a394d09ca9f4f7847"; //

			if (mentionedUserId != null) {
				String regexInner = "((?:^|(?<=\\s))" + found + "\\b)";
				textResult = textResult.replaceFirst(regexInner,
						"<a href='/profiles/" + mentionedUserId + "'>" + found + "</a>");
			}
		}
		return textResult;
	}

	public String parseURLsInText(String text) {
		String regex = "(http(s)?:\\/\\/.)?(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&/=]*)";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		String textResult = text;

		while (matcher.find()) {
			String found = matcher.group(0);
			String regexInner = "((?:^|(?<=\\s))" + found + "\\b)";

			if (found.contains("www.")) {
				textResult = textResult.replaceFirst(regexInner, "<a href='//" + found + "'>" + found + "</a>");
			} else {
				textResult = textResult.replaceFirst(regexInner, "<a href='" + found + "'>" + found + "</a>");
			}
		}
		return textResult;
	}

	public String parseKeywordsInTextToUrl(String text) {

		String regex = "((?:^|(?<=\\s))\\#\\w+\\b)";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		String textResult = text;

		while (matcher.find()) {
			String found = matcher.group(0);
			// String keyword = found.replaceFirst("#", "");

			String regexInner = "((?:^|(?<=\\s))" + found + "\\b)";
			textResult = textResult.replaceFirst(regexInner, "<a href=''>" + found + "</a>");

		}
		return textResult;
	}
}
