package eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate.response;

import eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate.StatusUpdate;
import eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate.StatusUpdateCategory;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class StatusUpdateResponse {

	public String statusUpdateId;
	public String creatorId;
	public String creatorName;
	public String statusUpdateText;
	public Date timestamp;

	// Key = User ID, Value = User Name
	public List<UserResponse> likesByUser;
	public List<UserResponse> sharesByUser;
	public String photoFileId;
	public String fileId;
	public String fileName;
	public String fileDescription;
	public List<SocialFeedbackResponse> socialFeedbacks;
	public Map<String, String> userIdsWhoInteractedWithPost;
	public StatusUpdateCategory category;

	public StatusUpdateResponse(StatusUpdate statusUpdate, String creatorName, List<UserResponse> likesByUser,
			List<UserResponse> sharesByUser, List<SocialFeedbackResponse> socialUpdateResponse,
			StatusUpdateCategory category) {
		this.statusUpdateId = statusUpdate.get_id().toHexString();
		this.creatorId = statusUpdate.getCreatorId();
		this.creatorName = creatorName;
		this.statusUpdateText = statusUpdate.getText();
		this.timestamp = statusUpdate.getTimestamp();
		this.likesByUser = likesByUser;
		this.sharesByUser = sharesByUser;
		this.photoFileId = statusUpdate.getPhotoId();
		this.fileId = statusUpdate.getFileId();
		this.fileName = statusUpdate.getFileName();
		this.fileDescription = statusUpdate.getFileDescription();
		this.socialFeedbacks = socialUpdateResponse;
		this.userIdsWhoInteractedWithPost = statusUpdate.getUserIdsWhoInteractedWithPost();
		this.category = category;
	}
}
