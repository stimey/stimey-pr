package eu.stimey.platform.microservice.api.gateway.storage.rest.resources;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.data.access.databind.Message;
import eu.stimey.platform.library.gamification.databind.GamificationEvents;
import eu.stimey.platform.library.gamification.databind.GamificationNotificationResponse;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.microservice.api.gateway.storage.beans.StatusUpdateService;
import eu.stimey.platform.microservice.api.gateway.storage.beans.mongoDBFilters.StatusUpdateFilter;
import eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate.SocialFeedback;
import eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate.StatusUpdate;
import eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate.StatusUpdateCategory;
import eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate.requests.StatusUpdateRequest;
import eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate.response.SocialFeedbackResponse;
import eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate.response.StatusUpdateResponse;
import eu.stimey.platform.microservice.api.gateway.storage.databind.statusupdate.response.UserResponse;
import eu.stimey.platform.microservice.api.gateway.storage.rest.client.NotificationClient;
import eu.stimey.platform.microservice.api.gateway.storage.startup.GamificationCommunicationService;
import eu.stimey.platform.microservice.api.gateway.storage.startup.Global;
import org.apache.commons.lang3.tuple.Pair;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Path("/statusupdate")
public class StatusUpdateResource {

	// TODO refactor!!!!!!!!!!!!

	@Inject
	StatusUpdateService statusUpdateService;

	@Inject
	UserSharedService userSharedService;
	// StatusUpdate

	/**
	 * Requests the server to return a list with StatusUpdates. The query parameters
	 * influence the list and can be combined.
	 *
	 * @param context         @see ContainerRequestContext
	 * @param userIds         get all Status Updates created by the userIds given in
	 *                        the List<String>
	 * @param onlyIds         get a List<String> of Status Update IDs instead of
	 *                        whole objects (true = onlyIds, false or all other
	 *                        values = StatusUpdateObject )
	 * @param statusUpdateIds filter results by the given StatusUpdate IDs
	 * @param categories      filter results by the given category ("myWall",
	 *                        "foreignWall", "mission" and/or "community")
	 * @param orderBy         order all results by decending "dec" (newest timestamp
	 *                        first) or ascending "asc" (oldest timestamp first)
	 * @param offset          for choosing at which index to start the fetch. If
	 *                        result list size e.g. 100 and offset is 30, then only
	 *                        results from 30 - limit will be returned. Fetch with
	 *                        "onlyIds" and limit 0 with your other search criteria
	 *                        first, to get a list of all StatusUpdate Ids and fetch
	 *                        the objects then with offset and limit, when needed.
	 *                        For example fetch first 30 and when scrolled to
	 *                        bottom, fetch next 30.
	 * @param limit           in combination with offset, the limit tells how many
	 *                        items to fetch, starting at offset index. 0 = all
	 *                        results
	 * @return a response with the requested list
	 */

	/*
	 * TODO if fetching all status updates, preferebly first fetch just list of
	 * status update ids (onlyIds) and with this list fetch statusupdates with
	 * offset and limit
	 **/
	// TODO test if limit -1 gives all results
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStatusUpdates(@Context ContainerRequestContext context,
			@CookieParam("access_token") Cookie cookie, @QueryParam("userIds[]") List<String> userIds,
			@QueryParam("statusUpdateIds[]") List<String> statusUpdateIds,
			@QueryParam("categories[]") List<String> categories,
			@QueryParam("onlyIds") @DefaultValue("false") Boolean onlyIds,
			@QueryParam("order") @DefaultValue("dec") String orderBy,
			@QueryParam("offset") @DefaultValue("0") Integer offset,
			@QueryParam("limit") @DefaultValue("30") Integer limit) {
		Response response;

		System.out.println(categories);

		if (onlyIds == true) {
			List<String> statusUpdateIdsList = statusUpdateService.getStatusUpdateIds(
					StatusUpdateFilter.filter(statusUpdateIds, userIds, categories), offset, limit, orderBy);
			response = Response.status(Response.Status.OK).entity(statusUpdateIdsList).build();
		} else {
			List<StatusUpdate> statusUpdates = statusUpdateService.getStatusUpdateList(
					StatusUpdateFilter.filter(statusUpdateIds, userIds, categories), offset, limit, orderBy);

			response = getStatusUpdateResponse(statusUpdates, cookie);
		}

		return response;
	}

	/**
	 * Helper function to build a Response from StatusUpdate objects
	 *
	 * @param statusUpdates List of StatusUpdate objects
	 * @return a response with the List of StatusUpdateresponses
	 */
	private Response getStatusUpdateResponse(List<StatusUpdate> statusUpdates, Cookie cookie) {
		List<StatusUpdateResponse> statusUpdateResponses = new LinkedList<>();

		for (StatusUpdate statusUpdate : statusUpdates) {
			String creatorName = this.userSharedService.getUser(statusUpdate.getCreatorId()).username;

			try {
				statusUpdateService.setStatusUpdate(refreshFileIdsInUserIdsWhoInteractedWithPost(statusUpdate, cookie));
			} catch (IOException e) {
				e.printStackTrace();
			}

			StatusUpdateResponse statusUpdateResponse = new StatusUpdateResponse(statusUpdate, creatorName,
					getListOfUserResponseByListOfUserId(statusUpdate.getLikesByUserId()),
					getListOfUserResponseByListOfUserId(statusUpdate.getSharesByUserId()),
					getSocialFeebackResponses(statusUpdate.getSocialFeedbacks()), statusUpdate.getCategory());
			statusUpdateResponses.add(statusUpdateResponse);
		}
		return Response.status(Response.Status.OK).entity(statusUpdateResponses).build();
	}

	private StatusUpdate buildUserIdsWhoInteractedWithPost(StatusUpdate statusupdate, String userId, Cookie cookie)
			throws IOException, JsonParseException, JsonMappingException {
		Map<String, String> userIds = statusupdate.getUserIdsWhoInteractedWithPost();

//        if (!userIds.containsKey(userId)) {
		List<String> userIdList = new ArrayList<>(userIds.keySet());
		userIdList.add(userId);

		// System.out.println("userid: " + userIds);
		Response response = getAvatarFileIdsByUserList(userIdList, cookie);

		userIds = new ObjectMapper().readValue(response.readEntity(String.class), HashMap.class);
		statusupdate.setUserIdsWhoInteractedWithPost(userIds);
//        }
		return statusupdate;
	}

	private StatusUpdate refreshFileIdsInUserIdsWhoInteractedWithPost(StatusUpdate statusupdate, Cookie cookie)
			throws IOException, JsonParseException, JsonMappingException {
		Map<String, String> userIds = statusupdate.getUserIdsWhoInteractedWithPost();

		// System.out.println("userid: " + userIds);
		List<String> userIdList = new ArrayList<>(userIds.keySet());

		Response response = getAvatarFileIdsByUserList(userIdList, cookie);

		// System.out.println("Response: " + response);

		userIds = new ObjectMapper().readValue(response.readEntity(String.class), HashMap.class);
		statusupdate.setUserIdsWhoInteractedWithPost(userIds);

		return statusupdate;
	}

	private StatusUpdate removeUserIdFromMapOfWhoInteractedWithPost(StatusUpdate statusupdate, String userId) {
		Map<String, String> userIds = statusupdate.getUserIdsWhoInteractedWithPost();
		userIds.remove(userId);

		statusupdate.setUserIdsWhoInteractedWithPost(userIds);
		return statusupdate;
	}

	private List<Pair<String, Object>> getUserIdListQueryParam(List<String> userIds) {
		List<Pair<String, Object>> queryParams = new ArrayList<>();
		userIds.forEach(userid -> queryParams.add(Pair.of("userids[]", userid)));

		return queryParams;
	}

	private Response getAvatarFileIdsByUserList(List<String> userIds, Cookie cookie) {
		Response response = StimeyClient.get(StimeyClient.create(), "dashboard", Global.DOCKER, "api/profile/image",
				cookie, getUserIdListQueryParam(userIds));

		return response;
	}

	/**
	 * Helper function to build a List of UserResponse objects out of a List of
	 * userIds
	 *
	 * @param userIds @see ContainerRequestContext
	 * @return a response with the requested list
	 */
	private List<UserResponse> getListOfUserResponseByListOfUserId(List<String> userIds) {
		List<UserResponse> userResponses = new LinkedList<>();

		for (String userId : userIds) {
			String username = this.userSharedService.getUser(userId).username;

			// can be extended with more information later on
			UserResponse userResponse = new UserResponse(userId, username);
			userResponses.add(userResponse);
		}
		return userResponses;
	}

	/**
	 * Helper function to build a List of SocialFeedBackResponse objects recursively
	 * out of a given List<SocialFeedback>
	 *
	 * @param SocialFeedbacks List of SocialFeedback objects
	 * @return a List of SocialFeedbackResponse objects
	 */
	private List<SocialFeedbackResponse> getSocialFeebackResponses(List<SocialFeedback> SocialFeedbacks) {
		List<SocialFeedbackResponse> socialFeedbackResponses = new LinkedList<>();

		for (SocialFeedback socialFeedback : SocialFeedbacks) {
			List<SocialFeedbackResponse> socialFeedbackResponsesTemp;

			if (!socialFeedback.getSocialFeedbacks().isEmpty()) {
				socialFeedbackResponsesTemp = getSocialFeebackResponses(socialFeedback.getSocialFeedbacks());
			} else {
				socialFeedbackResponsesTemp = new LinkedList<>();
			}
			String username = this.userSharedService.getUser(socialFeedback.getCreatorId()).username;
			SocialFeedbackResponse socialFeedbackResponse = new SocialFeedbackResponse(socialFeedback, username,
					getListOfUserResponseByListOfUserId(socialFeedback.getLikesByUserId()),
					socialFeedbackResponsesTemp);
			socialFeedbackResponses.add(socialFeedbackResponse);
		}
		return socialFeedbackResponses;
	}

	/**
	 * Requests the server to post a new StatusUpdate
	 *
	 * @param context @see ContainerRequestContext
	 * @param request StatusUpdateRequest objects which got posted by client
	 * @return Returns the created StatusUpdate as a StautsUpdateResponse objects
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response postStatusUpdate(@Context ContainerRequestContext context, StatusUpdateRequest request,
			@CookieParam("access_token") Cookie cookie) {
		String userid = (String) context.getProperty("userid");
		String textWithParsedMentions = statusUpdateService.parseMentionsInTextToUrl(request.text);
		String textWithParsedKeywords = statusUpdateService.parseKeywordsInTextToUrl(textWithParsedMentions);
		String textWithParsedURLs = statusUpdateService.parseURLsInText(textWithParsedKeywords);
		StatusUpdate statusUpdate = new StatusUpdate(userid, textWithParsedURLs,
				StatusUpdateCategory.fromString(request.category));

		// TODO parse mentions, and add to keywords service. How to display
		// keywords as link? Where is the link
		// pointing to?

		System.out.println(request.category);

		if (request.photoId != null && !request.photoId.equals("")) {
			statusUpdate.setPhotoId(request.photoId);
		}

		if (request.fileId != null && !request.fileId.equals("")) {
			// System.out.println("test " + request.fileId);
			statusUpdate.setFileId(request.fileId);
			statusUpdate.setFileName(request.fileName);
			statusUpdate.setFileDescription(request.fileDescription);
		}

		try {
			statusUpdate = buildUserIdsWhoInteractedWithPost(statusUpdate, userid, cookie);
		} catch (IOException e) {
			e.printStackTrace();
		}

		statusUpdateService.setStatusUpdate(statusUpdate);
		String creatorName = userSharedService.getUser(userid).username;
		StatusUpdateResponse statusUpdateResponse = new StatusUpdateResponse(statusUpdate, creatorName,
				getListOfUserResponseByListOfUserId(statusUpdate.getLikesByUserId()),
				getListOfUserResponseByListOfUserId(statusUpdate.getSharesByUserId()),
				getSocialFeebackResponses(statusUpdate.getSocialFeedbacks()), statusUpdate.getCategory());

		return Response.status(Response.Status.OK).entity(statusUpdateResponse).build();
	}

	/**
	 * Requests the server to return a list with StatusUpdates. The query parameters
	 * influence the list and can be combined.
	 *
	 * @param context        @see ContainerRequestContext
	 * @param statusUpdateId Object Id of the requested StatusUpdate
	 * @return a response with the requested StatusUpdate or StatusUpdates
	 */
	@GET
	@Path("/{statusUpdateId}")
	@Produces(MediaType.APPLICATION_JSON)

	// TODO List<String> statusUpdateIds statt nur einer id. Response ist dann
	// eine liste von StatusUpdates

	public Response getStatusUpdateById(@Context ContainerRequestContext context,
			@CookieParam("access_token") Cookie cookie, @PathParam("statusUpdateId") String statusUpdateId) {
		StatusUpdate statusUpdate = statusUpdateService.getStatusUpdateById(statusUpdateId);
		if (statusUpdate == null)
			return Response.status(Response.Status.NOT_FOUND).build();

		try {
			statusUpdateService.setStatusUpdate(refreshFileIdsInUserIdsWhoInteractedWithPost(statusUpdate, cookie));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String creatorName = this.userSharedService.getUser(statusUpdate.getCreatorId()).username;
		List<SocialFeedbackResponse> socialFeedbackResponses = getSocialFeebackResponses(
				statusUpdate.getSocialFeedbacks());

		StatusUpdateResponse statusUpdateResponse = new StatusUpdateResponse(statusUpdate, creatorName,
				getListOfUserResponseByListOfUserId(statusUpdate.getLikesByUserId()),
				getListOfUserResponseByListOfUserId(statusUpdate.getSharesByUserId()), socialFeedbackResponses,
				statusUpdate.getCategory());

		return Response.status(Response.Status.OK).entity(statusUpdateResponse).build();
	}

	/**
	 * Requests the server to delete a StatusUpdate based on the given Path
	 * Parameter "statusUpdateId"
	 *
	 * @param context        @see ContainerRequestContext
	 * @param statusUpdateId Object Id of the StatusUpdate to be deleted
	 * @return a response with success status if deleted, or unauthorized status if
	 *         requested by a user who didn't create this statusupdate
	 */
	@DELETE
	@Path("/{statusUpdateId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delStatusUpdateById(@Context ContainerRequestContext context,
			@PathParam("statusUpdateId") String statusUpdateId) {
		Response response;
		/*
		 * String userid = (String) context.getProperty("userid"); StatusUpdate
		 * statusUpdate = statusUpdateService.getStatusUpdateById(statusUpdateId);
		 */
		// TODO: UNSECURE WORKAROUND
		// if (statusUpdate.getCreatorId().equals(userid) /*|| moderators*/ ) {
		statusUpdateService.delStatusUpdate(statusUpdateId);
		response = Response.status(Response.Status.OK).entity("").build();
		/*
		 * } else { response =
		 * Response.status(Response.Status.UNAUTHORIZED).entity("").build(); }
		 */
		return response;
	}

	/**
	 * Requests the server to like a StatusUpdate based on the given Path Parameter
	 * "statusUpdateId". If user already liked the StatusUpdate, his like will be
	 * removed
	 *
	 * @param context        @see ContainerRequestContext
	 * @param statusUpdateId Object Id of the StatusUpdate to be deleted
	 * @return a response with the updated StatusUpdate as a StatusUpdateResponse
	 *         object
	 */
	@PUT
	@Path("/like/{statusUpdateId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response likeStatusUpdateById(@Context ContainerRequestContext context,
			@CookieParam("access_token") Cookie cookie, @PathParam("statusUpdateId") String statusUpdateId) {
		String userid = (String) context.getProperty("userid");
		StatusUpdate statusUpdate = statusUpdateService.getStatusUpdateById(statusUpdateId);
		List<String> likesByUserId = statusUpdate.getLikesByUserId();

		handleLike(likesByUserId, userid);

		statusUpdate.setLikesByUserId(likesByUserId);

		if (!userid.equals(statusUpdate.getCreatorId())) {
			// Remove like of own post
			List<String> list = new ArrayList<>(statusUpdate.getLikesByUserId());
			list.remove(statusUpdate.getCreatorId());

			// Only notifications for likes, not for removing a like
			// When list doesnt contain userid it was a like remove
			if (!list.isEmpty() && list.contains(userid)) {
				// Send notification for like
				NotificationClient.sendNotification(statusUpdate.getCreatorId(),
						"statusupdate_like_" + statusUpdate.get_id().toHexString(), list, "notification.post",
						"statusupdate/" + statusUpdate.get_id().toHexString(), "notification.liked", "ICON_Like.png",
						cookie);
			}
		}

		try {
			statusUpdate = buildUserIdsWhoInteractedWithPost(statusUpdate, userid, cookie);
		} catch (IOException e) {
			e.printStackTrace();
		}
		statusUpdateService.setStatusUpdate(statusUpdate);

		String username = this.userSharedService.getUser(statusUpdate.getCreatorId()).username;
		StatusUpdateResponse statusUpdateResponse = new StatusUpdateResponse(statusUpdate, username,
				getListOfUserResponseByListOfUserId(statusUpdate.getLikesByUserId()),
				getListOfUserResponseByListOfUserId(statusUpdate.getSharesByUserId()),
				getSocialFeebackResponses(statusUpdate.getSocialFeedbacks()), statusUpdate.getCategory());

		return Response.status(Response.Status.OK).entity(statusUpdateResponse).build();
	}

	private List<String> handleLike(List<String> likesByUserId, String userid) {
		if (likesByUserId.contains(userid)) {
			likesByUserId.remove(userid);
		} else {
			likesByUserId.add(userid);
//            sendGamificationEvent(userid, GamificationEvents.COMMUNITY_LIKE_SHARE_UPDATE);
		}
		return likesByUserId;
	}

//    /* gamification needs another parameter to check if e.g. a like was already executed. */
//    private void sendGamificationEvent(String userid, int event){
//        Message message = new Message(new ObjectId().toHexString(), userid,
//                event);
//
//        GamificationNotificationResponse notification = GamificationCommunicationService.getService()
//                .getGamificationCommunication().publish(null, message);
//    }

	/**
	 * Requests the server to share a StatusUpdate based on the given Path Parameter
	 * "statusUpdateId". If user already liked the StatusUpdate, his like will be
	 * removed
	 *
	 * @param context        @see ContainerRequestContext
	 * @param statusUpdateId Object Id of the StatusUpdate to be deleted
	 * @return a response with the updated StatusUpdate as a StatusUpdateResponse
	 *         object
	 */
	@PUT
	@Path("/share/{statusUpdateId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response shareStatusUpdateById(@Context ContainerRequestContext context,
			@PathParam("statusUpdateId") String statusUpdateId) {

		// System.out.println("StatusUpdateId: " + statusUpdateId);
		String userid = (String) context.getProperty("userid");
		StatusUpdate statusUpdate = statusUpdateService.getStatusUpdateById(statusUpdateId);
		List<String> sharesByUserId = statusUpdate.getSharesByUserId();

		sharesByUserId.add(userid);

		statusUpdate.setSharesByUserId(sharesByUserId);
		statusUpdateService.setStatusUpdate(statusUpdate);

		String username = this.userSharedService.getUser(statusUpdate.getCreatorId()).username;
		StatusUpdateResponse statusUpdateResponse = new StatusUpdateResponse(statusUpdate, username,
				getListOfUserResponseByListOfUserId(statusUpdate.getLikesByUserId()),
				getListOfUserResponseByListOfUserId(statusUpdate.getSharesByUserId()),
				getSocialFeebackResponses(statusUpdate.getSocialFeedbacks()), statusUpdate.getCategory());

		return Response.status(Response.Status.OK).entity(statusUpdateResponse).build();
	}

	///////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////// SocialFeedback
	// ///////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Requests the server to post a SocialFeedback in a StatusUpdate based on the
	 * statusupdateId.
	 *
	 * @param context             @see ContainerRequestContext
	 * @param statusUpdateId      post new SocialFeedback to the StatusUpdate
	 *                            referenced by the id
	 * @param socialFeedbackIndex post SocialFeedback to an already existing
	 *                            SocialFeedback. SocialFeedbacks are indexed from 0
	 *                            to n based of the index in the
	 *                            "List<SocialFeedback> socialfeedbacks" of
	 *                            StatusUpdate class. If left empty the function
	 *                            just creates a new SocialFeedback in StatusUpdate
	 * @return a response with the updated StatusUpdate
	 */
	@POST
	@Path("/socialfeedback/{statusUpdateId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	// TODO nicht mit queryparam....
	public Response postSocialFeedback(@Context ContainerRequestContext context,
			@CookieParam("access_token") Cookie cookie, @PathParam("statusUpdateId") String statusUpdateId,
			@QueryParam("index") @DefaultValue("-1") int socialFeedbackIndex,
			@QueryParam("childIndex") @DefaultValue("-1") int socialFeedbackChildIndex, String socialFeedbackText) {
		// System.out.println(socialFeedbackText);
		Response response;
		String userId = (String) context.getProperty("userid");
		System.out.println(socialFeedbackText);

		StatusUpdate statusUpdate = statusUpdateService.getStatusUpdateById(statusUpdateId);
		List<SocialFeedback> socialFeedbacks = statusUpdate.getSocialFeedbacks();

		String textWithParsedMentions = statusUpdateService.parseMentionsInTextToUrl(socialFeedbackText);
		String textWithParsedKeywords = statusUpdateService.parseKeywordsInTextToUrl(textWithParsedMentions);
		String textWithParsedURLs = statusUpdateService.parseURLsInText(textWithParsedKeywords);

		SocialFeedback socialFeedback = new SocialFeedback(userId, textWithParsedURLs);

		if ((socialFeedbacks.size() - 1) < socialFeedbackIndex) {
			response = Response.status(Response.Status.BAD_REQUEST).entity("Index out of range").build();
		} else {
			if (socialFeedbackIndex < 0) {
				// new comment

				socialFeedbacks.add(socialFeedback);

				// No notification for creating own comments
				if (!userId.equals(statusUpdate.getCreatorId())) {

					// IDs of the users commented this statusupdate
					Set<String> commenterSet = socialFeedbacks.stream().map(feedback -> feedback.getCreatorId())
							.collect(Collectors.toSet());

					// Remove creator id
					commenterSet.remove(statusUpdate.getCreatorId());

					NotificationClient.sendNotification(statusUpdate.getCreatorId(),
							"statusupdate_comment_" + statusUpdate.get_id().toHexString(), commenterSet,
							"notification.post", "statusupdate/" + statusUpdate.get_id().toHexString(),
							"notification.commented", "ICON_Comments.png", cookie);
				}

			} else {
				// comment comment
				// index in request is either 1 or 0, a real index is needed to identify the
				// post the got commented

				// System.out.println("comment comment");

				socialFeedbacks.get(socialFeedbackIndex).getSocialFeedbacks().add(socialFeedback);

				SocialFeedback feedback;

				if (socialFeedbackChildIndex >= 0) {
					feedback = statusUpdate.getSocialFeedbacks().get(socialFeedbackIndex).getSocialFeedbacks()
							.get(socialFeedbackChildIndex);
				} else {
					feedback = socialFeedbacks.get(socialFeedbackIndex);
				}

				// Notification for like of a comment of a comment
				if (!userId.equals(feedback.getCreatorId())) {

					// There is no possiblity to detect the users who commmented a comment
					// Remove like of own post
					// List<String> list = new ArrayList<>(feedback.getLikesByUserId());
					List<String> list = new ArrayList<>();
					list.add(userId);

					// Send notification for like of a comment of a comment
					NotificationClient.sendNotification(feedback.getCreatorId(),
							"statusupdate_feedback_comment_" + feedback.get_id().toHexString(), list,
							"notification.comment", "statusupdate/" + statusUpdate.get_id().toHexString(),
							"notification.commented", "ICON_Comments.png", cookie);
				}
			}

			statusUpdate.setSocialFeedbacks(socialFeedbacks);
			try {
				statusUpdate = buildUserIdsWhoInteractedWithPost(statusUpdate, userId, cookie);
			} catch (IOException e) {
				e.printStackTrace();
			}
			statusUpdateService.setStatusUpdate(statusUpdate);

			StatusUpdateResponse statusUpdateResponse = new StatusUpdateResponse(statusUpdate,
					this.userSharedService.getUser(statusUpdate.getCreatorId()).username,
					getListOfUserResponseByListOfUserId(statusUpdate.getLikesByUserId()),
					getListOfUserResponseByListOfUserId(statusUpdate.getSharesByUserId()),
					getSocialFeebackResponses(statusUpdate.getSocialFeedbacks()), statusUpdate.getCategory());

			response = Response.status(Response.Status.OK).entity(statusUpdateResponse).build();
		}
		return response;
	}

	// TODO refactor this garbage code
	@DELETE
	@Path("/socialfeedback/{statusupdateid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delSocialFeedbackById(@Context ContainerRequestContext context,
			@PathParam("statusupdateid") String statusUpdateId,
			@QueryParam("index") @DefaultValue("-1") int socialFeedbackIndex,
			@QueryParam("childIndex") @DefaultValue("-1") int socialFeedbackChildIndex) {
		Response response;
		// String userid = (String) context.getProperty("userid");
		StatusUpdate statusUpdate = statusUpdateService.getStatusUpdateById(statusUpdateId);
		boolean valid = true;

		if (socialFeedbackChildIndex >= 0) {
			String creatorId = statusUpdate.getSocialFeedbacks().get(socialFeedbackIndex).getSocialFeedbacks()
					.get(socialFeedbackIndex).getCreatorId();

			// TODO: UNSECURE WORKAROUND
			// if (!(creatorId.equals(userid))) {
			// valid = false;
			// } else {
			statusUpdate.getSocialFeedbacks().get(socialFeedbackIndex).getSocialFeedbacks()
					.remove(socialFeedbackChildIndex);
			// }

		} else if (socialFeedbackIndex >= 0) {
			// TODO: UNSECURE WORKAROUND
			// if
			// (!(statusUpdate.getSocialFeedbacks().get(socialFeedbackIndex).getCreatorId().equals(userid)))
			// {
			// valid = false;
			// } else {
			statusUpdate.getSocialFeedbacks().remove(socialFeedbackIndex);
			// }
		} else {
			valid = false;
		}

		if (valid) {
			statusUpdateService.setStatusUpdate(statusUpdate);
			String username = this.userSharedService.getUser(statusUpdate.getCreatorId()).username;
			StatusUpdateResponse statusUpdateResponse = new StatusUpdateResponse(statusUpdate, username,
					getListOfUserResponseByListOfUserId(statusUpdate.getLikesByUserId()),
					getListOfUserResponseByListOfUserId(statusUpdate.getSharesByUserId()),
					getSocialFeebackResponses(statusUpdate.getSocialFeedbacks()), statusUpdate.getCategory());
			response = Response.status(Response.Status.OK).entity(statusUpdateResponse).build();
		} else {
			response = Response.status(Response.Status.BAD_REQUEST).entity("").build();
		}
		return response;

	}

	// TODO refactor (combine with StatusUpdate like endpoint) and testing

	/**
	 * Requests the server to like a SocialFeedback based on the given Path
	 * Parameter "statusUpdateId" and the QueryParams to tell which SocialFeedback
	 * to like If user already liked the SocialFeedback, his like will be removed
	 *
	 * @param context                  @see ContainerRequestContext
	 * @param statusUpdateId           Object Id of the StatusUpdate to be deleted
	 * @param socialFeedbackIndex      index of first level SocialFeedback. 0 - n
	 * @param socialFeedbackChildIndex index of second level SocialFeedback
	 *                                 (Feedback to a Feedback). 0 - n
	 * @return a response with the updated StatusUpdate as a StatusUpdateResponse
	 *         object
	 */
	@PUT
	@Path("/like/socialfeedback/{statusUpdateId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response likeSocialFeedbackById(@Context ContainerRequestContext context,
			@CookieParam("access_token") Cookie cookie, @PathParam("statusUpdateId") String statusUpdateId,
			@QueryParam("index") @DefaultValue("-1") int socialFeedbackIndex,
			@QueryParam("childIndex") @DefaultValue("-1") int socialFeedbackChildIndex) {
		String userid = (String) context.getProperty("userid");
		StatusUpdate statusUpdate = statusUpdateService.getStatusUpdateById(statusUpdateId);
		List<String> likesByUserId;
		boolean valid = true;
		Response response;

		if (socialFeedbackChildIndex >= 0) {
			likesByUserId = statusUpdate.getSocialFeedbacks().get(socialFeedbackIndex).getSocialFeedbacks()
					.get(socialFeedbackChildIndex).getLikesByUserId();

			handleLike(likesByUserId, userid);

			statusUpdate.getSocialFeedbacks().get(socialFeedbackIndex).getSocialFeedbacks()
					.get(socialFeedbackChildIndex).setLikesByUserId(likesByUserId);

			SocialFeedback feedback = statusUpdate.getSocialFeedbacks().get(socialFeedbackIndex).getSocialFeedbacks()
					.get(socialFeedbackChildIndex);

			// Notification for like of a comment of a comment
			if (!userid.equals(feedback.getCreatorId())) {
				// Remove like of own post
				List<String> list = new ArrayList<>(feedback.getLikesByUserId());
				list.remove(feedback.getCreatorId());

				if (!list.isEmpty() && list.contains(userid)) {
					// Send notification for like of a comment of a comment
					NotificationClient.sendNotification(feedback.getCreatorId(),
							"statusupdate_feedback_like_" + feedback.get_id().toHexString(), list,
							"notification.comment", "statusupdate/" + statusUpdate.get_id().toHexString(),
							"notification.liked", "ICON_Like.png", cookie);
				}
			}

		} else if (socialFeedbackIndex >= 0) {
			likesByUserId = statusUpdate.getSocialFeedbacks().get(socialFeedbackIndex).getLikesByUserId();
			handleLike(likesByUserId, userid);

			statusUpdate.getSocialFeedbacks().get(socialFeedbackIndex).setLikesByUserId(likesByUserId);

			SocialFeedback feedback = statusUpdate.getSocialFeedbacks().get(socialFeedbackIndex);

			// Notification for like of a comment
			if (!userid.equals(feedback.getCreatorId())) {
				// Remove like of own post
				List<String> list = new ArrayList<>(feedback.getLikesByUserId());
				list.remove(feedback.getCreatorId());

				if (!list.isEmpty() && list.contains(userid)) {
					// Send notification for like of a comment
					NotificationClient.sendNotification(feedback.getCreatorId(),
							"statusupdate_feedback_like_" + feedback.get_id().toHexString(), list,
							"notification.comment", "statusupdate/" + statusUpdate.get_id().toHexString(),
							"notification.liked", "ICON_Like.png", cookie);
				}
			}

		} else {
			valid = false;
		}

		if (valid) {

			try {
				statusUpdate = buildUserIdsWhoInteractedWithPost(statusUpdate, userid, cookie);
			} catch (IOException e) {
				e.printStackTrace();
			}
			statusUpdateService.setStatusUpdate(statusUpdate);

			String username = this.userSharedService.getUser(statusUpdate.getCreatorId()).username;
			StatusUpdateResponse statusUpdateResponse = new StatusUpdateResponse(statusUpdate, username,
					getListOfUserResponseByListOfUserId(statusUpdate.getLikesByUserId()),
					getListOfUserResponseByListOfUserId(statusUpdate.getSharesByUserId()),
					getSocialFeebackResponses(statusUpdate.getSocialFeedbacks()), statusUpdate.getCategory());
			response = Response.status(Response.Status.OK).entity(statusUpdateResponse).build();
		} else {
			response = Response.status(Response.Status.BAD_REQUEST).entity("").build();
		}

		return response;
	}
}
