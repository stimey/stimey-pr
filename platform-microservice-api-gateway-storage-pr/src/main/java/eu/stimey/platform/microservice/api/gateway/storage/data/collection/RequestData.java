package eu.stimey.platform.microservice.api.gateway.storage.data.collection;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestData {
	public String username;
	public String usertype;

	public String method;
	public String requestURI;

	public Map<String, List<String>> headers;
	public Map<String, String[]> parameters;
	public String content;

	public RequestData() {
		super();

		headers = new HashMap<>();
		parameters = new HashMap<>();
	}

	public RequestData(String username, String usertype, String method, String requestURI,
			Map<String, List<String>> headers, Map<String, String[]> parameters, String content) {
		super();
		this.username = username;
		this.usertype = usertype;
		this.method = method;
		this.requestURI = requestURI;
		this.headers = headers;
		this.parameters = parameters;
		this.content = content;
	}

	@Override
	public String toString() {
		return "RequestData [username=" + username + ", usertype=" + usertype + ", method=" + method + ", requestURI="
				+ requestURI + ", headers=" + headers + ", parameters=" + parameters + ", content=" + content + "]";
	}
}
