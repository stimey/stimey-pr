package eu.stimey.platform.microservice.activitystream.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.activitystream.databind.SameCourses;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.List;

public class SameCoursesService {
	private Caching<String, SameCourses> cache;

	public SameCoursesService() {
		super();

		this.cache = new Caching<>("ms_activitystream", "_id", SameCourses.class, "same_courses",
				GlobalVariables.CACHE_CONFIG);
		this.cache.setWorkaround_keyMapper((key) -> new ObjectId(key));
	}

	public int getInteractionbyId(String interactionId) {
		int amountInteractions = 0;

		List<String> result = this.cache.find(new Document("uniqueID", interactionId), null, 0, 1,
				(r) -> String.valueOf(r.relation_count), true);

		if (result.size() == 0)
			return 0;
		else if (result.get(0).length() > 0)
			amountInteractions = Integer.valueOf(result.get(0));

		return amountInteractions;
	}
}
