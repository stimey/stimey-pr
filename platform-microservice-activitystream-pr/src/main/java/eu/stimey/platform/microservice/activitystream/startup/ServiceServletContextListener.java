package eu.stimey.platform.microservice.activitystream.startup;

import java.util.*;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import eu.stimey.platform.library.utils.StimeyLogger;
import eu.stimey.platform.microservice.activitystream.beans.SchedulerService;
import eu.stimey.platform.microservice.activitystream.client.Application;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;

/**
 * Global web application settings
 * 
 */
@WebListener
public class ServiceServletContextListener implements ServletContextListener {
	private SchedulerService schedulerService;

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		Global.startup();
		StimeyLogger.setApplicationName(servletContextEvent.getServletContext().getContextPath());

		MongoDBService.start("ms_activitystream");
		RedisAMQPService.start();

		if (!MongoDBService.getService().getClient().getDatabase(MongoDBService.getService().getDatabaseName())
				.listCollectionNames().into(new ArrayList<String>()).contains("activitystream"))
			new Application().execute();

		schedulerService = new SchedulerService();
		schedulerService.startScheduler();
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		schedulerService.getScheduler().shutdown();
		RedisAMQPService.stop();
		MongoDBService.stop();
	}
}
