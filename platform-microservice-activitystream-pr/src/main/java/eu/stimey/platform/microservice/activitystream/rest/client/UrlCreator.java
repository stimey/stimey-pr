package eu.stimey.platform.microservice.activitystream.rest.client;

import java.util.HashMap;

public class UrlCreator {
	private HashMap<String, String> serviceList = new HashMap<>();

	public UrlCreator() {
		this.serviceList.put("auth", "/platform-microservice-auth");
		this.serviceList.put("dashboard", "/platform-microservice-dashboard");
		this.serviceList.put("courses", "/platform-microservice-courses");
		this.serviceList.put("chat", "platform-microservice-chat");
		this.serviceList.put("files", "/platform-microservice-files");
		this.serviceList.put("activitystream", "/platform-microservice-activitystream");
		this.serviceList.put("template", "/prototype2-microservice-web-template");
		this.serviceList.put("achievements", "/platform-microservice-achievement");
		this.serviceList.put("communities", "/platform-microservice-communities");
		this.serviceList.put("lab", "/platform-microservice-lab");
		this.serviceList.put("badges", "/platform-microservice-badges");
		this.serviceList.put("planets", "/platform-microservice-planets");
		this.serviceList.put("collector", "/platform-microservice-collector");
		this.serviceList.put("api-gateway-storage", "/platform-microservice-api-gateway-storage");
		this.serviceList.put("scientix", "http://resources.scientix.eu");
	}

	public String getUrlWithoutApiPart(String neededMicroservice) {
		if (this.serviceList.containsKey(neededMicroservice) && System.getenv("STIMEY_DOCKER") == null)
			return serviceList.get(neededMicroservice);
		else
			return neededMicroservice;
	}

	public String getDomain() {
		String domain;

		if (System.getenv("STIMEY_DOCKER") != null) {
			domain = System.getenv("STIMEY_DOMAIN");
			if (domain.equals("firebird"))
				domain = "https://" + domain + "/api-gateway/api-proxy/";
			else
				domain += "/api-gateway/api-proxy/";
		} else {
			domain = "http://localhost:8080";
		}

		return domain;
	}
}
