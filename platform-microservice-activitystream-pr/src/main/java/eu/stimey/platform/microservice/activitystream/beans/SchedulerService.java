package eu.stimey.platform.microservice.activitystream.beans;

import eu.stimey.platform.library.activitystream.UserRelationCalc;
import eu.stimey.platform.library.core.startup.MongoDBService;

import javax.annotation.PreDestroy;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Timer/Scheduler Service
 *
 */

public class SchedulerService {
	protected ScheduledExecutorService scheduler;

	public SchedulerService() {
		scheduler = Executors.newScheduledThreadPool(1);
	}

	@PreDestroy
	public void preDestroy() {
		scheduler.shutdown();
	}

	public ScheduledExecutorService getScheduler() {
		return scheduler;
	}

	/**
	 * Converts an absolute time in a relative time. The date is checked if it is in
	 * the future. It returns -1 if the date is invalid, else it returns the
	 * relative time in milliseconds.
	 *
	 * This function is necessary to calculate when a task should be started (the
	 * relative time is needed).
	 *
	 * @param date Absolute time.
	 * @return Time Relative time in milliseconds.
	 */
	public long absoluteInRelative(Date date) {
		long result = -1;

		Date now = new Date();
		if (date.after(now)) {
			result = date.getTime() - now.getTime();
		}

		return result;
	}

	public void startScheduler() {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Berlin"));
		calendar.add(Calendar.SECOND, 0);

		UserRelationCalc urc = new UserRelationCalc(MongoDBService.getService().getClient());

		int hours = 24;
		int periodInSeconds = hours * 60 * 60;

		// TEST: Remove the next line for 24h-Scheduling
		// periodInSeconds = 10;

		// command: command to execute
		// initialDelay: time from now in TimeUnit -> see last parameter
		// period: period
		// time
		this.scheduler.scheduleAtFixedRate(() -> urc.calculateFriendsAndCoursesRelations(),
				this.absoluteInRelative(calendar.getTime()) / 1000, periodInSeconds, TimeUnit.SECONDS);
	}
}
