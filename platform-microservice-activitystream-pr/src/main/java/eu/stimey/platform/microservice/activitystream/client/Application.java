package eu.stimey.platform.microservice.activitystream.client;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;

public class Application {
	public static void main(String[] args) {
		MongoDBService.start("ms_activitystream");
		RedisAMQPService.start();

		MongoDBService.getService().getClient().dropDatabase(MongoDBService.getService().getDatabaseName());
		// new Application().execute();

		RedisAMQPService.stop();
		MongoDBService.stop();
	}

	public void execute() {

	}
}
