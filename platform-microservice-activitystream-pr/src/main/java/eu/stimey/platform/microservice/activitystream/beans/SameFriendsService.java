package eu.stimey.platform.microservice.activitystream.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.activitystream.databind.SameFriends;
import org.bson.Document;
import org.bson.types.ObjectId;

import eu.stimey.platform.library.activitystream.UserRelationCalc;

import java.util.List;

public class SameFriendsService {
	private Caching<String, SameFriends> sameFriendsCache;

	public SameFriendsService() {
		super();

		this.sameFriendsCache = new Caching<>("ms_activitystream", "_id", SameFriends.class, "same_friends",
				GlobalVariables.CACHE_CONFIG);
		this.sameFriendsCache.setWorkaround_keyMapper((key) -> new ObjectId(key));
	}

	public int getInteractionbyId(String interactionId) {
		int amountInteractions = 0;

		List<String> result = this.sameFriendsCache.find(new Document("uniqueID", interactionId), null, 0, 1,
				(r) -> String.valueOf(r.relation_count), true);
		if (result.size() == 0)
			return 0;
		else if (result.get(0).length() > 0)
			amountInteractions = Integer.valueOf(result.get(0));

		return amountInteractions;
	}
}
