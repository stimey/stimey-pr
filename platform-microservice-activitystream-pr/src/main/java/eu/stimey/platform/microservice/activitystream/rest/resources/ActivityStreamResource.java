package eu.stimey.platform.microservice.activitystream.rest.resources;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import eu.stimey.platform.library.activitystream.UserRelationCalc;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.microservice.activitystream.rest.client.DashboardClient;
import eu.stimey.platform.microservice.activitystream.rest.client.StatusupdateClient;

import java.util.ArrayList;
import java.util.List;

@Path("/")
public class ActivityStreamResource {
	private StatusupdateClient statusupdateClient;
	private DashboardClient dashboardClient;

	public ActivityStreamResource(@CookieParam("access_token") Cookie cookie) {
		this.dashboardClient = new DashboardClient();
		this.dashboardClient.setCookie(cookie);

		this.statusupdateClient = new StatusupdateClient();
		this.statusupdateClient.setCookie(cookie);
	}

	private List<String> combineFriends(String ownUserId) {
		List<String> showStatusupdateIds = new ArrayList<>();

		List<String> ownFriends = this.dashboardClient.getOwnFriends();
		List<String> following = this.dashboardClient.getFollowing();

		try {
			showStatusupdateIds.addAll(ownFriends);
			showStatusupdateIds.addAll(following);
		} catch (NullPointerException e) {
			// No friends, poor man! Catch this exception and get lucky :)
		}

		showStatusupdateIds.add(ownUserId);

		return showStatusupdateIds;
	}

	@GET
	@Path("/user/relevance")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getActivityStream(@Context ContainerRequestContext requestContext) {
		String ownUserId = (String) requestContext.getProperty("userid");
		String username = (String) requestContext.getProperty("username");

		this.dashboardClient.setUsername(username);
		this.statusupdateClient.setOwnUserId(ownUserId);
		this.statusupdateClient.setUserList(this.combineFriends(ownUserId));

		return Response.status(Status.OK).entity(this.statusupdateClient.getStatusupdatesByRelevance()).build();
	}

	@GET
	@Path("/user/chronological")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getActivityStreamChronological(@Context ContainerRequestContext requestContext,
			@CookieParam("access_token") Cookie cookie) {
		this.dashboardClient.setUsername((String) requestContext.getProperty("username"));
		String ownUserId = (String) requestContext.getProperty("userid");

		this.statusupdateClient.setUserList(this.combineFriends(ownUserId));

		return Response.status(Status.OK).entity(this.statusupdateClient.getStatusupdatesChronological()).build();
	}

	@GET
	@Path("/cron")
	@Produces(MediaType.APPLICATION_JSON)
	public Response periodically(@Context ContainerRequestContext requestContext) {
		UserRelationCalc urc = new UserRelationCalc(MongoDBService.getService().getClient());

		urc.calculateFriendsAndCoursesRelations();

		return Response.status(Status.OK).entity("ok").build();
	}
}
