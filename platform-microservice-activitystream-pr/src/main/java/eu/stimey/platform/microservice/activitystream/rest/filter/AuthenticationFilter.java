package eu.stimey.platform.microservice.activitystream.rest.filter;

import eu.stimey.platform.library.rest.filter.DefaultAuthenticationFilter;

import javax.ws.rs.ext.Provider;

/**
 * AuthenticationFilter for REST, checks for allowed access (successful authentication)
 *
 */
@Provider
public class AuthenticationFilter extends DefaultAuthenticationFilter {

}