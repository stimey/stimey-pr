package eu.stimey.platform.microservice.activitystream.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Turning off HTTP session (Stateless microservice)
 *
 */
public class StatelessFilter implements Filter {
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		chain.doFilter(new HttpServletRequestWrapper((HttpServletRequest) request) {
			@Override
			public HttpSession getSession() {
				return null;
			}

			@Override
			public HttpSession getSession(boolean create) {
				return null;
			}
		}, response);
	}

	@Override
	public void destroy() {
	}
}
