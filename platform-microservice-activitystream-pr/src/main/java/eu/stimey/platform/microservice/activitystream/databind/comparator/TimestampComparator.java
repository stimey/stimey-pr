package eu.stimey.platform.microservice.activitystream.databind.comparator;

import java.util.Comparator;
import java.util.LinkedHashMap;

public class TimestampComparator implements Comparator<LinkedHashMap> {
	public TimestampComparator() {
		super();
	}

	@Override
	public int compare(LinkedHashMap first, LinkedHashMap second) {
		return Long.compare(Long.valueOf(second.get("timestamp").toString()),
				Long.valueOf(first.get("timestamp").toString()));
	}
}
