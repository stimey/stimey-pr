package eu.stimey.platform.microservice.activitystream.rest.client;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.microservice.activitystream.startup.Global;
import org.apache.commons.lang3.tuple.Pair;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class DashboardClient {
	private LinkedHashMap<String, Object> response;
	private Cookie cookie;
	private String username;

	public DashboardClient() {
		this.response = new LinkedHashMap<>();
	}

	public void setCookie(Cookie cookie) {
		this.cookie = cookie;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@SuppressWarnings(value = "unchecked")
	private void getJsonData() {
		if (this.response.size() != 0)
			return;

		List<Pair<String, Object>> query = new ArrayList<>();
		query.add(Pair.of("username", this.username));

		Client client = StimeyClient.create();
		Response response = StimeyClient.get(client, "dashboard", Global.DOCKER, "api/profile/user", cookie, query);

		response.bufferEntity();

		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			this.response = mapper.readValue(response.readEntity(String.class), LinkedHashMap.class);
		} catch (IOException | NullPointerException e) {
			this.response = new LinkedHashMap<>();
		}
	}

	@SuppressWarnings(value = "unchecked")
	public List<String> getOwnFriends() {
		this.getJsonData();

		try {
			return (ArrayList<String>) this.response.get("friends");
		} catch (NullPointerException e) {
			return new ArrayList<>();
		}
	}

	@SuppressWarnings(value = "unchecked")
	public List<String> getFollowing() {
		this.getJsonData();

		try {
			return (ArrayList<String>) this.response.get("following");
		} catch (NullPointerException e) {
			return new ArrayList<>();
		}
	}
}
