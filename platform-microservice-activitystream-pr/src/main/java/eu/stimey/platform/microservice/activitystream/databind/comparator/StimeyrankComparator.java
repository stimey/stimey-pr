package eu.stimey.platform.microservice.activitystream.databind.comparator;

import java.util.Comparator;
import java.util.LinkedHashMap;

public class StimeyrankComparator implements Comparator<LinkedHashMap> {
	public StimeyrankComparator() {
		super();
	}

	@Override
	public int compare(LinkedHashMap first, LinkedHashMap second) {
		long firstTimestamp = Long.valueOf(first.get("timestamp").toString());
		long secondTimestamp = Long.valueOf(second.get("timestamp").toString());

		if (Long.valueOf(first.get("newestTimestamp").toString()) != 0)
			firstTimestamp = Long.valueOf(first.get("newestTimestamp").toString());

		if (Long.valueOf(second.get("newestTimestamp").toString()) != 0)
			secondTimestamp = Long.valueOf(second.get("newestTimestamp").toString());

		if (first.get("stimeyRankPoints").equals(second.get("stimeyRankPoints"))) {
			if (firstTimestamp < secondTimestamp)
				return 1;
			else
				return -1;
		} else
			return Long.compare(Long.valueOf(second.get("stimeyRankPoints").toString()),
					Long.valueOf(first.get("stimeyRankPoints").toString()));
	}
}
