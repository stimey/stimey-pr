package eu.stimey.platform.microservice.activitystream.rest.client;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.*;
import java.util.List;

import eu.stimey.platform.library.activitystream.StimeyRankImplementation;
import eu.stimey.platform.library.utils.StimeyLogger;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.microservice.activitystream.beans.SameCoursesService;
import eu.stimey.platform.microservice.activitystream.beans.SameFriendsService;
import eu.stimey.platform.microservice.activitystream.databind.comparator.StimeyrankComparator;
import eu.stimey.platform.microservice.activitystream.databind.comparator.TimestampComparator;
import eu.stimey.platform.microservice.activitystream.startup.Global;
import org.apache.commons.lang3.tuple.Pair;

public class StatusupdateClient {
	private Cookie cookie;
	private List<String> friends;
	private List<LinkedHashMap> response;
	private String ownUserId;

	public StatusupdateClient() {
	}

	public List<LinkedHashMap> getStatusupdatesChronological() {
		this.readStatusupdate();
		this.response.sort(new TimestampComparator());

		return this.response;
	}

	@SuppressWarnings(value = "unchecked")
	public List<LinkedHashMap> getStatusupdatesByRelevance() {
		List<LinkedHashMap> newResponse = new ArrayList<>();
		SameFriendsService sfs = new SameFriendsService();
		SameCoursesService scs = new SameCoursesService();
		StimeyRankImplementation sri = new StimeyRankImplementation();
		String combinedUserId;

		this.readStatusupdate();

		for (LinkedHashMap msg : this.response) {
			sri.resetValues();
			combinedUserId = sri.getCombinedIdFromUsers(this.ownUserId, (String) msg.get("creatorId"));

			if (combinedUserId != null) {
				sri.setSameCourses(sfs.getInteractionbyId(combinedUserId));
				sri.setSameFriends(scs.getInteractionbyId(combinedUserId));
				sri.setSameCommunities(0);
			}

			int activityPoints = this.calcActivityPoints(sri, msg);
			msg.put("activityPoints", activityPoints);

			msg.put("stimeyRankPoints", sri.calculateStimeyRank(sri.calculateUserAffinityPoints(),
					this.calcTimelinePoints(sri, msg), activityPoints));

			newResponse.add(msg);
		}

		newResponse.sort(new StimeyrankComparator());

		return newResponse;
	}

	/**
	 * SuppressWarnings:
	 *
	 * Neccessary, otherwise iterate through all LinkedHashMap's and cast included
	 * objects to string, objects and maps. Whats more important: Type safety or
	 * faster runtime? It's your turn!
	 */
	@SuppressWarnings(value = "unchecked")
	private void readStatusupdate() {
		List<Pair<String, Object>> queryParam = new ArrayList<>();

		for (String friendId : this.friends)
			queryParam.add(Pair.of("userIds[]", friendId));

		Client client = StimeyClient.create();
		Response response = StimeyClient.get(client, "api-gateway-storage", Global.DOCKER, "api/statusupdate", cookie,
				queryParam);

		response.bufferEntity();

		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			this.response = mapper.readValue(response.readEntity(String.class), ArrayList.class);
		} catch (IOException e) {
			e.printStackTrace();
			this.response = new ArrayList<>();
		}
	}

	@SuppressWarnings(value = "unchecked")
	private int calcTimelinePoints(StimeyRankImplementation sri, LinkedHashMap msg) {
		long newestTs = (long) msg.get("timestamp");

		ArrayList<LinkedHashMap> feedbacks = (ArrayList<LinkedHashMap>) msg.get("socialFeedbacks");
		if (feedbacks.size() != 0)
			for (LinkedHashMap feedback : feedbacks) {
				long tsInFeedback = (long) feedback.get("timestamp");

				if (newestTs < tsInFeedback) {
					newestTs = tsInFeedback;
				}
			}

		msg.put("newestTimestamp", newestTs);
		return sri.calcTimestampVal(new Date(newestTs));
	}

	@SuppressWarnings(value = "unchecked")
	private int calcActivityPoints(StimeyRankImplementation sri, LinkedHashMap msg) {
		ArrayList<String> likes = (ArrayList) msg.get("likesByUser");
		int amountLikes = likes.size();
		ArrayList<LinkedHashMap> feedbacks = (ArrayList<LinkedHashMap>) msg.get("socialFeedbacks");

		if (feedbacks.size() != 0)
			for (LinkedHashMap feedback : feedbacks) {

				likes = (ArrayList) feedback.get("likesByUser");
				amountLikes += likes.size();
			}

		sri.setSumLikes(amountLikes);
		sri.setSumComments(feedbacks.size());

		return sri.calculateActivityContentPoints();
	}

	public void setUserList(List<String> friends) {
		this.friends = friends;
	}

	public void setOwnUserId(String id) {
		this.ownUserId = id;
	}

	public void setCookie(Cookie cookie) {
		this.cookie = cookie;
	}
}
