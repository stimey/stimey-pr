package eu.stimey.platform.microservice.api.gateway.rest.databind;

public class RobotRequest {
	protected String commands;

	public RobotRequest() {
		super();
	}

	public String getCommands() {
		return commands;
	}

	public void setCommands(String commands) {
		this.commands = commands;
	}

	@Override
	public String toString() {
		return "RobotRequest [commands=" + commands + "]";
	}
}
