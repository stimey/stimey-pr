package eu.stimey.platform.microservice.api.gateway.rest.resources;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.tuple.Pair;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;
import eu.stimey.platform.library.utils.rest.RESTResponse;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.library.utils.security.JsonWebToken;
import eu.stimey.platform.microservice.api.gateway.startup.Global;

@Path("/v1/notification")
public class NotificationResource {
	@Inject
	UserSharedService userSharedService;

	@SuppressWarnings("unchecked")
	@GET
	@Path("{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response notfications(@Context HttpServletRequest request,
			@PathParam("userid") String userid) {
		String retrieveStart = request.getParameter("start");
		String retrieveCount = request.getParameter("count");
		String retrieveNew = request.getParameter("new");
		if (retrieveStart == null || retrieveCount == null || retrieveNew == null)
			return Response.status(400)
					.entity(new RESTResponse(RESTResponse.ERROR, 400, null, "The request was error prone.")).build();

		UserSharedCacheObject userSharedCacheObject = userSharedService.getUser(userid);
		if (userSharedCacheObject == null)
			return Response.status(404)
					.entity(new RESTResponse(RESTResponse.NO_SUCCESS, 404, null, "The user was not found.")).build();

		Cookie cookie = JsonWebToken.createCookie(userid, userSharedCacheObject.username,
				userSharedCacheObject.usertype, userSharedCacheObject.usersubtype, Global.getPrivateKey(),
				Global.DOMAIN);
		Client client = StimeyClient.create();
		List<Pair<String, Object>> queryParams = new LinkedList<>();
		queryParams.add(Pair.of("start", retrieveStart));
		queryParams.add(Pair.of("count", retrieveCount));
		queryParams.add(Pair.of("new", retrieveNew));
		Response response = StimeyClient.get(client, "notification", Global.DOCKER, "api/notification/retrieve", cookie,
				queryParams);

		if (response != null) {
			if (response.getStatus() == 200) {
				response.bufferEntity();

				List<Object> data = null;
				try {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
					data = mapper.readValue(response.readEntity(String.class), List.class);
				} catch (Exception e) {
					e.printStackTrace();
				}

				return Response.status(200).entity(new RESTResponse(RESTResponse.SUCCESS, 200, data, "")).build();
			} else
				return Response.status(response.getStatus()).entity(
						new RESTResponse(RESTResponse.ERROR, response.getStatus(), null, "Something went wrong."))
						.build();
		} else
			return Response.status(502).entity(
					new RESTResponse(RESTResponse.ERROR, 502, null, "The request could not handled successfully."))
					.build();
	}
}
