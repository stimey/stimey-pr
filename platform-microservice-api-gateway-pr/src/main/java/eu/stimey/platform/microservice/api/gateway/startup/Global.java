package eu.stimey.platform.microservice.api.gateway.startup;

import eu.stimey.platform.library.core.startup.DefaultGlobal;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import redis.clients.jedis.Jedis;

/**
 * 
 * Global settings (environment variables)
 *
 */
public final class Global extends DefaultGlobal {
	private static String PRIVATE_KEY = null;
	private static Object lock = new Object();

	public static String getPrivateKey() {
		String temp = PRIVATE_KEY;
		if (temp == null) {
			synchronized (lock) {
				temp = PRIVATE_KEY;
				if (temp == null) {
					Jedis jedis = RedisAMQPService.getService().getJedisPool().getResource();
					PRIVATE_KEY = temp = jedis.get("shared_global-private_key");
					jedis.close();
				}
			}
		}

		return temp;
	}
}
