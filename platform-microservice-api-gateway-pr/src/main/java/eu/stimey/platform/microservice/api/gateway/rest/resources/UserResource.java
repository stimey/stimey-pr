package eu.stimey.platform.microservice.api.gateway.rest.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.Document;
import org.bson.types.ObjectId;

import actor4j.core.data.access.MongoUtils;
import eu.stimey.platform.microservice.api.gateway.rest.databind.LoginResponse;
import eu.stimey.platform.library.core.databind.Login;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.utils.rest.RESTResponse;
import eu.stimey.platform.library.utils.security.JsonWebToken;
import redis.clients.jedis.Jedis;

@Path("/v1/user")
public class UserResource {
	@Context
	RedisAMQPService redisAMQPService;

	@GET
	@Path("{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(@PathParam("userid") String userid) {
		ObjectId useridAsObjectId = null;
		String error = null;
		try {
			useridAsObjectId = new ObjectId(userid);
		} catch (Exception e) {
			error = e.getMessage();
		}

		if (error == null && useridAsObjectId != null) {
			LoginResponse loginResponse = findUser(useridAsObjectId);
			if (loginResponse != null) {
				return Response.status(200)
						.entity(new RESTResponse(RESTResponse.SUCCESS, 200, loginResponse, "The user was found."))
						.build();
			} else
				return Response.status(404)
						.entity(new RESTResponse(RESTResponse.NO_SUCCESS, 404, null, "The user was not found."))
						.build();
		} else
			return Response.status(400)
					.entity(new RESTResponse(RESTResponse.ERROR, 400, error, "The request was error prone.")).build();
	}

	public LoginResponse findUser(ObjectId userid) {
		LoginResponse result = null;

		Login login = MongoUtils.findOne(new Document("_id", userid), MongoDBService.getService().getClient(),
				"ms_auth", "login", Login.class);

		if (login != null)
			result = new LoginResponse(login._id.toHexString(), login.username, login.usertype, login.usersubtype,
					login.language, login.robotname,
					login.robotchannel.replace("robot-", "").replace("-", "")/* WORKAROUND */, login.under13);

		return result;
	}

	@GET
	@Path("verify/{access_token}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response verify(@PathParam("access_token") String access_token) {
		Jedis jedis = redisAMQPService.getJedisPool().getResource();
		String private_key = jedis.get("ms_main_global-private_key");
		jedis.close();

		boolean verified = false;
		String error = null;
		try {
			verified = JsonWebToken.verify(JsonWebToken.transformKey(private_key), access_token) != null;
		} catch (Exception e) {
			error = e.getMessage();
		}

		if (error == null) {
			if (verified)
				return Response.status(200)
						.entity(new RESTResponse(RESTResponse.SUCCESS, 200, "", "User access is authorized.")).build();
			else
				return Response.status(401)
						.entity(new RESTResponse(RESTResponse.NO_SUCCESS, 401, "", "User access is unauthorized."))
						.build();
		} else
			return Response.status(400)
					.entity(new RESTResponse(RESTResponse.ERROR, 400, error, "The request was error prone.")).build();
	}
}
