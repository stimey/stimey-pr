package eu.stimey.platform.microservice.api.gateway.rest;

import static eu.stimey.platform.library.utils.StimeyLogger.*;

import javax.annotation.PreDestroy;
import javax.ws.rs.ApplicationPath;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.robot.startup.RobotAMQPService;

/**
 * 
 * REST configuration
 *
 */
@ApplicationPath("api")
public class RESTConfig extends ResourceConfig {
	protected final MongoDBService mongoDBService;
	protected final RedisAMQPService redisAMQPService;
	protected final RobotAMQPService robotAMQPService;

	public RESTConfig() {
		super();

		mongoDBService = MongoDBService.getService();
		redisAMQPService = RedisAMQPService.getService();
		robotAMQPService = RobotAMQPService.getService();

		logger().info("REST-Service started...");

		packages("eu.stimey.platform.microservice.api.gateway.rest");

		register(new AbstractBinder() {
			protected void configure() {
				bind(mongoDBService).to(MongoDBService.class);
				bind(redisAMQPService).to(RedisAMQPService.class);
				bind(robotAMQPService).to(RobotAMQPService.class);
				bind(new UserSharedService()).to(UserSharedService.class);
			}
		});

		register(new JacksonJsonProvider().configure(SerializationFeature.INDENT_OUTPUT, true));
	}

	@PreDestroy
	public void shutdown() {
		logger().info("REST-Service stopped...");
	}
}
