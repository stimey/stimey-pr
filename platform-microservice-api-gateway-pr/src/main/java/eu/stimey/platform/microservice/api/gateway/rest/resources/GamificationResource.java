package eu.stimey.platform.microservice.api.gateway.rest.resources;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.tuple.Pair;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;
import eu.stimey.platform.library.utils.rest.RESTResponse;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.library.utils.security.JsonWebToken;
import eu.stimey.platform.microservice.api.gateway.startup.Global;

@Path("v1/gamification")
public class GamificationResource {
	@Inject
	UserSharedService userSharedService;
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response gamification(@QueryParam("userid") String userid, @QueryParam("username") String username, 
			@QueryParam("knowledge") Integer knowledgePoints, @QueryParam("social") Integer socialPoints, @QueryParam("creativity") Integer creativityPoints) {
		
		if (knowledgePoints==null ||  socialPoints==null || creativityPoints==null)
			return Response.status(400)
				.entity(new RESTResponse(RESTResponse.ERROR, 400, null, "The request was error prone.")).build();
		
		if (username!=null)
			userid = userSharedService.getUserId(username);
		UserSharedCacheObject userSharedCacheObject = userSharedService.getUser(userid);
		if (userSharedCacheObject == null)
			return Response.status(404)
					.entity(new RESTResponse(RESTResponse.NO_SUCCESS, 404, null, "The user was not found.")).build();
		
		Cookie cookie = JsonWebToken.createCookie(userid, userSharedCacheObject.username,
				userSharedCacheObject.usertype, userSharedCacheObject.usersubtype, Global.getPrivateKey(),
				Global.DOMAIN);
		Client client = StimeyClient.create();
		List<Pair<String, Object>> queryParams = new LinkedList<>();
		queryParams.add(Pair.of("knowledge", knowledgePoints));
		queryParams.add(Pair.of("social", socialPoints));
		queryParams.add(Pair.of("creativity", creativityPoints));
		Response response = StimeyClient.get(client, "achievements", Global.DOCKER, "api/gamification/points", cookie,
				queryParams);
		
		if (response != null) {
			if (response.getStatus() == 200) 
				return Response.status(200).entity(new RESTResponse(RESTResponse.SUCCESS, 200, null, "The request was accepted.")).build();
			else
				return Response.status(response.getStatus()).entity(
						new RESTResponse(RESTResponse.ERROR, response.getStatus(), null, "Something went wrong."))
						.build();
		} else
			return Response.status(502).entity(
					new RESTResponse(RESTResponse.ERROR, 502, null, "The request could not handled successfully."))
					.build();
	}
}
