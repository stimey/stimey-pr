package eu.stimey.platform.microservice.api.gateway.rest.databind;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import eu.stimey.platform.library.utils.rest.RESTResponse;

/**
 * 
 * Default Ping resource
 *
 */
@Path("/ping")
public class PingResource {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response ping() {
		return Response.ok().entity(new RESTResponse(RESTResponse.SUCCESS, 200, "1", "pong")).build();
	}
}
