package eu.stimey.platform.microservice.api.gateway.startup;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.robot.startup.RobotAMQPService;

/**
 * Global web application settings
 * 
 */
@WebListener
public class ServiceServletContextListener implements ServletContextListener {
	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		Global.startup();

		MongoDBService.start("ms_api_gateway");
		RedisAMQPService.start();
		RobotAMQPService.start();
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		RobotAMQPService.stop();
		RedisAMQPService.stop();
		MongoDBService.stop();
	}
}
