package eu.stimey.platform.microservice.api.gateway.rest.resources;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;
import eu.stimey.platform.library.utils.rest.RESTResponse;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.library.utils.security.JsonWebToken;
import eu.stimey.platform.microservice.api.gateway.startup.Global;

@Path("/v1/friends")
public class FriendsResource {
	protected static class Friend {
		public String userid;
		public String username;
		public String robotchannel;
		public String avatarImageId;

		public Friend(String userid, String username, String robotchannel, String avatarImageId) {
			super();
			this.userid = userid;
			this.username = username;
			this.robotchannel = robotchannel;
			this.avatarImageId = avatarImageId;
		}

		public static Friend of(String userid, String username, String robotchannel, String avatarImageId) {
			return new Friend(userid, username, robotchannel, avatarImageId);
		}
	}

	@Inject
	UserSharedService userSharedService;

	@SuppressWarnings("unchecked")
	@GET
	@Path("{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	// localhost:8080/api-gateway/api/v1/notification/5a156e3a394d09ca9f4f7847?new=1&&start=0&&count=-1
	// https://stimey.eu:8443/api/v1/notification/5a156e3a394d09ca9f4f7847?new=1&&start=0&&count=-1
	public Response notfications(@Context HttpServletRequest request,
			@PathParam("userid") String userid) { /* LinkedHashMap<String, Object> request */
		UserSharedCacheObject userSharedCacheObject = userSharedService.getUser(userid);
		if (userSharedCacheObject == null)
			return Response.status(404)
					.entity(new RESTResponse(RESTResponse.NO_SUCCESS, 404, null, "The user was not found.")).build();

		Cookie cookie = JsonWebToken.createCookie(userid, userSharedCacheObject.username,
				userSharedCacheObject.usertype, userSharedCacheObject.usersubtype, Global.getPrivateKey(),
				Global.DOMAIN);
		Client client = StimeyClient.create();
		Response response = StimeyClient.get(client, "dashboard", Global.DOCKER, "api/profiles/" + userid, cookie);

		if (response != null) {
			if (response.getStatus() == 200) {
				response.bufferEntity();

				List<Friend> data = new LinkedList<>();
				Map<String, Object> responseData = null;
				try {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
					responseData = mapper.readValue(response.readEntity(String.class), LinkedHashMap.class);
					List<String> friends = (List<String>) (responseData.get("friends"));
					for (String friend : friends) {
						UserSharedCacheObject object = userSharedService.getUser(friend);
						if (object != null)
							data.add(Friend.of(friend, object.username,
									object.robotchannel.replace("robot-", "").replace("-", "")/* WORKAROUND */,
									getAvatarImageId(client, cookie, friend)));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				return Response.status(200).entity(new RESTResponse(RESTResponse.SUCCESS, 200, data, "")).build();
			} else
				return Response.status(response.getStatus()).entity(
						new RESTResponse(RESTResponse.ERROR, response.getStatus(), null, "Something went wrong."))
						.build();
		} else
			return Response.status(502).entity(
					new RESTResponse(RESTResponse.ERROR, 502, null, "The request could not handled successfully."))
					.build();
	}

	@SuppressWarnings("unchecked")
	public String getAvatarImageId(Client client, Cookie cookie, String userid) {
		String result = null;

		Response response = StimeyClient.get(client, "dashboard", Global.DOCKER, "api/profiles/" + userid, cookie);

		if (response != null) {
			if (response.getStatus() == 200) {
				response.bufferEntity();

				Map<String, Object> responseData = null;
				try {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
					responseData = mapper.readValue(response.readEntity(String.class), LinkedHashMap.class);
					result = (String) (responseData.get("avatarImageId"));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}

		return result;
	}
}
