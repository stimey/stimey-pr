package eu.stimey.platform.microservice.api.gateway.rest.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import eu.stimey.platform.library.robot.communication.RobotCommunication;
import eu.stimey.platform.library.robot.startup.RobotAMQPService;
import eu.stimey.platform.library.utils.rest.RESTResponse;
import eu.stimey.platform.microservice.api.gateway.rest.databind.RobotRequest;

@Path("/v1/robot")
public class RobotResource {
	@Context
	RobotAMQPService robotAMQPService;

	@POST
	@Path("{routingKey}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(@PathParam("routingKey") String routingKey, RobotRequest robotRequest) {
		if (robotRequest != null) {
			RobotCommunication<String, String> com = new RobotCommunication<>(robotAMQPService.getRabbitmqConnection(),
					String.class);
			if (com.publish(routingKey, robotRequest.getCommands()))
				return Response.status(202).entity(new RESTResponse(RESTResponse.SUCCESS, 202, "",
						"The request was accepted and the command was send to the robot.")).build();

			else
				return Response.status(500)
						.entity(new RESTResponse(RESTResponse.ERROR, 500, "", "Internal Server Error.")).build();
		} else
			return Response.status(400)
					.entity(new RESTResponse(RESTResponse.ERROR, 400, "", "The request was error prone.")).build();
	}
}
