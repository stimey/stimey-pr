package eu.stimey.platform.microservice.api.gateway.rest.resources;

import java.util.Arrays;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.Document;

import actor4j.core.data.access.MongoUtils;
import eu.stimey.platform.microservice.api.gateway.rest.databind.LoginRequest;
import eu.stimey.platform.microservice.api.gateway.rest.databind.LoginResponse;
import eu.stimey.platform.library.core.databind.Login;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.utils.rest.RESTResponse;
import eu.stimey.platform.library.utils.security.Credentials;

@Path("/v1/login")
public class LoginResource {
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(LoginRequest loginRequest) {
		if (loginRequest != null) {
			LoginResponse loginResponse = loginUser(loginRequest);
			if (loginResponse != null) {
				return Response.status(200).entity(new RESTResponse(RESTResponse.SUCCESS, 200, loginResponse,
						"The user for the given credentials was accepted.")).build();
			} else
				return Response.status(404).entity(new RESTResponse(RESTResponse.NO_SUCCESS, 404, null,
						"The user for the given credentials was not accepted.")).build();
		} else
			return Response.status(400)
					.entity(new RESTResponse(RESTResponse.ERROR, 400, "", "The request was error prone.")).build();
	}

	public LoginResponse loginUser(LoginRequest loginRequest) {
		LoginResponse result = null;

		Login login = MongoUtils.findOne(
				new Document("$or",
						Arrays.asList(new Document("username", loginRequest.getUsername()),
								new Document("email", loginRequest.getUsername()))),
				MongoDBService.getService().getClient(), "ms_auth", "login", Login.class);

		if (login != null) {
			if (Credentials.verify(loginRequest.getPassword(), new Credentials(login.salt, login.hash))) {
				result = new LoginResponse(login._id.toHexString(), login.username, login.usertype, login.usersubtype,
						login.language, login.robotname,
						login.robotchannel.replace("robot-", "").replace("-", "")/* WORKAROUND */, login.under13);

				loginRequest.setPassword(UUID.randomUUID().toString());
			}
		}

		return result;
	}
}
