package eu.stimey.platform.microservice.api.gateway.rest.databind;

import java.util.Date;

public class LoginResponse {
	protected String userid;
	protected String username;
	protected String usertype;
	protected String usersubtype;
	protected String language;
	protected String robotname;
	protected String robotchannel;
	protected boolean under13;

	protected Date timestamp;

	public LoginResponse() {
		super();
	}

	public LoginResponse(String userid, String username, String usertype, String usersubtype, String language,
			String robotname, String robotchannel, boolean under13) {
		super();
		this.userid = userid;
		this.username = username;
		this.usertype = usertype;
		this.usersubtype = usersubtype;
		this.language = language;
		this.robotname = robotname;
		this.robotchannel = robotchannel;
		this.under13 = under13;
		this.timestamp = new Date();
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public String getUsersubtype() {
		return usersubtype;
	}

	public void setUsersubtype(String usersubtype) {
		this.usersubtype = usersubtype;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getRobotname() {
		return robotname;
	}

	public void setRobotname(String robotname) {
		this.robotname = robotname;
	}

	public String getRobotchannel() {
		return robotchannel;
	}

	public void setRobotchannel(String robotchannel) {
		this.robotchannel = robotchannel;
	}

	public boolean isUnder13() {
		return under13;
	}

	public void setUnder13(boolean under13) {
		this.under13 = under13;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	@Override
	public String toString() {
		return "LoginResponse [userid=" + userid + ", username=" + username + ", usertype=" + usertype + ", language="
				+ language + ", robotname=" + robotname + ", robotchannel=" + robotchannel + ", timestamp=" + timestamp
				+ "]";
	}
}
