package eu.stimey.platform.microservice.dashboard.rest.resources;

import eu.stimey.platform.library.utils.StimeyLogger;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.microservice.dashboard.beans.PortfolioService;
import eu.stimey.platform.microservice.dashboard.databind.portfolio.EPortfolioImage;
import eu.stimey.platform.microservice.dashboard.databind.portfolio.Portfolio;
import eu.stimey.platform.microservice.dashboard.startup.Global;
import org.bson.Document;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/portfolios")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PortfolioResource {

	@Inject
	PortfolioService portfolioService;

	private static Client getClient() {
		return new StimeyClient().create();
	}

	@POST
	@Path("/{userId}/images")
	public Response addImage(@PathParam("userId") String userId, @CookieParam("access_token") Cookie cookie,
			Document imageData) {
		StimeyLogger.logger().info(imageData);

		// TODO: 1. Create statusupdate for image
		Document statusUpdate = new Document();
		statusUpdate.put("text", imageData.getString("description"));
		statusUpdate.put("photoId", "");
		statusUpdate.put("fileId", "");
		statusUpdate.put("fileName", "");
		statusUpdate.put("fileDescription", "");
		statusUpdate.put("category", "eportfolioAlbum");

		Response statusUpdateResponse = StimeyClient.post(getClient(), "api-gateway-storage", Global.DOCKER,
				"/api/statusupdate", cookie, statusUpdate.toJson());
		StimeyLogger.logger().info(statusUpdateResponse);
		String statusUpdateJson = statusUpdateResponse.readEntity(String.class);
		Document statusUpdateDocument = Document.parse(statusUpdateJson);
		StimeyLogger.logger().info(statusUpdateDocument);
		// TODO: 2. save image in db
		EPortfolioImage ePortfolioImage = new EPortfolioImage(imageData.getString("photoId"),
				imageData.getString("imageTitle"), statusUpdateDocument.getString("statusUpdateId"));
		Portfolio portfolio = portfolioService.getPortfolioById(userId);
		portfolio.ePortfolioImages.add(ePortfolioImage);
		portfolioService.setPortfolio(portfolio);
		return Response.status(Response.Status.OK).entity(ePortfolioImage).build();
	}

	@DELETE
	@Path("/{userId}/images/{imageId}")
	public Response deleteImage(@PathParam("userId") String userId, @PathParam("imageId") String imageId,
			@CookieParam("access_token") Cookie cookie) {
		Client client = new StimeyClient().create();
		Response response;

		response = StimeyClient.delete(getClient(), "files", Global.DOCKER, "/api/files/" + imageId, cookie);
		StimeyLogger.logger().info(response);

		Portfolio portfolio = portfolioService.getPortfolioById(userId);
		StimeyLogger.logger().info(response);
		EPortfolioImage image = portfolio.getEPortfolioImageById(imageId);
		if (image.getStatusUpdateId() != null) {
			response = StimeyClient.delete(client, "api-gateway-storage", Global.DOCKER,
					"/api/statusupdate/" + image.getStatusUpdateId(), cookie);
			System.out.println(response);
		}

		portfolio.ePortfolioImages.remove(image);
		this.portfolioService.setPortfolio(portfolio);
		return Response.status(Response.Status.OK).build();
	}

}
