package eu.stimey.platform.microservice.dashboard.rest.resources;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.library.utils.security.JsonWebToken;
import eu.stimey.platform.microservice.dashboard.beans.GlobalVariables;
import eu.stimey.platform.microservice.dashboard.beans.ProfileService;
import eu.stimey.platform.microservice.dashboard.beans.filter.StudentsRealtedToSchoolFilter;
import eu.stimey.platform.microservice.dashboard.beans.filter.TeachersRelatedToSchoolFilter;
import eu.stimey.platform.microservice.dashboard.databind.profile.BlockedUser;
import eu.stimey.platform.microservice.dashboard.databind.profile.Profile;
import eu.stimey.platform.microservice.dashboard.rest.databind.profile.MessageRequestResponse;
import eu.stimey.platform.microservice.dashboard.rest.filter.AuthenticationFilter;
import eu.stimey.platform.microservice.dashboard.startup.Global;
import org.bson.Document;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Path("/profiles")
@Produces(MediaType.APPLICATION_JSON)
public class ProfileResource {

	@Inject
	private ProfileService profileService;

	@Inject
	private UserSharedService userSharedService;

	@GET
	public Response getProfiles(@DefaultValue("") @QueryParam("query") final String query,
			@QueryParam("usertype") final String usertype, @QueryParam("ids[]") final List<String> ids) {
		if (query.isEmpty() && ids.size() == 0)
			return Response.status(Response.Status.NO_CONTENT).build();

		if (!query.isEmpty() && ids.size() > 0)
			return Response.status(Response.Status.BAD_REQUEST).entity("provide either search query or ids. Not both")
					.build();

		if (ids.isEmpty()) {
			final List<String> usernames = this.userSharedService.getFuzzySearchCaching().find(query, 100, 50, 35).stream().limit(50).collect(Collectors.toList());
			for (final String username : usernames) {
				ids.add(this.userSharedService.getUserId(username));
			}
		}

		List<Profile> profiles = this.profileService.getProfiles(ids);

		if (usertype == null || usertype.isEmpty()) {
			return Response.status(200).entity(profiles).build();
		}

		profiles = profiles.stream().filter(profile -> profile.usertype.equals(usertype)).collect(Collectors.toList());
		return Response.status(200).entity(profiles).build();
	}

	@POST
	public Response getProfilesByIds(List<String> ids) {
		return Response.status(200).entity(this.profileService.getProfiles(ids)).build();
	}

	@GET
	@Path("/{userid}")
	public Response getProfile(@PathParam("userid") final String userid) {
		return Response.status(Response.Status.OK).entity(profileService.get(userid)).build();
	}

	@PUT
	@Path("/{userid}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response setProfile(@PathParam("userid") final String userid, final Profile profile) {
		Profile oldProfile = this.profileService.get(userid);

		if (!oldProfile.verified && profile.verified) {
			Response response = this.verify(userid);
			if (response.getStatus() != 200)
				return response;
			profile.verified = true;
		}
		this.profileService.set(profile);
		return Response.status(Response.Status.OK).build();
	}

	@POST
	@Path("/{userid}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response verify(@PathParam("userid") final String userid, final Document verificationCode) {
		Profile profile = this.profileService.get(userid);
		if (!profile.usersubtype.equals("school"))
			throw new IllegalArgumentException("userid does not belong to a school-account");

		if (profile.verified)
			return Response.status(Response.Status.NOT_MODIFIED).build();

		if (!verificationCode.containsKey("verificationCode"))
			return Response.status(Response.Status.BAD_REQUEST).entity("body must contain key='verficationCode'")
					.build();

		final String verificationCodeInput = verificationCode.getString("verificationCode");

		if (!verificationCodeInput.equals(GlobalVariables.SCHOOL_VERIFICATION_CODE))
			return Response.status(Response.Status.BAD_REQUEST).entity("Wrong verification code").build();

		Response response = this.verify(userid);
		if (response.getStatus() != 200)
			return response;

		profile.verified = true;
		this.profileService.set(profile);
		return Response.status(Response.Status.OK).entity(profile).build();
	}

	private Response verify(String userid) {
		Cookie cookie = JsonWebToken.createCookie("", "", "admin", AuthenticationFilter.getPrivateKey(), Global.DOMAIN);
		Response response = StimeyClient.post(StimeyClient.create(), "auth", Global.DOCKER,
				"api/v1/verification/" + userid, cookie, "");
		return response;
	}

	@GET
	@Path("/organizations")
	public Response getOrganizations() {
		Document organizationFilter = new Document("usertype", "incubator");
		final List<Profile> organizations = this.findProfiles(organizationFilter);
		return Response.status(Response.Status.OK).entity(organizations).build();
	}

	private List<Profile> findProfiles(final Document filter) {
		final List<String> profileIds = this.profileService.getCache().find(filter, null, 0, 0,
				profile -> profile.getUserid(), true);
		return this.profileService.getProfiles(profileIds);
	}

	@GET
	@Path("/organizations/{userid}/username")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getOrganizationById(@PathParam("userid") final String userid) {
		final Profile profile = this.profileService.get(userid);
		if (!profile.usertype.equals("incubator"))
			return Response.status(Response.Status.NOT_FOUND).build();
		return Response.status(Response.Status.OK).entity(profile.username).build();
	}

	@GET
	@Path("/organizations/schools")
	public Response getSchools(@QueryParam("exclude") final List<String> schoolsToExclude) {
		Document usertypeFilter = new Document("usertype", "incubator");
		Document usersubtypeFilter = new Document("usersubtype", "school");
		List<Document> schoolFilter = new ArrayList<>();
		schoolFilter.add(usertypeFilter);
		schoolFilter.add(usersubtypeFilter);
		Document filter = new Document("$and", schoolFilter);
		List<String> profileIds = this.profileService.getCache().find(filter, null, 0, 0,
				profile -> profile.getUserid(), true);
		profileIds.removeAll(schoolsToExclude);
		final List<Profile> schools = this.profileService.getProfiles(profileIds);
		if (schools.size() == 0) {
			return Response.status(Response.Status.NO_CONTENT).build();
		}
		return Response.status(Response.Status.OK).entity(schools).build();
	}

	@GET
	@Path("/organizations/schools/{schoolId}/teachers")
	public Response getTeachersOfSchool(@PathParam("schoolId") final String schoolId) {
		final String realNameOfSchool = this.profileService.get(schoolId).basicInfo.realName;
		if (realNameOfSchool == null || realNameOfSchool.isEmpty())
			return Response.status(Response.Status.NO_CONTENT).build();

		final TeachersRelatedToSchoolFilter teachersRelatedToSchoolFilter = new TeachersRelatedToSchoolFilter(
				realNameOfSchool);
		final List<String> profileIds = this.profileService.getCache().find(teachersRelatedToSchoolFilter.toDocument(),
				null, 0, 0, profile -> profile.getUserid(), true);

		if (profileIds.isEmpty())
			return Response.status(Response.Status.NO_CONTENT).build();

		return Response.status(Response.Status.OK).entity(this.profileService.getProfiles(profileIds)).build();
	}

	@GET
	@Path("/organizations/schools/{schoolId}/parents")
	public Response getParentsOfStudentsOfSchool(@PathParam("schoolId") final String schoolId) {
		// TODO: implement

		return Response.status(200).entity(20).build();

	}

	@GET
	@Path("/organizations/schools/{schoolId}/students")
	public Response getStudentsOfSchool(@PathParam("schoolId") final String schoolId) {
		final String realNameOfSchool = this.profileService.get(schoolId).basicInfo.realName;
		if (realNameOfSchool == null || realNameOfSchool.isEmpty())
			return Response.status(Response.Status.NO_CONTENT).build();

		final StudentsRealtedToSchoolFilter studentsRealtedToSchoolFilter = new StudentsRealtedToSchoolFilter(
				realNameOfSchool);
		final List<String> profileIds = this.profileService.getCache().find(studentsRealtedToSchoolFilter.toDocument(),
				null, 0, 0, profile -> profile.getUserid(), true);

		if (profileIds.isEmpty())
			return Response.status(Response.Status.NO_CONTENT).build();

		return Response.status(Response.Status.OK).entity(this.profileService.getProfiles(profileIds)).build();
	}

	@GET
	@Path("/{profileId}/avatarImageId")
	public Response getAvatarImageId(@PathParam("profileId") final String profileId) {
		final Profile profile = this.profileService.get(profileId);
		if (profile.avatarImageId == null)
			return Response.status(Response.Status.NO_CONTENT).build();
		return Response.status(Response.Status.OK).entity(profile.avatarImageId).build();
	}

	@GET
	@Path("/{profileId}/avatarImageRedirect")
	public Response getAvatarImageRedirectLink(@PathParam("profileId") final String profileId) {
		final Profile profile = this.profileService.get(profileId);
		if (profile.avatarImageId == null)
			return Response.temporaryRedirect(URI.create("/dist/user.png")).build();

		if (Global.DOCKER) {
			return Response
					.temporaryRedirect(
							URI.create("https://" + Global.DOMAIN + "/files/api/files/" + profile.avatarImageId))
					.build();
		} else {
			return Response.temporaryRedirect(URI.create(
					"http://" + Global.DOMAIN + ":8080/platform-microservice-files/api/files/" + profile.avatarImageId))
					.build();
		}

	}

	/**
	 * NETWORKING FUNCTIONS
	 */
	@GET
	@Path("/{profileId}/friendRequests")
	public Response getFriendRequests(@PathParam("profileId") final String profileId) {
		Profile profile = this.profileService.get(profileId);
		List<Profile> profilesOfFriendRequests = this.profileService.getProfiles(profile.friendRequests);
		return Response.status(Response.Status.OK).entity(profilesOfFriendRequests).build();
	}

	@GET
	@Path("/{profileId}/friends")
	public Response getFriends(@PathParam("profileId") final String profileId) {
		final Profile profile = this.profileService.get(profileId);
		final List<Profile> profilesOfFriends = this.profileService.getProfiles(profile.friends);
		return Response.status(Response.Status.OK).entity(profilesOfFriends).build();
	}

	@GET
	@Path("/{profileId}/requestsFromParents")
	public Response getRequestsFromParents(@PathParam("profileId") final String profileId) {
		final Profile profile = this.profileService.get(profileId);
		final List<Profile> profilesOfFriends = this.profileService.getProfiles(profile.requestFromParent);
		return Response.status(Response.Status.OK).entity(profilesOfFriends).build();
	}

	@GET
	@Path("/{profileId}/parents")
	public Response getParents(@PathParam("profileId") final String profileId) {
		final Profile profile = this.profileService.get(profileId);
		final List<Profile> profilesOfParents = this.profileService.getProfiles(profile.parents);
		return Response.status(Response.Status.OK).entity(profilesOfParents).build();
	}

	@GET
	@Path("/{profileId}/childs")
	public Response getChilds(@PathParam("profileId") final String profileId) {
		final Profile profile = this.profileService.get(profileId);
		final List<Profile> profilesOfChilds = this.profileService.getProfiles(profile.childs);
		return Response.status(Response.Status.OK).entity(profilesOfChilds).build();
	}

	@GET
	@Path("/{profileId}/friends/usernames")
	public Response getUsernamesOfFriends(@PathParam("profileId") final String profileId) {
		final Profile profile = this.profileService.get(profileId);
		final List<Profile> profilesOfFriends = this.profileService.getProfiles(profile.friends);
		final List<String> usernamesOfFriends = new ArrayList<>();
		for (Profile friendProfile : profilesOfFriends) {
			usernamesOfFriends.add(friendProfile.username);
		}
		return Response.status(Response.Status.OK).entity(usernamesOfFriends).build();
	}

	@GET
	@Path("/{profileId}/following")
	public Response getFollowing(@PathParam("profileId") final String profileId) {
		final Profile profile = this.profileService.get(profileId);
		final List<Profile> profilesOfFollowing = this.profileService.getProfiles(profile.following);
		return Response.status(Response.Status.OK).entity(profilesOfFollowing).build();
	}

	@GET
	@Path("/{profileId}/follower")
	public Response getFollower(@PathParam("profileId") final String profileId) {
		final Profile profile = this.profileService.get(profileId);
		final List<Profile> profilesOfFollower = this.profileService.getProfiles(profile.follower);
		return Response.status(Response.Status.OK).entity(profilesOfFollower).build();
	}

	@GET
	@Path("/{profileId}/blockedUsers")
	public Response getBlockedUser(@PathParam("profileId") final String profileId) {
		final Profile profile = this.profileService.get(profileId);
		return Response.status(Response.Status.OK).entity(profile.blockedUser).build();
	}

	@POST
	@Path("/{profileId}/makeFriendRequest")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response friendRequest(@PathParam("profileId") final String profileId, final String targetUserid) {
		Profile targetProfile = this.profileService.get(targetUserid);
		Profile myProfile = this.profileService.get(profileId);

		targetProfile.friendRequests.add(profileId);

		myProfile.friendRequestsOutgoing.add(targetUserid);

		this.profileService.set(targetProfile);
		this.profileService.set(myProfile);
		return Response.status(Response.Status.CREATED).build();
	}

	@POST
	@Path("/{userId}/friendRequests/{friendRequestId}/accept")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response acceptFriendRequest(@PathParam("userId") final String userId,
			@PathParam("friendRequestId") final String friendRequestId) {
		Profile myProfile = this.profileService.get(userId);
		Profile friendProfile = this.profileService.get(friendRequestId);

		myProfile.following.remove(friendRequestId);
		myProfile.follower.remove(friendRequestId);
		myProfile.friendRequests.remove(friendRequestId);
		myProfile.friends.add(friendRequestId);
		myProfile.messageRequests.remove(friendRequestId);

		friendProfile.following.remove(userId);
		friendProfile.follower.remove(userId);
		friendProfile.friendRequestsOutgoing.remove(userId);
		friendProfile.friends.add(userId);
		friendProfile.messageRequests.remove(userId);

		this.profileService.set(myProfile);
		this.profileService.set(friendProfile);
		return Response.status(Response.Status.CREATED).build();
	}

	@POST
	@Path("/{userId}/friendRequests/{friendRequestId}/decline")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response declineFriendRequest(@PathParam("userId") final String userId,
			@PathParam("friendRequestId") final String friendRequestId) {
		Profile myProfile = this.profileService.get(userId);
		Profile friendProfile = this.profileService.get(friendRequestId);

		myProfile.friendRequests.remove(friendRequestId);
		friendProfile.friendRequestsOutgoing.remove(userId);

		this.profileService.set(myProfile);
		this.profileService.set(friendProfile);

		return Response.status(Response.Status.OK).build();
	}

	@DELETE
	@Path("/{userId}/friends/{friendId}")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response deleteFriendship(@PathParam("userId") final String userId,
			@PathParam("friendId") final String friendId) {
		Profile myProfile = this.profileService.get(userId);
		Profile friendProfile = this.profileService.get(friendId);

		myProfile.friends.remove(friendId);
		myProfile.following.add(friendId);
		myProfile.follower.add(friendId);

		friendProfile.friends.remove(userId);
		friendProfile.follower.add(userId);
		friendProfile.following.add(userId);

		this.profileService.set(myProfile);
		this.profileService.set(friendProfile);
		return Response.status(Response.Status.OK).build();
	}

	@POST
	@Path("/{childProfileId}/makeConnectWithChildRequest")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response connectWithChildRequest(@PathParam("childProfileId") final String childProfileId,
			final String parentProfileId) {
		Profile childProfile = this.profileService.get(childProfileId);

		childProfile.requestFromParent.add(parentProfileId);

		this.profileService.set(childProfile);
		return Response.status(Response.Status.CREATED).build();
	}

	@POST
	@Path("/{childProfileId}/requestsFromParent/{requestFromParentId}/accept")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response acceptRequestFromParent(@PathParam("childProfileId") final String childProfileId,
			@PathParam("requestFromParentId") final String requestFromParentId) {
		Profile child = this.profileService.get(childProfileId);
		child.requestFromParent.remove(requestFromParentId);
		child.parents.add(requestFromParentId);

		Profile parent = this.profileService.get(requestFromParentId);
		parent.childs.add(childProfileId);

		this.profileService.set(child);
		this.profileService.set(parent);
		return Response.status(Response.Status.CREATED).build();
	}

	@POST
	@Path("/{userId}/requestsFromParent/{requestFromParentId}/decline")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response declineRequestFromParent(@PathParam("userId") final String userId,
			@PathParam("requestFromParentId") final String requestFromParentId) {
		Profile myProfile = this.profileService.get(userId);
		myProfile.requestFromParent.remove(requestFromParentId);
		this.profileService.set(myProfile);
		return Response.status(Response.Status.OK).build();
	}

	@POST
	@Path("/{userId}/following")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response follow(@PathParam("userId") final String userId, String userIdToFollow) {
		Profile myProfile = this.profileService.get(userId);
		if (!myProfile.following.contains(userIdToFollow))
			myProfile.following.add(userIdToFollow);

		Profile followerProfile = this.profileService.get(userIdToFollow);
		if (!followerProfile.follower.contains(userId))
			followerProfile.follower.add(userId);

		this.profileService.set(followerProfile);
		this.profileService.set(myProfile);

		return Response.status(Response.Status.OK).build();
	}

	@DELETE
	@Path("/{userId}/following/{followerId}")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response unFollow(@PathParam("userId") final String userId, @PathParam("followerId") String followingId) {
		Profile myProfile = this.profileService.get(userId);
		Profile profileOfFollowing = this.profileService.get(followingId);

		myProfile.following.remove(followingId);
		profileOfFollowing.follower.remove(userId);

		this.profileService.set(profileOfFollowing);
		this.profileService.set(myProfile);
		return Response.status(Response.Status.OK).build();
	}

	@POST
	@Path("/{userId}/blockedUsers")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response blockUser(@PathParam("userId") final String userId, String userToBlock) {
		Profile myProfile = this.profileService.get(userId);
		Profile profileOfUserToBlock = this.profileService.get(userToBlock);

		myProfile.blockedUser.add(new BlockedUser(userToBlock, profileOfUserToBlock.username));
		myProfile.friends.remove(userToBlock);
		myProfile.following.remove(userToBlock);
		myProfile.follower.remove(userToBlock);

		profileOfUserToBlock.friends.remove(userId);
		profileOfUserToBlock.follower.remove(userId);
		profileOfUserToBlock.following.remove(userId);

		this.profileService.set(myProfile);
		this.profileService.set(profileOfUserToBlock);
		return Response.status(Response.Status.OK).build();
	}

	@DELETE
	@Path("/{userId}/blockedUsers/{idOfblockedUser}")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response unBlock(@PathParam("userId") final String userId,
			@PathParam("idOfblockedUser") final String idOfblockedUser) {
		Profile myProfile = this.profileService.get(userId);
		final String usernameOfBlockedUser = this.userSharedService.getUser(idOfblockedUser).username;
		BlockedUser blockedUser = new BlockedUser(idOfblockedUser, usernameOfBlockedUser);
		myProfile.blockedUser.remove(blockedUser);
		this.profileService.set(myProfile);
		return Response.status(Response.Status.OK).build();
	}

	@POST
	@Path("/{userId}/messageRequests")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postMessageRequest(@PathParam("userId") final String userId,
			final MessageRequestResponse messageRequestResponse) {
		Profile profile = this.profileService.get(userId);

		profile.messageRequests.remove(messageRequestResponse.targetId);
		if (!messageRequestResponse.delete) {
			profile.messageRequests.put(messageRequestResponse.targetId, messageRequestResponse.accepted);
		}

		System.out.println(profile.messageRequests.get(messageRequestResponse.targetId));
		this.profileService.set(profile);
		System.out.println(profile.messageRequests.get(messageRequestResponse.targetId));
		return Response.status(Response.Status.OK).build();
	}

	@GET
	@Path("/{userId}/messageRequests/")
	public Response getMessageRequests(@PathParam("userId") final String userId) {
		final Profile profile = this.profileService.get(userId);
		return Response.status(Response.Status.OK).entity(profile.messageRequests).build();
	}
}
