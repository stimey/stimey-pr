package eu.stimey.platform.microservice.dashboard.rest.databind.profile;

public class RestrictedUserProfile {

	private String avatarImageId;
	private String coverImageId;
	private String level;

	public RestrictedUserProfile() {
	}

	public RestrictedUserProfile(String avatarImageId, String coverImageId, String level) {
		this.avatarImageId = avatarImageId;
		this.coverImageId = coverImageId;
		this.level = level;
	}

	public String getAvatarImageId() {
		return avatarImageId;
	}

	public void setAvatarImageId(String avatarImageId) {
		this.avatarImageId = avatarImageId;
	}

	public String getCoverImageId() {
		return coverImageId;
	}

	public void setCoverImageId(String coverImageId) {
		this.coverImageId = coverImageId;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
}
