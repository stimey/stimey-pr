package eu.stimey.platform.microservice.dashboard.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.dashboard.databind.portfolio.Portfolio;
import org.bson.Document;

import java.util.LinkedList;
import java.util.List;

public class PortfolioService {

	private Caching<String, Portfolio> resourceCache;
	
	public PortfolioService() {
		this.resourceCache = new Caching<>("ms_dashboard_portfolio", "userId", Portfolio.class, "portfolio",
				GlobalVariables.CACHE_CONFIG);
		this.resourceCache.clear();
	}

	public void setPortfolio(Portfolio portfolio) {
		resourceCache.set(portfolio.getUserId(), portfolio);
	}

	public void delPortfolio(String portfolioId) {
		this.resourceCache.del(portfolioId);
	}

	public Portfolio getPortfolioById(String portfolioId) {
		Portfolio result = resourceCache.get(portfolioId);

		if (result == null) {
			result = new Portfolio(portfolioId);
			setPortfolio(result);
		}
		return result;
	}

	public Portfolio getPortfolioByPortfolioId(String portfolioId) {

		List<String> temp = resourceCache.find(new Document("ePortfolioId", portfolioId), null, 0, 0,
				(portfolio) -> portfolio.getUserId(), true);
		if (temp.size() > 1) {
			throw new IllegalStateException();
		}
		return resourceCache.get(temp.get(0));
	}

	public List<Portfolio> getPortfolioList(Document filter, int offset, int limit) {
		return this.getPortfolioByIds(this.getPortfolioIds(filter, offset, limit));
	}

	public List<String> getPortfolioIds(Document filter, int offset, int limit) {
		return resourceCache.find(filter, null, offset, limit, (portfolio) -> portfolio.getUserId(), true);
	}

	public List<Portfolio> getPortfolioByIds(List<String> portfolioIds) {
		List<Portfolio> portfolios = new LinkedList<>();
		for (String portfolioId : portfolioIds) {
			portfolios.add(this.getPortfolioById(portfolioId));
		}
		return portfolios;
	}
}
