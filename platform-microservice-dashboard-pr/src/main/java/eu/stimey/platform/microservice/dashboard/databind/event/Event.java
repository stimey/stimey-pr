package eu.stimey.platform.microservice.dashboard.databind.event;

import java.util.Date;

public class Event {
	public String title;
	public String description;
	public String coverImg;
	public Date start;
	public Date end;
}
