
package eu.stimey.platform.microservice.dashboard.rest.resources;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.stimey.platform.library.utils.StimeyLogger;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.library.utils.security.JsonWebToken;
import eu.stimey.platform.microservice.dashboard.databind.portfolio.Portfolio;
import eu.stimey.platform.microservice.dashboard.databind.portfolio.EPortfolioImage;
import eu.stimey.platform.microservice.dashboard.databind.portfolio.LearningJournalTask;
import eu.stimey.platform.microservice.dashboard.rest.databind.eportfolio.LearningJournalTaskWrapper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.*;

import eu.stimey.platform.microservice.dashboard.rest.databind.eportfolio.EPortfolioBasicInfoData;
import eu.stimey.platform.microservice.dashboard.rest.databind.eportfolio.EportfolioLearningplanData;
import eu.stimey.platform.microservice.dashboard.databind.portfolio.LearningPlan;
import eu.stimey.platform.microservice.dashboard.rest.filter.AuthenticationFilter;
import eu.stimey.platform.microservice.dashboard.startup.Global;

import eu.stimey.platform.microservice.dashboard.beans.ProfileService;
import eu.stimey.platform.microservice.dashboard.beans.PortfolioService;

import java.io.*;
import java.util.*;

@Path("/portfolio")
public class PortfolioResource_Old_Still_Used {
	@Context
	ProfileService profileService;

	@Context
	PortfolioService portfolioService;

	/**
	 * Get Portfolio Information from Portfolio with userid or eportfolioid. If User
	 * isn't owner of the Portfolio, Information is restricted according to
	 * UserPermission
	 *
	 * @param requestContext grants access to the actual UserId
	 * @param userId         access opportunity to get the Portfolio Information
	 * @param ePortfolioId   access opportunity to get the Portfolio Information
	 * @return an (maybe restricted) User Portfolio
	 */
	@GET
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPortfolioInformation(@Context ContainerRequestContext requestContext,
			@QueryParam("userid") @DefaultValue("") String userId,
			@QueryParam("eportfolioid") @DefaultValue("") String ePortfolioId) {
		// Use cookie to get Information as non stimey user!
		/*
		 * Client client = StimeyClient.create(); Cookie cookie =
		 * JsonWebToken.createCookie("", "", "student",
		 * AuthenticationFilter.getPrivateKey(), Global.DOMAIN);
		 * 
		 * Response response = StimeyClient.get(client, "files", Global.DOCKER, "api/",
		 * cookie, MediaType.APPLICATION_OCTET_STREAM); if (response.getStatus()==200) {
		 * InputStream inputStream = response.readEntity(InputStream.class); }
		 */

		String authorId = (String) requestContext.getProperty("userid");
		Portfolio portfolio;

		if (userId.equals("") && ePortfolioId.equals("")) {
			portfolio = portfolioService.getPortfolioById(authorId);

			return Response.status(Response.Status.OK).entity(portfolio).build();
		} else {
			if (!userId.equals("")) {
				portfolio = portfolioService.getPortfolioById(userId);
			} else {
				portfolio = portfolioService.getPortfolioByPortfolioId(ePortfolioId);

			}
		}

		if (portfolio.getePortfolioId() == null) {
			portfolio.setePortfolioId(UUID.randomUUID().toString());
			portfolioService.setPortfolio(portfolio);
		}

		portfolio = getInformation(authorId, portfolio);

		return Response.status(Response.Status.OK).entity(portfolio).build();
	}

	/**
	 * Get restricted Portfolio Information about a specific user. Method is usable
	 * for non stimey users.
	 *
	 * @param ePortfolioId grants access to the portfolio
	 * @return an restricted User Portfolio Object
	 */
	@GET
	@Path("/information")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getInformationFromPortfolio(@QueryParam("eportfolioid") @DefaultValue("") String ePortfolioId) {

		Portfolio portfolio = portfolioService.getPortfolioByPortfolioId(ePortfolioId);

		portfolio = getInformation("", portfolio);

		return Response.status(Response.Status.OK).entity(portfolio).build();
	}

	/**
	 * choose needed information about an Portfolio. Anything else will be removed.
	 *
	 * @param authorId  is the actual logged in user.
	 * @param portfolio contains all portfolio-information about the user you want
	 *                  to know.
	 * @return a restricted Portfolio Object
	 */
	private Portfolio getInformation(String authorId, Portfolio portfolio) {

		StimeyLogger.logger().info("Call get information of portfolio:" + portfolio);
		StimeyLogger.logger().info("authorid:" + portfolio);

		if (!authorId.equals(portfolio.getUserId())) {
			portfolio.setGeneratedPassword("");

			boolean alreadyFriends = false;

			if (!authorId.equals("")) {
				List<String> friends = new ArrayList<>(profileService.get(authorId).friends);

				for (String friend : friends) {
					if (friend.equals(portfolio.getUserId())) {
						alreadyFriends = true;
						break;
					}
				}
			}
			StimeyLogger.logger().info(portfolio.toString());
			if (portfolio.getBasicInfoUserPermission().equals("onlyMe")
					|| (portfolio.getBasicInfoUserPermission().equals("private") && !alreadyFriends)) {
				portfolio.setRealName("");
				portfolio.setUserName("");
				portfolio.setBirthday("");
				portfolio.setEmail("");
				portfolio.setCountry("");
				portfolio.setSchool("");
				portfolio.setBio("");
			}

			StimeyLogger.logger().info(portfolio.toString());

			if (portfolio.getAlbumUserPermission().equals("onlyMe")
					|| (portfolio.getAlbumUserPermission().equals("private") && !alreadyFriends)) {
				portfolio.setePortfolioImages(new LinkedList<>());
			}
			StimeyLogger.logger().info(portfolio.toString());
			if (portfolio.getLearningJournalUserPermission().equals("onlyMe")
					|| (portfolio.getLearningJournalUserPermission().equals("private") && !alreadyFriends)) {
				portfolio.setLearningJournalTasks(new LinkedList<>());
			}
		}
		return portfolio;
	}

	/**
	 * Update if EPortfolioInstruction should be shown or hidden
	 *
	 * @param userId                    grants access to the Eportfolio
	 * @param hideEPortfolioInstruction is the value to update
	 * @return the updated Portfolio
	 */
	@POST
	@Path("/instruction")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response changeEportfolioInstruction(@Context ContainerRequestContext requestContext,
			@QueryParam("userid") @DefaultValue("") String userId,
			@QueryParam("hideinstruction") @DefaultValue("false") Boolean hideEPortfolioInstruction) {
		Portfolio portfolio;
		if (userId.equals("")) {
			portfolio = portfolioService.getPortfolioById((String) requestContext.getProperty("userid"));
		} else {
			portfolio = portfolioService.getPortfolioById(userId);
		}

		portfolio.setHideEPortfolioInstruction(hideEPortfolioInstruction);
		portfolioService.setPortfolio(portfolio);

		return Response.status(Response.Status.OK).entity(portfolio).build();
	}

	/**
	 * Compare committed password with actual generatedPassword of the Portfolio
	 *
	 * @param userId   to choose which Portfolio should be accessed
	 * @param password is entered by the user and will be compared
	 * @return the correctness of the password
	 */
	@GET
	@Path("/password")
	@Produces(MediaType.APPLICATION_JSON)
	public Response comparePassword(@QueryParam("userid") @DefaultValue("") String userId,
			@QueryParam("password") @DefaultValue("") String password, @Context HttpServletRequest request,
			@Context HttpServletResponse response) {

		if (userId.equals("") || password.equals(""))
			return Response.status(Response.Status.BAD_REQUEST).build();

		boolean passwordIsCorrect = false;
		Portfolio portfolio = portfolioService.getPortfolioById(userId);

		if (portfolio.getGeneratedPassword() != null && portfolio.getGeneratedPassword().equals(password)) {
			passwordIsCorrect = true;
		}

		return Response.status(Response.Status.OK).entity(passwordIsCorrect).build();
	}

	/**
	 * Change the generated Password of an Eportfolio
	 *
	 * @param generatedPassword is the value to update
	 * @return an updated Portfolio
	 */
	@POST
	@Path("/newpassword")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response changeGeneratedPassword(@Context ContainerRequestContext requestContext, String generatedPassword) {

		Portfolio portfolio = portfolioService.getPortfolioById((String) requestContext.getProperty("userid"));
		portfolio.setGeneratedPassword(generatedPassword);
		portfolioService.setPortfolio(portfolio);

		return Response.status(Response.Status.OK).entity(portfolio).build();
	}

	/**
	 * Update basic Portfolio Information of the actual User
	 *
	 * @param requestContext grants access to the User Portfolio
	 * @param basicInfoData  contains basic Information like realUsername or E-Mail
	 * @return an updated EPortfolio Object
	 */
	@POST
	@Path("/basicinfo")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateBasicInfo(@Context ContainerRequestContext requestContext,
			EPortfolioBasicInfoData basicInfoData) {

		String authorId = (String) requestContext.getProperty("userid");

		Portfolio portfolio = portfolioService.getPortfolioById(authorId);
		portfolio.setBasicInfoUserPermission(basicInfoData.getBasicInfoUserPermission());
		portfolio.setRealName(basicInfoData.getRealName());
		portfolio.setUserName(basicInfoData.getUserName());
		portfolio.setBirthday(basicInfoData.getBirthday());
		portfolio.setEmail(basicInfoData.getEmail());
		portfolio.setCountry(basicInfoData.getCountry());
		portfolio.setSchool(basicInfoData.getSchool());
		portfolio.setBio(basicInfoData.getBio());

		portfolioService.setPortfolio(portfolio);

		return Response.status(Response.Status.OK).entity(portfolio).build();
	}

	/**
	 * Update Privacy Settings from StatusOverview
	 *
	 * @param requestContext               grants access to the UserPortfolio
	 * @param statusOverviewUserPermission contains the updated User Permission
	 * @return an updated Portfolio Object
	 */
	@POST
	@Path("/statusoverview")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateStatusOverviewInformation(@Context ContainerRequestContext requestContext,
			String statusOverviewUserPermission) {

		String authorId = (String) requestContext.getProperty("userid");

		Portfolio portfolio = portfolioService.getPortfolioById(authorId);
		portfolio.setStatusOverviewUserPermission(statusOverviewUserPermission);

		portfolioService.setPortfolio(portfolio);

		return Response.status(Response.Status.OK).entity(portfolio).build();
	}

	/**
	 * Update Settings from EPortfolio Album
	 *
	 * @param requestContext      grants access to the UserPortfolio
	 * @param albumUserPermission contains the updated User Permission
	 * @return an updated Portfolio Object
	 */
	@POST
	@Path("/albumsettings")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePortfolioAlbumSettings(@Context ContainerRequestContext requestContext,
			String albumUserPermission) {

		String authorId = (String) requestContext.getProperty("userid");

		Portfolio portfolio = portfolioService.getPortfolioById(authorId);
		portfolio.setAlbumUserPermission(albumUserPermission);

		portfolioService.setPortfolio(portfolio);

		return Response.status(Response.Status.OK).entity(portfolio).build();
	}

	/**
	 * Add new Image Informations (title, imageId, statusUpdateId) to the Portfolio
	 *
	 * @param requestContext  grants access to the Portfolio
	 * @param ePortfolioImage contains the Information to update
	 * @return an updated User Portfolio
	 */
	@POST
	@Path("/albumimage")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addPortfolioAlbumImage(@Context ContainerRequestContext requestContext,
			EPortfolioImage ePortfolioImage) {

		String authorId = (String) requestContext.getProperty("userid");

		Portfolio portfolio = portfolioService.getPortfolioById(authorId);
		List<EPortfolioImage> portfolioImages = portfolio.getePortfolioImages();

		if (portfolioImages == null) {
			portfolioImages = new LinkedList<>();
		}
		portfolioImages.add(ePortfolioImage);

		portfolio.setePortfolioImages(portfolioImages);
		portfolioService.setPortfolio(portfolio);

		return Response.status(Response.Status.OK).entity(portfolio).build();
	}

	/**
	 * Remove an image from the imageAlbum of the actual user.
	 *
	 * @param requestContext grants access to the actual user Portfolio
	 * @param imageId        is the removeable image
	 * @return the removed imageInformation
	 */
	@DELETE
	@Path("/albumimage")
	@Produces(MediaType.APPLICATION_JSON)
	public Response removePortfolioAlbumImage(@Context ContainerRequestContext requestContext,
			@QueryParam("imageId") String imageId) {

		String authorId = (String) requestContext.getProperty("userid");

		Portfolio portfolio = portfolioService.getPortfolioById(authorId);
		List<EPortfolioImage> portfolioImages = portfolio.getePortfolioImages();

		for (EPortfolioImage image : portfolioImages) {

			if (image.getImageId().equals(imageId)) {
				portfolioImages.remove(image);

				portfolio.setePortfolioImages(portfolioImages);
				portfolioService.setPortfolio(portfolio);

				return Response.status(Response.Status.OK).entity(image).build();
			}
		}

		return Response.status(Response.Status.NO_CONTENT).build();
	}

	/**
	 * Update User permissions of EPortfolio Learning Journal
	 *
	 * @param requestContext                grants access to the User Portfolio
	 * @param learningJournalUserPermission contains the value to change
	 * @return an updated EPortfolio Object
	 */
	@POST
	@Path("/journalsettings")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateLearningJournalSettings(@Context ContainerRequestContext requestContext,
			String learningJournalUserPermission) {
		String authorId = (String) requestContext.getProperty("userid");

		Portfolio portfolio = portfolioService.getPortfolioById(authorId);
		portfolio.setLearningJournalUserPermission(learningJournalUserPermission);

		portfolioService.setPortfolio(portfolio);

		return Response.status(Response.Status.OK).entity(portfolio).build();
	}

	/**
	 * Update the learning Journal entries (tasks and subtasks)
	 *
	 * @param requestContext             grants access to the User ID
	 * @param learningJournalTaskWrapper contains the tasks to update
	 * @return an updated EPortfolio Object
	 */
	@POST
	@Path("/journal")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateLearningJournalTasks(@Context ContainerRequestContext requestContext,
			LearningJournalTaskWrapper learningJournalTaskWrapper) {

		String authorId = (String) requestContext.getProperty("userid");
		Portfolio portfolio = portfolioService.getPortfolioById(authorId);
		List<LearningJournalTask> learningJournalTasks = new LinkedList<>();

		learningJournalTasks.addAll(Arrays.asList(learningJournalTaskWrapper.learningJournalTaskList));

		portfolio.setLearningJournalTasks(learningJournalTasks);
		portfolioService.setPortfolio(portfolio);

		return Response.status(Response.Status.OK).entity(portfolio).build();
	}

	/**
	 * Update the StatusUpdateId of the whole Learning Journal
	 *
	 * @param requestContext grants access to the Portfolio of the User
	 * @param statusUpdateId is the inserted statusUpdateId
	 * @return an updated Portfolio Object
	 */
	@POST
	@Path("/journalstatusupdate")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateJournalStatusUpdateId(@Context ContainerRequestContext requestContext,
			@QueryParam("statusupdateid") @DefaultValue("") String statusUpdateId) {

		if (statusUpdateId.equals("")) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

		String authorId = (String) requestContext.getProperty("userid");
		Portfolio portfolio = portfolioService.getPortfolioById(authorId);
		portfolio.setJournalStatusUpdateId(statusUpdateId);

		portfolioService.setPortfolio(portfolio);

		return Response.status(Response.Status.OK).entity(portfolio).build();
	}

	/**
	 * Save the implemented html content of the topic of the learning journal.
	 *
	 * @param requestContext grants access to the user portfolio
	 * @param statusUpdateId grants access to the correct task/subTask
	 * @param content        to save/update
	 * @return a boolean value if content was updated. Only true if a task/subTask
	 *         was found
	 */
	@POST
	@Path("/savecontent")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateJournalContent(@Context ContainerRequestContext requestContext,
			@QueryParam("statusupdateid") @DefaultValue("") String statusUpdateId, String content) {
		if (statusUpdateId.equals("")) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

		String authorId = (String) requestContext.getProperty("userid");
		Portfolio portfolio = portfolioService.getPortfolioById(authorId);
		boolean isValid = false;

		List<LearningJournalTask> learningJournalTasks = portfolio.getLearningJournalTasks();

		for (LearningJournalTask learningJournalTask : learningJournalTasks) {
			if (learningJournalTask.statusUpdateId.equals(statusUpdateId)) {
				learningJournalTask.content = content;
				isValid = true;
				break;
			} else {
				for (int i = 0; i < learningJournalTask.subTasks.length; i++) {
					if (learningJournalTask.subTasks[i].statusUpdateId.equals(statusUpdateId)) {
						learningJournalTask.subTasks[i].content = content;
						isValid = true;
						break;
					}
				}
				if (isValid) {
					break;
				}
			}
		}
		if (isValid) {
			portfolio.setLearningJournalTasks(learningJournalTasks);
			portfolioService.setPortfolio(portfolio);
		}

		return Response.status(Response.Status.OK).entity(isValid).build();
	}

	// ------------------------------//
	// Methods for unauthorized Users//
	// ------------------------------//

	/**
	 * Get the content of a task from the learning journal
	 *
	 * @param userId         is used to get the correct Portfolio
	 * @param statusUpdateId is used to get the correct task/subTask from the
	 *                       Portfolio
	 * @return the title and the content of the LearningJournal Task/SubTask
	 */
	@GET
	@Path("/content")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getContentFromTask(@QueryParam("userid") @DefaultValue("") String userId,
			@QueryParam("statusupdateid") @DefaultValue("") String statusUpdateId) {

		if (userId.equals("") || statusUpdateId.equals("")) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

		Portfolio portfolio = portfolioService.getPortfolioById(userId);
		List<LearningJournalTask> learningJournalTasks = portfolio.getLearningJournalTasks();
		LearningJournalTask temp = new LearningJournalTask();

		for (LearningJournalTask learningJournalTask : learningJournalTasks) {
			if (learningJournalTask.statusUpdateId.equals(statusUpdateId)) {
				temp.title = learningJournalTask.title;
				temp.content = learningJournalTask.content;
				return Response.status(Response.Status.OK).entity(temp).build();
			} else {
				for (int i = 0; i < learningJournalTask.subTasks.length; i++) {
					if (learningJournalTask.subTasks[i].statusUpdateId.equals(statusUpdateId)) {
						temp.title = learningJournalTask.subTasks[i].title;
						temp.content = learningJournalTask.subTasks[i].content;
						return Response.status(Response.Status.OK).entity(temp).build();
					}
				}
			}
		}

		return Response.status(Response.Status.OK).entity(temp).build();
	}

	/**
	 * Get an Image from files database
	 *
	 * @param imageId grants access to the image information
	 * @return the information about an image
	 */
	@GET
	@Path("/image")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getImagesFromFiles(@QueryParam("imageid") String imageId) {
		Client client = StimeyClient.create();
		Cookie cookie = JsonWebToken.createCookie("", "", "student", AuthenticationFilter.getPrivateKey(),
				Global.DOMAIN);

		Response response = StimeyClient.get(client, "files", Global.DOCKER, "/api/files/" + imageId, cookie,
				MediaType.APPLICATION_OCTET_STREAM);
		if (response.getStatus() != 200) {
			return response;
		}

		return Response.status(Response.Status.OK).entity(response.getEntity()).build();
	}

	/**
	 * Get the amount of missions of the user.
	 *
	 * @param userid grants access to the missions of the user
	 * @return all active and completed missions
	 */
	@GET
	@Path("/missions")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMissionInformation(@QueryParam("userid") String userid) {
		Response response;
		Client client = StimeyClient.create();
		Cookie cookie = JsonWebToken.createCookie("", "", "student", AuthenticationFilter.getPrivateKey(),
				Global.DOMAIN);

		response = StimeyClient.get(client, "courses", Global.DOCKER, "api/students/" + userid + "/missions", cookie);

		return Response.status(Response.Status.OK).entity(response.getEntity()).build();
	}

	/**
	 * Gets the amount of active and completed worlds of a user
	 *
	 * @param userid grants access to the worlds of the user
	 * @return the entity of the amount of worlds.
	 */
	@GET
	@Path("/worlds")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getWorldsInformation(@QueryParam("userid") String userid) {
		Response response;
		Client client = StimeyClient.create();
		Cookie cookie = JsonWebToken.createCookie("", "", "student", AuthenticationFilter.getPrivateKey(),
				Global.DOMAIN);

		response = StimeyClient.get(client, "planets", Global.DOCKER, "api/stats/worlds/" + userid, cookie);

		return Response.status(Response.Status.OK).entity(response.getEntity()).build();
	}

	/**
	 * gets the amount of brain, heart and spirit points from a user
	 *
	 * @param userid grants access to the achievements
	 * @return the entity of the achievements
	 */
	@GET
	@Path("/achievements")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAchievementInformation(@QueryParam("userid") String userid) {
		Response response;
		Client client = StimeyClient.create();
		Cookie cookie = JsonWebToken.createCookie("", "", "student", AuthenticationFilter.getPrivateKey(),
				Global.DOMAIN);

		response = StimeyClient.get(client, "achievements", Global.DOCKER, "api/achievements/" + userid, cookie);

		return Response.status(Response.Status.OK).entity(response.getEntity()).build();
	}

	/**
	 * gets information about the statusUpdate
	 *
	 * @param statusUpdateId grants access to the statusUpdate information
	 * @return the entity of the response of the REST-call
	 */
	@GET
	@Path("/statusupdate")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStatusUpdate(@QueryParam("statusupdateid") String statusUpdateId) {
		String result = "";

		Client client = StimeyClient.create();
		Cookie cookie = JsonWebToken.createCookie("", "", "student", AuthenticationFilter.getPrivateKey(),
				Global.DOMAIN);

		Response response = StimeyClient.get(client, "api-gateway-storage", Global.DOCKER,
				"api/statusupdate/" + statusUpdateId, cookie);

		String json = response.readEntity(String.class);
		ObjectMapper mapper = new ObjectMapper();
		try {
			JsonNode jsonNode = mapper.readTree(json);
			result = jsonNode.get("creatorId").asText();
			System.out.println(result);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return Response.status(Response.Status.OK).entity(result).build();
	}

	/* "/eportfolio/Learningplan" noch nicht fertig */
	@POST
	@Path("/learningplan")
	@Produces(MediaType.APPLICATION_JSON)
	@Deprecated
	public Response saveLearningplan(@Context ContainerRequestContext requestContext,
			EportfolioLearningplanData epDTO) {
		String authorId = (String) requestContext.getProperty("userid");

		System.out.println("creating portfolio db, userId:  " + authorId);

		Portfolio portfolioTemp = new Portfolio();
		// portfolioTemp.setPermission(epDTO.permission);
		// portfolioTemp.setStartDate(epDTO.startDate);
		// portfolioTemp.setEndDate(epDTO.endDate);
		// portfolioTemp.setScription(epDTO.scription);
		portfolioTemp.setUserId(authorId);

		portfolioService.setPortfolio(portfolioTemp);

		Portfolio portfolioTemp2 = portfolioService.getPortfolioById(authorId);

		return Response.status(Response.Status.OK).entity(portfolioTemp2).build();
	}

	/* "/LearningPlan/{index}" noch nicht fertig */
	@POST
	@Path("/LearningPlan/{index}")
	@Deprecated
	// @Path("/saveupdateeducation/{index}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response saveOrUpdateEducation(@Context ContainerRequestContext requestContext,
			@PathParam("index") int index, final LearningPlan LearningPlan) {

		String authorId = (String) requestContext.getProperty("userid");
		Portfolio portfolioTemp = portfolioService.getPortfolioById(authorId);

		// Add new LearningPlan
		if (index == -1) {
			System.out.println("New:" + LearningPlan.toString());
			// portfolioTemp.getLearningPlan().add(LearningPlan);
		}
		// Update existing Education
		else {
			try {
				// portfolioTemp.getLearningPlan().remove(index);
			} catch (IndexOutOfBoundsException e) {
				return Response.status(404).build();
			}
			// portfolioTemp.getLearningPlan().add(index,LearningPlan);
		}

		portfolioService.setPortfolio(portfolioTemp);
		Portfolio portfolioTemp2 = portfolioService.getPortfolioById(authorId);

		return Response.status(Response.Status.OK).entity(portfolioTemp2).build();
	}

}
