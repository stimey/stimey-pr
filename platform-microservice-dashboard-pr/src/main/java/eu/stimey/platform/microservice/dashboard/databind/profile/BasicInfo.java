package eu.stimey.platform.microservice.dashboard.databind.profile;

public class BasicInfo {

	public String realName;
	public String birthdate;
	public String country;
	public String city;
	public String bio;
	public String email;
	// public String gender;
	// public String school;
	// protected String username;

	private String permissions;

	public BasicInfo() {
		super();
		this.permissions = "private";
	}

	public BasicInfo(String gender, String birthdate, String country, String city, String email, String bio,
			String realName) {
		super();
		this.birthdate = birthdate;
		this.country = country;
		this.city = city;
		this.email = email;
		this.bio = bio;
		this.realName = realName;
		this.permissions = "private";
		// this.school = "";
		// this.gender = gender;
	}

	public String getBasicInfoPermission() {
		return this.permissions;
	}

	public void setBasicInfoPermission(String basicInfoPermission) {
		this.permissions = basicInfoPermission;
	}

}
