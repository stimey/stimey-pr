package eu.stimey.platform.microservice.dashboard.databind.portfolio;

import java.util.Objects;

public class EPortfolioImage {

	protected String imageId;
	protected String imageTitle;
	protected String statusUpdateId;

	public EPortfolioImage() {
	}

	public EPortfolioImage(String imageId, String imageTitle, String statusUpdateId) {
		this.imageId = imageId;
		this.imageTitle = imageTitle;
		this.statusUpdateId = statusUpdateId;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getImageTitle() {
		return imageTitle;
	}

	public void setImageTitle(String imageTitle) {
		this.imageTitle = imageTitle;
	}

	public String getStatusUpdateId() {
		return statusUpdateId;
	}

	public void setStatusUpdateId(String statusUpdateId) {
		this.statusUpdateId = statusUpdateId;
	}

	@Override
	public String toString() {
		return "ImageId: " + this.imageId + "    imageTitle: " + this.imageTitle + "    statusUpdateId: "
				+ this.statusUpdateId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		EPortfolioImage that = (EPortfolioImage) o;
		return Objects.equals(imageId, that.imageId) && Objects.equals(imageTitle, that.imageTitle)
				&& Objects.equals(statusUpdateId, that.statusUpdateId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(imageId, imageTitle, statusUpdateId);
	}
}
