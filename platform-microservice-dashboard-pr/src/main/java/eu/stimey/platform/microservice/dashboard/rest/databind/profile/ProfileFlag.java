package eu.stimey.platform.microservice.dashboard.rest.databind.profile;

public class ProfileFlag {
	public String userid;
	public boolean basicInfoFlag = true;
	public boolean educationFlag = true;
	public boolean experienceFlag = true;
	public boolean languageFlag = true;
	public boolean skillsFlag = true;
	public boolean certificationFlag = true;
	public boolean contactFlag = true;
	public boolean hobbiesFlag = true;
	public boolean badgesFlag = true;

	public ProfileFlag() {
	}
}
