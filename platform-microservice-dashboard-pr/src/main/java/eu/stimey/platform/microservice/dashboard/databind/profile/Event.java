package eu.stimey.platform.microservice.dashboard.databind.profile;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.stimey.platform.microservice.dashboard.databind.serializer.DateDeserializer;
import eu.stimey.platform.microservice.dashboard.databind.serializer.DateSerializer;

import java.util.Date;

public class Event {

	public String title;
	public String content;
	public String address;
	public Date start;
	public Date end;
	public String coverImageId;

	public Event() {

	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getStart() {
		return start;
	}

	@JsonDeserialize(using = DateDeserializer.class)
	public void setStart(Date start) {
		this.start = start;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getEnd() {
		return end;
	}

	@JsonDeserialize(using = DateDeserializer.class)
	public void setEnd(Date end) {
		this.end = end;
	}
}
