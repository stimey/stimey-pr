package eu.stimey.platform.microservice.dashboard.rest.databind.profile;

public class MessageRequestResponse {

	public String targetId;
	public boolean accepted;
	public boolean delete;

	public MessageRequestResponse() {
	}

	@Override
	public String toString() {
		return "MessageRequestResponse{" + "targetId='" + targetId + '\'' + ", accepted=" + accepted + ", delete="
				+ delete + '}';
	}
}
