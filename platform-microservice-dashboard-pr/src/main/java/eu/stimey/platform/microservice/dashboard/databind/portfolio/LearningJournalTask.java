package eu.stimey.platform.microservice.dashboard.databind.portfolio;

import java.util.Arrays;

public class LearningJournalTask {

	public String title;
	public String statusUpdateId;
	public String content;
	public LearningJournalTask[] subTasks;

	public LearningJournalTask() {
	}

	@Override
	public String toString() {
		return "LearningJournalTask [title=" + title + ", statusUpdateId=" + statusUpdateId + ", content=" + content
				+ ", subTasks=" + Arrays.toString(subTasks) + "]";
	}
}
