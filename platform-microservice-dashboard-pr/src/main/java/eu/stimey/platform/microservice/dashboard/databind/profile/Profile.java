package eu.stimey.platform.microservice.dashboard.databind.profile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;

public class Profile {

	public String userid;
	public String usertype;
	public String usersubtype;
	public String username;
	public String email;
	public boolean verified;

	public String avatarImageId;
	public String coverImageId;

	// still needed?
	// public String basicInfoPermission;

	public String skillsPermission;
	public String hobbiesPermission;
	public String languagesPermission;
	public int profile_process;

	public String school;
	public String rank;
	public String level;

	public BasicInfo basicInfo;

	public List<Education> education;
	public Education currentEducation;
	public List<Experience> experience;
	public Experience currentExperience;
	public List<Language> languages;
	public List<String> skills;
	public List<Certificate> certifications;
	public List<String> hobbies;

	public Showcase showcase;

	public List<String> statusUpdatesFromOtherUsers;

	public Set<String> friendRequests;
	public Set<String> friendRequestsOutgoing;
	public Set<String> friends;
	public Set<String> follower;
	public Set<String> following;
	public Set<BlockedUser> blockedUser;
	public HashMap<String, Boolean> messageRequests;
	public Set<String> childs;
	public Set<String> parents;
	public Set<String> requestFromParent;

	public boolean basicInfoFlag = true;
	public boolean educationFlag = true;
	public boolean experienceFlag = true;
	public boolean languageFlag = true;
	public boolean skillsFlag = true;
	public boolean certificationsFlag = true;
	public boolean contactFlag = true;
	public boolean hobbiesFlag = true;
	public boolean badgesFlag;

	public Profile() {
		this(null, null, null, null);
	}

	public Profile(String userId, String username, String usertype, String usersubtype) {
		this.userid = userId;
		this.username = username;
		this.usertype = usertype;
		this.usersubtype = usersubtype;
		this.friendRequests = new HashSet<>();
		this.friendRequestsOutgoing = new HashSet<>();
		this.friends = new HashSet<>();
		this.childs = new HashSet<>();
		this.parents = new HashSet<>();
		this.requestFromParent = new HashSet<>();
		this.follower = new HashSet<>();
		this.following = new HashSet<>();
		this.blockedUser = new HashSet<>();
		this.messageRequests = new HashMap<>();
		this.education = new ArrayList<>();
		this.experience = new ArrayList<>();
		this.languages = new ArrayList<>();
		this.skills = new ArrayList<>();
		this.certifications = new ArrayList<>();
		this.hobbies = new ArrayList<>();
		this.statusUpdatesFromOtherUsers = new ArrayList<>();
		this.basicInfo = new BasicInfo();
		this.showcase = new Showcase();
	}

	@JsonIgnore
	public boolean isUserInfoSet() {
		return this.usertype != null && this.usersubtype != null && this.userid != null && this.username != null
				&& this.email != null;
	}

	public int getProfile_process() {
		profile_process = 0;

		if (!education.isEmpty()) {
			profile_process = profile_process + 15;
		}
		if (!experience.isEmpty()) {
			profile_process = profile_process + 15;
		}
		if (!languages.isEmpty()) {
			profile_process = profile_process + 15;
		}
		if (!certifications.isEmpty()) {
			profile_process = profile_process + 15;
		}
		if (!skills.isEmpty() || !hobbies.isEmpty()) {
			profile_process = profile_process + 15;
		}
		if (basicInfo.birthdate != null && basicInfo.country != null) {
			profile_process = profile_process + 25;
		}

		return profile_process;
	}

	public void setProfile_process(int profile_process) {
		this.profile_process = profile_process;
	}

	@JsonProperty(value = "educationFlag")
	public boolean isEducationFlag() {
		if (this.usertype != null && this.usertype.equals("incubator")) {
			this.educationFlag = false;
		}
		return educationFlag;
	}

	@JsonProperty(value = "experienceFlag")
	public boolean isExperienceFlag() {
		if (this.usertype != null && this.usertype.equals("incubator")) {
			this.experienceFlag = false;
		}
		return experienceFlag;
	}

	@JsonProperty(value = "skillsFlag")
	public boolean isSkillsFlag() {
		return skillsFlag;
	}

	@JsonProperty(value = "certificationsFlag")
	public boolean isCertificationsFlag() {
		if (this.usertype != null && this.usertype.equals("incubator")) {
			this.certificationsFlag = false;
		}
		return certificationsFlag;
	}

	@JsonProperty(value = "hobbiesFlag")
	public boolean isHobbiesFlag() {
		if (this.usertype != null && this.usertype.equals("incubator")) {
			this.hobbiesFlag = false;
		}
		return hobbiesFlag;
	}

	@JsonProperty(value = "badgesFlag")
	public boolean isBadgesFlag() {
		if (this.usertype != null && this.usersubtype != null && this.usertype.equals("incubator")
				&& this.usersubtype.equals("school")) {
			this.badgesFlag = true;
		} else {
			this.badgesFlag = false;
		}
		return badgesFlag;
	}

	public void setFriendRequests(Set<String> friendRequests) {
		if (friendRequests == null)
			this.friendRequests = new HashSet<>();
		else {
			this.friendRequests = friendRequests;
		}
	}

	public void setFriends(Set<String> friends) {
		if (friends == null)
			this.friends = new HashSet<>();
		else {
			this.friends = friends;
		}
	}

	public void setFollower(Set<String> follower) {
		if (follower == null)
			this.follower = new HashSet<>();
		else {
			this.follower = follower;
		}
	}

	public void setFollowing(Set<String> following) {
		if (following == null)
			this.following = new HashSet<>();
		else {
			this.following = following;
		}
	}

	public void setBlockedUser(Set<BlockedUser> blockedUser) {
		if (blockedUser == null)
			this.blockedUser = new HashSet<>();
		else {
			this.blockedUser = blockedUser;
		}
	}

	public void setFriendRequestsOutgoing(Set<String> friendRequestsOutgoing) {
		if (friendRequestsOutgoing == null)
			friendRequestsOutgoing = new HashSet<>();
		this.friendRequestsOutgoing = friendRequestsOutgoing;
	}

	public void setMessageRequests(HashMap<String, Boolean> messageRequests) {
		if (messageRequests == null) {
			messageRequests = new HashMap<>();
		}
		this.messageRequests = messageRequests;
	}

	@Override
	public String toString() {
		return "Profile [userid=" + userid + ", basicInfo=" + basicInfo + ", education=" + education + ", experience="
				+ experience + ", languages=" + languages + ", Skills=" + skills + ", certifications=" + certifications
				+ ", hobbies=" + hobbies + "]";
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}
}
