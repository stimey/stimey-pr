package eu.stimey.platform.microservice.dashboard.databind.profile;

public class Language {
	public static final int LEVEL_BASIC = 25;
	public static final int LEVEL_GOOD = 50;
	public static final int LEVEL_FLUENT = 75;
	public static final int LEVEL_NATIVE = 100;

	public String name;
	public int level;

	public Language() {
		super();
	}

	public Language(String name, int level) {
		super();
		this.name = name;
		this.level = level;
	}

	@Override
	public String toString() {
		return "Language [name=" + name + ", level=" + level + "]";
	}

}
