package eu.stimey.platform.microservice.dashboard.databind.portfolio;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class Portfolio {
	public String userId;
	public String ePortfolioId;
	public Boolean hideEPortfolioInstruction;

	// basicInfo
	public String basicInfoUserPermission;
	public String realName;
	public String userName;
	public String birthday;
	public String email;
	public String country;
	public String school;
	public String bio;

	// statusOverview
	public String statusOverviewUserPermission;

	// album
	public String albumUserPermission;
	public List<EPortfolioImage> ePortfolioImages;

	// learningJournal
	public String learningJournalUserPermission;
	public String journalStatusUpdateId;
	public List<LearningJournalTask> learningJournalTasks;

	// old
	public List<LearningPlan> learningPlan;

	public boolean personalDataFlag;
	public boolean albumFlag;
	public boolean overviewFlag;
	public boolean learningJournalFlag;

	public String generatedPassword;

	private boolean showInstruction;

	public Portfolio() {
	}

	public Portfolio(String userId) {
		this.userId = userId;
		ePortfolioId = UUID.randomUUID().toString();
		this.hideEPortfolioInstruction = false;

		this.basicInfoUserPermission = "onlyMe";
		this.realName = "";
		this.userName = "";
		this.birthday = "";
		this.email = "";
		this.country = "";
		this.school = "";
		this.bio = "";

		statusOverviewUserPermission = "onlyMe";

		albumUserPermission = "onlyMe";
		ePortfolioImages = new LinkedList<>();

		learningJournalUserPermission = "onlyMe";
		journalStatusUpdateId = "";
		learningJournalTasks = new LinkedList<>();

		learningPlan = new LinkedList<>();
		this.personalDataFlag = true;
		this.albumFlag = true;
		this.overviewFlag = false;
		this.learningJournalFlag = false;
	}

	@JsonIgnore
	public EPortfolioImage getEPortfolioImageById(String imageId) {
		for (EPortfolioImage ePortfolioImage : this.ePortfolioImages) {
			if (ePortfolioImage.imageId.equals(imageId))
				return ePortfolioImage;
		}
		throw new IllegalArgumentException("portfolio does not contain image with id=" + imageId);
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public boolean isPersonalDataFlag() {
		return personalDataFlag;
	}

	public void setPersonalDataFlag(boolean personalDataFlag) {
		this.personalDataFlag = personalDataFlag;
	}

	public boolean isAlbumFlag() {
		return albumFlag;
	}

	public void setAlbumFlag(boolean albumFlag) {
		this.albumFlag = albumFlag;
	}

	public boolean isOverviewFlag() {
		return overviewFlag;
	}

	public void setOverviewFlag(boolean overviewFlag) {
		this.overviewFlag = overviewFlag;
	}

	public boolean isLearningJournalFlag() {
		return learningJournalFlag;
	}

	public void setLearningJournalFlag(boolean learningJournalFlag) {
		this.learningJournalFlag = learningJournalFlag;
	}

	public String getGeneratedPassword() {
		return generatedPassword;
	}

	public void setGeneratedPassword(String generatedPassword) {
		this.generatedPassword = generatedPassword;
	}

	public boolean isShowInstruction() {
		return showInstruction;
	}

	public void setShowInstruction(boolean showInstruction) {
		this.showInstruction = showInstruction;
	}

	public List<LearningPlan> getLearningPlan() {
		return learningPlan;
	}

	public void setLearningPlan(List<LearningPlan> learningPlan) {
		this.learningPlan = learningPlan;
	}

	public String getePortfolioId() {
		return ePortfolioId;
	}

	public void setePortfolioId(String ePortfolioId) {
		this.ePortfolioId = ePortfolioId;
	}

	public Boolean getHideEPortfolioInstruction() {
		return hideEPortfolioInstruction;
	}

	public void setHideEPortfolioInstruction(Boolean hideEPortfolioInstruction) {
		this.hideEPortfolioInstruction = hideEPortfolioInstruction;
	}

	public String getBasicInfoUserPermission() {
		return basicInfoUserPermission;
	}

	public void setBasicInfoUserPermission(String basicInfoUserPermission) {
		this.basicInfoUserPermission = basicInfoUserPermission;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getStatusOverviewUserPermission() {
		return statusOverviewUserPermission;
	}

	public void setStatusOverviewUserPermission(String statusOverviewUserPermission) {
		this.statusOverviewUserPermission = statusOverviewUserPermission;
	}

	public String getAlbumUserPermission() {
		return albumUserPermission;
	}

	public void setAlbumUserPermission(String albumUserPermission) {
		if (albumUserPermission == null) {
			albumUserPermission = "onlyMe";
		}
		this.albumUserPermission = albumUserPermission;
	}

	public List<EPortfolioImage> getePortfolioImages() {
		return ePortfolioImages;
	}

	public void setePortfolioImages(List<EPortfolioImage> ePortfolioImages) {
		this.ePortfolioImages = ePortfolioImages;
	}

	public String getLearningJournalUserPermission() {
		return learningJournalUserPermission;
	}

	public void setLearningJournalUserPermission(String learningJournalUserPermission) {
		if (learningJournalUserPermission == null) {
			learningJournalUserPermission = "onlyMe";
		}
		this.learningJournalUserPermission = learningJournalUserPermission;
	}

	public List<LearningJournalTask> getLearningJournalTasks() {
		return learningJournalTasks;
	}

	public void setLearningJournalTasks(List<LearningJournalTask> learningJournalTasks) {
		this.learningJournalTasks = learningJournalTasks;
	}

	public String getJournalStatusUpdateId() {
		return journalStatusUpdateId;
	}

	public void setJournalStatusUpdateId(String journalStatusUpdateId) {
		this.journalStatusUpdateId = journalStatusUpdateId;
	}

	@Override
	public String toString() {
		return "Portfolio{" + "userId='" + userId + '\'' + ", ePortfolioId='" + ePortfolioId + '\''
				+ ", hideEPortfolioInstruction=" + hideEPortfolioInstruction + ", basicInfoUserPermission='"
				+ basicInfoUserPermission + '\'' + ", realName='" + realName + '\'' + ", userName='" + userName + '\''
				+ ", birthday='" + birthday + '\'' + ", email='" + email + '\'' + ", country='" + country + '\''
				+ ", school='" + school + '\'' + ", bio='" + bio + '\'' + ", statusOverviewUserPermission='"
				+ statusOverviewUserPermission + '\'' + ", albumUserPermission='" + albumUserPermission + '\''
				+ ", ePortfolioImages=" + ePortfolioImages + ", learningJournalUserPermission='"
				+ learningJournalUserPermission + '\'' + ", journalStatusUpdateId='" + journalStatusUpdateId + '\''
				+ ", learningJournalTasks=" + learningJournalTasks + ", learningPlan=" + learningPlan
				+ ", personalDataFlag=" + personalDataFlag + ", albumFlag=" + albumFlag + ", overviewFlag="
				+ overviewFlag + ", learningJournalFlag=" + learningJournalFlag + ", generatedPassword='"
				+ generatedPassword + '\'' + ", showInstruction=" + showInstruction + '}';
	}
}
