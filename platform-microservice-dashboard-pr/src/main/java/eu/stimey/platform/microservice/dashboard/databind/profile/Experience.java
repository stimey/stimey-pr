package eu.stimey.platform.microservice.dashboard.databind.profile;

public class Experience {

	public String companyName;
	public String timePeriodFrom;
	public String timePeriodTo;
	public String title;
	public String fieldOfWork;
	public String description;
	public String permissions;

	public Experience() {
		super();
	}

	public Experience(String companyName, String timePeriodFrom, String timePeriodTo, String title, String fieldOfWork,
			String description, String permissions) {
		super();
		this.companyName = companyName;
		this.timePeriodFrom = timePeriodFrom;
		this.timePeriodTo = timePeriodTo;
		this.title = title;
		this.fieldOfWork = fieldOfWork;
		this.description = description;
		this.permissions = permissions;
	}

}
