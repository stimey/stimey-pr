package eu.stimey.platform.microservice.dashboard.beans;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.library.data.access.CachingConfiguration;
import eu.stimey.platform.microservice.dashboard.databind.profile.Profile;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProfileService {
	private /* final */ Caching<String, Profile> profiles;

	private UserSharedService userSharedService;

	public ProfileService(UserSharedService userSharedService) {
		super();
		this.userSharedService = userSharedService;
		CachingConfiguration configuration = new CachingConfiguration(RedisAMQPService.getService().getJedisPool(),
				RedisAMQPService.getService().getRabbitmqConnection(), MongoDBService.getService().getClient(),
				MongoDBService.getService().getDatabaseName());

		profiles = new Caching<>("ms_dashboard_profile", "userid", Profile.class, "profile", configuration);

		profiles.clear(); // clear cache
	}

	public Caching<String, Profile> getCache() {
		return profiles;
	}

	public Profile get(String userid) {
		// TODO: minimal effort soulution: Always set usertype, usersubtype and userid
		// new, because there are profiles
		// TODO: that do not have these properties set
		// TODO: Maybe better solution: one go through all profiles and set usertype and
		// usersubtype
		Profile profile = profiles.get(userid);
		final UserSharedCacheObject userSharedCacheObject = this.userSharedService.getUser(userid);
		if (userSharedCacheObject == null)
			return null;

		if (profile == null) {
			profile = new Profile(userSharedCacheObject.userid, userSharedCacheObject.username,
					userSharedCacheObject.usertype, userSharedCacheObject.usersubtype);
		}

		if (!profile.isUserInfoSet()) {
			profile.usertype = userSharedCacheObject.usertype;
			profile.usersubtype = userSharedCacheObject.usersubtype;
			profile.userid = userSharedCacheObject.userid;
			profile.username = userSharedCacheObject.username;
			profile.email = userSharedCacheObject.email;
		}
		profile.verified = userSharedCacheObject.user_verified;
		profiles.set(profile.getUserid(), profile);

		return profile;
	}

	public List<Profile> getProfiles(final Collection<String> userids) {
		if (userids == null)
			return new ArrayList<>();
		List<Profile> profiles = new ArrayList<>();
		for (final String userid : userids) {
			Profile profile = get(userid);
			if (profile != null)
				profiles.add(profile);
		}
		return profiles;
	}

	public void set(Profile profile) {
		profiles.set(profile.userid, profile);
	}
}
