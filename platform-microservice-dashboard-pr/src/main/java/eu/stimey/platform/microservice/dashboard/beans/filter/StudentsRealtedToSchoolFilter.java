package eu.stimey.platform.microservice.dashboard.beans.filter;

import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class StudentsRealtedToSchoolFilter implements DocFilter {

	private static final String JSON_USERTYPE_PATH = "usertype";
	private static final String JSON_SCHOOLNAME_OF_CURRENT_EDUCATION_PATH = "currentEducation.school";

	private final String realNameOfSchool;

	public StudentsRealtedToSchoolFilter(String realNameOfSchool) {
		this.realNameOfSchool = realNameOfSchool;
	}

	@Override
	public Document toDocument() {
		List<Document> query = new ArrayList<>();
		final Document studentsFilter = new Document(JSON_USERTYPE_PATH, "student");
		final Document schoolFilter = new Document(JSON_SCHOOLNAME_OF_CURRENT_EDUCATION_PATH, this.realNameOfSchool);
		query.add(studentsFilter);
		query.add(schoolFilter);
		return new Document("$and", query);
	}
}
