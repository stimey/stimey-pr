package eu.stimey.platform.microservice.dashboard.databind.profile;

import java.util.ArrayList;
import java.util.List;

public class Showcase {

	public List<String> videos;
	public List<String> images;
	public List<String> featuredMissions;
	public List<String> featuredWorlds;
	public List<Event> events;

	public Showcase() {
		this.videos = new ArrayList<>();
		this.images = new ArrayList<>();
		this.featuredMissions = new ArrayList<>();
		this.featuredWorlds = new ArrayList<>();
		this.events = new ArrayList<>();
	}

}
