package eu.stimey.platform.microservice.dashboard.databind.profile;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.stimey.platform.library.utils.databind.DateDeserializer;
import eu.stimey.platform.library.utils.databind.DateSerializer;

import java.util.Date;
import java.util.Objects;

public class BlockedUser {
	public String userid;
	public String username;
	private Date date;

	public BlockedUser() {
	}

	public BlockedUser(String userid, String username) {
		this.userid = userid;
		this.username = username;
		this.date = new Date();
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getDate() {
		return this.date;
	}

	@JsonDeserialize(using = DateDeserializer.class)
	public void setDate(Date startDate) {
		this.date = startDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		BlockedUser that = (BlockedUser) o;
		return Objects.equals(userid, that.userid) && Objects.equals(username, that.username);
	}

	@Override
	public int hashCode() {
		return Objects.hash(userid, username);
	}

	@Override
	public String toString() {
		return "BlockedUser{" + "userid='" + userid + '\'' + ", username='" + username + '\'' + ", date=" + date + '}';
	}
}
