package eu.stimey.platform.microservice.dashboard.databind.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Date;

public class DateSerializer extends JsonSerializer<Date> {
	@Override
	public void serialize(Date value, JsonGenerator gen, SerializerProvider serializers)
			throws IOException, JsonProcessingException {
		// gen.writeString(Global.dateFormat.format(value));
		gen.writeStartObject();
		// gen.writeNumber(value.getTime());
		gen.writeStringField("$numberLong", String.valueOf(value.getTime()));
		gen.writeEndObject();
	}
}