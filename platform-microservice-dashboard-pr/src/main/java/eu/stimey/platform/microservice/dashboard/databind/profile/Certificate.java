package eu.stimey.platform.microservice.dashboard.databind.profile;

import javax.validation.constraints.NotNull;

public class Certificate {
	@NotNull
	protected String certificateRecievedOn;
	@NotNull
	protected String titelOfCertification;
	@NotNull
	protected String issuer;
	protected String description;
	@NotNull
	protected String permissions;

	public Certificate() {
		super();
	}

	public Certificate(String certificateRecievedOn, String titelOfCertification, String issuer, String description,
			String permissions) {
		super();
		this.certificateRecievedOn = certificateRecievedOn;
		this.titelOfCertification = titelOfCertification;
		this.issuer = issuer;
		this.description = description;
		this.permissions = permissions;
	}

	public String getPermissions() {
		return permissions;
	}

	public void setPermissions(String permissions) {
		this.permissions = permissions;
	}

	public String getCertificateRecievedOn() {
		return certificateRecievedOn;
	}

	public void setCertificateRecievedOn(String certificateRecievedOn) {
		this.certificateRecievedOn = certificateRecievedOn;
	}

	public String getTitelOfCertification() {
		return titelOfCertification;
	}

	public void setTitelOfCertification(String titelOfCertification) {
		this.titelOfCertification = titelOfCertification;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Certifications [certificateRecievedOn=" + certificateRecievedOn + ", titelOfCertification="
				+ titelOfCertification + ", issuer=" + issuer + ", description=" + description + "]";
	}
}
