package eu.stimey.platform.microservice.dashboard.rest.databind.profile;

public class UserRequest {

	protected String userId;
	protected boolean requestAccepted;

	public UserRequest() {
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public boolean isRequestAccepted() {
		return requestAccepted;
	}

	public void setRequestAccepted(boolean requestAccepted) {
		this.requestAccepted = requestAccepted;
	}

	public String toString() {
		return "userId: " + this.userId + "  requestAccepted: " + this.requestAccepted;
	}
}
