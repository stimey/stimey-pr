package eu.stimey.platform.microservice.dashboard.beans;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.data.access.CachingConfiguration;
import eu.stimey.platform.library.data.access.VolatileCachingConfiguration;
import org.bson.types.ObjectId;

/**
 * Global variable settings for MongoDB and Redis
 *
 */
public class GlobalVariables {
	public static final ObjectId TEST_USER_ID = new ObjectId();
	public static final ObjectId ARCHIMEDIS_ID = new ObjectId("5a0ee2427e9d6459996d3c0b");
	public static final String SCHOOL_VERIFICATION_CODE = "STIMEY2019";

	public static final CachingConfiguration CACHE_CONFIG = new CachingConfiguration(
			RedisAMQPService.getService().getJedisPool(), RedisAMQPService.getService().getRabbitmqConnection(),
			MongoDBService.getService().getClient(), MongoDBService.getService().getDatabaseName());

	public static final VolatileCachingConfiguration VOLATILE_CACHE_CONFIG = new VolatileCachingConfiguration(
			RedisAMQPService.getService().getJedisPool(), RedisAMQPService.getService().getRabbitmqConnection());
}
