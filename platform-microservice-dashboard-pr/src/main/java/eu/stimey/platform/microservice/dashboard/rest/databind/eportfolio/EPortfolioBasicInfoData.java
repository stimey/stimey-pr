package eu.stimey.platform.microservice.dashboard.rest.databind.eportfolio;

public class EPortfolioBasicInfoData {

	protected String basicInfoUserPermission;
	protected String realName;
	protected String userName;
	protected String birthday;
	protected String email;
	protected String country;
	protected String school;
	protected String bio;

	EPortfolioBasicInfoData() {
	}

	EPortfolioBasicInfoData(String basicInfoUserPermission, String realName, String userName, String birthday,
			String email, String country, String school, String bio) {
		this.basicInfoUserPermission = basicInfoUserPermission;
		this.realName = realName;
		this.userName = userName;
		this.birthday = birthday;
		this.email = email;
		this.country = country;
		this.school = school;
		this.bio = bio;
	}

	public String getBasicInfoUserPermission() {
		return basicInfoUserPermission;
	}

	public void setBasicInfoUserPermission(String basicInfoUserPermission) {
		this.basicInfoUserPermission = basicInfoUserPermission;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}
}
