package eu.stimey.platform.microservice.dashboard.rest.databind.profile;

public class RestrictedUserSharedCacheObject {

	private String userName;
	private String userType;

	public RestrictedUserSharedCacheObject() {
	}

	public RestrictedUserSharedCacheObject(String userName, String userType) {
		this.userName = userName;
		this.userType = userType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
}
