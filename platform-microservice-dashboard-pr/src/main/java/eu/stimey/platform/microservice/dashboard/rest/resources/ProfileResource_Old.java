package eu.stimey.platform.microservice.dashboard.rest.resources;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;
import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.dashboard.beans.ProfileService;
import eu.stimey.platform.microservice.dashboard.databind.profile.*;
import eu.stimey.platform.microservice.dashboard.rest.databind.profile.Friend;
import eu.stimey.platform.microservice.dashboard.rest.databind.profile.ProfileFlag;
import eu.stimey.platform.microservice.dashboard.rest.databind.profile.RestrictedUserProfile;
import eu.stimey.platform.microservice.dashboard.rest.databind.profile.RestrictedUserSharedCacheObject;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

@Path("/profile")
public class ProfileResource_Old {

	@Inject
	private ProfileService profileService;
	@Inject
	private UserSharedService userSharedService;

	/**
	 * Update the profileFlags for all Information Types of a User.
	 *
	 * @param requestContext   grants access to the user id
	 * @param profileFlagInput is needed to update all flags of a Profile
	 * @return an updated Profile Object
	 */

	@POST
	@Path("/user")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveProfileSetup(@Context ContainerRequestContext requestContext, ProfileFlag profileFlagInput) {

		String userid = (String) requestContext.getProperty("userid");

		Profile profile = profileService.get(userid);

		profile.userid = userid;

		profile.experienceFlag = profileFlagInput.experienceFlag;
		profile.certificationsFlag = profileFlagInput.certificationFlag;
		profile.educationFlag = profileFlagInput.educationFlag;
		profile.languageFlag = profileFlagInput.languageFlag;
		profile.skillsFlag = profileFlagInput.skillsFlag;
		profile.badgesFlag = profileFlagInput.badgesFlag;
		profile.contactFlag = profileFlagInput.contactFlag;
		profile.hobbiesFlag = profileFlagInput.hobbiesFlag;
		profile.basicInfoFlag = profileFlagInput.basicInfoFlag;

		profileService.set(profile);

		Profile temp = profileService.get(userid);

		return Response.status(Response.Status.OK).entity(temp).build();
	}

	/**
	 * Using the delivered userids to get user information
	 *
	 * @param requestContext grants access to the user id.
	 * @param userid         is the userid of the knowing usertype.
	 * @return the usertype of the username.
	 */
	@GET
	@Path("/findById")
	@Produces(MediaType.APPLICATION_JSON)
	public Response profileWithUserType(@Context ContainerRequestContext requestContext,
			@QueryParam("userid") @DefaultValue("") String userid, @QueryParam("userids[]") List<String> userIds) {
		List<Friend> result = new LinkedList<>();

		for (String userId : userIds) {
			Profile userProfile = profileService.get(userId);
			UserSharedCacheObject user = userSharedService.getUser(userId);

			Friend friend = new Friend(user.userid, user.username, user.usertype, userProfile.avatarImageId);
			result.add(friend);
		}
		return Response.status(200).entity(result).build();
	}

	/**
	 * Method for EPortfolio to get needed information from User (specific for
	 * Usertype) (Method is usable for unauthenticated User)
	 *
	 * @param userid is used to get the UserSharedCacheObject
	 * @return a limited UserSharedCacheObject
	 */
	@GET
	@Path("/restrictedlogininfo")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRestrictedLoginInformation(@QueryParam("userid") @DefaultValue("") String userid) {

		UserSharedCacheObject user = userSharedService.getUser(userid);
		RestrictedUserSharedCacheObject userObject = new RestrictedUserSharedCacheObject(user.username, user.usertype);

		return Response.status(200).entity(userObject).build();
	}

	/**
	 * Add a statusUpdate to a specific User -> access through UserId
	 *
	 * @param requestContext grants access to the actual UserId
	 * @param userid         grants access to the Profile of the specific User
	 * @param statusUpdateId is the added value to the statusUpdates of the specific
	 *                       User
	 * @return the List of all for the specific User generated statusUpdate Ids
	 */
	@POST
	@Path("/statusUpdate")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateStatusUpdatesFromOtherUser(@Context ContainerRequestContext requestContext,
			@QueryParam("userid") String userid, String statusUpdateId) {
		Profile profile = profileService.get(userid);
		List<String> statusUpdates = profile.statusUpdatesFromOtherUsers;
		statusUpdates.add(statusUpdateId);
		profile.statusUpdatesFromOtherUsers = statusUpdates;
		profileService.set(profile);

		return Response.status(200).entity(statusUpdates).build();
	}

	/**
	 * Import a list of UserIds and get the Image Ids as HashMap.
	 *
	 * @param requestContext grants access to the actual User id.
	 * @param userIds        a List of user Ids
	 * @return a Map of Userid-keys to avatarImageId-values.
	 */
	@GET
	@Path("/image")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAvatarImage(@Context ContainerRequestContext requestContext,
			@QueryParam("userids[]") List<String> userIds) {

		Map<String, String> avatarImageIds = new HashMap<>();

		if (userIds == null)
			return Response.status(Response.Status.NO_CONTENT).build();

		// if (userIds != null) {
		for (String id : userIds) {
			String avatarImage = profileService.get(id).avatarImageId;

			avatarImageIds.put(id, avatarImage);
			// if (avatarImage != null) {
			// avatarImageIds.put(id, avatarImage);
			// } else {
			// avatarImageIds.put(id, null);
			// }
		}
		// }

		if (avatarImageIds.keySet().isEmpty())
			return Response.status(Response.Status.NO_CONTENT).build();

		return Response.status(200).entity(avatarImageIds).build();
	}

	/**
	 * Update the cover or avatar picture of the current user.
	 *
	 * @param requestContext grants access to the User Id.
	 * @param imageId        is the ID of the updating picture.
	 * @param isAvatar       checks if cover or avatar picture will be changed.
	 * @return the updated profile of the actual user.
	 */

	@POST
	@Path("/image")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateImage(@Context ContainerRequestContext requestContext,
			@QueryParam("id") @DefaultValue("") String imageId,
			@QueryParam("isAvatar") @DefaultValue("false") boolean isAvatar) {

		String userid = (String) requestContext.getProperty("userid");
		Profile profile = profileService.get(userid);
		if (isAvatar) {
			profile.avatarImageId = imageId;
		} else {
			profile.coverImageId = imageId;
		}
		profileService.set(profile);

		return Response.status(200).entity(profile).build();
	}

	@GET
	@Path("/myfriends")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFriendInformationFromId(@Context ContainerRequestContext requestContext) {
		List<Friend> friends = new LinkedList<>();

		String userid = (String) requestContext.getProperty("userid");

		Profile profile = profileService.get(userid);
		if (profile != null && profile.friends != null)
			for (String id : profile.friends) {
				UserSharedCacheObject user = userSharedService.getUser(id);
				if (user != null) {
					Friend friend = new Friend(id, user.username, user.usertype, profileService.get(id).avatarImageId);
					friends.add(friend);
				}
			}

		return Response.status(200).entity(friends).build();
	}

	@GET
	@Path("/friend/{friendId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFriendInformationFromId(@Context ContainerRequestContext requestContext,
			@PathParam("friendId") String id) {

		UserSharedCacheObject user = userSharedService.getUser(id);
		Profile profile = profileService.get(id);

		Friend friend = new Friend(id, user.username, user.usertype, profileService.get(id).avatarImageId);

		return Response.status(200).entity(friend).build();
	}

	/**
	 * Get Friend-Objects from a List of FriendIds which are delivered. Important
	 * information in Friend-Object: UserId, UserName, UserType.
	 *
	 * @param requestContext grants access to the User ID
	 * @param friendIds      is a list of UserIds. It's needed to get the necessary
	 *                       Information
	 * @return A List of Friends/FriendRequests
	 */

	@GET
	@Path("/friends")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFriendInformationFromId(@Context ContainerRequestContext requestContext,
			@QueryParam("friendIds[]") List<String> friendIds) {

		List<Friend> friends = new LinkedList<>();

		for (String id : friendIds) {
			UserSharedCacheObject user = userSharedService.getUser(id);
			Profile profile = profileService.get(id);

			Friend friend = new Friend(id, user.username, user.usertype, profile.avatarImageId);
			friends.add(friend);
		}

		return Response.status(200).entity(friends).build();
	}

	/**
	 * Get Friend-Objects from a List of FriendIds which are delivered. Important
	 * information in Friend-Object: UserId, UserName, UserType.
	 *
	 * @param requestContext grants access to the User ID
	 * @param memberIds      is a list of UserIds. It's needed to get the necessary
	 *                       Information
	 * @return A List of Friends/FriendRequests
	 */
	@GET
	@Path("/commMembers")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCommMembers(@Context ContainerRequestContext requestContext,
			@QueryParam("memberIds[]") List<String> memberIds) {

		List<Friend> friends = new LinkedList<>();

		for (String id : memberIds) {

			UserSharedCacheObject user = userSharedService.getUser(id);
			Profile profile = profileService.get(id);

			Friend friend = new Friend(id, user.username, user.usertype, profile.avatarImageId);
			friend.setFriendRequests(new ArrayList<>(profile.friendRequests));
			friends.add(friend);
		}

		return Response.status(200).entity(friends).build();
	}

}