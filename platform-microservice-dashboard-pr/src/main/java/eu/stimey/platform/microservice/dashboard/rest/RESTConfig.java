package eu.stimey.platform.microservice.dashboard.rest;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.microservice.dashboard.beans.PortfolioService;
import eu.stimey.platform.microservice.dashboard.beans.*;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import javax.annotation.PreDestroy;
import javax.ws.rs.ApplicationPath;

import static eu.stimey.platform.library.utils.StimeyLogger.logger;

/**
 * 
 * REST configuration
 *
 */
@ApplicationPath("api")
public class RESTConfig extends ResourceConfig {
	protected final MongoDBService mongoDBService;
	protected final RedisAMQPService redisAMQPService;

	public RESTConfig() {
		super();

		mongoDBService = MongoDBService.getService();
		redisAMQPService = RedisAMQPService.getService();

		logger().info("REST-Service started...");

		packages("eu.stimey.platform.microservice.dashboard.rest");

		UserSharedService userSharedService = new UserSharedService();
		ProfileService profileService = new ProfileService(userSharedService);
		PortfolioService portfolioService = new PortfolioService();

		register(new AbstractBinder() {
			protected void configure() {
				bind(mongoDBService).to(MongoDBService.class);
				bind(redisAMQPService).to(RedisAMQPService.class);
				bind(userSharedService).to(UserSharedService.class);
				bind(profileService).to(ProfileService.class);
				bind(portfolioService).to(PortfolioService.class);
			}
		});

		register(new JacksonJsonProvider().configure(SerializationFeature.INDENT_OUTPUT, true));
	}

	@PreDestroy
	public void shutdown() {
		logger().info("REST-Service stopped...");
	}
}
