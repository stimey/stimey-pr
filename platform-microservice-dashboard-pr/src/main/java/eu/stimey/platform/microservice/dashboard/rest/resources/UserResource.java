package eu.stimey.platform.microservice.dashboard.rest.resources;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

	@Inject
	private UserSharedService userSharedService;

	@GET
	@Path("/{id}")
	public Response getUser(@PathParam("id") String userId) {
		final UserSharedCacheObject userSharedCacheObject = this.userSharedService.getUser(userId);
		if (userSharedCacheObject == null)
			return Response.status(Response.Status.NOT_FOUND).build();
		return Response.status(Response.Status.OK).entity(userSharedCacheObject).build();
	}
}
