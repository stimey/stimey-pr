package eu.stimey.platform.microservice.dashboard.databind.profile;

public class Education {

	public String school;
	public String dateAttendedFrom;
	public String dateAttendedTo;
	// protected boolean currentEducation;
	public String degree;
	public String major;
	public String fieldOfStudy;
	public String activitiesAndClubs;
	public String permissions;

	public Education() {
		super();
	}

	public Education(String school, String dateAttendedFrom, String dateAttendedTo, String degree, String major,
			String fieldOfStudy, String activitiesAndClubs, String permissions) {
		super();
		this.school = school;
		this.dateAttendedFrom = dateAttendedFrom;
		this.dateAttendedTo = dateAttendedTo;
		this.degree = degree;
		this.major = major;
		this.fieldOfStudy = fieldOfStudy;
		this.activitiesAndClubs = activitiesAndClubs;
		this.permissions = permissions;
	}
}
