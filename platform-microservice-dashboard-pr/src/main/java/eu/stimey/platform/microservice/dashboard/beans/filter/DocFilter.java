package eu.stimey.platform.microservice.dashboard.beans.filter;

import org.bson.Document;

public interface DocFilter {
	Document toDocument();
}
