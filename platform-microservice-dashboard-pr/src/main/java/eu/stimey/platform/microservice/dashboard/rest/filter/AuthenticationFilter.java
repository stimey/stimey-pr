package eu.stimey.platform.microservice.dashboard.rest.filter;

import eu.stimey.platform.library.rest.filter.DefaultAuthenticationFilter;
import eu.stimey.platform.library.utils.security.JsonWebToken;
import eu.stimey.platform.microservice.dashboard.startup.Global;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import redis.clients.jedis.Jedis;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import java.util.HashMap;
import java.util.Map;

/**
 * AuthenticationFilter for REST, checks for allowed access (successful authentication)
 *
 */
@Provider
public class AuthenticationFilter extends DefaultAuthenticationFilter {
	static {
		whiteList.put("portfolio/information", true);
		whiteList.put("portfolio/password", true);
		whiteList.put("portfolio/image", true);
		whiteList.put("portfolio/missions", true);
		whiteList.put("portfolio/worlds", true);
		whiteList.put("portfolio/achievements", true);
		whiteList.put("portfolio/statusupdate", true);
		whiteList.put("portfolio/content", true);
		// whiteList.put("profile/userrelation", true);
		whiteList.put("profile/restrictedlogininfo", true);
		whiteList.put("profile/restricteduser", true);
	}
}
