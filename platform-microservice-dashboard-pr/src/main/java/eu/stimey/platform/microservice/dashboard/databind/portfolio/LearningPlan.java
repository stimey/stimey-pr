package eu.stimey.platform.microservice.dashboard.databind.portfolio;

public class LearningPlan {

	protected boolean permission;
	protected String scription;
	protected int index;

	public LearningPlan() {
		super();
	}

	public LearningPlan(boolean permission, String scription) {
		super();
		this.permission = permission;
		this.scription = scription;
	}

	public boolean getPermission() {
		return permission;
	}

	public void setPermission(boolean permission) {
		this.permission = permission;
	}

	public String getScription() {
		return scription;
	}

	public void setScription(String scription) {
		this.scription = scription;
	}

}
