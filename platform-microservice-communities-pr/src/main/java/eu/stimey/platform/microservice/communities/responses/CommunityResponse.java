package eu.stimey.platform.microservice.communities.responses;

import java.util.LinkedList;
import java.util.List;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.microservice.communities.databind.Community;

public class CommunityResponse {

	public String communityID;
	public String communityName;
	public String creatorID;
	public String creatorName;
	public String relatedCourseID;
	public String relatedCourseName;
	public String privacySetting;
	public String description;
	public String communityPhoto;
	public String memberType;
	public List<String> keywords;

	private List<String> mems;
	private List<String> mods;
	public List<Object> members;
	public List<Object> moderators;
	public List<String> blocked;

	// public List<> announcements;
	public List<String> photos;
	public List<String> files;
	public List<String> discussions;

	public List<Object> requests;
	public List<Object> invites;

	public List<Object> friends;

	public Object notification;

	public CommunityResponse(UserSharedService service, Community community, String userid) {
		this(service, community, userid, null);
	}

	public CommunityResponse(UserSharedService service, Community community, String userid, List<String> friends) {
		this.communityID = community.getCommunityID();
		this.communityName = community.getCommunityName();
		this.creatorID = community.getCreator();
		this.creatorName = service.getUser(creatorID).username;
		this.relatedCourseID = community.getRelatedCourseID();
		this.relatedCourseName = "";
		this.privacySetting = String.valueOf(community.getPrivacySetting());
		this.description = community.getDescription();
		this.communityPhoto = community.getCommunityPhoto();
		this.mems = community.getMembers();
		this.mods = community.getModerators();

		if (creatorID.equals(userid)) {
			memberType = "creator";
		} else if (mods.contains(userid)) {
			memberType = "mod";
		} else if (mems.contains(userid)) {
			memberType = "member";
		}
		this.keywords = community.getKeywords();
		this.discussions = community.getDiscussions();

		// if (community.getDiscussions() != null) {
		// this.discussions = new LinkedList<>();
		// List<Discussion> comms = community.getDiscussions();
		// Collections.sort(comms);
		// for (Discussion disc : comms) {
		// DiscussionResponse response = new DiscussionResponse(service, disc);
		// this.discussions.add(response);
		// }
		// }

		// announcements = new LinkedList<>();
		this.photos = community.getPhotos();
		this.files = community.getFiles();

		this.requests = new LinkedList<>();
		for (String s : community.getRequests()) {
			UserResponse user = new UserResponse(service, s);
			this.requests.add(user);
		}

		this.invites = new LinkedList<>();
		for (String s : community.getInvites()) {
			UserResponse user = new UserResponse(service, s);
			this.invites.add(user);
		}

		this.members = new LinkedList<>();
		for (String s : mems) {
			UserResponse user = new UserResponse(service, s);
			this.members.add(user);
		}

		this.moderators = new LinkedList<>();
		for (String s : mods) {
			UserResponse user = new UserResponse(service, s);
			this.moderators.add(user);
		}

		this.blocked = community.getBlocked();

		if (friends != null) {
			this.friends = new LinkedList<>();
			for (String s : friends) {
				// only friends who aren't already in the community
				if (!(mems.contains(s) || mods.contains(s) || community.getBlocked().contains(s)
						|| community.getInvites().contains(s) || community.getRequests().contains(s))) {
					UserResponse user = new UserResponse(service, s);
					this.friends.add(user);
				}
			}
		}

	}

}
