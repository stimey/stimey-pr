package eu.stimey.platform.microservice.communities.responses;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;

public class UserResponse {

	public String userid;
	public String username;
	public String usertype;
	public String memberType;
	public String avatarImageID;
	public String userrank;
	public String userlevel;

	public UserResponse(UserSharedService service, String userid) {
		// Profile userProfile = profileService.get(userid);
		// System.out.println("Profile: " + userProfile.toString());
		UserSharedCacheObject user = service.getUser(userid);
		this.userid = userid;
		this.username = user.username;
		this.usertype = user.usertype;
		this.avatarImageID = "";
		this.userrank = "Explorer";
		this.userlevel = "2";
	}

	@Override
	public String toString() {
		return "UserResponse [userid=" + userid + ", username=" + username + ", usertype=" + usertype
				+ ", avatarImageID=" + avatarImageID + ", userrank=" + userrank + ", userlevel=" + userlevel + "]";
	}

}
