package eu.stimey.platform.microservice.communities.responses;

import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;

import eu.stimey.platform.microservice.communities.databind.Community;

public class TestResponse {

	public String test;

	public TestResponse(ContainerRequestContext context, String t) {

		this.test = t;

	}

}