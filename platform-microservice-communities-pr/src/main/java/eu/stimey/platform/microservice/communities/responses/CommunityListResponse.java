package eu.stimey.platform.microservice.communities.responses;

import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.microservice.communities.databind.Community;

public class CommunityListResponse {

	public List<Object> allComms;
	public List<Object> pendingComms;
	public List<Object> managedComms;
	public List<Object> memberComms;
	public List<Object> interestingComms;
	public List<Object> popularComms;
	public List<Object> missionComms;
	public List<Object> foundComms;

	public List<String> friends;

	public CommunityListResponse(UserSharedService service, List<Community> all, List<Community> pending,
			List<Community> managed, List<Community> member, List<Community> interesting, List<Community> popular,
			List<Community> missions, List<Community> found, List<String> friends) {

		this.allComms = new LinkedList<>();
		this.pendingComms = new LinkedList<>();
		this.managedComms = new LinkedList<>();
		this.memberComms = new LinkedList<>();
		this.interestingComms = new LinkedList<>();
		this.popularComms = new LinkedList<>();
		this.missionComms = new LinkedList<>();
		this.foundComms = new LinkedList<>();
		this.friends = friends;

		if (all != null) {
			for (Community comm : all) {
				CommunityOverviewResponse response = new CommunityOverviewResponse(service, comm, friends);
				this.allComms.add(response);
			}
		}

		if (pending != null) {
			for (Community comm : pending) {
				CommunityOverviewResponse response = new CommunityOverviewResponse(service, comm, friends);
				this.pendingComms.add(response);
			}
		}

		if (managed != null) {
			for (Community comm : managed) {
				CommunityOverviewResponse response = new CommunityOverviewResponse(service, comm, friends);
				this.managedComms.add(response);
			}
		}

		if (member != null) {
			for (Community comm : member) {
				CommunityOverviewResponse response = new CommunityOverviewResponse(service, comm, friends);
				this.memberComms.add(response);
			}
		}

		if (interesting != null) {
			for (Community comm : interesting) {
				CommunityOverviewResponse response = new CommunityOverviewResponse(service, comm, friends);
				this.interestingComms.add(response);
			}
		}

		if (popular != null) {
			for (Community comm : popular) {
				CommunityOverviewResponse response = new CommunityOverviewResponse(service, comm, friends);
				this.popularComms.add(response);
			}
		}

		if (missions != null) {
			for (Community comm : missions) {
				CommunityOverviewResponse response = new CommunityOverviewResponse(service, comm, friends);
				this.missionComms.add(response);
			}
		}

		if (found != null) {
			for (Community comm : found) {
				CommunityOverviewResponse response = new CommunityOverviewResponse(service, comm, friends);
				this.foundComms.add(response);
			}
		}

	}

}
