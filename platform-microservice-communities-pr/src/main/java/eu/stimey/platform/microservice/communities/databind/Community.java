package eu.stimey.platform.microservice.communities.databind;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

//@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class Community {

	private String communityID;
	private String communityName;
	private String creator;
	private String relatedCourseID;
	private PrivacySetting privacySetting;
	private String description;
	private String communityPhoto;
	private List<String> keywords;

	private List<String> members;
	private List<String> moderators;
	private List<String> blocked;

	private List<Announcement> announcements;
	private List<String> photos;
	private List<String> files;
	// private List<Discussion> discussions;
	private List<String> discussions;

	private List<String> requests;
	private List<String> invites;

	public Community() {
		this(null, null, null, null, null, null, null);
	}

	public Community(String communityID, String communityName, String creator, String relatedCourseID,
			PrivacySetting privacy, String description, List<String> keyWords) {

		this.communityID = communityID;
		this.communityName = communityName;
		this.creator = creator;
		this.relatedCourseID = relatedCourseID;
		this.privacySetting = privacy;
		this.description = description;
		this.keywords = keyWords;

		this.discussions = new LinkedList<>();
		this.announcements = new LinkedList<>();
		this.photos = new LinkedList<>();
		this.files = new LinkedList<>();
		this.requests = new LinkedList<>();
		this.invites = new LinkedList<>();
		this.members = new LinkedList<>();
		this.moderators = new LinkedList<>();
		this.blocked = new LinkedList<>();

	}

	public String getCommunityID() {
		return communityID;
	}

	public void setCommunityID(String communityID) {
		this.communityID = communityID;
	}

	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getRelatedCourseID() {
		return relatedCourseID;
	}

	public void setRelatedCourseID(String relatedCourseID) {
		this.relatedCourseID = relatedCourseID;
	}

	public PrivacySetting getPrivacySetting() {
		return privacySetting;
	}

	public void setPrivacySetting(PrivacySetting privacySetting) {
		this.privacySetting = privacySetting;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCommunityPhoto() {
		return communityPhoto;
	}

	public void setCommunityPhoto(String communityPhoto) {
		this.communityPhoto = communityPhoto;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	public List<String> getMembers() {
		return members;
	}

	public void setMembers(List<String> members) {
		this.members = members;
	}

	public List<String> getModerators() {
		return moderators;
	}

	public void setModerators(List<String> moderators) {
		this.moderators = moderators;
	}

	public List<String> getBlocked() {
		return blocked;
	}

	public void setBlocked(List<String> blocked) {
		this.blocked = blocked;
	}

	public List<Announcement> getAnnouncements() {
		return announcements;
	}

	public void setAnnouncements(List<Announcement> announcements) {
		this.announcements = announcements;
	}

	public List<String> getPhotos() {
		return photos;
	}

	public void setPhotos(List<String> photos) {
		this.photos = photos;
	}

	public List<String> getFiles() {
		return files;
	}

	public void setFiles(List<String> files) {
		this.files = files;
	}

	// public Discussion getDiscussion(String id) {
	// return discussions
	// .stream()
	// .filter(d -> d.getDiscussionID().equals(id))
	// .findAny()
	// .orElse(null);
	// }

	public List<String> getDiscussions() {
		return discussions;
	}

	// public List<Discussion> getDiscussions() {
	// return discussions;
	// }

	public void addDiscussion(String statusUpdateId) {
		this.discussions.add(statusUpdateId);
	}

	public void setDiscussions(List<String> discussions) {
		this.discussions = discussions;
	}

	public List<String> getRequests() {
		return requests;
	}

	public void setRequests(List<String> requests) {
		this.requests = requests;
	}

	public List<String> getInvites() {
		return invites;
	}

	public void setInvites(List<String> invites) {
		this.invites = invites;
	}

	@Override
	public String toString() {
		return "Community [communityID=" + communityID + ", communityName=" + communityName + ", creator=" + creator
				+ ", relatedCourseID=" + relatedCourseID + ", privacySetting=" + privacySetting + ", description="
				+ description + ", communityPhoto=" + communityPhoto + ", keywords=" + keywords + ", members=" + members
				+ ", moderators=" + moderators + ", blocked=" + blocked + ", announcements=" + announcements
				+ ", photos=" + photos + ", files=" + files + ", discussions=" + discussions + ", requests=" + requests
				+ ", invites=" + invites + "]";
	}

}
