package eu.stimey.platform.microservice.communities.beans.filter;

import org.bson.Document;

public interface DocumentFilter {
	Document toDocument();
}
