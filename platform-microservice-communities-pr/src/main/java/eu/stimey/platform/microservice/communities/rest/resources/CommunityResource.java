package eu.stimey.platform.microservice.communities.rest.resources;

import static eu.stimey.platform.library.utils.StimeyLogger.logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import eu.stimey.platform.microservice.communities.rest.client.NotificationClient;
import org.bson.types.ObjectId;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.library.data.access.CachingConfiguration;
import eu.stimey.platform.library.data.access.databind.Message;
import eu.stimey.platform.microservice.communities.rest.client.DashboardClient;
import eu.stimey.platform.microservice.communities.beans.CommunityService;
import eu.stimey.platform.microservice.communities.databind.Community;
import eu.stimey.platform.microservice.communities.databind.Creativity;
import eu.stimey.platform.microservice.communities.databind.CreativityData;
import eu.stimey.platform.microservice.communities.databind.PrivacySetting;
import eu.stimey.platform.microservice.communities.databind.QuestionData;
import eu.stimey.platform.microservice.communities.databind.ValueData;
import eu.stimey.platform.microservice.communities.requests.CommunityRequest;
import eu.stimey.platform.microservice.communities.requests.DiscussionRequest;
import eu.stimey.platform.microservice.communities.requests.FileRequest;
import eu.stimey.platform.microservice.communities.requests.UserRequest;
import eu.stimey.platform.microservice.communities.responses.CommunityResponse;
import eu.stimey.platform.microservice.communities.responses.TestResponse;
import eu.stimey.platform.microservice.communities.rest.utils.RESTUri;

@Path("/community")
public class CommunityResource {

	@Inject
	CommunityService commService;
	@Inject
	UserSharedService userSharedService;

	// to get friend list from user
	private DashboardClient dashboardClient;

	// Cookie is needed for dashBoardClient
	public CommunityResource(@Context ContainerRequestContext context, @CookieParam("access_token") Cookie cookie) {
		this.dashboardClient = new DashboardClient();
		this.dashboardClient.setCookie(cookie);
	}

	/**
	 * 
	 * @param context
	 * @param commid
	 * @return
	 * 
	 *         Returns the requested Community. Needs the Community-ID
	 * 
	 */
	@GET
	@Path("/{commid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCommunity(@Context ContainerRequestContext context, @PathParam("commid") String commid) {

		Community community = commService.getCommunity(commid);

		String userid = (String) context.getProperty("userid");
		String username = (String) context.getProperty("username");

		// set Username for Cookies and to get Friends from other Microservice
		// (Dashboard)
		dashboardClient.setUsername(username);
		List<String> friends = this.dashboardClient.getOwnFriends();

		List<String> members = community.getMembers().stream().distinct().collect(Collectors.toList());
		List<String> mods = community.getModerators().stream().distinct().collect(Collectors.toList());

		community.setMembers(members);
		community.setModerators(mods);

		CommunityResponse response = new CommunityResponse(userSharedService, community, userid, friends);

		// if (!community.getPrivacySetting().equals(PrivacySetting.PUBLIC)) {
		//
		// if (community.getMembers().contains(userid) ||
		// community.getModerators().contains(userid)) {
		// response = new CommunityResponse(userSharedService, community, userid, null,
		// friends);
		// }
		// } else {
		// response = new CommunityResponse(userSharedService, community, userid, null,
		// friends);
		// }

		commService.setCommunity(commid, community);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context))
				.entity((response == null) ? new TestResponse(context, "No MEMBER") : response).build();
	}

	/**
	 * 
	 * @param context
	 * @param commid
	 * @return
	 * 
	 *         Creates a Discussion within a Community
	 * 
	 *         Needs: - Community-ID - DiscussionRequest with all the Info about the
	 *         posted discussion (Text, Pictures, Videos, Files)
	 * 
	 *         Returns the community, where the discussion was posted
	 * 
	 */
	@POST
	@Path("discussion/{commid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response postDiscussion(@Context ContainerRequestContext context, DiscussionRequest statusUpdateId,
			@PathParam("commid") String commid, @CookieParam("access_token") Cookie cookie) {

		String userid = (String) context.getProperty("userid");
		Community community = commService.getCommunity(commid);

		community.addDiscussion(statusUpdateId.statusID);
		commService.setCommunity(commid, community);

		CommunityResponse response = new CommunityResponse(userSharedService, community, userid, null);

		HashSet<String> members = new HashSet<>();
		members.addAll(community.getMembers());
		members.addAll(community.getModerators());
		// Remove own ID
		members.remove(userid);

		ArrayList<String> creatorList = new ArrayList<>();
		creatorList.add(userid);

		System.out.println("COMMUNITY: " + members);

		NotificationClient.broadcastNotification(members, "community_" + community.getCommunityName(), creatorList,
				community.getCommunityName(), "community/" + commid, "notification.posted",
				"ICON-COMMUNITIES_circle.png", cookie);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();

	}

	@GET
	@Path("update/{commid}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateDiscussions(@Context ContainerRequestContext context, @PathParam("commid") String commid,
			@QueryParam("ids[]") List<String> list) {

		Community community = commService.getCommunity(commid);
		community.setDiscussions(list);
		commService.setCommunity(commid, community);

	}

	/**
	 * 
	 * @param context
	 * @param request
	 * @param commid
	 * @return
	 * 
	 *         Edits the Community
	 * 
	 *         Needs: - Community-ID - CommunityRequest (Info: Name, Description,
	 *         Keywords, PrivacySetting)
	 * 
	 *         returns the changed Community
	 * 
	 */
	@POST
	@Path("edit/{commid}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response editCommunity(@Context ContainerRequestContext context, CommunityRequest request,
			@PathParam("commid") String commid) {

		String userid = (String) context.getProperty("userid");

		Community community = commService.getCommunity(commid);

		// TODO: if Moderator or Creator
		// if (community.getModerators().contains(userid)) {
		if (request!=null) {
			if (request.communityName!=null && !request.communityName.isEmpty())
				community.setCommunityName(request.communityName);
			if (request.privacy!=null && !request.privacy.isEmpty())
				community.setPrivacySetting(PrivacySetting.valueOf(request.privacy.toUpperCase()));
			if (request.keywords!=null && !request.keywords.isEmpty())
				community.setKeywords(request.keywords);
			if (request.description!=null && !request.description.isEmpty())
				community.setDescription(request.description);
		}

		commService.setCommunity(commid, community);

		CommunityResponse response = new CommunityResponse(userSharedService, community, userid, null);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();
	}

	/**
	 * 
	 * @param context
	 * @param request
	 * @param commid
	 * @return
	 * 
	 *         To change the Community Photo
	 * 
	 *         Needs: - Community-ID - FileRequest with the Picture
	 * 
	 *         Returns Community with new Picture
	 */
	@POST
	@Path("commphoto/{commid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response setCommPhoto(@Context ContainerRequestContext context, FileRequest request,
			@PathParam("commid") String commid) {

		Community community = commService.getCommunity(commid);
		community.setCommunityPhoto(request.fileID);

		commService.setCommunity(commid, community);

		return null;
	}

	/**
	 * 
	 * @param context
	 * @param commid
	 * @return
	 * 
	 *         Deletes the community
	 * 
	 *         returns null
	 */
	@GET
	@Path("delete/{commid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCommunity(@Context ContainerRequestContext context, @PathParam("commid") String commid) {

		// TODO: Check if creator
		System.out.println("ID: " + commid);
		commService.deleteCommunity(commid);

		// TODO: Return communities?
		return null;
	}

	/**
	 * 
	 * @param context
	 * @param request
	 * @param commid
	 * @return
	 * 
	 *         Adds a member to the Community
	 * 
	 */
	@POST
	@Path("add/{commid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addMember(@Context ContainerRequestContext context, UserRequest request,
			@PathParam("commid") String commid) {

		String userid = (String) context.getProperty("userid");
		String username = (String) context.getProperty("username");

		// set Username for Cookies and to get Friends from other Microservice
		// (Dashboard)
		dashboardClient.setUsername(username);
		List<String> friends = this.dashboardClient.getOwnFriends();

		Community community = commService.getCommunity(commid);

		// find member in invites list -> delete
		// add to member list
		community.getInvites().stream().filter(x -> x.equals(request.userid)).collect(Collectors.toList())
				.forEach(x -> {
					community.getMembers().add(x);
					community.getInvites().remove(x);
				});

		// find member in requests list -> delete
		// add to member list
		community.getRequests().stream().filter(x -> x.equals(request.userid)).collect(Collectors.toList())
				.forEach(x -> {
					community.getMembers().add(x);
					community.getRequests().remove(x);
				});

		commService.setCommunity(commid, community);

		// commService.addToUserCommunities(request.userid, commid,
		// CommunityTypes.MEMBER);

		CommunityResponse response = new CommunityResponse(userSharedService, community, userid, friends);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();
	}

	/**
	 * 
	 * @param context
	 * @param request
	 * @param commid
	 * @return
	 * 
	 *         Declines a request from a user to join the community
	 * 
	 */
	@POST
	@Path("decline/{commid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response declineUser(@Context ContainerRequestContext context, UserRequest request,
			@PathParam("commid") String commid) {

		String userid = (String) context.getProperty("userid");
		Community community = commService.getCommunity(commid);

		// find member in invites list -> delete
		community.getInvites().stream().filter(x -> x.equals(request.userid)).collect(Collectors.toList())
				.forEach(x -> community.getInvites().remove(x));

		// find member in requests list -> delete
		community.getRequests().stream().filter(x -> x.equals(request.userid)).collect(Collectors.toList())
				.forEach(x -> community.getRequests().remove(x));

		commService.setCommunity(commid, community);

		// commService.removeFromUserCommunities(request.userid, commid,
		// CommunityTypes.INVITED);

		CommunityResponse response = new CommunityResponse(userSharedService, community, userid, null);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();
	}

	/**
	 * 
	 * @param context
	 * @param request
	 * @param commid
	 * @return
	 * 
	 *         Sends invitation to a user to join the community
	 * 
	 */
	@POST
	@Path("invite/{commid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response inviteMember(@Context ContainerRequestContext context, UserRequest request,
			@PathParam("commid") String commid) {

		String userid = (String) context.getProperty("userid");
		String username = (String) context.getProperty("username");

		System.out.println("USERID: " + userid);
		System.out.println("REQID: " + request.userid);

		// set Username for Cookies and to get Friends from other Microservice
		// (Dashboard)
		dashboardClient.setUsername(username);
		List<String> friends = this.dashboardClient.getOwnFriends();

		Community community = commService.getCommunity(commid);

		community.getInvites().add(request.userid);

		for (String s : community.getInvites()) {
			System.out.println("INV:  " + s);
		}

		commService.setCommunity(commid, community);

		// commService.addToUserCommunities(request.userid, commid,
		// CommunityTypes.INVITED);

		CommunityResponse response = new CommunityResponse(userSharedService, community, userid, friends);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();
	}

	/**
	 * 
	 * @param context
	 * @param request
	 * @param commid
	 * @return
	 * 
	 *         Removes a member of the community from the community
	 * 
	 */
	@POST
	@Path("remove/{commid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeMember(@Context ContainerRequestContext context, UserRequest request,
			@PathParam("commid") String commid) {

		String userid = (String) context.getProperty("userid");
		Community community = commService.getCommunity(commid);

		// find user in moderator list -> remove
		community.getModerators().stream().filter(x -> x.equals(request.userid)).collect(Collectors.toList())
				.forEach(x -> community.getModerators().remove(x));

		// commService.removeFromUserCommunities(request.userid, commid,
		// CommunityTypes.MANAGED);

		// find user in mebers list -> remove
		community.getMembers().stream().filter(x -> x.equals(request.userid)).collect(Collectors.toList())
				.forEach(x -> community.getMembers().remove(x));

		// commService.removeFromUserCommunities(request.userid, commid,
		// CommunityTypes.MEMBER);

		commService.setCommunity(commid, community);

		CommunityResponse response = new CommunityResponse(userSharedService, community, userid, null);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();
	}

	/**
	 * 
	 * @param context
	 * @param request
	 * @param commid
	 * @return
	 * 
	 *         Promotes a community member to a moderator
	 * 
	 */
	@POST
	@Path("member2mod/{commid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response member2Mod(@Context ContainerRequestContext context, UserRequest request,
			@PathParam("commid") String commid) {

		String userid = (String) context.getProperty("userid");
		Community community = commService.getCommunity(commid);

		// TODO: Anzahl begrenzen: 3?

		// find user in members list -> remove
		// add to moderator list
		community.getMembers().stream().filter(x -> x.equals(request.userid)).collect(Collectors.toList())
				.forEach(x -> {
					community.getModerators().add(x);
					community.getMembers().remove(x);
				});

		commService.setCommunity(commid, community);

		// commService.addToUserCommunities(request.userid, commid,
		// CommunityTypes.MANAGED);
		// commService.removeFromUserCommunities(request.userid, commid,
		// CommunityTypes.MEMBER);

		CommunityResponse response = new CommunityResponse(userSharedService, community, userid, null);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();
	}

	/**
	 * 
	 * @param context
	 * @param request
	 * @param commid
	 * @return
	 * 
	 *         Degrades a moderator of the community to a member
	 * 
	 */
	@POST
	@Path("mod2member/{commid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response mod2Member(@Context ContainerRequestContext context, UserRequest request,
			@PathParam("commid") String commid) {

		String userid = (String) context.getProperty("userid");
		Community community = commService.getCommunity(commid);

		// find user in moderator list -> remove
		// add to members list
		community.getModerators().stream().filter(x -> x.equals(request.userid)).collect(Collectors.toList())
				.forEach(x -> {
					community.getMembers().add(x);
					community.getModerators().remove(x);
				});

		commService.setCommunity(commid, community);

		// commService.addToUserCommunities(request.userid, commid,
		// CommunityTypes.MEMBER);
		// commService.removeFromUserCommunities(request.userid, commid,
		// CommunityTypes.MANAGED);

		CommunityResponse response = new CommunityResponse(userSharedService, community, userid, null);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();
	}

	/**
	 * 
	 * @param context
	 * @param request
	 * @param commid
	 * @return
	 * 
	 *         Blocks a user for the community
	 * 
	 */
	@POST
	@Path("block/{commid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response blockUser(@Context ContainerRequestContext context, UserRequest request,
			@PathParam("commid") String commid) {

		String userid = (String) context.getProperty("userid");
		Community community = commService.getCommunity(commid);

		// find user in members list -> remove
		// add to blocked list
		community.getMembers().stream().filter(x -> x.equals(request.userid)).collect(Collectors.toList())
				.forEach(x -> {
					community.getBlocked().add(x);
					community.getMembers().remove(x);
					// commService.addToUserCommunities(request.userid, commid,
					// CommunityTypes.BLOCKED);
					// commService.removeFromUserCommunities(request.userid, commid,
					// CommunityTypes.MEMBER);
				});

		// find user in moderators list -> remove
		// add to blocked list
		community.getModerators().stream().filter(x -> x.equals(request.userid)).collect(Collectors.toList())
				.forEach(x -> {
					community.getBlocked().add(x);
					community.getModerators().remove(x);
					// commService.addToUserCommunities(request.userid, commid,
					// CommunityTypes.BLOCKED);
					// commService.removeFromUserCommunities(request.userid, commid,
					// CommunityTypes.MANAGED);
				});

		commService.setCommunity(commid, community);

		CommunityResponse response = new CommunityResponse(userSharedService, community, userid, null);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();
	}

	/**
	 * 
	 * @param context
	 * @param request
	 * @param commid
	 * @return
	 * 
	 *         Removes the block for the user
	 * 
	 */
	@POST
	@Path("unblock/{commid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response unblockUser(@Context ContainerRequestContext context, UserRequest request,
			@PathParam("commid") String commid) {

		String userid = (String) context.getProperty("userid");
		Community community = commService.getCommunity(commid);

		// find user in blocked list -> remove
		community.getBlocked().stream().filter(x -> x.equals(request.userid)).collect(Collectors.toList())
				.forEach(x -> community.getBlocked().remove(x));

		commService.setCommunity(commid, community);

		// commService.removeFromUserCommunities(request.userid, commid,
		// CommunityTypes.BLOCKED);

		CommunityResponse response = new CommunityResponse(userSharedService, community, userid, null);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();
	}

	// TODO: Delete?
	@GET
	@Path("/members")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCommMembers(@Context ContainerRequestContext requestContext,
			@QueryParam("memberIds[]") List<String> memberIds) {

		System.out.println("TEHEST");
		System.out.println(memberIds);
		// List<Friend> friends = new LinkedList<>();
		//
		// for (String id : memberIds) {
		// UserSharedCacheObject user = userSharedService.getUser(id);
		// Profile profile = profileService.get(id);
		//
		// Friend friend = new Friend(id, user.username, user.usertype,
		// profile.getAvatarImageId());
		// friends.add(friend);
		// System.out.println("Friend Info: " + friend);
		// }

		// return Response.status(200).entity(friends).build();
		return null;
	}

}
