package eu.stimey.platform.microservice.communities.beans.filter;

import org.bson.Document;

import java.util.Arrays;
import java.util.List;

public class FilterBuilder {
	public static final String CASE_INSENSITIVITY = "i";
	public static final String MULTILINE_MATCH = "m";
	public static final String EXTENDED = "x";
	public static final String MATCH_NEW_LINE = "s";

	public static Document and(Document... values) {
		if (values.length == 0)
			return new Document();
		if (values.length == 1)
			return values[0];
		return new Document("$and", Arrays.asList(values));
	}

	public static Document and(List<Document> values) {
		if (values.size() == 0)
			return new Document();
		if (values.size() == 1)
			return values.get(0);
		return new Document("$and", values);
	}

	public static Document or(Document... values) {
		if (values.length == 0)
			return new Document();
		if (values.length == 1)
			return values[0];
		return new Document("$or", Arrays.asList(values));
	}

	public static Document or(List<Document> values) {
		if (values.size() == 0)
			return new Document();
		if (values.size() == 1)
			return values.get(0);
		return new Document("$or", values);
	}

	public static Document elemMatch(Document value) {
		if (value == null)
			return new Document();
		return new Document("$elemMatch", value);
	}

	public static Document regex(String value, String options) {
		if (value == null || value.length() <= 0)
			return new Document();
		if (options == null || !options.matches("^[imxs]+$"))
			return new Document("$regex", value);
		return new Document("$regex", value).append("$options", options);
	}

}
