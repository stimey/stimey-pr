package eu.stimey.platform.microservice.communities.databind;

import java.util.List;

public class QuestionData {

	public String answer;
	public List<ValueData> suggestions;

	public QuestionData() {

	}

}
