package eu.stimey.platform.microservice.communities.databind;

public enum PrivacySetting {
	PUBLIC, PRIVATE, CLOSED

}
