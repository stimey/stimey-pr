package eu.stimey.platform.microservice.communities.responses;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.microservice.communities.databind.Discussion;
import eu.stimey.platform.microservice.communities.databind.DiscussionFirstComment;

public class DiscussionResponse {

	public String discussionID;
	public String creatorID;
	public String creatorName;
	public String text;
	public String photo;
	public String file;
	public Date timestamp;
	public List<Object> likes;
	public List<Object> firstcomments;

	public DiscussionResponse(UserSharedService service, Discussion discussion) {

		this.discussionID = discussion.getDiscussionID();
		this.creatorID = discussion.getCreator();
		this.creatorName = "Archimedis";
		this.text = discussion.getText();
		this.photo = discussion.getPhoto();
		this.file = discussion.getFile();
		this.timestamp = discussion.getTimestamp();

		this.likes = new LinkedList<>();
		for (String s : discussion.getLikes()) {
			UserResponse like = new UserResponse(service, s);
			this.likes.add(like);
		}

		this.firstcomments = new LinkedList<>();
		for (DiscussionFirstComment d : discussion.getComments()) {
			System.out.println("TEXT: " + d.getText());
			DiscFirstCommentResponse dfcr = new DiscFirstCommentResponse(service, d);
			this.firstcomments.add(dfcr);
		}
	}

}
