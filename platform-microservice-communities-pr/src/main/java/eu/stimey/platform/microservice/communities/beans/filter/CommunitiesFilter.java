package eu.stimey.platform.microservice.communities.beans.filter;

import com.mongodb.client.model.Filters;
import eu.stimey.platform.library.data.access.Caching;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Filter;

public class CommunitiesFilter implements DocumentFilter {

	public static final String KEYWORDS_PATH = "keywords";
	public static final String NAME_PATH = "communityName";
	public static final String LANGUAGE_PATH = "language";

	private Document filter;

	public CommunitiesFilter() {
	}

	public CommunitiesFilter language(String language) {
		if (language == null || language.isEmpty() || language.equals("Any"))
			return this;
		Document matchLanguage = new Document(LANGUAGE_PATH, language);
		if (this.filter == null)
			this.filter = matchLanguage;
		else
			this.filter = FilterBuilder.and(this.filter, matchLanguage);
		return this;
	}

	public CommunitiesFilter keywords(List<String> keywords) {
		if (keywords == null || keywords.isEmpty())
			return this;
		return this.keywords(keywords.toArray(new String[keywords.size()]));
	}

	public CommunitiesFilter keywords(String... keywords) {
		if (keywords == null || keywords.length == 0)
			return this;
		Document keywordsQuery = new Document(KEYWORDS_PATH, new Document("$all", Arrays.asList(keywords)));
		if (this.filter == null)
			this.filter = keywordsQuery;
		else
			this.filter = FilterBuilder.and(this.filter, keywordsQuery);
		return this;
	}

	public CommunitiesFilter query(String query) {
		if (query == null || query.isEmpty())
			return this;
		List<Document> result = new LinkedList<>();

		for (String part : query.split(" ")) {
			result.add(new Document(NAME_PATH, FilterBuilder.regex(part, FilterBuilder.CASE_INSENSITIVITY)));
			result.add(new Document(KEYWORDS_PATH, FilterBuilder.regex(part, FilterBuilder.CASE_INSENSITIVITY)));
		}
		Document regexDocument = FilterBuilder.or(result);
		if (this.filter == null)
			this.filter = regexDocument;
		// this.filter = Caching.createTextSearchFilter(query);
		else
			this.filter = FilterBuilder.and(this.filter, regexDocument);
		return this;
	}

	public static Document matchTitle(String title) {

		return new Document("$text", new Document("$search", title));
		// return new Document(TITLE_PATH, new Document("$regex", "^" + title +
		// "$").append("$options", "i"));
	}

	public static Document matchTitleCaseIntensive(String title) {
		return new Document(NAME_PATH, new Document("$regex", "^" + title + "$").append("$options", "i"));
	}

	@Override
	public Document toDocument() {
		Document result;
		if (this.filter == null) {
			result = new Document();
		} else {
			result = this.filter;
			this.filter = null;
		}
		return result;
	}
}
