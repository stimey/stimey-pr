package eu.stimey.platform.microservice.communities.startup;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.utils.StimeyLogger;

/**
 * Global web application settings
 * 
 */
@WebListener
public class ServiceServletContextListener implements ServletContextListener {
	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		Global.startup();
		StimeyLogger.setApplicationName(servletContextEvent.getServletContext().getContextPath());

		MongoDBService.start("ms_communities");
		RedisAMQPService.start();
		// RobotCommunicationService.start();

	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		// RobotCommunicationService.stop();
		RedisAMQPService.stop();
		MongoDBService.stop();
	}
}
