package eu.stimey.platform.microservice.communities.responses;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.microservice.communities.databind.DiscussionFirstComment;
import eu.stimey.platform.microservice.communities.databind.DiscussionOtherComments;

public class DiscFirstCommentResponse {

	public String commentID;
	public String creatorID;
	public String creatorName;
	public String text;
	public Date timestamp;
	public List<Object> likes;
	public List<Object> othercomments;

	public DiscFirstCommentResponse(UserSharedService service, DiscussionFirstComment d) {
		this.commentID = d.getMessageID();
		this.creatorID = d.getCreator();
		this.creatorName = "Archimedis";
		this.text = d.getText();
		this.timestamp = d.getTimestamp();

		this.likes = new LinkedList<>();
		for (String s : d.getLikes()) {
			UserResponse like = new UserResponse(service, s);
			this.likes.add(like);
		}

		this.othercomments = new LinkedList<>();
		for (DiscussionOtherComments doc : d.getComments()) {
			System.out.println("TEXT: " + doc.getText());
			DiscOtherCommentResponse docr = new DiscOtherCommentResponse(service, doc);
			this.othercomments.add(docr);
		}

	}

}
