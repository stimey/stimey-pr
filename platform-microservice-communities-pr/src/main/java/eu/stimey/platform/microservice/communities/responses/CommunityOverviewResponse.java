package eu.stimey.platform.microservice.communities.responses;

import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.microservice.communities.databind.Community;

public class CommunityOverviewResponse {

	public String commID;
	public String commName;
	public String commPhoto;
	public String creatorID;
	public String creatorName;
	public List<String> members_mods;
	public List<String> friends;
	public int membersCount = 0;
	public int friendsCount = 0;

	public CommunityOverviewResponse(UserSharedService service, Community community, List<String> friends) {
		this.commID = community.getCommunityID();
		this.commName = community.getCommunityName();
		this.commPhoto = community.getCommunityPhoto();
		this.creatorID = community.getCreator();
		this.creatorName = service.getUser(creatorID).username;
		this.members_mods = community.getMembers();
		this.members_mods.addAll(community.getModerators());
		this.membersCount = members_mods.size();
		this.friends = friends;
		if (this.friends != null) {
			this.friends.retainAll(this.members_mods);
			this.friendsCount = this.friends.size();
		}

	}

}
