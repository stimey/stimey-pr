package eu.stimey.platform.microservice.communities.startup;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HandlerService {
	protected ExecutorService executerService;

	public HandlerService() {
		executerService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
	}

	public ExecutorService getExecuterService() {
		return executerService;
	}
}
