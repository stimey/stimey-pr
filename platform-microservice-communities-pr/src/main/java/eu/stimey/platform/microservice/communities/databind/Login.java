/** Copyright (c) 2016-present STIMEY. All rights reserved. */
package eu.stimey.platform.microservice.communities.databind;

import java.util.UUID;

import eu.stimey.platform.library.utils.databind.DocumentWithId;

/**
 * The schema of the document for the login collection. Extended by the class
 * DocumentWithId.
 * 
 * @author David Bauer
 * @author Sascha M. Schumacher
 * @version 2018.09.10.00
 */
public class Login extends DocumentWithId {
	public String username;
	public String usertype;
	public String hash;
	public String salt;
	public String language;
	public String email;
	public String robotname;
	public String robotchannel;

	/** Is the user locked? */
	public boolean locked;

	/** Is the user verified? */
	public boolean user_verified;

	/** Is the email of the user verified? */
	public boolean email_confirmed;

	/** The email from the guardian. */
	public String guardian_email;

	public static final String DEFAULT_ROBOT_NAME = "stimeyrobot";

	public Login() {
		super();
	}

	public Login(String username, String usertype, String hash, String salt, String language, String email) {
		this(username, usertype, hash, salt, language, email, null);
	}

	public Login(String username, String usertype, String hash, String salt, String language, String email,
			String robotname) {
		super();
		this.username = username;
		this.usertype = usertype;
		this.hash = hash;
		this.salt = salt;
		this.language = language;
		this.email = email;
		if (robotname == null)
			this.robotname = DEFAULT_ROBOT_NAME;
		else
			this.robotname = robotname;
		this.robotchannel = "robot-" + UUID.randomUUID().toString();
		this.user_verified = false;
		this.email_confirmed = false;
		this.locked = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Login [username=" + username + ", usertype=" + usertype + ", hash=" + hash + ", salt=" + salt
				+ ", language=" + language + ", email=" + email + ", robotname=" + robotname + ", robotchannel="
				+ robotchannel + ", locked=" + locked + ", verified=" + user_verified + ", email_verified="
				+ email_confirmed + ", guardian_email=" + guardian_email + "]";
	}

}
