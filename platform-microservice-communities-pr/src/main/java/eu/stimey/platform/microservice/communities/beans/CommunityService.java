package eu.stimey.platform.microservice.communities.beans;

import java.util.LinkedList;
import java.util.List;

import org.bson.Document;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.communities.databind.Community;
import eu.stimey.platform.microservice.communities.databind.CommunityTypes;
import eu.stimey.platform.microservice.communities.databind.Creativity;
import eu.stimey.platform.microservice.communities.databind.PrivacySetting;
import eu.stimey.platform.microservice.communities.databind.UsersCommunities;

public class CommunityService {

	private static final int COMM_RESULT_CACHE_SIZE = 6;

	private Caching<String, Community> communityCache;
	private Caching<String, UsersCommunities> userCommsCache;
	private Caching<String, Creativity> creativityCache;

	public CommunityService() {
		this.communityCache = new Caching<>("ms_communities", "communityID", Community.class, "communities",
				GlobalVariables.CACHE_CONFIG);
		this.communityCache.clear();

		this.userCommsCache = new Caching<>("ms_communities", "userid", UsersCommunities.class, "users",
				GlobalVariables.CACHE_CONFIG);
		this.userCommsCache.clear();

		this.creativityCache = new Caching<>("ms_communities", "userid", Creativity.class, "testing",
				GlobalVariables.CACHE_CONFIG);
		this.creativityCache.clear();

	}

	public Community getCommunity(String id) {
		return communityCache.get(id);
	}

	public void setCommunity(String id, Community comm) {
		communityCache.set(id, comm);
	}

	public void deleteCommunity(String id) {
		communityCache.del(id);
	}

	public List<Community> searchComms(Document filter) {

		List<Community> comms = new LinkedList<>();
		List<String> keys = new LinkedList<>();

		keys = communityCache.find(filter, null, 0, 0, (c) -> c.getCommunityID(), true);

		for (String key : keys) {
			comms.add(communityCache.get(key));
		}
		return comms;
	}
	
	public List<Community> getMyCommunities(String userid) {
		List<Community> comms = new LinkedList<>();
		List<String> keys = new LinkedList<>();
		
		keys = communityCache.find(new Document("creator", new Document("$eq", userid)), null, 0, 0,
				(c) -> c.getCommunityID(), true);

		for (String key : keys) {
			comms.add(communityCache.get(key));
		}
		
		return comms;
	}

	public List<Community> getCommunities(CommunityTypes type, String userid) {

		List<Community> comms = new LinkedList<>();
		List<String> keys = new LinkedList<>();

		switch (type) {
		case ALL: {

			// find all Communities which aren't secret
			keys = communityCache.find(
					new Document("privacySetting", new Document("$ne", String.valueOf(PrivacySetting.CLOSED))), null, 0,
					20, (c) -> c.getCommunityID(), true);

			for (String key : keys) {
				comms.add(communityCache.get(key));
			}
			break;
		}
		case PENDING: {

			// Search in List with Invites of the Community for given User-ID
			keys = communityCache.find(new Document("invites", new Document("$eq", userid)), null, 0, 0,
					(c) -> c.getCommunityID(), true);

			for (String key : keys) {
				comms.add(communityCache.get(key));
			}
			break;
		}
		case MANAGED: {

			// Search for Communities where User is Moderator
			keys = communityCache.find(new Document("moderators", new Document("$eq", userid)), null, 0, 0,
					(c) -> c.getCommunityID(), true);

			for (String key : keys) {
				comms.add(communityCache.get(key));
			}
			break;
		}
		case MEMBER: {

			// Search for Communities where User is Member
			keys = communityCache.find(new Document("members", new Document("$eq", userid)), null, 0, 0,
					(c) -> c.getCommunityID(), true);

			for (String key : keys) {
				comms.add(communityCache.get(key));
			}
			break;
		}
		case INTERESTING: {
			// TODO: Find out what makes a community interesting

			keys = communityCache.find(null, null, 0, 9, (c) -> c.getCommunityID(), true);

			for (String key : keys) {
				Community comm = communityCache.get(key);
				if (!(comm.getModerators().contains(userid) || comm.getMembers().contains(userid)
						|| comm.getRequests().contains(userid) || comm.getInvites().contains(userid))) {
					comms.add(comm);
				}

			}

			break;
		}
		case POPULAR: {
			// TODO: find out what makes a community popular
			// vermutlich höchsten Memberzahlen
			break;
		}
		case MISSIONS: {
			keys = communityCache.find(new Document("relatedCourseID", new Document("$eq", userid)), null, 0, 0,
					(c) -> c.getCommunityID(), true);

			for (String key : keys) {
				comms.add(communityCache.get(key));
			}
			break;
		}
		default: {
			break;
		}
		}

		return comms;
	}

	// public void addToUserCommunities(String userid, String commid, CommunityTypes
	// type) {
	//
	// System.out.println("USERID: " + userid);
	// System.out.println("COMMID: " + commid);
	//
	// UsersCommunities uc = userCommsCache.get(userid);
	//
	// switch (type) {
	// case MEMBER: {
	// if (!uc.getMember().contains(commid)) {
	// uc.addMember(commid);
	// }
	//
	// break;
	// }
	// case MANAGED: {
	// if (!uc.getModerator_creator().contains(commid)) {
	// uc.addModerator_creator(commid);
	// }
	//
	// break;
	// }
	//
	// case PENDING: {
	// if (!uc.getMember().contains(commid)) {
	// uc.addMember(commid);
	// }
	//
	// break;
	//
	// }
	//
	// case INVITED: {
	// if (!uc.getInvites().contains(commid)) {
	// uc.addInvite(commid);
	// }
	//
	// break;
	//
	// }
	//
	// case BLOCKED: {
	// if (!uc.getBlocked().contains(commid)) {
	// uc.addBlock(commid);
	// }
	//
	// break;
	//
	// }
	// default:
	// break;
	// }
	//
	// userCommsCache.set(userid, uc);
	//
	// }
	//
	// public void removeFromUserCommunities(String userid, String commid,
	// CommunityTypes type) {
	//
	// UsersCommunities uc = userCommsCache.get(userid);
	//
	// switch (type) {
	// case MEMBER: {
	// if (uc.getMember().contains(commid)) {
	// uc.getMember().remove(commid);
	// }
	//
	// break;
	// }
	// case MANAGED: {
	// if (uc.getModerator_creator().contains(commid)) {
	// uc.getModerator_creator().remove(commid);
	// }
	//
	// break;
	// }
	//
	// case PENDING: {
	// if (uc.getMember().contains(commid)) {
	// uc.getMember().remove(commid);
	// }
	//
	// break;
	//
	// }
	//
	// case INVITED: {
	// System.out.println("TEEEEEST");
	// System.out.println("TEST: " + uc);
	// System.out.println("TEST: " + uc.getInvites().size());
	// if (uc.getInvites().contains(commid)) {
	// uc.getInvites().remove(commid);
	// }
	//
	// break;
	//
	// }
	//
	// case BLOCKED: {
	// if (uc.getBlocked().contains(commid)) {
	// uc.getBlocked().remove(commid);
	// }
	//
	// break;
	//
	// }
	// default:
	// break;
	// }
	//
	// userCommsCache.set(userid, uc);
	//
	// }

	public Creativity getCreativity(String id) {
		return creativityCache.get(id);
	}

	public void setCreativity(String id, Creativity creativity) {
		creativityCache.set(id, creativity);

	}

}
