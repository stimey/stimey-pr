package eu.stimey.platform.microservice.communities.startup;

import eu.stimey.platform.library.core.startup.DefaultGlobal;

/**
 * 
 * Global settings (environment variables)
 *
 */
public final class Global extends DefaultGlobal {
	public static void startup() {
		DefaultGlobal.startup();
	}
}
