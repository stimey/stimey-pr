package eu.stimey.platform.microservice.communities.rest.resources;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import eu.stimey.platform.microservice.communities.responses.CommunityOverviewResponse;
import eu.stimey.platform.microservice.communities.responses.CommunityResponse;

import eu.stimey.platform.microservice.communities.rest.client.NotificationClient;
import org.bson.Document;
import org.bson.types.ObjectId;

import eu.stimey.platform.microservice.communities.rest.client.DashboardClient;
import eu.stimey.platform.microservice.communities.rest.utils.RESTUri;
import eu.stimey.platform.library.core.beans.UserSharedService;
//import eu.stimey.platform.microservice.dashboard.beans.ProfileService;
import eu.stimey.platform.microservice.communities.beans.CommunityService;
import eu.stimey.platform.microservice.communities.beans.filter.CommunitiesFilter;
import eu.stimey.platform.microservice.communities.databind.Community;
import eu.stimey.platform.microservice.communities.databind.CommunityTypes;
import eu.stimey.platform.microservice.communities.databind.PrivacySetting;
import eu.stimey.platform.microservice.communities.requests.CommunityRequest;
import eu.stimey.platform.microservice.communities.responses.CommunityListResponse;


@Path("/communities")
public class CommunityOverviewResource {

	@Inject
	CommunityService commService;
	@Inject
	UserSharedService userSharedService;

	// to get friend list from user
	private DashboardClient dashboardClient;

	// Cookie is needed for dashBoardClient
	public CommunityOverviewResource(@Context ContainerRequestContext context,
			@CookieParam("access_token") Cookie cookie) {
		this.dashboardClient = new DashboardClient();
		this.dashboardClient.setCookie(cookie);
	}

	/**
	 * 
	 * @param context
	 * @param userid
	 * @return
	 * 
	 *         Returns a list of Communities - All communities - Pending invitations
	 *         or requests to communities - Communities which the user manages -
	 *         Communities which the user is a member of - Communities which might
	 *         be interesting for the user - Popular Communities
	 * 
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCommunities(@Context ContainerRequestContext context) {

		String userid = (String) context.getProperty("userid");
		String username = (String) context.getProperty("username");

		// set Username for Cookies and to get Friends from other Microservice
		// (Dashboard)
		dashboardClient.setUsername(username);
		List<String> friends = this.dashboardClient.getOwnFriends();

		// List<Community> all = commService.getCommunities(CommunityTypes.ALL, userid);
		List<Community> pending = commService.getCommunities(CommunityTypes.PENDING, userid);
		List<Community> managed = commService.getCommunities(CommunityTypes.MANAGED, userid);
		List<Community> member = commService.getCommunities(CommunityTypes.MEMBER, userid);
		List<Community> interesting = commService.getCommunities(CommunityTypes.INTERESTING, userid);
		List<Community> popular = commService.getCommunities(CommunityTypes.POPULAR, userid);

		CommunityListResponse response = new CommunityListResponse(userSharedService, null, pending, managed, member,
				interesting, popular, null, null, friends);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();
	}

	/**
	 * 
	 * @param context
	 * @param request
	 * @return
	 * 
	 *         Creates a new Community Needs: - Userid - Name for the new community
	 *         - PrivacySetting - Description of community - Keywords
	 * 
	 *         Returns a List of Communities
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createCommuntiy(@Context ContainerRequestContext context, CommunityRequest request) {

		String commid = new ObjectId().toHexString();
		String userid = (String) context.getProperty("userid");
		String username = (String) context.getProperty("username");

		// set Username for Cookies and to get Friends from other Microservice
		// (Dashboard)
		dashboardClient.setUsername(username);
		List<String> friends = this.dashboardClient.getOwnFriends();

		List<String> keywords = request.keywords;

		Community community = new Community(commid, request.communityName, userid, null,
				PrivacySetting.valueOf(request.privacy.toUpperCase()), request.description, keywords);
		community.getModerators().add(userid);

		commService.setCommunity(commid, community);

		List<Community> all = commService.getCommunities(CommunityTypes.ALL, userid);
		all.addAll(commService.getMyCommunities(userid));
		List<Community> pending = commService.getCommunities(CommunityTypes.PENDING, userid);
		List<Community> managed = commService.getCommunities(CommunityTypes.MANAGED, userid);
		List<Community> member = commService.getCommunities(CommunityTypes.MEMBER, userid);
		List<Community> interesting = commService.getCommunities(CommunityTypes.INTERESTING, userid);
		List<Community> popular = commService.getCommunities(CommunityTypes.POPULAR, userid);

		// commService.addToUserCommunities(userid, commid, CommunityTypes.MANAGED);

		CommunityListResponse response = new CommunityListResponse(userSharedService, all, pending, managed, member,
				interesting, popular, null, null, friends);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();
	}

	/**
	 * 
	 * @param context
	 * @param missionid
	 * @return
	 * 
	 *         Returns a List of Communities which are related to a mission
	 */
	@GET
	@Path("/missions/{missionid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMissionCommunities(@Context ContainerRequestContext context,
			@PathParam("missionid") String missionid) {

		String username = (String) context.getProperty("username");

		// set Username for Cookies and to get Friends from other Microservice
		// (Dashboard)
		dashboardClient.setUsername(username);
		List<String> friends = this.dashboardClient.getOwnFriends();

		List<Community> missions = commService.getCommunities(CommunityTypes.MISSIONS, missionid);

		CommunityListResponse response = new CommunityListResponse(userSharedService, null, null, null, null, null,
				null, missions, null, friends);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();
	}

	/**
	 * 
	 * @param context
	 * @param request
	 * @return
	 * 
	 *         Creates a new Community related to Missions Needs: - User-ID - Name
	 *         for the new community - PrivacySetting - Description of community -
	 *         Keywords - Mission-ID
	 * 
	 *         Returns a List of Communities
	 */
	@POST
	@Path("/missions/{missionid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMissionCommuntiy(@Context ContainerRequestContext context, CommunityRequest request,
			@PathParam("missionid") String missionid) {

		String commid = new ObjectId().toHexString();
		String userid = (String) context.getProperty("userid");
		String username = (String) context.getProperty("username");

		// set Username for Cookies and to get Friends from other Microservice
		// (Dashboard)
		dashboardClient.setUsername(username);
		List<String> friends = this.dashboardClient.getOwnFriends();

		List<String> keywords = request.keywords;

		Community community = new Community(commid, request.communityName, userid, missionid,
				PrivacySetting.valueOf(request.privacy.toUpperCase()), request.description, keywords);
		community.getModerators().add(userid);

		commService.setCommunity(commid, community);

		CommunityOverviewResponse communityOverviewResponse = new CommunityOverviewResponse(userSharedService,
				community, friends);

		return Response.status(Status.OK).entity(communityOverviewResponse).build();
	}

	/**
	 * 
	 * @param context
	 * @param commid
	 * @return
	 * 
	 *         Lets the user join to a community Cases: - PUBLIC - PRIVATE - SECRET
	 * 
	 *         returns a list of communities
	 * 
	 */
	@GET
	@Path("/join/{commid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response joinCommunity(@Context ContainerRequestContext context, @PathParam("commid") String commid,
			@CookieParam("access_token") Cookie cookie) {

		String userid = (String) context.getProperty("userid");
		String username = (String) context.getProperty("username");

		// set Username for Cookies and to get Friends from other Microservice
		// (Dashboard)
		dashboardClient.setUsername(username);
		List<String> friends = this.dashboardClient.getOwnFriends();

		Community community = commService.getCommunity(commid);

		switch (community.getPrivacySetting()) {

		// User can join instantly
		case PUBLIC: {

			if (!community.getMembers().contains(userid) || !community.getModerators().contains(userid)) {
				community.getMembers().add(userid);

				community.getInvites().stream().filter(s -> s.equals(userid)).collect(Collectors.toList())
						.forEach(community.getInvites()::remove);

				// commService.removeFromUserCommunities(userid, commid,
				// CommunityTypes.INVITED);
				// commService.addToUserCommunities(userid, commid, CommunityTypes.MEMBER);
			}

			break;
		}

		// User is either invited to a private community and accepts
		// or user sends a request to join the community
		case PRIVATE: {

			if (!community.getMembers().contains(userid) || !community.getModerators().contains(userid)) {

				if (community.getInvites().contains(userid)) {
					community.getMembers().add(userid);

					community.getInvites().stream().filter(s -> s.equals(userid)).collect(Collectors.toList())
							.forEach(community.getInvites()::remove);

					// commService.removeFromUserCommunities(userid, commid,
					// CommunityTypes.INVITED);
					// commService.addToUserCommunities(userid, commid, CommunityTypes.MEMBER);

				} else {
					community.getRequests().add(userid);
					// commService.addToUserCommunities(userid, commid, CommunityTypes.PENDING);

					ArrayList<String> joinList = new ArrayList<>();
					joinList.add(userid);

					NotificationClient.broadcastNotification(community.getModerators(),
							"community_join_" + community.getCommunityName() + "_" + userid, joinList,
							community.getCommunityName(), "community/" + commid, "notification.request",
							"ICON-COMMUNITIES_circle.png", cookie);
				}
			}

			break;
		}

		// User needs to be invited to join a secret community
		case CLOSED: {

			if (community.getMembers().contains(userid) || community.getModerators().contains(userid)) {
				community.getMembers().add(userid);

				community.getInvites().stream().filter(s -> s.equals(userid)).collect(Collectors.toList())
						.forEach(community.getInvites()::remove);

				// commService.removeFromUserCommunities(userid, commid,
				// CommunityTypes.INVITED);
				// commService.addToUserCommunities(userid, commid, CommunityTypes.MEMBER);
			}

			break;
		}
		default: {
			break;
		}
		}

		commService.setCommunity(commid, community);

		List<Community> all = commService.getCommunities(CommunityTypes.ALL, userid);
		List<Community> pending = commService.getCommunities(CommunityTypes.PENDING, userid);
		List<Community> managed = commService.getCommunities(CommunityTypes.MANAGED, userid);
		List<Community> member = commService.getCommunities(CommunityTypes.MEMBER, userid);
		List<Community> interesting = commService.getCommunities(CommunityTypes.INTERESTING, userid);
		List<Community> popular = commService.getCommunities(CommunityTypes.POPULAR, userid);

		CommunityListResponse response = new CommunityListResponse(userSharedService, all, pending, managed, member,
				interesting, popular, null, null, friends);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();

	}

	/**
	 * 
	 * @param context
	 * @param commid
	 * @return
	 * 
	 *         If the user wants to leave a community
	 * 
	 *         Returns a list of communities
	 * 
	 */
	@GET
	@Path("/leave/{commid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response leaveCommunity(@Context ContainerRequestContext context, @PathParam("commid") String commid) {

		String userid = (String) context.getProperty("userid");
		String username = (String) context.getProperty("username");

		// set Username for Cookies and to get Friends from other Microservice
		// (Dashboard)
		dashboardClient.setUsername(username);
		List<String> friends = this.dashboardClient.getOwnFriends();

		Community community = commService.getCommunity(commid);

		community.getModerators().stream().filter(s -> s.equals(userid)).collect(Collectors.toList())
				.forEach(community.getModerators()::remove);

		// commService.removeFromUserCommunities(userid, commid,
		// CommunityTypes.MANAGED);

		community.getMembers().stream().filter(s -> s.equals(userid)).collect(Collectors.toList())
				.forEach(community.getMembers()::remove);

		// commService.removeFromUserCommunities(userid, commid, CommunityTypes.MEMBER);

		commService.setCommunity(commid, community);

		List<Community> all = commService.getCommunities(CommunityTypes.ALL, userid);
		List<Community> pending = commService.getCommunities(CommunityTypes.PENDING, userid);
		List<Community> managed = commService.getCommunities(CommunityTypes.MANAGED, userid);
		List<Community> member = commService.getCommunities(CommunityTypes.MEMBER, userid);
		List<Community> interesting = commService.getCommunities(CommunityTypes.INTERESTING, userid);
		List<Community> popular = commService.getCommunities(CommunityTypes.POPULAR, userid);

		CommunityListResponse response = new CommunityListResponse(userSharedService, all, pending, managed, member,
				interesting, popular, null, null, friends);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();

	}

	/**
	 * 
	 * @param context
	 * @param commid
	 * @return
	 * 
	 *         If the user wants to decline an invitation to a community
	 * 
	 *         returns a list of Communities
	 * 
	 */
	@GET
	@Path("/decline/{commid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response declineInvite(@Context ContainerRequestContext context, @PathParam("commid") String commid) {

		String userid = (String) context.getProperty("userid");
		String username = (String) context.getProperty("username");

		// set Username for Cookies and to get Friends from other Microservice
		// (Dashboard)
		dashboardClient.setUsername(username);
		List<String> friends = this.dashboardClient.getOwnFriends();

		Community community = commService.getCommunity(commid);

		// find user in invites list -> remove
		community.getInvites().stream().filter(s -> s.equals(userid)).collect(Collectors.toList())
				.forEach(community.getInvites()::remove);

		// commService.removeFromUserCommunities(userid, commid,
		// CommunityTypes.INVITED);

		List<Community> all = commService.getCommunities(CommunityTypes.ALL, userid);
		List<Community> pending = commService.getCommunities(CommunityTypes.PENDING, userid);
		List<Community> managed = commService.getCommunities(CommunityTypes.MANAGED, userid);
		List<Community> member = commService.getCommunities(CommunityTypes.MEMBER, userid);
		List<Community> interesting = commService.getCommunities(CommunityTypes.INTERESTING, userid);
		List<Community> popular = commService.getCommunities(CommunityTypes.POPULAR, userid);

		CommunityListResponse response = new CommunityListResponse(userSharedService, all, pending, managed, member,
				interesting, popular, null, null, friends);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();
	}

	@GET
	@Path("/search/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchCommunities(@Context ContainerRequestContext context,
			@QueryParam("query") String query /*
												 * ,
												 * 
												 * @QueryParam("selectedLanguage") String language
												 */) {

		System.out.println("NUE");
		System.out.println("Query: " + query);
		// System.out.println("Language: " + language);

		final Document filter = new CommunitiesFilter()
				// .language(language)
				.query(query).toDocument();
		List<Community> communities = commService.searchComms(filter);

		for (Community c : communities) {
			System.out.println("COMM-Name: " + c.getCommunityName());
		}

		String username = (String) context.getProperty("username");
		dashboardClient.setUsername(username);
		List<String> friends = this.dashboardClient.getOwnFriends();

		CommunityListResponse response = new CommunityListResponse(userSharedService, null, null, null, null, null,
				null, null, communities, friends);

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();
	}

}
