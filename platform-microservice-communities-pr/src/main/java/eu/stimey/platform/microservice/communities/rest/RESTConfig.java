package eu.stimey.platform.microservice.communities.rest;

import static eu.stimey.platform.library.utils.StimeyLogger.*;

import javax.annotation.PreDestroy;
import javax.ws.rs.ApplicationPath;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.microservice.communities.beans.CommunityService;


/**
 * 
 * REST configuration
 *
 */
@ApplicationPath("api")
public class RESTConfig extends ResourceConfig {
	protected final MongoDBService mongoDBService;
	protected final RedisAMQPService redisAMQPService;

	public RESTConfig() {
		super();

		mongoDBService = MongoDBService.getService();
		redisAMQPService = RedisAMQPService.getService();

		logger().info("REST-Service started...");

		packages("eu.stimey.platform.microservice.communities.rest");

		UserSharedService userSharedService = new UserSharedService();
		CommunityService communityService = new CommunityService();
		// ProfileService profileService = new ProfileService();

		register(new AbstractBinder() {
			protected void configure() {
				bind(mongoDBService).to(MongoDBService.class);
				bind(redisAMQPService).to(RedisAMQPService.class);
				bind(userSharedService).to(UserSharedService.class);
				bind(communityService).to(CommunityService.class);
				// bind(profileService).to(ProfileService.class);

			}
		});

		register(new JacksonJsonProvider().configure(SerializationFeature.INDENT_OUTPUT, true));
	}

	@PreDestroy
	public void shutdown() {
		logger().info("REST-Service stopped...");
	}
}
