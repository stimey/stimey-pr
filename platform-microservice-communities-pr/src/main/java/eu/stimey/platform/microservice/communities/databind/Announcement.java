package eu.stimey.platform.microservice.communities.databind;

import java.util.Date;

public class Announcement {

	private String annID;
	private String creatorID;
	private String title;
	private String description;
	private Date createDate;
	private Date eventDate;
	private String eventTimeFrom;
	private String eventTimeTill;

	public Announcement() {
		this(null, null, null, null, null, null);
	}

	public Announcement(String creatorID, String title, String description, Date eventDate, String eventTimeFrom,
			String eventTimeTill) {
		this.annID = "ID";
		this.creatorID = creatorID;
		this.title = title;
		this.description = description;
		this.createDate = new Date();
		this.eventDate = eventDate;
		this.eventTimeFrom = eventTimeFrom;
		this.eventTimeTill = eventTimeTill;
	}

	public String getAnnID() {
		return annID;
	}

	public void setAnnID(String annID) {
		this.annID = annID;
	}

	public String getCreatorID() {
		return creatorID;
	}

	public void setCreatorID(String creatorID) {
		this.creatorID = creatorID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public String getEventTimeFrom() {
		return eventTimeFrom;
	}

	public void setEventTimeFrom(String eventTimeFrom) {
		this.eventTimeFrom = eventTimeFrom;
	}

	public String getEventTimeTill() {
		return eventTimeTill;
	}

	public void setEventTimeTill(String eventTimeTill) {
		this.eventTimeTill = eventTimeTill;
	}

	@Override
	public String toString() {
		return "Announcement [annID=" + annID + ", creatorID=" + creatorID + ", title=" + title + ", description="
				+ description + ", createDate=" + createDate + ", eventDate=" + eventDate + ", eventTimeFrom="
				+ eventTimeFrom + ", eventTimeTill=" + eventTimeTill + "]";
	}

}
