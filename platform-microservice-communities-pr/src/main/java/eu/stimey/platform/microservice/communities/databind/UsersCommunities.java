package eu.stimey.platform.microservice.communities.databind;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

//@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class UsersCommunities {

	private String userid;
	private List<String> member;
	private List<String> moderator_creator;
	private List<String> invites;
	private List<String> requests;
	private List<String> blocked;

	public UsersCommunities() {
		this(null);
	}

	public UsersCommunities(String userid) {
		this.userid = userid;
		this.member = new LinkedList<>();
		this.moderator_creator = new LinkedList<>();
		this.invites = new LinkedList<>();
		this.requests = new LinkedList<>();
		this.blocked = new LinkedList<>();

	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public List<String> getMember() {
		return member;
	}

	public void setMember(List<String> member) {
		this.member = member;
	}

	public void addMember(String member) {
		this.member.add(member);
	}

	public List<String> getModerator_creator() {
		return moderator_creator;
	}

	public void setModerator_creator(List<String> moderator_creator) {
		this.moderator_creator = moderator_creator;
	}

	public void addModerator_creator(String mod_creator) {
		this.moderator_creator.add(mod_creator);
	}

	public List<String> getInvites() {
		return invites;
	}

	public void setInvites(List<String> invites) {
		this.invites = invites;
	}

	public void addInvite(String invite) {
		this.invites.add(invite);
	}

	public List<String> getRequests() {
		return requests;
	}

	public void setRequests(List<String> requests) {
		this.requests = requests;
	}

	public void addRequest(String request) {
		this.requests.add(request);
	}

	public List<String> getBlocked() {
		return blocked;
	}

	public void setBlocked(List<String> blocked) {
		this.blocked = blocked;
	}

	public void addBlock(String block) {
		this.blocked.add(block);
	}

	@Override
	public String toString() {
		return "UsersCommunities [userid=" + userid + ", member=" + member + ", moderator_creator=" + moderator_creator
				+ ", invites=" + invites + ", requests=" + requests + ", blocked=" + blocked + "]";
	}

}
