package eu.stimey.platform.microservice.communities.responses;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.microservice.communities.databind.DiscussionOtherComments;

public class DiscOtherCommentResponse {

	public String commentID;
	public String creatorID;
	public String creatorName;
	public String text;
	public Date timestamp;
	public List<Object> likes;

	public DiscOtherCommentResponse(UserSharedService service, DiscussionOtherComments d) {
		this.commentID = d.getMessageID();
		this.creatorID = d.getCreator();
		this.creatorName = "Archimedis";
		this.text = d.getText();
		this.timestamp = d.getTimestamp();
		this.likes = new LinkedList<>();
		for (String s : d.getLikes()) {
			UserResponse like = new UserResponse(service, s);
			this.likes.add(like);
		}
	}

}
