package eu.stimey.platform.microservice.communities.requests;

import java.util.ArrayList;
import java.util.List;

public class CommunityRequest {

	public String communityName;
	public String privacy;
	public List<String> keywords;
	public String description;

	public CommunityRequest() {
		this.keywords = new ArrayList<>();
	}

	@Override
	public String toString() {
		return "CommunityRequest [communityName=" + communityName + ", privacy=" + privacy + ", keywords=" + keywords
				+ ", description=" + description + "]";
	}

}
