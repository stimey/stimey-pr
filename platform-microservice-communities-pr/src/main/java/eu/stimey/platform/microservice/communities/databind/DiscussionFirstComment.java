package eu.stimey.platform.microservice.communities.databind;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import eu.stimey.platform.library.utils.databind.DateDeserializer;
import eu.stimey.platform.library.utils.databind.DateSerializer;

public class DiscussionFirstComment {

	private String messageID;
	private String creator;
	private String text;
	@JsonSerialize(using = DateSerializer.class)
	@JsonDeserialize(using = DateDeserializer.class)
	private Date timestamp;
	private List<String> likes;
	private List<DiscussionOtherComments> comments;

	public DiscussionFirstComment() {
		this(null, null);
	}

	public DiscussionFirstComment(String creator, String text) {
		this.messageID = new ObjectId().toHexString();
		this.creator = creator;
		this.text = text;
		this.timestamp = new Date();
		this.likes = new LinkedList<>();
		this.comments = new LinkedList<>();
	}

	public String getMessageID() {
		return messageID;
	}

	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public List<String> getLikes() {
		return likes;
	}

	public void setLikes(List<String> likes) {
		this.likes = likes;
	}

	public List<DiscussionOtherComments> getComments() {
		return comments;
	}

	public void setComments(List<DiscussionOtherComments> comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "DiscussionMessage [messageID=" + messageID + ", creator=" + creator + ", text=" + text + ", timestamp="
				+ timestamp + ", likes=" + likes + ", comments=" + comments + "]";
	}

}
