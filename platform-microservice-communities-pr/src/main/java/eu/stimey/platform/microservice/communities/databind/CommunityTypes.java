package eu.stimey.platform.microservice.communities.databind;

public enum CommunityTypes {
	ALL, PENDING, MANAGED, MEMBER, INTERESTING, POPULAR, INVITED, BLOCKED, MISSIONS
}
