package eu.stimey.platform.microservice.communities.databind;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import eu.stimey.platform.library.utils.databind.DateDeserializer;
import eu.stimey.platform.library.utils.databind.DateSerializer;

public class Discussion implements Comparable<Discussion> {

	// TODO: implements sortable für sortieren nach timestamp

	private String discussionID;
	private String creator;
	private String text;
	@JsonSerialize(using = DateSerializer.class)
	@JsonDeserialize(using = DateDeserializer.class)
	private Date timestamp;
	private List<String> likes;
	private String photo;
	private String file;
	private List<DiscussionFirstComment> comments;

	public Discussion() {
		this(null, null);
	}

	public Discussion(String creator, String text) {

		this.discussionID = new ObjectId().toHexString();
		this.creator = creator;
		this.text = text;
		this.likes = new LinkedList<String>();
		this.timestamp = new Date();
		this.comments = new LinkedList<>();
	}

	// TODO: Discussion mit File oder Bild

	public String getDiscussionID() {
		return discussionID;
	}

	public void setDiscussionID(String discussionID) {
		this.discussionID = discussionID;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public List<String> getLikes() {
		return likes;
	}

	public void setLikes(List<String> likes) {
		this.likes = likes;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public DiscussionFirstComment getComment(String id) {
		return comments.stream().filter(d -> d.getMessageID().equals(id)).findAny().orElse(null);
	}

	public List<DiscussionFirstComment> getComments() {
		return comments;
	}

	public void setComments(List<DiscussionFirstComment> comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "Discussion [discussionID=" + discussionID + ", creator=" + creator + ", text=" + text + ", timestamp="
				+ timestamp + ", likes=" + likes + ", photo=" + photo + ", file=" + file + ", comments=" + comments
				+ "]";
	}

	@Override
	public int compareTo(Discussion other) {
		// sort for newest timestamp on first position in List
		return -this.getTimestamp().compareTo(other.getTimestamp());
	}

}
