package eu.stimey.platform.microservice.communities.requests;

public class CommentRequest {

	public String discussion;
	public String parent;
	public String text;

	public CommentRequest() {

	}

}
