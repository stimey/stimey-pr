package eu.stimey.platform.microservice.notification.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.library.utils.databind.DocumentWithId2;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.LinkedList;
import java.util.List;

/**
 * Class to store and retrieve objects from MongoDB
 *
 * @param <T> Template specifies the class of the Object
 */
public class DBService<T extends DocumentWithId2> {

	private final Caching<String, T> dataCache;

	public DBService(Class<T> clazz, String alias, String collectionName) {
		dataCache = new Caching<>(alias, "_id", clazz, collectionName, GlobalVariables.CACHE_CONFIG);
		dataCache.setWorkaround_keyMapper((key) -> new ObjectId(key));
	}

	public void setObject(T t) {
		dataCache.set(t.get_id().toHexString(), t);
	}

	public void setObject(String ObjectID, T t) {
		dataCache.set(ObjectID, t);
	}

	public void delObject(String ObjectID) {
		this.dataCache.del(ObjectID);
	}

	public T getObjectById(String ObjectID) {
		dataCache.find(new Document("_id", new ObjectId(ObjectID)), null, 0, 0, (t) -> t.get_id().toHexString(), true);
		return dataCache.get(ObjectID);
	}

	public List<T> getObjectList(Document filter, int offset, int limit, String order) {
		return this.getObjectByIds(this.getObjectIds(filter, offset, limit, order));
	}

	public List<String> getObjectIds(Document filter, int offset, int limit, String order) {
		Document sort;
		if (order.equals("dec")) {
			sort = new Document("timestamp", -1);
		} else {
			sort = null;
		}
		return dataCache.find(filter, sort, offset, limit, (t) -> (t.get_id().toHexString()), true);
	}

	public List<T> getObjectByIds(List<String> tIDs) {
		List<T> ts = new LinkedList<>();
		for (String tID : tIDs) {
			ts.add(this.getObjectById(tID));
		}
		return ts;
	}

	public Caching<String, T> getCache() {
		return dataCache;
	}
}
