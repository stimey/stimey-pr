package eu.stimey.platform.microservice.notification.rest.databind;

import eu.stimey.platform.library.utils.databind.DocumentWithId2;

import java.util.LinkedList;
import java.util.List;

/**
 * A List containing the notifications of a user, the list will have the same ID
 * as the user it belongs to.
 *
 */
public class NotificationList extends DocumentWithId2 {

	private final LinkedList<Notification> list;

	public LinkedList<String> filterList = new LinkedList<>();

	public NotificationList() {
		this(new LinkedList<>());
	}

	public NotificationList(List<Notification> list) {
		this.list = new LinkedList<>(list);
	}

	public void add(Notification notification) {

		if (!list.isEmpty()) {
			// Date will be used to identify the notification to mark it as read
			// So date will be refreshed until it is unique
			notification.refresh();
			while (list.get(0).date.equals(notification.date)) {
				notification.refresh();
			}
		}

		list.add(0, notification);
	}

	public List<Notification> getList() {
		return list;
	}

	public void setList(List<Notification> list) {
		this.list.clear();
		this.list.addAll(list);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(this.getClass().getSimpleName());

		builder.append(": [");
		for (Notification n : list) {
			builder.append(n.text).append(", ");
		}
		builder.append("]");

		return builder.toString();
	}
}
