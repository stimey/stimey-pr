package eu.stimey.platform.microservice.notification.rest.databind;

import eu.stimey.platform.library.utils.databind.DocumentWithId2;

import java.util.LinkedList;
import java.util.List;

/**
 * A list containing announcements
 */
public class AnnouncementList extends DocumentWithId2 {

	public String listID;
	private LinkedList<Announcement> list;

	public AnnouncementList() {
	}

	public AnnouncementList(String listID) {
		this.listID = listID;
		this.list = new LinkedList<>();
	}

	public void add(Announcement announcement) {
		list.add(0, announcement);
	}

	public List<Announcement> getList() {
		return list;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " (" + listID + "): " + list;
	}
}
