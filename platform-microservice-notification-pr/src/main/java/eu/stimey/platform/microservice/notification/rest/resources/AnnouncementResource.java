package eu.stimey.platform.microservice.notification.rest.resources;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.stimey.platform.library.rest.filter.DefaultAuthenticationFilter;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.library.utils.security.JsonWebToken;
import eu.stimey.platform.microservice.notification.beans.AnnouncementListService;
import eu.stimey.platform.microservice.notification.beans.NotificationListService;
import eu.stimey.platform.microservice.notification.beans.UserSubscriptionListService;
import eu.stimey.platform.microservice.notification.rest.databind.*;
import eu.stimey.platform.microservice.notification.rest.databind.Request.AnnouncementRequest;
import eu.stimey.platform.microservice.notification.startup.Global;
import org.apache.commons.lang3.tuple.Pair;
import org.bson.Document;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.*;

@Path("/announcement")
public class AnnouncementResource {

	@Inject
	AnnouncementListService announcementListService;

	@Inject
	UserSubscriptionListService userSubscriptionListService;

	// To check if a user exists
	@Inject
	NotificationListService notificationListService;

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response post(@Context ContainerRequestContext context) {

		System.out.println("announcement test");

		return Response.ok().entity("AnnouncementRequest test").build();
	}

	/**
	 * Retrieve announcements
	 *
	 * @param context @see Context
	 * @param start   Startposition of notifications
	 * @param count   Count of how many notifications
	 *
	 * @return List of notifications or string with error message (as Response)
	 */
	@Path("retrieve")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAnnouncements(@Context ContainerRequestContext context,
			@QueryParam("start") @DefaultValue("0") int start, @QueryParam("count") @DefaultValue("-1") int count) {

		try {

			UserSubscriptionList userSubscriptionList = getUserSubscriptionList((String) context.getProperty("userid"));
			ArrayList<Announcement> list = new ArrayList<>();

			// Filter the announcements the user marked as deleted
			HashSet<String> filter = userSubscriptionList.deletedAnnouncements;
			HashSet<String> marked = userSubscriptionList.markedAsReadAnnouncements;

			for (String listID : userSubscriptionList.announcementIDs) {
				// list.addAll(getAnnouncementList(listID).getList());
				for (Announcement announcement : getAnnouncementList(listID).getList()) {
					if (!filter.contains(announcement.announcementID)) {
						if (marked.contains(announcement.announcementID)) {
							announcement.isMarkedAsRead = true;
						}
						list.add(announcement);
					}
				}
			}

			return Response.ok().entity(mapDataToAnnouncement(list)).build();
		} catch (Exception e) {

			System.out.println("Retrieve EXCEPTION: " + e);
			return Response.status(Response.Status.BAD_REQUEST).build();
			// return Response.ok().entity("ARetrieve Exception: " + e).build();
		}
	}

	/**
	 * Sends an announcement to a specified ID
	 *
	 * @param context Context
	 * @param listID  ID of who receives the announcement
	 * @param request The notification in NotificationRequest format
	 *
	 * @return Response with status
	 */
	@POST
	@Path("send/{lid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addAnnouncement(@Context ContainerRequestContext context, @PathParam("lid") String listID,
			AnnouncementRequest request) {

		System.out.println("ASend: " + listID);

		if (request == null) {
			System.out.println("Asend error: request is null");
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

		System.out.println("category: " + request.source);
		System.out.println("id: " + request.contentID);

		System.out.println("creatorID: " + request.creatorID);
		System.out.println("title: " + request.title);
		System.out.println("description: " + request.description);
		System.out.println("ref: " + request.contentRef);

		try {
			AnnouncementList announcementList = getAnnouncementList(listID);
			announcementList.add(new Announcement(request));
			announcementListService.setObject(announcementList);

			System.out.println("ASend: " + announcementList);

			return Response.ok().build();
		} catch (Exception e) {
			System.out.println("ASend Exception: " + e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@POST
	@Path("delete/{lid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteAnnouncementSource(@Context ContainerRequestContext context,
			@PathParam("lid") String listID) {

		System.out.println("ADelete: " + listID);

		try {
			AnnouncementList announcementList = getAnnouncementList(listID);
			announcementListService.delObject(announcementList.get_id().toHexString());

			return Response.ok().build();
		} catch (Exception e) {
			System.out.println("ADelete Exception: " + e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/**
	 * Subscribe user to a source of announcements
	 *
	 * @param context @see Context
	 * @param userID  ID of the user who gets registered
	 * @param listID  Any ID to create or get a distributor
	 *
	 * @return Response with status
	 */
	@GET
	@Path("subscribe/{uid}/{lid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response subscribeUser(@Context ContainerRequestContext context, @PathParam("uid") final String userID,
			@PathParam("lid") final String listID) {

		System.out.println("Subscribe");
		System.out.println("User: " + userID);
		System.out.println("List: " + listID);

		try {
			UserSubscriptionList userSubscriptionList = getUserSubscriptionList(userID);
			userSubscriptionList.add(listID);
			userSubscriptionListService.setObject(userSubscriptionList);

			return Response.ok().build();
		} catch (Exception e) {
			System.out.println("Subscribe EXCEPTION: " + e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/**
	 * Unsubscribe user from a source of announcements
	 *
	 * @param context @see Context
	 * @param userID  ID of the user who gets registered
	 * @param listID  Any ID to create or get a distributor
	 *
	 * @return Response with status
	 */
	@GET
	@Path("unsubscribe/{uid}/{lid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response unsubscribeUser(@Context ContainerRequestContext context, @PathParam("uid") final String userID,
			@PathParam("lid") final String listID) {

		System.out.println("Unsubscribe");
		System.out.println("User: " + userID);
		System.out.println("List: " + listID);

		try {
			UserSubscriptionList userSubscriptionList = getUserSubscriptionList(userID);
			userSubscriptionList.remove(listID);
			userSubscriptionListService.setObject(userSubscriptionList);

			return Response.ok().build();
		} catch (Exception e) {
			System.out.println("Unsubscribe EXCEPTION: " + e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/**
	 * Marks announcement as read
	 *
	 * @param context            @see Context
	 * @param announcementIDList IDs of the announcements that will be marked
	 *
	 * @return Response with status
	 */
	@Path("read")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response markAsRead(@Context ContainerRequestContext context, List<String> announcementIDList) {

		final String userID = (String) context.getProperty("userid");

		System.out.println("ARead: " + userID);
		System.out.println("announcementIDList: " + announcementIDList);

		try {
			UserSubscriptionList userSubscriptionList = getUserSubscriptionList((String) context.getProperty("userid"));
			userSubscriptionList.markedAsReadAnnouncements.addAll(announcementIDList);

			userSubscriptionListService.setObject(userSubscriptionList);

			return Response.ok().build();
		} catch (Exception e) {
			System.out.println("ARead EXCEPTION: " + e);
			// return Response.ok().entity("Read Error: userID has no
			// notificationlist").build();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/**
	 * Marks announcement as unread
	 *
	 * @param context            @see Context
	 * @param announcementIDList IDs of the announcements that will be marked
	 *
	 * @return Response with status
	 */
	@Path("unread")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response markAsUnRead(@Context ContainerRequestContext context, List<String> announcementIDList) {

		final String userID = (String) context.getProperty("userid");

		System.out.println("AUnread: " + userID);
		System.out.println("announcementIDList: " + announcementIDList);

		try {
			UserSubscriptionList userSubscriptionList = getUserSubscriptionList((String) context.getProperty("userid"));
			userSubscriptionList.markedAsReadAnnouncements.removeAll(announcementIDList);

			userSubscriptionListService.setObject(userSubscriptionList);

			return Response.ok().build();
		} catch (Exception e) {
			System.out.println("ARead EXCEPTION: " + e);
			// return Response.ok().entity("Read Error: userID has no
			// notificationlist").build();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/**
	 * Deletes announcements for a user
	 *
	 * @param context            Context
	 * @param announcementIDList An array of announcement ids
	 *
	 * @return Response with status
	 */
	@POST
	@Path("delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteAnnouncement(@Context ContainerRequestContext context, List<String> announcementIDList) {

		final String userID = (String) context.getProperty("userid");

		System.out.println("Delete announcement: " + userID);
		System.out.println("announcementIDList: " + announcementIDList);

		try {
			UserSubscriptionList userSubscriptionList = getUserSubscriptionList((String) context.getProperty("userid"));
			userSubscriptionList.deletedAnnouncements.addAll(announcementIDList);
			userSubscriptionList.markedAsReadAnnouncements.removeAll(announcementIDList);

			userSubscriptionListService.setObject(userSubscriptionList);

			return Response.ok().build();

		} catch (Exception e) {
			System.out.println("ADelete EXCEPTION: " + e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/**
	 * Deletes a users subscriptionlist
	 *
	 * @param context Context
	 *
	 * @return Response with status
	 */
	@POST
	@Path("deleteuser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteUser(@Context ContainerRequestContext context) {

		final String userID = (String) context.getProperty("userid");

		System.out.println("ADelete user: " + userID);

		try {
			UserSubscriptionList userSubscriptionList = getUserSubscriptionList((String) context.getProperty("userid"));

			userSubscriptionListService.delObject(userSubscriptionList.get_id().toHexString());

			return Response.ok().build();

		} catch (Exception e) {
			System.out.println("ADelete user EXCEPTION: " + e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	//////// Helper methods

	/**
	 * Returns an AnnouncementList for the given ID or creates one
	 *
	 * @param listID The list ID
	 *
	 * @return The announcement list
	 *
	 * @throws IllegalArgumentException When the ID is malformed
	 */
	private AnnouncementList getAnnouncementList(String listID) throws IllegalArgumentException {

		// Search for the list with the fitting ID
		final List<String> results = announcementListService.getCache().find(new Document("listID", listID), null, 0, 1,
				element -> element.get_id().toHexString(), true);

		// When there is no announcement list, create new one
		if (results.isEmpty()) {
			System.out.println(listID + " has no list, create");
			AnnouncementList announcementList = new AnnouncementList(listID);
			announcementListService.setObject(announcementList);
			return announcementList;
		} else {
			return announcementListService.getObjectById(results.get(0));
		}
	}

	/**
	 * Returns the UserSubscriptionList for the given user ID or creates one
	 *
	 * @param userID The user ID the list belongs to
	 *
	 * @return The UserSubscriptionList belonging to the user
	 *
	 * @throws IllegalArgumentException When user ID is malformed or profile does
	 *                                  not exist
	 */
	private UserSubscriptionList getUserSubscriptionList(String userID) throws IllegalArgumentException {

		// Search for the list with the fitting userID
		final List<String> results = userSubscriptionListService.getCache().find(new Document("userID", userID), null,
				0, 1, element -> element.get_id().toHexString(), true);

		// When there is no userSubscriptionList, but profile exists, then create
		// userSubscriptionList
		if (results.isEmpty()) {
			// Get the type of a user like student or teacher
			String type = checkUser(userID);
			if (type != null) {
				System.out.println("User has no List, create userSubscriptionList for: " + userID);

				UserSubscriptionList userSubscriptionList = new UserSubscriptionList(userID);
				userSubscriptionList.add("all");
				// Type will be added so user get announcements specific for their type
				// like student or teacher
				userSubscriptionList.add(type);
				userSubscriptionListService.setObject(userSubscriptionList);
				return userSubscriptionList;
			} else {
				throw new IllegalArgumentException("Invalid userid");
			}
		} else {
			return userSubscriptionListService.getObjectById(results.get(0));
		}
	}

	/**
	 * Makes a request to the dashboard service to check if the user profile exists
	 * and returns its type.
	 *
	 * @param userID The ID of the user
	 *
	 * @return Type of user if exists else null
	 */
	private static final String checkUser(String userID) {
		Cookie cookie = JsonWebToken.createCookie("", "", "admin", DefaultAuthenticationFilter.getPrivateKey(), Global.DOMAIN);
		Response response = StimeyClient.get(StimeyClient.create(), "dashboard", Global.DOCKER,
				"api/profiles/" + userID, cookie);

		try {
			JsonNode node = new ObjectMapper().readTree(response.readEntity(String.class)).get("usertype");
			if (node != null) {
				return node.textValue();
			}
		} catch (IOException ignore) {
		}
		return null;
	}

	/**
	 * Requests user data from dashboard and adds them to the announcements
	 *
	 * @param list A list with announcements
	 *
	 * @return The modified list
	 *
	 * @throws IOException When the response is invalid
	 */
	private final List<Announcement> mapDataToAnnouncement(List<Announcement> list) throws IOException {

		HashSet<String> set = new HashSet<>();

		// Checking if user ID is valid
		for (Announcement a : list) {
			// Exception when ID is malformed
			try {
				// When the user has a notification list he exists
				if (notificationListService.getObjectById(a.creatorID) != null) {
					set.add(a.creatorID);
				}
			} catch (IllegalArgumentException ignore) {
			}
		}

		if (set.isEmpty()) {
			list.sort((a, b) -> -a.compareDate(b));
			return list;
		}

		Response response = getProfilesByUserList(set);
		JsonNode jsonObject = new ObjectMapper().readTree(response.readEntity(String.class));

		// Data from response gets mapped to announcement list
		if (jsonObject.isArray()) {
			for (JsonNode node : jsonObject) {
				String id = node.get("userid").textValue();
				list.forEach(element -> {

					if (element.creatorID.equals(id)) {
						element.userName = node.get("username").textValue();
						element.userImageID = node.get("avatarImageId").textValue();
					}

				});
			}
		}

		LinkedList<Announcement> result = new LinkedList<>();
		LinkedList<Announcement> missionAnnouncements = new LinkedList<>();
		LinkedList<Announcement> communityAnnouncements = new LinkedList<>();
		LinkedList<Announcement> worldAnnouncements = new LinkedList<>();

		for (Announcement announcement : list) {
			switch (announcement.source) {
			case Announcement.SOURCE_MISSION:
				missionAnnouncements.add(announcement);
				break;
			case Announcement.SOURCE_COMMUNITY:
				communityAnnouncements.add(announcement);
				break;
			case Announcement.SOURCE_WORLD:
				worldAnnouncements.add(announcement);
				break;
			default:
				result.add(announcement);
				break;
			}
		}

		result.addAll(mapMissionDataToAnnouncement(missionAnnouncements));
		result.addAll(mapCommunityDataToAnnouncement(communityAnnouncements));
		result.addAll(mapWorldDataToAnnouncement(worldAnnouncements));

		result.sort((a, b) -> -a.compareDate(b));

		return result;
	}

	/**
	 * Requests mission data from courses and adds them to the announcements
	 *
	 * @param list A list with announcements
	 *
	 * @return The modified list
	 *
	 * @throws IOException When the response is invalid
	 */
	private final List<Announcement> mapMissionDataToAnnouncement(List<Announcement> list) throws IOException {
		Cookie cookie = JsonWebToken.createCookie("", "", "admin", DefaultAuthenticationFilter.getPrivateKey(), Global.DOMAIN);
		HashSet<String> set = new HashSet<>();

		// Checking if mission ID is valid
		for (Announcement a : list) {
			// Exception when ID is malformed
			try {
				notificationListService.getObjectById(a.contentID);
				set.add(a.contentID);
			} catch (IllegalArgumentException ignore) {
			}
		}

		if (set.isEmpty()) {
			return list;
		}

		for (String missionID : set) {
			try {
				Response response = StimeyClient.get(StimeyClient.create(), "courses", Global.DOCKER,
						"api/missions/" + missionID, cookie);
				JsonNode jsonObject = new ObjectMapper().readTree(response.readEntity(String.class));

				jsonObject = jsonObject.get("missionMetadata").get("generalMetadata");

				for (Announcement announcement : list) {
					if (announcement.contentID.equals(missionID)) {
						announcement.contentName = jsonObject.get("title").textValue();
						announcement.contentAvatarID = jsonObject.get("avatarImageId").textValue();
					}
				}

			} catch (IOException ignore) {
			}
		}

		return list;
	}

	/**
	 * Requests community data from communities and adds them to the announcements
	 *
	 * @param list A list with announcements
	 *
	 * @return The modified list
	 *
	 * @throws IOException When the response is invalid
	 */
	private final List<Announcement> mapCommunityDataToAnnouncement(List<Announcement> list) throws IOException {
		Cookie cookie = JsonWebToken.createCookie("", "", "admin", DefaultAuthenticationFilter.getPrivateKey(), Global.DOMAIN);
		HashSet<String> set = new HashSet<>();

		// Checking if mission ID is valid
		for (Announcement a : list) {
			// Exception when ID is malformed
			try {
				notificationListService.getObjectById(a.contentID);
				set.add(a.contentID);
			} catch (IllegalArgumentException ignore) {
			}
		}

		if (set.isEmpty()) {
			return list;
		}

		for (String comID : set) {
			try {
				Response response = StimeyClient.get(StimeyClient.create(), "communities", Global.DOCKER,
						"api/community/" + comID, cookie);
				JsonNode jsonObject = new ObjectMapper().readTree(response.readEntity(String.class));

				for (Announcement announcement : list) {
					if (announcement.contentID.equals(comID)) {
						announcement.contentName = jsonObject.get("communityName").textValue();
						announcement.contentAvatarID = jsonObject.get("communityPhoto").textValue();
					}
				}

			} catch (IOException ignore) {
			}
		}

		return list;
	}

	/**
	 * Requests world data from worlds and adds them to the announcements
	 *
	 * @param list A list with announcements
	 *
	 * @return The modified list
	 *
	 * @throws IOException When the response is invalid
	 */
	private final List<Announcement> mapWorldDataToAnnouncement(List<Announcement> list) throws IOException {
		Cookie cookie = JsonWebToken.createCookie("", "", "admin", DefaultAuthenticationFilter.getPrivateKey(), Global.DOMAIN);
		HashSet<String> set = new HashSet<>();

		// Checking if mission ID is valid
		for (Announcement a : list) {
			// Exception when ID is malformed
			try {
				notificationListService.getObjectById(a.contentID);
				set.add(a.contentID);
			} catch (IllegalArgumentException ignore) {
			}
		}

		if (set.isEmpty()) {
			return list;
		}

		for (String worldID : set) {
			try {
				Response response = StimeyClient.get(StimeyClient.create(), "planets", Global.DOCKER,
						"api/worlds/get/" + worldID, cookie);
				JsonNode jsonObject = new ObjectMapper().readTree(response.readEntity(String.class));

				for (Announcement announcement : list) {
					if (announcement.contentID.equals(worldID)) {
						announcement.contentName = jsonObject.get("name").textValue();
					}
				}

			} catch (IOException ignore) {
			}
		}

		return list;
	}

	/**
	 * Makes a request to the dashboard service to the profiles endpoint
	 *
	 * @param userIds List of user IDs
	 *
	 * @return A Response containing user data
	 */
	private static final Response getProfilesByUserList(Collection<String> userIds) {
		Cookie cookie = JsonWebToken.createCookie("", "", "admin", DefaultAuthenticationFilter.getPrivateKey(), Global.DOMAIN);
		List<Pair<String, Object>> queryParams = new ArrayList<>();
		queryParams.add(Pair.of("query", ""));
		userIds.forEach(userid -> queryParams.add(Pair.of("ids[]", userid)));

		return StimeyClient.get(StimeyClient.create(), "dashboard", Global.DOCKER, "api/profiles", cookie, queryParams);
	}
}
