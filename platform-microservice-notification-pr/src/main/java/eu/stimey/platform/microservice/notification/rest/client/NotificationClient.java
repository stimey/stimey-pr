package eu.stimey.platform.microservice.notification.rest.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import eu.stimey.platform.library.rest.filter.DefaultAuthenticationFilter;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.library.utils.security.JsonWebToken;
import eu.stimey.platform.microservice.notification.startup.Global;
import org.apache.commons.lang3.tuple.Pair;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Class to send notifications and announcements to the notification
 * microservice
 */
public class NotificationClient {

	/**
	 * Sends a notification request to the notification service
	 *
	 * @param receiverUserID  The ID of the User who is to receive the notification
	 *
	 * @param userIDs         The ID of the person mentioned in the notification
	 * @param postContentText Text of a content link
	 * @param postContentRef  The reference of a context link
	 * @param text            The text showing inside the notification
	 * @param cookie          @see Cookie
	 *
	 * @return The response of the request
	 */
	public static final Response sendNotification(String receiverUserID, String origin, Collection<String> userIDs,
			String postContentText, String postContentRef, String text, String icon, Cookie cookie) {

		return StimeyClient.post(StimeyClient.create(), "notification", Global.DOCKER,
				"api/notification/send/" + receiverUserID, cookie,
				notificationToJsonObject(origin, userIDs, postContentText, postContentRef, text, icon).toString());
	}

	// Same as above but creates own cookie
	public static final Response sendNotification(String receiverUserID, String origin, Collection<String> userIDs,
			String postContentText, String postContentRef, String text, String icon) {

		Cookie cookie = JsonWebToken.createCookie("", "", "admin", DefaultAuthenticationFilter.getPrivateKey(), Global.DOMAIN);
		return sendNotification(receiverUserID, origin, userIDs, postContentText, postContentRef, text, icon, cookie);
	}

	/**
	 * Sends a notification request to broadcast the notification
	 *
	 * @param distributorID   The ID of the User who is to receive the notification
	 *
	 * @param userIDs         The ID of the person mentioned in the notification
	 * @param postContentText Text of a content link
	 * @param postContentRef  The reference of a context link
	 * @param text            The text showing inside the notification
	 * @param cookie          @see Cookie
	 *
	 * @return The response of the request
	 */
	public static final Response broadcastNotification(String distributorID, String origin, Collection<String> userIDs,
			String postContentText, String postContentRef, String text, String icon, Cookie cookie) {

		return StimeyClient.post(StimeyClient.create(), "notification", Global.DOCKER,
				"api/notification/distribute/" + distributorID, cookie,
				notificationToJsonObject(origin, userIDs, postContentText, postContentRef, text, icon).toString());
	}

	// Same as above but creates own cookie
	public static final Response broadcastNotification(String distributorID, String origin, Collection<String> userIDs,
			String postContentText, String postContentRef, String text, String icon) {

		Cookie cookie = JsonWebToken.createCookie("", "", "admin", DefaultAuthenticationFilter.getPrivateKey(), Global.DOMAIN);
		return broadcastNotification(distributorID, origin, userIDs, postContentText, postContentRef, text, icon,
				cookie);
	}

	/**
	 * Sends a notification request to broadcast the notification
	 *
	 *
	 * @param userIDList      The IDs of the users to receive the notification
	 * @param userIDs         The ID of the person mentioned in the notification
	 * @param postContentText Text of a content link
	 * @param postContentRef  The reference of a context link
	 * @param text            The text showing inside the notification
	 * @param cookie          @see Cookie
	 *
	 * @return The response of the request
	 */
	public static final Response broadcastNotification(Collection<String> userIDList, String origin,
			Collection<String> userIDs, String postContentText, String postContentRef, String text, String icon,
			Cookie cookie) {

		return StimeyClient.post(StimeyClient.create(), "notification", Global.DOCKER, "api/notification/broadcast",
				cookie, broadcastToJsonString(userIDList,
						notificationToJsonObject(origin, userIDs, postContentText, postContentRef, text, icon)));
	}

	// Same as above but creates own cookie
	public static final Response broadcastNotification(Collection<String> userIDList, String origin,
			Collection<String> userIDs, String postContentText, String postContentRef, String text, String icon) {

		Cookie cookie = JsonWebToken.createCookie("", "", "admin", DefaultAuthenticationFilter.getPrivateKey(), Global.DOMAIN);
		return broadcastNotification(userIDList, origin, userIDs, postContentText, postContentRef, text, icon, cookie);
	}

	/**
	 *
	 * Sends a request to register a user to a distributor
	 *
	 * @param userID        ID of user to be registered
	 * @param distributorID The ID of the distributor
	 *
	 * @return The response of the request
	 */
	public static final Response registerUserToDistributor(String userID, String distributorID) {

		List<Pair<String, Object>> queryParams = new ArrayList<>();
		queryParams.add(Pair.of("did", distributorID));

		Cookie cookie = JsonWebToken.createCookie("", "", "admin", DefaultAuthenticationFilter.getPrivateKey(), Global.DOMAIN);
		return StimeyClient.get(StimeyClient.create(), "notification", Global.DOCKER,
				"api/notification/register/" + userID, cookie, queryParams);
	}

	/**
	 *
	 * Sends a request to unregister a user to a distributor
	 *
	 * @param userID        ID of user to be unregistered
	 * @param distributorID The ID of the distributor
	 *
	 * @return The response of the request
	 */
	public static final Response unregisterUserFromDistributor(String userID, String distributorID) {

		List<Pair<String, Object>> queryParams = new ArrayList<>();
		queryParams.add(Pair.of("did", distributorID));

		Cookie cookie = JsonWebToken.createCookie("", "", "admin", DefaultAuthenticationFilter.getPrivateKey(), Global.DOMAIN);
		return StimeyClient.get(StimeyClient.create(), "notification", Global.DOCKER,
				"api/notification/unregister/" + userID, cookie, queryParams);
	}

	//// Announcement methods

	/**
	 * Sends a announcement request to the announcement service
	 *
	 * @param receiverListID The ID of what the announcement belongs to like
	 *                       student, teacher or all
	 * @param source         The source of the announcement, either community, world
	 *                       or mission
	 * @param contentID      The ID of the community, world or mission
	 * @param creatorID      The ID of the user who creates the announcement
	 * @param title          Title of the announcement
	 * @param description    Text of the announcement from the translation.json
	 * @param contentText    The content of the notification like mission or course
	 * @param contentRef     The link to the content
	 * @param cookie         @see Cookie
	 *
	 * @return The response of the request
	 */
	public static final Response sendAnnouncement(String receiverListID, String source, String contentID,
			String creatorID, String title, String description, String contentText, String contentRef, Cookie cookie) {

		return StimeyClient.post(StimeyClient.create(), "notification", Global.DOCKER,
				"api/announcement/send/" + receiverListID, cookie,
				announcementToJsonString(source, contentID, creatorID, title, description, contentText, contentRef));
	}

	// Same as above but creates own cookie
	public static final Response sendAnnouncement(String receiverListID, String source, String contentID,
			String creatorID, String title, String description, String contentText, String contentRef) {
		Cookie cookie = JsonWebToken.createCookie("", "", "admin", DefaultAuthenticationFilter.getPrivateKey(), Global.DOMAIN);
		return sendAnnouncement(receiverListID, source, contentID, creatorID, title, description, contentText,
				contentRef, cookie);
	}

	/**
	 * Subscribes a user to an announcement source
	 *
	 * @param userID The user ID
	 * @param listID The ID of the source the user will be registered
	 * @param cookie @see Cookie
	 * @return The response from the notification service
	 */
	public static final Response subscribeUserToAnnouncement(String userID, String listID, Cookie cookie) {
		return StimeyClient.get(StimeyClient.create(), "notification", Global.DOCKER,
				"api/announcement/subscribe/" + userID + "/" + listID, cookie);
	}

	/**
	 * Unsubscribes a user from an announcement source
	 *
	 * @param userID The user ID
	 * @param listID The ID of the source the user will be registered
	 * @param cookie @see Cookie
	 * @return The response from the notification service
	 */
	public static final Response unSubscribeUserFromAnnouncement(String userID, String listID, Cookie cookie) {
		return StimeyClient.get(StimeyClient.create(), "notification", Global.DOCKER,
				"api/announcement/unsubscribe/" + userID + "/" + listID, cookie);
	}

	//////// Helper methods

	/**
	 * Json parsing did not work, this is a workaround to parse a normal
	 * notification to Json
	 *
	 * @param userIDs         @see Notification
	 * @param postContentText @see Notification
	 * @param postContentRef  @see Notification
	 * @param text            @see Notification
	 * @return Json representation of the Notification as String
	 */
	protected static final ObjectNode notificationToJsonObject(String origin, Collection<String> userIDs,
			String postContentText, String postContentRef, String text, String icon) {

		ObjectMapper mapper = new ObjectMapper();
		ObjectNode node = mapper.createObjectNode();

		node.put("origin", origin);
		node.putArray("userIDs").addAll((ArrayNode) mapper.valueToTree(userIDs));
		node.put("postContentText", postContentText);
		node.put("postContentRef", postContentRef);
		node.put("text", text);
		node.put("icon", icon);

		return node;
	}

	/**
	 * Json parsing did not work, this is a workaround to parse a BroadcastRequest
	 * to Json
	 *
	 * @param userIDList List of userIDs
	 * @param node       A ObjectNode of the Json representation of a notification
	 *
	 * @return Json representation of the BroadCastRequest as String
	 */
	protected static final String broadcastToJsonString(Collection<String> userIDList, ObjectNode node) {
		node.putArray("userIDList").addAll((ArrayNode) new ObjectMapper().valueToTree(userIDList));
		return node.toString();
	}

	/**
	 * Json parsing did not work, this is a workaround to parse a
	 * AnnouncementRequest to Json
	 *
	 * @param source      @see Announcement
	 * @param contentID   @see Announcement
	 * @param creatorID   @see Announcement
	 * @param title       @see Announcement
	 * @param description @see Announcement
	 * @param contentText @see Announcement
	 * @param contentRef  @see Announcement
	 *
	 * @return Json representation of the AnnoucementRequest as String
	 */
	protected static final String announcementToJsonString(String source, String contentID, String creatorID,
			String title, String description, String contentText, String contentRef) {

		ObjectMapper mapper = new ObjectMapper();
		ObjectNode node = mapper.createObjectNode();

		node.put("source", source);
		node.put("contentID", contentID);
		node.put("creatorID", creatorID);
		node.put("title", title);
		node.put("description", description);
		node.put("contentText", contentText);
		node.put("contentRef", contentRef);

		return node.toString();
	}
}
