package eu.stimey.platform.microservice.notification.rest.databind;

import eu.stimey.platform.library.utils.databind.DocumentWithId2;
import java.util.HashSet;

/**
 * A list containing the IDs of sources of announcements the user is subscribed
 * to
 */
public class UserSubscriptionList extends DocumentWithId2 {

	public String userID; // The ID of the user this list belongs to
	public HashSet<String> announcementIDs; // A set containing IDs were the user fetches announcements from
	public HashSet<String> deletedAnnouncements; // A set containing announcement IDs from deleted announcements
	public HashSet<String> markedAsReadAnnouncements; // A set containing announcement IDs that are marked as read

	public UserSubscriptionList() {
	}

	public UserSubscriptionList(String userID) {
		this.userID = userID;
		this.announcementIDs = new HashSet<>();
		this.deletedAnnouncements = new HashSet<>();
		this.markedAsReadAnnouncements = new HashSet<>();
	}

	public boolean add(String id) {
		return announcementIDs.add(id);
	}

	public boolean remove(String id) {
		return announcementIDs.remove(id);
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " (" + userID + "): " + announcementIDs;
	}
}
