package eu.stimey.platform.microservice.notification.rest.databind;

import eu.stimey.platform.microservice.notification.rest.databind.Request.AnnouncementRequest;

import java.util.UUID;

/**
 * Representation of an Announcement
 */
public class Announcement {

	// Constants for possible sources
	public static final String SOURCE_MISSION = "mission"; // fetches avatar and name from missions
	public static final String SOURCE_COMMUNITY = "community"; // fetches avatar and name from communities
	public static final String SOURCE_WORLD = "world"; // worlds dont have avatars at this moment so skip
	public static final String SOURCE_USER = "user"; // uses avatar and name from user who created this, if it is an
														// admin announcement

	// Format of an announcement
	// {contentAvatar} (contentName) (title)
	// by (username): "description"

	public String announcementID; // To identify the announcement for marking it as read or delete it

	public String source; // Source like mission, course or community
	// it decides where the contentID pulls the data for the contentAvatarID comes
	// from

	public String contentID; // ID of the mission or course
	public String contentName; // Name of the mission or course, stays blank until it is retrieved
	public String contentAvatarID; // ID of the avatar of the mission or course, stays blank until it is retrieved

	public String creatorID; // User ID of the creator of this announcement
	public String userName; // Field stays blank until the announcements gets retrieved
	public String userImageID; // Field stays blank until the announcements gets retrieved

	public String title; // The title of the announcement
	public String description; // The text of the announcement

	public String contentText; // Name of the course or mission like "Happy Math"
	public String contentRef; // link to the content

	public String createDate; // Creation date of this message in ms

	public boolean isMarkedAsRead = false; // If it is marked as read

	public Announcement() {
	}

	public Announcement(String source, String contentID, String creatorID, String title, String description,
			String contentText, String contentRef) {

		this.announcementID = UUID.randomUUID().toString().replace("-", "");

		this.source = source;
		this.contentID = contentID;
		this.creatorID = creatorID;
		this.title = title;
		this.description = description;
		this.contentText = contentText;
		this.contentRef = contentRef;
		this.createDate = Long.toString(System.currentTimeMillis());
	}

	public Announcement(AnnouncementRequest request) {
		this(request.source, request.contentID, request.creatorID, request.title, request.description,
				request.contentText, request.contentRef);
	}

	/**
	 * Compares if an announcement is older, without parsing the string to a number
	 *
	 * @param announcement the announcement to compare
	 * @return negative if the input is older 0 if same, positive if input is newer
	 */
	public int compareDate(Announcement announcement) {
		int result = this.createDate.length() - announcement.createDate.length();
		if (result == 0) {
			for (int i = 0, end = this.createDate.length(); i < end; i++) {
				result = this.createDate.charAt(i) - announcement.createDate.charAt(i);
				if (result != 0) {
					return result;
				}
			}
			return 0;
		} else {
			return result;
		}
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " (" + createDate + " | " + (title.isEmpty() ? "{No title}" : title)
				+ ")";
	}
}
