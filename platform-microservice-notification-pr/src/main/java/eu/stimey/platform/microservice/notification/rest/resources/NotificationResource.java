package eu.stimey.platform.microservice.notification.rest.resources;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.stimey.platform.library.rest.filter.DefaultAuthenticationFilter;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.library.utils.security.JsonWebToken;
import eu.stimey.platform.microservice.notification.beans.NotificationListService;
import eu.stimey.platform.microservice.notification.rest.databind.Notification;
import eu.stimey.platform.microservice.notification.rest.databind.NotificationList;
import eu.stimey.platform.microservice.notification.rest.databind.Request.BroadcastRequest;
import eu.stimey.platform.microservice.notification.rest.databind.Request.NotificationRequest;
import eu.stimey.platform.microservice.notification.startup.Global;
import org.apache.commons.lang3.tuple.Pair;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Path("/notification")
public class NotificationResource {

	@Inject
	NotificationListService notificationListService;

	/**
	 * Retrieve notifications for specified user
	 *
	 * @param context @see Context
	 * @param unRead  Retrieve only unread notifications if 1
	 * @param start   Startposition of notifications
	 * @param count   Count of how many notifications
	 *
	 * @return List of notifications or string with error message (as Response)
	 */
	@Path("retrieve")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNotifications(@Context ContainerRequestContext context,
			@QueryParam("new") @DefaultValue("0") int unRead, @QueryParam("start") @DefaultValue("0") int start,
			@QueryParam("count") @DefaultValue("-1") int count) {

		try {
			List<Notification> list = getNotifications((String) context.getProperty("userid"), unRead, start, count);

			return Response.ok().entity(mapProfileDataToNotification(list)).build();
		} catch (Exception e) {
			System.out.println("Retrieve EXCEPTION: " + e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/**
	 * Get the count of unread notifications for the menu header
	 *
	 * @param context @see Context
	 *
	 * @return Count of new notifications
	 */
	@Path("retrievenew")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNewCount(@Context ContainerRequestContext context) {

		final String userID = (String) context.getProperty("userid");

		try {
			List<Notification> list = getNotifications(userID, 1, 0, -1);

			int i = 0;

			for (Notification notification : list) {
				if (!notification.isMarkedAsRead) {
					i++;
				}
			}

			return Response.ok().entity(i).build();
		} catch (Exception e) {
			System.out.println("New EXCEPTION: " + e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/**
	 * Marks notification as read
	 *
	 * @param context            @see Context
	 * @param notificationIDList IDs of the notification that will be marked
	 *
	 * @return Status message as response
	 */
	@Path("read")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response markAsRead(@Context ContainerRequestContext context, List<String> notificationIDList) {

		final String userID = (String) context.getProperty("userid");

		System.out.println("Read: " + userID);
		System.out.println("notificationID: " + notificationIDList);

		try {

			final NotificationList notificationlist = getNotificationList(userID);

			if (notificationlist == null) {
				return Response.ok().entity("Read Error: userID has no notificationlist").build();
			}

			// Mark as read if is in list
			notificationlist.getList().forEach((element) -> {
				if (notificationIDList.contains(element.date)) {
					element.isMarkedAsRead = true;
				}
			});

			notificationListService.setObject(notificationlist);

			return Response.ok().build();
		} catch (Exception e) {
			System.out.println("Read EXCEPTION: " + e);
			// return Response.ok().entity("Read Error: userID has no
			// notificationlist").build();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/**
	 * Marks notification as unread
	 *
	 * @param context            @see Context
	 * @param notificationIDList IDs of the notification that will be marked
	 *
	 * @return Status message as response
	 */
	@Path("unread")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response markAsUnRead(@Context ContainerRequestContext context, List<String> notificationIDList) {

		final String userID = (String) context.getProperty("userid");

		System.out.println("Unread: " + userID);
		System.out.println("notificationID: " + notificationIDList);

		try {

			final NotificationList notificationlist = getNotificationList(userID);

			if (notificationlist == null) {
				return Response.ok().entity("Read Error: userID has no notificationlist").build();
			}

			// Mark as unread if is in list
			notificationlist.getList().forEach((element) -> {
				if (notificationIDList.contains(element.date)) {
					element.isMarkedAsRead = false;
				}
			});

			notificationListService.setObject(notificationlist);

			return Response.ok().build();
		} catch (Exception e) {
			System.out.println("Read EXCEPTION: " + e);
			// return Response.ok().entity("Read Error: userID has no
			// notificationlist").build();
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/**
	 * Send a notification to a specified user
	 *
	 * @param context Context
	 * @param userID  ID of the user who receives this notification
	 * @param request The notification in NotificationRequest format
	 *
	 * @return Status message as response
	 */
	@POST
	@Path("send/{uid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addNotification(@Context ContainerRequestContext context,
			@CookieParam("access_token") Cookie cookie, @PathParam("uid") String userID, NotificationRequest request) {

		System.out.println("Send: " + userID);

		if (request == null) {
			System.out.println("send error: request is null");
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

//        System.out.println("origin: " + request.origin);
//        System.out.println("name: " + request.userIDs);
		System.out.println("cont text: " + request.postContentText);
//        System.out.println("cont ref: " + request.postContentRef);
//        System.out.println("text: " + request.text);
//        System.out.println("icon: " + request.icon);

		try {
			// Send notificaiton
			sendNotification(userID, new Notification(request));

			return Response.ok().build();
		} catch (Exception e) {
			System.out.println("Send Exception: " + e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/**
	 * Broadcasts a notification to a list of user IDs
	 *
	 * @param request The notification to be send and the list of user IDs
	 *
	 * @return Status message as response
	 */
	@POST
	@Path("broadcast")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response broadcastNotification(@Context ContainerRequestContext context, BroadcastRequest request) {
		//System.out.println("broadcast");
		//System.out.println("id: " + request.userIDList);

		if (request!=null) {
			final Notification notification = new Notification(request);
		
			if (request.userIDList!=null)
				for (String userID : request.userIDList) {
					try {
						sendNotification(userID, notification);
					} catch (IllegalArgumentException ignore) {
					}
				}
		}

		return Response.ok().build();
	}

	/**
	 * Deletes notifications
	 *
	 * @param context            Context
	 * @param notificationIDList An array of notification ids
	 *
	 * @return Status message as response
	 */
	@POST
	@Path("delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteNotification(@Context ContainerRequestContext context, List<String> notificationIDList) {

		final String userID = (String) context.getProperty("userid");

		System.out.println("Delete notification: " + userID);
		System.out.println("notificationID: " + notificationIDList);

		try {
			final NotificationList notificationlist = getNotificationList(userID);

			// Remove if is in ID list (new list with only nondeleted notifications)
			notificationlist.setList(
					notificationlist.getList().stream().filter((element) -> !notificationIDList.contains(element.date))
							.collect(Collectors.toCollection(LinkedList::new)));

			notificationListService.setObject(notificationlist);

			return Response.ok().build();

		} catch (Exception e) {
			System.out.println("Delete EXCEPTION: " + e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/**
	 * Adds or removes origin from filter list of a user
	 *
	 * @param context    @see ContainerRequestContext
	 * @param originList
	 *
	 * @return Status message as response
	 */
	@POST
	@Path("filter")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response filter(@Context ContainerRequestContext context, List<String> originList) {

		final String userID = (String) context.getProperty("userid");

		System.out.println("Filter: " + userID);
		System.out.println("origin: " + originList);

		try {
			final NotificationList notificationlist = getNotificationList(userID);

			for (String origin : originList) {
				// Remove if it contains the origin else add it to the filter
				if (!notificationlist.filterList.remove(origin)) {
					notificationlist.filterList.add(origin);
				}
			}

			notificationListService.setObject(notificationlist);

			return Response.ok().build();

		} catch (Exception e) {
			System.out.println("Delete EXCEPTION: " + e);
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	/**
	 * Returns a list for the given user ID or creates one
	 *
	 * @param userID The user ID
	 * @return The notificationlist belonging to the user
	 *
	 * @throws IllegalArgumentException When user ID is malformed or profile does
	 *                                  not exist
	 */
	private NotificationList getNotificationList(String userID) throws IllegalArgumentException {

		NotificationList list = notificationListService.getObjectById(userID);

		// When there is no list, but profile exists, then create list
		if (list == null) {
			if (checkUser(userID)) {
				System.out.println("User has no List, create list for: " + userID);

				list = new NotificationList();
				list.set_id(new ObjectId(userID));
				notificationListService.setObject(list);
			} else {
				throw new IllegalArgumentException("User has no profile");
			}
		}

		return list;
	}

	/**
	 * Send a notification to a specified user, add it to his list
	 *
	 * @param userID       ID of the user who receives this notification
	 * @param notification The notification to be send
	 *
	 * @throws IllegalArgumentException When user ID is malformed or profile does
	 *                                  not exist
	 */
	private void sendNotification(String userID, Notification notification) throws IllegalArgumentException {

		// Synchronized to avoid shadow transactions
		// intern() to synchronize with the right string object
		synchronized (userID.intern()) {
			final NotificationList notificationList = getNotificationList(userID);

			// When there is no origin it wont get grouped
			if (!notification.origin.isEmpty()) {
				// Check if notification with same origin exists
				// Index loop because item position gets modified
				List<Notification> list = notificationList.getList();
				for (int i = 0, end = list.size(); i < end; i++) {
					Notification n = list.get(i);
					if (n.origin.equals(notification.origin)) {

						n.setUserIDs(notification.userData.keySet());
						n.refresh();
						n.isMarkedAsRead = false;

						// place notification with new time at the front
						list.remove(n);
						list.add(0, n);

						notificationListService.setObject(notificationList);
						return;
					}
				}
			}

			notificationList.add(notification);
			notificationListService.setObject(notificationList);
		}
	}

	/**
	 * Retrieve list of notifications for specified user
	 *
	 * @param unRead Retrieve only unread notifications if 1
	 * @param userID ID of Users whose notification will be retrieved
	 * @param start  Startposition of notifications
	 * @param count  Count of how many notifications
	 *
	 * @return List of notifications or string
	 *
	 * @throws IllegalArgumentException When user ID is malformed or profile does
	 *                                  not exist
	 */
	private List<Notification> getNotifications(String userID, int unRead, int start, int count)
			throws IllegalArgumentException {

		final NotificationList notificationlist = getNotificationList(userID);

		List<Notification> list = notificationlist.getList();

		if (unRead == 1) {
			// Only unread elements
			list = list.stream().filter(element -> !element.isMarkedAsRead)
					.collect(Collectors.toCollection(LinkedList::new));
		}

		// Check if empty
		if (list.size() == 0) {
			return list;
		}

		// Check if start value is ok
		if (start < 0 || start >= list.size() || count < -1) {
			throw new IllegalArgumentException("Retrieve Error: invalid start or count: " + start + ", " + count);
		}

		// Check if end value is ok and inside list bounds
		int end;
		if (count == -1) {
			end = list.size();
		} else {
			end = start + count;

			if (end >= list.size()) {
				end = list.size();
			}
		}

		return list.subList(start, end);
	}

	/**
	 * Requests user data from dashboard and adds them to the notifications in case
	 * a notification contains a username or profile pic
	 *
	 * @param list A list with notifications
	 *
	 * @return The modified list
	 *
	 * @throws IOException When the response is invalid
	 */
	private final List<Notification> mapProfileDataToNotification(List<Notification> list) throws IOException {

		HashSet<String> set = new HashSet<>();

		// Checking if user ID is valid and user exists
		for (Notification n : list) {
			for (String userID : n.userData.keySet()) {
				// Exception gets thrown when ID invalid formatted
				try {
					if (notificationListService.getObjectById(userID) != null) {
						set.add(userID);
					}
				} catch (IllegalArgumentException ignore) {
				}
			}
		}

		if (set.isEmpty()) {
			return list;
		}

		Response response = getProfilesByUserList(set);
		JsonNode jsonObject = new ObjectMapper().readTree(response.readEntity(String.class));

		// Data from response gets mapped to notification list
		if (jsonObject.isArray()) {
			for (JsonNode node : jsonObject) {
				String id = node.get("userid").textValue();
				list.forEach(element -> {

					if (element.userData.containsKey(id)) {
						element.userData.put(id, new String[] { node.get("username").textValue(),
								node.get("avatarImageId").textValue() });
					}

				});
			}
		}

		return list;
	}

	/**
	 * Makes a request to the dashboard service to the profiles endpoint
	 *
	 * @param userIds List of user IDs
	 *
	 * @return A Response containing user data
	 */
	private static final Response getProfilesByUserList(Collection<String> userIds, Cookie cookie) {
		List<Pair<String, Object>> queryParams = new LinkedList<>();
		queryParams.add(Pair.of("query", ""));
		userIds.forEach(userid -> queryParams.add(Pair.of("ids[]", userid)));

		return StimeyClient.get(StimeyClient.create(), "dashboard", Global.DOCKER, "api/profiles", cookie, queryParams);
	}

	// Same as above but creates own cookie
	private static final Response getProfilesByUserList(Collection<String> userIds) {
		return getProfilesByUserList(userIds,
				JsonWebToken.createCookie("", "", "admin", DefaultAuthenticationFilter.getPrivateKey(), Global.DOMAIN));
	}

	/**
	 * Makes a request to the dashboard service to check if the user profile exists.
	 *
	 * @param userID The ID of the user
	 *
	 * @return True if the profile exists else false
	 */
	private static final boolean checkUser(String userID, Cookie cookie) {
		Response response = StimeyClient.get(StimeyClient.create(), "dashboard", Global.DOCKER,
				"api/profiles/" + userID, cookie);

		try {
			return new ObjectMapper().readTree(response.readEntity(String.class)).has("userid");
		} catch (IOException ignore) {
			return false;
		}
	}

	// Same as above but creates own cookie
	private static final boolean checkUser(String userID) {
		return checkUser(userID, JsonWebToken.createCookie("", "", "admin", DefaultAuthenticationFilter.getPrivateKey(), Global.DOMAIN));
	}
}
