package eu.stimey.platform.microservice.notification.beans;

import eu.stimey.platform.microservice.notification.rest.databind.UserSubscriptionList;

public final class UserSubscriptionListService extends DBService<UserSubscriptionList> {
	public UserSubscriptionListService() {
		super(UserSubscriptionList.class, "ms_notification", "usersubscription");
	}
}
