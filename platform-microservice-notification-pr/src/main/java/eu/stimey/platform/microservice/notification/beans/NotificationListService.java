package eu.stimey.platform.microservice.notification.beans;

import eu.stimey.platform.microservice.notification.rest.databind.NotificationList;

/**
 * Class for storing NotificationLists, the list ID will be the same as the ID
 * of the user it belongs to.
 */
public final class NotificationListService extends DBService<NotificationList> {
	public NotificationListService() {
		super(NotificationList.class, "ms_notification", "notification");
	}
}
