package eu.stimey.platform.microservice.notification.rest.databind.Request;

public class AnnouncementRequest {
	public String source;
	public String contentID;
	public String creatorID;
	public String title;
	public String description;
	public String contentText;
	public String contentRef;
}
