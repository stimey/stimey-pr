package eu.stimey.platform.microservice.notification.rest.databind;

import eu.stimey.platform.microservice.notification.rest.databind.Request.NotificationRequest;

import java.util.Collection;
import java.util.HashMap;

/**
 * Notification
 *
 *
 */
public class Notification {

	// default Notification Format
	// (Username) (liked) (the post contents).
	// 08.09.2018 at 9:00 am

	// multiple users Notification Format
	// (User 1), (User 2) and (count) others (liked) (the post contents)
	// 08.09.2018 at 9:00 am

	public String origin = ""; // Notifications will be grouped if not null by this value, to avoid flooding

	public HashMap<String, String[]> userData = new HashMap<>(); // user IDs are keyset, value is String[]{username,
																	// userImageID}
	public String postContentText; // Text des Kontentlinks
	public String postContentRef; // Link
	public String text; // zB. "liked", but as property from translation.json

	public String date; // in milliseconds, will also be used as ID to mark notification as read and
						// delete, so it have to be unique

	public String icon; // File id of an icon showing in the notification

	public boolean isMarkedAsRead = false; // If notification is old or new, if it is marked as read

	public Notification() {
	}

	public Notification(String origin, Collection<String> userIDs, String postContentText, String postContentRef,
			String text, String date, String icon, boolean isMarkedAsRead) {

		this.origin = origin;

		setUserIDs(userIDs);

		this.postContentText = postContentText;
		this.postContentRef = postContentRef;
		this.text = text;
		this.date = date;
		this.icon = icon;
		this.isMarkedAsRead = isMarkedAsRead;
	}

	public Notification(String origin, Collection<String> userIDs, String postContentText, String postContentRef,
			String text, String icon) {
		this(origin, userIDs, postContentText, postContentRef, text, Long.toString(System.currentTimeMillis()), icon,
				false);
	}

	/**
	 * Set user IDs
	 *
	 * @param userIDs The IDs
	 */
	public void setUserIDs(Collection<String> userIDs) {
		this.userData.clear();
		for (String userID : userIDs) {
			// Only the ID will be put in the keyset, the data will be put when retrieving
			// the notifictation
			this.userData.put(userID, null);
		}
	}

	/**
	 * Refreshes timestamp
	 */
	public void refresh() {
		this.date = Long.toString(System.currentTimeMillis());
	}

	/**
	 * Notification from Request
	 *
	 * @param request The request containing the data
	 */
	public Notification(NotificationRequest request) {
		this(request.origin, request.userIDs, request.postContentText, request.postContentRef, request.text,
				request.icon);
	}
}
