package eu.stimey.platform.microservice.notification.rest.databind.Request;

import java.util.Set;

/**
 * Request format of a notification
 */
public class NotificationRequest {
	public String origin;
	public Set<String> userIDs;
	public String postContentText;
	public String postContentRef;
	public String text;
	public String icon;

	// public NotificationRequest(){}
}
