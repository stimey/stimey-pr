package eu.stimey.platform.microservice.notification.beans;

import eu.stimey.platform.microservice.notification.rest.databind.AnnouncementList;

public final class AnnouncementListService extends DBService<AnnouncementList> {
	public AnnouncementListService() {
		super(AnnouncementList.class, "ms_notification", "announcement");
	}
}
