package eu.stimey.platform.microservice.robot.databind;

import java.util.LinkedList;
import java.util.List;

/**
 * 
 * Corresponding quiz request for the robot
 *
 */
public class QuizRequest {
	public List<Object> quiz;

	public QuizRequest() {
		super();

		quiz = new LinkedList<>();
	}
}
