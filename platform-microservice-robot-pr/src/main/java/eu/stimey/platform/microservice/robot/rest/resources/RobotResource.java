package eu.stimey.platform.microservice.robot.rest.resources;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;
import eu.stimey.platform.library.rest.filter.DefaultAuthenticationFilter;
import eu.stimey.platform.library.robot.communication.RobotCommunication;
import eu.stimey.platform.library.robot.databind.v2.RobotActivities;
import eu.stimey.platform.library.robot.databind.v2.RobotActivity;
import eu.stimey.platform.library.robot.startup.RobotAMQPService;
import eu.stimey.platform.library.utils.StimeyUtils;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.library.utils.security.JsonWebToken;
import eu.stimey.platform.microservice.robot.beans.RobotService;
import eu.stimey.platform.microservice.robot.databind.QuizRequest;
import eu.stimey.platform.microservice.robot.startup.Global;

/**
 * 
 * Robot resource
 *
 */
@Path("/robot")
public class RobotResource {
	@Inject
	UserSharedService userSharedService;
	@Inject
	RobotService robotService;

	@Inject
	RobotAMQPService robotAMQPService;

	@POST
	@Path("quiz")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response quiz(@Context ContainerRequestContext requestContext, QuizRequest quizRequest) {
		String userid = (String) requestContext.getProperty("userid");
		if (userid != null && quizRequest != null) {
			UserSharedCacheObject userSharedCacheObject = userSharedService.getUser(userid);
			RobotCommunication<String, String> robotCommunication = new RobotCommunication<>(
					robotAMQPService.getRabbitmqConnection(), String.class);
			System.out.println("command sended to: /Commands/"
					+ userSharedCacheObject.robotchannel.replace("robot-", "").replace("-", ""));
			if (robotCommunication.publish(
					"Commands"
							+ userSharedCacheObject.robotchannel.replace("robot-", "").replace("-", "")/* WORKAROUND */,
					"[ShowQuiz]" + quizRequest.quiz + "[/ShowQuiz]"))
				return Response.status(202).build();

			else
				return Response.status(500).entity("{\"error\":\"publish command failed\"}").build();
		} else
			return Response.status(500).entity("{\"error\":\"author not found\"}").build();
	}

	@GET
	@Path("activities/short")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getActivitiesShort(@Context ContainerRequestContext requestContext) {
		String userid = (String) requestContext.getProperty("userid");

		RobotActivities robotActivities = robotService.get(userid);
		if (robotActivities != null) {
			ObjectMapper mapper = new ObjectMapper();
			ArrayNode arrayNode = mapper.createArrayNode();
			for (RobotActivity robotActivity : robotActivities.activities) {
				ObjectNode node = mapper.createObjectNode();
				node.put(robotActivity.name, robotActivity.id);
				arrayNode.add(node);
			}

			return Response.ok().entity(arrayNode.toString()).build();
		} else
			return Response.status(404).build();
	}

	@POST
	@Path("{activityid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response executeRobotActivityByUser(@Context ContainerRequestContext requestContext,
			@PathParam("activityid") String activityid) {
		String userid = (String) requestContext.getProperty("userid");
		String usertype = (String) requestContext.getProperty("usertype");

		if (activityid != null) {
			if (userid != null) {
				RobotActivities robotActivities = robotService.get(userid);
				RobotActivity activity = null;
				if (robotActivities != null)
					activity = robotService.find(activityid, robotActivities);
				if (activity == null)
					return Response.status(404).build();

				UserSharedCacheObject userSharedCacheObject = userSharedService.getUser(userid);
				RobotCommunication<String, String> robotCommunication = new RobotCommunication<>(
						robotAMQPService.getRabbitmqConnection(), String.class);
				String command = "Commands" /*+ usertype*/
						+ userSharedCacheObject.robotchannel.replace("robot-", "").replace("-", "")/* WORKAROUND */;
				System.out.println("command sended to: " + command);
				if (robotCommunication.publish(command, activity.getActivityAsString()))
					return Response.status(202).entity("{\"success\":\"" + command + "\"}").build();

				else
					return Response.status(500).entity("{\"error\":\"publish command failed\"}").build();
			} else
				return Response.status(500).entity("{\"error\":\"userid not found\"}").build();

		} else
			return Response.status(400).build();
	}

	@POST
	@Path("{activityid}/{missionid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response executeRobotActivity(@Context ContainerRequestContext requestContext,
			@PathParam("activityid") String activityid, @PathParam("missionid") String missionid) {
		String userid = (String) requestContext.getProperty("userid");

		if (activityid != null) {
			Cookie cookie = JsonWebToken.createCookie("", "", "admin", DefaultAuthenticationFilter.getPrivateKey(),
					Global.DOMAIN);
			Client client = StimeyClient.create();
			Response response = StimeyClient.get(client, "courses", Global.DOCKER,
					"api/missions/" + missionid + "/author", cookie);
			if (response.getStatus() == 200) {
				try {
					String authorid = ((Map<String, String>) new ObjectMapper()
							.readValue(response.readEntity(String.class), Map.class)).get("authorid");
					if (authorid != null) {
						RobotActivities robotActivities = robotService.get(authorid);
						RobotActivity activity = null;
						if (robotActivities != null)
							activity = robotService.find(activityid, robotActivities);
						if (activity == null)
							return Response.status(404).build();

						UserSharedCacheObject userSharedCacheObject = userSharedService.getUser(userid);
						RobotCommunication<String, String> robotCommunication = new RobotCommunication<>(
								robotAMQPService.getRabbitmqConnection(), String.class);
						System.out.println("command sended to: /Commands/"
								+ userSharedCacheObject.robotchannel.replace("robot-", "").replace("-", ""));
						if (robotCommunication.publish("Commands" + userSharedCacheObject.robotchannel
								.replace("robot-", "").replace("-", "")/* WORKAROUND */,
								activity.getActivityAsString()))
							return Response.status(202).build();

						else
							return Response.status(500).entity("{\"error\":\"publish command failed\"}").build();
					} else
						return Response.status(500).entity("{\"error\":\"author not found\"}").build();

				} catch (IOException e) {
					e.printStackTrace();
					return Response.status(500).entity("{\"error\":\"mapping failed\"}").build();
				} finally {
					client.close();
				}
			} else
				return Response.status(500).entity("{\"error\":\"api call to courses failed\"}").build();
		} else
			return Response.status(400).build();
	}

	@GET
	@Path("activities")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getActivities(@Context ContainerRequestContext requestContext) {
		String userid = (String) requestContext.getProperty("userid");

		RobotActivities robotActivities = robotService.get(userid);
		if (robotActivities != null)
			return Response.ok().entity(robotActivities).build();
		else
			return Response.status(404).build();
	}

	@POST
	@Path("activities")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postActivities(@Context ContainerRequestContext requestContext, RobotActivities robotActivities) {
		String userid = (String) requestContext.getProperty("userid");
		if (robotActivities != null) {
			robotService.set(userid, robotActivities);
			return Response.ok().build();
		} else
			return Response.status(400).build();
	}

	@POST
	@Path("activity")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response postActivity(@Context ContainerRequestContext requestContext, RobotActivity robotActivity) {
		String userid = (String) requestContext.getProperty("userid");
		if (robotActivity != null) {
			// robotActivity.actions.get(0).value1 =
			// robotActivity.actions.get(0).value1.replaceFirst("<p>",
			// "").replaceFirst("</p>", "");
			robotActivity.actions.get(0).value1 = StimeyUtils.html2text(robotActivity.actions.get(0).value1);
			// System.out.println(robotActivity.actions.get(0).value1);
			String activityid = robotService.getAndSet(userid, robotActivity);
			LinkedHashMap<String, Object> map = new LinkedHashMap<>();
			map.put("activityid", activityid);
			return Response.ok().entity(map).build();
		} else
			return Response.status(400).build();
	}

	@DELETE
	@Path("activities/{activityid}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteActivity(@Context ContainerRequestContext requestContext,
			@PathParam("activityid") String activityid) {
		String userid = (String) requestContext.getProperty("userid");
		if (activityid != null) {
			robotService.delete(userid, activityid);
			return Response.ok().build();
		} else
			return Response.status(400).build();
	}
}
