package eu.stimey.platform.microservice.robot.beans;

import java.util.Comparator;
import java.util.UUID;

import org.apache.commons.lang3.mutable.MutableObject;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.library.robot.databind.v2.RobotAction;
import eu.stimey.platform.library.robot.databind.v2.RobotActivities;
import eu.stimey.platform.library.robot.databind.v2.RobotActivity;

/**
 * 
 * Service for robot
 *
 */
public class RobotService {
	protected Caching<String, RobotActivities> robotCache;

	protected static final RobotActivities ROBOT_ACTIVITIES;

	static {
		ROBOT_ACTIVITIES = new RobotActivities();

		ROBOT_ACTIVITIES.activities.add(new RobotActivity("Welcome (Predefined)")
				.add(new RobotAction("MovementPredefined", "Welcome"))
				.add(new RobotAction("Light", "1", "5000", "Green"))
				.add(new RobotAction("ASR", "Welcome to this mission. It’ s time to work!"))
				.add(new RobotAction("Received", "ok")).add(new RobotAction("TTS", "Let’s do this!"))
				.add(new RobotAction("RobotFace", "Blink Left Eye")).setId("900470b8-6627-453e-8c4d-52a57a10ba23"));
		ROBOT_ACTIVITIES.activities.add(new RobotActivity("Thank you (Predefined)")
				.add(new RobotAction("MovementPredefined", "Yes")).add(new RobotAction("Light", "1", "5000", "Yellow"))
				.add(new RobotAction("ASR", "Thank you very much!")).add(new RobotAction("Received", "You are welcome"))
				.add(new RobotAction("RobotFace", "Laughter")).setId("123e21c4-26cc-4df6-934c-0c0dd37bfa81"));
		ROBOT_ACTIVITIES.activities.add(new RobotActivity("You are welcome (Predefined)")
				.add(new RobotAction("MovementPredefined", "Rise Left Arm"))
				.add(new RobotAction("Light", "1", "5000", "Olive")).add(new RobotAction("ASR", "You are welcome!"))
				.setId("298d2156-3739-4568-9f77-ba05babb2842"));
		ROBOT_ACTIVITIES.activities.add(new RobotActivity("Positive feedback 1 (Predefined)")
				.add(new RobotAction("MovementPredefined", "Dub")).add(new RobotAction("Light", "1", "5000", "Red"))
				.add(new RobotAction("ASR", "You did great!")).add(new RobotAction("Received", "Thank you"))
				.add(new RobotAction("TTS", "It’s my pleasure, because you make me proud!"))
				.add(new RobotAction("RobotFace", "Laughter")).setId("c0cddb3b-177f-474e-b36c-986315f64bc3"));
		ROBOT_ACTIVITIES.activities.add(new RobotActivity("Positive feedback 2 (Predefined)")
				.add(new RobotAction("MovementPredefined", "Well done"))
				.add(new RobotAction("Light", "1", "5000", "Pink")).add(new RobotAction("ASR", "Well done my friend!"))
				.add(new RobotAction("Received", "Thank you"))
				.add(new RobotAction("TTS", "I am so happy about your progress"))
				.add(new RobotAction("RobotFace", "Blink Left Eye")).setId("1567e754-585f-4e80-89fb-71fc76fb03f8"));
		ROBOT_ACTIVITIES.activities.add(new RobotActivity("Reinforcement (Predefined)")
				.add(new RobotAction("MovementPredefined", "Rise Left Arm"))
				.add(new RobotAction("Light", "1", "5000", "Orange"))
				.add(new RobotAction("ASR", "Come on, I know you can do better!"))
				.add(new RobotAction("Received", "I will try")).add(new RobotAction("TTS", "Of course you will!"))
				.add(new RobotAction("Received", "I know")).add(new RobotAction("TTS", "Then prove it!"))
				.add(new RobotAction("RobotFace", "Blink")).setId("b8ba456e-6d5c-4739-a828-c3d237c9c43d"));
		ROBOT_ACTIVITIES.activities
				.add(new RobotActivity("Surprise (Predefined)").add(new RobotAction("MovementPredefined", "Surprise"))
						.add(new RobotAction("Light", "1", "5000", "Light Sky Blue"))
						.add(new RobotAction("ASR", "I am surprised!")).add(new RobotAction("Received", "Yes"))
						.add(new RobotAction("TTS", "I trust you!")).add(new RobotAction("Received", "No"))
						.add(new RobotAction("TTS", "Would you ever say lies to me?"))
						.add(new RobotAction("RobotFace", "Surprise")).setId("7f873058-42ea-432e-a8d6-7576f2e4edc7"));
		ROBOT_ACTIVITIES.activities.add(new RobotActivity("Goodbye (Predefined)")
				.add(new RobotAction("MovementPredefined", "Bye")).add(new RobotAction("Light", "1", "5000", "Silver"))
				.add(new RobotAction("ASR", "Goodbye my friend")).add(new RobotAction("Received", "bye"))
				.add(new RobotAction("TTS", "I am so sorry to see you leave"))
				.add(new RobotAction("Received", "goodbye"))
				.add(new RobotAction("TTS", "I am so sorry to see you leave")).add(new RobotAction("RobotFace", "Sad"))
				.setId("d7680793-a348-4c30-9119-79df076315d6"));
		ROBOT_ACTIVITIES.activities.add(new RobotActivity("Thinking (Predefined)")
				.add(new RobotAction("MovementPredefined", "Thinking"))
				.add(new RobotAction("Light", "1", "5000", "Purple"))
				.add(new RobotAction("ASR", "Why don’t you think a bit more?")).add(new RobotAction("Received", "Yes"))
				.add(new RobotAction("TTS", "Great, I can help you")).add(new RobotAction("Received", "No"))
				.add(new RobotAction("TTS", "Ask me to help you, I can help you!"))
				.add(new RobotAction("RobotFace", "Thinking")).setId("9aad937d-814d-4fc9-b207-682892e2c5d8"));
		ROBOT_ACTIVITIES.activities.add(new RobotActivity("Angry (Predefined)")
				.add(new RobotAction("MovementPredefined", "Enough"))
				.add(new RobotAction("Light", "1", "5000", "Burnt Orange"))
				.add(new RobotAction("ASR", "You make me fill so angry")).add(new RobotAction("Received", "why"))
				.add(new RobotAction("TTS", "Because you don’t pay attention")).add(new RobotAction("Received", "ok"))
				.add(new RobotAction("TTS", "please concentrate")).add(new RobotAction("RobotFace", "Angry"))
				.setId("150ebe51-c6d5-45d0-bc89-5db105e3f7c6"));
	}

	public RobotService() {
		super();

		robotCache = new Caching<>("ms_robot_activities", "userid", RobotActivities.class, "activities",
				GlobalVariables.CACHE_CONFIG);
		robotCache.clear();
	}

	public RobotActivities get(String userid) {
		RobotActivities result = robotCache.get(userid);
		if (result == null) {
			result = new RobotActivities(userid);
			robotCache.set(userid, result);
		}

		result.activities.addAll(ROBOT_ACTIVITIES.activities);
		result.activities.sort(new Comparator<RobotActivity>() {
			@Override
			public int compare(RobotActivity ra1, RobotActivity ra2) {
				if (ra1.name == null || ra2.name == null)
					return 0;
				else
					return ra1.name.compareTo(ra2.name);
			}
		});

		return result;
	}

	public void set(String userid, RobotActivities robotActivities) {
		robotActivities.userid = userid;
		robotActivities.activities.removeAll(ROBOT_ACTIVITIES.activities);

		robotCache.set(userid, robotActivities);
	}

	public String getAndSet(String userid, RobotActivity robotActivity) {
		MutableObject<String> result = new MutableObject<>(robotActivity.id);

		robotCache.getAndSet(userid, (robotActivities) -> {
			if (robotActivity.id == null || (robotActivity.id != null && robotActivity.id.isEmpty())) {
				robotActivity.id = UUID.randomUUID().toString();
				robotActivities.activities.add(robotActivity);
				result.setValue(robotActivity.id);
			} else {
				RobotActivity oldRobotActivity = find(robotActivity.id, robotActivities);
				if (oldRobotActivity != null && oldRobotActivity.actions.size() > 0) {
					oldRobotActivity.name = robotActivity.name;
					oldRobotActivity.actions.get(0).value1 = robotActivity.actions.get(0).value1;
				}
			}

			return robotActivities;
		});

		return result.getValue();
	}

	public void delete(String userid, String activityid) {
		robotCache.getAndSet(userid, (robotActivities) -> {
			RobotActivity robotActivity = find(activityid, robotActivities);
			robotActivities.activities.remove(robotActivity);
			return robotActivities;
		});
	}

	public RobotActivity find(String activityid, RobotActivities robotActivities) {
		RobotActivity result = null;

		for (RobotActivity robotActivity : robotActivities.activities) {
			if (robotActivity.id.equals(activityid)) {
				result = robotActivity;
				break;
			}
		}

		return result;
	}
}
