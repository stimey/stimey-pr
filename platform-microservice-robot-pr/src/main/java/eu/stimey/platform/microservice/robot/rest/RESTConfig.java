package eu.stimey.platform.microservice.robot.rest;

import static eu.stimey.platform.library.utils.StimeyLogger.*;

import javax.annotation.PreDestroy;
import javax.ws.rs.ApplicationPath;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.robot.startup.RobotAMQPService;
import eu.stimey.platform.microservice.robot.beans.RobotService;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

/**
 * REST configuration
 *
 */
@ApplicationPath("api")
public class RESTConfig extends ResourceConfig {
	protected final MongoDBService mongoDBService;
	protected final RedisAMQPService redisAMQPService;
	protected final RobotAMQPService robotAMQPService;

	public RESTConfig() {
		super();

		mongoDBService = MongoDBService.getService();
		redisAMQPService = RedisAMQPService.getService();
		robotAMQPService = RobotAMQPService.getService();

		logger().info("REST-Service started...");

		packages("eu.stimey.platform.microservice.robot.rest");

		register(new AbstractBinder() {
			protected void configure() {
				UserSharedService userSharedService = new UserSharedService();
				bind(mongoDBService).to(MongoDBService.class);
				bind(redisAMQPService).to(RedisAMQPService.class);
				bind(robotAMQPService).to(RobotAMQPService.class);
				bind(userSharedService).to(UserSharedService.class);
				bind(new RobotService()).to(RobotService.class);
			}
		});

		register(new JacksonJsonProvider().configure(SerializationFeature.INDENT_OUTPUT, true));
	}

	@PreDestroy
	public void shutdown() {
		logger().info("REST-Service stopped...");
	}
}
