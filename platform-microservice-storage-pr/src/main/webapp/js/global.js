var storage_global_docker = false;
var storage_global_production = false;

var storage_global_settings = {
  "DEFAULT": {
    "API_GATEWAY_STORAGE": "http://localhost:8080/platform-microservice-api-gateway-storage",
    "FILES": "http://localhost:8080/platform-microservice-files",
    "API_GATEWAY": "http://localhost:8080/platform-microservice-api-gateway",
    "FAAS": "http://localhost:8090",
    "FETCH_UNREAD_MESSAGES": true,
    "FETCH_NOTIFICATIONS_AND_ANNOUNCEMENTS": true

  },
  "DOCKER": {
    "API_GATEWAY_STORAGE": "/api-gateway",
    "FILES": "/files",
    "API_GATEWAY": "",
    "FAAS": ""
  }
}


function storage_getURL(key) {

  if (storage_global_docker)
    return storage_global_settings['DOCKER'][key];
  else
    return storage_global_settings['DEFAULT'][key];
}

function fetch_unread_messages() {
  return storage_global_settings['DEFAULT']['FETCH_UNREAD_MESSAGES'];
}

function fetch_notifications_and_announcements() {
  return storage_global_settings['DEFAULT']['FETCH_NOTIFICATIONS_AND_ANNOUNCEMENTS'];
}

function getMissionApiUrl(key) {
  let result = "";
  if (!storage_global_production && key === "API_GATEWAY_STORAGE") {
    result = "http://localhost:8080/platform-microservice-courses/api";
  } else {
    result = storage_getURL(key) + "/api-proxy/courses/api";
  }
  return result;
}


function storage_getdashboardApiUrl(key) {
  let result = "";
  if (!storage_global_production && key === "API_GATEWAY_STORAGE")
    result = "http://localhost:8080/platform-microservice-dashboard/api";
  else
    result = storage_getURL(key) + "/api-proxy/dashboard/api";

  return result;
}

function storage_getAuthApiUrl(key) {
  let result = "";

  if (!storage_global_production && key === "API_GATEWAY_STORAGE")
    result = "http://localhost:8080/platform-microservice-auth/api";
  else
    result = storage_getURL(key) + "/api-proxy/auth/api";

  return result;
}

function getActivityStreamApiUrl(key) {
  let result = "";
  if (!storage_global_production && key === "API_GATEWAY_STORAGE")
    result = "http://localhost:8080/platform-microservice-activitystream/api";
  else
    result = storage_getURL(key) + "/api-proxy/activitystream/api";
  return result;
}


function getCommunityApiUrl(key) {
	  let result = "";
	  if (!storage_global_production && key === "API_GATEWAY_STORAGE")
		  result = "http://localhost:8080/platform-microservice-communities/api";
	  else
		  result = storage_getURL(key) + "/api-proxy/communities/api"
	  return result;
	}

