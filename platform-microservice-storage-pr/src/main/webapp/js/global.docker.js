var storage_global_docker = true;
var storage_global_production = true;

var storage_global_settings = {
  "DEFAULT": {
    "API_GATEWAY_STORAGE": "http://localhost:8080/platform-microservice-api-gateway-storage",
    "FILES": "http://localhost:8080/platform-microservice-files",
    "API_GATEWAY": "http://localhost:8080/platform-microservice-api-gateway",
    "FAAS": "http://localhost:8090"
  },
  "DOCKER": {
    "API_GATEWAY_STORAGE": "/api-gateway",
    "FILES": "/files",
    "API_GATEWAY": "",
    "FAAS": ""
  }
}


function storage_getURL(key) {

  if (storage_global_docker)
    return storage_global_settings['DOCKER'][key];
  else
    return storage_global_settings['DEFAULT'][key];
}


function getMissionApiUrl(key) {
  var result = "";
  if (!storage_global_production && key === "API_GATEWAY_STORAGE") {
    result = "http://localhost:8080/platform-microservice-courses/api";
  } else {
    result = storage_getURL(key) + "/api-proxy/courses/api";
  }
  return result;
}


function storage_getdashboardApiUrl(key) {
  var result = "";
  if (!storage_global_production && key === "API_GATEWAY_STORAGE")
    result = "http://localhost:8080/platform-microservice-dashboard/api";
  else
    result = storage_getURL(key) + "/api-proxy/dashboard/api";

  return result;
}

function getActivityStreamApiUrl(key) {
  let result = "";
  if (!storage_global_production && key === "API_GATEWAY_STORAGE")
    result = "http://localhost:8080/platform-microservice-activitystream/api";
  else
    result = storage_getURL(key) + "/api-proxy/activitystream/api";
  return result;
}

function getCommunityApiUrl(key) {
	  let result = "";
	  if (!storage_global_production && key === "API_GATEWAY_STORAGE")
		  result = "http://localhost:8080/platform-microservice-communities/api";
	  else
		  result = storage_getURL(key) + "/api-proxy/communities/api"
	  return result;
	}

