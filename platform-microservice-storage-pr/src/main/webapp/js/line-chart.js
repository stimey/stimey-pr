
    jQuery(function ($) {
        $("#chart").shieldChart({
            theme: "bootstrap",
            exportOptions: {
                image: false,
                print: false
            },
            seriesSettings: {
                bar: {
                    stackMode: "normal"
                }
            },
            axisX: {
                categoricalValues: [
                    "1952", "1956", "1960", "1964", "1968", "1972", "1976", "1980",
                    "1984", "1988", "1992", "1996", "2000", "2004", "2008", "2012"
                ]
            },
            primaryHeader: {
                text: "Olympic Medals won by USA"
            },
            dataSeries: [{
                seriesType: "bar",
                collectionAlias: "Gold Medals",
                data: [40, 32, 34, 36, 45, 33, 34, 0, 83, 36, 37, 44, 37, 35, 36, 46]
            }, {
                seriesType: "bar",
                collectionAlias: "Silver Medals",
                data: [19, 25, 21, 26, 28, 31, 35, 0, 60, 31, 34, 32, 24, 40, 38, 29]
            }, {
                seriesType: "bar",
                collectionAlias: "Bronze Medals",
                data: [17, 17, 16, 28, 34, 30, 25, 0, 30, 27, 37, 25, 33, 26, 36, 29]
            }]
        });
    });
