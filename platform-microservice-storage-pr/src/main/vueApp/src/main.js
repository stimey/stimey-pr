import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';

import App from './App.vue';
import {storage_getURL} from '../js/global.js';

const Home = () => import(/*webpackChunkName: "home"*/'./components/Home.vue');
const Login = () => import(/*webpackChunkName: "login"*/'./components/Login.vue');
const Signup = () => import(/*webpackChunkName: "signup"*/'./components/Signup.vue');
const PasswordReset = () => import(/*webpackChunkName: "passwordReset"*/ './components/auth/PasswordReset.vue');
const NotFound = () => import(/*webpackChunkName: "notFound"*/ './components/NotFound.vue');
const ActivityStream = () => import(/*webpackChunkName: "activityStream"*/ './components/ActivityStream.vue');
const ActivityStreamActivities = () => import(/*webpackChunkName: "activityStreamActivities"*/ './components/ActivityStreamActivities.vue');
const StatusUpdate = () => import(/*webpackChunkName: "statusupdate"*/'./components/StatusUpdate.vue');
const TermsAndConditions = () => import(/*webpackChunkName: "termsAndConditions"*/'./components/TermsAndCondition.vue');
const PrivacyPolicy = () => import(/*webpackChunkName: "privacyPolicy"*/'./components/PrivacyPolicy.vue');
const Imprint = () => import(/*webpackChunkName: "imprint"*/'./components/Imprint.vue');
const Disclaimer = () => import(/*webpackChunkName: "disclaimer"*/'./components/Disclaimer.vue');

const Achievement = () => import(/*webpackChunkName: "achievement"*/ './components/Achievement.vue');
const CommunityOverview = () => import(/*webpackChunkName: "communityOverview"*/'./components/CommunityOverview.vue');
const Community = () => import(/*webpackChunkName: "community"*/'./components/Community.vue');

const Profiles = () => import(/*webpackChunkName: "profiles"*/'./components/profile/Profiles.vue');
const Wall = () => import(/*webpackChunkName: "wall"*/'./components/profile/Profiles_Wall.vue');
const Bookmarks = () => import(/*webpackChunkName: "bookmarks"*/'./components/profile/Profiles_Bookmark.vue');
const Network = () => import(/*webpackChunkName: "network"*/'./components/profile/Profiles_Network.vue');
const About = () => import(/*webpackChunkName: "about"*/'./components/profile/Profiles_About.vue');
const Showcase = () => import(/*webpackChunkName: "showcase"*/'./components/profile/Profiles_Showcase.vue');

const EPortfolio = () => import(/*webpackChunkName: "eportfolio"*/'./components/eportfolio/EPortfolio.vue');
const LearningJournalContent = () => import(/*webpackChunkName: "LearningJournalContent"*/'./components/eportfolio/LearningJournalContent.vue');
const Missions = () => import(/*webpackChunkName: "missions"*/'./components/mission/view/Missions.vue');
const MissionOverview = () => import(/*webpackChunkName: "missionOverview"*/'./components/mission/view/MissionOverview.vue');
const MissionViewContent = () => import(/*webpackChunkName: "missionViewContent"*/'./components/mission/view/MissionViewContent.vue');
const BonusTask = () => import(/*webpackChunkName: "bonusTask"*/'./components/mission/view/BonusTask.vue');
const EditMissionOverview = () => import(/*webpackChunkName: "editMissionOverview"*/'./components/mission/editor/MissionEditing.vue');
const EditMissionAfterPublish = () => import(/*webpackChunkName: "editMissionAfterPublish"*/'./components/mission/editor/EditMissionAfterPublish.vue');
const EditLevel = () => import(/*webpackChunkName: "editLevel"*/'./components/mission/editor/EditLevel.vue');
const EditTask = () => import(/*webpackChunkName: "editTask"*/'./components/mission/editor/EditTask.vue');
const EditAssignment = () => import(/*webpackChunkName: "editAssignment"*/'./components/mission/editor/EditAssignment.vue');

const EditChallenge = () => import(/*webpackChunkName: "editChallenge"*/'./components/mission/editor/EditChallenge.vue');
const Planets = () => import(/*webpackChunkName: "planets"*/'./components/Planets.vue');
const Lab = () => import(/*webpackChunkName: "lab"*/'./components/Lab.vue');
const Badges = () => import(/*webpackChunkName: "badges"*/'./components/Badges.vue');
const Chat = () => import(/*webpackChunkName: "chat"*/'./components/Chat.vue');
const Chatrequest = () => import(/*webpackChunkName: "chatrequest"*/'./components/Chatrequest.vue');
const Robot = () => import(/*webpackChunkName: "robot"*/'./components/Robot.vue');
const Worlds = () => import(/*webpackChunkName: "worlds"*/'./components/worlds/view/WorldsOverview.vue');
const WorldEdit = () => import(/*webpackChunkName: "worldEdit"*/'./components/worlds/view/WorldEdit.vue');
const WorldView = () => import(/*webpackChunkName: "worldView"*/'./components/worlds/view/WorldView.vue');

const Radio = () => import(/*webpackChunkName: "radio"*/'./components/Radio.vue');
const RadioLogin = () => import(/*webpackChunkName: "radioLogin"*/'./components/radio/RadioLogin.vue');
const RadioBackend = () => import(/*webpackChunkName: "radioBackend"*/'./components/radio/RadioBackend.vue');
const Notifications = () => import(/*webpackChunkName: "notifications"*/'./components/Notifications.vue');

const CreativityTest = () => import('./components/CreativityTest.vue');
const RocketScientist = () => import('./components/RocketScientist.vue');
const MentalCalculator = () => import('./components/MentalCalculator.vue');

//const NotFoundComponent = () => import(/*webpackChunkName: *notFoundComponent**/'./components/NotFoundComponent.vue');

import i18next from 'i18next';
import XHR from 'i18next-xhr-backend';
import LngDetector from 'i18next-browser-languagedetector';
import VueI18Next from '@panter/vue-i18next';
import {use_mock_data} from '../js/global.js';

Vue.use(VueI18Next);
Vue.use(VueRouter);
Vue.use(Vuex);

//sweetalert
import VueSweetalert2 from 'vue-sweetalert2';

Vue.use(VueSweetalert2);
///

i18next
    .use(XHR)
    .use(LngDetector)
    .init({
        fallbackLng: 'en',
        debug: false,
        ns: ['translation'],
        defaultNS: 'translation',
        detection: {
            order: ['cookie'],
            lookupCookie: 'language',
            caches: ['cookie']
        },
        backend: {
            // loadPath: '/js/locales/{{lng}}/{{ns}}.json'
            loadPath: storage_getURL("API_GATEWAY_STORAGE")+'/api-proxy/config/api/v1/translation/{{lng}}'
        	// loadPath: '/api-gateway/api-proxy/config/api/v1/translation/{{lng}}'
        }
    }, function (err, t) {
        if (err)
            console.log(err);
    });

const i18n = new VueI18Next(i18next);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {path: '/', redirect: '/home'},
        {path: '/home', component: Home},
        {path: '/login', component: Login},
        {path: '/reset', component: PasswordReset},
        {path: '/signup/:usertype?', component: Signup, props: true},
        //{path: '/profiles/:username', component: Profiles},
        {
            path: '/profiles/:profileId', component: Profiles,
            children: [
                {
                    path: 'wall', alias: '/',
                    component: Wall,
                },
                {
                    path: 'bookmarks',
                    component: Bookmarks,
                },
                {
                    path: 'network',
                    component: Network,
                },
                {
                    path: 'about',
                    component: About,
                },
                {
                    path: 'showcase',
                    component: Showcase,
                },
            ],
            props: true
        },
        {path: '/eportfolio/:eportfolioid', component: EPortfolio, props:true},
        {path: '/eportfolio/learningJournalContent/:statusupdateid', component: LearningJournalContent},
        {path: '/missions', component: Missions},
        {path: '/missions/:missionId/overview', component: MissionOverview, props: true},
        {path: '/missions/:missionId/editOverview', component: EditMissionOverview, props: true},
        {path: '/missions/:missionId/editAfterPublish', component: EditMissionAfterPublish, props: true},
        {path: '/missions/:missionId/levels/:levelIndex/edit', component: EditLevel, props: true},
        {path: '/missions/:missionId/viewContent', component: MissionViewContent, props: true},
        {path: '/missions/:missionId/bonusTask', component: BonusTask, props: true},
        {path: '/missions/:missionId/learningObjects/tasks/:learningObjectId/edit', component: EditTask, props: true},
        {
            path: '/missions/:missionId/learningObjects/assignments/:learningObjectId/edit',
            component: EditAssignment,
            props: true
        },
        {
            path: '/missions/:missionId/learningObjects/challenges/:learningObjectId/edit',
            component: EditChallenge,
            props: true
        },
        {path: '/communities', component: CommunityOverview, props: true},
        {path: '/community/:id', component: Community, props: true},
        {path: '/achievements', component: Achievement},
        {path: '/planets', component: Planets},
        {path: '/worlds', component: Worlds},
        {path: '/worlds/edit/:id', component: WorldEdit, props: true},
        {path: '/worlds/view/:id', component: WorldView, props: true},
        {path: '/worlds/preview/:id', component: WorldView, props: true},
        {path: '/badges/:schoolId', component: Badges, props: true},
        {path: '/activitystream', component: ActivityStream},
        {path: '/activityStreamActivities', component: ActivityStreamActivities},
        {path: '/statusupdate/:statusupdateId', component: StatusUpdate, props: true},
        {path: '/termsAndConditions', component: TermsAndConditions},
        {path: '/privacyPolicy', component: PrivacyPolicy},
        {path: '/imprint', component: Imprint},
        {path: '/disclaimer', component: Disclaimer},

        {path: '/creativity', component: CreativityTest},
        {path: '/tools/:tool', component: RocketScientist},
        {path: '/mentalcalc', component: MentalCalculator},
        {path: '/notifications', component: Notifications},

        {path: '/chatrequest', component: Chatrequest},
        {path: '/chat', component: Chat},
        {path: '/lab', component: Lab},
        {path: '/robot', component: Robot},

        {path: '/radio', component: Radio},
        {path: '/radio/login', component: RadioLogin},
        {path: '/radio/admin', component: RadioBackend},

        {path: '*', name: '404', redirect: '/notfound'},
        {path: '/notfound', component: NotFound}
        //{ path: '/learnings', component: learning }
        //{path: '/test', component: Test},
    ],
    scrollBehavior(to, from, savedPosition) {
        if(to.fullPath.includes('profiles'))
            return false;
        return {x: 0, y: 0}
    }
});

var globalStore = {
    state: {
        currentUser: JSON.parse(localStorage.getItem("currentUser")),
        profile: JSON.parse(localStorage.getItem("profile")),
        friendList: JSON.parse(localStorage.getItem("friendList")),
        points: {
        	kp: 23,
        	cp: 24,
        	sp: 34
        }
    }
    
};

if (use_mock_data) {
    globalStore.state.currentUser = {
        username: "Archimedis",
        usertype: "teacher",
        userid: "5a0ee2427e9d6459996d3c0b"
    };
}

export const chatMessageBus = new Vue();

Vue.prototype.$dailyGoalBus = new Vue();

new Vue({
    el: '#app',
    i18n,
    router: router,
    store: globalStore.state,
    render: h => h(App)
});