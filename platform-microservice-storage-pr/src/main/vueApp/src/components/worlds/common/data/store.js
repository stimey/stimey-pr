export default {
    stages: [
        'Basic',
        'Primary',
        'Intermediate',
        'Advanced'
    ],
    difficulties: [
        'Easy',
        'Medium',
        'Difficult',
        'Challenge'
    ],
    topics: [
        'Mathematics',
        'Science',
        'Engineer',
        'Innovation',
        'Technology',
        'Entrepreneurship',
        'Physic',
        'Chemistry',
        'Biology',
        'Computer Science'
    ],
    languages: {
        en: 'English',
        de: 'Deutsch',
        es: 'Español',
        fi: 'Suomi',
        gr: 'Ελληνικά',
        ru: 'Русский'
    },
    themes: {
        forest: {
          name: 'Forest Theme',
        },
        factory: {
            name: 'Factory Theme'
        },
        crash: {
            name: 'Crash Theme'
        },
        ocean: {
            name: 'Ocean Theme'
        },
        young: {
            name: 'Young Theme'
        },
    }
};