import Vue from 'vue';

/** Copyright (c) 2018, Stimey */

/**
 * vue-autocomplete.js
 *
 * @author Michael Kopirski & Sascha M. Schumacher
 * @version 2018.04.25.0000
 */
export default Vue.component('v-autocomplete', {
    props: {
    	/** Name of the input */
    	name: {
    		type: String,
    		required: false,
    		default: 'autocomplete'
    	},
    	/** Placeholder for the input */
    	placeholder: {
    		type: String,
    		required: false,
    		default: 'Search...'
    	},
    	/** Disable the input */
    	disabled: {
    		type: Boolean,
    		required: false,
    		default: false
    	},
    	/** Minimum characters to start the autocomplete */
    	min: {
    		type: Number,
    		required: false,
    		default: 3
    	},
    	/** Source for the items to show as results */
    	source: {
    		type: [String, Array],
    		required: true,
    	},
    	/** Items will filtered in backend */
    	backend: {
    		type: Boolean,
    		required: false,
    		default: false
    	},
    	/** Activate for the use of v-model */
    	async: {
    		type: Boolean,
    		required: false,
    		default: false
    	}
    },
    template:
    	'<div class="form-control autocomplete">' +
    	'	<input ' +
    	'		 class="form-control autocomplete" :placeholder="placeholder" :disabled="disabled" type="text" v-model="search" '+
    	'		@input="onChange" @keyup.enter="onEnter" @keyup.esc="onEsc" @keyup.up="onUp" @keyup.down="onDown"' +
    	'	/>' +
    	'	<div id="autocomplete-results" class="autocomplete-items" role="listbox" v-show="isOpen">' +
    	'		<div class="loading" v-if="isLoading">Loading results...</div>' +
    	'		<div ' +
    	'			class="autocomplete-result" :class="{ \'is-active\': i === arrowCounter }" :key="i" @click="setResult(result)"'+
    	'			v-else v-for="(result, i) in results">' +
    	'			{{ result }}' +
    	'		</div>' +
    	'	</div>' +
    	'</div>',
    data() {
    	return {
    		response: new Data(),
    		items: new Form(),
    		isOpen: false,
    		isLoading: false,
    		arrowCounter: 0,
    		search: "",
    		results: []
    	};
    },
    methods: {
    	/** Will calls if the input changes */
    	onChange() {
            this.$emit('input', this.search);
            if (this.isAsync) {
              this.isLoading = true;
            } else {
              this.filterResults();
              this.isOpen = true;
            }
    	},
    	/** Requested and/or filtered the result */
    	filterResults() {
    		if (typeof this.source === 'string') {
	    		if (this.backend) {
	    			this.response.get(storage_getURL('API_GATEWAY_STORAGE') + this.source + this.search.toLowerCase().replace(' ','+'));
		    		this.results = this.response.data;
	    		} else {
	    			this.response.get(storage_getURL('API_GATEWAY_STORAGE') + this.source);
	    			this.items = (Object.assign([], this.response.data));
	    			this.results = this.items.filter(item => item.toLowerCase().indexOf(this.search.toLowerCase()) > -1);
	    		}
	    	} else {
	    		this.results = this.source.filter(item => item.toLowerCase().indexOf(this.search.toLowerCase()) > -1);
	    	}
    	},
    	/** Sets the result to */
    	setResult(result) {
            this.search = result;
            this.isOpen = false;
         },
         onDown() {
             if (this.arrowCounter < this.results.length -1) {
               this.arrowCounter = this.arrowCounter + 1;
             }
         },
         onUp() {
             if (this.arrowCounter > 0) {
               this.arrowCounter = this.arrowCounter - 1;
             }
         },
         onEnter() {
        	 if (this.isOpen) {
	        	 this.search = this.results[this.arrowCounter];
	             this.isOpen = false;
	             this.arrowCounter = -1;
        	 }
         },
         onEsc() {
        	if (this.isOpen) {
        		this.isOpen = false;
        	}
         },
         handleClickOutside(evt) {
             if (!this.$el.contains(evt.target)) {
               this.isOpen = false;
               this.arrowCounter = -1;
             }
         }
    },
    watch: {
    	items: function (value, oldValue) {
    		if (this.isAsync) {
    			this.results = value;
    	        this.isOpen = true;
    	        this.isLoading = false;
    	    }
    	}
    },
    mounted() {
      document.addEventListener('click', this.handleClickOutside);
    },
    destroyed() {
      document.removeEventListener('click', this.handleClickOutside);
    }
});
