import { storage_getURL, storage_getdashboardApiUrl } from '../../../../js/global';

/**
 * Function to follow another user.
 * @param userId: The userId of the user who wants to follower another user.
 * @param userIdOfUsertoFollow: The userId of the user to follow.
 * @returns {*|Q.Promise<any>} that contains the axios response object.
 */
export function followUser(userId, userIdOfUsertoFollow) {
  let config = {
    headers: {
      'Content-Type': 'text/plain'
    }
  };
  return axios.post(
    `${storage_getdashboardApiUrl('API_GATEWAY_STORAGE')}/profiles/${userId}/following`,
    userIdOfUsertoFollow,
    config
  );
}

/**
 * Function to unfollow another user.
 * @param userId: The userId of the user who wants to unfollow another user.
 * @param userIdOfUsertoUnFollow: The userId of the user to unfollow.
 * @returns {*|Q.Promise<any>}: The axios response object.
 */
export function unFollow(userId, userIdOfUsertoUnFollow) {
  return axios.delete(
    `${storage_getdashboardApiUrl('API_GATEWAY_STORAGE')}/profiles/${userId}/following/${userIdOfUsertoUnFollow}`
  );
}

/**
 * Make a friend request to another user.
 * @param userId: The id of the user who initiates the friend request.
 * @param userIdOfUserToBeFriendWith: The id of the user to acompanion.
 * @returns {*|Q.Promise<any>}: The axios response object.
 */
export function makeFriendRequest(userId, userIdOfUserToBeFriendWith) {
  let config = {
    headers: {
      'Content-Type': 'text/plain'
    }
  };
  return axios.post(
    `${storage_getdashboardApiUrl('API_GATEWAY_STORAGE')}/profiles/${userId}/makeFriendRequest`,
    userIdOfUserToBeFriendWith,
    config
  );
}

/**
 * Accept a friend request.
 * @param userId: The Id of the user who accepts a friend request
 * @param userIdOfFriendRequestToAccept: The Id of the user who has sent a friend request
 * @returns {*|Q.Promise<any>}: The axios response object
 */
export function acceptFriendRequest(userId, userIdOfFriendRequestToAccept) {
  return axios.post(
    `${storage_getdashboardApiUrl(
      'API_GATEWAY_STORAGE'
    )}/profiles/${userId}/friendRequests/${userIdOfFriendRequestToAccept}/accept`
  );
}

/**
 *
 * @param userId
 * @param userIdOfFriendRequestToDecline
 * @returns {*|Q.Promise<any>}
 */
export function declineFriendRequest(userId, userIdOfFriendRequestToDecline) {
  return axios.post(
    `${storage_getdashboardApiUrl(
      'API_GATEWAY_STORAGE'
    )}/profiles/${userId}/friendRequests/${userIdOfFriendRequestToDecline}/decline`
  );
}

/**
 *
 * @param userId
 * @param userIdOfUserToRemoveFromFriend
 * @returns {*}
 */
export function deleteFriendship(userId, userIdOfUserToRemoveFromFriend) {
  return axios.delete(
    `${storage_getdashboardApiUrl('API_GATEWAY_STORAGE')}/profiles/${userId}/friends/${userIdOfUserToRemoveFromFriend}`
  );
}

/**
 *
 * @param userId
 * @param userIdOfUserToBlock
 * @returns {*|Q.Promise<any>}
 */
export function blockUser(userId, userIdOfUserToBlock) {
  let config = {
    headers: {
      'Content-Type': 'text/plain'
    }
  };
  return axios.post(
    `${storage_getdashboardApiUrl('API_GATEWAY_STORAGE')}/profiles/${userId}/blockedUsers`,
    userIdOfUserToBlock,
    config
  );
}

/**
 *
 * @param userId
 * @param userIdOfUserToUnBlock
 * @returns {*}
 */
export function unBlock(userId, userIdOfUserToUnBlock) {
  return axios.delete(
    `${storage_getdashboardApiUrl('API_GATEWAY_STORAGE')}/profiles/${userId}/blockedUsers/${userIdOfUserToUnBlock}`
  );
}

/**
 * Checks if profileA and profileB are friends
 * @param profileA
 * @param profileB
 * @returns true if they are friends, else false
 */
export function areFriends(profileA, profileB) {
  if (!profileA.friends || !profileB.friends) return false;
  if (profileA.friends.includes(profileB.userid) && profileB.friends.includes(profileA.userid)) return true;
  return false;
}

/**
 * Checks if profileA follows profileB
 * @param profileA
 * @param profileB
 * @returns true if profileA follows profileB else false
 */
export function isFollowing(profileA, profileB) {
  if (!profileA.follower || !profileB.following || !profileA.following || !profileB.following) return false;
  if (profileA.following.includes(profileB.userid) && profileB.follower.includes(profileA.userid)) return true;
  return false;
}

/**
 * Checks if profileA has blocked profileB
 * @param profileA
 * @param profileB
 * @returns true if profileA has blocked profileB else false
 */
export function isBlocked(profileA, profileB) {
  if (!profileA.blockedUser) return false;
  return profileA.blockedUser.filter(blockedUser => blockedUser.userid === profileB.userid).length > 0;
}

/**
 *
 * @param profileA
 * @param profileB
 * @returns true if profileA has sent friendrequest to profileB else false
 */
export function isFriendRequestSent(profileA, profileB) {
  if (!profileA.friendRequestsOutgoing || !profileB.friendRequests) return false;
  if (profileA.friendRequestsOutgoing.includes(profileB.userid) && profileB.friendRequests.includes(profileA.userid))
    return true;
  return false;
}

/**
 * Send a message request to another user.
 * @param from: the userId of the user who initiates the message request
 * @param profileTo: the profile of the user who should receive the message request
 * @param message: the content of the message
 * @returns {*|Q.Promise<any>}: A promise containing the axios response object
 */
export function sendMessageRequest(from, profileTo, message) {
  let requestBody = {
    from: from,
    targetId: profileTo.userid,
    targetName: profileTo.username,
    message: message
  };
  return axios
    .post(`${storage_getURL('API_GATEWAY_STORAGE')}/api-proxy/chat/api/chat/send-message-request/`, requestBody)
    .then(() => {
      let requestBody = {
        targetId: profileTo.userid,
        accepted: false,
        delete: false
      };
      axios.post(`${storage_getdashboardApiUrl('API_GATEWAY_STORAGE')}/profiles/${from}/messageRequests`, requestBody);
    });
}

export function messageRequestAccepted(profileFrom, profileTo) {
  if (!profileFrom.messageRequests) return false;
  return profileFrom.messageRequests[profileTo.userid];
}

export function isMessageRequestPending(profileFrom, profileTo) {
  console.log(profileFrom);
  console.log(profileTo);
  if (!profileFrom.messageRequests) return false;
  let messageRequests = profileFrom.messageRequests;
  if (messageRequests[profileFrom.userid]) return false;
  if (!(profileTo.userid in profileFrom.messageRequests)) return false;
  let messageRequestStatus = messageRequests[profileTo.userid];
  return !messageRequestStatus;
}

export function makeConnectWithChildRequest(userIdParent, userIdChild) {
  let config = {
    headers: {
      'Content-Type': 'text/plain'
    }
  };
  return axios.post(
    `${storage_getdashboardApiUrl('API_GATEWAY_STORAGE')}/profiles/${userIdChild}/makeConnectWithChildRequest`,
    userIdParent,
    config
  );
}

export function acceptRequestFromParent(userIdOfChild, userIdOfParent) {
  return axios.post(
    `${storage_getdashboardApiUrl(
      'API_GATEWAY_STORAGE'
    )}/profiles/${userIdOfChild}/requestsFromParent/${userIdOfParent}/accept`
  );
}

export function isParentRequestSent(profileOfParent, profileOfChild) {
  console.log(profileOfChild);
  if (!profileOfChild.requestFromParent) return false;
  return profileOfChild.requestFromParent.includes(profileOfParent.userid);
}
