export const gamificationToast = function (obj, response) {
	
	if (response.data.type == 'success') {
    	obj.$swal({
    	    text: "+" + response.data.social,
    	    toast: true,
    	    position: 'bottom',
    	    showConfirmButton: false,
    	    background: 'rgb(170, 220, 100)',
    	    timer: 1500,
    	    imageUrl: require('../../../img/ICONS-Knowledge-Points-Blue.png'),
    	    imageWidth: 30,
    	    imageHeight: 30,
    	  });
    	
		setTimeout(function(){
			obj.$swal({
        	    text: "+" + response.data.creativity,
        	    toast: true,
        	    position: 'bottom',
        	    showConfirmButton: false,
        	    background: 'rgb(170, 220, 100)',
        	    timer: 1500,
        	    imageUrl: require('../../../img/ICONS-Social-Points-Yellow.png'),
        	    imageWidth: 30,
        	    imageHeight: 27,
        	  });
		}, 2000);
		
		setTimeout(function(){
			obj.$swal({
        	    text: "+" + response.data.knowledge,
        	    toast: true,
        	    position: 'bottom',
        	    showConfirmButton: false,
        	    background: 'rgb(170, 220, 100)',
        	    timer: 1500,
        	    imageUrl: require('../../../img/ICONS-Creativity-Points-Red.png'),
        	    imageWidth: 30,
        	    imageHeight: 34,
        	  });
		}, 4000);
	} else if (response.data.type == 'error') {
		obj.$swal({
    	    type: 'error',
    	    text: response.data.message,
    	    toast: true,
    	    position: 'bottom',
    	    showConfirmButton: false,
    	    timer: 1500,
    	  });
	} else if (response.data.type == 'info') {
		obj.$swal({
    	    type: 'info',
    	    text: response.data.message,
    	    toast: true,
    	    position: 'bottom',
    	    showConfirmButton: false,
    	    timer: 1500,
    	});
	} else {
		obj.$swal({
    	    type: 'info',
    	    title: response.data.title,
    	    text: response.data.message,
    	    showConfirmButton: false,
    	    timer: 1500,
    	});
		
	}

};
