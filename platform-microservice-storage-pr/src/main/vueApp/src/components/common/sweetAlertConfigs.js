export const confirmDeleteConfig = (titleText, confirmText, cancelText) => {
  return {
    /*
    confirmButtonClass: 'btn btn-success',
    cancelButtonClass: 'btn btn-danger',
    buttonsStyling: false,
    */
    title: titleText,
    type: 'warning',
    confirmButtonText: confirmText,
    showCancelButton: true,
    cancelButtonText: cancelText,
    reverseButtons: true
  };
};

export const successToast = text => {
  return {
    type: 'success',
    text: text,
    toast: true,
    position: 'bottom',
    showConfirmButton: false,
    timer: 1500
  };
};

export const verifySchoolAlert = (title, confirmButtonText) => {
  return {
    title: title,
    input: 'text',
    inputAttributes: {
      autocapitalize: 'off'
    },
    showCancelButton: true,
    confirmButtonText: confirmButtonText,
  };
};

export const sweetAlertMessageRequestConfig = (title, confirmButtonText) => {
  return {
    input: 'textarea',
    inputPlaceholder: title,
    showCancelButton: true,
    confirmButtonText: confirmButtonText
  };
};
