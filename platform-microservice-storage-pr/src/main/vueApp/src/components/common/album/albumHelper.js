import { storage_getURL } from '../../../../js/global';

export function splitArrayIntoOrderedChunks(array, amountOfChunks) {
    if (!array) return [];
    let chunks = [];
    for (let i = 0; i < amountOfChunks; i++) {
        let chunk = [];
        for (let j = i; j < array.length; j += amountOfChunks) {
            chunk.push(array[j]);
        }
        chunks.push(chunk);
    }
    return chunks;
}

export function getImageUrl(imageId) {
    return `${storage_getURL('FILES')}/api/files/${imageId}`;
}