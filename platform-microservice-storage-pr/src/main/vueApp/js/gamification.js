// Register
export const REGISTER = "1";
export const DAILY_LOGIN = "2";

// Courses
export const ENROL_COURSE = "3";
export const COMPLETE_LEARNING_OBJECT_COURSE = "4";
export const COMPLETE_25_PERCENT_COURSE = "5";
export const COMPLETE_50_PERCENT_COURSE = "6";
export const COMPLETE_75_PERCENT_COURSE = "7";
export const COMPLETE_100_PERCENT_COURSE = "8";
export const POST_QUESTION_COURSE_QA = "9";
export const COMMENT_POST_QA = "10";
export const COMMENT_ABOUT_COURSE_OR_TEACHER = "11";
export const COURSE_REVIEW = "12";

// Community
export const CREATE_COMMUNITY = "13";
export const JOIN_COMMUNITY = "14";
export const CREATE_COMMUNITY_ANNOUNCEMENT = "15";
export const COMMUNITY_PUBLISH_UPDATE_ONLY_TEXT = "16";
export const COMMUNITY_PUBLISH_UPDATE_IMAGE_OR_VIDEO = "17";
export const COMMUNITY_PUBLISH_UPDATE_ONLY_URL = "18";
export const COMMUNITY_COMMENT_UPDATE = "19";
export const COMMUNITY_LIKE_SHARE_UPDATE = "20";

// User Profile
export const USER_PROFILE_PUBLISH_UPDATE_ONLY_TEXT = "21";
export const USER_PROFILE_PUBLISH_UPDATE_IMAGE_OR_VIDEO = "22";
export const USER_PROFILE_PUBLISH_UPDATE_ONLY_URL = "23";
export const USER_PROFILE_COMMENT_UPDATE = "24";
export const USER_PROFILE_LIKE_SHARE_UPDATE = "25";
export const ADD_PROFILE_PHOTO_FIRST_TIME = "26";
export const ADD_COVER_PHOTO_FIRST_TIME = "27";

// Discussions
export const POST_DISCUSSION = "28";
export const DISCUSSION_COMMENT_POST = "29";

// Lab
export const COMPLETE_LEVEL_1_ACCURACY_OVER_60 = "30";
export const COMPLETE_LEVEL_2_ACCURACY_OVER_60 = "31";
export const COMPLETE_LEVEL_3_ACCURACY_OVER_60 = "32";
export const COMPLETE_LEVEL_4_ACCURACY_OVER_60 = "33";
export const COMPLETE_LEVEL_5_ACCURACY_OVER_60 = "34";
export const COMPLETE_LEVEL_6_ACCURACY_OVER_60 = "35";
export const LISTEN_STIMEY_RADIO = "36";

// Additional Points
export const SET_FIRST_DAILY_GOAL_VALUES_OVER_20 = "37";
export const COMPLETE_DAILY_GOAL = "38";

// Lab
export const START_MENTAL_CALCULATOR = "39";
export const START_PHYSICS_ENGINE = "40";
export const START_CHEMISTRY_ENGINE = "41";
export const START_SERIOUS_GAMES = "42";

// Mental Calculator
export const COMPLETE_TRAINING_LEVEL_1 = "43";
export const COMPLETE_TRAINING_LEVEL_2 = "44";
export const COMPLETE_TRAINING_LEVEL_3 = "45";
export const COMPLETE_TRAINING_LEVEL_4 = "46";
export const COMPLETE_TRAINING_LEVEL_5 = "47";
export const COMPLETE_TRAINING_LEVEL_6 = "48";

export const COMPLETE_FANTASY = "49";