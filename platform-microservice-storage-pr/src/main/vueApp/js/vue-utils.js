// https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
function generateUUID() {
  return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  )
}


// https://github.com/laracasts/Vue-Forms/blob/master/public/js/app.js
// License: MIT
// Updated by David A. Bauer
// Version: 2.0

class Errors {
    /**
     * Create a new Errors instance.
     */
    constructor() {
        this.errors = {};
    }


    /**
     * Determine if an errors exists for the given field.
     *
     * @param {string} field
     */
    has(field) {
        return this.errors.hasOwnProperty(field);
    }


    /**
     * Determine if we have any errors.
     */
    any() {
        return Object.keys(this.errors).length > 0;
    }


    /**
     * Retrieve the error message for a field.
     *
     * @param {string} field
     */
    get(field) {
        if (this.errors[field]) {
            return this.errors[field][0];
        }
    }


    /**
     * Record the new errors.
     *
     * @param {object} errors
     */
    record(errors) {
        this.errors = errors;
    }


    /**
     * Clear one or all error fields.
     *
     * @param {string|null} field
     */
    clear(field) {
        if (field) {
            delete this.errors[field];

            return;
        }

        this.errors = {};
    }
}


class Form {
    /**
     * Create a new Form instance.
     *
     * @param {object} data
     */
    constructor(id, data) { /*
        if (id==null)
        	this.id = generateUUID();
        else*/
        	this.id = id;
        
        this.set(data);
        this.errors = new Errors();
        
        this.resetFields = true;
        
    }
    
    getFromStorage() {
    	let json = localStorage.getItem(this.id);
    	if (json!=null) {
    		this.set(JSON.parse(json));
    		return this.data;
    	}
    	else {
    		return null;
    	}
    }
    
    setId(id) {
    	this.id = id;
    }
    
    set(data) {
    	this.originalData = data;
    	if (this.data==null)
    		this.data = {};
    	for (let field in data) {
            this.data[field] = data[field];
        }
        
    	if (this.id!=null && data!=null)
    		localStorage.setItem(this.id, JSON.stringify(data));
    }

    /**
     * Fetch all relevant data for the form.
     */
    fetch() {
        this.data = this.orignalData;

        return this.data;
    }


    /**
     * Reset the form fields.
     */
    reset() {
        if (this.resetFields) {
        	for (let field in this.originalData) {
                this.data[field] = '';
            }
        }

        this.errors.clear();
    }
    
    resetData() {
        for (let field in this.data) {
            this.data[field] = '';
        }
    }
    
    get(url, data) {
    	return new Promise((resolve, reject) => {
            axios.get(url, data)
                .then(response => {
                    this.onSuccess(response.data);
                    this.set(response.data);

                    resolve(response.data);
                })
                .catch(error => { 
                    this.onFail(error.response);
                    
                    if (error.response)
                    	reject(error.response.data);
                });
        });
    }
    
   load(url, data) {	
    	if (this.getFromStorage()==null) {
    		this.get(url, data);
    	}
    }
    
    clear() {
    	this.set({});
    	this.errors.clear();
    }

    /**
     * Send a POST request to the given URL.
     * .
     * @param {string} url
     */
    post(url) {
        return this.submit('post', url);
    }


    /**
     * Send a PUT request to the given URL.
     * .
     * @param {string} url
     */
    put(url) {
        return this.submit('put', url);
    }


    /**
     * Send a PATCH request to the given URL.
     * .
     * @param {string} url
     */
    patch(url) {
        return this.submit('patch', url);
    }


    /**
     * Send a DELETE request to the given URL.
     * .
     * @param {string} url
     */
    delete(url) {
        return this.submit('delete', url);
    }


    /**
     * Submit the form.
     *
     * @param {string} requestType
     * @param {string} url
     */
    submit(requestType, url) {
        return new Promise((resolve, reject) => {
            axios[requestType](url, this.data)
                .then(response => {
                    this.onSuccess(response.data);

                    resolve(response.data);
                })
                .catch(error => {
                    this.onFail(error.response.data);
                    
                    if (error.response)
                    	reject(error.response.data);
                });
        });
    }


    /**
     * Handle a successful form submission.
     *
     * @param {object} data
     */
    onSuccess() {
        this.reset();
    }


    /**
     * Handle a failed form submission.
     *
     * @param {object} errors
     */
    onFail(errors) {
        this.errors.record(errors);
    }
}

class Data extends Form {
	constructor(id, data) {
		super(id, data);

        this.resetFields = false;
    }
}



