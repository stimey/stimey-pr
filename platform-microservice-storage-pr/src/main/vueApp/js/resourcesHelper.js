


export function mapResourceIdToResources(resources) {
    let resourceIdToResourceMapping = {};
    for (let resource of resources) {
        resourceIdToResourceMapping[resource.resourceId] = resource;
    }
    return resourceIdToResourceMapping;
}

/**
 * We have resourceIdToResourceMapping. E.g. {id1 : {resource1}, id2 : {resource2}}, and
 * we have learningObjectTitleToResourceIdsMapping {'learningObject1' : [id1, id2]} and
 * what we want to have is: {'learningObject1':[{resource1}, {resource2}]}
 *
 * @param resourceIdToResourceMapping
 * @param learningObjectTitleToResourceIdsMapping
 */
export function mapLearningObjectTitleToResources(resourceIdToResourceMapping, learningObjectTitleToResourceIdsMapping) {
    let learningObjectTitleToResourceMapping = new Map();
    if (isObjectEmpty(learningObjectTitleToResourceIdsMapping) || isObjectEmpty(resourceIdToResourceMapping))
        return learningObjectTitleToResourceMapping;
    for (let key in learningObjectTitleToResourceIdsMapping) {
        let resourceIds = learningObjectTitleToResourceIdsMapping[key];
        let resources = [];
        for (let resourceId of resourceIds)
            resources.push(resourceIdToResourceMapping[resourceId]);
        if(resources.length > 0)
            learningObjectTitleToResourceMapping.set(key, resources);
    }
    console.log(learningObjectTitleToResourceMapping.values());
    return learningObjectTitleToResourceMapping;
}

/**
 * Converts array to enumerated object
 * e.g. [a, b, c] -> {0:a, 1:b, 2:c}
 * @param arr: the array to convert
 */
export function arrayToEnumeratedObject(arr) {
    let enumeratedObject = {};
    for (let i = 0; i < arr.length; i++) {
        enumeratedObject[i] = arr[i];
    }
    return enumeratedObject;
}


function isObjectEmpty(obj) {
    //if(obj ===)
    return Object.keys(obj).length === 0 && obj.constructor === Object
}