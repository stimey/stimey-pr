let storage_global_docker = true;
let storage_global_production = true;

export let use_mock_data = false;
export let user_type = {teacher: 'teacher', student: 'student'};

let storage_global_settings = {
    "DEFAULT": {
        "API_GATEWAY_STORAGE": "http://localhost:8080/platform-microservice-api-gateway-storage",
        "FILES": "http://localhost:8080/platform-microservice-files",
        "CDN": "http://localhost:8080/platform-microservice-cdn",
        "API_GATEWAY": "http://localhost:8080/platform-microservice-api-gateway",
        "FAAS": "http://localhost:8090",
        "STATIC": "http://localhost:8080/platform-microservice-static"
    },
    "DOCKER": {
        "API_GATEWAY_STORAGE": "/api-gateway",
        "FILES": "/files",
        "CDN": "/cdn",
        "API_GATEWAY": "",
        "FAAS": "",
        "STATIC": "/static"
    }
};

export function storage_getURL(key) {
    if (storage_global_docker)
        return storage_global_settings['DOCKER'][key];
    else
        return storage_global_settings['DEFAULT'][key];
}

export function getMissionApiUrl(key) {
    let result = "";
    if (!storage_global_production && key === "API_GATEWAY_STORAGE") {
        result = "http://localhost:8080/platform-microservice-courses/api";
    } else {
        result = storage_getURL(key) + "/api-proxy/courses/api";
    }
    return result;
}

export function getMissionsEndpoint(key) {
    return getMissionApiUrl(key) + "/missions";
}

export function getTeachersEndpoint(key) {
    return getMissionApiUrl(key) + "/teachers";
}

export function getStudentsEndpoint(key) {
    return getMissionApiUrl(key) + "/students";
}

export function getLearningObjectsEndpoint(key) {
    return getMissionApiUrl(key) + "/learningObjects";
}

export function getMissionContentsEndpoint(key) {
    return getMissionApiUrl(key) + "/missionContents";
}

export function getChallengeResultsEndpoint(key) {
    return getMissionApiUrl(key) + "/challengeResults";
}

export function getMissionResourcesEndpoint(key) {
    return getMissionApiUrl(key) + "/resources";
}

export function getMetadataEndpoint(key) {
    return getMissionApiUrl(key) + "/metadata";
}

export function getDiscussionsEndpoint(key) {
    return getMissionApiUrl(key) + "/discussions";
}

export function getKhanAcademyEndpoint(key) {
    return getMissionApiUrl(key) + "/khanAcademyVideos";
}

export function storage_getdashboardApiUrl(key) {
    var result = "";

    if (!storage_global_production && key === "API_GATEWAY_STORAGE")
        result = "http://localhost:8080/platform-microservice-dashboard/api";
    else
        result = storage_getURL(key) + "/api-proxy/dashboard/api";

    return result;
}

export function getLabApiUrl(key) {
    var result = "";
    if (!storage_global_production && key === "API_GATEWAY_STORAGE") {
        result = "http://localhost:8080/platform-microservice-lab/api";
    } else {
        result = storage_getURL(key) + "/api-proxy/lab/api"; //richtig???
    }
    return result;
}

export function getActivityStreamApiUrl(key) {
    var result = "";
    if (!storage_global_production && key === "API_GATEWAY_STORAGE")
        result = "http://localhost:8080/platform-microservice-activitystream/api";
    else
        result = storage_getURL(key) + "/api-proxy/activitystream/api";
    return result;
}

export function getResourceEndpoint(key) {
    return getLabApiUrl(key) + "/resource";
}

export function getStatusUpdateApiUrl(key) {
    var result = "";
    if (!storage_global_production && key === "API_GATEWAY_STORAGE") {
        result = storage_getURL(key) + "/api/statusupdate";
    } else {
        result = storage_getURL(key) + "/api/statusupdate";
    }
    return result;
}

export function getNotificationApiUrl(key) {
    let result = "";
    if (!storage_global_production && key === "API_GATEWAY_STORAGE") {
        result = "http://localhost:8080/platform-microservice-notification/api";
    } else {
        result = storage_getURL(key) + "/api-proxy/notification/api";
    }
    return result;
}

export function getPlanetsApiUrl(key) {
    if (!storage_global_production && key === "API_GATEWAY_STORAGE") {
        return "http://localhost:8080/platform-microservice-planets/api";
    } else {
        return storage_getURL(key) + "/api-proxy/planets/api";
    }
}