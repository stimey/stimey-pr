let path = require('path');
let webpack = require('webpack');
const { VueLoaderPlugin } = require('vue-loader');
const WebpackShellPlugin = require('webpack-shell-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, '../webapp/dist'),
    publicPath: '/dist/',
    filename: 'build.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['vue-style-loader', 'css-loader']
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        include: path.resolve(__dirname, 'src')
      },
      {
        test: /\.svg$/,
        loader: 'vue-svg-loader',
      },
      {
        test: /\.scss?$/,
        loaders: ['vue-style-loader', 'css-loader', 'sass-loader']
      },

      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg|off|woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]?[hash]'
            }
          },

          {
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: {
                progressive: true,
                quality: 65
              },
              // optipng.enabled: false will disable optipng
              optipng: {
                enabled: false
              },
              pngquant: {
                quality: '65-90',
                speed: 4
              },
              gifsicle: {
                interlaced: false
              }
              /*
                                // the webp option will enable WEBP
                                webp: {
                                    quality: 75
                                }
                                */
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      minimize: true
    }),
    new VueLoaderPlugin(),
    new CleanWebpackPlugin()
  ],
  resolveLoader: {
    alias: {
      'scss-loader': 'sass-loader'
    }
  },
  watchOptions: {
    ignored: ['node_modules', 'mdl', 'css', 'config', 'img'],
    poll: 1000
  },
  resolve: {
    alias: {
      vue$: 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  devtool: '#source-map'
};

if (process.env.NODE_ENV === 'development') {
  module.exports.mode = 'development';
  module.exports.output.path = path.resolve(__dirname, './../../../target/platform-microservice-storage-1.0.0/dist');
}

if (process.env.NODE_ENV === 'developmentLinux') {
  module.exports.mode = 'development';
  module.exports.plugins = (module.exports.plugins || []).concat([
    new WebpackShellPlugin({
      onBuildEnd: ['bash deployLinux.sh'],
      dev: false,
      safe: true
    })
  ]);
  module.exports.mode = 'development';
}

if (process.env.NODE_ENV === 'production') {
  module.exports.mode = 'production';
  module.exports.optimization = {
    minimizer: [
      // we specify a custom UglifyJsPlugin here to get source maps in production
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        uglifyOptions: {
          compress: false,
          ecma: 6,
          mangle: true
        },
        sourceMap: true
      })
    ]
  };
}
