@ECHO OFF
CD ..
CD webapp
SET WEBAPP_FOLDER="%CD%"
CD ..\..\..
CD target\platform-microservice-storage-1.0.0
REM /H          Kopiert auch Dateien mit den Attributen "Versteck" und "System"
REM /E          Kopiert alle Unterverzeichnisse (leer oder nicht leer). Wie /S /E. Mit dieser Option kann die Option /T geändert werden.
REM /K          Kopiert Attribute. Standardmäßig wird "Schreibgeschützt" gelöscht.
REM /F          Zeigt die Namen der Quell- und Zieldateien beim Kopieren an.
REM /Y          Unterdrückt die Aufforderung zur Bestätigung, dass eine vorhandene Zieldatei überschrieben werden soll.
REM /Q          Zeigt beim Kopieren keine Dateinamen an.
REM /D:M-T-J    Kopiert nur die an oder nach dem Datum geänderten Dateien. Ist kein Datum angegeben, werden nur Dateien kopiert, die neuer als die bestehenden Zieldateien sind.
REM /N          Beim Kopieren werden die erstellten Kurznamen verwendet.
REM /L          Listet die Dateien auf, die ggf. kopiert werden.
XCOPY %WEBAPP_FOLDER%\dist %CD%\dist /H /E /K /F /Y /D
CD ..\..
CD src\main\vueApp