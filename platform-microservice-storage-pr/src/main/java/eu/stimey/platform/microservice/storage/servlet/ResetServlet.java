package eu.stimey.platform.microservice.storage.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Forwarding to Vue.js (SPA)
 *
 */
@WebServlet("/reset")
public class ResetServlet extends HttpServlet {
	protected static final long serialVersionUID = 3796385105960343120L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
		try {
			request.getRequestDispatcher("index.html").forward(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
}
