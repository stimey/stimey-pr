package eu.stimey.platform.microservice.storage.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import redis.clients.jedis.Jedis;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.utils.security.JsonWebToken;
import eu.stimey.platform.microservice.storage.startup.Global;

/**
 * AuthenticationFilter, checks for allowed access (successful authentication or URL is whitelisted)
 *
 */
public class AuthenticationFilter implements Filter {
	protected static Map<String, Boolean> whiteList;

	static {
		whiteList = new HashMap<>();
		whiteList.put("/", true);
		whiteList.put("/home", true);
		whiteList.put("/login", true);
		whiteList.put("/signup", true);
		whiteList.put("/signup/*", true);
		whiteList.put("/reset", true);
		whiteList.put("/notfound", true);
		whiteList.put("/language", true);
		whiteList.put("/eportfolio/*", true);
		whiteList.put("/termsAndConditions", true);
		whiteList.put("/privacyPolicy", true);
		whiteList.put("/imprint", true);
		whiteList.put("/disclaimer", true);

		whiteList.put("/css/*", true);
		whiteList.put("/img/*", true);
		whiteList.put("/js/*", true);
		whiteList.put("/less/*", true);
		whiteList.put("/mdl/*", true);
		whiteList.put("/vendor/*", true);

		whiteList.put("/dist/*", true);

		whiteList.put("/favicon.ico", true);

		whiteList.put("/pedagogical_framework.pdf", true);
		
		whiteList.put("/index.html", true);
	}

	protected static boolean accessable(ServletRequest request) {
		boolean result = false;

		String url = ((HttpServletRequest) request).getRequestURI()
				.replace(((HttpServletRequest) request).getContextPath(), "");
		// logger().debug("accessable:url:"+url);
		result = whiteList.get(url) != null;
		if (!result) {
			String path = url.substring(0, url.lastIndexOf("/"));
			// logger().debug("accessable:path:"+path);
			if (path != null)
				for (String key : whiteList.keySet())
					if (path.startsWith(key.replace("/*", ""))) {
						result = true;
						break;
					}
		}

		// logger().debug("accessable:result:"+result);
		return result;
	}

	protected static volatile String PRIVATE_KEY = null;
	protected static final Object lock = new Object();

	// uses Double-Check-Idiom a la Bloch
	public static String getPrivateKey() {
		String temp = PRIVATE_KEY;
		if (temp == null) {
			synchronized (lock) {
				temp = PRIVATE_KEY;
				if (temp == null) {
					Jedis jedis = RedisAMQPService.getService().getJedisPool().getResource();
					PRIVATE_KEY = temp = jedis.get("shared_global-private_key");
					jedis.close();
				}
			}
		}

		return temp;
	}

	@Override
	public void init(FilterConfig filterConfig) {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		boolean authenticated = false;

		// @see also:
		// https://router.vuejs.org/guide/essentials/history-mode.html#example-server-configurations
		boolean errorRequestURI = (String) request.getAttribute(RequestDispatcher.ERROR_REQUEST_URI) != null;
		if (Global.PRODUCTION && (!accessable(request) || errorRequestURI)) {
			Jws<Claims> claims = null;
			try {
				String compactJws = JsonWebToken.getJWT((HttpServletRequest) request);
				authenticated = compactJws != null && (claims = JsonWebToken
						.verify(JsonWebToken.transformKey(getPrivateKey()), compactJws)) != null;
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (authenticated) {
				if (errorRequestURI) {
					request.removeAttribute(RequestDispatcher.ERROR_REQUEST_URI);
					((HttpServletResponse) response).setStatus(200);
				}

				request.setAttribute("userid", claims.getBody().getSubject());
				request.setAttribute("username", claims.getBody().get("username"));
				request.setAttribute("usertype", claims.getBody().get("usertype"));
			} else {
				if (errorRequestURI)
					request.removeAttribute(RequestDispatcher.ERROR_REQUEST_URI);
				handleNotAuthenticated(response);
			}
		}

		// Turning off HTTP session (Stateless microservice)
		chain.doFilter(new HttpServletRequestWrapper((HttpServletRequest) request) {
			@Override
			public HttpSession getSession() {
				return null;
			}

			@Override
			public HttpSession getSession(boolean create) {
				return null;
			}
		}, response);
	}

	protected void handleNotAuthenticated(ServletResponse response) {
		((HttpServletResponse) response).setStatus(307);
		if (Global.DOMAIN.equals("localhost"))
			((HttpServletResponse) response).addHeader("Location", "http://localhost:8080/login");
		else
			((HttpServletResponse) response).addHeader("Location", "https://" + Global.DOMAIN + ":443/login");
	}

	@Override
	public void destroy() {
	}
}
