package eu.stimey.platform.microservice.storage.servlet;

import eu.stimey.platform.library.utils.language.Language;
import eu.stimey.platform.microservice.storage.startup.Global;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Forwarding to Vue.js (SPA)
 *
 */
@WebServlet("/imprint")
public class ImprintServlet extends HttpServlet {
	protected static final long serialVersionUID = 5150776452263615334L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
		// set language cookie if not exists
		Language.get(request, response, Global.DOMAIN);

		try {
			request.getRequestDispatcher("/index.html").forward(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
}
