package eu.stimey.platform.microservice.storage.rest;

import static eu.stimey.platform.library.utils.StimeyLogger.*;

import javax.annotation.PreDestroy;
import javax.ws.rs.ApplicationPath;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import eu.stimey.platform.microservice.storage.beans.SchedulerService;

/**
 * REST configuration
 *
 */
@ApplicationPath("api")
public class RESTConfig extends ResourceConfig {
	protected final SchedulerService schedulerService;

	public RESTConfig() {
		super();

		logger().info("REST-Service started...");

		schedulerService = new SchedulerService();

		packages("eu.stimey.platform.microservice.storage.rest");

		register(new AbstractBinder() {
			protected void configure() {
				bind(schedulerService).to(SchedulerService.class);
			}
		});

		register(new JacksonJsonProvider().configure(SerializationFeature.INDENT_OUTPUT, true));
	}

	@PreDestroy
	public void shutdown() {
		schedulerService.getScheduler().shutdown();
		logger().info("REST-Service stopped...");
	}
}
