package eu.stimey.platform.microservice.storage.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;

import eu.stimey.platform.library.utils.language.Language;
import eu.stimey.platform.microservice.storage.startup.Global;

import java.io.IOException;

/**
 * Forwarding to Vue.js (SPA)
 *
 */
@WebServlet("/language")
public class LanguageServlet extends HttpServlet {
	protected static final long serialVersionUID = 8473886958381360346L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
		String language = request.getParameter("language");
		System.out.println(language);

		Language.set(request, response, Global.DOMAIN, language);

		response.setStatus(HttpServletResponse.SC_OK);
		response.setIntHeader(HttpHeaders.CONTENT_LENGTH, 0);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
}
