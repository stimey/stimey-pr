package eu.stimey.platform.microservice.storage.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Forwarding for Scientix
 *
 */
@WebServlet("/scientix")
public class ScientixServlet extends HttpServlet {
	protected static final long serialVersionUID = 1964609899121765625L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
		try {
			request.getRequestDispatcher("scientixtest.html").forward(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
}
