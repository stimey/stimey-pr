package eu.stimey.platform.microservice.storage.startup;

import eu.stimey.platform.library.core.startup.DefaultGlobal;

/**
 * Global settings (environment variables)
 * 
 */
public final class Global extends DefaultGlobal {
	public static boolean PRODUCTION = true;

	public static void startup() {
		DefaultGlobal.startup();
		if (DOCKER)
			PRODUCTION = true;
	}
}
