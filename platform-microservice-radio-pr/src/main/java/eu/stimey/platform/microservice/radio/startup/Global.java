package eu.stimey.platform.microservice.radio.startup;

import eu.stimey.platform.library.core.startup.DefaultGlobal;

/**
 * 
 * Global settings (environment variables)
 *
 */
public final class Global extends DefaultGlobal {
	public static boolean RADIO_ADMIN;
	public static void startup() {
		DefaultGlobal.startup();
    }
}
