package eu.stimey.platform.microservice.radio.databind;

import eu.stimey.platform.library.utils.databind.DocumentWithId2;

import java.util.HashMap;
import java.util.Map;

import static eu.stimey.platform.library.utils.StimeyLogger.logger;

public class PodcastRating extends DocumentWithId2 {
    /*
        Determines the podcast, which this rating depends on
     */
    private String resourceId;

    /*
        Calculated overall rating for the frontend
     */
    private int overallRating;

    /*
        Key: The Star (form 1 - 5)
        Value: The overall ratings of this star
     */
    private Map<Integer, Integer> ratings;
    /*
        Key: UserId
        Value: the star vote, the user gave
     */
    private Map<String, Integer> userVotes;

    public PodcastRating() {
        super();
    }

    public PodcastRating(String resourceId) {
        this.resourceId = resourceId;
        this.ratings = new HashMap<>();
        this.userVotes = new HashMap<>();

        // fill ratings with 0
        for (int i = 1; i < 6; i++) {
            this.ratings.put(i, 0);
        }
    }

    private void calculateRating() {
        int overall;

        int firstPart = 0;
        int secondPart = 0;

        // weight algorithm for each star..

        for (Map.Entry<Integer, Integer> entry : this.ratings.entrySet()) {
            int star = entry.getKey(); // star type
            int votes = entry.getValue(); // votes

            firstPart += (star * votes);
            secondPart += votes;
        }

        // niemals durch 0
        if (firstPart == 0 || secondPart == 0)
            overall = 0;
        else
            overall = (firstPart) / (secondPart);

        setOverallRating(overall);
    }

    public void addRating(final int star, final String userId) {
        if (this.userVotes.containsKey(userId)) {
            int previous = this.userVotes.get(userId);

            int previousRatings = this.ratings.get(previous) - 1;
            this.ratings.replace(previous, previousRatings);

            int newRating = this.ratings.get(star) + 1;
            this.ratings.replace(star, newRating);

            this.userVotes.remove(userId);
            this.userVotes.put(userId, star);
        } else {
            int ratings = this.ratings.get(star) + 1;
            this.ratings.replace(star, ratings);
            this.userVotes.put(userId, star);
        }
    }

    private void setOverallRating(int rating) {
        this.overallRating = rating;
    }

    public int getOverallRating() {
        calculateRating();
        return this.overallRating;
    }

    public Map<Integer, Integer> getRatings() {
        return this.ratings;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceId() {
        return this.resourceId;
    }

    public void setUserVotes(HashMap<String, Integer> votes) {
        this.userVotes = votes;
    }
    public Map<String, Integer> getUserVotes() {
        return this.userVotes;
    }
}
