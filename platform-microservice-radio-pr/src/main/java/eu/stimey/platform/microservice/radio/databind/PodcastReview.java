package eu.stimey.platform.microservice.radio.databind;

import eu.stimey.platform.library.utils.databind.DocumentWithId2;

public class PodcastReview extends DocumentWithId2 {
    public String podcastId;
    public String userId;
    public String username;
    public String avatarImage;
    public String comment;
    public String timestamp;

    public PodcastReview() {
        super();
    }

    public PodcastReview(String podcastId, String userId, String username, String avatarImage, String comment, String timestamp) {
        this.podcastId = podcastId;
        this.userId = userId;
        this.username = username;
        this.avatarImage = avatarImage;
        this.comment = comment;
        this.timestamp = timestamp;
    }
}
