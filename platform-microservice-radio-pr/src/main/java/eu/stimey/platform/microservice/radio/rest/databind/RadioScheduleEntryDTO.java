package eu.stimey.platform.microservice.radio.rest.databind;

public class RadioScheduleEntryDTO {
    public int day;
    public String time;
    public String title;
    public String description;
}
