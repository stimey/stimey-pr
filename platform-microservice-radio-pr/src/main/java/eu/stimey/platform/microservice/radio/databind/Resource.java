package eu.stimey.platform.microservice.radio.databind;

import java.util.Date;
import java.util.List;

import eu.stimey.platform.library.utils.databind.DocumentWithId2;

public class Resource extends DocumentWithId2 implements Comparable {
    public String authorId;
    public String authorName;
    public String title;
    public String language;
    public String program;
    public List<String> keywords;
    public String description;
    public FileMetadata uploadedFile;
    public String creationDate;
    public boolean approved;
    public String approvedDate;

    public PodcastRating podcastRating;
    public List<PodcastReview> podcastReviews;
    
	public Resource() {
		super();
	}

	public Resource(String authorId, String authorName, String title, String language, String program, List<String> keywords,
			String description, FileMetadata uploadedFile) {
		super();
		this.authorId = authorId;
		this.authorName = authorName;
		this.title = title;
		this.language = language;
		this.program = program;
		this.keywords = keywords;
		this.description = description;
		this.uploadedFile = uploadedFile;
		this.creationDate = String.valueOf(new Date().getTime());
		this.approved = false;
	}

	public void setPodcastRating(PodcastRating rating) {
		this.podcastRating = rating;
	}
	public void setPodcastReviews(List<PodcastReview> reviews) { this.podcastReviews = reviews; }

	@Override
	public int compareTo(Object compareto) {
		Resource compareRes = ((Resource) compareto);

		if (compareRes.podcastRating != null && this.podcastRating != null) {
			return compareRes.podcastRating.getOverallRating() - this.podcastRating.getOverallRating();
		}

		return -1; // means no ratings
	}
}
