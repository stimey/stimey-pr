package eu.stimey.platform.microservice.radio.rest.filter;

import javax.ws.rs.ext.Provider;

import eu.stimey.platform.library.rest.filter.DefaultAuthenticationFilter;

/**
 * AuthenticationFilter for REST, checks for allowed access (successful authentication)
 *
 */
@Provider
public class AuthenticationFilter extends DefaultAuthenticationFilter {

}
