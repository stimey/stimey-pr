package eu.stimey.platform.microservice.radio.rest.databind;

public class PodcastFeedback {
    public String podcastId;
    public String authorId;
    public String feedback;
}
