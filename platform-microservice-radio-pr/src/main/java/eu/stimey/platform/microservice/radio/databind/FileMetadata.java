package eu.stimey.platform.microservice.radio.databind;

public class FileMetadata {
    public String fileId;
    public String fileName;
    public String contentType;

    public FileMetadata(){
    	super();
    }

    public FileMetadata(String fileId, String fileName, String contentType) {
    	super();
        this.fileId = fileId;
        this.fileName = fileName;
        this.contentType = contentType;
    }
}
