package eu.stimey.platform.microservice.radio.rest.resources;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.microservice.radio.beans.RadioService;
import eu.stimey.platform.microservice.radio.databind.ChangePassword;
import eu.stimey.platform.microservice.radio.databind.Resource;
import eu.stimey.platform.microservice.radio.databind.ResourceFeedback;
import eu.stimey.platform.microservice.radio.rest.databind.PodcastFeedback;
import eu.stimey.platform.microservice.radio.rest.databind.RadioScheduleEntryDTO;
import eu.stimey.platform.microservice.radio.rest.databind.SignInInfo;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.*;
import java.util.Base64;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static eu.stimey.platform.library.utils.StimeyLogger.logger;


@Path("/admin")
public class AdminResource {
    /*
        Define the auth cookie that contains encrypted login info
     */
    private static final String COOKIE_NAME = "radio-access";

    @Inject
    private UserSharedService userSharedService;
    @Inject
    private RadioService radioService;

    @GET
    @Path("is-admin")
    @Produces(MediaType.TEXT_PLAIN)
    public Response isAdmin(@Context ContainerRequestContext requestContext, @CookieParam(COOKIE_NAME) Cookie cookie) {
        String userId = (String) requestContext.getProperty("userid");
        String username = (String) requestContext.getProperty("username");

        boolean isAdmin = false;

        if (cookie != null) {
            String value = requestContext.getCookies().get(COOKIE_NAME).getValue();
            isAdmin = radioService.checkAdminRights(value, userSharedService.getUser(userId));
        }

        if (userSharedService.getUser(userId).usertype.equals("radio")) {
            isAdmin = true;
        }

        return Response.status(200).entity(isAdmin).build();
    }

    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response signIn(@Context ContainerRequestContext requestContext, SignInInfo signInInfo) {
        String userId = (String) requestContext.getProperty("userid");
        String username = (String) requestContext.getProperty("username");

        boolean result;

        try {
            byte[] password = Base64.getDecoder().decode(signInInfo.password);
            result = radioService.checkPassword(password);
        } catch (Exception ex) {
            return Response.status(401).entity(ex.toString()).build();
        }

        if (result) {
            NewCookie authCookie;

            try {
                authCookie = radioService.createCookie(COOKIE_NAME, userId, username);
            } catch (Exception ex) {
                return Response.status(401).entity(ex.toString()).build();
            }

            return Response.status(200).entity(userId).cookie(authCookie).build();
        } else {
            return Response.status(401).entity("").build();
        }
    }

    @POST
    @Path("logout")
    public Response signOut(@Context ContainerRequestContext requestContext, @CookieParam(COOKIE_NAME) Cookie cookie) {
        String userId = (String) requestContext.getProperty("userid");
        String username = (String) requestContext.getProperty("username");

        String cookieValue = "";

        if (cookie != null)
            cookieValue = cookie.getValue();

        if (radioService.checkAdminRights(cookieValue, userSharedService.getUser(userId))) {
            NewCookie authCookie = radioService.createCookie(COOKIE_NAME,"", "");
            return Response.status(200).cookie(authCookie).build();
        }
        return Response.status(401).build();
    }

    @GET
    @Path("podcasts")
    public Response getPodcasts(@Context ContainerRequestContext requestContext, @CookieParam(COOKIE_NAME) Cookie cookie) {
        String userId = (String) requestContext.getProperty("userid");
        String username = (String) requestContext.getProperty("username");

        String cookieValue = "";

        if (cookie != null)
            cookieValue = cookie.getValue();

        if (radioService.checkAdminRights(cookieValue, userSharedService.getUser(userId))) {
            List<Resource> podcasts = radioService.findResources(false, "");
            return Response.status(200).entity(podcasts).build();
        }

        return Response.status(401).build();
    }

    @POST
    @Path("feedback/send")
    public Response sendFeedback(@Context ContainerRequestContext requestContext, @CookieParam("access_token") Cookie access_token, @CookieParam(COOKIE_NAME) Cookie cookie, PodcastFeedback feedback) {
        String userId = (String) requestContext.getProperty("userid");
        String username = (String) requestContext.getProperty("username");

        String cookieValue = "";

        if (cookie != null)
            cookieValue = cookie.getValue();

        if (radioService.checkAdminRights(cookieValue, userSharedService.getUser(userId)) && feedback != null) {
            boolean success = radioService.sendFeedbackMessage(feedback, access_token);

            int status = (success) ? 200 : 405;

            return Response.status(status).build();
        }

        return Response.status(401).build();
    }

    @GET
    @Path("feedback/get/{podcast}")
    public Response getExistingFeedback(@Context ContainerRequestContext requestContext, @CookieParam(COOKIE_NAME) Cookie cookie, @PathParam("podcast") String podcastId) {
        String userId = (String) requestContext.getProperty("userid");
        String username = (String) requestContext.getProperty("username");

        String cookieValue = "";

        if (cookie != null)
            cookieValue = cookie.getValue();

        if (radioService.checkAdminRights(cookieValue, userSharedService.getUser(userId)) && podcastId != null && podcastId.length() > 0) {
            ResourceFeedback feedback = radioService.getResourceFeedback(podcastId);

            if (feedback != null) {
                return Response.status(200).entity(feedback).build();
            } else {
                return Response.status(404).build();
            }
        }

        return Response.status(401).build();
    }

    @POST
    @Path("podcast/approve/{podcast}")
    public Response approvePodcast(@Context ContainerRequestContext requestContext, @CookieParam(COOKIE_NAME) Cookie cookie, @CookieParam("access_token") Cookie access_token, @PathParam("podcast") String podcastId, PodcastFeedback feedback) {
        String userId = (String) requestContext.getProperty("userid");
        String username = (String) requestContext.getProperty("username");

        String cookieValue = "";

        if (cookie != null)
            cookieValue = cookie.getValue();

        if (radioService.checkAdminRights(cookieValue, userSharedService.getUser(userId)) && podcastId != null && podcastId.length() > 0) {
            boolean success = radioService.approveContent(podcastId);

            int status = (success) ? 200 : 405;

            if (status == 200) {
                if (feedback != null) {
                    success = radioService.sendFeedbackMessage(feedback, access_token);
                }
            }

            status = (success) ? 200 : 405;


            return Response.status(status).build();
        }

        return Response.status(401).build();
    }

    @POST
    @Path("podcast/delete/{podcast}")
    public Response deletePodcast(@Context ContainerRequestContext requestContext, @CookieParam(COOKIE_NAME) Cookie cookie, @PathParam("podcast") String podcastId) {
        String userId = (String) requestContext.getProperty("userid");
        String username = (String) requestContext.getProperty("username");

        String cookieValue = "";

        if (cookie != null)
            cookieValue = cookie.getValue();

        if (radioService.checkAdminRights(cookieValue, userSharedService.getUser(userId)) && podcastId != null && podcastId.length() > 0) {
            boolean success = radioService.deletePodcast(podcastId);

            int status = (success) ? 200 : 405;

            return Response.status(status).build();
        }

        return Response.status(401).build();
    }

    @POST
    @Path("settings/password/change")
    public Response changePassword(@Context ContainerRequestContext requestContext, @CookieParam(COOKIE_NAME) Cookie cookie, ChangePassword changePassword) {
        String userId = (String) requestContext.getProperty("userid");
        String username = (String) requestContext.getProperty("username");

        String cookieValue = "";

        if (cookie != null)
            cookieValue = cookie.getValue();

        if (radioService.checkAdminRights(cookieValue, userSharedService.getUser(userId)) && changePassword != null) {
            boolean success = radioService.changePassword(changePassword);

            int status = (success) ? 200 : 405;

            return Response.status(status).build();
        }

        return Response.status(401).build();
    }

    @POST
    @Path("radio/schedule/{id}/remove")
    public Response removeScheduleEntry(@Context ContainerRequestContext requestContext, @CookieParam(COOKIE_NAME) Cookie cookie, @PathParam("id") final String entryId) {
        String userId = (String) requestContext.getProperty("userid");
        String username = (String) requestContext.getProperty("username");

        String cookieValue = "";

        if (cookie != null)
            cookieValue = cookie.getValue();

        if (radioService.checkAdminRights(cookieValue, userSharedService.getUser(userId))) {
            boolean success = radioService.deleteEntryFromSchedule(entryId);

            int status = (success) ? 200 : 405;

            return Response.status(status).entity(success).build();
        }

        return Response.status(401).build();
    }

    @POST
    @Path("radio/schedule/replace")
    public Response replaceOrAddScheduleEntry(@Context ContainerRequestContext requestContext, @CookieParam(COOKIE_NAME) Cookie cookie, RadioScheduleEntryDTO entry) {
        String userId = (String) requestContext.getProperty("userid");
        String username = (String) requestContext.getProperty("username");

        String cookieValue = "";

        if (cookie != null)
            cookieValue = cookie.getValue();

        if (radioService.checkAdminRights(cookieValue, userSharedService.getUser(userId))) {
            Pattern pattern = Pattern.compile("(\\d{1,2}:\\d{2})-(\\d{1,2}:\\d{2})");
            Matcher m = pattern.matcher(entry.time.trim());

            if (m.matches()) {
                radioService.replaceOrAddEntryToSchedule(entry);
                return Response.status(200).build();
            } else {
                return Response.status(204).build();
            }
        }

        return Response.status(401).build();
    }

    @POST
    @Path("podcast/review/{id}/delete")
    public Response deletePodcastReview(@Context ContainerRequestContext requestContext, @CookieParam(COOKIE_NAME) Cookie cookie, @PathParam("id") final String reviewId) {
        String userId = (String) requestContext.getProperty("userid");

        String cookieValue = "";
        if (cookie != null)
            cookieValue = cookie.getValue();

        if (radioService.checkAdminRights(cookieValue, userSharedService.getUser(userId)) || radioService.isReviewOwner(reviewId, userId)) {
            radioService.deletePodcastReview(reviewId);
            return Response.status(200).build();
        }

        return Response.status(401).build();
    }
}
