package eu.stimey.platform.microservice.radio.rest.databind;

public class PodcastReviewDTO {
    public String podcastId;
    public String avatarImage;
    public String comment;
}
