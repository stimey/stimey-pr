package eu.stimey.platform.microservice.radio.databind;

import java.io.Serializable;

public class UserMessageWrapper implements Serializable {
    public String message;
    public String chatlisttopic;
    public String fileId;
    public String fileName;
    public String photoFileId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getChatlisttopic() {
        return chatlisttopic;
    }

    public void setChatlisttopic(String chatlisttopic) {
        this.chatlisttopic = chatlisttopic;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPhotoFileId() {
        return photoFileId;
    }

    public void setPhotoFileId(String photoFileId) {
        this.photoFileId = photoFileId;
    }

    public UserMessageWrapper() {}
}