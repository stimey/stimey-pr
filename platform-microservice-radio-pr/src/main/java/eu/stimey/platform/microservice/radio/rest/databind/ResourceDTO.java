package eu.stimey.platform.microservice.radio.rest.databind;

import java.util.List;

import eu.stimey.platform.microservice.radio.databind.FileMetadata;

public class ResourceDTO {
    public String title;
    public String language;
    public String program;
    public List<String> keywords;
    public String description;
    public FileMetadata uploadedFile;
    
	public ResourceDTO() {
		super();
	}

	public ResourceDTO(String title, String language, String program, List<String> keywords, String description,
			FileMetadata uploadedFile) {
		super();
		this.title = title;
		this.language = language;
		this.program = program;
		this.keywords = keywords;
		this.description = description;
		this.uploadedFile = uploadedFile;
	}
}
