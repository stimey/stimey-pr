package eu.stimey.platform.microservice.radio.databind;

import eu.stimey.platform.library.utils.databind.DocumentWithId2;


public class ResourceFeedback extends DocumentWithId2 {
    public String resourceId;
    public String feedbackComment;
    public String feedbackCreated;
}
