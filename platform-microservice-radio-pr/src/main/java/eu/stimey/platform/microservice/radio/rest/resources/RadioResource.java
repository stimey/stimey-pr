package eu.stimey.platform.microservice.radio.rest.resources;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;
import eu.stimey.platform.microservice.radio.beans.RadioService;
import eu.stimey.platform.microservice.radio.databind.PodcastReview;
import eu.stimey.platform.microservice.radio.databind.RadioScheduleEntry;
import eu.stimey.platform.microservice.radio.databind.Resource;
import eu.stimey.platform.microservice.radio.rest.databind.PodcastRatingDTO;
import eu.stimey.platform.microservice.radio.rest.databind.PodcastReviewDTO;
import eu.stimey.platform.microservice.radio.rest.databind.ResourceDTO;

/**
 * 
 * Radio resource
 *
 */
@Path("/radio")
public class RadioResource {
	@Inject
	UserSharedService userSharedService;
	@Inject
	RadioService radioService;
	
	@GET
	@Path("resources")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNotApprovedResources(@Context ContainerRequestContext requestContext) {
		String userId = (String)requestContext.getProperty("userid");

		UserSharedCacheObject userSharedCacheObject = userSharedService.getUser(userId);
		if (userSharedCacheObject!=null && userSharedCacheObject.usertype.equalsIgnoreCase("radio")) {
			List<Resource> notApprovedResources = radioService.findResources(false, userId);
			return Response.ok().entity(notApprovedResources).build();
		}
		return Response.status(401).build();
	}
	
	@POST
	@Path("resource")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postActivity(@Context ContainerRequestContext requestContext, ResourceDTO resourceDTO) {
		String userId = (String) requestContext.getProperty("userid");
		String username = (String) requestContext.getProperty("username");

		if (resourceDTO!=null) {
			Resource resource = new Resource(
					userId,
					username,
					resourceDTO.title, 
					resourceDTO.language,
					resourceDTO.program, 
					resourceDTO.keywords,
					resourceDTO.description, 
					resourceDTO.uploadedFile);

			radioService.set(resource.get_id().toHexString(), resource);

			return Response.ok().build();
		}
		else
			return Response.status(400).build();
	}

	@GET
	@Path("proposed-resources")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response fetchUserProposedResources(@Context ContainerRequestContext requestContext) {
		String userId = (String)requestContext.getProperty("userid");

		UserSharedCacheObject userSharedCacheObject = userSharedService.getUser(userId);

		if (userSharedCacheObject != null){
			List<Resource> notApprovedResources = radioService.findResources(false, userId);
			return Response.ok().entity(notApprovedResources).build();
		}

		return Response.status(401).build();
	}

	@GET
	@Path("podcasts/{filter}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchPodcasts(@Context ContainerRequestContext requestContext, @PathParam("filter") final String fil) {
		RadioService.PodcastZoneFilter filter = RadioService.PodcastZoneFilter.DATE;

		if (fil.equals("rating"))
			filter = RadioService.PodcastZoneFilter.RATING;

		List<Resource> approvedResources = radioService.getResourcesWithFilter(filter);

		return Response.status(200).entity(approvedResources).build();
	}

	@POST
	@Path("podcast/rate")
	public Response ratePodcast(@Context ContainerRequestContext requestContext, PodcastRatingDTO podcastRating) {
		String userId = (String) requestContext.getProperty("userid");

		if (podcastRating != null)
			radioService.ratePodcast(userId, podcastRating.podcastId, podcastRating.rating);

		return Response.status(200).build();
	}

	@GET
	@Path("podcasts/{program}/{filter}")
	public Response podcastsByProgram(@Context ContainerRequestContext requestContext, @PathParam("program") final String program, @PathParam("filter") final String fil) {
		List<Resource> result = new LinkedList<>();

		if (program.length() > 0) {
			RadioService.PodcastZoneFilter filter = RadioService.PodcastZoneFilter.DATE;
			if (fil.equals("rating"))
				filter = RadioService.PodcastZoneFilter.RATING;

			result = radioService.getResourcesByProgram(program, filter);
		}

		return Response.status(200).entity(result).build();
	}

	@GET
	@Path("podcast/{id}")
	public Response getPodcastById(@Context ContainerRequestContext requestContext, @PathParam("id") final String podcastId) {
		Resource resource = radioService.get(podcastId);

		if (resource != null) {
			resource.setPodcastRating(radioService.getPodcastRating(podcastId));
			resource.setPodcastReviews(radioService.getReviewsForPodcast(podcastId));
			return Response.status(200).entity(resource).build();
		}
		else
			return Response.status(204).build();
	}

	@POST
	@Path("podcast/review")
	public Response createPodcastReview(@Context ContainerRequestContext requestContext, PodcastReviewDTO reviewDTO) {
		String userId = (String) requestContext.getProperty("userid");
		String username = (String) requestContext.getProperty("username");

		radioService.createPodcastReview(reviewDTO.podcastId, userId, username, reviewDTO.avatarImage, reviewDTO.comment);
		return Response.status(200).build();
	}

	@GET
	@Path("podcast/reviews/{podcastId}")
	public Response getPodcastReviews(@Context ContainerRequestContext requestContext, @PathParam("podcastId") final String podcastId) {
		List<PodcastReview> reviews = radioService.getReviewsForPodcast(podcastId);
		return Response.status(200).entity(reviews).build();
	}

	@GET
	@Path("radio/schedule")
	public Response getRadioSchedule(@Context ContainerRequestContext requestContext) {
		HashMap<Integer, List<RadioScheduleEntry>> list = radioService.getRadioSchedule();
		return Response.status(200).entity(list).build();
	}
}
