package eu.stimey.platform.microservice.radio.rest.databind;

public class PodcastRatingDTO {
    public String podcastId;
    public int rating;
}
