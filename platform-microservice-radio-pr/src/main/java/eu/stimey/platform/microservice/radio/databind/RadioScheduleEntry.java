package eu.stimey.platform.microservice.radio.databind;

import eu.stimey.platform.library.utils.databind.DocumentWithId2;

public class RadioScheduleEntry extends DocumentWithId2 implements Comparable {
    public int day;
    public String time;
    public String title;
    public String description;

    public RadioScheduleEntry() {
        super();
    }

    public RadioScheduleEntry(int day, String time, String title, String description) {
        this.day = day;
        this.time = time;
        this.title = title;
        this.description = description;
    }

    @Override
    public String toString() {
        return "RadioScheduleEntry{day='" + day + "', time='" + time + "'}";
    }

    @Override
    public int compareTo(Object o) {
        RadioScheduleEntry other = (RadioScheduleEntry) o;

        Integer thisTime = Integer.valueOf(time.trim().split("-")[0].split(":")[0]);
        Integer compareTime = Integer.valueOf(other.time.trim().split("-")[0].split(":")[0]);

        return thisTime.compareTo(compareTime);
    }
}
