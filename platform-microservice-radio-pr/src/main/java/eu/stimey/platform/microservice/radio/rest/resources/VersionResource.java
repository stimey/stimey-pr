package eu.stimey.platform.microservice.radio.rest.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import eu.stimey.platform.library.utils.rest.RESTResponse;

/**
 * 
 * Version resource
 *
 */
@Path("/version")
public class VersionResource {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response version() {
		return Response.ok().entity(new RESTResponse(RESTResponse.SUCCESS, 200, "1.0.0", "")).build();
	}
}
