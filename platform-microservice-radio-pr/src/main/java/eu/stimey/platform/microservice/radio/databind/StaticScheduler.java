package eu.stimey.platform.microservice.radio.databind;

import eu.stimey.platform.library.data.access.Caching;

public class StaticScheduler {
    public static void fillCache(Caching<String, RadioScheduleEntry> radioScheduleCaching) {
        RadioScheduleEntry entry = new RadioScheduleEntry(0, "7:00 - 9:00", "Wake Up with Energy", "Selection of music and tips to wake up full of energy.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(0, "9:00 - 10:00", "Musical Culture 90's and 2000's", "Music selection from the 90's to today.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(0, "10:00 - 11:00", "Musical Culture 90's and 2000's", "Music selection from the 90's to today.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(0, "11:00 - 12:00", "PENDRIVE", "Information about the best and most useful mobile Apps and technological devices.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(0, "12:00 - 13:00", "Europe in Your Pocket", "Information about European cities and travel tips.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(0, "13:00 - 14:00", "Healthy life", "Tips for a healthy lifestyle through mind and body care.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(0, "14:00 - 15:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(0, "15:00 - 16:00", "Music to focus", "Selection of music to concentrate and disconnect.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(0, "16:00 - 17:00", "Pencil, Ink and Paper", "Literature, theater plays and comic books all in one space.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(0, "17:00 - 18:00", "Stimey Experience", "Missions, challenges and all about your Stem content.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(0, "18:00 - 19:00", "PENDRIVE", "Information about the best and most useful mobile Apps and technological devices.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(0, "19:00 - 20:00", "Cinema & Series", "Find out everything about the latest movie premieres, the great classics and your favorite television series.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(0, "20:00 - 21:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(0, "21:00 - 22:00", "Europe in Your Pocket", "Information about European cities and travel tips.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(0, "22:00 - 23:00", "Music to focus", "Selection of music to concentrate and disconnect.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(0, "23:00 - 7:00", "Instrumental Music", "All types of instrumental music.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);


        entry = new RadioScheduleEntry(1, "7:00 - 9:00", "Wake Up with Energy", "Selection of music and tips to wake up full of energy.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(1, "9:00 - 10:00", "Musical Culture 90's and 2000's", "Music selection from the 90's to today.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(1, "10:00 - 11:00", "Musical Culture 90's and 2000's", "Music selection from the 90's to today.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(1, "11:00 - 12:00", "PENDRIVE", "Information about the best and most useful mobile Apps and technological devices.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(1, "12:00 - 13:00", "Europe in Your Pocket", "Information about European cities and travel tips.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(1, "13:00 - 14:00", "Healthy life", "Tips for a healthy lifestyle through mind and body care.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(1, "14:00 - 15:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(1, "15:00 - 16:00", "Music to focus", "Selection of music to concentrate and disconnect.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(1, "16:00 - 17:00", "Pencil, Ink and Paper", "Literature, theater plays and comic books all in one space.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(1, "17:00 - 18:00", "Stimey Experience", "Missions, challenges and all about your Stem content.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(1, "18:00 - 19:00", "PENDRIVE", "Information about the best and most useful mobile Apps and technological devices.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(1, "19:00 - 20:00", "Cinema & Series", "Find out everything about the latest movie premieres, the great classics and your favorite television series.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(1, "20:00 - 21:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(1, "21:00 - 22:00", "Europe in Your Pocket", "Information about European cities and travel tips.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(1, "22:00 - 23:00", "Music to focus", "Selection of music to concentrate and disconnect.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(1, "23:00 - 7:00", "Instrumental Music", "All types of instrumental music.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);


        entry = new RadioScheduleEntry(2, "7:00 - 9:00", "Wake Up with Energy", "Selection of music and tips to wake up full of energy.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(2, "9:00 - 10:00", "Musical Culture 90's and 2000's", "Music selection from the 90's to today.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(2, "10:00 - 11:00", "Musical Culture 90's and 2000's", "Music selection from the 90's to today.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(2, "11:00 - 12:00", "PENDRIVE", "Information about the best and most useful mobile Apps and technological devices.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(2, "12:00 - 13:00", "Europe in Your Pocket", "Information about European cities and travel tips.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(2, "13:00 - 14:00", "Healthy life", "Tips for a healthy lifestyle through mind and body care.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(2, "14:00 - 15:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(2, "15:00 - 16:00", "Music to focus", "Selection of music to concentrate and disconnect.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(2, "16:00 - 17:00", "Pencil, Ink and Paper", "Literature, theater plays and comic books all in one space.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(2, "17:00 - 18:00", "Stimey Experience", "Missions, challenges and all about your Stem content.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(2, "18:00 - 19:00", "PENDRIVE", "Information about the best and most useful mobile Apps and technological devices.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(2, "19:00 - 20:00", "Cinema & Series", "Find out everything about the latest movie premieres, the great classics and your favorite television series.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(2, "20:00 - 21:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(2, "21:00 - 22:00", "Europe in Your Pocket", "Information about European cities and travel tips.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(2, "22:00 - 23:00", "Music to focus", "Selection of music to concentrate and disconnect.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(2, "23:00 - 7:00", "Instrumental Music", "All types of instrumental music.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);


        entry = new RadioScheduleEntry(3, "7:00 - 9:00", "Wake Up with Energy", "Selection of music and tips to wake up full of energy.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(3, "9:00 - 10:00", "Musical Culture 90's and 2000's", "Music selection from the 90's to today.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(3, "10:00 - 11:00", "Musical Culture 90's and 2000's", "Music selection from the 90's to today.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(3, "11:00 - 12:00", "PENDRIVE", "Information about the best and most useful mobile Apps and technological devices.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(3, "12:00 - 13:00", "Europe in Your Pocket", "Information about European cities and travel tips.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(3, "13:00 - 14:00", "Healthy life", "Tips for a healthy lifestyle through mind and body care.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(3, "14:00 - 15:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(3, "15:00 - 16:00", "Music to focus", "Selection of music to concentrate and disconnect.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(3, "16:00 - 17:00", "Pencil, Ink and Paper", "Literature, theater plays and comic books all in one space.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(3, "17:00 - 18:00", "Stimey Experience", "Missions, challenges and all about your Stem content.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(3, "18:00 - 19:00", "PENDRIVE", "Information about the best and most useful mobile Apps and technological devices.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(3, "19:00 - 20:00", "Cinema & Series", "Find out everything about the latest movie premieres, the great classics and your favorite television series.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(3, "20:00 - 21:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(3, "21:00 - 22:00", "Europe in Your Pocket", "Information about European cities and travel tips.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(3, "22:00 - 23:00", "Music to focus", "Selection of music to concentrate and disconnect.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(3, "23:00 - 7:00", "Instrumental Music", "All types of instrumental music.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);


        entry = new RadioScheduleEntry(4, "9:00 - 10:00", "Wake Up with Energy", "Selection of music and tips to wake up full of energy.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(4, "10:00 - 11:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(4, "11:00 - 12:00", "PENDRIVE", "Information about the best and most useful mobile Apps and technological devices.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(4, "12:00 - 13:00", "Musical Culture 90's and 2000's", "Music selection from the 90's to today.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(4, "13:00 - 14:00", "Cinema & Series", "Find out everything about the latest movie premieres, the great classics and your favorite television series.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(4, "14:00 - 15:00", "Healthy life", "Tips for a healthy lifestyle through mind and body care.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(4, "15:00 - 16:00", "Instrumental Music", "All types of instrumental music.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(4, "16:00 - 17:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(4, "17:00 - 18:00", "Europe in Your Pocket", "Information about European cities and travel tips.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(4, "18:00 - 19:00", "PENDRIVE", "Information about the best and most useful mobile Apps and technological devices.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(4, "19:00 - 20:00", "Musical Culture 90's and 2000's", "Music selection from the 90's to today.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(4, "20:00 - 24:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(4, "24:00 - 8:00", "Instrumental Music", "All types of instrumental music.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);


        entry = new RadioScheduleEntry(5, "9:00 - 10:00", "Wake Up with Energy", "Selection of music and tips to wake up full of energy.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(5, "10:00 - 11:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(5, "11:00 - 12:00", "PENDRIVE", "Information about the best and most useful mobile Apps and technological devices.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(5, "12:00 - 13:00", "Musical Culture 90's and 2000's", "Music selection from the 90's to today.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(5, "13:00 - 14:00", "Cinema & Series", "Find out everything about the latest movie premieres, the great classics and your favorite television series.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(5, "14:00 - 15:00", "Healthy life", "Tips for a healthy lifestyle through mind and body care.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(5, "15:00 - 16:00", "Instrumental Music", "All types of instrumental music.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(5, "16:00 - 17:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(5, "17:00 - 18:00", "Europe in Your Pocket", "Information about European cities and travel tips.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(5, "18:00 - 19:00", "PENDRIVE", "Information about the best and most useful mobile Apps and technological devices.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(5, "19:00 - 20:00", "Musical Culture 90's and 2000's", "Music selection from the 90's to today.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(5, "20:00 - 24:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(5, "24:00 - 8:00", "Instrumental Music", "All types of instrumental music.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);


        entry = new RadioScheduleEntry(6, "7:00 - 9:00", "Wake Up with Energy", "Selection of music and tips to wake up full of energy.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(6, "9:00 - 10:00", "Musical Culture 90's and 2000's", "Music selection from the 90's to today.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(6, "10:00 - 11:00", "Musical Culture 90's and 2000's", "Music selection from the 90's to today.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(6, "11:00 - 12:00", "PENDRIVE", "Information about the best and most useful mobile Apps and technological devices.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(6, "12:00 - 13:00", "Europe in Your Pocket", "Information about European cities and travel tips.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(6, "13:00 - 14:00", "Healthy life", "Tips for a healthy lifestyle through mind and body care.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(6, "14:00 - 15:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(6, "15:00 - 16:00", "Music to focus", "Selection of music to concentrate and disconnect.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(6, "16:00 - 17:00", "Pencil, Ink and Paper", "Literature, theater plays and comic books all in one space.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(6, "17:00 - 18:00", "Stimey Experience", "Missions, challenges and all about your Stem content.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(6, "18:00 - 19:00", "PENDRIVE", "Information about the best and most useful mobile Apps and technological devices.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(6, "19:00 - 20:00", "Cinema & Series", "Find out everything about the latest movie premieres, the great classics and your favorite television series.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(6, "20:00 - 21:00", "Stimey Hits", "Music selection. The number one tracks in the world.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(6, "21:00 - 22:00", "Europe in Your Pocket", "Information about European cities and travel tips.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(6, "22:00 - 23:00", "Music to focus", "Selection of music to concentrate and disconnect.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);

        entry = new RadioScheduleEntry(6, "23:00 - 7:00", "Instrumental Music", "All types of instrumental music.");
        radioScheduleCaching.set(entry.get_id().toHexString(), entry);
    }
}
