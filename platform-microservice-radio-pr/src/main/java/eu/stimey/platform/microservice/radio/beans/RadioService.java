package eu.stimey.platform.microservice.radio.beans;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.util.*;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.library.utils.security.AESEncryption;
import eu.stimey.platform.microservice.radio.databind.*;

import eu.stimey.platform.microservice.radio.rest.databind.PodcastFeedback;
import eu.stimey.platform.microservice.radio.rest.databind.RadioScheduleEntryDTO;
import eu.stimey.platform.microservice.radio.startup.Global;
import org.bson.Document;
import org.bson.types.ObjectId;

import eu.stimey.platform.library.data.access.Caching;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import static eu.stimey.platform.library.utils.StimeyLogger.logger;

/**
 * 
 * Service for radio
 *
 */
public class RadioService {

	public enum PodcastZoneFilter {
		DATE,
		RATING,
	}

	protected Caching<String, RadioScheduleEntry> radioScheduleCaching;
	protected Caching<String, Resource> radioResourcesCache;
	protected Caching<String, BackendUser> radioBackendCache;
	protected Caching<String, PodcastRating> radioRatingCache;
	protected Caching<String, PodcastReview> podcastReviewCache;
	protected Caching<String, ResourceFeedback> radioResourceFeedbackCache;

	protected static final List<Resource> RADIO_RESOURCES;
	
	static {
		RADIO_RESOURCES = new ArrayList<>();
	}

	public RadioService() {
		super();
		
		radioResourcesCache = new Caching<>("ms_radio_resources", "_id", Resource.class, "resources", GlobalVariables.CACHE_CONFIG);
		radioResourcesCache.setWorkaround_keyMapper((key) -> new ObjectId(key));
		radioResourcesCache.createIndex("approved");
		radioResourcesCache.clear();

		radioScheduleCaching = new Caching<>("ms_radio_schedule", "_id", RadioScheduleEntry.class, "schedule", GlobalVariables.CACHE_CONFIG);
		radioScheduleCaching.setWorkaround_keyMapper((key) -> new ObjectId(key));
		radioScheduleCaching.clear();

		radioBackendCache = new Caching<>("ms_radio_backend", "_id", BackendUser.class, "backend", GlobalVariables.CACHE_CONFIG);
		radioBackendCache.setWorkaround_keyMapper((key) -> new ObjectId(key));
		radioBackendCache.clear();

		radioRatingCache = new Caching<>("ms_radio_rating", "_id", PodcastRating.class, "rating", GlobalVariables.CACHE_CONFIG);
		radioRatingCache.setWorkaround_keyMapper((key) -> new ObjectId(key));
		radioRatingCache.clear();

		radioResourceFeedbackCache = new Caching<>("ms_radio_feedback", "_id", ResourceFeedback.class, "feedback", GlobalVariables.CACHE_CONFIG);
		radioResourceFeedbackCache.setWorkaround_keyMapper((key) -> new ObjectId(key));
		radioResourceFeedbackCache.clear();

		podcastReviewCache = new Caching<>("ms_podcast_review", "_id", PodcastReview.class, "review", GlobalVariables.CACHE_CONFIG);
		podcastReviewCache.setWorkaround_keyMapper((key) -> new ObjectId(key));
		podcastReviewCache.clear();

		if (radioScheduleCaching.find(new Document(), null, 0, -1, (res) -> res.get_id().toHexString(), true).size() == 0) {
			StaticScheduler.fillCache(radioScheduleCaching);
		}
	}
	
	public Resource get(String resourceId) {
		return radioResourcesCache.get(resourceId);
	}

	public void set(String resourceId, Resource resource) {
		radioResourcesCache.set(resourceId, resource);
	}
	
	public List<Resource> findResources(boolean approved, String userId) {
		List<Resource> result = new LinkedList<>();
		List<String> resourceIds;

		if (userId.length() > 0) {
			Map<String, Object> search = new HashMap<>();
			search.put("approved", approved);
			search.put("authorId", userId);

			resourceIds = radioResourcesCache.find(new Document(search), null, 0, -1, (resource) -> resource.get_id().toHexString(), true);
		} else {
			resourceIds = radioResourcesCache.find(new Document("approved", approved), null, 0, -1, (resource) -> resource.get_id().toHexString(), true);
		}

		for (String resourceId : resourceIds) {
			Resource res = radioResourcesCache.get(resourceId);
			if (res != null) {
				res.setPodcastRating(this.getPodcastRating(resourceId));
				res.setPodcastReviews(this.getReviewsForPodcast(resourceId));
				result.add(res);
			}
		}
		
		return result;
	}

	public List<Resource> getResourcesByProgram(final String program, final PodcastZoneFilter filter) {
		List<Resource> result = new LinkedList<>();

		Document regQuery = new Document();// /^bar$/i
		regQuery.append("$regex", "^(?)" + Pattern.quote(program) + "$");
		regQuery.append("$options", "i");

		Document filterDoc = new Document();
		filterDoc.put("approved", true);
		filterDoc.put("program", regQuery);

		List<String> resourceIds = radioResourcesCache.find(filterDoc, new Document("approvedDate", -1), 0, -1, (res) -> res.get_id().toHexString(), true);

		for (String id : resourceIds) {
			Resource res = this.get(id);
			if (res != null) {
				res.setPodcastRating(this.getPodcastRating(id));
				res.setPodcastReviews(this.getReviewsForPodcast(id));
				result.add(res);
			}
		}

		if (filter == PodcastZoneFilter.RATING)
			Collections.sort(result);

		return result;
	}

	public List<Resource> getResourcesWithFilter(PodcastZoneFilter filter) {
		List<Resource> results = new LinkedList<>();
		List<String> resourceIds;

		resourceIds = radioResourcesCache.find(new Document("approved", true), new Document("approvedDate", -1), 0, -1, (resource) -> resource.get_id().toHexString(), true);

		if (resourceIds.size() < 6) {
			for (int i = 0; i < resourceIds.size(); i++) {
				String id = resourceIds.get(i);
				Resource res = radioResourcesCache.get(id);
				res.setPodcastRating(this.getPodcastRating(id));
				res.setPodcastReviews(this.getReviewsForPodcast(id));
				results.add(res);
			}
		} else {
			for (int i = 0; i < 6; i++) {
				String id = resourceIds.get(i);
				Resource res = radioResourcesCache.get(id);
				res.setPodcastRating(this.getPodcastRating(id));
				res.setPodcastReviews(this.getReviewsForPodcast(id));
				results.add(res);
			}
		}

		if (filter == PodcastZoneFilter.RATING) {
			Collections.sort(results);
		}

		return results;
	}

	public boolean ratePodcast(final String userId, final String resourceId, final int rating) {
		List<String> ids = this.radioRatingCache.find(new Document("resourceId", resourceId), null, 0, -1, (res) -> res.get_id().toHexString(), true);

		// no ratings
		if (ids.size() == 0) {
			PodcastRating podcastRating = new PodcastRating(resourceId);
			podcastRating.addRating(rating, userId);
			this.radioRatingCache.set(podcastRating.get_id().toHexString(), podcastRating);
			return true;
		} else {
			PodcastRating podcastRating = this.radioRatingCache.get(ids.get(0));
			if (podcastRating != null) {
				podcastRating.addRating(rating, userId);
				this.radioRatingCache.set(ids.get(0), podcastRating);
				return true;
			}
		}

		return false;
	}

	public PodcastRating getPodcastRating(final String resourceId) {
		List<String> ids = this.radioRatingCache.find(new Document("resourceId", resourceId), null, 0, -1, (res) -> res.get_id().toHexString(), true);

		if (ids.size() > 0)
			return this.radioRatingCache.get(ids.get(0));

		return null;
	}

	public boolean approveContent(String resourceId) {
		Resource content = this.get(resourceId);

		if (content != null) {
			content.approved = true;
			content.approvedDate = Long.toString(System.currentTimeMillis());
			this.set(resourceId, content);
			return true;
		} else {
			return false;
		}
	}

	public boolean setFeedback(String resourceId, String comment) {
		Resource content = this.get(resourceId);

		if (content != null) {
			ResourceFeedback feedback = new ResourceFeedback();

			feedback.resourceId = resourceId;
			feedback.feedbackComment = comment;
			feedback.feedbackCreated = Long.toString(System.currentTimeMillis());

			radioResourceFeedbackCache.set(feedback.get_id().toHexString(), feedback);

			return true;
		}

		return false;
	}

	public boolean checkPassword(byte[] inputPassword) throws Exception {
		boolean result = false;
		/*
			First time login, no password was set.
			After login first time, please change the password in admin panel
		 */
		if (this.radioBackendCache.count() == 0) {
			if (new String(inputPassword).equals("radioadmin")) {
				BackendUser user = new BackendUser();
				user._pwd = BCrypt.hashpw(new String(inputPassword), BCrypt.gensalt());
				this.radioBackendCache.set(user.get_id().toHexString(), user);
				return true;
			}
		}

		List<String> backend = radioBackendCache.find(new Document(), null, 0, -1, (user) -> user.get_id().toHexString(), false);

		if (backend.size() > 0) {
			BackendUser user = radioBackendCache.get(backend.get(0));

			//String userHash = new String(user.get_pwd().getData());
			String userHash = user._pwd;

			// old versions starts with $2a, since the new and old are compatible we can just inject the hash
			// AS LONG AS THE PASSWORD IS < 255 CHARS
			// java does not support the $2b updated version yet?!?!
			if (userHash.startsWith("$2b")) {
				String inject = "$2a";
				inject += userHash.substring(3); // substring the first $2b
				userHash = inject;
			}

			result = BCrypt.checkpw(new String(inputPassword, "UTF-8"), userHash);
		}

		return result;
	}

	public boolean changePassword(ChangePassword password) {
		boolean result = false;

		try {
			byte[] currentPassword = Base64.getDecoder().decode(password.oldPassword);
			byte[] newPassword = Base64.getDecoder().decode(password.newPassword);
			byte[] newPasswordRepeat = Base64.getDecoder().decode(password.newPasswordRepeat);

			if (this.checkPassword(currentPassword)) {
				if (new String(newPassword).equals(new String(newPasswordRepeat))) {
					String newHash = BCrypt.hashpw(new String(newPassword), BCrypt.gensalt());
					List<String> ids = this.radioBackendCache.find(new Document(), null, 0, -1, (resource) -> resource.get_id().toHexString(), false);

					if (ids.size() > 0) {
						String id = ids.get(0);
						BackendUser user = this.radioBackendCache.get(id);

						if (user != null) {
							user._pwd = newHash;
							this.radioBackendCache.set(id, user);
							result = true;
						}
					}
				} else {
					result = false;
				}
			} else {
				result = false;
			}
		}
		catch (Exception ex) {
			logger().error("Change password exception");
			ex.printStackTrace();
			result = false;
		}

		return result;
	}

	public boolean sendFeedbackMessage(PodcastFeedback feedback, Cookie cookie) {
		boolean success = false;

		Client client = StimeyClient.create();
		Response response = StimeyClient.post(client, "chat", Global.DOCKER, "api/chat/start-chat-go/" + feedback.authorId, cookie, "");

		if (response != null && response.getStatus() == 200) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				String json = response.readEntity(String.class);
				Map<String, Object> map = mapper.readValue(json, new TypeReference<HashMap<String,Object>>(){});

				String chatTopic = map.get("topic").toString();

				UserMessageWrapper umw = new UserMessageWrapper();
				umw.message = feedback.feedback;
				umw.chatlisttopic = chatTopic;

				mapper = new ObjectMapper();
				String chatMessageJson = mapper.writer().writeValueAsString(umw);

				response = StimeyClient.post(client, "chat", Global.DOCKER, "api/chat/chat-messages-post/" + chatTopic, cookie, chatMessageJson);

				if (response != null && response.getStatus() == 200) {
					success = this.setFeedback(feedback.podcastId, feedback.feedback);
				}
			} catch (IOException e) {
				System.out.println(e);
			}
		}

		return success;
	}

	public ResourceFeedback getResourceFeedback(String resourceId) {
		ResourceFeedback feedback = null;
		Resource res = this.get(resourceId);

		if (res != null) {
			List<String> feedbackIds = radioResourceFeedbackCache.find(new Document("resourceId", resourceId), null, 0, -1, (resource) -> resource.get_id().toHexString(), true);

			if (feedbackIds.size() >= 1) {
				feedback = radioResourceFeedbackCache.get(feedbackIds.get(0));
			}
		}
		return feedback;
	}

	public NewCookie createCookie(String cookie, String userId, String username) {
		String hashCookie = BCrypt.hashpw(userId + "/" + username, BCrypt.gensalt());
		NewCookie authCookie = new NewCookie(cookie, hashCookie, "/", Global.DOMAIN, "", 120*60, false, true);
		return authCookie;
	}

	public boolean checkAdminRights(String value, UserSharedCacheObject user) {
		if (user.usertype.equals("radio"))
			return true;

		if (value.length() > 0) {
			String userValue = user.userid + "/" + user.username;

			if (BCrypt.checkpw(userValue, value))
				return true;
		}

		return false;
	}

	public boolean deletePodcast(String podcastId) {
		Resource resource = this.get(podcastId);

		List<PodcastReview> reviews = this.getReviewsForPodcast(resource.get_id().toHexString());

		if (reviews != null) {
			for (PodcastReview rev : reviews) {
				this.deletePodcastReview(rev.get_id().toHexString());
			}
		}

		PodcastRating rating = this.getPodcastRating(resource.get_id().toHexString());
		if (rating != null)
			this.radioRatingCache.del(rating.get_id().toHexString());


		if (resource != null) {
			return this.radioResourcesCache.del(resource.get_id().toHexString());
		}

		return false;
	}

	public void createPodcastReview(final String podcastId, final String userId, final String username, final String avatarImage, final String comment) {
		PodcastReview review = new PodcastReview(podcastId, userId, username, avatarImage, comment, Long.toString(System.currentTimeMillis()));
		this.podcastReviewCache.set(review.get_id().toHexString(), review);
	}

	public void deletePodcastReview(final String reviewId) {
		this.podcastReviewCache.del(reviewId);
	}

	public boolean isReviewOwner(final String reviewId, final String userId) {
		PodcastReview review = this.podcastReviewCache.get(reviewId);
		if (review != null)
			if (review.userId.equals(userId))
				return true;

		return false;
	}

	public List<PodcastReview> getReviewsForPodcast(final String podcastId) {
		List<PodcastReview> reviews = new LinkedList<>();

		List<String> ids = this.podcastReviewCache.find(new Document("podcastId", podcastId), null, 0, -1, (res) -> res.get_id().toHexString(), true);

		for (String id : ids) {
			PodcastReview review = this.podcastReviewCache.get(id);
			if (review != null)
				reviews.add(review);
		}

		return reviews;
	}

	public HashMap<Integer, List<RadioScheduleEntry>> getRadioSchedule() {
		HashMap<Integer, List<RadioScheduleEntry>> result = new HashMap<>();

		List<String> ids = this.radioScheduleCaching.find(null, null, 0, -1, (res) -> res.get_id().toHexString(), true);

		for (String id : ids) {
			RadioScheduleEntry entry = this.radioScheduleCaching.get(id);

			if (!result.containsKey(entry.day)) {
				result.put(entry.day, new ArrayList<>());
			}

			result.get(entry.day).add(entry);
		}

		// sort schedule with time
		for (List<RadioScheduleEntry> list : result.values()) {
			Collections.sort(list);
		}

		return result;
	}

	public boolean deleteEntryFromSchedule(String entryId) {
		return this.radioScheduleCaching.del(entryId);
	}

	public void replaceOrAddEntryToSchedule(RadioScheduleEntryDTO entry) {
		List<String> ids = this.radioScheduleCaching.find(new Document("day", entry.day).append("time", entry.time), null, 0, -1, (res) -> res.get_id().toHexString(), true);

		if (ids.size() > 0) {
			RadioScheduleEntry cacheEntry = this.radioScheduleCaching.get(ids.get(0));
			cacheEntry.title = entry.title;
			cacheEntry.description = entry.description;
			this.radioScheduleCaching.set(cacheEntry.get_id().toHexString(), cacheEntry);
		}

		if (ids.size() == 0) {
			RadioScheduleEntry cacheEntry = new RadioScheduleEntry(entry.day, entry.time, entry.title, entry.description);
			this.radioScheduleCaching.set(cacheEntry.get_id().toHexString(), cacheEntry);
		}
	}
}
