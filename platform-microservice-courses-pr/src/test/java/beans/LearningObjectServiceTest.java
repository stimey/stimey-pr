package beans;

import com.mongodb.MongoClient;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.multimedia.Assignment;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.multimedia.Task;
import eu.stimey.platform.microservice.courses.databind.learningObject.composite.Challenge;
import eu.stimey.platform.microservice.courses.startup.Global;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class LearningObjectServiceTest {

	private static LearningObjectsService learningObjectsService;

	private static List<LearningObject> testLearningObjects() {
		List<LearningObject> testLearningObjects = new LinkedList<>();
		testLearningObjects.add(new Task());
		testLearningObjects.add(new Task());
		testLearningObjects.add(new Assignment());
		testLearningObjects.add(new Challenge());
		testLearningObjects.add(new Challenge());
		testLearningObjects.add(new Challenge());
		return testLearningObjects;
	}

	private static List<String> getLearningObjectIds(List<LearningObject> learningObjects) {
		List<String> ids = new LinkedList<>();
		learningObjects.forEach(learningObject -> ids.add(learningObject.get_id()));
		return ids;
	}

	@BeforeClass
	public static void beforeAllTests() {
		Global.USE_TEST_DB = true;
		learningObjectsService = new LearningObjectsService();
	}

	@AfterClass
	public static void afterAllTests() {
		MongoClient client = MongoDBService.getService().getClient();
		String databaseName = MongoDBService.getService().getDatabaseName();
		client.getDatabase(databaseName).drop();
		Global.USE_TEST_DB = false;
	}

	@Test
	public void canCalculateTypesOfLearningObject() {
		List<LearningObject> testLearningObjects = testLearningObjects();
		learningObjectsService.setLearningObjects(testLearningObjects);
		Triple<Integer, Integer, Integer> typesOfLearningObjects = learningObjectsService
				.typesOfLearningObjects(getLearningObjectIds(testLearningObjects));
		assertEquals(2, (int) typesOfLearningObjects.getLeft());
		assertEquals(1, (int) typesOfLearningObjects.getMiddle());
		assertEquals(3, (int) typesOfLearningObjects.getRight());
	}

}
