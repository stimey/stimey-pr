package beans;

import com.mongodb.MongoClient;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.microservice.courses.beans.GlobalVariables;
import eu.stimey.platform.microservice.courses.beans.TeacherService;
import eu.stimey.platform.microservice.courses.databind.mission.Mission;
import eu.stimey.platform.microservice.courses.databind.user.Teacher;
import eu.stimey.platform.microservice.courses.startup.Global;
import org.bson.types.ObjectId;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TeacherServiceTest {

	private static TeacherService teacherService;

	private static final Teacher testTeacher = new Teacher(GlobalVariables.TEST_TEACHER_ID.toHexString());

	private static final Mission testMission = new Mission();

	@Before
	public void beforeEachTest() {
		teacherService.setTeacher(testTeacher);
	}

	@BeforeClass
	public static void beforeAllTests() {
		Global.USE_TEST_DB = true;
		teacherService = new TeacherService();
	}

	@AfterClass
	public static void afterAllTests() {
		MongoClient client = MongoDBService.getService().getClient();
		String databaseName = MongoDBService.getService().getDatabaseName();
		client.getDatabase(databaseName).drop();
		Global.USE_TEST_DB = false;
	}

	@Test
	public void canAddMissionToSavedMissions() {
		teacherService.addMissionToSavedMissions(testTeacher.get_id(), testMission.get_id());
		Teacher updatedTeacher = teacherService.getTeacher(testTeacher.get_id());
		assertTrue(updatedTeacher.savedMissions.contains(testMission.get_id()));
	}

	@Test
	public void canPublishMission() {
		Teacher teacher = testTeacher;
		teacher.savedMissions.add(testMission.get_id());
		teacherService.setTeacher(teacher);
		teacherService.publishMission(testTeacher.get_id(), testMission.get_id());
		Teacher updatedTeacher = teacherService.getTeacher(testTeacher.get_id());
		assertTrue(!updatedTeacher.savedMissions.contains(testMission.get_id()));
		assertTrue(updatedTeacher.activeMissions.contains(testMission.get_id()));
	}

	@Test
	public void addsTeacherIfNotExists() {
		assertNotNull(teacherService.getTeacher(GlobalVariables.ARCHIMEDIS_ID.toHexString()));
	}
}
