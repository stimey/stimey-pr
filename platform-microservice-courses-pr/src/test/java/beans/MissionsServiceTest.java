package beans;

import com.mongodb.MongoClient;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.microservice.courses.beans.MissionsService;
import eu.stimey.platform.microservice.courses.beans.filter.MissionsFilter;
import eu.stimey.platform.microservice.courses.databind.mission.Mission;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.Difficulty;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.Stage;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.Topic;
import eu.stimey.platform.microservice.courses.startup.Global;
import org.junit.*;

import java.util.List;

import static org.junit.Assert.*;

public class MissionsServiceTest {

	private static MissionsService missionsService;

	@BeforeClass
	public static void beforeAllTests() {
		Global.USE_TEST_DB = true;
		missionsService = new MissionsService();
	}

	@After
	public void afterEachClass() {
		missionsService.getMissionDataAccess().clear();
	}

	@AfterClass
	public static void afterAllTests() {
		MongoClient client = MongoDBService.getService().getClient();
		String databaseName = MongoDBService.getService().getDatabaseName();
		client.getDatabase(databaseName).drop();
		Global.USE_TEST_DB = false;
	}

	@Test
	public void canFindMissionBySearchString() {
		// Test to find mission by title
		final String testTitle = "Eat it or Leave it";
		Mission testMission = new Mission();
		testMission.missionMetadata.generalMetadata.title = "Eat it or Leave it";
		missionsService.setMission(testMission);
		MissionsFilter missionsFilter = new MissionsFilter();
		missionsFilter.query("eat");
		List<Mission> missions = missionsService.findMissions(0, 0, missionsFilter.toDocument());
		assertTrue(missions.size() > 0);
		missions.forEach(mission -> assertEquals(mission.missionMetadata.generalMetadata.title, testTitle));

		missionsService.delMission(testMission.get_id());

		Mission testMission2 = new Mission();
		testMission2.missionMetadata.generalMetadata.keywords.add("eat");
		missionsService.setMission(testMission2);
		missions = missionsService.findMissions(0, 0, missionsFilter.toDocument());
		assertTrue(missions.size() > 0);
		missions.forEach(mission -> assertTrue(mission.missionMetadata.generalMetadata.keywords.contains("eat")));
	}

	@Test
	public void canFindMissionsByMultipleStages() {
		Mission mission = new Mission();
		mission.missionMetadata.educationalMetadata.stage = Stage.LOWER;
		Mission mission2 = new Mission();
		mission2.missionMetadata.educationalMetadata.stage = Stage.PRIMARY;
		Mission mission3 = new Mission();
		mission.missionMetadata.educationalMetadata.stage = Stage.UPPER;
		missionsService.setMission(mission);
		missionsService.setMission(mission2);
		missionsService.setMission(mission3);
		MissionsFilter missionsFilter = new MissionsFilter();
		List<Mission> missions = missionsService.findMissions(0, 0, missionsFilter
				.stages(Stage.LOWER.getText(), Stage.PRIMARY.getText(), Stage.UPPER.getText()).toDocument());
		assertTrue(missions.size() >= 2);
		missions.forEach(mission1 -> assertTrue(mission1.missionMetadata.educationalMetadata.stage.equals(Stage.LOWER)
				|| mission1.missionMetadata.educationalMetadata.stage.equals(Stage.PRIMARY)
				|| mission1.missionMetadata.educationalMetadata.stage.equals(Stage.UPPER)));
	}

	@Test
	public void canFindMissionsByTopics() {
		Mission mission = new Mission();
		mission.missionMetadata.educationalMetadata.topics.add(Topic.SCIENCE);
		mission.missionMetadata.educationalMetadata.topics.add(Topic.ENGINEERING);
		Mission mission2 = new Mission();
		mission2.missionMetadata.educationalMetadata.topics.add(Topic.SCIENCE);
		missionsService.setMission(mission);
		missionsService.setMission(mission2);
		MissionsFilter missionsFilter = new MissionsFilter();
		List<Mission> missions = missionsService.findMissions(0, 0, missionsFilter.topics("Science").toDocument());
		// assertTrue(missions.size() == 2);
		missions.forEach(
				mission1 -> assertTrue(mission1.missionMetadata.educationalMetadata.topics.contains(Topic.SCIENCE)));
	}

	@Test
	public void canFindMissionsByDifficulty() {
		Mission mission = new Mission();
		mission.missionMetadata.educationalMetadata.difficulty = Difficulty.BASIC;
		Mission mission2 = new Mission();
		mission2.missionMetadata.educationalMetadata.difficulty = Difficulty.ADVANCED;
		missionsService.setMission(mission);
		missionsService.setMission(mission2);
		MissionsFilter missionsFilter = new MissionsFilter();
		List<Mission> missions = missionsService.findMissions(0, 0,
				missionsFilter.difficulties(Difficulty.BASIC.getText(), Difficulty.ADVANCED.getText()).toDocument());
		missions.forEach(
				mission1 -> assertTrue(mission1.missionMetadata.educationalMetadata.difficulty.equals(Difficulty.BASIC)
						|| mission1.missionMetadata.educationalMetadata.difficulty.equals(Difficulty.ADVANCED)));
	}

	@Test
	public void canFindMissionsByTopicsAndKeywords() {
		Mission mission = new Mission();
		mission.missionMetadata.generalMetadata.keywords.add("test1");
		mission.missionMetadata.generalMetadata.keywords.add("test2");
		mission.missionMetadata.generalMetadata.keywords.add("test3");
		mission.missionMetadata.educationalMetadata.topics.add(Topic.SCIENCE);
		mission.missionMetadata.educationalMetadata.topics.add(Topic.ENGINEERING);
		missionsService.setMission(mission);
		MissionsFilter missionsFilter = new MissionsFilter();
		List<Mission> missions = missionsService.findMissions(0, 0,
				missionsFilter.keywords("test1", "test2", "test3").topics("Science").toDocument());
		assertTrue(missions.contains(mission));
	}

	@Test
	public void canFindMissions() {
		Mission mission = new Mission();
		mission.missionMetadata.generalMetadata.keywords.add("test1");
		mission.missionMetadata.generalMetadata.keywords.add("test2");
		mission.missionMetadata.generalMetadata.keywords.add("test3");
		mission.missionMetadata.educationalMetadata.difficulty = Difficulty.BASIC;
		mission.missionMetadata.educationalMetadata.topics.add(Topic.SCIENCE);
		mission.missionMetadata.educationalMetadata.topics.add(Topic.ENGINEERING);
		missionsService.setMission(mission);
		MissionsFilter missionsFilter = new MissionsFilter();
		List<Mission> missions = missionsService.findMissions(0, 0, missionsFilter.keywords("test1", "test2", "test3")
				.topics("Science").difficulties("Basic").toDocument());
		assertTrue(missions.contains(mission));
	}

	@Test
	public void canFindMissionsWithAllParameters() {
		Mission mission = new Mission();
		mission.missionMetadata.generalMetadata.keywords.add("test1");
		mission.missionMetadata.generalMetadata.keywords.add("test2");
		mission.missionMetadata.generalMetadata.keywords.add("test3");
		mission.missionMetadata.educationalMetadata.stage = Stage.UPPER;
		mission.missionMetadata.educationalMetadata.topics.add(Topic.SCIENCE);
		mission.missionMetadata.educationalMetadata.topics.add(Topic.ENGINEERING);
		mission.missionMetadata.educationalMetadata.difficulty = Difficulty.BASIC;
		missionsService.setMission(mission);
		MissionsFilter missionsFilter = new MissionsFilter();
		List<Mission> missions = missionsService.findMissions(0, 0, missionsFilter.keywords("test1")
				.stages("Upper Secondary").topics("Science").difficulties("Basic").toDocument());
		assertTrue(missions.contains(mission));
	}

	@Ignore
	@Test
	public void canFindMissionByInputString() {
		Mission mission = new Mission();
		mission.missionMetadata.generalMetadata.title = "Test Mission";
		/*
		 * mission.missionMetadata.generalMetadata.keywords.add("test1");
		 * mission.missionMetadata.generalMetadata.keywords.add("test2");
		 * mission.missionMetadata.generalMetadata.keywords.add("test3");
		 * mission.missionMetadata.educationalMetadata.stage = Stage.UPPER;
		 * mission.missionMetadata.educationalMetadata.topics.add(Topic.SCIENCE);
		 * mission.missionMetadata.educationalMetadata.topics.add(Topic.BIOLOGY);
		 * mission.missionMetadata.educationalMetadata.difficulty = Difficulty.BASIC;
		 */
		missionsService.setMission(mission);

		List<Mission> missions = missionsService.findMissions(0, 0, MissionsFilter.matchTitle("test"));
		assertTrue(missions.contains(mission));
	}

	@Test
	public void canDeleteMission() {
		Mission mission = new Mission();
		missionsService.setMission(mission);
		missionsService.delMission(mission.get_id());
		assertNull(missionsService.getMissionById(mission.get_id()));
	}

}
