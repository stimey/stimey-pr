package beans;

import com.mongodb.MongoClient;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.microservice.courses.beans.GlobalVariables;
import eu.stimey.platform.microservice.courses.beans.MissionToAuthorNamesMapper;
import eu.stimey.platform.microservice.courses.beans.MissionsService;
import eu.stimey.platform.microservice.courses.databind.mission.Mission;
import eu.stimey.platform.microservice.courses.startup.Global;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class MissionToAuthorNamesMapperTest {

	private static MissionsService missionsService;
	private static UserSharedService userSharedService;
	private static MissionToAuthorNamesMapper missionToAuthorNamesMapper;

	@BeforeClass
	public static void beforeAllTests() {
		Global.USE_TEST_DB = true;
		missionsService = new MissionsService();
		userSharedService = new UserSharedService();
		missionToAuthorNamesMapper = new MissionToAuthorNamesMapper(missionsService, userSharedService);
	}

	@After
	public void afterEach() {
		missionsService.getMissionDataAccess().clear();
	}

	@AfterClass
	public static void afterAllTests() {
		MongoClient client = MongoDBService.getService().getClient();
		String databaseName = MongoDBService.getService().getDatabaseName();
		client.getDatabase(databaseName).drop();
		Global.USE_TEST_DB = false;
	}

	@Test
	public void mapsMissionIdsToAuthorNames() {
		Mission missionOne = new Mission();
		missionOne.missionMetadata.generalMetadata.author = GlobalVariables.TEST_TEACHER_ID.toHexString();

		Mission missionTwo = new Mission();
		missionTwo.missionMetadata.generalMetadata.author = GlobalVariables.ARCHIMEDIS_ID.toHexString();

		missionsService.setMission(missionOne);
		missionsService.setMission(missionTwo);

		List<String> missionIds = new LinkedList<>();
		missionIds.add(missionOne.get_id());
		missionIds.add(missionTwo.get_id());

		assertTrue(missionToAuthorNamesMapper.mapMissionIdsToAuthorName(missionIds).values()
				.contains(userSharedService.getUser(GlobalVariables.TEST_TEACHER_ID.toHexString()).username));
		assertTrue(missionToAuthorNamesMapper.mapMissionIdsToAuthorName(missionIds).values()
				.contains(userSharedService.getUser(GlobalVariables.ARCHIMEDIS_ID.toHexString()).username));

	}
}
