package beans;

import com.mongodb.MongoClient;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.microservice.courses.beans.MongoDBDataAccess;
import eu.stimey.platform.microservice.courses.databind.DocumentEntity;
import eu.stimey.platform.microservice.courses.startup.Global;
import junit.framework.TestCase;
import org.bson.Document;
import org.junit.*;

import java.util.List;

import static junit.framework.TestCase.*;

public class MongoDBDataAccessTest {

	private static MongoDBDataAccess<DocumentEntity> documentEntityMongoDBDataAccess;

	@BeforeClass
	public static void beforeClass() {
		Global.USE_TEST_DB = true;
		documentEntityMongoDBDataAccess = new MongoDBDataAccess<>("documentEntity", DocumentEntity.class,
				"testDocumentEntities");
		for (int i = 0; i < 15; i++) {
			documentEntityMongoDBDataAccess.set(new DocumentEntity());
		}
	}

	@After
	public void afterEach() {
		documentEntityMongoDBDataAccess.clear();
	}

	@AfterClass
	public static void afterAllTests() {
		MongoClient client = MongoDBService.getService().getClient();
		String databaseName = MongoDBService.getService().getDatabaseName();
		client.getDatabase(databaseName).drop();
		Global.USE_TEST_DB = false;
	}

	@Test
	public void add_AddsEntityToDatabase() {
		DocumentEntity documentEntity = new DocumentEntity();
		documentEntityMongoDBDataAccess.set(documentEntity);
		assertNotNull(documentEntityMongoDBDataAccess.get(documentEntity.get_id()));
	}

	@Test
	public void findTest() {
		List<DocumentEntity> entities = documentEntityMongoDBDataAccess.find(10, 0, new Document());
		assertEquals(10, entities.size());
		entities.forEach(TestCase::assertNotNull);
		List<DocumentEntity> otherEntites = documentEntityMongoDBDataAccess.find(10, 1, new Document());
		assertEquals(10, entities.size());
		entities.forEach(TestCase::assertNotNull);
		assertNotSame(entities.get(0), otherEntites.get(0));
		assertNotSame(entities.get(9), otherEntites.get(9));
	}

	@Ignore
	@Test
	public void secondFind_isFasterThan_firstFind() {
		long start = System.currentTimeMillis();
		List<DocumentEntity> searchResult = documentEntityMongoDBDataAccess.find(15, 1, new Document());
		long end = System.currentTimeMillis();
		assertEquals(15, searchResult.size());
		long timeElapsed1 = end - start;
		long start2 = System.currentTimeMillis();
		List<DocumentEntity> searchResult2 = documentEntityMongoDBDataAccess.find(15, 1, new Document());
		long end2 = System.currentTimeMillis();
		assertEquals(15, searchResult2.size());
		long timeElapsed2 = end2 - start2;
		assertTrue(timeElapsed2 < timeElapsed1);
	}

}
