package beans;

import com.mongodb.MongoClient;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.microservice.courses.beans.MissionsService;
import eu.stimey.platform.microservice.courses.beans.StudentService;
import eu.stimey.platform.microservice.courses.databind.mission.Mission;
import eu.stimey.platform.microservice.courses.databind.user.MissionState;
import eu.stimey.platform.microservice.courses.databind.user.Student;
import eu.stimey.platform.microservice.courses.startup.Global;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StudentServiceTest {

	private static StudentService studentService;
	private static MissionsService missionsService;

	@BeforeClass
	public static void beforeAllTests() {
		Global.USE_TEST_DB = true;
		missionsService = new MissionsService();
		studentService = new StudentService(missionsService);
	}

	@AfterClass
	public static void afterAllTests() {
		MongoClient client = MongoDBService.getService().getClient();
		String databaseName = MongoDBService.getService().getDatabaseName();
		client.getDatabase(databaseName).drop();
		Global.USE_TEST_DB = false;
	}

	@After
	public void afterEachTest() {
		studentService.clear();
		missionsService.getMissionDataAccess().clear();
	}

	@Test
	public void canEnrollStudentInAMission() {
		Mission mission = new Mission();
		missionsService.setMission(mission);

		Student student = new Student();
		studentService.setStudent(student);

		studentService.enroll(student.get_id(), mission.get_id());

		Mission updatedMission = missionsService.getMissionById(mission.get_id());
		assertTrue(updatedMission.students.contains(student.get_id()));
	}
}
