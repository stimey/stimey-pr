import beans.*;
import databind.learningObject.LearningObjectTest;
import databind.question.QuestionTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ KhanAcademyVideoServiceTest.class, LearningObjectServiceTest.class, MissionsServiceTest.class,
		MissionToAuthorNamesMapperTest.class, MongoDBDataAccessTest.class, TeacherServiceTest.class,
		StudentServiceTest.class, LearningObjectTest.class, QuestionTest.class })
public class AllTests {
}
