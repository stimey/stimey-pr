package databind.question;

import eu.stimey.platform.microservice.courses.databind.challengeResult.MultipleChoiceStudentInput;
import eu.stimey.platform.microservice.courses.databind.challengeResult.OneChoiceStudentInput;
import eu.stimey.platform.microservice.courses.databind.challengeResult.TrueFalseStudentInput;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question.Answer;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question.MultipleChoiceQuestion;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question.OneChoiceQuestion;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question.TrueFalseQuestion;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class QuestionTest {

	@Test
	public void MultipleChoiceQuestion_CheckWithCorrectAnswer_Returns_True() {
		Answer falseAnswer = new Answer();
		Answer correctAnswer = new Answer("B", "", true);
		MultipleChoiceQuestion checkableQuestion = new MultipleChoiceQuestion();
		checkableQuestion.answers.add(falseAnswer);
		checkableQuestion.answers.add(correctAnswer);
		List<String> studentInput = new ArrayList<>();
		studentInput.add(correctAnswer.answerText);
		MultipleChoiceStudentInput multipleChoiceStudentInput = new MultipleChoiceStudentInput(studentInput);
		assertTrue(checkableQuestion.check(multipleChoiceStudentInput));
	}

	@Test
	public void MultipleChoiceQuestion_CheckWithFalseAnswer_Returns_False() {
		Answer falseAnswer = new Answer();
		Answer correctAnswer = new Answer("B", "", true);
		MultipleChoiceQuestion checkableQuestion = new MultipleChoiceQuestion();
		checkableQuestion.answers.add(falseAnswer);
		checkableQuestion.answers.add(correctAnswer);
		List<String> studentInput = new ArrayList<>();
		studentInput.add(falseAnswer.answerText);
		MultipleChoiceStudentInput multipleChoiceStudentInput = new MultipleChoiceStudentInput(studentInput);
		assertFalse(checkableQuestion.check(multipleChoiceStudentInput));
	}

	@Test
	public void OneChoiceQuestion_CheckWithCorrectAnswer_Returns_True() {
		Answer falseAnswer = new Answer();
		Answer correctAnswer = new Answer("B", "", true);
		OneChoiceQuestion checkableQuestion = new OneChoiceQuestion();
		checkableQuestion.answers.add(falseAnswer);
		checkableQuestion.answers.add(correctAnswer);
		OneChoiceStudentInput oneChoiceStudentInput = new OneChoiceStudentInput(correctAnswer.answerText);
		assertTrue(checkableQuestion.check(oneChoiceStudentInput));
	}

	@Test
	public void OneChoiceQuestion_CheckWithFalseAnswer_Returns_False() {
		Answer falseAnswer = new Answer();
		Answer correctAnswer = new Answer("B", "", true);
		OneChoiceQuestion checkableQuestion = new OneChoiceQuestion();
		checkableQuestion.answers.add(falseAnswer);
		checkableQuestion.answers.add(correctAnswer);
		OneChoiceStudentInput multipleChoiceStudentInput = new OneChoiceStudentInput(falseAnswer.answerText);
		assertFalse(checkableQuestion.check(multipleChoiceStudentInput));
	}

	@Test
	public void TrueFalseQuestion_CheckWithCorrectAnswer_Returns_True() {
		Answer answer = new Answer("B", "", true);
		TrueFalseQuestion checkableQuestion = new TrueFalseQuestion();
		checkableQuestion.answer = answer;
		TrueFalseStudentInput trueFalseStudentInput = new TrueFalseStudentInput(true);
		assertTrue(checkableQuestion.check(trueFalseStudentInput));
	}

	@Test
	public void TrueFalseQuestion_CheckWithFalseAnswer_Returns_False() {
		Answer answer = new Answer("B", "", true);
		TrueFalseQuestion checkableQuestion = new TrueFalseQuestion();
		checkableQuestion.answer = answer;
		TrueFalseStudentInput trueFalseStudentInput = new TrueFalseStudentInput(false);
		assertFalse(checkableQuestion.check(trueFalseStudentInput));
	}

}
