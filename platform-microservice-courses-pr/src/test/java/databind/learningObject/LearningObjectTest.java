package databind.learningObject;

import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.composite.Challenge;
import org.junit.Test;

import static org.junit.Assert.*;

public class LearningObjectTest {

	private static LearningObject getTestLearningObject() {
		return new Challenge();
	}

	@Test
	public void canBeSelfpaced() {
		LearningObject testLearningObject = getTestLearningObject();
		testLearningObject.setSelfPaced(true);
		assertTrue(testLearningObject.isSelfPaced());
		assertNull(testLearningObject.endDate);
		assertNull(testLearningObject.startDate);
	}

	@Test
	public void canBeInstructorLed() {
		LearningObject testLearningObject = getTestLearningObject();
		testLearningObject.setSelfPaced(false);
		assertFalse(testLearningObject.isSelfPaced());
		assertEquals(testLearningObject.suggestedTime, -1, 0.0);
	}

}
