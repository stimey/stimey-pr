package eu.stimey.platform.microservice.courses.databind.challengeResult;

import java.util.List;

public class MultipleChoiceStudentInput extends StudentInput {

	public final List<String> studentInput;

	public MultipleChoiceStudentInput() {
		this(null);
	}

	public MultipleChoiceStudentInput(List<String> studentInput) {
		super();
		this.studentInput = studentInput;
	}

	@Override
	public String toString() {
		return "MultipleChoiceStudentInput{" + "studentInput=" + studentInput + '}';
	}

	@Override
	public List<String> studentInput() {
		return studentInput;
	}
}
