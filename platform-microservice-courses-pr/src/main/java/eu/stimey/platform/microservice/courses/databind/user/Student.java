package eu.stimey.platform.microservice.courses.databind.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.stimey.platform.microservice.courses.databind.Entity;
import eu.stimey.platform.microservice.courses.databind.mission.Mission;
import org.bson.types.ObjectId;

import java.util.*;

public class Student implements Entity {
	public String userId;
	public final Map<String, MissionState> completedMissions;
	public final Map<String, MissionState> activeMissions;

	public Student() {
		this.activeMissions = new HashMap<>();
		this.completedMissions = new HashMap<>();
		this.userId = new ObjectId().toHexString();
	}

	public Student(String userId) {
		this.userId = userId;
		this.activeMissions = new HashMap<>();
		this.completedMissions = new HashMap<>();
	}

	public void enroll(Mission mission) {
		if (mission == null)
			throw new IllegalArgumentException("missionId is null or empty");
		if (this.activeMissions.containsKey(mission.get_id()))
			throw new IllegalStateException("student has already enrolled mission with id=" + mission.get_id());
		MissionState missionState = new MissionState(mission.getMissionLength() /*- 1*/);
		if (missionState.isMissionFinished()) {
			this.completedMissions.put(mission.get_id(), missionState);
		} else {
			this.activeMissions.put(mission.get_id(), missionState);
		}
	}

	public boolean isEnrolled(String missionId) {
		if (missionId == null || missionId.isEmpty())
			throw new IllegalArgumentException("missionId is null or empty");
		return this.activeMissions.containsKey(missionId);
	}

	public boolean hasEnrolled(String missionId) {
		if (missionId == null || missionId.isEmpty())
			throw new IllegalArgumentException("missionId is null or empty");
		return this.completedMissions.containsKey(missionId) || this.isEnrolled(missionId);
	}

	public List<String> activeMissions() {
		return new ArrayList<>(activeMissions.keySet());
	}

	public List<String> completedMissions() {
		return new ArrayList<>(completedMissions.keySet());
	}

	@JsonIgnore
	public void removeNotExistingMissions(List<String> missionIds) {
		this.completedMissions.keySet().removeAll(missionIds);
		this.activeMissions.keySet().removeAll(missionIds);
	}

	@Override
	public String get_id() {
		return this.userId;
	}

	@Override
	public void set_id(String _id) {
		this.userId = _id;
	}

	@Override
	public String toString() {
		return "Student{" + "userId='" + userId + '\'' + ", completedMissions=" + completedMissions
				+ ", activeMissions=" + activeMissions + '}';
	}
}
