package eu.stimey.platform.microservice.courses.databind.learningObject;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.stimey.platform.microservice.courses.databind.Copyable;
import eu.stimey.platform.microservice.courses.databind.DocumentEntity;
import eu.stimey.platform.microservice.courses.databind.robotActions.LearningObjectRobotActions;
import eu.stimey.platform.microservice.courses.databind.robotActions.RobotActions;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.AtomicLearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.composite.CompositeLearningObject;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.Reward;
import eu.stimey.platform.microservice.courses.utils.DateDeserializer;
import eu.stimey.platform.microservice.courses.utils.DateSerializer;
import org.bson.types.ObjectId;

import java.util.*;

@JsonTypeInfo(use = Id.NAME, property = "type")
@JsonSubTypes({ @Type(value = AtomicLearningObject.class, name = "AtomicLearningObject"),
		@Type(value = CompositeLearningObject.class, name = "CompositeLearningObject") })
public abstract class LearningObject extends DocumentEntity implements Copyable {

	public String title;
	public String introduction;
	public Reward reward;
	public Date startDate;
	public Date endDate;
	public double suggestedTime;
	public int progressiveInquiryModelPhase;

	public List<String> resources;
	public List<String> discussions;

	private boolean isSelfPaced;

	public RobotActions robotActions;

	public LearningObject() {
		this("edit title", null, new Reward(), null, null);
	}

	public LearningObject(String title, String introduction) {
		this(title, introduction, new Reward(), null, null);
	}

	public LearningObject(String title, String introduction, Reward reward) {
		this(title, introduction, reward, null, null);
	}

	public LearningObject(String title, String introduction, Reward reward, Date startDate, Date endDate) {
		this.title = title;
		this.introduction = introduction;
		this.reward = reward;
		this.startDate = startDate;
		this.endDate = endDate;
		this.progressiveInquiryModelPhase = -1;
		this.isSelfPaced = false;
		this.suggestedTime = -1;
		this.resources = new ArrayList<>();
		this.discussions = new ArrayList<>();
		this.robotActions = new LearningObjectRobotActions();
	}

	public LearningObject(LearningObject original) {
		this(original.title, original.introduction, original.reward, original.startDate, original.endDate);
		this.set_id(new ObjectId().toHexString());
		this.progressiveInquiryModelPhase = original.progressiveInquiryModelPhase;
	}

	public boolean isSelfPaced() {
		return this.isSelfPaced;
	}

	public void setSelfPaced(boolean selfPaced) {
		this.isSelfPaced = selfPaced;
		if (this.isSelfPaced) {
			this.startDate = null;
			this.endDate = null;
			// this.suggestedTime = 0;
		} else {
			this.suggestedTime = -1;
		}
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getStartDate() {
		return this.startDate;
	}

	@JsonDeserialize(using = DateDeserializer.class)
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getEndDate() {
		return this.endDate;
	}

	@JsonDeserialize(using = DateDeserializer.class)
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
