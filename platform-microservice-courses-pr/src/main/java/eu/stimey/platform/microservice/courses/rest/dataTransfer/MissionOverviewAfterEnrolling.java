package eu.stimey.platform.microservice.courses.rest.dataTransfer;

import eu.stimey.platform.microservice.courses.databind.Discussion;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.Level;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.MissionMetadata;

import java.util.List;
import java.util.Map;

public class MissionOverviewAfterEnrolling {

	private MissionMetadata missionMetadata;
	private List<Level> levels;
	private List<LearningObject> learningObjects;

	private List<Discussion> discussions;
	private String statusUpdateAsJsonString;
	// private Map<String, StatusUpdate> idToStatusUpdateMapping;

	private Map<String, List<String>> learningObjectTitleToResourceIdsMapping;

}
