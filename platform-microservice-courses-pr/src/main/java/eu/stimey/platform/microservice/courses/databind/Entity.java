package eu.stimey.platform.microservice.courses.databind;

public interface Entity {
	String get_id();

	void set_id(String _id);
}
