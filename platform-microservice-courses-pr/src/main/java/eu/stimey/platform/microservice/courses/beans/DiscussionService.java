package eu.stimey.platform.microservice.courses.beans;

import eu.stimey.platform.microservice.courses.databind.Discussion;

import java.util.ArrayList;
import java.util.List;

public class DiscussionService {

	private final MongoDBDataAccess<Discussion> discussionMongoDBDataAccess;

	public DiscussionService() {
		discussionMongoDBDataAccess = new MongoDBDataAccess<>("ms_courses_discussions", Discussion.class, "discussions",
				"_id", true);
	}

	public Discussion getById(String id) {
		return this.discussionMongoDBDataAccess.get(id);
	}

	public List<Discussion> getByIds(List<String> ids) {
		List<Discussion> result = new ArrayList<>();
		for (String id : ids) {
			Discussion discussion = getById(id);
			if (discussion != null)
				result.add(discussion);
		}
		return result;
	}

	public void setDiscussion(Discussion discussion) {
		this.discussionMongoDBDataAccess.set(discussion);
	}

	public void deleteDiscussion(String dicussionId) {
		this.discussionMongoDBDataAccess.delete(dicussionId);
	}
}
