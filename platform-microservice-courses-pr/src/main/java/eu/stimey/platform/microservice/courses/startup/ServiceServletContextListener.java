package eu.stimey.platform.microservice.courses.startup;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.utils.StimeyLogger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Global web application settings
 * 
 */
@WebListener
public class ServiceServletContextListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		Global.startup();
		StimeyLogger.setApplicationName(servletContextEvent.getServletContext().getContextPath());
		
		String databaseName = "ms_courses";
		if (Global.USE_TEST_DB)
			databaseName = "test";
		MongoDBService.start(databaseName);
		RedisAMQPService.start();
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		RedisAMQPService.stop();
		MongoDBService.stop();
	}
}
