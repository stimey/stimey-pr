package eu.stimey.platform.microservice.courses.beans;

import eu.stimey.platform.microservice.courses.databind.challengeResult.StudentInput;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question.Checkable;

public class ChallengeChecker {

	public boolean checkQuestion(Checkable checkable, StudentInput studentInput) {
		return checkable.check(studentInput);
	}
}
