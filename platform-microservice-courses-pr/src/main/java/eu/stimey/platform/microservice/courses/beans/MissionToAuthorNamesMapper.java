package eu.stimey.platform.microservice.courses.beans;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;
import eu.stimey.platform.microservice.courses.databind.mission.Mission;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MissionToAuthorNamesMapper {

	private final MissionsService missionsService;
	private final UserSharedService userSharedService;

	public MissionToAuthorNamesMapper(MissionsService missionsService, UserSharedService userSharedService) {
		this.missionsService = missionsService;
		this.userSharedService = userSharedService;
	}

	public Map<Mission, String> mapMissionIdsToAuthorName(List<String> missionIds) {
		return mapMissionsToAuthorName(this.missionsService.getMissionsByIds(missionIds));
	}

	public Map<Mission, String> mapMissionsToAuthorName(List<Mission> missions) {
		Map<Mission, String> missionToTeacherNameMapping = new HashMap<>();
		if (missions == null || missions.isEmpty())
			return missionToTeacherNameMapping;

		for (Mission mission : missions) {
			missionToTeacherNameMapping.put(mission,
					userSharedService.getUser(mission.missionMetadata.generalMetadata.author).username);
		}
		return missionToTeacherNameMapping;
	}

	public String getAuthorNameOfMission(Mission mission) {
		final String authorId = mission.missionMetadata.generalMetadata.author;
		UserSharedCacheObject userSharedCacheObject = userSharedService.getUser(authorId);
		if (userSharedCacheObject != null)
			return userSharedCacheObject.username;
		else
			return "not found";
	}
}
