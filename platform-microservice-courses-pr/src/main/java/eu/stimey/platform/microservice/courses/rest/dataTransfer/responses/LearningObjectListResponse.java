package eu.stimey.platform.microservice.courses.rest.dataTransfer.responses;

import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;

import java.util.List;

public class LearningObjectListResponse {
	public final List<LearningObject> learningObjects;

	public LearningObjectListResponse(List<LearningObject> learningObjects) {
		this.learningObjects = learningObjects;
	}
}
