package eu.stimey.platform.microservice.courses.databind.mission.missionMetadata;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.stimey.platform.microservice.courses.utils.DateDeserializer;
import eu.stimey.platform.microservice.courses.utils.DateSerializer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GeneralMetadata {
	public String author;
	public String inspiredBy;
	public String title;
	public String avatarImageId;
	public final List<String> keywords;
	public String language;
	public Date creationDate;

	/**
	 * if published -> mission can be searched if not published -> students must
	 * have invite link
	 */
	public boolean published;

	public GeneralMetadata() {
		this.keywords = new ArrayList<>();
		this.published = false;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getCreationDate() {
		return creationDate;
	}

	@JsonDeserialize(using = DateDeserializer.class)
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
}
