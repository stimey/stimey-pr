package eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.challengeAnalysis;

public class StudentAnswer {
	private String questionTitle;
	private Object studentInput;
	private boolean isCorrect;

	public StudentAnswer() {
	}

	public StudentAnswer(String questionTitle, String studentInput, boolean isCorrect) {
		this.questionTitle = questionTitle;
		this.studentInput = studentInput;
		this.isCorrect = isCorrect;
	}

	public String getQuestionTitle() {
		return questionTitle;
	}

	public void setQuestionTitle(String questionTitle) {
		this.questionTitle = questionTitle;
	}

	public Object getStudentInput() {
		return studentInput;
	}

	public void setStudentInput(Object studentInput) {
		this.studentInput = studentInput;
	}

	public boolean isCorrect() {
		return isCorrect;
	}

	public void setCorrect(boolean correct) {
		isCorrect = correct;
	}
}
