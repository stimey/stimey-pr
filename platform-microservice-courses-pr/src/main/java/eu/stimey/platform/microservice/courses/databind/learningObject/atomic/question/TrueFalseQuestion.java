package eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question;

import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.databind.challengeResult.StudentInput;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;

public class TrueFalseQuestion extends CheckableQuestion {

	public Answer answer;
	public String hint;

	public TrueFalseQuestion() {
		this.answer = new Answer();
	}

	public TrueFalseQuestion(TrueFalseQuestion other) {
		super(other);
		this.answer = other.answer;
		this.hint = other.hint;
	}

	@Override
	public boolean check(StudentInput studentInput) {
		if (studentInput.studentInput().size() > 1)
			throw new IllegalArgumentException("wrong student input " + studentInput);
		return this.answer.isCorrect == Boolean.valueOf(studentInput.studentInput().get(0));
	}

	@Override
	public LearningObject copy(LearningObjectsService learningObjectsService) {
		LearningObject copy = new TrueFalseQuestion(this);
		learningObjectsService.setLearningObject(copy);
		return copy;
	}
}
