package eu.stimey.platform.microservice.courses.beans.filter;

import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.Difficulty;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.Stage;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.Topic;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class MissionsFilter implements DocumentFilter {

	public static final String KEYWORDS_PATH = "missionMetadata.generalMetadata.keywords";
	public static final String STAGE_PATH = "missionMetadata.educationalMetadata.stage";
	public static final String TOPICS_PATH = "missionMetadata.educationalMetadata.topics";
	public static final String DIFFICULTY_PATH = "missionMetadata.educationalMetadata.difficulty";
	public static final String TITLE_PATH = "missionMetadata.generalMetadata.title";
	public static final String LANGUAGE_PATH = "missionMetadata.generalMetadata.language";
	public static final String CREATIONDATE_PATH = "missionMetadata.generalMetadata.creationDate";

	private Document filter;

	public MissionsFilter() {
	}

	public MissionsFilter language(String language) {
		if (language == null || language.isEmpty() || language.equals("Any"))
			return this;
		Document matchLanguage = new Document(LANGUAGE_PATH, language);
		if (this.filter == null)
			this.filter = matchLanguage;
		else
			this.filter = FilterBuilder.and(this.filter, matchLanguage);
		return this;
	}

	public MissionsFilter keywords(List<String> keywords) {
		if (keywords == null || keywords.isEmpty())
			return this;
		return this.keywords(keywords.toArray(new String[0]));
	}

	public MissionsFilter keywords(String... keywords) {
		if (keywords == null || keywords.length == 0)
			return this;
		Document keywordsQuery = new Document(KEYWORDS_PATH, new Document("$all", Arrays.asList(keywords)));
		if (this.filter == null)
			this.filter = keywordsQuery;
		else
			this.filter = FilterBuilder.and(this.filter, keywordsQuery);
		return this;
	}

	public MissionsFilter query(String query) {
		if (query == null || query.isEmpty())
			return this;
		List<Document> result = new LinkedList<>();

		for (String part : query.split(" ")) {
			result.add(new Document(TITLE_PATH, FilterBuilder.regex(part, FilterBuilder.CASE_INSENSITIVITY)));
			// result.add(new Document(KEYWORDS_PATH, FilterBuilder.regex(part,
			// FilterBuilder.CASE_INSENSITIVITY)));
		}
		Document regexDocument = FilterBuilder.or(result);
		if (this.filter == null)
			this.filter = regexDocument;
		else
			this.filter = FilterBuilder.and(this.filter, regexDocument);
		return this;
	}

	public MissionsFilter stages(List<String> stages) {
		if (stages == null || stages.isEmpty())
			return this;
		return this.stages(stages.toArray(new String[0]));
	}

	public MissionsFilter stages(String... stageLiterals) {
		if (stageLiterals == null || stageLiterals.length == 0)
			return this;
		List<Document> stages = new ArrayList<>();
		for (String stageLiteral : stageLiterals) {
			stages.add(new Document(STAGE_PATH, Stage.fromString(stageLiteral).getText()));
		}
		Document stageQuery = new Document("$or", stages);
		if (this.filter == null)
			this.filter = stageQuery;
		else
			this.filter = FilterBuilder.and(this.filter, stageQuery);
		return this;
	}

	public MissionsFilter topics(List<String> topics) {
		if (topics == null || topics.isEmpty())
			return this;
		return this.topics(topics.toArray(new String[0]));
	}

	public MissionsFilter topics(String... topicLiterals) {
		List<String> topics = new ArrayList<>();
		for (String topicLiteral : topicLiterals) {
			topics.add(Topic.fromString(topicLiteral).getText());
		}
		Document topicQuery = new Document(TOPICS_PATH, new Document("$in", topics));
		if (this.filter == null)
			this.filter = topicQuery;
		else
			this.filter = FilterBuilder.and(this.filter, topicQuery);
		return this;
	}

	public MissionsFilter difficulties(List<String> difficulties) {
		if (difficulties == null || difficulties.isEmpty())
			return this;
		return this.difficulties(difficulties.toArray(new String[0]));
	}

	public MissionsFilter difficulties(String... difficultyLiterals) {
		List<Document> difficulties = new ArrayList<>();
		for (String difficultyLiteral : difficultyLiterals) {
			difficulties.add(new Document(DIFFICULTY_PATH, Difficulty.fromString(difficultyLiteral).getText()));
		}
		Document difficultyQuery = new Document("$or", difficulties);
		if (this.filter == null)
			this.filter = difficultyQuery;
		else
			this.filter = FilterBuilder.and(this.filter, difficultyQuery);
		return this;
	}

	public MissionsFilter ids(List<String> ids) {
		List<ObjectId> objectIds = ids.stream().map(id -> new ObjectId(id)).collect(Collectors.toList());
		Document idsFilter = new Document("$in", objectIds);
		Document query = new Document("_id", idsFilter);
		if (this.filter == null)
			this.filter = query;
		else
			this.filter = FilterBuilder.and(this.filter, query);
		return this;
	}

	public static Document matchTitle(String title) {
		return new Document("$text", new Document("$search", title));
		// return new Document(TITLE_PATH, new Document("$regex", "^" + title +
		// "$").append("$options", "i"));
	}

	public static Document matchTitleCaseIntensive(String title) {
		return new Document(TITLE_PATH, new Document("$regex", "^" + title + "$").append("$options", "i"));
	}

	@Override
	public Document toDocument() {
		Document result;
		if (this.filter == null) {
			result = new Document();
		} else {
			result = this.filter;
			this.filter = null;
		}
		return result;
	}
}
