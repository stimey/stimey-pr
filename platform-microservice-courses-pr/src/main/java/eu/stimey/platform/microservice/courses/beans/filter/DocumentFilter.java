package eu.stimey.platform.microservice.courses.beans.filter;

import org.bson.Document;

public interface DocumentFilter {
	Document toDocument();
}
