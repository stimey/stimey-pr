package eu.stimey.platform.microservice.courses.databind.robotActions;

public class IdleRobotAction extends RobotAction {
	public long timeToTriggerMinutes;
	public long additionalSeconds;

	public IdleRobotAction() {
		super();
	}
}
