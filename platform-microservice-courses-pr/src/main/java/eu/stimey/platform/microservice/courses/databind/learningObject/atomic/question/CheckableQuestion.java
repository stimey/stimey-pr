package eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.AtomicLearningObject;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = MultipleChoiceQuestion.class, name = "MultipleChoiceQuestion"),
		@JsonSubTypes.Type(value = OneChoiceQuestion.class, name = "OneChoiceQuestion"),
		@JsonSubTypes.Type(value = TrueFalseQuestion.class, name = "TrueFalseQuestion") })
public abstract class CheckableQuestion extends AtomicLearningObject implements Checkable {
	public CheckableQuestion(CheckableQuestion other) {
		super(other);
	}

	public CheckableQuestion() {
	}
}
