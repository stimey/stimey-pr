package eu.stimey.platform.microservice.courses.databind.robotActions;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = LevelRobotActions.class, name = "LevelRobotActions"),
		@JsonSubTypes.Type(value = MissionRobotActions.class, name = "MissionRobotActions") })
public abstract class RobotActions {
	public RobotAction onLoad;

	public RobotActions() {
		this.onLoad = new RobotAction();
	}
}
