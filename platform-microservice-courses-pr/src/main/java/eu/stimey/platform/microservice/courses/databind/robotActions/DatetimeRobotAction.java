package eu.stimey.platform.microservice.courses.databind.robotActions;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.stimey.platform.microservice.courses.utils.DateDeserializer;
import eu.stimey.platform.microservice.courses.utils.DateSerializer;

import java.awt.*;
import java.util.Date;

public class DatetimeRobotAction extends RobotAction {

	public Date date;

	public DatetimeRobotAction() {
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getDate() {
		return this.date;
	}

	@JsonDeserialize(using = DateDeserializer.class)
	public void setDate(Date date) {
		this.date = date;
	}

}
