package eu.stimey.platform.microservice.courses.databind;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.stimey.platform.microservice.courses.utils.EntityDeserializer;
import eu.stimey.platform.microservice.courses.utils.EntitySerializer;
import org.bson.types.ObjectId;

import java.util.Objects;

public class DocumentEntity implements Entity {
	private String _id;

	public DocumentEntity() {
		this._id = new ObjectId().toHexString();
	}

	@Override
	@JsonSerialize(using = EntitySerializer.class)
	public String get_id() {
		if (_id == null)
			_id = new ObjectId().toHexString();
		return _id;
	}

	@Override
	@JsonDeserialize(using = EntityDeserializer.class)
	public void set_id(String _id) {
		this._id = _id;
	}

	@Override
	public String toString() {
		return "DocumentEntity{" + "id='" + _id + '\'' + '}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		DocumentEntity that = (DocumentEntity) o;
		return Objects.equals(_id, that._id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(_id);
	}

}
