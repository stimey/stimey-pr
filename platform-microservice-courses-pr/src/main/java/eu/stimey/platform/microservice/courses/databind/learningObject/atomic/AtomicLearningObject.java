package eu.stimey.platform.microservice.courses.databind.learningObject.atomic;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.multimedia.Assignment;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.multimedia.MultiMediaLearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.multimedia.Task;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question.CheckableQuestion;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question.MultipleChoiceQuestion;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question.OneChoiceQuestion;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question.TrueFalseQuestion;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.tool.MindMapTool;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.Reward;

@JsonTypeInfo(use = Id.NAME, property = "type")
@JsonSubTypes({ @Type(value = MindMapTool.class, name = "MindMapTool"),
		@Type(value = MultipleChoiceQuestion.class, name = "MultipleChoiceQuestion"),
		@Type(value = TrueFalseQuestion.class, name = "TrueFalseQuestion"),
		@Type(value = OneChoiceQuestion.class, name = "OneChoiceQuestion"),
		@Type(value = MultiMediaLearningObject.class, name = "MultiMediaLearningObject"),
		@Type(value = CheckableQuestion.class, name = "CheckableQuestion") })
public abstract class AtomicLearningObject extends LearningObject {

	public AtomicLearningObject() {
		super();
	}

	public AtomicLearningObject(String title, String description, Reward reward) {
		super(title, description, reward);
	}

	public AtomicLearningObject(AtomicLearningObject original) {
		super(original);
	}
}
