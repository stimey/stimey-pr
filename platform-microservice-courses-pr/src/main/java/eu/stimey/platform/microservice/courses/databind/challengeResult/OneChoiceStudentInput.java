package eu.stimey.platform.microservice.courses.databind.challengeResult;

import java.util.ArrayList;
import java.util.List;

public class OneChoiceStudentInput extends StudentInput {

	public final String studentInput;

	public OneChoiceStudentInput() {
		this(null);
	}

	public OneChoiceStudentInput(String studentInput) {
		super();
		this.studentInput = studentInput;
	}

	@Override
	public String toString() {
		return "OneChoiceStudentInput{" + "studentInput='" + studentInput + '\'' + '}';
	}

	@Override
	public List<String> studentInput() {
		List<String> studentInput = new ArrayList<>();
		studentInput.add(this.studentInput);
		return studentInput;
	}

}
