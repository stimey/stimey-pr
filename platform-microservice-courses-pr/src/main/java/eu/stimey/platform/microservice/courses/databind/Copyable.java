package eu.stimey.platform.microservice.courses.databind;

import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;

public interface Copyable {
	LearningObject copy(LearningObjectsService learningObjectsService);
}
