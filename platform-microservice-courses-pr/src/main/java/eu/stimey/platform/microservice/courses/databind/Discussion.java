package eu.stimey.platform.microservice.courses.databind;

import java.util.Objects;

public class Discussion extends DocumentEntity {
	private String title;
	private String topic;
	private String statusUpdateId;

	public Discussion() {
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public void setStatusUpdateId(String statusUpdateId) {
		this.statusUpdateId = statusUpdateId;
	}

	public String getTopic() {
		return topic;
	}

	public String getStatusUpdateId() {
		return statusUpdateId;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public String toString() {
		return "Discussion{" + "title='" + title + '\'' + ", topic='" + topic + '\'' + ", statusUpdateId='"
				+ statusUpdateId + '\'' + '}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Discussion that = (Discussion) o;
		return Objects.equals(title, that.title) && Objects.equals(topic, that.topic)
				&& Objects.equals(statusUpdateId, that.statusUpdateId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(title, topic, statusUpdateId);
	}
}
