package eu.stimey.platform.microservice.courses.databind.challengeResult;

import java.util.ArrayList;
import java.util.List;

public class TrueFalseStudentInput extends StudentInput {
	public boolean studentInput;

	public TrueFalseStudentInput() {
		this(false);
	}

	public TrueFalseStudentInput(boolean studentInput) {
		super();
		this.studentInput = studentInput;
	}

	@Override
	public String toString() {
		return "TrueFalseStudentInput{" + "studentInput=" + studentInput + '}';
	}

	@Override
	public List<String> studentInput() {
		List<String> studentInput = new ArrayList<>();
		studentInput.add(String.valueOf(this.studentInput));
		System.out.println(studentInput);
		System.out.println(this.studentInput);
		return studentInput;
	}
}
