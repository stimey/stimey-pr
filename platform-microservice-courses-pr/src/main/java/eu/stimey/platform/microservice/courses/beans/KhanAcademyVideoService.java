package eu.stimey.platform.microservice.courses.beans;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.library.utils.StimeyLogger;
import eu.stimey.platform.microservice.courses.beans.filter.FilterBuilder;
import eu.stimey.platform.microservice.courses.databind.KhanAcademyVideoLesson;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class KhanAcademyVideoService {

	private static final String KHAN_ACADEMY_API_URL = "http://www.khanacademy.org/api/v1/topictree";

	private Caching<String, KhanAcademyVideoLesson> khanAcademyVideoLessonCache;

	@Inject
	private KhanAcademyAPIConnection khanAcademyAPIConnection;

	public KhanAcademyVideoService(KhanAcademyAPIConnection khanAcademyAPIConnection, boolean updateDatabase) {
		// StimeyLogger.logger().info("constructor khan academy");

		this.khanAcademyAPIConnection = khanAcademyAPIConnection;
		if (updateDatabase) {
			this.updateDatabase();
		}

		this.khanAcademyVideoLessonCache = new Caching<>("ms_courses_khanacademy", "_id", KhanAcademyVideoLesson.class,
				"khanAcademyVideos", GlobalVariables.CACHE_CONFIG);
		this.khanAcademyVideoLessonCache.setWorkaround_keyMapper((key) -> new ObjectId(key));
	}

	public KhanAcademyVideoService(boolean updateDatabase) {
		if (updateDatabase)
			this.updateDatabase();
	}

	/**
	 * builds a filter with the $or for the searching in the database
	 *
	 * @param values the list of documents to or together
	 * @return a document with the $or filter for the searching in the database
	 */
	public static Document or(Document... values) {
		if (values.length == 0)
			return new Document();
		if (values.length == 1)
			return values[0];
		return new Document("$or", Arrays.asList(values));
	}

	/**
	 * builds a filter with the keywords for the searching in the database
	 *
	 * @param keywords the filter for the keywords
	 * @return a document with the keywords filter for the searching in the database
	 */
	public static Document searchKeywordsFilter(String keywords) {
		if (keywords == null || keywords.length() <= 0) {
			return new Document();
		}
		List<Document> result = new LinkedList<>();
		for (String part : keywords.split(" ")) {
			result.add(new Document("searchKeywords", FilterBuilder.regex(part, FilterBuilder.CASE_INSENSITIVITY)));
		}
		if (result.size() == 0)
			return new Document();
		if (result.size() == 1)
			return result.get(0);
		return or(result.toArray(new Document[result.size()]));
	}

	public KhanAcademyVideoLesson getKhanAcademyVideoLessonById(String videoLessonId) {
		khanAcademyVideoLessonCache.find(new Document("_id", new ObjectId(videoLessonId)), null, 0, 0,
				(khanAcademyVideoLesson) -> khanAcademyVideoLesson.get_id(), true);
		return khanAcademyVideoLessonCache.get(videoLessonId);
	}

	public List<KhanAcademyVideoLesson> getKhanAcademyVideoLessonList(Document filter, int offset, int limit) {
		return this.getKhanAcademyVideoLessonByIds(this.getKhanAcademyVideoLessonIds(filter, offset, limit, null));
	}

	public List<String> getKhanAcademyVideoLessonIds(Document filter, int offset, int limit, String order) {
//        Document sort;
//        if (order.equals("dec")) {
//            sort = new Document("timestamp", -1);
//        } else {
//            sort = null;
//        }

		return khanAcademyVideoLessonCache.find(filter, null, offset, limit,
				(khanAcademyVideoLesson) -> khanAcademyVideoLesson.get_id(), true);
	}

	public List<KhanAcademyVideoLesson> getKhanAcademyVideoLessonByIds(List<String> khanAcademyVideoLessonIds) {
		List<KhanAcademyVideoLesson> khanAcademyVideoLessons = new LinkedList<>();
		for (String videoLessonId : khanAcademyVideoLessonIds) {
			khanAcademyVideoLessons.add(this.getKhanAcademyVideoLessonById(videoLessonId));
		}
		return khanAcademyVideoLessons;
	}

	public List<KhanAcademyVideoLesson> searchVideosByKeywords(String keywords) {
		List<KhanAcademyVideoLesson> khanAcademyVideoLessons = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		MongoClient mongoClient = MongoDBService.getService().getClient();
		MongoCollection<Document> collection = mongoClient.getDatabase("ms_courses").getCollection("khanAcademyVideos");

		// Document filter = new Document("searchKeywords", regex(part,
		// DocumentFilter.CASE_INSENSITIVITY));

		Document filter = searchKeywordsFilter(keywords);

		// FindIterable<Document> videosAsDocuments =
		// collection.find(Filters.text(keywords)).projection(Projections.metaTextScore("score")).sort(Sorts.metaTextScore("score"));
		FindIterable<Document> videosAsDocuments = collection.find(filter)
				.projection(Projections.metaTextScore("score")).sort(Sorts.metaTextScore("score"));

		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		try {
			for (Document doc : videosAsDocuments) {
				khanAcademyVideoLessons.add(mapper.readValue(doc.toJson(), KhanAcademyVideoLesson.class));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return khanAcademyVideoLessons;
	}

	private void updateDatabase() {

		MongoClient client = MongoDBService.getService().getClient();
		if (!client.getDatabase("ms_courses").listCollectionNames().into(new ArrayList<>())
				.contains("khanAcademyVideos")) {
			StimeyLogger.logger().info("need to update database, because video from khan academy are not added yet");
			this.khanAcademyAPIConnection.pushVideoLessonsToDatabase(client, "ms_courses", "khanAcademyVideos");
		} else {
			StimeyLogger.logger().info("did not update database");
		}

	}

}
