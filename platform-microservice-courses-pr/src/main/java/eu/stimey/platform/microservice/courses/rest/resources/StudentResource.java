package eu.stimey.platform.microservice.courses.rest.resources;

import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.beans.MissionToAuthorNamesMapper;
import eu.stimey.platform.microservice.courses.beans.MissionsService;
import eu.stimey.platform.microservice.courses.beans.StudentService;
import eu.stimey.platform.microservice.courses.databind.mission.Mission;
import eu.stimey.platform.microservice.courses.databind.user.BonusTaskState;
import eu.stimey.platform.microservice.courses.databind.user.MissionState;
import eu.stimey.platform.microservice.courses.databind.user.Student;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.MissionResponse;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.StudentsMissionsResponse;
import org.apache.commons.lang3.tuple.Triple;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/students")
public class StudentResource {

	@Inject
	private StudentService studentService;

	@Inject
	private MissionToAuthorNamesMapper missionToAuthorNamesMapper;

	@Inject
	private MissionsService missionsService;

	@Inject
	private LearningObjectsService learningObjectsService;

	@GET
	@Path("/{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response student(@PathParam("userid") String userid) {
		return Response.ok().entity(this.studentService.getStudent(userid)).build();
	}

	@GET
	@Path("/{userid}/missions")
	@Produces(MediaType.APPLICATION_JSON)
	public Response studentsMissions(@PathParam("userid") String userid) {
		Student student = this.studentService.getStudent(userid);

		StudentsMissionsResponse studentsMissionsResponse = new StudentsMissionsResponse(
				this.createMissionResponse(student, student.activeMissions()),
				this.createMissionResponse(student, student.completedMissions()));
		return Response.ok().entity(studentsMissionsResponse).build();
	}

	/**
	 * It is possible that missions are deleted from database, but a Student still
	 * has ids of deleted missions. This will throw an error. Therefore, references
	 * to missions that do not exist anymore are deleted from Student. This method
	 * will be obsolete, when deleting missions from frontend is possible.
	 *
	 * @param student
	 * @param missionIds
	 * @return List of missions that do exist.
	 */
	private List<Mission> validateStudentsMissions(Student student, final List<String> missionIds) {
		List<Mission> missions = new ArrayList<>();
		List<String> idsOfMissionsThatDoNotExist = new ArrayList<>();
		for (String missionId : missionIds) {
			final Mission mission = this.missionsService.getMissionById(missionId);
			if (mission == null) {
				idsOfMissionsThatDoNotExist.add(missionId);
			} else {
				missions.add(mission);
			}
		}
		if (!idsOfMissionsThatDoNotExist.isEmpty()) {
			student.removeNotExistingMissions(idsOfMissionsThatDoNotExist);
			this.studentService.setStudent(student);
		}
		return missions;
	}

	private List<MissionResponse> createMissionResponse(Student student, final List<String> missionIds) {
		final List<Mission> validMissions = validateStudentsMissions(student, missionIds);
		List<MissionResponse> missionResponses = new ArrayList<>();
		for (Mission mission : validMissions) {
			final String authorName = this.missionToAuthorNamesMapper.getAuthorNameOfMission(mission);
			final Triple<Integer, Integer, Integer> typesOfLearningObjects = this.learningObjectsService
					.typesOfLearningObjects(mission.getLearningObjectIds());
			missionResponses.add(new MissionResponse(mission, authorName, typesOfLearningObjects.getLeft(),
					typesOfLearningObjects.getMiddle(), typesOfLearningObjects.getRight()));
		}
		return missionResponses;
	}

	@POST
	@Path("/{studentId}/activeMissions")
	@Consumes("text/plain")
	@Produces(MediaType.APPLICATION_JSON)
	public Response enroll(@PathParam("studentId") String studentId, String missionId) {
		this.studentService.enroll(studentId, missionId);
		return Response.status(Response.Status.OK).build();
	}

	@HEAD
	@Path("/{studentId}/activeMissions/{missionId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response isStudentEnrolled(@PathParam("studentId") String studentId,
			@PathParam("missionId") String missionId) {
		Student student = this.studentService.getStudent(studentId);
		Response response;
		if (student.isEnrolled(missionId))
			response = Response.status(Response.Status.OK).build();
		else
			response = Response.status(Response.Status.NOT_FOUND).build();
		return response;
	}

	@HEAD
	@Path("/{studentId}/completedMissions/{missionId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response hasStudentCompletedMission(@PathParam("studentId") String studentId,
			@PathParam("missionId") String missionId) {
		final Student student = this.studentService.getStudent(studentId);
		Response response;
		if (student.completedMissions.keySet().contains(missionId))
			response = Response.status(Response.Status.OK).build();
		else
			response = Response.status(Response.Status.NOT_FOUND).build();
		return response;
	}

	@GET
	@Path("/{studentId}/missions/{missionId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProgress(@PathParam("studentId") String studentId, @PathParam("missionId") String missionId) {
		Student student = this.studentService.getStudent(studentId);
		if (!student.hasEnrolled(missionId))
			return Response.status(Response.Status.NOT_FOUND).build();
		final MissionState missionState = this.studentService.getProgressOfMission(studentId, missionId);
		return Response.status(Response.Status.OK).entity(missionState).build();
	}

	@PUT
	@Path("/{studentId}/missions/{missionId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateMissionState(@PathParam("studentId") String studentId,
			@PathParam("missionId") String missionId, MissionState missionState) {
		this.studentService.setMissionstate(studentId, missionId, missionState);
		return Response.status(Response.Status.OK).entity(missionState).build();
	}

	@POST
	@Path("/{studentId}/missions/{missionId}/progress")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response setProgress(@PathParam("studentId") String studentId, @PathParam("missionId") String missionId,
			int progress) {
		Student student = this.studentService.getStudent(studentId);
		MissionState missionState = this.studentService.getProgressOfMission(studentId, missionId);
		missionState.setCurrentProgress(progress);
		if (student.activeMissions.containsKey(missionId)) {
			student.activeMissions.put(missionId, missionState);
		} else {
			student.completedMissions.put(missionId, missionState);
		}
		this.studentService.setStudent(student);
		return Response.status(Response.Status.OK).entity(missionState).build();
	}

	@POST
	@Path("/{studentId}/activeMissions/{missionId}/increaseProgress")
	@Produces(MediaType.APPLICATION_JSON)
	public Response increaseProgress(@PathParam("studentId") String studentId,
			@PathParam("missionId") String missionId) {
		final MissionState missionState = this.studentService.increaseProgress(studentId, missionId);
		return Response.status(Response.Status.OK).entity(missionState).build();
	}

	@POST
	@Path("/{studentId}/activeMissions/{missionId}/decreaseProgress")
	@Produces(MediaType.APPLICATION_JSON)
	public Response decreaseProgress(@PathParam("studentId") String studentId,
			@PathParam("missionId") String missionId) {
		final MissionState missionState = this.studentService.decreaseProgress(studentId, missionId);
		return Response.status(Response.Status.OK).entity(missionState).build();
	}

	@PUT
	@Path("/{studentId}/missions/{missionId}/bonusTaskState")
	@Produces(MediaType.APPLICATION_JSON)
	public Response setBonusTaskState(@PathParam("studentId") String studentId,
			@PathParam("missionId") String missionId, BonusTaskState bonusTaskState) {
		MissionState missionState = this.studentService.getProgressOfMission(studentId, missionId);
		missionState.bonusTaskState = bonusTaskState;
		studentService.setMissionstate(studentId, missionId, missionState);
		return Response.status(Response.Status.OK).entity(missionState).build();
	}

}
