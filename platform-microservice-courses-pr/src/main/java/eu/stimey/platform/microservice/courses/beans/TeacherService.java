package eu.stimey.platform.microservice.courses.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.courses.databind.user.Teacher;

public class TeacherService {

	public final Caching<String, Teacher> teacherCache;

	public TeacherService() {
		teacherCache = new Caching<>("ms_courses_teachers", "userId", Teacher.class, "teachers",
				GlobalVariables.CACHE_CONFIG);
		teacherCache.clear();
	}

	public Teacher getTeacher(String teacherId) {
		Teacher teacher = this.teacherCache.get(teacherId);
		if (teacher == null) {
			teacher = new Teacher(teacherId);
			this.teacherCache.set(teacherId, teacher);
		}
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacherCache.set(teacher.userId, teacher);
	}

	public void addMissionToSavedMissions(String teacherId, String missionId) {
		Teacher teacher = this.getTeacher(teacherId);
		if (teacher.savedMissions.contains(missionId))
			return;
		teacher.savedMissions.add(missionId);
		this.teacherCache.set(teacherId, teacher);
	}

	public void publishMission(String teacherId, String missionId) {
		Teacher teacher = this.teacherCache.get(teacherId);
		teacher.publish(missionId);
		this.teacherCache.set(teacherId, teacher);
	}
}
