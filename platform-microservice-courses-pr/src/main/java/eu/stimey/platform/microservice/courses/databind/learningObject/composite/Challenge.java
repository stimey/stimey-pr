package eu.stimey.platform.microservice.courses.databind.learningObject.composite;

import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.Reward;

public class Challenge extends CompositeLearningObject {

	public ChallengeDuration challengeDuration;

	public Challenge() {
		this("");
	}

	public Challenge(String title) {
		super(title, "", new Reward());
		this.challengeDuration = new ChallengeDuration();
	}

	public Challenge(Challenge original) {
		super(original);
		this.challengeDuration = original.challengeDuration;
	}

	@Override
	public LearningObject copy(LearningObjectsService learningObjectsService) {
		Challenge copy = new Challenge(this);
		for (String learningObjectId : this.learningObjectIds) {
			LearningObject learningObject = learningObjectsService.getById(learningObjectId);
			LearningObject copyOfInnerLearningObject = learningObject.copy(learningObjectsService);
			copy.learningObjectIds.add(copyOfInnerLearningObject.get_id());
		}
		learningObjectsService.setLearningObject(copy);
		return copy;
	}
}
