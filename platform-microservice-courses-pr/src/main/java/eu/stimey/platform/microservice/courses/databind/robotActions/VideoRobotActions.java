package eu.stimey.platform.microservice.courses.databind.robotActions;

import java.util.ArrayList;
import java.util.List;

public class VideoRobotActions {
	public RobotAction onStart;
	public List<IdleRobotAction> timerTrigger;
	public RobotAction onEnd;

	public VideoRobotActions() {
		this.onStart = new RobotAction();
		this.timerTrigger = new ArrayList<>();
		this.timerTrigger.add(new IdleRobotAction());
		this.onEnd = new RobotAction();
	}
}
