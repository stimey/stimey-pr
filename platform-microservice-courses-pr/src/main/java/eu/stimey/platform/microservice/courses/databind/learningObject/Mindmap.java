package eu.stimey.platform.microservice.courses.databind.learningObject;

public class Mindmap {
	private String courseId;
	private String mindmapModel;

	public Mindmap() {
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getMindmapModel() {
		return mindmapModel;
	}

	public void setMindmapModel(String mindmapModel) {
		this.mindmapModel = mindmapModel;
	}

}
