package eu.stimey.platform.microservice.courses.databind.mission.missionMetadata;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Template {
	BLANK("Blank"), PROGRESSIVE_INQUIRY_MODEL("Progressive Inquiry Model"), PROBLEM_BASED_MODEL("Problem Based Model"),
	PROJECT_BASED_MODEL("Project Based Model");

	private final String text;

	Template(String text) {
		this.text = text;
	}

	public static Template fromString(String str) {
		for (Template template : Template.values()) {
			if (template.text.equals(str)) {
				return template;
			}
		}
		throw new IllegalArgumentException("no such stage " + str);
	}

	@JsonValue
	public String getText() {
		return text;
	}
}
