package eu.stimey.platform.microservice.courses.beans;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import eu.stimey.platform.microservice.courses.databind.KhanAcademyVideoLesson;
import org.bson.Document;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class KhanAcademyAPIConnection {
	private final List<String> currentPath;
	private final List<KhanAcademyVideoLesson> khanAcademyVideoLessons;
	private final Set<String> alreadyAddedYoutubeIds;

	public KhanAcademyAPIConnection() {
		currentPath = new ArrayList<>();
		khanAcademyVideoLessons = new ArrayList<>();
		alreadyAddedYoutubeIds = new HashSet<>();
	}

	public void updateVideoList() {
		JsonNode rootChildArray;

		try {
			rootChildArray = parseJSONFromURL();
			findAllVideos(rootChildArray);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private JsonNode parseJSONFromURL() throws IOException {
		URL jsonUrl = new URL("http://www.khanacademy.org/api/v1/topictree");
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readTree(jsonUrl).get("children");
	}

	/**
	 * Scans the JSON Tree recursively for children arrays. If an array Objects has
	 * a children array too, it will also be scanned till one recursive call reaches
	 * the bottom of the subtree (no more children arrays). Each iteration in the
	 * for-loop checks for "kind = Video" to identify a video object and then adds a
	 * VideoLesson instance to the ArrayList "videoLessons". The current path will
	 * be saved to an ArrayList in each recursive call and respectively the last
	 * item deleted after the call finished, so we can identify the topic titles of
	 * one Video e.g. Math, Early Math, Counting...
	 *
	 * @param childArray
	 */
	private void findAllVideos(JsonNode childArray) {
		for (int i = 0; i < childArray.size(); i++) {
			if (childArray.get(i).has("children")) {
				if (childArray.get(i).get("children").size() > 0) {
					addCurrentPathTitle(childArray.get(i).get("title").textValue());

					JsonNode nestedChildArray = childArray.get(i).get("children");

					findAllVideos(nestedChildArray);
					removeLastPathTitle();
				}
			}

			if (childArray.get(i).get("kind").toString().equals("\"Video\"")
					&& !(alreadyAddedYoutubeIds.contains(childArray.get(i).get("youtube_id").textValue()))) {
				addVideoLesson(childArray, i);
			}
		}
	}

	private void addVideoLesson(JsonNode childArray, int index) {
		KhanAcademyVideoLesson vL = new KhanAcademyVideoLesson();

		vL.setTopicPathKeywords(currentPath);
		vL.setVideoTitle(childArray.get(index).get("title").textValue());

		vL.setThumbnailDefaultUrl(childArray.get(index).get("thumbnail_urls").get("default").textValue());
		vL.setThumbnailFilteredUrl(childArray.get(index).get("thumbnail_urls").get("filtered").textValue());

		if (childArray.get(index).get("description") != null) {
			vL.setVideoDescription(childArray.get(index).get("description").textValue());
		}

		String youtubeID = childArray.get(index).get("youtube_id").textValue();
		vL.setVideoYoutubeUrls(youtubeID);
		alreadyAddedYoutubeIds.add(youtubeID);

		JsonNode downloadUrl = childArray.get(index).get("download_urls");
		if (downloadUrl.has("mp4")) {
			vL.setVideoMP4DownloadUrl(downloadUrl.get("mp4").textValue());
		}

		if (downloadUrl.has("mp4-low")) {
			vL.setVideoMP4LowQualityDownloadUrl(downloadUrl.get("mp4-low").textValue());
		}

		vL.setVideoCreationDate(childArray.get(index).get("creation_date").textValue());
		vL.buildSearchKeywords();

		khanAcademyVideoLessons.add(vL);
	}

	private void addCurrentPathTitle(String pathTitle) {
		currentPath.add(pathTitle);
	}

	private void removeLastPathTitle() {
		if (currentPath.size() > 0) {
			currentPath.remove(currentPath.size() - 1);
		}
	}

	public void pushVideoLessonsToDatabase(MongoClient mongoClient, String databaseName, String collectionName) {
		this.updateVideoList();
		mongoClient.getDatabase(databaseName).getCollection(collectionName).drop();
		MongoCollection<Document> collection = mongoClient.getDatabase(databaseName).getCollection(collectionName);
		List<Document> videoDocumentList = new LinkedList<>();
		ObjectMapper objM = new ObjectMapper();

		for (KhanAcademyVideoLesson vl : this.khanAcademyVideoLessons) {
			try {
				videoDocumentList.add(Document.parse(objM.writeValueAsString(vl)));
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
		collection.insertMany(videoDocumentList);
		Document document = new Document("searchKeywords", "text");
		collection.createIndex(document);

	}

	public void writeResultToPath(String path) {
		ObjectMapper objM = new ObjectMapper();
		try {
			objM.writeValue(new File(path), khanAcademyVideoLessons);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
