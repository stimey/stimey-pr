package eu.stimey.platform.microservice.courses.rest.resources;

import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.beans.MissionsService;
import eu.stimey.platform.microservice.courses.databind.mission.Mission;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/resources")
public class ResourcesResource {

	@Inject
	private MissionsService missionsService;

	@Inject
	private LearningObjectsService learningObjectsService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllResourceIdsOfLevel(@QueryParam("learningObjectId[]") List<String> learningObjectIds) {
		return Response.status(Response.Status.OK)
				.entity(learningObjectsService.learningObjectTitlesToResourceIdsMapping(learningObjectIds)).build();
	}

	@GET
	@Path("/missions/{missionId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllResourcesOfMission(@PathParam("missionId") String missionId) {
		Mission mission = this.missionsService.getMissionById(missionId);
		List<String> learningObjectIds = mission.getLearningObjectIds();
		return Response.status(Response.Status.OK)
				.entity(learningObjectsService.learningObjectTitlesToResourceIdsMapping(learningObjectIds)).build();
	}

	@GET
	@Path("/learningObjects/{learningObjectId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllResourcesOfLearningObject(@PathParam("learningObjectId") String learningObjectId) {
		return Response.status(Response.Status.OK)
				.entity(learningObjectsService.learningObjectTitleToResourceIdsMapping(learningObjectId)).build();
	}

}
