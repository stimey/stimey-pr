package eu.stimey.platform.microservice.courses.databind.challengeResult;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.List;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = TrueFalseStudentInput.class, name = "TrueFalseStudentInput"),
		@JsonSubTypes.Type(value = OneChoiceStudentInput.class, name = "OneChoiceStudentInput"),
		@JsonSubTypes.Type(value = MultipleChoiceStudentInput.class, name = "MultipleChoiceStudentInput") })
public abstract class StudentInput {

	public boolean studentGuessed;

	public StudentInput() {
	}

	@Override
	public String toString() {
		return "StudentInput{" + "studentGuessed=" + studentGuessed + '}';
	}

	public abstract List<String> studentInput();
}
