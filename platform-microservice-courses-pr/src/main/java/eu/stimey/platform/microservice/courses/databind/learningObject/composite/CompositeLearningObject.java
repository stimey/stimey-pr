package eu.stimey.platform.microservice.courses.databind.learningObject.composite;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.Reward;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@JsonTypeInfo(use = Id.NAME, property = "type")
@JsonSubTypes({ @Type(value = Challenge.class, name = "Challenge") })
public abstract class CompositeLearningObject extends LearningObject {

	public List<String> learningObjectIds;

	public CompositeLearningObject() {
		this("click to edit", null, new Reward());
	}

	public CompositeLearningObject(String title, String description, Reward reward) {
		super(title, description, reward);
		this.learningObjectIds = new LinkedList<>();
	}

	public CompositeLearningObject(CompositeLearningObject original) {
		super(original);
		this.learningObjectIds = new ArrayList<>();
	}
}
