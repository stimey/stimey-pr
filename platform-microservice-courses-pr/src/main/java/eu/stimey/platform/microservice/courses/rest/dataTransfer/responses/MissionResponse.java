package eu.stimey.platform.microservice.courses.rest.dataTransfer.responses;

import eu.stimey.platform.microservice.courses.databind.mission.Mission;

public class MissionResponse {
	public final String authorName;
	public final Mission mission;
	public final int amountOfLevels;
	public int amountOfTasks;
	public int amountOfAssignments;
	public int amountOfChallenges;

	public MissionResponse(Mission mission, String authorName) {
		this.authorName = authorName;
		this.mission = mission;
		this.amountOfLevels = mission.levels.size();
	}

	public MissionResponse(Mission mission, String authorName, int amountOfTasks, int amountOfAssignments,
			int amountOfChallenges) {
		this(mission, authorName);
		this.amountOfTasks = amountOfTasks;
		this.amountOfAssignments = amountOfAssignments;
		this.amountOfChallenges = amountOfChallenges;
	}
}