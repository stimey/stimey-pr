package eu.stimey.platform.microservice.courses.databind.learningObject.atomic.tool;

import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.AtomicLearningObject;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.Reward;

public class MindMapTool extends AtomicLearningObject {
	public String mindMapModel;

	public MindMapTool() {
		this("", "", new Reward(), "");
	}

	public MindMapTool(String title, String description, Reward reward, String mindMapModel) {
		super(title, description, reward);
		this.mindMapModel = mindMapModel;
	}

	public MindMapTool(MindMapTool original) {
		super(original);
	}

	@Override
	public LearningObject copy(LearningObjectsService learningObjectsService) {
		return null;
	}
}
