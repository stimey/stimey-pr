package eu.stimey.platform.microservice.courses.databind.challengeResult;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.stimey.platform.microservice.courses.databind.Entity;
import eu.stimey.platform.microservice.courses.utils.DateDeserializer;
import eu.stimey.platform.microservice.courses.utils.DateSerializer;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ChallengeStudentInput implements Entity {
	/**
	 * compound key of challengeId + studentId
	 */
	public String _id;

	public Map<String, StudentInput> results;

	public Date startDate;
	public Date submitDate;

	public ChallengeStudentInput() {
	}

	public ChallengeStudentInput(String challengeId, String studentId) {
		this(challengeId.concat(studentId));
	}

	public ChallengeStudentInput(String challengeResultId) {
		this.set_id(challengeResultId);
		this.startDate = Calendar.getInstance().getTime();
		this.results = new HashMap<>();
	}

	@Override
	public String get_id() {
		return _id;
	}

	@Override
	public void set_id(String _id) {
		this._id = _id;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getStartDate() {
		return startDate;
	}

	@JsonDeserialize(using = DateDeserializer.class)
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getSubmitDate() {
		return this.submitDate;
	}

	@JsonDeserialize(using = DateDeserializer.class)
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

}
