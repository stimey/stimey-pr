package eu.stimey.platform.microservice.courses.databind.mission.missionMetadata;

public class InformationAboutMission {
	public String description;
	public String objectives;
	public String requirements;
	public String resultOfMission;

	public InformationAboutMission() {

	}
}
