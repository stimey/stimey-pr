package eu.stimey.platform.microservice.courses.rest.dataTransfer.responses;

import eu.stimey.platform.microservice.courses.databind.Discussion;

import java.util.List;

public class DiscussionsResponse {
	public List<Discussion> discussions;
	public String statusUpdates;

	public DiscussionsResponse() {
	}
}
