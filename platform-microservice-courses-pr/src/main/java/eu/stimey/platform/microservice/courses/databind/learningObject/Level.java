package eu.stimey.platform.microservice.courses.databind.learningObject;

import eu.stimey.platform.microservice.courses.databind.robotActions.LevelRobotActions;
import eu.stimey.platform.microservice.courses.databind.robotActions.RobotActions;
import eu.stimey.platform.microservice.courses.databind.robotActions.VideoRobotActions;

import java.util.*;

public class Level /* extends CompositeLearningObject */ {

	public String html;
	public String title;
	public List<String> learningObjectIds;
	public RobotActions robotActions;

	// Id of this map is the id of the iframe tag of the youtube video
	// dont know other solution
	public Map<String, VideoRobotActions> videoIdToRobotactionMap;

	public Level() {
		this.title = "Click to edit title";
		this.learningObjectIds = new ArrayList<>();
		this.robotActions = new LevelRobotActions();
		this.videoIdToRobotactionMap = new HashMap<>();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Level level = (Level) o;
		return Objects.equals(title, level.title) && Objects.equals(learningObjectIds, level.learningObjectIds)
				&& Objects.equals(html, level.html);
	}

	@Override
	public int hashCode() {
		return Objects.hash(title, learningObjectIds, html);
	}

	@Override
	public String toString() {
		return "Level{" + "title='" + title + '\'' + ", learningObjectIds=" + learningObjectIds + ", html='" + html
				+ '\'' + '}';
	}
	/*
	 * @Override public LearningObject copy() { return null; }
	 */
}
