package eu.stimey.platform.microservice.courses.rest.dataTransfer;

import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.Level;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.MissionMetadata;

import java.util.List;

public class MissionOverview {

	private MissionMetadata missionMetadata;
	private List<Level> levels;
	private List<LearningObject> learningObjects;

	public MissionOverview() {
	}

	public MissionMetadata getMissionMetadata() {
		return missionMetadata;
	}

	public void setMissionMetadata(MissionMetadata missionMetadata) {
		this.missionMetadata = missionMetadata;
	}

	public List<Level> getLevels() {
		return levels;
	}

	public void setLevels(List<Level> levels) {
		this.levels = levels;
	}

	public List<LearningObject> getLearningObjects() {
		return learningObjects;
	}

	public void setLearningObjects(List<LearningObject> learningObjects) {
		this.learningObjects = learningObjects;
	}
}
