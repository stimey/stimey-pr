package eu.stimey.platform.microservice.courses.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.library.data.access.databind.Message;
import eu.stimey.platform.library.gamification.databind.GamificationEvents;
import eu.stimey.platform.microservice.courses.databind.mission.Mission;
import eu.stimey.platform.microservice.courses.databind.user.MissionState;
import eu.stimey.platform.microservice.courses.databind.user.Student;
import org.bson.types.ObjectId;

public class StudentService {
	private final MissionsService missionsService;
	private final Caching<String, Student> studentCache;

	public StudentService(MissionsService missionsService) {
		studentCache = new Caching<>("ms_courses_students", "userId", Student.class, "students",
				GlobalVariables.CACHE_CONFIG);
		studentCache.clear();
		this.missionsService = missionsService;
	}

	public Student getStudent(String studentId) {
		Student student = this.studentCache.get(studentId);
		if (student == null) {
			student = new Student(studentId);
			this.studentCache.set(studentId, student);
		}
		return student;
	}

	public void setStudent(Student student) {
		if (student == null)
			throw new IllegalArgumentException("student is null");
		this.studentCache.set(student.get_id(), student);
	}

	public MissionState increaseProgress(String studentId, String missionId) {
		Student student = this.getStudent(studentId);
		if (student.completedMissions.containsKey(missionId) && student.activeMissions.containsKey(missionId))
			throw new IllegalStateException("mission cannot be active and completed at the same time");
		MissionState missionState = this.getProgressOfMission(studentId, missionId);
		missionState.increaseProgress();
		if (missionState.isMissionFinished()) {
			student.completedMissions.put(missionId, missionState);
			student.activeMissions.remove(missionId);
		} else {
			student.activeMissions.put(missionId, missionState);
		}
		this.setStudent(student);
		return missionState;
	}

	public MissionState decreaseProgress(String studentId, String missionId) {
		Student student = this.getStudent(studentId);
		MissionState missionState = this.getProgressOfMission(studentId, missionId);
		missionState.decreaseProgress();
		// TODO: ist quatsch
		student.activeMissions.put(missionId, missionState);
		this.setStudent(student);
		return missionState;
	}

	public void enroll(String studentId, String missionId) {
		if (missionId == null)
			throw new IllegalArgumentException("missionId is null");
		if (studentId == null)
			throw new IllegalArgumentException("studentId is null");

		Mission mission = missionsService.getMissionById(missionId);

		if (!mission.students.contains(studentId)) {
			// throw new IllegalStateException("student with id=" + studentId + " is already
			// enrolled in mission with id=" + missionId);
			mission.students.add(studentId);
		}

		Student student = this.getStudent(studentId);
		student.enroll(mission);
		this.setStudent(student);
		this.missionsService.setMission(mission);
	}

	public MissionState getProgressOfMission(String studentId, String missionId) {
		if (missionId == null)
			throw new IllegalArgumentException("missionId is null");
		if (studentId == null)
			throw new IllegalArgumentException("studentId is null");
		Student student = this.getStudent(studentId);

		if (student.completedMissions.containsKey(missionId))
			return student.completedMissions.get(missionId);
		if (student.activeMissions.containsKey(missionId))
			return student.activeMissions.get(missionId);

		throw new IllegalStateException("missionId=" + missionId
				+ " is neither an active mission nor a completed mission of student with id=" + studentId);
	}

	public void setMissionstate(String studentId, String missionId, MissionState missionState) {
		if (missionId == null)
			throw new IllegalArgumentException("missionId is null");
		if (studentId == null)
			throw new IllegalArgumentException("studentId is null");
		Student student = this.getStudent(studentId);

		if (student.completedMissions.containsKey(missionId))
			student.completedMissions.put(missionId, missionState);
		if (student.activeMissions.containsKey(missionId))
			student.activeMissions.put(missionId, missionState);

		this.setStudent(student);
	}

	public void clear() {
		this.studentCache.clear();
	}
}
