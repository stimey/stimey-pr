package eu.stimey.platform.microservice.courses.databind.learningObject.atomic.tool;

import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.AtomicLearningObject;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.Reward;

public class StimeyRadio extends AtomicLearningObject {

	public StimeyRadio() {
		super();
	}

	public StimeyRadio(String title, String description, Reward reward, String learningObjectType) {
		super(title, description, reward);
	}

	public StimeyRadio(StimeyRadio original) {
		super(original);
	}

	@Override
	public LearningObject copy(LearningObjectsService learningObjectsService) {
		return null;
	}
}
