package eu.stimey.platform.microservice.courses.rest.resources;

import eu.stimey.platform.microservice.courses.beans.DiscussionService;
import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.beans.MissionsService;
import eu.stimey.platform.microservice.courses.databind.Discussion;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.mission.Mission;
import eu.stimey.platform.microservice.courses.startup.TestDataUtils;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/discussions")
public class DiscussionResource {

	@Inject
	private DiscussionService discussionService;

	@Inject
	private LearningObjectsService learningObjectsService;

	@Inject
	private MissionsService missionsService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDiscussions(@QueryParam("missionId") String missionId,
			@QueryParam("id[]") List<String> discussionIds) {

		// Don't know what to do if no query params are specified
		if (missionId == null && discussionIds == null)
			return Response.status(Response.Status.NO_CONTENT).build();

		Response response;

		if (missionId != null) {
			final Mission mission = missionsService.getMissionById(missionId);
			final List<String> learningObjectIds = mission.getLearningObjectIds();
			final List<LearningObject> learningObjects = learningObjectsService.getByIds(learningObjectIds);
			List<String> idsOfDiscussions = new ArrayList<>();
			learningObjects.forEach(learningObject -> idsOfDiscussions.addAll(learningObject.discussions));
			final List<Discussion> discussions = discussionService.getByIds(idsOfDiscussions);
			Discussion discussion = TestDataUtils.getRandomElement(discussions);
			discussion.setTopic(learningObjectsService.getById(discussion.getTopic()).title);
			response = Response.status(Response.Status.OK).entity(discussion).build();
		} else {
			List<Discussion> discussions = discussionService.getByIds(discussionIds);
			for (Discussion discussion : discussions) {
				LearningObject learningObject = this.learningObjectsService.getById(discussion.getTopic());
				discussion.setTopic(learningObject.title);
			}
			response = Response.status(Response.Status.OK).entity(discussions).build();
		}

		return response;
	}

	@POST
	@Path("/{learningObjectId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createDiscussion(@PathParam("learningObjectId") String learningObjectId, Discussion discussion) {
		this.discussionService.setDiscussion(discussion);
		LearningObject learningObject = this.learningObjectsService.getById(learningObjectId);
		learningObject.discussions.add(discussion.get_id());
		this.learningObjectsService.setLearningObject(learningObject);
		discussion.setTopic(learningObject.title);
		return Response.status(Response.Status.OK).entity(discussion).build();
	}
}
