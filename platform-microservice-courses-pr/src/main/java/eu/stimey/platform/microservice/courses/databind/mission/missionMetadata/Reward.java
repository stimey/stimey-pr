package eu.stimey.platform.microservice.courses.databind.mission.missionMetadata;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Reward {

	public int knowledge;
	public int social;
	public int creativity;

	public Reward() {
		this(0, 0, 0);
	}

	public Reward(int knowledge, int social, int creativity) {
		this.knowledge = knowledge;
		this.social = social;
		this.creativity = creativity;
	}

	public void addReward(Reward reward) {
		this.knowledge += reward.knowledge;
		this.social += reward.social;
		this.creativity += reward.creativity;
	}

	@JsonIgnore
	public int getTotalReward() {
		return this.knowledge + this.social + this.creativity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + creativity;
		result = prime * result + knowledge;
		result = prime * result + social;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reward other = (Reward) obj;
		if (creativity != other.creativity)
			return false;
		if (knowledge != other.knowledge)
			return false;
		if (social != other.social)
			return false;
		return true;
	}
}
