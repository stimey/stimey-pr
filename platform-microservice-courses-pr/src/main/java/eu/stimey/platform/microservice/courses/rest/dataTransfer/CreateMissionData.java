
package eu.stimey.platform.microservice.courses.rest.dataTransfer;

import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.EducationalMetadata;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.GeneralMetadata;

public class CreateMissionData {
	public GeneralMetadata generalMetadata;
	public EducationalMetadata educationalMetadata;

	public CreateMissionData() {

	}
}
