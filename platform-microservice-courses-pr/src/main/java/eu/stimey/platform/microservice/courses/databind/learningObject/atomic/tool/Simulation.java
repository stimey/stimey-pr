package eu.stimey.platform.microservice.courses.databind.learningObject.atomic.tool;

import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.AtomicLearningObject;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.Reward;

public class Simulation extends AtomicLearningObject {

	public Simulation() {
		this("", "", new Reward());
	}

	public Simulation(String title, String description, Reward reward) {
		super(title, description, reward);
	}

	@Override
	public LearningObject copy(LearningObjectsService learningObjectsService) {
		return null;
	}
}
