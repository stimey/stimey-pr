package eu.stimey.platform.microservice.courses.databind.mission.missionMetadata;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Difficulty {
	BASIC("Basic"), INTERMEDIATE("Intermediate"), ADVANCED("Advanced");

	private final String text;

	Difficulty(String text) {
		this.text = text;
	}

	public static Difficulty fromString(String text) {
		for (Difficulty d : Difficulty.values()) {
			if (d.text.equals(text)) {
				return d;
			}
		}
		throw new IllegalArgumentException("no such difficulty " + text);
	}

	@JsonValue
	public String getText() {
		return text;
	}
}
