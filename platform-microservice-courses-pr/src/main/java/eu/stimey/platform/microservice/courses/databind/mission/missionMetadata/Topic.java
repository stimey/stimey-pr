package eu.stimey.platform.microservice.courses.databind.mission.missionMetadata;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Topic {
	SCIENCE("Science"), TECHNOLOGY("Technology"), INNOVATION("Innovation"), MATHEMATICS("Mathematics"),
	ENGINEERING("Engineering"), ENTREPRENEURSHIP("Entrepreneurship");

	private final String text;

	Topic(String text) {
		this.text = text;
	}

	public static Topic fromString(String literal) {
		for (Topic topic : Topic.values()) {
			if (topic.text.equals(literal))
				return topic;
		}
		throw new IllegalArgumentException("no such topic " + literal);
	}

	@JsonValue
	public String getText() {
		return text;
	}

}
