package eu.stimey.platform.microservice.courses.rest.dataTransfer.responses;

import java.util.List;

public class TeachersMissionsResponse {

	public List<MissionResponse> activeMissions;
	public List<MissionResponse> completedMissions;
	public List<MissionResponse> savedMissions;
	public List<MissionResponse> pendingModeratorInvitations;

	public TeachersMissionsResponse() {
	}

	public void setActiveMissions(List<MissionResponse> activeMissions) {
		this.activeMissions = activeMissions;
	}

	public void setCompletedMissions(List<MissionResponse> completedMissions) {
		this.completedMissions = completedMissions;
	}

	public void setSavedMissions(List<MissionResponse> savedMissions) {
		this.savedMissions = savedMissions;
	}

	public void setPendingModeratorInvitations(List<MissionResponse> pendingModeratorInvitations) {
		this.pendingModeratorInvitations = pendingModeratorInvitations;
	}

}
