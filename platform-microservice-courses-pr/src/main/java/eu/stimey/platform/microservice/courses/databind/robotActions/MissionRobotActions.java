package eu.stimey.platform.microservice.courses.databind.robotActions;

import java.util.ArrayList;
import java.util.List;

public class MissionRobotActions extends RobotActions {

	public RobotAction onMissionFinished;
	public RobotAction onNotificationSend;
	public List<DatetimeRobotAction> scheduledRobotActions;

	public MissionRobotActions() {
		super();
		this.onMissionFinished = new RobotAction();
		this.onNotificationSend = new RobotAction();
		this.scheduledRobotActions = new ArrayList<>();
		this.scheduledRobotActions.add(new DatetimeRobotAction());
	}
}
