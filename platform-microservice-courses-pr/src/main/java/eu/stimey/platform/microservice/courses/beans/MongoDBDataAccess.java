package eu.stimey.platform.microservice.courses.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.library.data.access.CachingConfiguration;
import eu.stimey.platform.library.data.access.VolatileCachingConfiguration;
import eu.stimey.platform.library.data.access.VolatileCachingSearchResult;
import eu.stimey.platform.microservice.courses.databind.Entity;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MongoDBDataAccess<T extends Entity> {
	private final Caching<String, T> cache;
	private final VolatileCachingSearchResult<String, T> searchResult;

	public MongoDBDataAccess(String alias, Class type, String collectionName) {
		this(alias, type, collectionName, "_id");
	}

	public MongoDBDataAccess(String alias, Class type, String collectionName, String key) {
		this(alias, type, collectionName, key, false);
	}

	public MongoDBDataAccess(String alias, Class type, String collectionName, String key, boolean mapKeyToObjectId) {
		this.cache = new Caching<>(alias, key, type, collectionName, GlobalVariables.CACHE_CONFIG);
		this.searchResult = new VolatileCachingSearchResult("searchResult", GlobalVariables.VOLATILE_CACHE_CONFIG,
				cache);
		if (mapKeyToObjectId) {
			this.cache.setWorkaround_keyMapper(ObjectId::new);
		}
		this.cache.clear();
	}

	public T get(String key) {
		return cache.get(key);
	}

	public void set(T value) {
		cache.set(value.get_id(), value);
	}

	public void delete(String key) {
		this.cache.del(key);
	}

	public List<T> find(int amountOfResults, int skip, Document filter) {
		return this.find(amountOfResults, skip, filter, new Document());
	}

	public List<T> find(int amountOfResults, int skip, Document filter, Document sort) {
		// direct access to database
		List<String> keys = cache.find(filter, sort, skip, amountOfResults, Entity::get_id, false);
		return getValues(keys);
				
		/* disabled not working properly with filter.hashCode()
		return searchResult.getValues(0, filter.hashCode(),
				(mul) -> cache.find(filter, sort, skip, amountOfResults, Entity::get_id, true));
			*/
	}
	
	protected List<T> getValues(List<String> keys) {
		List<T> result = new LinkedList<>();

		if (keys != null)
			for (String key : keys)
				result.add(cache.get(key));

		return result;
	}

	public void clear() {
		this.cache.getKeys().forEach(this.cache::del);
	}

	public Caching<String, T> getCache() {
		return this.cache;
	}
}
