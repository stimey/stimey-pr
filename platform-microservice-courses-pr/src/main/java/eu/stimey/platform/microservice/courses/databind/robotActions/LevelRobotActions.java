package eu.stimey.platform.microservice.courses.databind.robotActions;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.ArrayList;
import java.util.List;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = LearningObjectRobotActions.class, name = "LearningObjectRobotActions") })
public class LevelRobotActions extends RobotActions {
	public RobotAction onNext;
	public List<IdleRobotAction> idleActions;

	public LevelRobotActions() {
		super();
		this.onNext = new RobotAction();
		this.idleActions = new ArrayList<>();
		this.idleActions.add(new IdleRobotAction());
	}
}
