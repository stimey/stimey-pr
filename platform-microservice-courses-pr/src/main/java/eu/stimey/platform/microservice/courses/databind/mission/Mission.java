package eu.stimey.platform.microservice.courses.databind.mission;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.stimey.platform.microservice.courses.databind.DocumentEntity;
import eu.stimey.platform.microservice.courses.databind.Copyable;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.robotActions.MissionRobotActions;
import eu.stimey.platform.microservice.courses.databind.robotActions.RobotActions;
import eu.stimey.platform.microservice.courses.databind.learningObject.Level;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.MissionMetadata;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Mission extends DocumentEntity {

	public final MissionMetadata missionMetadata;
	public final List<String> students;
	public List<Level> levels;
	public final List<String> moderators;
	public List<String> communities;
	public List<String> resources;
	public RobotActions robotActions;

	public Mission() {
		this.missionMetadata = new MissionMetadata();
		this.students = new LinkedList<>();
		this.levels = new LinkedList<>();
		this.moderators = new LinkedList<>();
		this.resources = new LinkedList<>();
		this.robotActions = new MissionRobotActions();
	}

	public Mission(Mission other, String authorId, String inspiredBy) {
		this.set_id(new ObjectId().toHexString());

		this.missionMetadata = other.missionMetadata;
		this.missionMetadata.generalMetadata.title = "Copy of ".concat(this.missionMetadata.generalMetadata.title);
		this.missionMetadata.generalMetadata.author = authorId;
		this.missionMetadata.generalMetadata.inspiredBy = inspiredBy;
		this.missionMetadata.generalMetadata.creationDate = new Date();

		// TODO: should students be copied to?
		// this.students = other.students;
		this.students = new ArrayList<>();

		// TODO: should moderators be copied to?
		// this.students = other.students;
		this.moderators = new ArrayList<>();

		this.levels = other.levels;
		this.communities = other.communities;
		this.resources = other.resources;
		this.robotActions = other.robotActions;
	}

	@JsonIgnore
	public void publish() {
		if (this.missionMetadata.generalMetadata.published)
			throw new IllegalStateException("mission is already published");
		this.missionMetadata.generalMetadata.published = true;
	}

	@JsonIgnore
	public List<String> getLearningObjectIds() {
		List<String> learningObjectIds = new ArrayList<>();
		for (Level level : this.levels) {
			learningObjectIds.addAll(level.learningObjectIds);
		}
		return learningObjectIds;
	}

	@JsonIgnore
	public int getMissionLength() {
		int length = 0;
		for (Level level : this.levels) {
			length += level.learningObjectIds.size() + 1;
		}
		return length;
	}

	@Override
	public String toString() {
		return "Mission{" + "missionMetadata=" + missionMetadata + ", students=" + students + ", levels=" + levels
				+ ", moderators=" + moderators + '}';
	}
}
