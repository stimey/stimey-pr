package eu.stimey.platform.microservice.courses.databind.learningObject.atomic.multimedia;

import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;

public class Task extends MultiMediaLearningObject {

	public Task() {
		super();
	}

	public Task(Task original) {
		super(original);
	}

	@Override
	public LearningObject copy(LearningObjectsService learningObjectsService) {
		LearningObject copy = new Task(this);
		learningObjectsService.setLearningObject(copy);
		return copy;
	}
}
