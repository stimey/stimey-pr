package eu.stimey.platform.microservice.courses.databind.mission.missionMetadata;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.stimey.platform.microservice.courses.utils.DateDeserializer;
import eu.stimey.platform.microservice.courses.utils.DateSerializer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EducationalMetadata {

	public final Reward reward;
	public Stage stage;
	public Difficulty difficulty;
	public final List<Topic> topics;
	public Template template;
	public boolean isPublic;
	public boolean isSelfPaced;
	public Date startDate;
	public Date finishDate;
	public double suggestedTime;

	public EducationalMetadata() {
		this.topics = new ArrayList<>();
		this.reward = new Reward();
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getStartDate() {
		return startDate;
	}

	@JsonDeserialize(using = DateDeserializer.class)
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getFinishDate() {
		return finishDate;
	}

	@JsonDeserialize(using = DateDeserializer.class)
	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	@JsonIgnore
	public double getDuration() {
		if (this.isSelfPaced)
			return suggestedTime * 1000;
		if (startDate == null || finishDate == null)
			return 0;
		return finishDate.getTime() - startDate.getTime();
	}

}
