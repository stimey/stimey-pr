package eu.stimey.platform.microservice.courses.databind.robotActions;

public class LearningObjectRobotActions extends LevelRobotActions {

	public RobotAction onDiscussionPost;

	public LearningObjectRobotActions() {
		super();
		this.onDiscussionPost = new RobotAction();
	}
}
