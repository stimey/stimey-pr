package eu.stimey.platform.microservice.courses.rest.dataTransfer.responses;

import eu.stimey.platform.microservice.courses.databind.mission.Mission;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MissionListResponse {
	public final List<MissionResponse> missions;

	public MissionListResponse(Map<Mission, String> missions) {
		this.missions = new LinkedList<>();
		for (Mission mission : missions.keySet()) {
			this.missions.add(new MissionResponse(mission, missions.get(mission)));
		}
	}

	public MissionListResponse() {
		this.missions = new ArrayList<>();
	}

	public MissionListResponse(List<MissionResponse> missionResponses) {
		this.missions = missionResponses;
	}
}
