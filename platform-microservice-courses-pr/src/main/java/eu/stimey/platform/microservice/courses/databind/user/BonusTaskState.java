package eu.stimey.platform.microservice.courses.databind.user;

public class BonusTaskState {
	public final boolean accepted;
	public final boolean done;
	public String discussionId;
	public String socialFeedbackText;

	public BonusTaskState() {
		this.accepted = false;
		this.done = false;
	}
}
