package eu.stimey.platform.microservice.courses.beans;

import eu.stimey.platform.microservice.courses.databind.challengeResult.ChallengeStudentInput;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class ChallengeResultService {

	private final MongoDBDataAccess<ChallengeStudentInput> challengeResultDataAccess;

	public ChallengeResultService() {
		challengeResultDataAccess = new MongoDBDataAccess<>("ms_courses_challengeResults", ChallengeStudentInput.class,
				"challengeResults", "_id");
	}

	public ChallengeStudentInput getChallengeResult(String challengeId, String studentId) {
		return this.challengeResultDataAccess.get(challengeId.concat(studentId));
	}

	public ChallengeStudentInput getChallengeResult(String challengeResultId) {
		return this.challengeResultDataAccess.get(challengeResultId);
	}

	public void setChallengeResult(ChallengeStudentInput challengeStudentInput) {
		this.challengeResultDataAccess.set(challengeStudentInput);
	}

}
