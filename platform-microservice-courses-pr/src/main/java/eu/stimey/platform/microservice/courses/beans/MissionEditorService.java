package eu.stimey.platform.microservice.courses.beans;

import eu.stimey.platform.library.data.access.databind.Message;
import eu.stimey.platform.library.utils.StimeyLogger;
import eu.stimey.platform.microservice.courses.databind.mission.Mission;
import eu.stimey.platform.microservice.courses.databind.user.Teacher;

public class MissionEditorService {

	private final MissionsService missionsService;
	private final TeacherService teacherService;

	public MissionEditorService(MissionsService missionsService, TeacherService teacherService) {
		this.missionsService = missionsService;
		this.teacherService = teacherService;
	}

	public void setMission(String authorId, Mission mission) {
		this.missionsService.setMission(mission);
		this.teacherService.addMissionToSavedMissions(authorId, mission.get_id());
		StimeyLogger.logger().info("MissionEditorService: sending keywords to api-gateway-storage:"
				+ mission.missionMetadata.generalMetadata.keywords);
		this.missionsService.getMissionDataAccess().getCache().publish("addKeywords",
				new Message(null, mission.missionMetadata.generalMetadata.keywords));
	}

	public boolean deleteMission(String authorId, String missionId) {
		Teacher teacher = this.teacherService.getTeacher(authorId);
		boolean removedMissionIdFromTeacher = teacher.removeMission(missionId);
		if (removedMissionIdFromTeacher) {
			this.missionsService.delMission(missionId);
			this.teacherService.setTeacher(teacher);
		}
		return removedMissionIdFromTeacher;
	}

}
