package eu.stimey.platform.microservice.courses.databind;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.stimey.platform.microservice.courses.databind.DocumentEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KhanAcademyVideoLesson extends DocumentEntity {

	public final List<String> topicPathKeywords;
	public String searchKeywords;

	public String thumbnailDefaultUrl;
	public String thumbnailFilteredUrl;
	public String videoTitle;
	public String videoDescription;
	public String videoYoutubeUrl;
	public String videoYoutubeEmbeddedUrl;
	public String videoMP4DownloadUrl;
	public String videoMP4LowQualityDownloadUrl;
	public String videoCreationDate;

	public KhanAcademyVideoLesson() {
		topicPathKeywords = new ArrayList<>();
//        searchKeywords = new ArrayList<>();
	}

	public void buildSearchKeywords() {
		ArrayList<String> searchKeywordsVideoTitle;
		ArrayList<String> searchKeywordsVideoDescription;

		searchKeywordsVideoTitle = new ArrayList<>(Arrays.asList((videoTitle.split("\\W+"))));
		searchKeywords = searchKeywordsVideoTitle.toString().replaceAll("[\\[\\]]", "");
		searchKeywords += ", ";
//        searchKeywords.addAll(searchKeywordsVideoTitle);

		if (videoDescription != null && !videoDescription.isEmpty()) {
			searchKeywordsVideoDescription = new ArrayList<>(Arrays.asList((videoDescription.split(" "))));
			searchKeywords += searchKeywordsVideoDescription.toString().replaceAll("[\\[\\]]", "");
//            searchKeywords.addAll(searchKeywordsVideoDescription);
		}
	}

	public void setVideoTitle(String videoTitle) {
		this.videoTitle = videoTitle;
	}

	public void setVideoDescription(String videoDescription) {
		this.videoDescription = videoDescription;
	}

	@JsonIgnore
	public void setVideoYoutubeUrls(String videoYoutubeId) {
		this.videoYoutubeUrl = "https://www.youtube.com/watch?v=" + videoYoutubeId;
		this.videoYoutubeEmbeddedUrl = "https://www.youtube.com/embed/" + videoYoutubeId;
	}

	public void setVideoMP4DownloadUrl(String videoMP4DownloadUrl) {
		this.videoMP4DownloadUrl = videoMP4DownloadUrl;
	}

	public void setVideoMP4LowQualityDownloadUrl(String videoMP4LowQualityDownloadUrl) {
		this.videoMP4LowQualityDownloadUrl = videoMP4LowQualityDownloadUrl;
	}

	public void setVideoCreationDate(String dateFmt) {
		this.videoCreationDate = dateFmt;
	}

	public void setTopicPathKeywords(List<String> topicTitleKeywords) {
		this.topicPathKeywords.addAll(topicTitleKeywords);
	}

	public void setThumbnailDefaultUrl(String thumbnailDefaultUrl) {
		this.thumbnailDefaultUrl = thumbnailDefaultUrl;
	}

	public void setThumbnailFilteredUrl(String thumbnailFilteredUrl) {
		this.thumbnailFilteredUrl = thumbnailFilteredUrl;
	}

	@Override
	public String toString() {
		return "VideoLesson{" + "topicPathKeywords=" + topicPathKeywords + ", searchKeywords=" + searchKeywords
				+ ", thumbnailDefaultUrl='" + thumbnailDefaultUrl + '\'' + ", thumbnailFilteredUrl='"
				+ thumbnailFilteredUrl + '\'' + ", videoTitle='" + videoTitle + '\'' + ", videoDescription='"
				+ videoDescription + '\'' + ", videoYoutubeUrl='" + videoYoutubeUrl + '\''
				+ ", videoYoutubeEmbeddedUrl='" + videoYoutubeEmbeddedUrl + '\'' + ", videoMP4DownloadUrl='"
				+ videoMP4DownloadUrl + '\'' + ", videoMP4LowQualityDownloadUrl='" + videoMP4LowQualityDownloadUrl
				+ '\'' + ", videoCreationDate='" + videoCreationDate + '\'' + '}';
	}
}