package eu.stimey.platform.microservice.courses.beans;

import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.Level;
import eu.stimey.platform.microservice.courses.databind.mission.Mission;

import java.util.ArrayList;
import java.util.List;

public class MissionCopyService {

	private final MissionsService missionsService;
	private final LearningObjectsService learningObjectsService;

	public MissionCopyService(MissionsService missionsService, LearningObjectsService learningObjectsService) {
		this.missionsService = missionsService;
		this.learningObjectsService = learningObjectsService;
	}

	/**
	 *
	 * @param missionIdOfMissionToCopy
	 * @param newAuthor                The id of the author who wants to copy the
	 *                                 mission.
	 * @param oldAuthor                author of mission that should be copied.
	 *                                 Oldauthor needs to be saved in order to show
	 *                                 it inspired by field.
	 * @return copy of mission
	 */
	public Mission copyMission(String missionIdOfMissionToCopy, String newAuthor, String oldAuthor) {
		final Mission toBeCopied = this.missionsService.getMissionById(missionIdOfMissionToCopy);
		Mission copy = new Mission(toBeCopied, newAuthor, oldAuthor);
		for (Level level : copy.levels) {
			level.learningObjectIds = this.copyLearningObjects(level.learningObjectIds);
		}
		return copy;
	}

	private List<String> copyLearningObjects(List<String> learningObjectIds) {
		List<String> idsOfCopiedLearningObjects = new ArrayList<>();
		for (String toCopy : learningObjectIds) {
			idsOfCopiedLearningObjects.add(this.copyLearningObject(toCopy));
		}
		return idsOfCopiedLearningObjects;
	}

	private String copyLearningObject(String learningObjectId) {
		LearningObject toCopy = this.learningObjectsService.getById(learningObjectId);
		LearningObject copied = toCopy.copy(this.learningObjectsService);
		return copied.get_id();
	}
}
