package eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.challengeAnalysis;

import java.util.ArrayList;
import java.util.List;

public class ChallengeResultResponse {

	private String missionName;
	private String challengeName;
	private String studentName;
	private String challengeStartDate;
	private String challengeEndDate;
	private List<StudentAnswer> studentAnswers;

	public ChallengeResultResponse() {
		this.studentAnswers = new ArrayList<>();
	}

	public ChallengeResultResponse(String missionName, String challengeName, String studentName,
			String challengeStartDate, String challengeEndDate) {
		this(missionName, challengeName, studentName, challengeStartDate, challengeEndDate, null);
		this.studentAnswers = new ArrayList<>();
	}

	public ChallengeResultResponse(String missionName, String challengeName, String studentName,
			String challengeStartDate, String challengeEndDate, List<StudentAnswer> studentAnswers) {
		this.missionName = missionName;
		this.challengeName = challengeName;
		this.studentName = studentName;
		this.challengeStartDate = challengeStartDate;
		this.challengeEndDate = challengeEndDate;
		this.studentAnswers = studentAnswers;
	}

	public String getMissionName() {
		return missionName;
	}

	public void setMissionName(String missionName) {
		this.missionName = missionName;
	}

	public String getChallengeName() {
		return challengeName;
	}

	public void setChallengeName(String challengeName) {
		this.challengeName = challengeName;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getChallengeStartDate() {
		return challengeStartDate;
	}

	public void setChallengeStartDate(String challengeStartDate) {
		this.challengeStartDate = challengeStartDate;
	}

	public String getChallengeEndDate() {
		return challengeEndDate;
	}

	public void setChallengeEndDate(String challengeEndDate) {
		this.challengeEndDate = challengeEndDate;
	}

	public List<StudentAnswer> getStudentAnswers() {
		return studentAnswers;
	}

	public void setStudentAnswers(List<StudentAnswer> studentAnswers) {
		this.studentAnswers = studentAnswers;
	}
}
