package eu.stimey.platform.microservice.courses.databind.learningObject.atomic.multimedia;

import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.AtomicLearningObject;

public class Assignment extends MultiMediaLearningObject {

	public Assignment() {
		super();
	}

	public Assignment(Assignment original) {
		super(original);
	}

	@Override
	public LearningObject copy(LearningObjectsService learningObjectsService) {
		LearningObject copy = new Assignment(this);
		learningObjectsService.setLearningObject(copy);
		return copy;
	}
}
