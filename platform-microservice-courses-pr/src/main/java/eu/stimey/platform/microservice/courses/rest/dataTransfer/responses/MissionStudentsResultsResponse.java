package eu.stimey.platform.microservice.courses.rest.dataTransfer.responses;

import java.util.List;

public class MissionStudentsResultsResponse {
	private final GamificationPoints gamificationPoints;
	private final List<StudentResult> studentsResults;

	public MissionStudentsResultsResponse(GamificationPoints gamificationPoints, List<StudentResult> studentsResults) {
		super();
		this.gamificationPoints = gamificationPoints;
		this.studentsResults = studentsResults;
	}

	public GamificationPoints getGamificationPoints() {
		return gamificationPoints;
	}

	public List<StudentResult> getStudentsResults() {
		return studentsResults;
	}
}
