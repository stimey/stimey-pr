package eu.stimey.platform.microservice.courses.databind.mission.missionMetadata;

public class MissionMetadata {

	public GeneralMetadata generalMetadata;
	public EducationalMetadata educationalMetadata;
	public InformationAboutMission informationAboutMission;

	public MissionMetadata() {
		this.generalMetadata = new GeneralMetadata();
		this.educationalMetadata = new EducationalMetadata();
		this.informationAboutMission = new InformationAboutMission();
	}

	@Override
	public String toString() {
		return "MissionMetadata{" + "generalMetadata=" + generalMetadata + ", educationalMetadata="
				+ educationalMetadata + ", informationAboutMission=" + informationAboutMission + '}';
	}
}
