package eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question;

import eu.stimey.platform.microservice.courses.databind.challengeResult.StudentInput;

public interface Checkable {
	boolean check(StudentInput studentInput);
}
