package eu.stimey.platform.microservice.courses.beans;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.data.access.CachingConfiguration;
import eu.stimey.platform.library.data.access.VolatileCachingConfiguration;
import org.bson.types.ObjectId;

/**
 * Global variable settings for MongoDB and Redis
 *
 */
public class GlobalVariables {
	// taken from ServletContextListener from Auth microservice
	// username = alexa
	// password = 12345678
	public static final ObjectId TEST_STUDENT_ID = new ObjectId("5a156e3a394d09ca9f4f7847");

	// username = teacher
	// password = teacher
	public static final ObjectId TEST_TEACHER_ID = new ObjectId("5b90d93dfb24ce3454829a4c");

	public static final ObjectId ARCHIMEDIS_ID = new ObjectId("5a0ee2427e9d6459996d3c0b");

	public static final CachingConfiguration CACHE_CONFIG = new CachingConfiguration(
			RedisAMQPService.getService().getJedisPool(), RedisAMQPService.getService().getRabbitmqConnection(),
			MongoDBService.getService().getClient(), MongoDBService.getService().getDatabaseName());

	public static final VolatileCachingConfiguration VOLATILE_CACHE_CONFIG = new VolatileCachingConfiguration(
			RedisAMQPService.getService().getJedisPool(), RedisAMQPService.getService().getRabbitmqConnection());
}
