package eu.stimey.platform.microservice.courses.rest.resources;

import eu.stimey.platform.microservice.courses.beans.KhanAcademyVideoService;
import eu.stimey.platform.microservice.courses.databind.KhanAcademyVideoLesson;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.KhanAcademyResponseShort;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/khanAcademyVideos")
public class KhanAcademyVideoResource {

	@Inject
	private KhanAcademyVideoService khanAcademyVideoService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getKhanAcademyVideos(@QueryParam("keywords") @DefaultValue("") String keywords,
			@QueryParam("offset") @DefaultValue("0") Integer offset,
			@QueryParam("limit") @DefaultValue("30") Integer limit) {
		Response response;

		System.out.println(keywords + "offset: " + offset + "   limit: " + limit);

//        List<KhanAcademyVideoLesson> khanAcademyVideoLessons = khanAcademyVideoService.searchVideosByKeywords(keywords);
		List<KhanAcademyVideoLesson> khanAcademyVideoLessons = khanAcademyVideoService
				.getKhanAcademyVideoLessonList(khanAcademyVideoService.searchKeywordsFilter(keywords), offset, limit);

		if (khanAcademyVideoLessons.isEmpty()) {
			response = Response.status(Response.Status.NO_CONTENT).build();

		} else {
			List<KhanAcademyResponseShort> khanAcademyResponseShortList = new ArrayList<>();

			for (KhanAcademyVideoLesson khanAcademyVideoLesson : khanAcademyVideoLessons) {
				KhanAcademyResponseShort videoResponse = new KhanAcademyResponseShort(khanAcademyVideoLesson.videoTitle,
						khanAcademyVideoLesson.videoDescription, khanAcademyVideoLesson.thumbnailDefaultUrl,
						khanAcademyVideoLesson.videoYoutubeUrl, khanAcademyVideoLesson.videoYoutubeEmbeddedUrl,
						khanAcademyVideoLesson.videoCreationDate);
				khanAcademyResponseShortList.add(videoResponse);
			}
			response = Response.status(Response.Status.OK).entity(khanAcademyResponseShortList).build();
		}
		return response;
	}
}