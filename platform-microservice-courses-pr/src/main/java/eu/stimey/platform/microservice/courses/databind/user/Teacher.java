package eu.stimey.platform.microservice.courses.databind.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.stimey.platform.microservice.courses.databind.Entity;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Teacher implements Entity {
	public String userId;
	public final List<String> activeMissions;
	public final List<String> completedMissions;
	public final List<String> savedMissions;
	public final List<String> pendingModeratorInvitation;

	public Teacher() {
		this.activeMissions = new LinkedList<>();
		this.savedMissions = new LinkedList<>();
		this.completedMissions = new LinkedList<>();
		this.pendingModeratorInvitation = new LinkedList<>();
	}

	public Teacher(String teacherId) {
		this.userId = teacherId;
		this.activeMissions = new LinkedList<>();
		this.savedMissions = new LinkedList<>();
		this.completedMissions = new LinkedList<>();
		this.pendingModeratorInvitation = new LinkedList<>();
	}

	@JsonIgnore
	public void publish(String missionId) {
		if (missionId == null || missionId.isEmpty())
			throw new IllegalArgumentException("missionId is null");
		if (this.activeMissions.contains(missionId))
			throw new IllegalArgumentException("mission already published");
		if (!this.savedMissions.contains(missionId))
			throw new IllegalArgumentException(this + " does not have mission with id=" + missionId);
		this.savedMissions.remove(missionId);
		this.activeMissions.add(missionId);
	}

	@JsonIgnore
	public boolean hasMissions() {
		return this.activeMissions.size() > 0 || this.savedMissions.size() > 0 || this.completedMissions.size() > 0
				|| this.pendingModeratorInvitation.size() > 0;
	}

	@JsonIgnore
	public void removeNotExistingMissions(List<String> missionIds) {
		this.pendingModeratorInvitation.removeAll(missionIds);
		this.completedMissions.removeAll(missionIds);
		this.savedMissions.removeAll(missionIds);
		this.activeMissions.removeAll(missionIds);
	}

	@JsonIgnore
	public boolean removeMission(String missionId) {
		return this.savedMissions.remove(missionId) || this.activeMissions.remove(missionId);
	}

	@JsonIgnore
	public List<String> allMissions() {
		List<String> allMissions = new ArrayList<>();
		allMissions.addAll(this.savedMissions);
		allMissions.addAll(this.activeMissions);
		allMissions.addAll(this.completedMissions);
		allMissions.addAll(this.pendingModeratorInvitation);
		return allMissions;
	}

	@Override
	public String get_id() {
		return this.userId;
	}

	@Override
	public void set_id(String _id) {
		this.userId = _id;
	}

	@Override
	public String toString() {
		return "Teacher{" + "userId='" + userId + '\'' + ", setActiveMissions=" + activeMissions
				+ ", setCompletedMissions=" + completedMissions + ", setSavedMissions=" + savedMissions
				+ ", pendingModeratorInvitation=" + pendingModeratorInvitation + '}';
	}
}
