package eu.stimey.platform.microservice.courses.databind.learningObject.composite;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ChallengeDuration {
	private final long minutes;
	private final long seconds;

	@JsonCreator
	public ChallengeDuration() {
		this.minutes = 0;
		this.seconds = 0;
	}

	@JsonCreator
	public ChallengeDuration(@JsonProperty("minutes") long minutes, @JsonProperty("seconds") long seconds) {
		this.minutes = minutes;
		this.seconds = seconds;
	}

	public long getMinutes() {
		return minutes;
	}

	public long getSeconds() {
		return seconds;
	}

}
