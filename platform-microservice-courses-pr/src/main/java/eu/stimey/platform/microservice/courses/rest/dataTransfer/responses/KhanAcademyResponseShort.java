package eu.stimey.platform.microservice.courses.rest.dataTransfer.responses;

public class KhanAcademyResponseShort {
	public String videoTitle;
	public String videoDescription;
	public String thumbnailDefaultUrl;
	public String videoYoutubeUrl;
	public String videoYoutubeEmbeddedUrl;
	public String videoCreationDate;

	public KhanAcademyResponseShort(String videoTitle, String videoDescription, String thumbnailDefaultUrl,
			String videoYoutubeUrl, String videoYoutubeEmbeddedUrl, String videoCreationDate) {
		this.videoTitle = videoTitle;
		this.videoDescription = videoDescription;
		this.thumbnailDefaultUrl = thumbnailDefaultUrl;
		this.videoYoutubeUrl = videoYoutubeUrl;
		this.videoYoutubeEmbeddedUrl = videoYoutubeEmbeddedUrl;
		this.videoCreationDate = videoCreationDate;
	}
}
