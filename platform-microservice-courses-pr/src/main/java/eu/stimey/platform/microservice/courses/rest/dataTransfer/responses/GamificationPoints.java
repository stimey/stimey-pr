package eu.stimey.platform.microservice.courses.rest.dataTransfer.responses;

public class GamificationPoints {
	private final String missionName;
	private final int knowledge;
	private final int social;
	private final int creativity;

	public GamificationPoints() {
		this(0, 0, 0, "");
	}

	public GamificationPoints(int knowledge, int social, int creativity) {
		this(knowledge, social, creativity, "");
	}

	public GamificationPoints(int knowledge, int social, int creativity, String missionName) {
		this.knowledge = knowledge;
		this.social = social;
		this.creativity = creativity;
		this.missionName = missionName;
	}

	public int getKnowledge() {
		return knowledge;
	}

	public int getSocial() {
		return social;
	}

	public int getCreativity() {
		return creativity;
	}
}
