package eu.stimey.platform.microservice.courses.beans;

import eu.stimey.platform.microservice.courses.beans.filter.MissionsFilter;
import eu.stimey.platform.microservice.courses.databind.mission.Mission;
import org.bson.Document;

import java.util.LinkedList;
import java.util.List;

public class MissionsService {

	private final MongoDBDataAccess<Mission> missionDataAccess;

	public MissionsService() {
		missionDataAccess = new MongoDBDataAccess<>("ms_courses_courses", Mission.class, "courses", "_id", true);
		missionDataAccess.getCache().createIndex(MissionsFilter.KEYWORDS_PATH);
		missionDataAccess.getCache().createIndex(MissionsFilter.DIFFICULTY_PATH);
		missionDataAccess.getCache().createIndex(MissionsFilter.STAGE_PATH);
		missionDataAccess.getCache().createIndex(MissionsFilter.TOPICS_PATH);
	}

	public void setMission(Mission mission) {
		missionDataAccess.set(mission);
	}

	public void delMission(String missionId) {
		missionDataAccess.delete(missionId);
	}

	public Mission getMissionById(String missionId) {
		return this.missionDataAccess.get(missionId);
	}

	public List<Mission> getMissionsByIds(List<String> missionIds) {
		List<Mission> missions = new LinkedList<>();
		for (String missionId : missionIds) {
			missions.add(this.getMissionById(missionId));
		}
		return missions;
	}

	public List<Mission> findMissions(int limit, int offset, Document filter) {
		return this.missionDataAccess.find(limit, offset, filter);
	}

	public List<Mission> findMissions(int limit, int offset, Document filter, Document sort) {
		return this.missionDataAccess.find(limit, offset, filter, sort);
	}

	public void publishMission(String missionId) {
		Mission mission = this.getMissionById(missionId);
		mission.publish();
		this.setMission(mission);
	}

	public MongoDBDataAccess<Mission> getMissionDataAccess() {
		return this.missionDataAccess;
	}
}
