package eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question;

import java.util.Objects;

public class Answer {

	public final String answerText;
	public final String explanation;
	public final boolean isCorrect;

	public Answer() {
		this("Click to edit.", null, false);
	}

	public Answer(String answerText, String explanation, boolean isCorrect) {
		this.answerText = answerText;
		this.explanation = explanation;
		this.isCorrect = isCorrect;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Answer answer = (Answer) o;
		return isCorrect == answer.isCorrect && Objects.equals(answerText, answer.answerText);
	}

	@Override
	public int hashCode() {
		return Objects.hash(answerText, isCorrect);
	}
}
