package eu.stimey.platform.microservice.courses.startup;

import eu.stimey.platform.library.core.startup.DefaultGlobal;

/**
 * 
 * Global settings (environment variables)
 *
 */
public final class Global extends DefaultGlobal {
	public static boolean USE_TEST_DB = false;
	public static boolean UPDATE_KHAN_ACEDEMY_DATA = false;
	
	public static void startup() {
		DefaultGlobal.startup();
		
		UPDATE_KHAN_ACEDEMY_DATA = Global.DOCKER;
	}
}
