package eu.stimey.platform.microservice.courses.startup;

import eu.stimey.platform.microservice.courses.databind.Entity;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class TestDataUtils {

	public static <T> T getRandomElement(List<T> list) {
		Random rand = new Random();
		return list.get(rand.nextInt(list.size()));
	}

	public static <T extends Entity> String getIdRandomElement(List<T> list) {
		Random rand = new Random();
		return list.get(rand.nextInt(list.size())).get_id();
	}

	public static <T> List<T> getRandomElements(List<T> list, int amount) {
		List<T> result = new LinkedList<>();
		for (int i = 0; i < amount; i++) {
			result.add(getRandomElement(list));
		}
		return result;
	}

	public static <T extends Entity> List<String> getIdsOfRandomElements(List<T> elements) {
		List<String> ids = new LinkedList<>();
		elements.forEach(e -> ids.add(e.get_id()));
		return ids;
	}
}
