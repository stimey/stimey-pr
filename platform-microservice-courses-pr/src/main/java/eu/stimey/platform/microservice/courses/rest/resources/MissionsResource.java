package eu.stimey.platform.microservice.courses.rest.resources;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;
import eu.stimey.platform.library.utils.StimeyLogger;
import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.microservice.courses.beans.*;
import eu.stimey.platform.microservice.courses.beans.filter.MissionsFilter;
import eu.stimey.platform.microservice.courses.databind.challengeResult.*;
import eu.stimey.platform.microservice.courses.databind.Discussion;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question.*;
import eu.stimey.platform.microservice.courses.databind.learningObject.composite.Challenge;
import eu.stimey.platform.microservice.courses.databind.learningObject.Level;
import eu.stimey.platform.microservice.courses.databind.mission.Mission;
import eu.stimey.platform.microservice.courses.databind.user.MissionState;
import eu.stimey.platform.microservice.courses.databind.user.Student;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.CreateMissionData;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.MissionOverview;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.DiscussionsResponse;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.GamificationPoints;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.challengeAnalysis.ChallengeResultResponse;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.MissionListResponse;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.MissionResponse;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.MissionStudentsResultsResponse;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.StudentResult;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.challengeAnalysis.StudentAnswer;
import eu.stimey.platform.microservice.courses.startup.Global;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.bson.Document;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Path("/missions")
public class MissionsResource {

	public static final String DEFAULT_OFFSET = "0";
	public static final String DEFAULT_LIMIT = "256";

	@Inject
	private MissionsService missionsService;

	@Inject
	private UserSharedService userSharedService;

	@Inject
	private MissionEditorService missionEditorService;

	@Inject
	private MissionCopyService missionCopyService;

	@Inject
	private MissionToAuthorNamesMapper missionToAuthorNamesMapper;

	@Inject
	private LearningObjectsService learningObjectsService;

	@Inject
	private DiscussionService discussionService;

	@Inject
	private ChallengeResultService challengeResultService;

	@Inject
	private StudentService studentService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMissions(@Context ContainerRequestContext context,
			@QueryParam("o") @DefaultValue(DEFAULT_OFFSET) int offset,
			@QueryParam("l") @DefaultValue(DEFAULT_LIMIT) int limit, @QueryParam("selectedLanguage") String language,
			@QueryParam("searchString") String searchString, @QueryParam("topic[]") List<String> topics,
			@QueryParam("stage[]") List<String> stages, @QueryParam("difficulty[]") List<String> difficulties,
			@QueryParam("newMissions") @DefaultValue("false") boolean newMissions) {

		final Document filter = new MissionsFilter().language(language).query(searchString).topics(topics)
				.difficulties(difficulties).stages(stages).toDocument();

		List<Mission> missions;
		if (newMissions) {
			missions = missionsService.findMissions(limit, offset, filter,
					new Document("missionMetadata.creationDate", -1));
		} else {
			missions = missionsService.findMissions(limit, offset, filter, new Document());
		}

		if (missions.size() == 0)
			return Response.status(Status.NO_CONTENT).build();

		MissionListResponse missionListResponse = new MissionListResponse();
		for (Mission mission : missions) {
			String authorName = this.missionToAuthorNamesMapper.getAuthorNameOfMission(mission);
			Triple<Integer, Integer, Integer> typesOfLearningObjects = this.learningObjectsService
					.typesOfLearningObjects(mission.getLearningObjectIds());
			missionListResponse.missions.add(new MissionResponse(mission, authorName, typesOfLearningObjects.getLeft(),
					typesOfLearningObjects.getMiddle(), typesOfLearningObjects.getRight()));
		}
		return Response.status(Status.OK).entity(missionListResponse).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createMission(@Context ContainerRequestContext context, CreateMissionData missionMetadata) {
		String authorId = (String) context.getProperty("userid");
		Mission mission = new Mission();
		mission.missionMetadata.generalMetadata = missionMetadata.generalMetadata;
		mission.missionMetadata.generalMetadata.author = authorId;
		mission.missionMetadata.generalMetadata.setCreationDate(new Date());
		mission.missionMetadata.educationalMetadata = missionMetadata.educationalMetadata;
		missionEditorService.setMission(authorId, mission);
		return Response.status(Status.OK).entity(mission).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response setMission(@Context ContainerRequestContext context, Mission mission) {
		this.missionsService.setMission(mission);
		return Response.status(Status.OK).entity(mission).build();
	}

	@POST
	@Path("/{missionId}/copy")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response copyMission(@Context ContainerRequestContext context, @PathParam("missionId") String missionId) {
		String authorId = (String) context.getProperty("userid");
		Mission toCopy = this.missionsService.getMissionById(missionId);
		String oldAuthor = toCopy.missionMetadata.generalMetadata.author;
		String oldAuthorName = this.userSharedService.getUser(oldAuthor).username;
		Mission copy = this.missionCopyService.copyMission(toCopy.get_id(), authorId, oldAuthorName);

		this.missionEditorService.setMission(authorId, copy);

		return Response.status(Status.CREATED).entity(copy).build();
	}

	@PUT
	@Path("/{missionId}/generalMissionMetadata/avatarImageId")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response changeAvatarImageId(@Context ContainerRequestContext context,
			@PathParam("missionId") String missionId, String avatarImageId) {
		Mission mission = missionsService.getMissionById(missionId);
		mission.missionMetadata.generalMetadata.avatarImageId = avatarImageId;
		this.missionsService.setMission(mission);
		return Response.status(Status.OK).build();
	}

	@DELETE
	@Path("/{missionId}")
	public Response deleteMission(@Context ContainerRequestContext context, @PathParam("missionId") String missionId) {
		String authorId = (String) context.getProperty("userid");
		if (this.missionEditorService.deleteMission(authorId, missionId))
			return Response.status(Status.ACCEPTED).build();
		else
			return Response.status(Status.UNAUTHORIZED).build();
	}

	@GET
	@Path("/{missionId}/author")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMissionAuthor(@PathParam("missionId") String missionId) {
		Mission mission = this.missionsService.getMissionById(missionId);

		String authorId = mission.missionMetadata.generalMetadata.author;

		return Response.status(Status.OK).entity("{\"authorid\":\"" + authorId + "\"}").build();
	}

	@GET
	@Path("/{missionId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMission(@PathParam("missionId") String missionId) {
		Mission mission = this.missionsService.getMissionById(missionId);

		// Reminder: authorId is replaced with authorName
		String authorId = mission.missionMetadata.generalMetadata.author;
		String authorName = this.userSharedService.getUser(authorId).username;
		mission.moderators.add(authorId);
		mission.missionMetadata.generalMetadata.author = authorName;

		// Reminder: inspiredby authorId is replaced with authorName
		/*
		 * String inspiredByAuthorId =
		 * mission.missionMetadata.generalMetadata.inspiredBy; if (inspiredByAuthorId !=
		 * null && !inspiredByAuthorId.isEmpty()) {
		 * System.out.println(mission.missionMetadata.generalMetadata.inspiredBy);
		 * 
		 * String inspiredByAuthorName =
		 * this.userSharedService.getUser(inspiredByAuthorId).username;
		 * mission.missionMetadata.generalMetadata.inspiredBy = inspiredByAuthorName; }
		 */

		return Response.status(Status.OK).entity(mission).build();
	}

	// Reuse partially: MissionsResource.getOverallGamificationPointsOfMission
	// Reuse partially: StudentResource.getProgress
	@GET
	@Path("/{missionId}/studentsResults")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStudentsResultsOfMission(@PathParam("missionId") String missionId) {
		Mission mission = this.missionsService.getMissionById(missionId);

		if (mission != null) {
			int amountOfLearningObjects = mission.levels.stream().map(level -> level.learningObjectIds)
					.flatMap(List::stream).collect(Collectors.toList()).size();
			final GamificationPoints gamificationPoints = new GamificationPoints(amountOfLearningObjects * 5, 0, 0);
			// GamificationPoints gamificationPoints = new GamificationPoints(0 * 5, 0, 0);

			List<StudentResult> studentsResults = new LinkedList<>();
			for (String studentId : mission.students) {
				final Student student = studentService.getStudent(studentId);
				if (student.hasEnrolled(missionId)) {
					final MissionState missionState = studentService.getProgressOfMission(studentId, missionId);
					UserSharedCacheObject userSharedCacheObject = userSharedService.getUser(studentId);
					// studentsResults.add(new StudentResult(userSharedCacheObject.username, "1",
					// "", missionState.getCurrentProgress() * 5));
					// gamificationPoints = new GamificationPoints(missionState.getMaxProgress() *
					// 5, 0, 0);
					
					if (missionState.getMaxProgress()>0)
						studentsResults.add(new StudentResult(userSharedCacheObject.username, "1", "",
							missionState.getCurrentProgress()*100/missionState.getMaxProgress()));
					else
						studentsResults.add(new StudentResult(userSharedCacheObject.username, "1", "", 100));
				}
			}

			MissionStudentsResultsResponse missionStudentsResultsResponse = new MissionStudentsResultsResponse(
					gamificationPoints, studentsResults);
			return Response.status(Status.OK).entity(missionStudentsResultsResponse).build();
		} else
			return Response.status(Status.NOT_FOUND).build();
	}

	@GET
	@Path("/{missionId}/overAllGamificationPoints")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOverallGamificationPointsOfMission(@PathParam("missionId") String missionId) {
		final Mission mission = this.missionsService.getMissionById(missionId);
		int amountOfLearningObjects = mission.levels.stream().map(level -> level.learningObjectIds)
				.flatMap(List::stream).collect(Collectors.toList()).size();
		final GamificationPoints gamificationPoints = new GamificationPoints(amountOfLearningObjects * 5, 0, 0,
				mission.missionMetadata.generalMetadata.title);
		return Response.status(Status.OK).entity(gamificationPoints).build();
	}

	@GET
	@Path("/{missionId}/missionOverview")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMissionOverview(@PathParam("missionId") String missionId) {
		Mission mission = this.missionsService.getMissionById(missionId);
		// Reminder: authorIf is replaced with authorName
		mission.missionMetadata.generalMetadata.author = this.missionToAuthorNamesMapper
				.getAuthorNameOfMission(mission);
		MissionOverview missionOverview = new MissionOverview();
		missionOverview.setMissionMetadata(mission.missionMetadata);
		missionOverview.setLevels(mission.levels);
		final List<LearningObject> learningObjects = learningObjectsService.getByIds(mission.getLearningObjectIds());
		missionOverview.setLearningObjects(learningObjects);
		return Response.status(Status.OK).entity(missionOverview).build();
	}

	@GET
	@Path("/{missionId}/{progress}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getContent(@PathParam("missionId") String missionId, @PathParam("progress") int progress) {
		final Object content = this.getContents(missionId, progress);
		return Response.status(200).entity(content).build();
	}

	public Object getContents(String missionId, int progress) {
		Mission mission = this.missionsService.getMissionById(missionId);
		List<Level> levels = mission.levels;
		if (levels.size() == 0)
			return false;
		List<Object> flattenedLevels = this.flattenLevel(levels);
		if (flattenedLevels.size() == 0)
			return false;
		Object content = flattenedLevels.get(progress);
		if (content instanceof Level)
			return content;
		else
			return this.learningObjectsService.getById((String) content);
	}

	private List<Object> flattenLevel(List<Level> levels) {
		List<Object> flattened = new ArrayList<>();
		for (Level level : levels) {
			flattened.add(level);
			flattened.addAll(level.learningObjectIds);
		}
		return flattened;
	}

	@GET
	@Path("/{missionId}/discussions")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllDiscussionsInMission(@CookieParam("access_token") Cookie cookie,
			@PathParam("missionId") String missionId) {
		final Mission mission = this.missionsService.getMissionById(missionId);
		mission.missionMetadata.generalMetadata.author = this.missionToAuthorNamesMapper
				.getAuthorNameOfMission(mission);
		final List<LearningObject> learningObjects = learningObjectsService.getByIds(mission.getLearningObjectIds());
		final List<String> discussionIds = getDiscussionIds(learningObjects);
		if (discussionIds.size() == 0)
			return Response.status(Status.NO_CONTENT).build();

		List<Discussion> allDiscussionsOfMission = this.discussionService.getByIds(discussionIds);
		for (Discussion discussion : allDiscussionsOfMission) {
			LearningObject learningObject = this.learningObjectsService.getById(discussion.getTopic());
			discussion.setTopic(learningObject.title);
		}

		List<String> statusUpdateIds = new ArrayList<>();
		allDiscussionsOfMission.forEach(discussion -> statusUpdateIds.add(discussion.getStatusUpdateId()));

		List<Pair<String, Object>> queryParams = new ArrayList<>();
		statusUpdateIds.forEach(statusUpdateId -> queryParams.add(Pair.of("statusUpdateIds[]", statusUpdateId)));
		Response statusUpdateResponse = StimeyClient.get(StimeyClient.create(), "api-gateway-storage", Global.DOCKER,
				"api/statusupdate", cookie, queryParams);

		DiscussionsResponse discussionsResponse = new DiscussionsResponse();
		discussionsResponse.discussions = allDiscussionsOfMission;
		discussionsResponse.statusUpdates = statusUpdateResponse.readEntity(String.class);

		return Response.status(Status.OK).entity(discussionsResponse).build();
	}

	@GET
	@Path("/{missionId}/resources")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllResources(@PathParam("missionId") String missionId) {
		final Mission mission = this.missionsService.getMissionById(missionId);

		Document responseData = new Document();

		Map<String, Pair<String, String>> resourceIdToRelevanceMapping = new LinkedHashMap<>();
		for (String resourceId : mission.resources) {
			resourceIdToRelevanceMapping.put(resourceId, Pair.of(missionId, "General"));
		}
		resourceIdToRelevanceMapping
				.putAll(learningObjectsService.resourceIdToRelevanceMapping(mission.getLearningObjectIds()));

		responseData.put("resourceIdToRelevanceMapping", resourceIdToRelevanceMapping);

		List<Pair<String, String>> relevances = new LinkedList<>();
		relevances.add(Pair.of(missionId, "General"));
		relevances.addAll(learningObjectsService.relevances(mission.getLearningObjectIds()));
		responseData.put("relevances", relevances);

		return Response.status(Status.OK).entity(responseData).build();
	}

	@DELETE
	@Path("/{missionId}/resources/{resourceId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteResource(@PathParam("missionId") String missionId,
			@PathParam("resourceId") String resourceId) {
		Mission mission = this.missionsService.getMissionById(missionId);
		mission.resources.remove(resourceId);
		this.missionsService.setMission(mission);
		return Response.status(Status.OK).entity(mission).build();
	}

	@GET
	@Path("/{missionId}/resources/relevances")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRelevances(@PathParam("missionId") String missionId) {
		final Mission mission = this.missionsService.getMissionById(missionId);
		List<Pair<String, String>> relevances = new LinkedList<>();
		relevances.add(Pair.of(missionId, "General"));
		relevances.addAll(learningObjectsService.relevances(mission.getLearningObjectIds()));
		return Response.status(Status.OK).entity(relevances).build();
	}

	@POST
	@Path("/{missionId}/resources")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addResource(@PathParam("missionId") String missionId, String resourceId) {
		Mission mission = missionsService.getMissionById(missionId);
		if (mission.resources.contains(resourceId)) {
			return Response.status(Response.Status.NOT_MODIFIED).build();
		}
		mission.resources.add(resourceId);
		this.missionsService.setMission(mission);
		return Response.status(Response.Status.OK).entity(mission).build();
	}

	@DELETE
	@Path("/{missionId}/discussions/{discussionId}")
	public Response deleteDiscussion(@PathParam("missionId") String missionId,
			@PathParam("discussionId") String discussionId) {
		final Mission mission = this.missionsService.getMissionById(missionId);
		final Discussion discussion = this.discussionService.getById(discussionId);
		final int indexOfLearningObjectThatContainsDiscussion = mission.getLearningObjectIds()
				.indexOf(discussion.getTopic());
		if (indexOfLearningObjectThatContainsDiscussion < 0)
			return Response.status(Status.BAD_REQUEST).build();
		final String learningObjectId = mission.getLearningObjectIds().get(indexOfLearningObjectThatContainsDiscussion);
		LearningObject learningObject = this.learningObjectsService.getById(learningObjectId);
		learningObject.discussions.remove(discussionId);
		assert !learningObject.discussions.contains(discussionId);
		this.learningObjectsService.setLearningObject(learningObject);
		this.discussionService.deleteDiscussion(discussionId);
		return Response.status(Response.Status.ACCEPTED).build();
	}

	private List<String> getDiscussionIds(List<LearningObject> learningObjects) {
		List<String> discussionIds = new ArrayList<>();
		for (LearningObject learningObject : learningObjects) {
			discussionIds.addAll(learningObject.discussions);
		}
		return discussionIds;
	}

	@GET
	@Path("/{missionId}/levels/{levelIndex}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLevel(@PathParam("missionId") String missionId, @PathParam("levelIndex") int levelIndex) {
		Mission mission = this.missionsService.getMissionById(missionId);
		Level level = mission.levels.get(levelIndex);
		return Response.status(Status.OK).entity(level).build();
	}

	@POST
	@Path("/{missionId}/levels/{levelIndex}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateLevel(@PathParam("missionId") String missionId, @PathParam("levelIndex") int levelIndex,
			Level level) {
		Mission mission = this.missionsService.getMissionById(missionId);
		mission.levels.set(levelIndex, level);
		this.missionsService.setMission(mission);
		return Response.status(Status.OK).build();
	}

	@GET
	@Path("/{missionId}/challengeResults")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStudentsChallengeResults(@Context ContainerRequestContext context,
			@PathParam("missionId") String missionId) {
		StimeyLogger.logger().info(context.getUriInfo().getBaseUri());
		StimeyLogger.logger().info(context.getUriInfo().getPath());
		final Mission mission = this.missionsService.getMissionById(missionId);
		final List<String> studentIds = mission.students;
		final List<Challenge> challenges = this.getChallengesOfMission(mission);
		List<ChallengeResultResponse> challengeResultResponses = new ArrayList<>();
		for (Challenge challenge : challenges) {
			for (String studentId : studentIds) {
				final ChallengeStudentInput challengeStudentInput = this.challengeResultService
						.getChallengeResult(challenge.get_id(), studentId);
				// If student did not participate challenge continue
				if (challengeStudentInput == null)
					continue;
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
				String challengeStartDate = "";
				if (challengeStudentInput.startDate != null) {
					challengeStartDate = format.format(challengeStudentInput.startDate);
				}
				String challengeSubmitDate = "";
				if (challengeStudentInput.submitDate != null) {
					challengeSubmitDate = format.format(challengeStudentInput.submitDate);
				}
				ChallengeResultResponse challengeResultResponse = new ChallengeResultResponse(challenge.title,
						mission.missionMetadata.generalMetadata.title,
						this.userSharedService.getUser(studentId).username, challengeStartDate, challengeSubmitDate);
				for (String learningObjectId : challenge.learningObjectIds) {
					final LearningObject question = this.learningObjectsService.getById(learningObjectId);
					final StudentInput studentInput = challengeStudentInput.results.get(question.get_id());
					challengeResultResponse.getStudentAnswers().add(getStudentAnswer(question, studentInput));
				}
				challengeResultResponses.add(challengeResultResponse);

			}
		}
		return Response.status(Status.OK).entity(challengeResultResponses).build();
	}

	private List<Challenge> getChallengesOfMission(Mission mission) {
		List<Challenge> challenges = new ArrayList<>();
		final List<String> learningObjectIds = mission.getLearningObjectIds();
		for (String learningObjectId : learningObjectIds) {
			final LearningObject learningObject = this.learningObjectsService.getById(learningObjectId);
			if (learningObject instanceof Challenge)
				challenges.add((Challenge) learningObject);
		}
		return challenges;
	}

	public StudentAnswer getStudentAnswer(LearningObject question, StudentInput studentInput) {
		if (question instanceof CheckableQuestion == false)
			throw new IllegalArgumentException(question + " is not checkable question");
		if (question == null || studentInput == null)
			return new StudentAnswer();
		StudentAnswer studentAnswer = new StudentAnswer();
		studentAnswer.setQuestionTitle(question.title);
		studentAnswer.setStudentInput(studentInput.studentInput());
		studentAnswer.setCorrect(((CheckableQuestion) question).check(studentInput));
		return studentAnswer;
	}

}
