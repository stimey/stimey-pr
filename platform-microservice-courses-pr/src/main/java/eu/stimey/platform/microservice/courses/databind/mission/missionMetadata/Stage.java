package eu.stimey.platform.microservice.courses.databind.mission.missionMetadata;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Stage {
	PRIMARY("Primary"), LOWER("Lower Secondary"), UPPER("Upper Secondary");

	private final String text;

	Stage(String text) {
		this.text = text;
	}

	public static Stage fromString(String text) {
		for (Stage s : Stage.values()) {
			if (s.text.equals(text)) {
				return s;
			}
		}
		throw new IllegalArgumentException("no such stage " + text);
	}

	@JsonValue
	public String getText() {
		return text;
	}
}