package eu.stimey.platform.microservice.courses.rest.resources;

import eu.stimey.platform.microservice.courses.beans.ChallengeResultService;
import eu.stimey.platform.microservice.courses.databind.challengeResult.ChallengeStudentInput;
import eu.stimey.platform.microservice.courses.databind.challengeResult.StudentInput;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Calendar;
import java.util.Map;

@Path("/challengeResults")
public class ChallengeResultResource {

	@Inject
	private ChallengeResultService challengeResultService;

	@GET
	@Path("/{challengeResultId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getChallengeResult(@PathParam("challengeResultId") String challengeResultId) {
		ChallengeStudentInput challengeStudentInput = this.challengeResultService.getChallengeResult(challengeResultId);
		if (challengeStudentInput == null)
			return Response.status(Response.Status.NOT_FOUND).build();
		return Response.status(Response.Status.CREATED).entity(challengeStudentInput).build();
	}

	@POST
	@Path("/{challengeResultId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response startChallenge(@PathParam("challengeResultId") String challengeResultId) {
		ChallengeStudentInput challengeStudentInput = new ChallengeStudentInput(challengeResultId);
		this.challengeResultService.setChallengeResult(challengeStudentInput);
		return Response.status(Response.Status.CREATED).entity(challengeStudentInput).build();
	}

	@HEAD
	@Path("/{challengeResultId}/submitDate")
	@Produces(MediaType.APPLICATION_JSON)
	public Response hasStudentSubmittedChallenge(@PathParam("challengeResultId") String challengeResultId) {
		final ChallengeStudentInput challengeStudentInput = this.challengeResultService
				.getChallengeResult(challengeResultId);
		if (challengeStudentInput == null)
			return Response.status(Response.Status.BAD_REQUEST).build();
		if (challengeStudentInput.submitDate == null)
			return Response.status(Response.Status.NOT_FOUND).build();
		return Response.status(Response.Status.OK).build();
	}

	@POST
	@Path("/{challengeResultId}/results")
	@Produces(MediaType.APPLICATION_JSON)
	public Response submitChallenge(@PathParam("challengeResultId") String challengeResultId,
			Map<String, StudentInput> results) {
		ChallengeStudentInput challengeStudentInput = this.challengeResultService.getChallengeResult(challengeResultId);
		challengeStudentInput.results = results;
		challengeStudentInput.submitDate = Calendar.getInstance().getTime();
		this.challengeResultService.setChallengeResult(challengeStudentInput);
		return Response.status(Response.Status.OK).entity(challengeStudentInput).build();
	}
}
