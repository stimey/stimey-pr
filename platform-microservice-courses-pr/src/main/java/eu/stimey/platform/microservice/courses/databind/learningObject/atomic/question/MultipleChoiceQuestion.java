package eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.databind.challengeResult.StudentInput;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;

import java.util.*;
import java.util.stream.Collectors;

public class MultipleChoiceQuestion extends CheckableQuestion {

	public List<Answer> answers;
	public String hint;

	public MultipleChoiceQuestion() {
		this.answers = new ArrayList<>();
		for (int i = 0; i < 4; i++) {
			this.answers.add(new Answer());
		}
	}

	public MultipleChoiceQuestion(MultipleChoiceQuestion other) {
		super(other);
		this.answers = other.answers;
		this.hint = other.hint;
	}

	@JsonIgnore
	public List<String> randomizedAnswerTexts() {
		List<String> allAnswerTexts = new ArrayList<>();
		this.answers.forEach(answer -> allAnswerTexts.add(answer.answerText));
		Collections.shuffle(allAnswerTexts);
		return allAnswerTexts;
	}

	private List<String> correctAnswers() {
		return this.answers.stream().filter(answer -> answer.isCorrect).map(answer -> answer.answerText)
				.collect(Collectors.toList());
	}

	@Override
	public boolean check(StudentInput studentInput) {
		Set<String> correctAnswers = new HashSet<>(this.correctAnswers());
		return new HashSet(studentInput.studentInput()).equals(correctAnswers);
	}

	@Override
	public LearningObject copy(LearningObjectsService learningObjectsService) {
		LearningObject copy = new MultipleChoiceQuestion(this);
		learningObjectsService.setLearningObject(copy);
		return copy;
	}
}
