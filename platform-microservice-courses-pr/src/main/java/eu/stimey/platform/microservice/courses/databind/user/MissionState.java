package eu.stimey.platform.microservice.courses.databind.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.stimey.platform.microservice.courses.utils.DateDeserializer;
import eu.stimey.platform.microservice.courses.utils.DateSerializer;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class MissionState {

	private int maxProgress;
	private int currentProgress;
	private boolean[] finishedLearningObjects;
	private int overallProgress;
	private boolean missionFinished;
	public Date startDate;
	public Date finishDate;

	public BonusTaskState bonusTaskState;

	public MissionState() {
	}

	public MissionState(int maxProgress) {
		// this.missionFinished = false;
		this.maxProgress = maxProgress;
		this.currentProgress = 0;
		if (maxProgress >= 0) {
			this.finishedLearningObjects = new boolean[maxProgress];
			for (int i = 0; i < maxProgress; i++) {
				this.finishedLearningObjects[i] = false;
			}
		}
		// this.overallProgress = this.getOverallProgress();
		this.bonusTaskState = new BonusTaskState();
		this.startDate = Calendar.getInstance().getTime();
	}

	@JsonIgnore
	public void increaseProgress() {
		if (this.currentProgress == this.maxProgress)
			return;
		this.finishedLearningObjects[this.currentProgress] = true;
		this.currentProgress++;
		this.getOverallProgress();
	}

	@JsonIgnore
	public void decreaseProgress() {
		if (this.currentProgress == 0)
			throw new IllegalStateException("cannot decrease progress because currentProgress is 0");
		this.currentProgress--;
	}

	@JsonProperty("missionFinished")
	public boolean isMissionFinished() {
		return this.getOverallProgress() >= this.maxProgress;
	}

	public void setMissionFinished(boolean missionFinished) {
		this.missionFinished = missionFinished;
	}

	@JsonProperty("overallProgress")
	public int getOverallProgress() {
		if (this.finishedLearningObjects == null)
			return 0;

		int amountOfFinishedLearningObjects = 0;
		for (int i = 0; i < this.finishedLearningObjects.length; i++) {
			if (this.finishedLearningObjects[i])
				amountOfFinishedLearningObjects++;
		}
		return amountOfFinishedLearningObjects;
	}

	public void setOverallProgress(int overallProgress) {
		this.overallProgress = overallProgress;
	}

	public int getCurrentProgress() {
		return currentProgress;
	}

	public void setCurrentProgress(int currentProgress) {
		this.currentProgress = currentProgress;
	}

	public boolean[] getFinishedLearningObjects() {
		return this.finishedLearningObjects;
	}

	public void setFinishedLearningObjects(boolean[] finishedLearningObjects) {
		if (finishedLearningObjects == null) {
			if (maxProgress >= 0) {
				this.finishedLearningObjects = new boolean[maxProgress];
				for (int i = 0; i < maxProgress; i++) {
					this.finishedLearningObjects[i] = false;
				}
			}
		} else {
			this.finishedLearningObjects = finishedLearningObjects;
		}
	}

	public int getMaxProgress() {
		return maxProgress;
	}

	public void setMaxProgress(int maxProgress) {
		this.maxProgress = maxProgress;
	}

	@JsonIgnore
	public double getOverallProgressInPercent() {
		return ((double) this.getOverallProgress()) / ((double) this.maxProgress);
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getStartDate() {
		return startDate;
	}

	@JsonDeserialize(using = DateDeserializer.class)
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@JsonSerialize(using = DateSerializer.class)
	public Date getFinishDate() {
		if (this.isMissionFinished() && this.finishDate == null)
			this.finishDate = Calendar.getInstance().getTime();
		return this.finishDate;
	}

	@JsonDeserialize(using = DateDeserializer.class)
	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	@Override
	public String toString() {
		return "MissionState{" + "maxProgress=" + maxProgress + ", currentProgress=" + currentProgress
				+ ", finishedLearningObjects=" + Arrays.toString(finishedLearningObjects) + ", startDate=" + startDate
				+ ", finishDate=" + finishDate + ", bonusTaskState=" + bonusTaskState + '}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		MissionState that = (MissionState) o;
		return maxProgress == that.maxProgress && currentProgress == that.currentProgress
				&& Arrays.equals(finishedLearningObjects, that.finishedLearningObjects)
				&& startDate.equals(that.startDate) && finishDate.equals(that.finishDate)
				&& bonusTaskState.equals(that.bonusTaskState);
	}

	@Override
	public int hashCode() {
		int result = Objects.hash(maxProgress, currentProgress, startDate, finishDate, bonusTaskState);
		result = 31 * result + Arrays.hashCode(finishedLearningObjects);
		return result;
	}
}
