package eu.stimey.platform.microservice.courses.databind.learningObject.atomic.multimedia;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.AtomicLearningObject;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.Reward;
import eu.stimey.platform.microservice.courses.databind.robotActions.VideoRobotActions;

import java.util.HashMap;
import java.util.Map;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = Task.class, name = "Task"),
		@JsonSubTypes.Type(value = Assignment.class, name = "Assignment") })
public class MultiMediaLearningObject extends AtomicLearningObject {
	public String teachersAdvice;
	public String html;

	// Id of this map is the id of the iframe tag of the youtube video
	// dont know other solution
	public Map<String, VideoRobotActions> videoIdToRobotactionMap;

	public MultiMediaLearningObject() {
		super();
		this.progressiveInquiryModelPhase = -1;
		this.html = "";
		this.introduction = "";
		this.teachersAdvice = "";
		this.videoIdToRobotactionMap = new HashMap<>();
	}

	public MultiMediaLearningObject(String title) {
		this(title, new Reward());
	}

	public MultiMediaLearningObject(String title, Reward reward) {
		super(title, "", reward);
	}

	public MultiMediaLearningObject(String title, String description, Reward reward) {
		super(title, description, reward);
	}

	public MultiMediaLearningObject(MultiMediaLearningObject original) {
		super(original);
	}

	@Override
	public LearningObject copy(LearningObjectsService learningObjectsService) {
		LearningObject copy = new MultiMediaLearningObject(this);
		learningObjectsService.setLearningObject(copy);
		return copy;
	}
}
