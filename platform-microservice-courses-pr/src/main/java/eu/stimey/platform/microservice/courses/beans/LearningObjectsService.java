package eu.stimey.platform.microservice.courses.beans;

import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.multimedia.Assignment;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.multimedia.Task;
import eu.stimey.platform.microservice.courses.databind.learningObject.composite.Challenge;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.util.*;

public class LearningObjectsService {
	private final MongoDBDataAccess<LearningObject> learningObjectDataAccess;

	public LearningObjectsService() {
		this.learningObjectDataAccess = new MongoDBDataAccess<>("ms_courses_learningObjects", LearningObject.class,
				"learningObjects", "_id", true);
	}

	public LearningObject getById(String id) {
		LearningObject learningObject = this.learningObjectDataAccess.get(id);
		if (learningObject == null)
			throw new IllegalStateException("learningObject with id=" + id + " does not exist");
		return learningObject;
	}

	public List<LearningObject> getByIds(List<String> ids) {
		List<LearningObject> result = new ArrayList<>();
		for (String s : ids) {
			result.add(getById(s));
		}
		return result;
	}

	public Triple<Integer, Integer, Integer> typesOfLearningObjects(List<String> learningObjectIds) {
		int amountOfTasks = 0;
		int amountOfAssignments = 0;
		int amountOfChallenges = 0;
		List<LearningObject> learningObjects = this.getByIds(learningObjectIds);
		for (LearningObject learningObject : learningObjects) {
			if (learningObject instanceof Task)
				amountOfTasks++;
			else if (learningObject instanceof Challenge)
				amountOfChallenges++;
			else if (learningObject instanceof Assignment)
				amountOfAssignments++;
			else
				throw new IllegalStateException("Level cannot contain" + learningObject.getClass().getSimpleName());
		}
		return Triple.of(amountOfTasks, amountOfAssignments, amountOfChallenges);
	}

	public Pair<String, List<String>> learningObjectTitleToResourceIdsMapping(String learningObjectId) {
		LearningObject learningObject = this.getById(learningObjectId);
		return Pair.of(learningObject.title, learningObject.resources);
	}

	public Map<String, List<String>> learningObjectTitlesToResourceIdsMapping(List<String> learningObjectIds) {
		Map<String, List<String>> learningObjectTitleToResourcesMapping = new LinkedHashMap<>();
		for (String learningObjectId : learningObjectIds) {
			Pair<String, List<String>> learningObjectTitleToResourceIdsMapping = this
					.learningObjectTitleToResourceIdsMapping(learningObjectId);
			learningObjectTitleToResourcesMapping.put(learningObjectTitleToResourceIdsMapping.getKey(),
					learningObjectTitleToResourceIdsMapping.getValue());
		}
		return learningObjectTitleToResourcesMapping;
	}

	public Map<String, String> resourceIdToLearningObjectTitleMapping(List<String> learningObjectIds) {
		Map<String, String> resourceIdToLearningObjectTitleMapping = new LinkedHashMap<>();
		for (String learningObjectId : learningObjectIds) {
			final LearningObject learningObject = this.getById(learningObjectId);
			for (String resourceId : learningObject.resources)
				resourceIdToLearningObjectTitleMapping.put(resourceId, learningObject.title);
		}
		return resourceIdToLearningObjectTitleMapping;
	}

	public Map<String, Pair<String, String>> resourceIdToRelevanceMapping(List<String> learningObjectIds) {
		Map<String, Pair<String, String>> resourceIdToRelevanceMapping = new LinkedHashMap<>();
		for (String learningObjectId : learningObjectIds) {
			final LearningObject learningObject = this.getById(learningObjectId);
			for (String resourceId : learningObject.resources) {
				Pair<String, String> relevance = Pair.of(learningObjectId, learningObject.title);
				resourceIdToRelevanceMapping.put(resourceId, relevance);
			}
		}
		return resourceIdToRelevanceMapping;
	}

	public List<Pair<String, String>> relevances(List<String> learningObjectIds) {
		List<Pair<String, String>> relevances = new LinkedList<>();
		for (String learningObjectId : learningObjectIds) {
			final LearningObject learningObject = this.getById(learningObjectId);
			relevances.add(Pair.of(learningObjectId, learningObject.title));
		}
		return relevances;
	}

	public void setLearningObject(LearningObject learningObject) {
		this.learningObjectDataAccess.set(learningObject);
	}

	public void setLearningObjects(List<LearningObject> learningObjects) {
		for (LearningObject learningObject : learningObjects)
			this.setLearningObject(learningObject);
	}

}
