package eu.stimey.platform.microservice.courses.rest.dataTransfer.responses;

import java.util.List;

public class StudentsMissionsResponse {

	public final List<MissionResponse> activeMissions;
	public final List<MissionResponse> completedMissions;

	public StudentsMissionsResponse(List<MissionResponse> activeMissions, List<MissionResponse> completedMissions) {
		this.activeMissions = activeMissions;
		this.completedMissions = completedMissions;
	}
}
