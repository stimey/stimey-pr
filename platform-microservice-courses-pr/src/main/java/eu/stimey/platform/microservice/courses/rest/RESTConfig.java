package eu.stimey.platform.microservice.courses.rest;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.microservice.courses.beans.*;
import eu.stimey.platform.microservice.courses.startup.Global;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import javax.annotation.PreDestroy;
import javax.ws.rs.ApplicationPath;

import static eu.stimey.platform.library.utils.StimeyLogger.logger;

/**
 * 
 * REST configuration
 *
 */
@ApplicationPath("api")
public class RESTConfig extends ResourceConfig {
	protected final MongoDBService mongoDBService;
	protected final RedisAMQPService redisAMQPService;

	public RESTConfig() {
		super();

		mongoDBService = MongoDBService.getService();
		redisAMQPService = RedisAMQPService.getService();

		logger().info("REST-MongoDBDataAccess started...");

		packages("eu.stimey.platform.microservice.courses.rest");

		register(getAbstractBinder());
		JacksonJsonProvider jacksonJsonProvider = new JacksonJsonProvider();
		jacksonJsonProvider.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		jacksonJsonProvider.configure(SerializationFeature.INDENT_OUTPUT, true);
		register(jacksonJsonProvider);
	}

	private AbstractBinder getAbstractBinder() {
		final MissionsService missionsService = new MissionsService();
		final TeacherService teacherService = new TeacherService();
		final LearningObjectsService learningObjectsService = new LearningObjectsService();
		final StudentService studentService = new StudentService(missionsService/* , gamificationCommunication */);
		final UserSharedService userSharedService = new UserSharedService();

		return new AbstractBinder() {
			@Override
			protected void configure() {
				bind(userSharedService).to(UserSharedService.class);
				bind(learningObjectsService).to(LearningObjectsService.class);
				bind(missionsService).to(MissionsService.class);
				bind(studentService).to(StudentService.class);
				bind(teacherService).to(TeacherService.class);
				bind(new MissionEditorService(missionsService, teacherService)).to(MissionEditorService.class);
				bind(new MissionCopyService(missionsService, learningObjectsService)).to(MissionCopyService.class);
				bind(new MissionToAuthorNamesMapper(missionsService, userSharedService))
						.to(MissionToAuthorNamesMapper.class);
				bind(new KhanAcademyVideoService(new KhanAcademyAPIConnection(), Global.UPDATE_KHAN_ACEDEMY_DATA))
						.to(KhanAcademyVideoService.class);
				bind(new ChallengeResultService()).to(ChallengeResultService.class);
				bind(new DiscussionService()).to(DiscussionService.class);
			}
		};
	}

	@PreDestroy
	public void shutdown() {
		logger().info("REST-MongoDBDataAccess stopped...");
	}
}
