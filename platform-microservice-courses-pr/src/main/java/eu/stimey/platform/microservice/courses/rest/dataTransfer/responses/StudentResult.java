package eu.stimey.platform.microservice.courses.rest.dataTransfer.responses;

public class StudentResult {
	private final String username;
	private final String level;
	private final String country;
	private final int progress;

	public StudentResult(String username, String level, String country, int progress) {
		super();
		this.username = username;
		this.level = level;
		this.country = country;
		this.progress = progress;
	}

	public String getUsername() {
		return username;
	}

	public String getLevel() {
		return level;
	}

	public String getCountry() {
		return country;
	}

	public int getProgress() {
		return progress;
	}
}
