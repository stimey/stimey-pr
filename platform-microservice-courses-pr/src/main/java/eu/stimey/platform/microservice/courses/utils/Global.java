package eu.stimey.platform.microservice.courses.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Global {
	public static boolean xml = false;

	public static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z");
}
