package eu.stimey.platform.microservice.courses.rest.resources;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.utils.StimeyLogger;
import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.beans.MissionsService;
import eu.stimey.platform.microservice.courses.beans.TeacherService;
import eu.stimey.platform.microservice.courses.beans.filter.MissionsFilter;
import eu.stimey.platform.microservice.courses.databind.mission.Mission;
import eu.stimey.platform.microservice.courses.databind.user.Teacher;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.MissionResponse;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.TeachersMissionsResponse;
import org.apache.commons.lang3.tuple.Triple;
import org.bson.Document;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Path("/teachers")
public class TeacherResource {

	@Inject
	TeacherService teacherService;

	@Inject
	MissionsService missionsService;

	@Inject
	UserSharedService userSharedService;

	@Inject
	LearningObjectsService learningObjectsService;

	@GET
	@Path("/{id}/missions")
	@Produces(MediaType.APPLICATION_JSON)
	public Response teachersMissions(@PathParam("id") String teacherId) {
		final Teacher teacher = this.teacherService.getTeacher(teacherId);

		if (!teacher.hasMissions())
			return Response.status(Response.Status.NO_CONTENT).build();

		this.validateTeachersMissions(teacher, teacher.allMissions());

		TeachersMissionsResponse teachersMissionsResponse = new TeachersMissionsResponse();
		teachersMissionsResponse.setActiveMissions(
				this.createMissionResponse(teacher, this.missionsService.getMissionsByIds(teacher.activeMissions)));
		teachersMissionsResponse.setPendingModeratorInvitations(this.createMissionResponse(teacher,
				this.missionsService.getMissionsByIds(teacher.pendingModeratorInvitation)));
		teachersMissionsResponse.setCompletedMissions(
				this.createMissionResponse(teacher, this.missionsService.getMissionsByIds(teacher.completedMissions)));
		teachersMissionsResponse.setSavedMissions(
				this.createMissionResponse(teacher, this.missionsService.getMissionsByIds(teacher.savedMissions)));
		return Response.ok().entity(teachersMissionsResponse).build();
	}

	@GET
	@Path("/{id}/missions/recent")
	@Produces(MediaType.APPLICATION_JSON)
	public Response teachersRecentMissions(@PathParam("id") String teacherId, @QueryParam("amount") int amount) {
		final Teacher teacher = this.teacherService.getTeacher(teacherId);

		if (!teacher.hasMissions())
			return Response.status(Response.Status.NO_CONTENT).build();

		List<String> allMissions = teacher.allMissions();
		Document idsFilter = new MissionsFilter().ids(allMissions).toDocument();
		System.out.println(idsFilter);

		Document sortByCreationDate = new Document(MissionsFilter.CREATIONDATE_PATH, -1);

		List<Mission> missions = this.missionsService.findMissions(amount, 0, idsFilter, sortByCreationDate);

		return Response.ok().entity(this.createMissionResponse(teacher, missions)).build();
	}

	/**
	 * It is possible that missions are deleted from database, but a Teacher still
	 * has ids of deleted missions. This will throw an error. Therefore, references
	 * to missions that do not exist anymore are deleted from Teacher. This method
	 * will be obsolete, when deleting missions from frontend is possible.
	 *
	 * @param teacher
	 * @param missionIds
	 * @return List of missions that do exist.
	 */
	private Teacher validateTeachersMissions(Teacher teacher, final List<String> missionIds) {
		List<String> idsOfMissionsThatDoNotExist = new ArrayList<>();
		for (String missionId : missionIds) {
			final Mission mission = this.missionsService.getMissionById(missionId);
			if (mission == null) {
				idsOfMissionsThatDoNotExist.add(missionId);
			}
		}
		teacher.removeNotExistingMissions(idsOfMissionsThatDoNotExist);
		this.teacherService.setTeacher(teacher);
		return teacher;
	}

	private List<MissionResponse> createMissionResponse(Teacher teacher, List<Mission> missions) {
		final String teacherName = this.userSharedService.getUser(teacher.userId).username;
		List<MissionResponse> missionResponses = new ArrayList<>();
		for (Mission mission : missions) {
			Triple<Integer, Integer, Integer> typesOfLearningObjects = this.learningObjectsService
					.typesOfLearningObjects(mission.getLearningObjectIds());
			// TODO: authorName is not always the name of this teacher
			// TODO: e.g. moderated missions have other author
			missionResponses.add(new MissionResponse(mission, teacherName, typesOfLearningObjects.getLeft(),
					typesOfLearningObjects.getMiddle(), typesOfLearningObjects.getRight()));
		}
		return missionResponses;
	}

	@GET
	@Path("/{id}/overAllStudents")
	@Produces(MediaType.APPLICATION_JSON)
	public Response overallStudentsOfTeacher(@PathParam("id") String teacherId) {
		final Teacher teacher = this.teacherService.getTeacher(teacherId);

		if (!teacher.hasMissions())
			return Response.status(Response.Status.NO_CONTENT).build();

		final List<Mission> activeMissions = this.missionsService.getMissionsByIds(teacher.activeMissions);
		Map<String, List<String>> missionToStudentsMap = new HashMap();
		for (final Mission mission : activeMissions) {
			missionToStudentsMap.put(mission.get_id(), mission.students);
		}
		return Response.ok().entity(missionToStudentsMap).build();
	}

	@HEAD
	@Path("{id}/missions/activeMissions/{missionId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response isMissionPublished(@PathParam("id") String teacherId, @PathParam("missionId") String missionId) {
		StimeyLogger.logger().info("test");
		final Teacher teacher = this.teacherService.getTeacher(teacherId);
		Response response;
		if (teacher.activeMissions.contains(missionId))
			response = Response.status(Response.Status.OK).build();
		else
			response = Response.status(Response.Status.NOT_FOUND).build();
		return response;
	}

	@POST
	@Path("/missions/activeMissions/{missionId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response publishMission(@Context ContainerRequestContext context, @PathParam("missionId") String missionId) {
		final String teacherId = (String) context.getProperty("userid");
		this.teacherService.publishMission(teacherId, missionId);
		this.missionsService.publishMission(missionId);
		return Response.ok().build();
	}

}
