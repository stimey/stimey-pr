package eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.databind.challengeResult.StudentInput;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.AtomicLearningObject;
import eu.stimey.platform.microservice.courses.databind.mission.missionMetadata.Reward;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OneChoiceQuestion extends CheckableQuestion {

	public List<Answer> answers;
	public String hint;

	public OneChoiceQuestion() {
		// this("Click to edit", null, new Reward());
		this.answers = new ArrayList<>();
		for (int i = 0; i < 4; i++) {
			this.answers.add(new Answer());
		}
	}

	public OneChoiceQuestion(OneChoiceQuestion other) {
		super(other);
		this.answers = other.answers;
		this.hint = other.hint;
	}

	@JsonIgnore
	public List<String> randomizedAnswerTexts() {
		List<String> allAnswerTexts = new ArrayList<>();
		this.answers.forEach(answer -> allAnswerTexts.add(answer.answerText));
		Collections.shuffle(allAnswerTexts);
		return allAnswerTexts;
	}

	private Answer getCorrectAnswer() {
		for (Answer answer : this.answers)
			if (answer.isCorrect)
				return answer;
		throw new IllegalStateException("OneChoiceQuestion must have a correct answer");
	}

	@Override
	public boolean check(StudentInput studentInput) {
		if (studentInput.studentInput().size() > 1)
			throw new IllegalArgumentException("wrong student input " + studentInput);
		return this.getCorrectAnswer().answerText.equals(studentInput.studentInput().get(0));
	}

	@Override
	public LearningObject copy(LearningObjectsService learningObjectsService) {
		LearningObject copy = new OneChoiceQuestion(this);
		learningObjectsService.setLearningObject(copy);
		return copy;
	}
}
