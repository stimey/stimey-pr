package eu.stimey.platform.microservice.courses.rest.resources;

import eu.stimey.platform.microservice.courses.beans.LearningObjectsService;
import eu.stimey.platform.microservice.courses.databind.learningObject.LearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.multimedia.Assignment;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.AtomicLearningObject;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.multimedia.Task;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question.MultipleChoiceQuestion;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question.OneChoiceQuestion;
import eu.stimey.platform.microservice.courses.databind.learningObject.atomic.question.TrueFalseQuestion;
import eu.stimey.platform.microservice.courses.databind.learningObject.composite.Challenge;
import eu.stimey.platform.microservice.courses.databind.learningObject.composite.CompositeLearningObject;
import eu.stimey.platform.microservice.courses.rest.dataTransfer.responses.LearningObjectListResponse;
import org.apache.commons.lang3.tuple.Pair;
import org.bson.Document;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Path("/learningObjects")
public class LearningObjectResource {

	@Inject
	LearningObjectsService learningObjectsService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLearningObjects(@QueryParam("id[]") List<String> ids) {
		List<LearningObject> learningObjects = learningObjectsService.getByIds(ids);
		return Response.status(Response.Status.OK).entity(new LearningObjectListResponse(learningObjects)).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(String json) {
		Document document = Document.parse(json);
		LearningObject learningObject = this.fromType(document.getString("type"));
		learningObject.setSelfPaced(document.getBoolean("selfPaced"));
		this.learningObjectsService.setLearningObject(learningObject);
		return Response.status(Response.Status.OK).entity(learningObject).build();
	}

	@GET
	@Path("/sample")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLearningObjectByType(@QueryParam("type") String learningObjectType) {
		LearningObject learningObject = this.fromType(learningObjectType);
		return Response.status(Response.Status.OK).entity(learningObject).build();
	}

	private LearningObject fromType(String type) {
		LearningObject learningObject = compositeLearningObjectfromType(type);
		if (learningObject == null) {
			learningObject = atomicLearningObjectfromType(type);
		}
		return learningObject;
	}

	private AtomicLearningObject atomicLearningObjectfromType(String type) {
		switch (type) {
		case "Task": {
			return new Task();
		}
		case "Assignment": {
			return new Assignment();
		}
		case "MultipleChoiceQuestion": {
			return new MultipleChoiceQuestion();
		}
		case "TrueFalseQuestion": {
			return new TrueFalseQuestion();
		}
		case "OneChoiceQuestion": {
			return new OneChoiceQuestion();
		}
		default: {
			throw new IllegalArgumentException("learning object with type=" + type + " does not exist");
		}
		}
	}

	private CompositeLearningObject compositeLearningObjectfromType(String type) {
		final String title = "Click here to add text";
		if ("Challenge".equals(type)) {
			return new Challenge(title);
		}
		return null;
	}

	@POST
	@Path("/{id}/title")
	@Consumes("text/plain")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateLearningObjectTitle(@PathParam("id") String learningObjectId, String title) {
		LearningObject learningObject = this.learningObjectsService.getById(learningObjectId);
		learningObject.title = title;
		learningObjectsService.setLearningObject(learningObject);
		return Response.status(Response.Status.OK).build();
	}

	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateLearningObject(@PathParam("id") String learningObjectId, LearningObject learningObject) {
		System.out.println(learningObject.suggestedTime);
		learningObjectsService.setLearningObject(learningObject);
		return Response.status(Response.Status.OK).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLearningObjectById(@PathParam("id") String id) {
		LearningObject learningObject = learningObjectsService.getById(id);
		return Response.status(Response.Status.OK).entity(learningObject).build();
	}

	@GET
	@Path("/resources")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getResources(@QueryParam("learningObjectIds[]") List<String> learningObjectIds) {
		Document responseData = new Document();
		Map<String, Pair<String, String>> resourceIdToRelevanceMapping = new LinkedHashMap<>();
		resourceIdToRelevanceMapping.putAll(learningObjectsService.resourceIdToRelevanceMapping(learningObjectIds));
		responseData.put("resourceIdToRelevanceMapping", resourceIdToRelevanceMapping);
		List<Pair<String, String>> relevances = new LinkedList<>();
		relevances.addAll(learningObjectsService.relevances(learningObjectIds));
		responseData.put("relevances", relevances);
		return Response.status(Response.Status.OK).entity(responseData).build();
	}

	@POST
	@Path("/{learningObjectId}/resources")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addResource(@PathParam("learningObjectId") String learningObjectId, String resourceId) {
		LearningObject learningObject = learningObjectsService.getById(learningObjectId);
		if (learningObject.resources.contains(resourceId)) {
			return Response.status(Response.Status.NOT_MODIFIED).build();
		}
		learningObject.resources.add(resourceId);
		this.learningObjectsService.setLearningObject(learningObject);
		return Response.status(Response.Status.OK).entity(learningObject).build();
	}

	@DELETE
	@Path("/{learningObjectId}/resources/{resourceId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteResource(@PathParam("learningObjectId") String learningObjectId,
			@PathParam("resourceId") String resourceId) {
		LearningObject learningObject = learningObjectsService.getById(learningObjectId);
		if (!learningObject.resources.contains(resourceId)) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		learningObject.resources.remove(resourceId);
		this.learningObjectsService.setLearningObject(learningObject);
		return Response.status(Response.Status.OK).entity(learningObject).build();
	}

}
