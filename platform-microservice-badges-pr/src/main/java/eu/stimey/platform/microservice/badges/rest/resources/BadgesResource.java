package eu.stimey.platform.microservice.badges.rest.resources;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.microservice.badges.beans.BadgesService;
import eu.stimey.platform.microservice.badges.beans.SchedulerService;
import eu.stimey.platform.microservice.badges.databind.caching.BadgesCacheObject;
import eu.stimey.platform.microservice.badges.databind.caching.SchoolDataCacheObject;
import eu.stimey.platform.microservice.badges.utils.Labler;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/v1/badges")
public class BadgesResource {
	@Context
	UserSharedService userSharedService;
	@Context
	SchedulerService schedulerService;
	@Context
	BadgesService badgesService;

	@GET
	@Path("/{schoolid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBadgeFromSchool(@Context ContainerRequestContext requestContext,
			@PathParam("schoolid") String schoolid) {

		BadgesCacheObject obj = badgesService.get(schoolid);
		if (obj != null) {
			obj.currentSchoolData = badgesService.getSchoolData(schoolid);
			// System.out.println("school found:" + badgesService.get(schoolid).schoolid);
		} else {
			obj = new BadgesCacheObject(schoolid, BadgesCacheObject.BADGE_NONE);
			badgesService.set(obj);
			// badgesService.setSchoolData(schoolid, new SchoolDataCacheObject(schoolid,
			// BadgesCacheObject.BADGE_NONE, false));
			obj.currentSchoolData = badgesService.getSchoolData(schoolid);
			// System.out.println("school built:" + badgesService.get(schoolid).schoolid);
		}

		if (obj != null) {
			obj.currentSchoolData.listMinBlue = Labler.getMinBlue();
			obj.currentSchoolData.listMinSilver = Labler.getMinSilver();
			obj.currentSchoolData.listMinGold = Labler.getMinGold();
			obj.currentSchoolData.listMinDiamond = Labler.getMinDiamond();

			// obj.currentSchoolData.listBlue =
			// SchoolStats.getSchoolFromID(schoolid).getListBlue();
		}
		// obj.username = userSharedService.getUser(schoolid).username;

		badgesService.schedule();

		return Response.ok().entity(obj).build();
	}

	@GET
	@Path("/admin/{schoolid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBadgeFromSchoolAsAdmin(@Context ContainerRequestContext requestContext,
			@PathParam("schoolid") String schoolid) {
		BadgesCacheObject obj = badgesService.get(schoolid);

		if (obj != null)
			return Response.ok().entity(obj).build();
		else
			return Response.status(404).entity("{data:'School not found!'}").build();
	}

	/*
	 * @POST
	 * 
	 * @Path("/admin/{schoolid}")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON) public Response
	 * postBadgeFromSchoolAsAdmin(@Context ContainerRequestContext
	 * requestContext, @PathParam("schoolid") String schoolid){ String userid =
	 * (String) requestContext.getProperty("userid"); String usertype = (String)
	 * requestContext.getProperty("usertype"); if ((usertype.equals("school") &&
	 * userid.equals(schoolid))|| usertype.equals("admin"))
	 * 
	 * BadgesCacheObject obj = new BadgesCacheObject(schoolid,
	 * BadgesCacheObject.BADGE_NONE); badgesService.set(schoolid, obj);
	 * 
	 * return Response.ok().entity(obj).build(); }
	 */

	@PUT
	@Path("/admin/{schoolid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response putBadgeFromSchoolAsAdmin(@Context ContainerRequestContext requestContext,
			@PathParam("schoolid") String schoolid) {
		BadgesCacheObject obj = new BadgesCacheObject(schoolid, BadgesCacheObject.BADGE_NONE);
		badgesService.set(obj);
		// badgesService.setSchoolData(schoolid, new SchoolDataCacheObject(schoolid,
		// BadgesCacheObject.BADGE_NONE, false));

		return Response.ok().build();
	}

	@PUT
	@Path("/makeTest/{schoolid}/{badgeLevel}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response putTestSchool(@Context ContainerRequestContext requestContext,
			@PathParam("schoolid") String schoolid, @PathParam("badgeLevel") int badgeLevel) {

		BadgesCacheObject obj = new BadgesCacheObject(schoolid, badgeLevel);

		badgesService.set(obj);
		// badgesService.setSchoolData(schoolid, new SchoolDataCacheObject(schoolid,
		// badgeLevel, true));

		return Response.ok().build();
	}

	@PUT
	@Path("/{schoolid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response putSchool(@Context ContainerRequestContext requestContext, @PathParam("schoolid") String schoolid,
			int badgeLevel) {

		BadgesCacheObject obj = new BadgesCacheObject(schoolid, badgeLevel);

		badgesService.set(obj);
		// badgesService.setSchoolData(schoolid, new SchoolDataCacheObject(schoolid,
		// badgeLevel, false));

		return Response.ok().build();
	}

	@POST
	@Path("/{schoolid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateItems(@Context ContainerRequestContext requestContext, @PathParam("schoolid") String schoolid,
			int badgeLevel) {

		BadgesCacheObject obj = new BadgesCacheObject(schoolid, badgeLevel);

		badgesService.set(obj);
		// badgesService.setSchoolData(schoolid, new SchoolDataCacheObject(schoolid,
		// badgeLevel, false));

		return Response.ok().build();
	}

	@GET
	@Path("/progress/{schoolid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBadgeProgess(@Context ContainerRequestContext requestContext,
			@PathParam("schoolid") String schoolid) {

		SchoolDataCacheObject school = SchoolDataCacheObject.getSchoolFromID(schoolid);
		float progress;

		if (school != null) {
			progress = Labler.progressToNext(SchoolDataCacheObject.getSchoolFromID(schoolid));

			return Response.status(200).entity(progress).build();
		}

		return Response.status(203).build();
	}

	@GET
	@Path("/badge/{schoolid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBadge(@Context ContainerRequestContext requestContext, @PathParam("schoolid") String schoolid) {

		SchoolDataCacheObject school = SchoolDataCacheObject.getSchoolFromID(schoolid);

		if (school != null) {
			return Response.status(200).entity(school.badge).build();
		}

		return Response.status(203).build();
	}
}
