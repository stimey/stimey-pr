package eu.stimey.platform.microservice.badges.startup;

import actor4j.core.data.access.MongoUtils;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.microservice.badges.databind.caching.BadgesCacheObject;
import eu.stimey.platform.microservice.badges.databind.caching.SchoolDataCacheObject;

import java.util.ArrayList;

public class GenerateTestData {
	public void generate() {
		if (MongoDBService.getService().getClient().getDatabase(MongoDBService.getService().getDatabaseName())
				.listCollectionNames().into(new ArrayList<String>()).contains("school_badge"))
			return;
	}

	public void saveToDatabase(BadgesCacheObject badgesCacheObject, SchoolDataCacheObject schoolDataCacheObject) {
		MongoUtils.insertOne(badgesCacheObject, MongoDBService.getService().getClient(),
				MongoDBService.getService().getDatabaseName(), "school_badge");
		MongoUtils.insertOne(schoolDataCacheObject, MongoDBService.getService().getClient(),
				MongoDBService.getService().getDatabaseName(), "school_data");

	}
}
