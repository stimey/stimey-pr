package eu.stimey.platform.microservice.badges.utils;

import java.util.*;

public class SchoolStats {
	public String schoolid;
	public String schoolname;
	public ArrayList<String> Students = new ArrayList<String>();
	public ArrayList<String> Teachers = new ArrayList<String>();
	public ArrayList<CourseStats> Courses = new ArrayList<CourseStats>();
	public ArrayList<String> Parents = new ArrayList<String>();
	public double points;

	public SchoolStats(String id, String name) {
		this.schoolid = id;
		this.schoolname = name;
	}

	public void addStudent(String id) {
		this.Students.add(id);
	}

	public void addCourse(String id) {
		this.Courses.add(new CourseStats(id));
	}

	public void addTeacher(String id) {
		this.Teachers.add(id);
	}

	public void addStudentInCourse(String courseid, String studid) {
		for (int i = 0; i < Courses.size(); i++) {
			if (Courses.get(i).getCourseID().equals(courseid)) {
				Courses.get(i).addStudent(studid);
			}
		}
	}

	public void setSharedCourse(String courseid, boolean shared) {
		for (int i = 0; i < Courses.size(); i++) {
			if (Courses.get(i).getCourseID().equals(courseid)) {
				Courses.get(i).shared = shared;
			}
		}
	}

}
