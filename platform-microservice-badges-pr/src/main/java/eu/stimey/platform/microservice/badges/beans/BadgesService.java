package eu.stimey.platform.microservice.badges.beans;

import com.fasterxml.jackson.databind.*;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.data.access.Caching;

import eu.stimey.platform.library.utils.rest.client.StimeyClient;
import eu.stimey.platform.library.utils.security.JsonWebToken;
import eu.stimey.platform.microservice.badges.databind.caching.BadgesCacheObject;
import eu.stimey.platform.microservice.badges.startup.Global;

import eu.stimey.platform.microservice.badges.databind.caching.SchoolDataCacheObject;
import eu.stimey.platform.microservice.badges.rest.filter.AuthenticationFilter;
import eu.stimey.platform.microservice.badges.utils.SchoolStats;
import org.bson.Document;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;

import java.util.function.Consumer;
import java.util.*;

import static eu.stimey.platform.library.utils.StimeyLogger.logger;

public class BadgesService {
	private Caching<String, BadgesCacheObject> badgesCache;
	private Caching<String, SchoolDataCacheObject> schoolDataCache;

	private ArrayList<SchoolDataCacheObject> ListSchoolObjects;

	public BadgesService() {
		super();
		postConstruct();
	}

	@PostConstruct
	public void postConstruct() {
		badgesCache = new Caching<>("ms_badges", "schoolid", BadgesCacheObject.class, "school_badge",
				GlobalVariables.CACHE_CONFIG);
		badgesCache.clear();

		schoolDataCache = new Caching<>("ms_badges", "schoolid", SchoolDataCacheObject.class, "school_data",
				GlobalVariables.CACHE_CONFIG);
		schoolDataCache.clear();

		ListSchoolObjects = new ArrayList<SchoolDataCacheObject>();
	}

	public void schedule() {
		Cookie cookie = JsonWebToken.createCookie("", "", "admin", AuthenticationFilter.getPrivateKey(), Global.DOMAIN);

		System.out.println("bin in iterate");

		ArrayList<SchoolStats> ListSchools = new ArrayList<SchoolStats>();
		ArrayList<String> missionTypes = new ArrayList<String>();
		missionTypes.add("activeMissions");
		missionTypes.add("completedMissions");
		missionTypes.add("savedMissions");

		// if (schoolData != null) {
		Client client = StimeyClient.create();

		// ========================================================================================================================================
		// Read all schools
		Response response = StimeyClient.get(client, "dashboard", Global.DOCKER, "api/profiles/organizations/schools/",
				cookie);

		if (response.getStatus() == 200) {

			try {
				ObjectMapper mapper = new ObjectMapper();
				JsonNode actualObj = mapper.readTree(response.readEntity(String.class));
				String uID;

				// Save each school with id and name
				for (int i = 0; i < actualObj.size(); i++) {
					uID = actualObj.get(i).get("userid").textValue();
					String name = actualObj.get(i).get("username").textValue();
					ListSchools.add(new SchoolStats(uID, name));
				}

				// schoolStats = new ObjectMapper().readValue(response.readEntity(String.class),
				// SchoolStats.class);
			} catch (Exception e) {
				logger().info("errschool: " + e.toString());
				// e.printStackTrace();
			}
		} else {
			System.out.println("code in schoolresponse: " + response.getStatus());
		}

		// Read badge data for each school
		for (int j = 0; j < ListSchools.size(); j++) {
			Response responseForSchool;
			Response responseForTeacher;
			Response responseForParents;
			Response responseForStudents;
			String currID = ListSchools.get(j).schoolid;

			SchoolStats currSchool = ListSchools.get(j);

			// read students in school
			responseForSchool = StimeyClient.get(client, "dashboard", Global.DOCKER,
					"api/profiles/organizations/schools/" + currID + "/students", cookie); // holt sich per REST-Anfrage
																							// die Zahl der Studenten

			if (responseForSchool.getStatus() == 200) {
				try {
					ObjectMapper mapper = new ObjectMapper();
					JsonNode actualObj = mapper.readTree(responseForSchool.readEntity(String.class));

					for (int k = 0; k < actualObj.size(); k++) {
						currSchool.addStudent(actualObj.get(k).get("userid").textValue());
					}

				} catch (Exception e) {
					logger().info("errstudents: " + e.toString());
					// e.printStackTrace();
				}
			} else {
				System.out.println("code in studentsresponse: " + responseForSchool.getStatus());
			}

			// read teachers in school
			responseForSchool = StimeyClient.get(client, "dashboard", Global.DOCKER,
					"api/profiles/organizations/schools/" + currID + "/teachers", cookie);
			String teacherid = "";

			if (responseForSchool.getStatus() == 200) {
				// SchoolStats schoolStats = null;
				try {
					ObjectMapper mapper = new ObjectMapper();
					JsonNode actualObj = mapper.readTree(responseForSchool.readEntity(String.class));
					String courseid = "";

					// for every teacher, read missions for every teacher
					for (int k = 0; k < actualObj.size(); k++) {
						teacherid = actualObj.get(k).get("userid").textValue();

						responseForTeacher = StimeyClient.get(client, "courses", Global.DOCKER,
								"api/teachers/" + teacherid + "/missions", cookie);
						System.out.println("STATUS:" + responseForTeacher.getStatus() + ", TEACHERID: " + teacherid);

						if (responseForTeacher.getStatus() == 200) {
							try {
								JsonNode actualObjTeacher = mapper
										.readTree(responseForTeacher.readEntity(String.class));

								// get active, completed and saved missions
								for (int l = 0; l < missionTypes.size(); l++) {
									String currMissiontype = missionTypes.get(l);

									for (int m = 0; m < actualObjTeacher.get(currMissiontype).size(); m++) {
										courseid = actualObjTeacher.get(currMissiontype).get(m).get("mission")
												.get("_id").get("$oid").textValue();
										currSchool.addCourse(courseid);

										int studentsinCourse = actualObjTeacher.get(currMissiontype).get(m)
												.get("mission").get("students").size();

										for (int n = 0; n < studentsinCourse; n++) {
											String studid = actualObjTeacher.get(currMissiontype).get(m).get("mission")
													.get("students").get(n).textValue();
											currSchool.addStudentInCourse(courseid, studid);
										}

										if (actualObjTeacher.get(currMissiontype).get(m).get("mission")
												.get("missionMetadata").get("generalMetadata").get("published")
												.asBoolean()) {
											currSchool.setSharedCourse(courseid, true);
										}
									}
								}
							} catch (Exception e) {
								logger().info("error in missions: " + e.toString());
							}

						}
						currSchool.addTeacher(teacherid);
					}
				} catch (Exception e) {
					logger().info("error in teacher: " + e.toString());
				}
			}

			// read parents in school
			responseForParents = StimeyClient.get(client, "dashboard", Global.DOCKER,
					"api/profiles/organizations/schools/" + currID + "/parents", cookie);

			System.out.println("PARENTS STATUS: " + responseForParents.getStatus());
			if (responseForParents.getStatus() == 200) {
				try {
					String parentsNumber = responseForParents.readEntity(String.class);
					System.out.println("PARENTS: " + parentsNumber);

				} catch (Exception e) {
					logger().info("error in parents: " + e.toString());
					// e.printStackTrace();
				}
			}

			// read points from students

			double pointLimit = 0;
			double pointCount = 0;

			// for each student
			for (int l = 0; l < currSchool.Students.size(); l++) {
				String userID = currSchool.Students.get(l);
				responseForStudents = StimeyClient.get(client, "achievements", Global.DOCKER,
						"api/achievements/" + userID, cookie);

				try {
					ObjectMapper mapperStudent = new ObjectMapper();
					JsonNode actualObjStudent = mapperStudent.readTree(responseForStudents.readEntity(String.class));

					pointLimit += actualObjStudent.get("knowledgeLimit").asInt();
					pointLimit += actualObjStudent.get("socialLimit").asInt();
					pointLimit += actualObjStudent.get("creativityLimit").asInt();
					pointCount += actualObjStudent.get("totalPoints").asInt();
				} catch (Exception e) {
					logger().info("error in missions: " + e.toString());
				}
			}
			if (pointCount != 0 && pointLimit != 0) {
				currSchool.points = pointCount / pointLimit;
			} else {
				currSchool.points = 0;
			}
		}

		// evaluate writen schoolclasses
		int blueStudentsRegistered = 0;
		int blueTeachersRegistered = 0;
		float blueParentsPercent = 0;

		int silverCoursesByTeachers = 0;
		int silverStudentsInOneCourse = 0;
		int silverStudentsInOneCourseMax = 0;
		int silverCourseHasStudentsAmount = 0;
		int silverStudentsInOneCoursePercentage = 0;
		int silverCoursesShared = 0;

		int goldCoursesbyTeacher = 0;
		int goldCoursesShared = 0;
		double goldPointsStudents = 0;

		HashMap<String, Integer> listBlue = new HashMap<String, Integer>();
		HashMap<String, Integer> listSilver = new HashMap<String, Integer>();
		HashMap<String, Integer> listGold = new HashMap<String, Integer>();
		HashMap<String, Integer> listDiamond = new HashMap<String, Integer>();

		for (int k = 0; k < ListSchools.size(); k++) {
			String id = ListSchools.get(k).schoolid;
			String name = ListSchools.get(k).schoolname;
			SchoolStats currSchool = ListSchools.get(k);

			int parent = 1;
			float parentPercentage = 0;

			blueStudentsRegistered = currSchool.Students.size();
			blueTeachersRegistered = currSchool.Teachers.size();

			if (blueStudentsRegistered == 0) {
				parentPercentage = 0;
			} else {
				parentPercentage = ((float) parent / (float) blueStudentsRegistered) * 100;
			}

			silverCoursesByTeachers = goldCoursesbyTeacher = currSchool.Courses.size();

			int count = 0;
			silverStudentsInOneCourse = 0;
			silverStudentsInOneCourseMax = 0;
			int studentInCourses = 0;
			int studentsInTwoCourses = 0;
			String studentAdded = "";

			// badge requirement: each student is in at least 1 (2) courses
			for (int l = 0; l < currSchool.Students.size(); l++) {
				String studentIDToCheck = currSchool.Students.get(l);
				studentInCourses = 0;

				for (int m = 0; m < currSchool.Courses.size(); m++) {

					if (currSchool.Courses.get(m).Students.contains(studentIDToCheck)) {
						if (!studentAdded.equals(studentIDToCheck)) {
							count++;
							studentAdded = studentIDToCheck;
						}
						studentInCourses++;
					}
				}
				if (studentInCourses >= 2) {
					studentsInTwoCourses++;
				}
			}
			silverStudentsInOneCourse = count;
			silverStudentsInOneCourseMax = currSchool.Students.size();

			if (silverStudentsInOneCourse != 0 && silverStudentsInOneCourseMax != 0) {
				silverStudentsInOneCoursePercentage = (int) (double) (silverStudentsInOneCourse
						/ silverStudentsInOneCourseMax) * 100;

				float div = ((float) silverStudentsInOneCourse / (float) silverStudentsInOneCourseMax) * 100;
				silverStudentsInOneCoursePercentage = (int) div;
			} else {
				silverStudentsInOneCoursePercentage = 0;
			}

			// badge requirement: school has shared courses
			for (int l = 0; l < currSchool.Courses.size(); l++) {
				if (currSchool.Courses.get(l).shared) {
					silverCoursesShared++;
					goldCoursesShared++;
				}
			}

			// badge requirement: course has at least N students
			for (int l = 0; l < currSchool.Courses.size(); l++) {
				if (currSchool.Courses.get(l).Students.size() > silverCourseHasStudentsAmount) {
					silverCourseHasStudentsAmount = currSchool.Courses.get(l).Students.size();
				}
			}

			if (currSchool.Parents.size() != 0) {
				blueParentsPercent = blueStudentsRegistered / currSchool.Parents.size();
			} else {
				blueParentsPercent = 0;
			}
			goldPointsStudents = currSchool.points * 100;

			System.out.println("OPK: " + ListSchools.get(k).Courses.toString());

			System.out.println("DATA: Students: " + blueStudentsRegistered + ", Teachers: " + blueTeachersRegistered
					+ ", StudentsInOneCourse: " + silverStudentsInOneCourse + ", CoursesByTeacher: "
					+ silverCoursesByTeachers + ", CoursesShared: " + goldCoursesShared + ", StudentsInTwoCourses: "
					+ studentsInTwoCourses + ", StudentPoints: " + goldPointsStudents);

			listBlue.put("StudentAgreements", 40); // blueStudentsRegistered
			listBlue.put("TeacherAgreements", 20); // blueTeachersRegistered
			listBlue.put("ParentsAgreements", 38); // not aviable
			listBlue.put("AuthorizedMinor", 100); // not aviable

			listSilver.put("ParticipateSurvey", 10); // not aviable
			listSilver.put("ParentsAgreements", 50); // not aviable
			listSilver.put("TeacherCourses", 20); // silverCoursesByTeachers
			listSilver.put("Radio", 20); // not aviable
			listSilver.put("StudentsInOneCourse", 100); // silverStudentsInOneCourse
			listSilver.put("StudentsInCourseMax", 100); // silverCourseHasStudentAmount
			listSilver.put("SharedCourses", 10); // silverCoursesByTeachers

			listGold.put("ParticipateSurvey", 10);
			listGold.put("TeacherCourses", goldCoursesbyTeacher); // goldCoursesbyTeacher
			listGold.put("TeacherAgreements", blueTeachersRegistered); // blueTeachersRegistered
			listGold.put("ParentsAgreements", (int) blueParentsPercent); // not aviable
			listGold.put("StudentsInTwoCourses", studentsInTwoCourses); // goldStudentsInTwoCourses
			listGold.put("StudentPoints", (int) goldPointsStudents); // goldPointsStudents
			listGold.put("StudentsInCourseMax", silverCourseHasStudentsAmount); // silverCourseHasStudentsAmount
			listGold.put("SharedCourse", goldCoursesShared); // goldCoursesShared
			listGold.put("RadioCapsules", 0); // not aviable

			listDiamond.put("Workshop", 2);

			SchoolDataCacheObject temp = new SchoolDataCacheObject(id, name, listBlue, listSilver, listGold,
					listDiamond);
			ListSchoolObjects.add(temp);
			// SchoolDataCacheObject temp = new SchoolDataCacheObject(id, name, 40, 40,
			// (int)40, 1, silverCoursesByTeachers, silverStudentsInOneCourse,
			// silverStudentsInOneCourseMax);

			System.out.println("LABEL: " + temp.badge);

			schoolDataCache.set(id, temp);
		}

		logger().info("ende von iterate");

		logger().info("schedule start()");

		schedule_iterate("school_data", SchoolDataCacheObject.class, (ListSchoolObjects) -> {

		});
	}

	public <V> void schedule_iterate(String collectionName, Class<V> valueType, Consumer<V> consumer) {
		MongoCollection<Document> collection = MongoDBService.getService().getClient()
				.getDatabase(MongoDBService.getService().getDatabaseName()).getCollection(collectionName);

		FindIterable<Document> iterable = collection.find();
		iterable.forEach((Block<Document>) document -> {
			try {
				ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				V v = objectMapper.readValue(document.toJson(), valueType);
				if (consumer != null)
					consumer.accept(v);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

	public BadgesCacheObject get(String schoolid) {
		return badgesCache.get(schoolid);
	}

	public void set(BadgesCacheObject value) {
		badgesCache.set(value.schoolid, value);
	}

	public SchoolDataCacheObject getSchoolData(String schoolid) {
		return schoolDataCache.get(schoolid);
	}

	public void setSchoolData(String schoolid, SchoolDataCacheObject value) {
		schoolDataCache.set(schoolid, value);
	}

}
