package eu.stimey.platform.microservice.badges.rest;

import static eu.stimey.platform.library.utils.StimeyLogger.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ws.rs.ApplicationPath;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.microservice.badges.beans.BadgesService;
import eu.stimey.platform.microservice.badges.beans.SchedulerService;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import java.util.concurrent.TimeUnit;

/**
 * 
 * REST configuration
 *
 */
@ApplicationPath("api")
public class RESTConfig extends ResourceConfig {
	protected final MongoDBService mongoDBService;
	protected final RedisAMQPService redisAMQPService;

	protected final SchedulerService schedulerService;
	protected final BadgesService badgesService;

	public RESTConfig() {
		super();

		mongoDBService = MongoDBService.getService();
		redisAMQPService = RedisAMQPService.getService();
		schedulerService = new SchedulerService();
		badgesService = new BadgesService();

		logger().info("REST-Service started...");

		packages("eu.stimey.platform.microservice.badges.rest");

		register(new AbstractBinder() {
			protected void configure() {
				bind(mongoDBService).to(MongoDBService.class);
				bind(redisAMQPService).to(RedisAMQPService.class);

				bind(new UserSharedService()).to(UserSharedService.class);

				bind(schedulerService).to(SchedulerService.class);
				bind(badgesService).to(BadgesService.class);
			}
		});

		register(new JacksonJsonProvider().configure(SerializationFeature.INDENT_OUTPUT, true));

		postConstruct();
	}

	@PostConstruct
	public void postConstruct() {
		schedulerService.getScheduler().scheduleAtFixedRate(() -> badgesService.schedule(), 0, 90, TimeUnit.SECONDS);
	}

	@PreDestroy
	public void shutdown() {
		schedulerService.getScheduler().shutdown();
		logger().info("REST-Service stopped...");
	}
}
