package eu.stimey.platform.microservice.badges.rest.resources;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;
import eu.stimey.platform.library.utils.rest.RESTResponse;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/v1/example")
public class ExampleResource {
	@Context
	UserSharedService userSharedService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response example(@Context ContainerRequestContext requestContext) {
		String username = (String) requestContext.getProperty("username");
		System.out.println("example-Service " + username);
		UserSharedCacheObject obj = userSharedService.getUser((String) requestContext.getProperty("userid"));
		return Response.ok().entity(new RESTResponse(RESTResponse.SUCCESS, 200, obj, "")).build();
	}
}
