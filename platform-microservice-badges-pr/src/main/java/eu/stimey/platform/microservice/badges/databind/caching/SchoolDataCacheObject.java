package eu.stimey.platform.microservice.badges.databind.caching;

import eu.stimey.platform.microservice.badges.utils.Labler;

import java.util.ArrayList;
import java.util.HashMap;

/*
 * DataContainer to calculate Lable
 * @author Jeffrey Amofa
 * @date 12.09.18
 */
public class SchoolDataCacheObject {

	public String schoolid;

	public String getSchoolName() {
		return schoolName;
	}

	private String schoolName;

	public int badge;

	public double progress;

	public HashMap<String, Integer> listBlue = new HashMap<String, Integer>();
	public HashMap<String, Integer> listSilver = new HashMap<String, Integer>();
	public HashMap<String, Integer> listGold = new HashMap<String, Integer>();
	public HashMap<String, Integer> listDiamond = new HashMap<String, Integer>();

	public HashMap<String, Integer> listMinBlue = new HashMap<String, Integer>();
	public HashMap<String, Integer> listMinSilver = new HashMap<String, Integer>();
	public HashMap<String, Integer> listMinGold = new HashMap<String, Integer>();
	public HashMap<String, Integer> listMinDiamond = new HashMap<String, Integer>();

	// TEST-DATA
	// blue
	public int schoolCount = 1;
	public int parentCount = 38;
	public int teacherCount = 2;
	public int studentCount = 38;
	// silver
	public int sharings = 1;
	public int dataCollection = 2;
	public int registrationIncreasement = 50;
	public int courses = 2;
	public int radio = 2;
	public int courseparticipationStudent = 100;
	public int courseparticipationTeacher = 2;
	// gold
	public int participationIncreasement = 0;
	public int robotUsage = 0;
	// diamond
	public int workshop = 0;

	public SchoolDataCacheObject() {
		super();
	}

	public SchoolDataCacheObject(String schoolid, String schoolName, HashMap<String, Integer> listBlue,
			HashMap<String, Integer> listSilver, HashMap<String, Integer> listGold,
			HashMap<String, Integer> listDiamond) {
		this.schoolid = schoolid;
		this.schoolName = schoolName;

		this.listBlue = listBlue;
		this.listSilver = listSilver;
		this.listGold = listGold;
		this.listDiamond = listDiamond;

		this.badge = Labler.updateLabel(this);
		this.progress = Labler.progressToNext(this);

		SchoolHandler.ListSchools.add(this);
	}

	public SchoolDataCacheObject(String schoolid, String schoolName, int studentCount, int teacherCount,
			int parentCount, int schoolCount, int coursesTeacher, int coursesStudents,
			int silverStudentsInOneCourseMax) {
		super();
		this.schoolid = schoolid;
		this.studentCount = studentCount;
		this.teacherCount = teacherCount;
		this.parentCount = parentCount;
		this.schoolName = schoolName;
		this.schoolCount = schoolCount;

		// blue requirements
		listBlue.put("ParentsAgreements", parentCount);
		listBlue.put("TeacherAgreements", teacherCount);
		listBlue.put("StudentAgreements", 38);
		listBlue.put("SchoolAgreements", schoolCount);

		// silver requirements

		listSilver.put("Collection", 10);
		listSilver.put("Increasement", 50);
		listSilver.put("Courses", 80);
		listSilver.put("Radio", 10);
		listSilver.put("CourseparticipationStudent", coursesStudents);
		listSilver.put("CourceparticipationTeacher", coursesTeacher);

		// gold requirements
		listGold.put("DataCollection", 10);
		listGold.put("Radio", 10);
		listGold.put("Courses", 10);
		// Neuanmeldungen der anderen User...
		listGold.put("ParticipationIncreasement", 10);
		listGold.put("CourseparticipationStudent", 10);
		listGold.put("CourseparticipationTeacher", 10);
		listGold.put("TeacherAgreements", 0);

		// diamond requirements
		listDiamond.put("Workshop", 2);

		this.badge = Labler.updateLabel(this);
		this.progress = Labler.progressToNext(this);

		SchoolHandler.ListSchools.add(this);
	}

	public static SchoolDataCacheObject getSchoolFromID(String id) {
		if (id != null) {
			for (SchoolDataCacheObject school : SchoolHandler.ListSchools) {
				if (school.schoolid.equals(id)) {
					return school;
				}
			}
		}
		return null;
	}
}

class SchoolHandler {
	static ArrayList<SchoolDataCacheObject> ListSchools = new ArrayList<>();
}