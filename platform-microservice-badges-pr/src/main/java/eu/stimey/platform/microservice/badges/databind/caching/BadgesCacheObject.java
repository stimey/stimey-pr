package eu.stimey.platform.microservice.badges.databind.caching;

public class BadgesCacheObject {
	public static final int BADGE_NONE = 0;
	public static final int BADGE_BLUE = 1;
	public static final int BADGE_SILVER = 2;
	public static final int BADGE_GOLD = 3;

	public String schoolid;
	public String username;
	public int badge;
	public double progress;
	public SchoolDataCacheObject currentSchoolData;

	public BadgesCacheObject() {
		this("0", 0);
	}

	public BadgesCacheObject(String schoolid, int badge) {
		super();
		this.schoolid = schoolid;
		this.badge = badge;
	}

}
