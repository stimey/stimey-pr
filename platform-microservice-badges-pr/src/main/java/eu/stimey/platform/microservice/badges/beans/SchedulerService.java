package eu.stimey.platform.microservice.badges.beans;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import javax.annotation.PreDestroy;

/**
 * Timer/Scheduler Service
 * 
 */

public class SchedulerService {
	protected ScheduledExecutorService scheduler;

	public SchedulerService() {
		scheduler = Executors.newScheduledThreadPool(1);
	}

	@PreDestroy
	public void preDestroy() {
		scheduler.shutdown();
	}

	public ScheduledExecutorService getScheduler() {
		return scheduler;
	}

	/**
	 * Converts an absolute time in a relative time. The date is checked if it is in
	 * the future. It returns -1 if the date is invalid, else it returns the
	 * relative time in milliseconds.
	 * 
	 * This function is necessary to calculate when a task should be started (the
	 * relative time is needed).
	 * 
	 * @param date Absolute time.
	 * @return Time Relative time in milliseconds.
	 */
	public long absoluteInRelative(Date date) {
		long result = -1;

		Date now = new Date();
		if (date.after(now)) {
			result = date.getTime() - now.getTime();
		}

		return result;
	}
}
