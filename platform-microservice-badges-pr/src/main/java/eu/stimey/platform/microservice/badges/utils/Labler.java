package eu.stimey.platform.microservice.badges.utils;

import eu.stimey.platform.microservice.badges.databind.caching.SchoolDataCacheObject;

import java.util.*;

/*
 * Class to calculate Lable of a school
 * @author Jeffrey Amofa, Fabian Wessels
 * @date 12.09.18
 */

public class Labler {

	public static ArrayList<Integer> listMinBlue = new ArrayList<Integer>();
	public static ArrayList<Integer> listMinSilver = new ArrayList<Integer>();
	public static ArrayList<Integer> listMinGold = new ArrayList<Integer>();
	public static ArrayList<Integer> listMinDiamond = new ArrayList<Integer>();

	public static HashMap<String, Integer> RequirementsForBlue = new HashMap<String, Integer>();
	public static HashMap<String, Integer> RequirementsForSilver = new HashMap<String, Integer>();
	public static HashMap<String, Integer> RequirementsForGold = new HashMap<String, Integer>();
	public static HashMap<String, Integer> RequirementsForDiamond = new HashMap<String, Integer>();

	// blue
	public static int MinBparentsAgreements = 38;
	public static int MinBteachersAgreements = 2;
	public static int MinBstudentsAgreements = 38;
	public static int MinBAuthorizedMinor = 100;

	// silver
	public static int MinSparticipateSurvey = 1;
	public static int MinSparentsAgreements = 50;
	public static int MinScreatedCourses = 2;
	public static int MinSradio = 1;
	public static int MinSStudentsInOneCourses = 100;
	public static int MinSstudentsInCourseMax = 20;
	public static int MinSsharedCourse = 1;

	// gold
	public static int MinGparticipateSurvey = 2;
	public static int MinGcreatedCourses = 4;
	public static int MinGteacherAgreements = 4;
	public static int MinGparentsAgreements = 51;
	public static int MinGstudentsInTwoCourses = 100;
	public static int MinGstudentPoints = 40;
	public static int MinGstudentsInCourseMax = 30;
	public static int MinGsharedCourse = 3;
	public static int MinGRadioCapsules = 2;

	// diamond
	public static int MinDworkshop = 2;

	private static boolean initialized = false;

	public static float progressToNext(SchoolDataCacheObject data) {
		float prog = -1;
		float progSum = 0;
		float percentagePerRequirement = 0;
		float countRequirements = 0;
		HashMap<String, Integer> setRequirements = new HashMap<String, Integer>();
		HashMap<String, Integer> schooldata = new HashMap<String, Integer>();

		if (updateLabel(data) == 0) {
			countRequirements = RequirementsForBlue.size();
			setRequirements = RequirementsForBlue;
			schooldata = data.listBlue;
		} else if (updateLabel(data) == 1) {
			countRequirements = RequirementsForSilver.size();
			setRequirements = RequirementsForSilver;
			schooldata = data.listSilver;
		} else if (updateLabel(data) == 2) {
			countRequirements = RequirementsForGold.size();
			setRequirements = RequirementsForGold;
			schooldata = data.listGold;
		} else if (updateLabel(data) == 3) {
			countRequirements = RequirementsForDiamond.size();
			setRequirements = RequirementsForDiamond;
			schooldata = data.listDiamond;
		} else if (updateLabel(data) == 4) {
			return 1;
		} else {
			return -1;
		}

		percentagePerRequirement = 1 / countRequirements;

		for (Map.Entry<String, Integer> entry : setRequirements.entrySet()) {
			prog = 0;
			String key = entry.getKey();
			Integer value = entry.getValue();

			if (schooldata.get(key) == null || value == 0 || schooldata.get(key) == 0) {
				prog = 0;
			} else if (schooldata.get(key) >= value) {
				prog = percentagePerRequirement;
			} else {
				prog = (float) ((float) schooldata.get(key) / (float) value) * (float) percentagePerRequirement;

			}
			progSum += prog;
		}
		return progSum;
	}

	public static int updateLabel(SchoolDataCacheObject data) {

		if (data == null) {
			System.out.println("Labler.java : No Data in Cache-Object");
			return 0;
		}

		if (!initialized) {
			init();
			initialized = true;
		}

		boolean blueCheck = false;
		boolean silverCheck = false;
		boolean goldCheck = false;
		boolean diamondCheck = false;
		int result = 0;

		// check requirements for each badge
		if (RequirementsForBlue.size() > 0) {
			blueCheck = true;
			for (Map.Entry<String, Integer> entry : RequirementsForBlue.entrySet()) {
				String key = entry.getKey();
				Integer value = entry.getValue();

				if (key != null && data.listBlue.get(key) != null) {
					if (!(data.listBlue.get(key) >= value)) {
						blueCheck = false;
						break;
					}
				}
			}
		}

		if (RequirementsForSilver.size() > 0 && data.listSilver.size() > 0) {
			silverCheck = true;
			for (Map.Entry<String, Integer> entry : RequirementsForSilver.entrySet()) {
				String key = entry.getKey();
				Integer value = entry.getValue();

				if (key != null && data.listSilver.get(key) != null) {
					if (!(data.listSilver.get(key) >= value)) {
						silverCheck = false;
						break;
					}
				}
			}
		}

		if (RequirementsForGold.size() > 0 && data.listGold.size() > 0) {
			goldCheck = true;
			for (Map.Entry<String, Integer> entry : RequirementsForGold.entrySet()) {
				String key = entry.getKey();
				Integer value = entry.getValue();

				if (!(data.listGold.get(key) >= value)) {
					goldCheck = false;
					break;
				}
			}
		}

		if (RequirementsForDiamond.size() > 0 && data.listDiamond.size() > 0) {
			diamondCheck = true;
			for (Map.Entry<String, Integer> entry : RequirementsForDiamond.entrySet()) {
				String key = entry.getKey();
				Integer value = entry.getValue();

				if (!(data.listDiamond.get(key) >= value)) {
					diamondCheck = false;
					break;
				}
			}
		}

		// count badge, result is the badge-id
		if (blueCheck == true) {
			result++;

			if (silverCheck) {
				result++;

				if (goldCheck) {
					result++;

					if (diamondCheck) {
						result++;
					}
				}
			}
		}
		return result;
	}

	private static void init() {
		RequirementsForBlue = new HashMap<String, Integer>();
		RequirementsForSilver = new HashMap<String, Integer>();
		RequirementsForGold = new HashMap<String, Integer>();
		RequirementsForDiamond = new HashMap<String, Integer>();

		RequirementsForBlue.put("StudentAgreements", MinBstudentsAgreements);
		RequirementsForBlue.put("TeacherAgreements", MinBteachersAgreements);
		RequirementsForBlue.put("ParentsAgreements", MinBparentsAgreements);
		RequirementsForBlue.put("AuthorizedMinor", MinBAuthorizedMinor);

		RequirementsForSilver.put("ParticipateSurvey", MinSparticipateSurvey);
		RequirementsForSilver.put("ParentsAgreements", MinSparentsAgreements);
		RequirementsForSilver.put("TeacherCourses", MinScreatedCourses);
		RequirementsForSilver.put("Radio", MinSradio);
		RequirementsForSilver.put("StudentsInOneCourse", MinSStudentsInOneCourses);
		RequirementsForSilver.put("StudentsInCourseMax", MinSstudentsInCourseMax);
		RequirementsForSilver.put("SharedCourses", MinSsharedCourse);

		RequirementsForGold.put("ParticipateSurvey", MinGparticipateSurvey);
		RequirementsForGold.put("TeacherCourses", MinGcreatedCourses);
		RequirementsForGold.put("TeacherAgreements", MinGteacherAgreements);
		RequirementsForGold.put("ParentsAgreements", MinGparentsAgreements);
		RequirementsForGold.put("StudentsInTwoCourses", MinGstudentsInTwoCourses);
		RequirementsForGold.put("StudentPoints", MinGstudentPoints);
		RequirementsForGold.put("StudentsInCourseMax", MinGstudentsInCourseMax);
		RequirementsForGold.put("SharedCourse", MinGsharedCourse);
		RequirementsForGold.put("RadioCapsules", MinGRadioCapsules);

		RequirementsForDiamond.put("Workshop", MinDworkshop);

	}

	public static HashMap<String, Integer> getMinBlue() {
		return RequirementsForBlue;
	}

	public static HashMap<String, Integer> getMinSilver() {
		return RequirementsForSilver;
	}

	public static HashMap<String, Integer> getMinGold() {
		return RequirementsForGold;
	}

	public static HashMap<String, Integer> getMinDiamond() {
		return RequirementsForDiamond;
	}

	public static SchoolDataCacheObject setDataforBadge(String id, int label) {
		HashMap<String, Integer> listBlue = new HashMap<String, Integer>();
		HashMap<String, Integer> listSilver = new HashMap<String, Integer>();
		HashMap<String, Integer> listGold = new HashMap<String, Integer>();
		HashMap<String, Integer> listDiamond = new HashMap<String, Integer>();

		if (label < 0 || label > 4) {
			return null;
		}

		if (label >= 1) {
			for (Map.Entry<String, Integer> entryblue : RequirementsForBlue.entrySet()) {
				String key = entryblue.getKey();
				Integer value = entryblue.getValue();

				listBlue.put(key, value);
			}

			if (label >= 2) {
				for (Map.Entry<String, Integer> entrysilver : RequirementsForSilver.entrySet()) {
					String key = entrysilver.getKey();
					Integer value = entrysilver.getValue();

					listSilver.put(key, value);
				}

				if (label >= 3) {
					for (Map.Entry<String, Integer> entrygold : RequirementsForGold.entrySet()) {
						String key = entrygold.getKey();
						Integer value = entrygold.getValue();

						listGold.put(key, value);
					}

					if (label == 4) {
						for (Map.Entry<String, Integer> entrydiamond : RequirementsForGold.entrySet()) {
							String key = entrydiamond.getKey();
							Integer value = entrydiamond.getValue();

							listDiamond.put(key, value);
						}
					}
				}
			}
		}
		return new SchoolDataCacheObject(id, "name", listBlue, listSilver, listGold, listDiamond);
	}
}
