package eu.stimey.platform.microservice.badges.utils;

import java.util.*;

public class CourseStats {
	public String courseid;
	public boolean shared = false;
	public ArrayList<String> Students = new ArrayList<String>();

	public CourseStats(String id) {
		this.courseid = id;
	}

	public void addStudent(String id) {
		this.Students.add(id);
	}

	public CourseStats getCourseByID(String id) {
		if (this.courseid.equals(id)) {
			return this;
		}
		return null;
	}

	public String getCourseID() {
		return this.courseid;
	}

	public String toString() {
		String ret = "CourseID: " + this.courseid + ", Shared: " + this.shared;

		for (int i = 0; i < Students.size(); i++) {
			ret += this.Students.get(i);
		}
		return ret;
	}
}
