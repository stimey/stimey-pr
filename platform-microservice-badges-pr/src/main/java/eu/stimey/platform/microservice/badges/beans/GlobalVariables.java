package eu.stimey.platform.microservice.badges.beans;

import org.bson.types.ObjectId;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.data.access.CachingConfiguration;
import eu.stimey.platform.library.data.access.VolatileCachingConfiguration;

/**
 * Global variable settings for MongoDB and Redis
 *
 */
public class GlobalVariables {
	public static final ObjectId SCHOOL_ID = new ObjectId("5a0ee2427e9d6459996d3c0b");

	public static final CachingConfiguration CACHE_CONFIG = new CachingConfiguration(
			RedisAMQPService.getService().getJedisPool(), RedisAMQPService.getService().getRabbitmqConnection(),
			MongoDBService.getService().getClient(), MongoDBService.getService().getDatabaseName());

	public static final VolatileCachingConfiguration VOLATILE_CACHE_CONFIG = new VolatileCachingConfiguration(
			RedisAMQPService.getService().getJedisPool(), RedisAMQPService.getService().getRabbitmqConnection());
}
