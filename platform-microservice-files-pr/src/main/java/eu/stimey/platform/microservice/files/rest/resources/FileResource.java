package eu.stimey.platform.microservice.files.rest.resources;

import java.io.*;
import java.net.URLConnection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.*;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.bson.types.ObjectId;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.stimey.platform.library.utils.rest.RESTResponse;
import eu.stimey.platform.microservice.files.databind.FileInfo;
import eu.stimey.platform.microservice.files.startup.FileService;

/**
 * 
 * File resource
 *
 */
@Path("/files")
public class FileResource {
	@Context
	FileService fileService;

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(@FormDataParam("file") InputStream inputStream,
			@FormDataParam("file") FormDataContentDisposition formDataContentDisposition,
			@Context ContainerRequestContext requestContext, @Context HttpServletRequest request) {
		ObjectId fileId;

		if (inputStream == null && formDataContentDisposition == null)
			return errorResponse();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		String contentType;
		try {
			IOUtils.copy(inputStream, outputStream);
			contentType = URLConnection
					.guessContentTypeFromStream(new ByteArrayInputStream(outputStream.toByteArray()));
		} catch (IOException e) {
			e.printStackTrace();
			return errorResponse();
		}

		if (!validateFile(contentType))
			return errorResponse();

		fileId = fileService.saveFile(new ByteArrayInputStream(outputStream.toByteArray()),
				formDataContentDisposition.getFileName(), contentType,
				new ObjectId((String) requestContext.getProperty("userid")), request.getParameter("tag"));
		return Response.status(200).entity(fileId.toHexString()).build();
	}

	private Response errorResponse() {
		return Response.status(400)
				.entity(new RESTResponse(RESTResponse.ERROR, 400, "", "The request was error prone.")).build();
	}

	@POST
	@Path("{userId}/{fileName}")
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	@Produces(MediaType.APPLICATION_JSON)
	public Response upload2File(InputStream inputStream, @PathParam("userId") String userId,
			@PathParam("fileName") String fileName, @Context HttpServletRequest request) {
		boolean error = true;
		ObjectId fileId = null;

		if (inputStream != null && fileName != null) {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			String contentType = null;
			try {
				IOUtils.copy(inputStream, outputStream);
				contentType = URLConnection
						.guessContentTypeFromStream(new ByteArrayInputStream(outputStream.toByteArray()));

				if (validateFile(contentType)) {
					fileId = fileService.saveFile(new ByteArrayInputStream(outputStream.toByteArray()), fileName,
							contentType, new ObjectId(userId), request.getParameter("tag"));
					error = false;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (!error)
			return Response.status(200).entity(new RESTResponse(RESTResponse.SUCCESS, 200, fileId.toHexString(),
					"The file was successfully uploaded.")).build();
		else
			return Response.status(400)
					.entity(new RESTResponse(RESTResponse.ERROR, 400, "", "The request was error prone.")).build();
	}

	protected boolean validateFile(String fileType) {
		return true;
	}

	@GET
	@Path("{fileId}")
	// @Produces(MediaType.APPLICATION_OCTET_STREAM)
	public void downloadFile(@PathParam("fileId") String fileId,
			@QueryParam("content-deposition") @DefaultValue("attachment") String contentDeposition,
			@Context Request request, @Context HttpServletResponse response) {

		// fileId is unique, so eTag from fileId is also unique
		EntityTag etag = new EntityTag(fileId);
		// check if request contains eTag with fileId
		Response.ResponseBuilder builder = request.evaluatePreconditions(etag);
		// if builder is not null, request contains eTag
		if (builder != null) {
			response.setStatus(304);
			try {
				response.flushBuffer();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}

		Pair<FileInfo, InputStream> file = fileService.loadFile(fileId);

		response.setContentType(file.getKey().contentType);
		response.setHeader("Cache-Control", "max-age=604800");
		response.setHeader("ETag", etag.toString());
		response.setHeader("Content-Disposition", contentDeposition + "; filename=\"" + file.getKey().fileName + "\"");
		response.setContentLengthLong(file.getKey().length);

		try {
			IOUtils.copy(file.getValue(), response.getOutputStream());
			file.getValue().close();
			response.flushBuffer();
		} catch (IOException e) {
			e.printStackTrace();
			try {
				response.sendError(400, "The request was error prone.");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	@DELETE
	@Path("{fileId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeFile(@PathParam("fileId") String fileId) {
		if (fileId != null && ObjectId.isValid(fileId)) {
			fileService.removeFile(new ObjectId(fileId));
			return Response.status(202).entity(new RESTResponse(RESTResponse.SUCCESS, 202, "",
					"The request was accepted and the file will be removed.")).build();
		} else
			return Response.status(400)
					.entity(new RESTResponse(RESTResponse.ERROR, 400, "", "The request was error prone.")).build();
	}

	@GET
	@Path("find/{fileName}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findFiles(@PathParam("fileName") String fileName) {
		List<FileInfo> files = fileService.find(fileName);

		String json = null;
		try {
			json = new ObjectMapper().writeValueAsString(files);

			return Response.status(200)
					.entity(new RESTResponse(RESTResponse.SUCCESS, 200, json, "The request was successfully!")).build();
		} catch (JsonProcessingException e) {
			return Response.serverError()
					.entity(new RESTResponse(RESTResponse.ERROR, 500, e.getMessage(), "Internal Server Error")).build();
		}
	}

	@GET
	@Path("find/{userId}/{fileName}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response find2Files(@PathParam("userId") String userId, @PathParam("fileName") String fileName) {
		List<FileInfo> files = fileService.find(fileName, new ObjectId(userId));

		String json = null;
		try {
			json = new ObjectMapper().writeValueAsString(files);

			return Response.status(200)
					.entity(new RESTResponse(RESTResponse.SUCCESS, 200, json, "The request was successfully!")).build();
		} catch (JsonProcessingException e) {
			return Response.serverError()
					.entity(new RESTResponse(RESTResponse.ERROR, 500, e.getMessage(), "Internal Server Error")).build();
		}
	}

	@GET
	@Path("find/{userId}/domain/{domain}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response find3Files(@PathParam("userId") String userId, @PathParam("domain") String domain) {
		List<FileInfo> files = fileService.findByDomain(domain, new ObjectId(userId));

		String json = null;
		try {
			json = new ObjectMapper().writeValueAsString(files);

			return Response.status(200)
					.entity(new RESTResponse(RESTResponse.SUCCESS, 200, json, "The request was successfully!")).build();
		} catch (JsonProcessingException e) {
			return Response.serverError()
					.entity(new RESTResponse(RESTResponse.ERROR, 500, e.getMessage(), "Internal Server Error")).build();
		}
	}

	@GET
	@Path("info/{fileId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fileInfo(@PathParam("fileId") String fileId) {
		FileInfo file = fileService.info(new ObjectId(fileId));

		String json = null;
		try {
			json = new ObjectMapper().writeValueAsString(file);

			return Response.status(200)
					.entity(new RESTResponse(RESTResponse.SUCCESS, 200, json, "The request was successfully!")).build();
		} catch (JsonProcessingException e) {
			return Response.serverError()
					.entity(new RESTResponse(RESTResponse.ERROR, 500, e.getMessage(), "Internal Server Error")).build();
		}
	}
}
