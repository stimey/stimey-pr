package eu.stimey.platform.microservice.files.databind;

import java.util.Objects;

/**
 * 
 * Corresponding file information of the saved file within the FileService.
 *
 */
public class FileInfo {
	public String fileId;
	public String fileName;
	public String contentType;

	public String userId;
	public String domain;

	public long length;

	public FileInfo() {
		super();
	}

	public FileInfo(String fileId, String fileName, String contentType, String userId, String domain) {
		this(fileId, fileName, contentType, userId, domain, -1);
	}

	public FileInfo(String fileId, String fileName, String contentType, String userId, String domain, long length) {
		super();
		this.fileId = fileId;
		this.fileName = fileName;
		this.contentType = contentType;
		this.userId = userId;
		this.domain = domain;
		this.length = length;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		FileInfo fileInfo = (FileInfo) o;
		return length == fileInfo.length && Objects.equals(fileId, fileInfo.fileId)
				&& Objects.equals(fileName, fileInfo.fileName) && Objects.equals(contentType, fileInfo.contentType)
				&& Objects.equals(userId, fileInfo.userId) && Objects.equals(domain, fileInfo.domain);
	}

	@Override
	public int hashCode() {
		return Objects.hash(fileId, fileName, contentType, userId, domain, length);
	}
}
