package eu.stimey.platform.microservice.files.startup;

import eu.stimey.platform.library.core.startup.DefaultGlobal;

/**
 * 
 * Global settings (environment variables)
 *
 */
public final class Global extends DefaultGlobal {

}
