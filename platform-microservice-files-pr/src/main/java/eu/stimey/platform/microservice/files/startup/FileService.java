package eu.stimey.platform.microservice.files.startup;

import static eu.stimey.platform.library.utils.StimeyLogger.logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.Pair;
import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.microservice.files.databind.FileInfo;

/**
 * 
 * File service for the whole STIMEY platform. Handling files, saved in a MongoDB database.
 *
 */
public class FileService extends MongoDBService {
	protected static FileService service;

	public FileService(String host, int port, String databaseName) {
		super(host, port, databaseName);
	}

	public ObjectId saveFile(InputStream inputStream, String fileName, String contentType, ObjectId userId,
			String tag) {
		ObjectId result = null;

		@SuppressWarnings("deprecation")
		GridFS gridFS = new GridFS((DB) client.getDB(databaseName));

		GridFSInputFile inputFile = gridFS.createFile(inputStream);
		inputFile.setFilename(fileName);
		inputFile.setContentType(contentType);
		// Meta-Data
		inputFile.put("userId", userId);
		if (tag != null)
			inputFile.put("tag", tag);
		inputFile.save();

		result = (ObjectId) inputFile.getId();
		logger().debug(inputFile.getId());

		// Check if available
		GridFSDBFile outputFile = gridFS.findOne(fileName);
		logger().debug(outputFile.get("userId"));

		return result;
	}

	public boolean loadFile(HttpServletResponse response, ObjectId fileId) {
		boolean result = false;

		@SuppressWarnings("deprecation")
		GridFS gridFS = new GridFS((DB) client.getDB(databaseName));

		GridFSDBFile outputFile = gridFS.findOne(fileId);
		logger().debug(outputFile.get("userId"));

		try {
			logger().debug(outputFile.getFilename());
			logger().debug(outputFile.getContentType());

			response.setContentType(outputFile.getContentType());
			response.setHeader("Content-Disposition", "inline; filename=\"" + outputFile.getFilename() + "\"");
			response.setContentLengthLong(outputFile.getLength());

			org.apache.commons.io.IOUtils.copy(outputFile.getInputStream(), response.getOutputStream());
			response.flushBuffer();
			result = true;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	public Pair<FileInfo, InputStream> loadFile(String fileId) {
		@SuppressWarnings("deprecation")
		GridFS gridFS = new GridFS((DB) client.getDB(databaseName));

		GridFSDBFile dbFile = gridFS.findOne(new ObjectId(fileId));
		FileInfo fileInfo = new FileInfo(((ObjectId) dbFile.getId()).toHexString(), dbFile.getFilename(),
				dbFile.getContentType(), ((ObjectId) dbFile.get("userId")).toHexString(), (String) dbFile.get("tag"),
				(Long) dbFile.get("length"));

		return Pair.of(fileInfo, dbFile.getInputStream());
	}

	public void removeFile(ObjectId fileId) {
		@SuppressWarnings("deprecation")
		GridFS gridFS = new GridFS((DB) client.getDB(databaseName));
		gridFS.remove(fileId);
	}

	public List<FileInfo> find(String fileName) {
		List<FileInfo> files = new ArrayList<>();

		@SuppressWarnings("deprecation")
		GridFS gridFS = new GridFS((DB) client.getDB(databaseName));
		List<GridFSDBFile> list = gridFS.find(fileName);

		for (GridFSDBFile file : list)
			files.add(new FileInfo(((ObjectId) file.getId()).toHexString(), file.getFilename(), file.getContentType(),
					((ObjectId) file.get("userId")).toHexString(), (String) file.get("tag")));

		return files;
	}

	public List<FileInfo> find(String fileName, ObjectId userId) {
		List<FileInfo> files = new ArrayList<>();

		@SuppressWarnings("deprecation")
		GridFS gridFS = new GridFS((DB) client.getDB(databaseName));
		List<GridFSDBFile> list = gridFS.find(new BasicDBObject("userId", userId).append("filename", fileName));

		for (GridFSDBFile file : list)
			files.add(new FileInfo(((ObjectId) file.getId()).toHexString(), file.getFilename(), file.getContentType(),
					((ObjectId) file.get("userId")).toHexString(), (String) file.get("tag")));

		return files;
	}

	public List<FileInfo> findByDomain(String domain, ObjectId userId) {
		List<FileInfo> files = new ArrayList<>();

		@SuppressWarnings("deprecation")
		GridFS gridFS = new GridFS((DB) client.getDB(databaseName));
		List<GridFSDBFile> list = gridFS.find(new BasicDBObject("userId", userId).append("tag", domain));

		for (GridFSDBFile file : list)
			files.add(new FileInfo(((ObjectId) file.getId()).toHexString(), file.getFilename(), file.getContentType(),
					((ObjectId) file.get("userId")).toHexString(), (String) file.get("tag")));

		return files;
	}

	public FileInfo info(ObjectId fileId) {
		FileInfo result = null;

		@SuppressWarnings("deprecation")
		GridFS gridFS = new GridFS((DB) client.getDB(databaseName));
		GridFSDBFile file = gridFS.find(fileId);
		if (file != null)
			result = new FileInfo(((ObjectId) file.getId()).toHexString(), file.getFilename(), file.getContentType(),
					((ObjectId) file.get("userId")).toHexString(), (String) file.get("tag"));

		return result;
	}

	public static void start() {
		logger().info(String.format("MongoDB (GridFS) - Service started..."));
		if (Global.DOCKER)
			service = new FileService("mongo-files", 27017, "ms_files");
		else
			service = new FileService("localhost", 27017, "ms_files");
		service.postConstrcut();
	}

	public static void stop() {
		service.preDestroy();
		logger().info(String.format("MongoDB (GridFS) - Service stopped..."));
	}

	public static FileService getService() {
		return service;
	}
}
