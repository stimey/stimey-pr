package eu.stimey.platform.microservice.files.rest.resources;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.*;

import eu.stimey.platform.microservice.files.databind.FileInfo;
import eu.stimey.platform.microservice.files.startup.FileService;
import org.apache.commons.lang3.tuple.Pair;

import java.io.*;

import static eu.stimey.platform.library.utils.StimeyLogger.logger;

/**
 * 
 * Streaming resource for audio files.
 *
 */
@Path("/files/stream")
public class StreamResource {
	@Context
	FileService fileService;

	final int CHUNK_SIZE = (1024 * 1024) / 2;

	@GET
	@Path("{fileId}")
	@Produces("audio/mpeg")
	public Response streamAudioFile(@PathParam("fileId") String fileId, @HeaderParam("Range") String range) {
		final Pair<FileInfo, InputStream> file = fileService.loadFile(fileId);

		if (file == null)
			return Response.status(404).build();

		return this.buildStreamResponse(range, file);
	}

	private Response buildStreamResponse(final String range, final Pair<FileInfo, InputStream> file) {
		String contentType = (getExtension(file.getKey().fileName).equals("wav") ? "audio/wav" : "audio/mpeg");

		if (range == null) {
			final long length = file.getKey().length;
			Streamer streamer = new Streamer(length, file.getValue());

			return Response.status(200).entity(streamer).header("Pragma", "no-cache").header("Content-Length", length)
					.header("Accept-Ranges", "bytes").header("Content-Type", contentType).build();
		}

		String[] ranges = range.split("=")[1].split("-");

		final long from = Long.parseLong(ranges[0]);

		long to = CHUNK_SIZE + from;
		if (to >= file.getKey().length) {
			to = (int) (file.getKey().length - 1);
		}
		if (ranges.length == 2) {
			to = Integer.parseInt(ranges[1]);
		}

		InputStream fileStream = file.getValue();

		// skip the bytes | seek
		try {
			fileStream.skip(from);
		} catch (IOException e) {
			logger().error(e.getMessage());
		}

		final long len = to - from + 1;
		Streamer streamer = new Streamer(len, fileStream);

		return Response.status(Response.Status.PARTIAL_CONTENT).header("Accept-Ranges", "bytes")
				.header("Cache-Control", "max-age=604800").header("Content-Length", streamer.getLength())
				.header("Content-Range", "bytes " + from + "-" + to + "/" + file.getKey().length)
				.header("Content-Type", contentType).entity(streamer).build();
	}

	private String getExtension(final String filename) {
		int idx = filename.lastIndexOf(".");
		return filename.substring(idx);
	}

	private class Streamer implements StreamingOutput {
		private long length;
		private InputStream inputStream;

		final byte[] buffer = new byte[4096];

		public Streamer(long length, InputStream inputStream) {
			this.length = length;
			this.inputStream = inputStream;
		}

		@Override
		public void write(OutputStream outputStream) throws IOException, WebApplicationException {
			while (length != 0) {
				int read = this.inputStream.read(buffer, 0, buffer.length > length ? (int) length : buffer.length);
				outputStream.write(buffer, 0, read);
				length -= read;
			}
		}

		public long getLength() {
			return this.length;
		}
	}
}
