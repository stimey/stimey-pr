package eu.stimey.platform.microservice.files.startup;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.utils.StimeyLogger;

/**
 * Global web application settings
 * 
 */
@WebListener
public class ServiceServletContextListener implements ServletContextListener {
	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		Global.startup();

		StimeyLogger.setApplicationName(servletContextEvent.getServletContext().getContextPath());

		RedisAMQPService.start();
		FileService.start();
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		RedisAMQPService.stop();
		FileService.stop();
	}
}
