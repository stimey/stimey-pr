package eu.stimey.platform.microservice.lab.rest.dataTransfer.responses.keyword;

import eu.stimey.platform.microservice.lab.databind.keyword.Keyword;
import eu.stimey.platform.microservice.lab.rest.utils.RESTUri;

import javax.ws.rs.container.ContainerRequestContext;
import java.util.LinkedList;
import java.util.List;

/**
 * KeywordListResponse.java
 *
 * @author Sascha M. Schumacher (Sash)
 * @version 2018.04.10.0000
 */
public class KeywordListResponse {

	/** the uris to the keywords */
	public List<Object> keywords;

	/**
	 * @param context  @see {@link ContainerRequestContext}
	 * @param keywords a list with keywords for the response
	 * @param expand   the parameter to expand keywords
	 */
	public KeywordListResponse(ContainerRequestContext context, List<Keyword> keywords) {
		super();
		this.keywords = new LinkedList<>();
		for (Keyword keyword : keywords) {
			if (keyword != null)
				this.keywords.add(RESTUri.parseSubPath(context, keyword.tag));
		}
	}

	/** @see {@link Object#toString()} */
	@Override
	public String toString() {
		return "KeywordListResponse [keywords=" + keywords + "]";
	}

}
