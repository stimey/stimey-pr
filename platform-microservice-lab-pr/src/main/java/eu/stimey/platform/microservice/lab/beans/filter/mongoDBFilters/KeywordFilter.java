package eu.stimey.platform.microservice.lab.beans.filter.mongoDBFilters;

import eu.stimey.platform.library.fuzzy.search.FuzzySearch;
import org.bson.Document;

import java.util.LinkedList;
import java.util.List;

/**
 * KeywordsFilter.java
 *
 * @author Sascha M. Schumacher
 * @version 2018.04.13.0000
 */
public class KeywordFilter extends DocumentFilter {

	/**
	 * builds a filter with the soundex for the searching in the database
	 * 
	 * @param query the filter for the soundex
	 * @return a document with the soundex filter for the searching in the database
	 */
	public static Document soundex(String query) {
		if (query == null || query.isEmpty())
			return new Document();
		List<Document> result = new LinkedList<>();
		for (String part : query.split(" ")) {
			String soundex = FuzzySearch.soundexTrimZeros(FuzzySearch.soundex(part));
			result.add(new Document("soundex", regex(soundex, CASE_INSENSITIVITY)));
		}
		return or(result.toArray(new Document[result.size()]));
	}

	/**
	 * builds a filter with the tag for the searching in the database
	 * 
	 * @param query the filter for the tag
	 * @return a document with the tag filter for the searching in the database
	 */
	public static Document tag(String query) {
		if (query == null || query.length() <= 0)
			return new Document();
		List<Document> result = new LinkedList<>();
		for (String part : query.split(" ")) {
			result.add(new Document("tag", regex(part, CASE_INSENSITIVITY)));
		}
		if (result.size() == 0)
			return new Document();
		if (result.size() == 1)
			return result.get(0);
		return and(result.toArray(new Document[result.size()]));
	}

}
