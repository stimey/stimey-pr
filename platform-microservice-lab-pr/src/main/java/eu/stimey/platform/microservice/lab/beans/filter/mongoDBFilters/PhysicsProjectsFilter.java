package eu.stimey.platform.microservice.lab.beans.filter.mongoDBFilters;

import eu.stimey.platform.library.data.access.Caching;
import org.bson.Document;

import java.util.LinkedList;
import java.util.List;

public class PhysicsProjectsFilter extends DocumentFilter {

	/**
	 * builds a filter with the keywords, the topics, the stages and the
	 * difficulties for the searching in the database
	 *
	 * @return a document with the keywords filter, the topics filter, the stage
	 *         filter and the difficulty filter for the searching in the database
	 */
	public static Document filter(List<String> userId) {
		List<Document> result = new LinkedList<>();
		Document buffer;
		if (!(buffer = userIds(userId)).isEmpty())
			result.add(buffer);

		return and(result.toArray(new Document[result.size()]));
	}

	/**
	 * builds a filter with the userIds for searching in the database
	 *
	 * @param userIds the filter for the userIds
	 * @return a document with the userIds filter for searching in the database
	 */
	public static Document userIds(List<String> userIds) {
		if (userIds == null || userIds.isEmpty()) {
			return new Document();
		}
		List<Document> result = new LinkedList<>();
		for (String userId : userIds) {
			result.add(new Document("userid", userId));
		}
		if (result.size() == 0)
			return new Document();
		if (result.size() == 1)
			return result.get(0);
		return or(result.toArray(new Document[result.size()]));
	}

}
