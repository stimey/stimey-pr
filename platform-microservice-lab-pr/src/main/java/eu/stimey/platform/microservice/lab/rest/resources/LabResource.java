package eu.stimey.platform.microservice.lab.rest.resources;

import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.data.access.databind.Message;
import eu.stimey.platform.microservice.lab.beans.CreativityTestService;
import eu.stimey.platform.microservice.lab.beans.ResourceService;
import eu.stimey.platform.microservice.lab.beans.filter.mongoDBFilters.ResourceFilter;
import eu.stimey.platform.microservice.lab.databind.Creativity;
import eu.stimey.platform.microservice.lab.databind.Resource;
import eu.stimey.platform.microservice.lab.databind.metadata.Category;
import eu.stimey.platform.microservice.lab.databind.metadata.Difficulty;
import eu.stimey.platform.microservice.lab.databind.metadata.FileMetadata;
import eu.stimey.platform.microservice.lab.databind.metadata.Stage;
import eu.stimey.platform.microservice.lab.rest.dataTransfer.CreativityData;
import eu.stimey.platform.microservice.lab.rest.dataTransfer.QuestionData;
import eu.stimey.platform.microservice.lab.rest.dataTransfer.ResourceDTO;
import eu.stimey.platform.microservice.lab.rest.dataTransfer.ResourceOverviewDTO;
import eu.stimey.platform.microservice.lab.rest.dataTransfer.ValueData;
import eu.stimey.platform.microservice.lab.rest.utils.RESTUri;
import org.apache.commons.lang3.tuple.ImmutablePair;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Path("/resource")
public class LabResource {
	public static final String DEFAULT_OFFSET = "0";
	public static final String DEFAULT_LIMIT = "32";

	@Inject
	ResourceService resourceService;
	@Inject
	CreativityTestService creativityService;

	// @Inject
	// KeywordService keywordService;

	@Inject
	UserSharedService userSharedService;

	@GET
	@Path("/test")
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response increaseResourceViewCountById(@Context ContainerRequestContext context) {
		System.out.println("keywordsTemp");
		// macht probleme
		List<String> keywordsTemp = new ArrayList<>();
		resourceService.getCache().publish("addKeywords", new Message(null, keywordsTemp));

		return Response.status(Response.Status.OK).entity(0).build();
	}

	@GET
	@Path("/{resourceId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getResource(@Context ContainerRequestContext context, @PathParam("resourceId") String resourceId) {
		Resource resource = resourceService.getResourceById(resourceId);
		System.out.println("resourceID: " + resourceId);

		// resource.setAmountOfViews();

		ResourceOverviewDTO resourceOverviewDTO = buildResourceOverviewDTO(resource);
		return Response.status(Response.Status.OK).entity(resourceOverviewDTO).build();
	}

	/**
	 * Requests the server to return a list with resources. The query parameters
	 * influence the list and can be combined.
	 *
	 * @param context
	 * @see ContainerRequestContext
	 * @param offset     indicates with which resources will started (default: 0)
	 * @param limit      indicates how many resources are returned (default: 32)
	 * @param query      search criteria to which the resources must correspond
	 *                   (default: empty)
	 * @param newest     filter for the aspects (default: false, values: true=newest
	 *                   | false=hottest)
	 * @param category   filter by category (default: any, values: library,
	 *                   media,...)
	 * @param stage      filter by stage (default: any, values: Basic, Primary,...)
	 * @param difficulty filter by difficulty (default: any, values: easy,
	 *                   medium,...
	 * @return a response with the requested list
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getResources(@Context ContainerRequestContext context, @QueryParam("id[]") List<String> ids,
			@QueryParam("o") @DefaultValue(DEFAULT_OFFSET) int offset,
			@QueryParam("l") @DefaultValue(DEFAULT_LIMIT) int limit, @QueryParam("q") @DefaultValue("") String query,
			@QueryParam("n") @DefaultValue("false") boolean newest,
			@QueryParam("e") @DefaultValue("false") boolean eachCategory,
			@QueryParam("t") @DefaultValue("") String resourceTitle,
			@QueryParam("c") @DefaultValue("any") String category, @QueryParam("s") @DefaultValue("any") String stage,
			@QueryParam("d") @DefaultValue("any") String difficulty) {

		if (!ids.isEmpty()) {
			List<ResourceOverviewDTO> resourcesDTOList = new ArrayList<>();
			this.resourceService.getResourcesByIds(ids)
					.forEach(resource -> resourcesDTOList.add(buildResourceOverviewDTO(resource)));
			return Response.status(Response.Status.OK).entity(resourcesDTOList).build();
		}
		try {
			// String keywords =
			// keywordService.getTagsOfKeywords(KeywordFilter.soundex(query), 0, 0);

			List<Resource> resources = resourceService.getResourceList(
					ResourceFilter.filter(query, resourceTitle, category, stage, difficulty), offset, limit);

			System.out.println(resources);
			List<ResourceOverviewDTO> resourcesDTOList = new ArrayList<>();

			for (Resource resource : resources) {
				System.out.println(
						resource.get_id() + "  " + resource.getName() + "   " + resource.getCategory().getText());
				resourcesDTOList.add(buildResourceOverviewDTO(resource));
			}

			System.out.println("resourceList: " + resourcesDTOList);

			return Response.status(Response.Status.OK).header("URI", RESTUri.parseCompletePath(context))
					.entity(resourcesDTOList).build();
		} catch (Exception ex) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.header("URI", RESTUri.parseCompletePath(context))
					.entity(new ImmutablePair<String, String>("Error", ex.getMessage())).build();
		}
	}

	// format of return string "dd.MM.yyyy at HH:mm"
	private String getDate(Date date) {
		SimpleDateFormat sdfDate = new SimpleDateFormat("dd.MM.yyyy");
		SimpleDateFormat sdfDateTime = new SimpleDateFormat("HH:mm");

		return "" + sdfDate.format(date) + " at " + sdfDateTime.format(date);
	}

	private ResourceOverviewDTO buildResourceOverviewDTO(Resource resource) {

		String externalUrl;
		String fileName;
		String fileId;
		String authorName = "";
		try {
			authorName = this.userSharedService.getUser(resource.getAuthorId()).username;
		} catch (Exception e) {
			System.out.println(e);
		}

		Date date = new Date(Long.valueOf(resource.getCreationDate()));
		String creationDate = getDate(date);

		if (resource.isExternalResource()) {
			externalUrl = resource.getExternalUrl();
			fileName = "";
			fileId = "";
		} else {
			externalUrl = "";
			fileName = resource.getUploadedFile().getFileName();
			fileId = resource.getUploadedFile().getFileId();
		}

		System.out.println("resource DTO: " + resource.getCategory().getText());

		ResourceOverviewDTO resourceDTO = new ResourceOverviewDTO(resource.get_id().toHexString(), authorName,
				resource.getAuthorId(), resource.getName(), resource.getDescription(), resource.getKeywords(),
				resource.getStage().getText(), resource.getDifficulty().getText(), resource.getCategory().getText(),
				creationDate, Integer.toString(resource.getAmountOfViews()), resource.isExternalResource(), externalUrl,
				fileName, fileId);

		return resourceDTO;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createResource(@Context ContainerRequestContext context, ResourceDTO resourceData) {
		String authorId = (String) context.getProperty("userid");

		Resource labResource = new Resource();
		labResource.setAuthorId(authorId);
		labResource.setName(resourceData.name);

		Date date = new Date();

		labResource.setCreationDate(Long.toString(date.getTime()));
		labResource.setDescription(resourceData.description);
		labResource.setStage(Stage.fromString(resourceData.stage));
		labResource.setDifficulty(Difficulty.fromString(resourceData.difficulty));

		List<String> keywordsTemp = new ArrayList<>();
		String keyWordTemp = resourceData.keywords;
		String keyWordsByResourceTitleTemp = resourceData.name;

		if (keyWordTemp.length() > 0) {
			keyWordTemp = keyWordTemp.replace("#", "");

			String[] stringSplit1 = keyWordTemp.split(" *(, ) *");

			// später in eine schleife kombinieren
			for (int i = 0; i < stringSplit1.length; i++) {
				keywordsTemp.add(stringSplit1[i]);
				// Keyword keyword = new Keyword(stringSplit1[i]);
				System.out.println("String split1: " + stringSplit1[i]);
			}
		}

		String[] stringSplit2 = keyWordsByResourceTitleTemp.split(" *(, | |-|:) *");

		for (int i = 0; i < stringSplit2.length; i++) {
			keywordsTemp.add(stringSplit2[i]);
			// Keyword keyword = new Keyword(stringSplit2[i]);
			System.out.println("String split2: " + stringSplit2[i]);
		}

		resourceService.getCache().publish("addKeywords", new Message(null, keywordsTemp));

		labResource.setKeywords(keywordsTemp);

		labResource.setCategory(Category.fromString(resourceData.category));

		if (resourceData.isExternalResource) {
			labResource.setExternalResource(true);
			labResource.setExternalUrl(resourceData.externalUrl);
		} else {
			labResource.setExternalResource(false);
			labResource.setUploadedFile(new FileMetadata(resourceData.uploadedFile.fileId,
					resourceData.uploadedFile.fileName, resourceData.uploadedFile.contentType));
		}
		resourceService.setResource(labResource);

		return Response.status(Response.Status.OK).entity(labResource).build();
	}

	/**
	 * Requests the server to remove the resource with the given id.
	 *
	 * @param context
	 * @see ContainerRequestContext
	 * @param resourceId the id of the specific resource
	 * @return a response with a message
	 */
	@DELETE
	@Path("/{resourceId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteResource(@Context ContainerRequestContext context,
			@PathParam("resourceId") String resourceId) {
		try {

			System.out.println("deleeeete");
			resourceService.delResource(resourceId);

			return Response.status(Response.Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(
					new ImmutablePair<String, String>("Message", "the resource with id=" + resourceId + " is deleted."))
					.build();
		} catch (Exception ex) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
					.header("URI", RESTUri.parseAbsolutePath(context))
					.entity(new ImmutablePair<String, String>("Error", ex.getMessage())).build();
		}
	}

	@POST
	@Path("/creativity/")
	@Consumes(MediaType.APPLICATION_JSON)
	public void collectData(@Context ContainerRequestContext context, CreativityData data) {

		String userid = (String) context.getProperty("userid");

		System.out.println("USER:" + userid);
		System.out.println("LANG:" + data.lang);
		System.out.println("AGE:" + data.age);
		System.out.println("GENDER:" + data.gender);
		System.out.println("FIRST:" + data.first);

		for (QuestionData q : data.first) {
			System.out.println(q.answer);

			for (ValueData v : q.suggestions) {
				System.out.println(v.value);
			}

		}

		Creativity creativity = new Creativity(userid, data.lang, data.age, data.gender, data.first, data.second);

		creativityService.setCreativity(userid, creativity);
		// JSONObject json = new JSONObject(data.first);
		// String statistics = json.getString("statistics");
		// JSONObject name1 = json.getJSONObject("John");
		// String ageJohn = name1.getString("Age");
		//
		// System.out.println("FIRST:" + data.second);
		//
		// Creativity c = creativityService.getCreativity(userid);
		// System.out.println("TEST:" + c.getFirst().get(0).answer);
	}

	@GET
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createData(@Context ContainerRequestContext context) {
		System.out.println("TEST");

		List<Creativity> c = creativityService.getCreativities();

		// System.out.println("TESTING");
		HashMap<String, Date> map = creativityService.timestamp();

		for (Creativity cr : c) {
			Date d = map.get(cr.getUserid());
			cr.setDate(d);
		}

		// TestResponse response = new TestResponse("TEST");

		return Response.status(200).entity(c).build();
	}

}
