package eu.stimey.platform.microservice.lab.databind.metadata;

public class FileMetadata {

	private String fileId;
	private String fileName;
	private String contentType;

	public FileMetadata() {

	}

	public FileMetadata(String fileId, String fileName, String contentType) {
		this.fileId = fileId;
		this.fileName = fileName;
		this.contentType = contentType;
	}

	public String getFileId() {
		return fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileExtension() {
		return contentType;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileExtension(String contentType) {
		this.contentType = contentType;
	}
}
