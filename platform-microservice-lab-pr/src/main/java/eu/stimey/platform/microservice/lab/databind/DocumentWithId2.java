package eu.stimey.platform.microservice.lab.databind;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import eu.stimey.platform.microservice.lab.databind.serializer.ObjectIdDeserializer;
import eu.stimey.platform.microservice.lab.databind.serializer.ObjectIdSerializer;
import org.bson.types.ObjectId;

public class DocumentWithId2 {
	protected ObjectId _id;

	@JsonSerialize(using = ObjectIdSerializer.class)
	public ObjectId get_id() {
		if (_id == null)
			_id = new ObjectId();
		return _id;
	}

	@JsonDeserialize(using = ObjectIdDeserializer.class)
	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	@Override
	public String toString() {
		return "DocumentWithId2 [_id=" + _id + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_id == null) ? 0 : _id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentWithId2 other = (DocumentWithId2) obj;
		if (_id == null) {
			return other._id == null;
		} else
			return _id.equals(other._id);
	}
}
