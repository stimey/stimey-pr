package eu.stimey.platform.microservice.lab.rest.dataTransfer.responses.keyword;

import eu.stimey.platform.library.fuzzy.search.FuzzySearch;
import eu.stimey.platform.microservice.lab.databind.keyword.Keyword;

/**
 * KeywordResponse.java
 *
 * @author Sascha M. Schumacher
 * @version 2018.04.19.0000
 */
public class KeywordResponse {

	/** the tag of the keyword */
	public String tag;

	/** the soundex of the keyword */
	public String soundex;

	/** the matches of the keyword */
	public long matches;

	/**
	 * Constructs a keyword response
	 * 
	 * @param keyword the keywords for the response
	 */
	public KeywordResponse(Keyword keyword) {
		super();
		this.tag = keyword.tag;
		this.soundex = FuzzySearch.soundex(keyword.tag);
		this.matches = keyword.matches;
	}

}
