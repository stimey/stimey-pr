package eu.stimey.platform.microservice.lab.rest.dataTransfer.responses;

import eu.stimey.platform.microservice.lab.databind.Resource;
import eu.stimey.platform.microservice.lab.rest.utils.RESTUri;

import javax.ws.rs.container.ContainerRequestContext;
import java.util.LinkedList;
import java.util.List;

/**
 * MissionResponse.java
 *
 * @author Sascha M. Schumacher
 * @version 2018.04.17.0000
 */
public class ResourceListResponse {

	/** The list for the resources */
	public List<Object> resources;

	/**
	 * @param context   @see ContainerRequestContext
	 * @param resources a list of resources for the response
	 * @param expand    this parameter changes the kind of the return
	 */
	public ResourceListResponse(ContainerRequestContext context, List<Resource> resources, String expand) {
		this(context, resources);
		if (expand.contains("any") || expand.contains("resources")) {
			for (Resource resource : resources) {
//                if (resource != null) this.resources.add(new ResourceResponse(context, resource, expand));
			}
		}
	}

	public ResourceListResponse(ContainerRequestContext context, List<Resource> resources) {
		super();
		this.resources = new LinkedList<>();
		for (Resource resource : resources) {
			if (resource != null)
				this.resources.add(RESTUri.parseSubPath(context, RESTUri.RESOURCE, resource));
		}
	}

}
