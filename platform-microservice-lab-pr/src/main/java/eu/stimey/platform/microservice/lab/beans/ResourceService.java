package eu.stimey.platform.microservice.lab.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.lab.databind.Resource;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.LinkedList;
import java.util.List;

public class ResourceService {

	private Caching<String, Resource> resourceCache;

	public ResourceService() {
		this.resourceCache = new Caching<>("ms_lab_resources", "_id", Resource.class, "resources",
				GlobalVariables.CACHE_CONFIG);
		this.resourceCache.setWorkaround_keyMapper((key) -> new ObjectId(key));

		this.resourceCache.createIndex("stage", "difficulty", "category", "keywords");
		this.resourceCache.createTextSearchIndex("name");
	}

	public void setResource(Resource resource) {
		resourceCache.set(resource.get_id().toHexString(), resource);
	}

	public void setResource(String resourceId, Resource resource) {
		resourceCache.set(resourceId, resource);
	}

	public void delResource(String resourceId) {
		this.resourceCache.del(resourceId);
	}

	public Resource getResourceById(String resourceId) {
		resourceCache.find(new Document("_id", new ObjectId(resourceId)), null, 0, 0,
				(resource) -> resource.get_id().toHexString(), true);
		return resourceCache.get(resourceId);
	}

	public List<Resource> getResourceList(Document filter, int offset, int limit) {
		return this.getResourcesByIds(this.getResourceIds(filter, offset, limit));
	}

	public List<String> getResourceIds(Document filter, int offset, int limit) {
		return resourceCache.find(filter, null, offset, limit, (resource) -> resource.get_id().toHexString(), true);
	}

	public List<Resource> getResourcesByIds(List<String> resourceIds) {
		List<Resource> resources = new LinkedList<>();
		for (String resourceId : resourceIds) {
			resources.add(this.getResourceById(resourceId));
		}
		return resources;
	}

	public Caching<String, Resource> getCache() {
		return resourceCache;
	}
}
