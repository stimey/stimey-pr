package eu.stimey.platform.microservice.lab.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.lab.databind.physicsengine.ProjectMeta;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.LinkedList;
import java.util.List;

public class ProjectService {

	private Caching<String, ProjectMeta> projectCache;

	public ProjectService() {
		this.projectCache = new Caching<>("ms_lab_projects", "projectFileId", ProjectMeta.class,
				"physics_projects_meta", GlobalVariables.CACHE_CONFIG);
	}

	public Caching<String, ProjectMeta> getProjectCache() {
		return this.projectCache;
	}

	public void createProject(ProjectMeta project) {
		System.out.println("Creating project: " + project.getProjectFileId());
		projectCache.set(project.getProjectFileId(), project);
	}

	public void delProject(String projectId) {
		System.out.println("Deleting project: " + projectId);
		boolean successFlag = this.projectCache.del(projectId);
		System.out.println("Success of deletion: " + successFlag);
	}

	public ProjectMeta getProjectById(String projectId) {
		// projectCache.find(new Document("projectFileId", new ObjectId(projectId)),
		// null, 0, 0, (project) -> project.getProjectFileId(),
		// true);
		return projectCache.get(projectId);
	}

	public List<ProjectMeta> getProjectList(Document filter, int offset, int limit) {
		return this.getProjectsByIds(this.getProjectIds(filter, offset, limit));
	}

	public List<String> getProjectIds(Document filter, int offset, int limit) {
		return projectCache.find(filter, null, offset, limit, (projectMeta) -> projectMeta.getProjectFileId(), true);
	}

	public List<ProjectMeta> getProjectsByIds(List<String> projectIds) {
		List<ProjectMeta> projects = new LinkedList<>();
		for (String projectId : projectIds) {
			projects.add(this.getProjectById(projectId));
		}
		return projects;
	}

	public void replaceProject(String projectId, ProjectMeta project) {
		projectCache.set(project.getProjectFileId(), project);
	}

}
