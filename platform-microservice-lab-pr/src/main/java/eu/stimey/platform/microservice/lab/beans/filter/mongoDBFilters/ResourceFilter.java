package eu.stimey.platform.microservice.lab.beans.filter.mongoDBFilters;

import eu.stimey.platform.library.data.access.Caching;
import org.bson.Document;

import java.util.LinkedList;
import java.util.List;

import org.bson.Document;

public class ResourceFilter extends DocumentFilter {
	/**
	 * builds a filter with the keywords, the topics, the stages and the
	 * difficulties for the searching in the database
	 *
	 * @param keywords      the filter for the keywords
	 * @param resourceTitle the filter for the resource title
	 * @param category      the filter for the category
	 * @param stage         the filter for the stage
	 * @param difficulty    the filter for the difficulty
	 * @return a document with the keywords filter, the topics filter, the stage
	 *         filter and the difficulty filter for the searching in the database
	 */
	public static Document filter(String keywords, String resourceTitle, String category, String stage,
			String difficulty) {
		List<Document> result = new LinkedList<>();
		Document buffer;
		if (!(buffer = keywords(keywords)).isEmpty())
			result.add(buffer);
		if (!(buffer = resourceTitle(resourceTitle)).isEmpty())
			result.add(buffer);
		if (!(buffer = category(category)).isEmpty())
			result.add(buffer);
		if (!(buffer = stage(stage)).isEmpty())
			result.add(buffer);
		if (!(buffer = difficulty(difficulty)).isEmpty())
			result.add(buffer);

		return and(result.toArray(new Document[result.size()]));
	}

	/**
	 * builds a filter with the keywords for the searching in the database
	 *
	 * @param keywords the filter for the keywords
	 * @return a document with the keywords filter for the searching in the database
	 */
	public static Document keywords(String keywords) {
		if (keywords == null || keywords.length() <= 0) {
			return new Document();
		}
		List<Document> result = new LinkedList<>();
		for (String part : keywords.split(" ")) {
			result.add(new Document("keywords", DocumentFilter.regex(part, DocumentFilter.CASE_INSENSITIVITY)));
		}
		if (result.size() == 0)
			return new Document();
		if (result.size() == 1)
			return result.get(0);
		return or(result.toArray(new Document[result.size()]));
	}

	/**
	 * builds a filter with the difficulties for the searching in the database
	 *
	 * @param resourceTitle the filter for the difficulty
	 * @return a document with the difficulties filter for the searching in the
	 *         database
	 */
	public static Document resourceTitle(String resourceTitle) {
		/*
		 * if (resourceTitle == null || resourceTitle.length() <= 0) { return new
		 * Document(); }
		 * 
		 * List<Document> result = new LinkedList<>(); for (String part :
		 * resourceTitle.split(" ")) { result.add(new Document("name",
		 * DocumentFilter.regex(part, DocumentFilter.CASE_INSENSITIVITY))); } if
		 * (result.size() == 0) return new Document(); if (result.size() == 1) return
		 * result.get(0);
		 */
		if (resourceTitle != null && !resourceTitle.isEmpty())
			return Caching.createTextSearchFilter(resourceTitle);// or(result.toArray(new Document[result.size()]));
		else
			return new Document();
	}

	/**
	 * builds a filter with the difficulties for the searching in the database
	 *
	 * @param difficulty the filter for the difficulty
	 * @return a document with the difficulties filter for the searching in the
	 *         database
	 */
	public static Document difficulty(String difficulty) {

		if (difficulty == null || difficulty.toLowerCase().contains("any") || difficulty.isEmpty()) {
			return new Document();
		}
		List<Document> result = new LinkedList<>();
		for (String part : difficulty.split(" *(,| ) *")) {
			result.add(new Document("difficulty", part.toUpperCase()));
		}
		if (result.size() == 0)
			return new Document();
		if (result.size() == 1)
			return result.get(0);
		return or(result.toArray(new Document[result.size()]));
	}

	/**
	 * builds a filter with the difficulties for the searching in the database
	 *
	 * @param category the filter for the category
	 * @return a document with the difficulties filter for the searching in the
	 *         database
	 */
	public static Document category(String category) {
		if (category == null || category.toLowerCase().contains("any") || category.isEmpty()) {
			return new Document();
		}
		List<Document> result = new LinkedList<>();
		for (String part : category.split(" *(,| ) *")) {
			result.add(new Document("category", part.toUpperCase()));
		}
		if (result.size() == 0)
			return new Document();
		if (result.size() == 1)
			return result.get(0);
		return or(result.toArray(new Document[result.size()]));
	}

	/**
	 * builds a filter with the difficulties for the searching in the database
	 *
	 * @param stage the filter for the stage
	 * @return a document with the difficulties filter for the searching in the
	 *         database
	 */
	public static Document stage(String stage) {
		if (stage == null || stage.toLowerCase().contains("any") || stage.isEmpty()) {
			return new Document();
		}
		List<Document> result = new LinkedList<>();
		for (String part : stage.split(" *(,| ) *")) {
			result.add(new Document("stage", part.toUpperCase()));
		}
		if (result.size() == 0)
			return new Document();
		if (result.size() == 1)
			return result.get(0);
		return or(result.toArray(new Document[result.size()]));
	}
}
