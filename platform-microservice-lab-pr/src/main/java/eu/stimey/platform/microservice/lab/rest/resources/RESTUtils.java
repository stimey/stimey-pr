package eu.stimey.platform.microservice.lab.rest.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;

import java.io.IOException;

public class RESTUtils {

	private static final ObjectMapper mapper = new ObjectMapper();

	// json Beispiel[{"op":"add", "path":"/data/-", "value": "foo" }]
	public static <T> T patch(T t, String patchAsJson, Class<T> classType) {
		T patchedObject = null;
		try {
			JsonPatch patch = mapper.readValue(patchAsJson, JsonPatch.class);
			patchedObject = mapper.treeToValue(patch.apply(mapper.valueToTree(t)), classType);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JsonPatchException e) {
			e.printStackTrace();
		}
		return patchedObject;
	}

}
