package eu.stimey.platform.microservice.lab.beans;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.bson.Document;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.lab.databind.Creativity;

public class CreativityTestService {

	private Caching<String, Creativity> creativityCache;

	public CreativityTestService() {
		this.creativityCache = new Caching<>("ms_lab", "userid", Creativity.class, "testing",
				GlobalVariables.CACHE_CONFIG);
		// this.creativityCache.clear();
	}

	public Creativity getCreativity(String id) {
		return creativityCache.get(id);
	}

	public void setCreativity(String id, Creativity creativity) {
		creativityCache.set(id, creativity);
	}

	public List<Creativity> getCreativities() {
		List<Creativity> creativities = new LinkedList<>();
		List<String> keys = new LinkedList<>();

		keys = creativityCache.find(null, null, 0, 0, (c) -> c.getUserid(), true);

		for (String key : keys) {
			System.out.println("KEY:" + key);
			creativities.add(creativityCache.get(key));
		}
		return creativities;
	}

	public HashMap<String, Date> timestamp() {
		MongoCollection<Document> coll = creativityCache.getCollection();

		//List<Document> docs = new ArrayList<>();

		HashMap<String, Date> map = new HashMap<>();

		// Performing a read operation on the collection.
		FindIterable<Document> fi = coll.find();
		MongoCursor<Document> cursor = fi.iterator();
		try {
			while (cursor.hasNext()) {
				Document d = cursor.next();

				long t = d.getObjectId("_id").getTimestamp();
				t = t * 1000;
				Date date = new Date(t);
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				System.out.println(format.format(date));

				String id = (String) d.get("userid");
				System.out.println(id);

				map.put(id, date);
			}
		} finally {
			cursor.close();
		}

		return map;
	}

}
