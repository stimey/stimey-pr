package eu.stimey.platform.microservice.lab.databind.metadata;

import java.util.Arrays;
import java.util.List;

public enum Difficulty {
	EASY("Easy"), MEDIUM("Medium"), MIDDLE("Middle"), DIFFICULT("Difficult"), CHALLENGE("Challenge");

	private String text;

	Difficulty(String text) {
		this.text = text;
	}

	public static Difficulty fromString(String text) {
		for (Difficulty d : Difficulty.values()) {
			if (d.text.equals(text)) {
				return d;
			}
		}
		throw new IllegalArgumentException("no such difficulty " + text);
	}

	public String getText() {
		return text;
	}

	public List<Difficulty> getAll() {
		return Arrays.asList(Difficulty.values());
	}
}
