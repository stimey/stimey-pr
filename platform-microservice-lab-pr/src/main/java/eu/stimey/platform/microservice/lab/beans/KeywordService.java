package eu.stimey.platform.microservice.lab.beans;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.library.utils.StimeyLogger;
import eu.stimey.platform.microservice.lab.databind.keyword.Keyword;
import org.bson.Document;

import java.util.LinkedList;
import java.util.List;

/**
 * KeywordService.java
 *
 * @author Sascha M. Schumacher
 * @version 2018.04.19.0000
 */
public class KeywordService {

	/** The cache for the keywords */
	private Caching<String, Keyword> keywordCache;

	/** Constructs the keyword service */
	public KeywordService() {
		super();
		this.keywordCache = new Caching<String, Keyword>("ms_courses_keywords", "tag", Keyword.class, "keywords",
				GlobalVariables.CACHE_CONFIG);
		this.keywordCache.clear();
	}

	/** Clears the cache of the service */
	public void clear() {
		this.keywordCache.clear();
	}

	/**
	 * Gets the keyword with the given tag
	 * 
	 * @param keywordTag the tag of the keyword
	 * @return the keyword with the given tag
	 */
	public Keyword getKeywordByTag(String keywordTag) {
		if (keywordTag == null || keywordTag.isEmpty())
			throw new IllegalArgumentException("the keyword tag cannot be null.");
		this.keywordCache.find(new Document("tag", keywordTag), null, 0, 0, (keyword) -> keyword.tag, true);
		Keyword keyword = this.keywordCache.get(keywordTag);
		// if (keyword == null) throw new IllegalArgumentException("required keyword
		// does not exist. KeywordTag: [" + keywordTag + "]");
		return keyword;
	}

	/**
	 * Gets the keywords with the given tags
	 * 
	 * @param keywordTags the tags of the keywords
	 * @return the keywords with the given tags
	 */
	public List<Keyword> getKeywordsByTags(List<String> keywordTags) {
		List<Keyword> keywords = new LinkedList<>();
		for (String keywordTag : keywordTags) {
			if (keywordTag != null)
				keywords.add(this.getKeywordByTag(keywordTag));
		}
		return keywords;
	}

	/**
	 * Finds keywords which contains the given filter
	 * 
	 * @param filter used to find the specific keywords in the cache
	 * @param offset indicates with which document from the database will started
	 * @param limit  indicates how many documents are retrieved from the database
	 * @return a list with keywords which contains the given filter
	 */
	public List<Keyword> findKeywords(Document filter, int offset, int limit) {
		List<Keyword> keywords = new LinkedList<>();
		for (String tag : findTagsOfKeywords(filter, offset, limit)) {
			keywords.add(this.keywordCache.get(tag));
		}
		return keywords;
	}

	/**
	 * Finds the tags of keywords which contains the given filter
	 * 
	 * @param filter used to find the specific tags of keywords in the cache
	 * @param offset indicates with which document from the database will started
	 * @param limit  indicates how many documents are retrieved from the database
	 * @return a list with tags of keywords which contains the given filter
	 */
	public List<String> findTagsOfKeywords(Document filter, int offset, int limit) {
		return this.keywordCache.find(filter, new Document("timestamp", 1), offset, limit, (keyword) -> keyword.tag,
				true);
	}

	/**
	 * Gets the tags of keywords which contains the given filter as string
	 * 
	 * @param filter used to find the specific tags of keywords in the cache
	 * @param offset indicates with which document from the database will started
	 * @param limit  indicates how many documents are retrieved from the database
	 * @return a string with tags of keywords which contains the given filter
	 */
	public String getTagsOfKeywords(Document filter, int offset, int limit) {
		String tags = "";
		for (String tag : findTagsOfKeywords(filter, offset, limit)) {
			tags += tag.concat(" ");
		}
		return tags;
	}

	/**
	 * Persists a keyword in the database. If the keyword already exists, matches is
	 * incremented.
	 *
	 * @param keyword the keyword to post in the database
	 */
	public void setKeyword(Keyword keyword) {
		StimeyLogger.logger().info(keyword);
		if (keyword == null)
			StimeyLogger.logger().error("the keyword cannot be null.",
					new IllegalArgumentException("the keyword cannot be null."));
		Keyword existingKeyword = getKeywordByTag(keyword.tag);
		if (existingKeyword != null) {
			existingKeyword.matches++;
			this.keywordCache.set(existingKeyword.tag, existingKeyword);
		} else {
			this.keywordCache.set(keyword.tag, keyword);
		}
	}

	/**
	 * Deletes the keyword with the given tag
	 * 
	 * @param keywordTag the tag of the keyword
	 */
	public void delKeyword(String keywordTag) {
		if (keywordTag == null || keywordTag.isEmpty())
			throw new IllegalArgumentException("the keyword id cannot be null.");
		this.keywordCache.del(keywordTag);
	}

}
