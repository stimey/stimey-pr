package eu.stimey.platform.microservice.lab.databind.keyword;

import eu.stimey.platform.library.fuzzy.search.FuzzySearch;
import eu.stimey.platform.microservice.lab.databind.Entity;

/**
 * Keyword.java
 *
 * @author Sascha M. Schumacher
 * @version 2018.03.29.0000
 */
public class Keyword implements Entity {

	/**
	 * the soundex of the keyword
	 */
	public String soundex;

	/**
	 * the tag of the keyword
	 */
	public String tag;

	/**
	 * the matches of the keyword
	 */
	public long matches;

	/**
	 * constructs a keyword
	 */
	public Keyword() {
		super();
		this.soundex = "";
		this.tag = "";
		this.matches = 0;
	}

	public Keyword(String tag) {
		super();
		this.tag = tag;
		this.soundex = FuzzySearch.soundex(tag);
		this.matches = 0;
	}

	@Override
	public String get_id() {
		return this.tag;
	}

	@Override
	public void set_id(String _id) {
		this.tag = _id;
	}

	@Override
	public String toString() {
		return "Keyword [soundex=" + soundex + ", tag=" + tag + ", matches=" + matches + "]";
	}

}
