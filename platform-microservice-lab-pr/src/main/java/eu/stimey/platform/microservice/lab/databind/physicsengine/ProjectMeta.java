package eu.stimey.platform.microservice.lab.databind.physicsengine;

public class ProjectMeta {

	private String projectFileId;
	private String userid;
	private String name;
	private String timestamp;
	private String permissionState;
	private String imageFileId;

	public ProjectMeta() {
	}

	public ProjectMeta(String projectFileId, String userid, String name, String timestamp) {
		this.projectFileId = projectFileId;
		this.userid = userid;
		this.name = name;
		this.timestamp = timestamp;
		this.permissionState = null;
		this.imageFileId = null;
	}

	public String getProjectFileId() {
		return projectFileId;
	}

	public void setProjectFileId(String projectFileId) {
		this.projectFileId = projectFileId;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getImageFileId() {
		return imageFileId;
	}

	public void setImageFileId(String imageFileId) {
		this.imageFileId = imageFileId;
	}

}
