package eu.stimey.platform.microservice.lab.rest.dataTransfer;

import java.util.List;

public class ResourceOverviewDTO {
	public String resourceId;
	public String authorName;
	public String authorId;
	public String resourceName;
	public String description;
	public List<String> keywords;
	public String stage;
	public String difficulty;
	public String category;
	public String creationDate;
	public String amountOfViews;
	public boolean isExternalResource;
	public String externalUrl;
	public String fileNameWithExtension;
	public String fileId;

	public ResourceOverviewDTO(String resourceId, String authorName, String authorId, String resourceName,
			String description, List<String> keywords, String stage, String difficulty, String category,
			String creationDate, String amountOfViews, boolean isExternalResource, String externalUrl,
			String fileNameWithExtension, String fileId) {
		this.resourceId = resourceId;
		this.authorName = authorName;
		this.authorId = authorId;
		this.resourceName = resourceName;
		this.description = description;
		this.keywords = keywords;
		this.stage = stage;
		this.difficulty = difficulty;
		this.category = category;
		this.creationDate = creationDate;
		this.amountOfViews = amountOfViews;
		this.isExternalResource = isExternalResource;
		this.externalUrl = externalUrl;
		this.fileNameWithExtension = fileNameWithExtension;
		this.fileId = fileId;
	}
}
