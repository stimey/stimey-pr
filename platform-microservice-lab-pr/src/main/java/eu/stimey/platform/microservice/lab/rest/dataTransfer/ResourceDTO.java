package eu.stimey.platform.microservice.lab.rest.dataTransfer;

public class ResourceDTO {
	public String name;
	public String category;
	public String stage;
	public String difficulty;
	public String keywords;
	public String description;

	public Boolean isExternalResource;
	public String externalUrl;

	public FileDTO uploadedFile;

	public ResourceDTO() {
	}
}
