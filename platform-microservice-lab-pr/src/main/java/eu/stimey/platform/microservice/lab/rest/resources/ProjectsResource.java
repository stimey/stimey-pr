package eu.stimey.platform.microservice.lab.rest.resources;

import eu.stimey.platform.microservice.lab.beans.ProjectService;
import eu.stimey.platform.microservice.lab.beans.filter.mongoDBFilters.PhysicsProjectsFilter;
import eu.stimey.platform.microservice.lab.databind.physicsengine.ProjectMeta;
import eu.stimey.platform.microservice.lab.databind.physicsengine.ProjectMetaRequest;
import org.bson.Document;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;

@Path("/projects")
public class ProjectsResource {

	@Inject
	ProjectService projectService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProjects(@Context ContainerRequestContext context) {
		List<ProjectMeta> projects = projectService.getProjectList(null, 0, 100);
		return Response.ok().entity(projects).build();
	}

	/**
	 * Returns one specific project with the given id.
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProjectById(@PathParam("id") String id) {
		ProjectMeta project = projectService.getProjectById(id);
		return Response.ok().entity(project).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createProject(@Context ContainerRequestContext context, ProjectMetaRequest projectMetaRequest) {
		System.out.println("Saving new project!");
		String userid = (String) context.getProperty("userid");
		ProjectMeta projectMeta = new ProjectMeta(projectMetaRequest.projectFileId, userid, projectMetaRequest.name,
				projectMetaRequest.timestamp);
		List<String> useridList = new LinkedList<>();
		useridList.add(userid);

		System.out.println(projectMeta.getName());

		projectService.createProject(projectMeta);
		/* List<ProjectMeta> projects = projectService.getProjectList(null, 0, 100); */
		return getProjectsResponse(useridList);
	}

	private Response getProjectsResponse(List<String> useridList) {
		Response response;

		List<ProjectMeta> projectsMetaList = projectService.getProjectList(PhysicsProjectsFilter.filter(useridList), 0,
				0);

		response = Response.status(Response.Status.OK).entity(projectsMetaList).build();

		return response;
	}

	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteProject(@PathParam("id") String id) {
		System.out.println("dddddddddddddddd " + projectService.getProjectCache().get(id));
		projectService.delProject(id);
		System.out.println("dddddddddddddddd2 " + projectService.getProjectCache().get(id));
		List<ProjectMeta> projects = projectService.getProjectList(null, 0, 100);
		return Response.ok().entity(projects).build();
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	// Id: Id of old project meta - project: new project!
	public Response replaceProject(@PathParam("id") String id, ProjectMeta project) {
		projectService.replaceProject(id, project);
		List<ProjectMeta> projects = projectService.getProjectList(null, 0, 100);
		return Response.ok().entity(projects).build();
	}

	@GET
	@Path("/allofuser")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProjectsOfUser(@Context ContainerRequestContext context) {
		String authorId = (String) context.getProperty("userid");
		Document userfilter = new Document("userid", authorId);
		List<ProjectMeta> projects = projectService.getProjectList(userfilter, 0, 100);
		return Response.ok().entity(projects).build();
	}
}
