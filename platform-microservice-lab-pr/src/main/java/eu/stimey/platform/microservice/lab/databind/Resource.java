package eu.stimey.platform.microservice.lab.databind;

import eu.stimey.platform.microservice.lab.databind.metadata.Category;
import eu.stimey.platform.microservice.lab.databind.metadata.Difficulty;
import eu.stimey.platform.microservice.lab.databind.metadata.Stage;
import eu.stimey.platform.microservice.lab.databind.metadata.FileMetadata;

import java.util.List;

import java.util.Date;

public class Resource extends DocumentWithId2 {
	private String authorId;
	private String name;
	private String description;
	private Stage stage;
	private Difficulty difficulty;
	private List<String> keywords;
	private Category category;
	private String creationDate;
	private boolean isExternalResource;
	private String externalUrl;
	private int amountOfViews;
	private int amountOfDownloads;
	private FileMetadata uploadedFile;

	public Resource() {
	}

	public Resource(String authorId, String name, String description, Stage stage, Difficulty difficulty,
			List<String> keywords, Category category, String creationDate, String externalUrl, int amountOfViews,
			FileMetadata uploadedFile) {
		this.authorId = authorId;
		this.name = name;
		this.description = description;
		this.stage = stage;
		this.difficulty = difficulty;
		this.keywords = keywords;
		this.category = category;
		this.creationDate = creationDate;
		this.externalUrl = externalUrl;
		this.uploadedFile = uploadedFile;
		this.amountOfViews = 0;
		this.amountOfDownloads = 0;
	}

	public void setAuthorId(String authorId) {
		this.authorId = authorId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public void setExternalUrl(String externalUrl) {
		this.externalUrl = externalUrl;
	}

	public void incrementAmountOfViews() {
		this.amountOfViews++;
	}

	public void setUploadedFile(FileMetadata uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public void setExternalResource(boolean externalResource) {
		isExternalResource = externalResource;
	}

	public boolean isExternalResource() {
		return isExternalResource;
	}

	public String getAuthorId() {
		return authorId;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Stage getStage() {
		return stage;
	}

	public Difficulty getDifficulty() {
		return difficulty;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public Category getCategory() {
		return category;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public String getExternalUrl() {
		return externalUrl;
	}

	public int getAmountOfViews() {
		return amountOfViews;
	}

	public FileMetadata getUploadedFile() {
		return uploadedFile;
	}

}
