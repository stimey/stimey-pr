package eu.stimey.platform.microservice.lab.databind.metadata;

import java.util.Arrays;
import java.util.List;

public enum Category {
	LIBRARY("Library"), MEDIA("Media"), GAMES("Games"), TOOLS("Tools");

	private String text;

	Category(String text) {
		this.text = text;
	}

	public static Category fromString(String text) {
		for (Category d : Category.values()) {
			if (d.text.equals(text)) {
				return d;
			}
		}
		throw new IllegalArgumentException("no such category " + text);
	}

	public String getText() {
		return text;
	}

	public List<Category> getAll() {
		return Arrays.asList(Category.values());
	}
}
