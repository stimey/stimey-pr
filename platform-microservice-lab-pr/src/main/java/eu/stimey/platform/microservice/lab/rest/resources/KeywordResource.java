package eu.stimey.platform.microservice.lab.rest.resources;

import eu.stimey.platform.microservice.lab.beans.KeywordService;
import eu.stimey.platform.microservice.lab.beans.filter.mongoDBFilters.KeywordFilter;
import eu.stimey.platform.microservice.lab.databind.keyword.Keyword;
import eu.stimey.platform.microservice.lab.rest.dataTransfer.responses.keyword.KeywordListResponse;
import eu.stimey.platform.microservice.lab.rest.dataTransfer.responses.keyword.KeywordResponse;
import eu.stimey.platform.microservice.lab.rest.utils.RESTUri;
import org.apache.commons.lang3.tuple.ImmutablePair;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.LinkedList;
import java.util.List;

/**
 * KeywordResource.java
 *
 * @author Sascha M. Schumacher (Sash)
 * @version 2018.04.26.0000
 */
@Path("/keywords")
public class KeywordResource {

	/** Default value of the offset */
	public static final String DEFAULT_OFFSET = "0";

	/** Default value of the limit */
	public static final String DEFAULT_LIMIT = "32";

	/** @see {@link KeywordService} */
	@Context
	KeywordService keywordService;

	/**
	 * Requests a list of keywords from the database. the query parameters influence
	 * this list and can be combined.
	 *
	 * @param context @see {@link ContainerRequestContext}
	 * @param offset  indicates with which document from the database will started
	 *                (default: 0)
	 * @param limit   indicates how many documents are retrieved from the database
	 *                (default: 32)
	 * @param query   search criterion to which the documents of the database must
	 *                correspond (default: empty)
	 * @param expand  this parameter changes the type of the return (default: empty,
	 *                values: objects | autocomplete | any | keywords)
	 * @return a response with a list of keywords from the database
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getKeywordList(@Context ContainerRequestContext context,
			@QueryParam("x") @DefaultValue("") String expand,
			@QueryParam("o") @DefaultValue(DEFAULT_OFFSET) Integer offset,
			@QueryParam("l") @DefaultValue(DEFAULT_LIMIT) Integer limit,
			@QueryParam("q") @DefaultValue("") String query) {
		try {
			List<Keyword> keywords = keywordService.findKeywords(KeywordFilter.soundex(query), offset, limit);
			List<String> autocomplete = null;
			KeywordListResponse response = null;
			if (expand.contains("autocomplete")) {
				autocomplete = new LinkedList<>();
				for (Keyword keyword : keywordService.findKeywords(KeywordFilter.soundex(query), offset, limit)) {
					autocomplete.add(keyword.tag);
				}
			} else if (!expand.contains("objects")) {
				response = new KeywordListResponse(context, keywords);
				if (expand.contains("any") || expand.contains("keywords")) {
					response.keywords = new LinkedList<>();
					for (Keyword keyword : keywords) {
						if (keyword != null)
							response.keywords.add(this.getKeywordById(context, keyword.tag, expand).getEntity());
					}
				}
			}
			return Response.status(Status.OK).header("URI", RESTUri.parseCompletePath(context))
					.entity((response != null) ? response : (autocomplete != null) ? autocomplete : keywords).build();
		} catch (Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).header("URI", RESTUri.parseCompletePath(context))
					.entity(new ImmutablePair<String, String>("Error", ex.getMessage())).build();
		}
	}

	/**
	 * Sets a keyword to the keywords and returns the URI in the header under
	 * URI-Location.
	 *
	 * @param context @see {@link ContainerRequestContext}
	 * @param keyword the keyword to add in database
	 * @return a response with the create message and the URI in the header under
	 *         URI-Location.
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postKeyword(@Context ContainerRequestContext context, Keyword keyword) {
		keywordService.setKeyword(keyword);
		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context))
				.header("Location", RESTUri.parseSubPath(context, keyword.tag))
				.entity(new ImmutablePair<String, String>("Message",
						"the keyword with tag=" + keyword.tag + " is created."))
				.build();
	}

	/**
	 * Requests the specified keyword from the database. the query parameter
	 * influences the type of return.
	 *
	 * @param context    @see {@link ContainerRequestContext}
	 * @param keywordTag the tag of the keyword to return
	 * @param expand     this parameter changes the type of the return (default:
	 *                   empty, value: object)
	 * @return a response with the keyword which has the id.
	 */
	@GET
	@Path("/{keywordTag}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getKeywordById(@Context ContainerRequestContext context, @PathParam("keywordTag") String keywordTag,
			@QueryParam("x") @DefaultValue("") String expand) {
		try {
			Keyword keyword = keywordService.getKeywordByTag(keywordTag);
			KeywordResponse response = null;
			if (!expand.contains("object"))
				response = new KeywordResponse(keyword);
			return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context))
					.entity((response != null) ? response : keyword).build();
		} catch (Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).header("URI", RESTUri.parseAbsolutePath(context))
					.entity(new ImmutablePair<String, String>("Error", ex.getMessage())).build();
		}
	}

	/**
	 * Part of the specified keyword is changed. (don't tested)
	 *
	 * @param context     @see {@link ContainerRequestContext}
	 * @param keywordTag  the tag of the keyword
	 * @param patchAsJson the operation for the patch
	 * @return a response with the changed message
	 */
	@PATCH
	@Path("/{keywordTag}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response patchKeywordById(@Context ContainerRequestContext context,
			@PathParam("keywordTag") String keywordTag, String patchAsJson) {
		try {
			Keyword keyword = keywordService.getKeywordByTag(keywordTag);
			keyword = RESTUtils.patch(keyword, patchAsJson, Keyword.class);
			return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(
					new ImmutablePair<String, String>("Message", "the keyword with tag=" + keywordTag + " is updated."))
					.build();
		} catch (Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).header("URI", RESTUri.parseAbsolutePath(context))
					.entity(new ImmutablePair<String, String>("Error", ex.getMessage())).build();
		}
	}

	/**
	 * Deletes the specified keyword.
	 *
	 * @param context    @see {@link ContainerRequestContext}
	 * @param keywordTag the tag of the keyword
	 * @return a response with the delete message
	 */
	@DELETE
	@Path("/{keywordTag}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteKeywordById(@Context ContainerRequestContext context,
			@PathParam("keywordTag") String keywordTag) {
		try {
			keywordService.delKeyword(keywordTag);
			return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(
					new ImmutablePair<String, String>("Message", "the keyword with tag=" + keywordTag + " is deleted."))
					.build();
		} catch (Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).header("URI", RESTUri.parseAbsolutePath(context))
					.entity(new ImmutablePair<String, String>("Error", ex.getMessage())).build();
		}
	}

}
