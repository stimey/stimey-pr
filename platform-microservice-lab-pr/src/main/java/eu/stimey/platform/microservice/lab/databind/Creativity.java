package eu.stimey.platform.microservice.lab.databind;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import eu.stimey.platform.microservice.lab.rest.dataTransfer.QuestionData;

//@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class Creativity {

	private String userid;
	private String lang;
	private String age;
	private String gender;
	private List<QuestionData> first;
	private List<QuestionData> second;
	private Date date;

	public Creativity() {
		this(null, null, null, null, null, null);
	}

	public Creativity(String userid, String lang, String age, String gender, List<QuestionData> first,
			List<QuestionData> second) {

		this.userid = userid;
		this.lang = lang;
		this.age = age;
		this.gender = gender;
		this.first = first;
		this.second = second;

	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public List<QuestionData> getFirst() {
		return first;
	}

	public void setFirst(List<QuestionData> first) {
		this.first = first;
	}

	public List<QuestionData> getSecond() {
		return second;
	}

	public void setSecond(List<QuestionData> second) {
		this.second = second;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Creativity [userid=" + userid + ", lang=" + lang + ", age=" + age + ", gender=" + gender + ", first="
				+ first + ", second=" + second + "]";
	}

}
