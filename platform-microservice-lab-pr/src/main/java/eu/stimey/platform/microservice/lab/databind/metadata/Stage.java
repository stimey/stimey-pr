package eu.stimey.platform.microservice.lab.databind.metadata;

import java.util.Arrays;
import java.util.List;

public enum Stage {
	BASIC("Basic"), PRIMARY("Primary"), Intermediate("Intermediate"), ADVANCED("Advanced");

	private String text;

	Stage(String text) {
		this.text = text;
	}

	public static Stage fromString(String text) {
		for (Stage d : Stage.values()) {
			if (d.text.equals(text)) {
				return d;
			}
		}
		throw new IllegalArgumentException("no such stage " + text);
	}

	public String getText() {
		return text;
	}

	public List<Stage> getAll() {
		return Arrays.asList(Stage.values());
	}
}
