package eu.stimey.platform.microservice.achievement.databind;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import eu.stimey.platform.library.utils.databind.DateDeserializer;
import eu.stimey.platform.library.utils.databind.DateSerializer;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class GamificationDailyGoal {

	@JsonSerialize(using = DateSerializer.class)
	@JsonDeserialize(using = DateDeserializer.class)
	private Date date;
	private int goal_kp;
	private int goal_sp;
	private int goal_cp;
	private int kp;
	private int sp;
	private int cp;

	public GamificationDailyGoal() {
		this(0, 0, 0);
	}

	public GamificationDailyGoal(int goal_kp, int goal_sp, int goal_cp) {
		this.date = new Date();
		this.goal_kp = goal_kp;
		this.goal_sp = goal_sp;
		this.goal_cp = goal_cp;
		this.kp = 0;
		this.sp = 0;
		this.cp = 0;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getGoal_kp() {
		return goal_kp;
	}

	public void setGoal_kp(int goal_kp) {
		this.goal_kp = goal_kp;
	}

	public int getGoal_sp() {
		return goal_sp;
	}

	public void setGoal_sp(int goal_sp) {
		this.goal_sp = goal_sp;
	}

	public int getGoal_cp() {
		return goal_cp;
	}

	public void setGoal_cp(int goal_cp) {
		this.goal_cp = goal_cp;
	}

	public int getKp() {
		return kp;
	}

	public void setKp(int kp) {
		this.kp = kp;
	}

	public int getSp() {
		return sp;
	}

	public void setSp(int sp) {
		this.sp = sp;
	}

	public int getCp() {
		return cp;
	}

	public void setCp(int cp) {
		this.cp = cp;
	}

	@Override
	public String toString() {
		return "GamificationDailyGoal [date=" + date + ", goal_kp=" + goal_kp + ", goal_sp=" + goal_sp + ", goal_cp="
				+ goal_cp + ", kp=" + kp + ", sp=" + sp + ", cp=" + cp + "]";
	}

}
