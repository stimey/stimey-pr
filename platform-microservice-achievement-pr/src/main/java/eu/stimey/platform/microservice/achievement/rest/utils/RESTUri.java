package eu.stimey.platform.microservice.achievement.rest.utils;

import javax.ws.rs.container.ContainerRequestContext;

//import eu.stimey.platform.microservice.achievement.databind.course.serializer.DocumentWithId2;

/**
 * RestURI.java
 *
 * @author Sascha M. Schumacher
 * @version 2018.04.10.0000
 */
public class RESTUri {

	/** The local path to the microservice for the gateway storage */
	public static final String LOCAL_GATEWAY_STORAGE = "http://localhost:8080/platform-microservice-api-gateway-storage/api-proxy/achievements/api";

	/** The local path to the microservice for achievements */
	public static final String LOCAL_ACHIEVEMENTS = "http://localhost:8080/platform-microservice-achievement/api";

	/** The path to the resource for users */
	public static final String USERS = "/users";
	//
	// /** The path to the resource for teachers */
	// public static final String TEACHERS = "/teachers";
	//
	// /** The path to the resource for students */
	// public static final String STUDENTS = "/students";
	//
	// /** The path to the resource for keywords */
	// public static final String KEYWORDS = "/keywords";
	//
	// /** The path to the resource for planets */
	// public static final String PLANETS = "/planets";
	//
	// /** The path to the resource for missions */
	// public static final String MISSIONS = "/missions";
	//
	// /** The path to the resource for challenges */
	// public static final String CHALLENGES = "/challenges";
	//
	// /** The path to the resource for games */
	// public static final String GAMES = "/games";
	//
	// /** The path to the resource for topics */
	// public static final String TOPICS = "/topics";
	//
	// /** The path to the resource for levels */
	// public static final String LEVELS = "/levels";
	//
	// /** The path to the resource for learning objects */
	// public static final String LEARNING_OBJECTS = "/learningObjects";

	/**
	 * Parses the base URI from the context
	 * 
	 * @param context
	 * @see ContainerRequestContext
	 * @return the parsed base path of the context
	 */
	public static String parseBasePath(ContainerRequestContext context) {
		String baseURI = context.getUriInfo().getBaseUri().toString().replace(LOCAL_ACHIEVEMENTS,
				LOCAL_GATEWAY_STORAGE);
		return baseURI.substring(0, baseURI.length() - 1);
	}

	/**
	 * Parses the absolute path from the context
	 * 
	 * @param context
	 * @see ContainerRequestContext
	 * @return the parsed absolute path of the context
	 */
	public static String parseAbsolutePath(ContainerRequestContext context) {
		return context.getUriInfo().getAbsolutePath().toString().replace(LOCAL_ACHIEVEMENTS, LOCAL_GATEWAY_STORAGE);
	}

	/**
	 * Parses the absolute path and the query from the context
	 * 
	 * @param context
	 * @see ContainerRequestContext
	 * @return the parsed complete path of the context
	 */
	public static String parseCompletePath(ContainerRequestContext context) {
		return parseAbsolutePath(context).concat(parseQuery(context));
	}

	/**
	 * Parses the base URI from the context and adds the given path
	 * 
	 * @param context
	 * @see ContainerRequestContext
	 * @param path the path to the resource
	 * @return the parsed base path of the context and the path
	 */
	public static String parsePath(ContainerRequestContext context, String path) {
		return parseBasePath(context).concat(path);
	}

	/**
	 * Parses the absolute path and adds the id of the given object as sub path
	 * 
	 * @param context
	 * @see ContainerRequestContext
	 * @param object a child of @see DocumentWithId2 for the sub path
	 * @return the parsed sub path of the context and the id
	 */
	// public static String parseSubPath(ContainerRequestContext context,
	// DocumentWithId2 object) {
	// return parseAbsolutePath(context).concat(parseId(object));
	// }

	/**
	 * Parses the path and adds the id of the given object as sub path
	 * 
	 * @param context
	 * @see ContainerRequestContext
	 * @param path   the path of the resource
	 * @param object a child of @see DocumentWithId2 for the sub path
	 * @return the parsed sub path of the context and the id
	 */
	// public static String parseSubPath(ContainerRequestContext context, String
	// path, DocumentWithId2 object) {
	// return parsePath(context, path).concat(parseId(object));
	// }

	/**
	 * Parses the absolute path and adds the key as sub path
	 * 
	 * @param context
	 * @see ContainerRequestContext
	 * @param key the key for the sub path
	 * @return the parsed sub path of the context and the key
	 */
	public static String parseSubPath(ContainerRequestContext context, String key) {
		return parseAbsolutePath(context).concat(parseKey(key));
	}

	/**
	 * Parses the path and adds the key as sub path
	 * 
	 * @param context
	 * @see ContainerRequestContext
	 * @param path the path of the resource
	 * @param key  the key for the sub path
	 * @return the parsed sub path of the context and the key
	 */
	public static String parseSubPath(ContainerRequestContext context, String path, String key) {
		return parsePath(context, path).concat(parseKey(key));
	}

	/**
	 * Parses the query of the context and the question mark on the beginning
	 * 
	 * @param context
	 * @see ContainerRequestContext
	 * @return the query of the context with the question mark
	 */
	public static String parseQuery(ContainerRequestContext context) {
		String result = context.getUriInfo().getRequestUri().getQuery();
		return (result == null) ? "" : "?".concat(result);
	}

	/**
	 * Parse the id of the object and the forward slash on the beginning
	 * 
	 * @param object a child of @see DocumentWithId2
	 * @return the id of the object with the forward slash
	 */
	// public static String parseId(DocumentWithId2 object) {
	// return (object == null) ? "" : "/".concat(object.get_id().toHexString());
	// }

	/**
	 * Parse the id of the object and the forward slash on the beginning
	 * 
	 * @param key the id for the id path
	 * @return the id path
	 */
	public static String parseKey(String key) {
		return (key == null || key.isEmpty()) ? "" : "/".concat(key);
	}

}