package eu.stimey.platform.microservice.achievement.databind;

public enum NotificationType {
	success, error, info, warning, question, fantasy_info

}
