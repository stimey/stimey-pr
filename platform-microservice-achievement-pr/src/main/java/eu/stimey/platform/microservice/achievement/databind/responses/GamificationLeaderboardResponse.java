package eu.stimey.platform.microservice.achievement.databind.responses;

import javax.ws.rs.container.ContainerRequestContext;

import eu.stimey.platform.microservice.achievement.databind.GamificationLeaderboard;
import eu.stimey.platform.microservice.achievement.rest.utils.RESTUri;

public class GamificationLeaderboardResponse {
	public String userid;
	public String username;
	public String userlink;
	public int position;
	public int level;
	public int kp;
	public int sp;
	public int cp;
	public String country;

	public GamificationLeaderboardResponse(ContainerRequestContext context, GamificationLeaderboard user) {
		this.userid = user.getUserid();
		this.username = user.getName();
		this.userlink = RESTUri.parseSubPath(context, RESTUri.USERS, user.getUserid());
		this.level = user.getLevel();
		this.kp = user.getKnowledge();
		this.sp = user.getSocial();
		this.cp = user.getCreativity();
		this.country = user.getCountry();
	}

}
