package eu.stimey.platform.microservice.achievement.databind;

public interface GamificationEvents {

	// Time Limits
	final static int NO_LIMIT = 0;
	final static int ONCE = 1;
	final static int TIMES_A_DAY = 2;
	final static int ONCE_AND_TIMES_A_DAY = 3;
	// Not a time limit, but for Phantasy
	final static int FANTASY = 4;

	// Events
	// Sign up and login
	final static int REGISTER = 1;
	final static int DAILY_LOGIN = 2;

	// Courses
	final static int ENROL_COURSE = 3;
	final static int COMPLETE_LEARNING_OBJECT_COURSE = 4;
	final static int COMPLETE_25_PERCENT_COURSE = 5;
	final static int COMPLETE_50_PERCENT_COURSE = 6;
	final static int COMPLETE_75_PERCENT_COURSE = 7;
	final static int COMPLETE_100_PERCENT_COURSE = 8;
	final static int POST_QUESTION_COURSE_QA = 9;
	final static int COMMENT_POST_QA = 10;
	final static int COMMENT_ABOUT_COURSE_OR_TEACHER = 11;
	final static int COURSE_REVIEW = 12;

	// Community
	final static int CREATE_COMMUNITY = 13;
	final static int JOIN_COMMUNITY = 14;
	final static int CREATE_COMMUNITY_ANNOUNCEMENT = 15;
	final static int COMMUNITY_PUBLISH_UPDATE_ONLY_TEXT = 16;
	final static int COMMUNITY_PUBLISH_UPDATE_IMAGE_OR_VIDEO = 17;
	final static int COMMUNITY_PUBLISH_UPDATE_ONLY_URL = 18;
	final static int COMMUNITY_COMMENT_UPDATE = 19;
	final static int COMMUNITY_LIKE_SHARE_UPDATE = 20;

	// User Profile
	final static int USER_PROFILE_PUBLISH_UPDATE_ONLY_TEXT = 21;
	final static int USER_PROFILE_PUBLISH_UPDATE_IMAGE_OR_VIDEO = 22;
	final static int USER_PROFILE_PUBLISH_UPDATE_ONLY_URL = 23;
	final static int USER_PROFILE_COMMENT_UPDATE = 24;
	final static int USER_PROFILE_LIKE_SHARE_UPDATE = 25;
	final static int ADD_PROFILE_PHOTO_FIRST_TIME = 26;
	final static int ADD_COVER_PHOTO_FIRST_TIME = 27;

	// Discussions
	final static int POST_DISCUSSION = 28;
	final static int DISCUSSION_COMMENT_POST = 29;

	// Lab
	final static int COMPLETE_LEVEL_1_ACCURACY_OVER_60 = 30;
	final static int COMPLETE_LEVEL_2_ACCURACY_OVER_60 = 31;
	final static int COMPLETE_LEVEL_3_ACCURACY_OVER_60 = 32;
	final static int COMPLETE_LEVEL_4_ACCURACY_OVER_60 = 33;
	final static int COMPLETE_LEVEL_5_ACCURACY_OVER_60 = 34;
	final static int COMPLETE_LEVEL_6_ACCURACY_OVER_60 = 35;
	final static int LISTEN_STIMEY_RADIO = 36;

	// Additional Points
	final static int SET_FIRST_DAILY_GOAL_VALUES_OVER_20 = 37;
	final static int COMPLETE_DAILY_GOAL = 38;

	// Lab
	final static int START_MENTAL_CALCULATOR = 39;
	final static int START_PHYSICS_ENGINE = 40;
	final static int START_CHEMISTRY_ENGINE = 41;
	final static int START_SERIOUS_GAMES = 42;

	// Mental Calculator
	final static int COMPLETE_TRAINING_LEVEL_1 = 43;
	final static int COMPLETE_TRAINING_LEVEL_2 = 44;
	final static int COMPLETE_TRAINING_LEVEL_3 = 45;
	final static int COMPLETE_TRAINING_LEVEL_4 = 46;
	final static int COMPLETE_TRAINING_LEVEL_5 = 47;
	final static int COMPLETE_TRAINING_LEVEL_6 = 48;

	//
	final static int COMPLETE_FANTASY = 49;

}
