package eu.stimey.platform.microservice.achievement.databind.responses;

import java.util.Map;

import eu.stimey.platform.microservice.achievement.databind.GamificationDailyGoalProgress;

public class DailyGoalProgressResponse {

	public String[] goals = new String[7];
	public String[] reached = new String[7];

	public DailyGoalProgressResponse(Map<Integer, GamificationDailyGoalProgress> dgProgress) {

		for (int i = 1; i <= 7; i++) {
			GamificationDailyGoalProgress dgp = dgProgress.get(i);
			if (dgp != null) {
				goals[i - 1] = String.valueOf(dgp.getGoal());
				reached[i - 1] = String.valueOf(dgp.getReached());
			} else {
				goals[i - 1] = null;
				reached[i - 1] = null;
			}

		}

	}

}
