package eu.stimey.platform.microservice.achievement.databind.responses;

import eu.stimey.platform.microservice.achievement.databind.GamificationEvent;
import eu.stimey.platform.microservice.achievement.databind.NotificationType;

public class GamificationNotificationResponse {

	public String title;
	public String message;
	public String type;

	public GamificationNotificationResponse(GamificationEvent event, NotificationType type) {

		if (type == NotificationType.success) {
			this.title = event.getSuccessNotificationTitle();
			this.message = String.format(event.getSuccessNotification(), event.getKnowledgepoints().getValue(),
					event.getSocialpoints().getValue(), event.getCreativitypoints().getValue());
			this.type = "success";
		}
		if (type == NotificationType.error) {
			this.title = event.getErrorNotificationTitle();
			this.message = event.getErrorNotification();
			this.type = "error";
		}
		if (type == NotificationType.info) {
			this.title = event.getInfoNotificationTitle();
			this.message = event.getInfoNotification();
			this.type = "info";
		}
	}

	public GamificationNotificationResponse(String title, String message, NotificationType type) {
		this.title = title;
		this.message = message;
		this.type = type.toString();
	}

}
