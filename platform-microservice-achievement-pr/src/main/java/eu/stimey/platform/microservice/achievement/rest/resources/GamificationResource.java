package eu.stimey.platform.microservice.achievement.rest.resources;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.bson.types.ObjectId;

import eu.stimey.platform.microservice.achievement.databind.GamificationNotificationResponse;
import eu.stimey.platform.microservice.achievement.databind.UserGamification;
import eu.stimey.platform.microservice.achievement.beans.GamificationService;
import eu.stimey.platform.microservice.achievement.rest.utils.RESTUri;

@Path("/gamification")
public class GamificationResource {

	@Inject
	GamificationService gamificationservice;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserInfo(@Context ContainerRequestContext context, @QueryParam("userid") String userid,
			@QueryParam("eventid") Integer eventid, @QueryParam("courseid") String courseid,
			@QueryParam("fantasy_points") Integer points, @QueryParam("response") String respType) {

		// System.out.println("EVID:" + eventid.toString());
		// System.out.println("TEST");

		String messageID = new ObjectId().toHexString();
		GamificationNotificationResponse response;

		if (points == null) {
			response = gamificationservice.calculatePoints(userid, eventid, messageID, courseid);
		} else {
			response = gamificationservice.calculatePoints(userid, eventid, messageID, courseid, points, respType);
		}

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();

	}

	@GET
	@Path("/points")
	@Produces(MediaType.APPLICATION_JSON)
	public Response gamification(@Context ContainerRequestContext context, 
			@QueryParam("knowledge") Integer knowledgePoints, @QueryParam("social") Integer socialPoints, @QueryParam("creativity") Integer creativityPoints) {
		
		String userid = (String) context.getProperty("userid");

		UserGamification usergamification = gamificationservice.getUserGamification(userid);
		if (usergamification!=null) {
			gamificationservice.setUserGamification(userid, new UserGamification(
					usergamification.getUserid(),
					usergamification.getLevel(),
					usergamification.getKnowledgePoints()+knowledgePoints,
					usergamification.getSocialPoints()+socialPoints,
					usergamification.getCreativityPoints()+creativityPoints,
					usergamification.getEnergyOrbs()
					));
			
			return Response.status(Status.OK).build();
		}
		else
			return Response.status(404).build();
	}
}
