package eu.stimey.platform.microservice.achievement.databind;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class GamificationLevel {

	private String level;
	private int kp_limit;
	private int sp_limit;
	private int cp_limit;
	private String feedback_same;
	private String feedback_kp_high;
	private String feedback_sp_high;
	private String feedback_cp_high;

	// image

	public GamificationLevel() {
		this(null, 0, 0, 0, null, null, null, null);
	}

	public GamificationLevel(String level, int kp_limit, int sp_limit, int cp_limit, String feedback_same,
			String feedback_kp_high, String feedback_sp_high, String feedback_cp_high) {

		this.level = level;
		this.kp_limit = kp_limit;
		this.sp_limit = sp_limit;
		this.cp_limit = cp_limit;
		this.feedback_same = feedback_same;
		this.feedback_kp_high = feedback_kp_high;
		this.feedback_sp_high = feedback_sp_high;
		this.feedback_cp_high = feedback_cp_high;

	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public int getKp_limit() {
		return kp_limit;
	}

	public void setKp_limit(int kp_limit) {
		this.kp_limit = kp_limit;
	}

	public int getSp_limit() {
		return sp_limit;
	}

	public void setSp_limit(int sp_limit) {
		this.sp_limit = sp_limit;
	}

	public int getCp_limit() {
		return cp_limit;
	}

	public void setCp_limit(int cp_limit) {
		this.cp_limit = cp_limit;
	}

	public String getFeedback_same() {
		return feedback_same;
	}

	public void setFeedback_same(String feedback_same) {
		this.feedback_same = feedback_same;
	}

	public String getFeedback_kp_high() {
		return feedback_kp_high;
	}

	public void setFeedback_kp_high(String feedback_kp_high) {
		this.feedback_kp_high = feedback_kp_high;
	}

	public String getFeedback_sp_high() {
		return feedback_sp_high;
	}

	public void setFeedback_sp_high(String feedback_sp_high) {
		this.feedback_sp_high = feedback_sp_high;
	}

	public String getFeedback_cp_high() {
		return feedback_cp_high;
	}

	public void setFeedback_cp_high(String feedback_cp_high) {
		this.feedback_cp_high = feedback_cp_high;
	}

	@Override
	public String toString() {
		return "GamificationLevel [level=" + level + ", kp_limit=" + kp_limit + ", sp_limit=" + sp_limit + ", cp_limit="
				+ cp_limit + ", feedback_same=" + feedback_same + ", feedback_kp_high=" + feedback_kp_high
				+ ", feedback_sp_high=" + feedback_sp_high + ", feedback_cp_high=" + feedback_cp_high + "]";
	}

}
