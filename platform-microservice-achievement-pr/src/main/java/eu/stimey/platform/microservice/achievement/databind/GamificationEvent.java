package eu.stimey.platform.microservice.achievement.databind;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class GamificationEvent {

	private String event_id;
	private String event_name;
	private String successNotificationTitle;
	private String successNotification;
	private String errorNotificationTitle;
	private String errorNotification;
	private String infoNotificationTitle;
	private String infoNotification;
	private GamificationPoint knowledgepoints;
	private GamificationPoint socialpoints;
	private GamificationPoint creativitypoints;
	private int timeLimit;
	private int limit;

	public GamificationEvent() {
		this(null, null, null, null, null, null, null, null, null, null, null, 0);
	}

	public GamificationEvent(String event_id, String event_name, String successTitle, String success, String errorTitle,
			String error, String infoTitle, String info, GamificationPoint knowledgepoints,
			GamificationPoint socialpoints, GamificationPoint creativitypoints, int timeLimit) {
		this.event_id = event_id;
		this.event_name = event_name;
		this.successNotificationTitle = successTitle;
		this.successNotification = success;
		this.errorNotificationTitle = errorTitle;
		this.errorNotification = error;
		this.infoNotificationTitle = infoTitle;
		this.infoNotification = info;
		this.knowledgepoints = knowledgepoints;
		this.socialpoints = socialpoints;
		this.creativitypoints = creativitypoints;
		this.timeLimit = timeLimit;
		this.limit = 0;
	}

	public GamificationEvent(String event_id, String event_name, String successTitle, String success, String errorTitle,
			String error, String infoTitle, String info, GamificationPoint knowledgepoints,
			GamificationPoint socialpoints, GamificationPoint creativitypoints, int timeLimit, int limit) {
		this.event_id = event_id;
		this.event_name = event_name;
		this.successNotificationTitle = successTitle;
		this.successNotification = success;
		this.errorNotificationTitle = errorTitle;
		this.errorNotification = error;
		this.infoNotificationTitle = infoTitle;
		this.infoNotification = info;
		this.knowledgepoints = knowledgepoints;
		this.socialpoints = socialpoints;
		this.creativitypoints = creativitypoints;
		this.timeLimit = timeLimit;
		this.limit = limit;
	}

	public String getEvent_id() {
		return event_id;
	}

	public void setEvent_id(String event_id) {
		this.event_id = event_id;
	}

	public String getEvent_name() {
		return event_name;
	}

	public void setEvent_name(String event_name) {
		this.event_name = event_name;
	}

	public String getSuccessNotificationTitle() {
		return successNotificationTitle;
	}

	public void setSuccessNotificationTitle(String successNotificationTitle) {
		this.successNotificationTitle = successNotificationTitle;
	}

	public String getSuccessNotification() {
		return successNotification;
	}

	public void setSuccessNotification(String successNotification) {
		this.successNotification = successNotification;
	}

	public String getErrorNotificationTitle() {
		return errorNotificationTitle;
	}

	public void setErrorNotificationTitle(String errorNotificationTitle) {
		this.errorNotificationTitle = errorNotificationTitle;
	}

	public String getErrorNotification() {
		return errorNotification;
	}

	public void setErrorNotification(String errorNotification) {
		this.errorNotification = errorNotification;
	}

	public String getInfoNotificationTitle() {
		return infoNotificationTitle;
	}

	public void setInfoNotificationTitle(String infoNotificationTitle) {
		this.infoNotificationTitle = infoNotificationTitle;
	}

	public String getInfoNotification() {
		return infoNotification;
	}

	public void setInfoNotification(String infoNotification) {
		this.infoNotification = infoNotification;
	}

	public GamificationPoint getKnowledgepoints() {
		return knowledgepoints;
	}

	public void setKnowledgepoints(GamificationPoint knowledgepoints) {
		this.knowledgepoints = knowledgepoints;
	}

	public GamificationPoint getSocialpoints() {
		return socialpoints;
	}

	public void setSocialpoints(GamificationPoint socialpoints) {
		this.socialpoints = socialpoints;
	}

	public GamificationPoint getCreativitypoints() {
		return creativitypoints;
	}

	public void setCreativitypoints(GamificationPoint creativitypoints) {
		this.creativitypoints = creativitypoints;
	}

	public int getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(int timeLimit) {
		this.timeLimit = timeLimit;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	@Override
	public String toString() {
		return "GamificationEvent [event_id=" + event_id + ", event_name=" + event_name + ", successNotificationTitle="
				+ successNotificationTitle + ", successNotification=" + successNotification
				+ ", errorNotificationTitle=" + errorNotificationTitle + ", errorNotification=" + errorNotification
				+ ", infoNotificationTitle=" + infoNotificationTitle + ", infoNotification=" + infoNotification
				+ ", knowledgepoints=" + knowledgepoints + ", socialpoints=" + socialpoints + ", creativitypoints="
				+ creativitypoints + ", timeLimit=" + timeLimit + ", limit=" + limit + "]";
	}

}
