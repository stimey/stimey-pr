package eu.stimey.platform.microservice.achievement.databind;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import eu.stimey.platform.library.utils.databind.DateDeserializer;
import eu.stimey.platform.library.utils.databind.DateSerializer;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class GamificationDailyGoalProgress {

	@JsonSerialize(using = DateSerializer.class)
	@JsonDeserialize(using = DateDeserializer.class)
	private Date date;
	private int goal;
	private int reached;

	public GamificationDailyGoalProgress() {
		this(null, 0, 0);
	}

	public GamificationDailyGoalProgress(Date date, int goal, int reached) {
		this.date = date;
		this.goal = goal;
		this.reached = reached;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getGoal() {
		return goal;
	}

	public void setGoal(int goal) {
		this.goal = goal;
	}

	public int getReached() {
		return reached;
	}

	public void setReached(int reached) {
		this.reached = reached;
	}

	@Override
	public String toString() {
		return "GamificationDailyGoalProgress [date=" + date + ", goal=" + goal + ", reached=" + reached + "]";
	}

}
