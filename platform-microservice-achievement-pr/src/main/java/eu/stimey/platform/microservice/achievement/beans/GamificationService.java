package eu.stimey.platform.microservice.achievement.beans;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bson.Document;

import actor4j.core.data.access.MongoUtils;
import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.library.core.databind.caching.UserSharedCacheObject;
import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.achievement.databind.GamificationDailyGoal;
import eu.stimey.platform.microservice.achievement.databind.GamificationDailyGoalProgress;
import eu.stimey.platform.microservice.achievement.databind.GamificationEvent;
import eu.stimey.platform.microservice.achievement.databind.GamificationEvents;
import eu.stimey.platform.microservice.achievement.databind.GamificationLeaderboard;
import eu.stimey.platform.microservice.achievement.databind.GamificationLevel;
import eu.stimey.platform.microservice.achievement.databind.GamificationMessage;
import eu.stimey.platform.microservice.achievement.databind.GamificationNotificationResponse;
import eu.stimey.platform.microservice.achievement.databind.GamificationPoint;
import eu.stimey.platform.microservice.achievement.databind.LeaderboardType;
import eu.stimey.platform.microservice.achievement.databind.NotificationType;
import eu.stimey.platform.microservice.achievement.databind.UserGamification;

public class GamificationService implements GamificationEvents {

	private Caching<String, UserGamification> usersCache;
	private Caching<String, GamificationLevel> levelCache;
	private Caching<String, GamificationEvent> eventCache;

	public GamificationService() {

		this.usersCache = new Caching<>("ms_achievement", "userid", UserGamification.class, "gamificationstream",
				GlobalVariables.CACHE_CONFIG);
		this.usersCache.clear();

		this.levelCache = new Caching<>("ms_achievement", "level", GamificationLevel.class, "gamificationlevel",
				GlobalVariables.CACHE_CONFIG);
		this.levelCache.clear();

		this.eventCache = new Caching<>("ms_achievement", "event_id", GamificationEvent.class, "gamificationevents",
				GlobalVariables.CACHE_CONFIG);
		this.eventCache.clear();
	}

	public UserGamification getUserGamification(String id) {
		return usersCache.get(id);
	}

	public void setUserGamification(String id, UserGamification ug) {
		usersCache.set(id, ug);
	}

	public List<UserGamification> getUserGamifications() {
		List<String> keys = usersCache.find(null, null, 0, 0, (ug) -> ug.getUserid(), true);

		List<UserGamification> users = new LinkedList<>();

		for (String key : keys) {
			users.add(usersCache.get(key));

		}

		return users;
	}
	
	public List<UserGamification> getUserGamificationsLimited(int skip, int limit) {
		List<UserGamification> users  = MongoUtils.find(null, new Document("totalPoints", -1), null, skip, limit,
				MongoDBService.getService().getClient(), "ms_achievement", "gamificationstream", UserGamification.class);

		return users;
	}

	public GamificationLevel getGamificationLevel(String id) {
		return levelCache.get(id);
	}

	public GamificationEvent getGamificationEvent(String id) {
		return eventCache.get(id);
	}

	public List<GamificationLeaderboard> getLeaderboard(UserSharedService usersharedservice, LeaderboardType type, List<UserGamification> allusers) {
		List<GamificationLeaderboard> leaderboard = new LinkedList<>();

		if (type == LeaderboardType.ALLTIME) {

			for (UserGamification ug : allusers) {

				String userid = ug.getUserid();
				UserSharedCacheObject userSharedCacheObject = usersharedservice.getUser(userid);
				if (userSharedCacheObject!=null) {
					String username = userSharedCacheObject.username;

					// TODO: country is missing
					GamificationLeaderboard user = new GamificationLeaderboard(userid, username, ug.getLevel(), ug.getTotalPoints(),
							ug.getKnowledgePoints(), ug.getSocialPoints(), ug.getCreativityPoints(), "");
					leaderboard.add(user);
				}

			}

			// sort leaderboard by:
			// * level
			// * total points
			// * knowledge points
			// * social points
			// * creativity points
			// reverse list -> highest on first position
			leaderboard = leaderboard.stream()
					.sorted(Comparator.comparing(GamificationLeaderboard::getLevel)
							.thenComparing(GamificationLeaderboard::getTotal)
							.thenComparing(GamificationLeaderboard::getKnowledge)
							.thenComparing(GamificationLeaderboard::getSocial)
							.thenComparing(GamificationLeaderboard::getCreativity).reversed())
					.collect(Collectors.toList());

			return leaderboard;

		} else {

			// set Calendar in relation to Leaderboardtype
			Calendar cal = Calendar.getInstance();
			if (type == LeaderboardType.DAILY) {
				cal.add(Calendar.DATE, -1);
			} else if (type == LeaderboardType.WEEKLY) {
				cal.add(Calendar.DATE, -7);
			} else if (type == LeaderboardType.MONTHLY) {
				cal.add(Calendar.DATE, -30);
			}

			Date date = cal.getTime();

			for (UserGamification ug : allusers) {

				List<GamificationPoint> points = new LinkedList<>();

				String userid = ug.getUserid();
				UserSharedCacheObject userSharedCacheObject = usersharedservice.getUser(userid);
				if (userSharedCacheObject!=null) {
					String username = userSharedCacheObject.username;

					// TODO: country is missing
					GamificationLeaderboard  user = new GamificationLeaderboard(userid, username, ug.getLevel(), ug.getTotalPoints(),
							ug.getKnowledgePoints(), ug.getSocialPoints(), ug.getCreativityPoints(), "");

					for (GamificationPoint gp : ug.getAllPoints()) {
						if (gp.getDate().getTime() > date.getTime()) {
							points.add(gp);
						}
					}

					int kp = 0;
					int sp = 0;
					int cp = 0;

					for (GamificationPoint g : points) {
						switch (g.getType()) {
						case KNOWLEDGEPOINT: {
							kp += g.getValue();
							break;
						}
						case SOCIALPOINT: {
							sp += g.getValue();
							break;
						}
						case CREATIVITYPOINT: {
							cp += g.getValue();
							break;
						}
						default:
							break;
						}
					}

					user.setKnowledge(kp);
					user.setSocial(sp);
					user.setCreativity(cp);
					leaderboard.add(user);
				}
			}

			// sort leaderboard by:
			// * level
			// * total points
			// * knowledge points
			// * social points
			// * creativity points
			// reverse list -> highest on first position
			leaderboard = leaderboard.stream()
					/*.sorted(Comparator.comparing(GamificationLeaderboard::getLevel)
							.thenComparing(GamificationLeaderboard::getTotal)
							.thenComparing(GamificationLeaderboard::getKnowledge)
							.thenComparing(GamificationLeaderboard::getSocial)
							.thenComparing(GamificationLeaderboard::getCreativity).reversed())*/
					.collect(Collectors.toList());

			return leaderboard;

		}

	}

	/**
	 *
	 * @param userID
	 * @param eventID
	 * @param messageID or CourseID
	 */
	// public GamificationNotificationResponse publish(String userID, int eventID,
	// String messageID) {
	//
	// // store total points before the new GamificationEvent will be saved
	// int before = usersCache.get(userID).getTotalPoints();
	//
	// usersCache.publish("gamificationstream", null, new Message(messageID, userID,
	// eventID));
	//
	// // give time for data to be written in Database
	// try {
	// Thread.sleep(500);
	// } catch (InterruptedException e) {
	// e.printStackTrace();
	// }
	//
	// UserGamification ug = usersCache.get(userID);
	//
	// GamificationEvent event = eventCache.get(String.valueOf(eventID));
	// // check if published event was stored in the database
	// if (ug.getEvents().stream().anyMatch(m ->
	// m.getMessageid().equals(messageID))) {
	// // check if sum of total Points increased
	// if (before == ug.getTotalPoints()) {
	// return new GamificationNotificationResponse(
	// event, NotificationType.info); // Event is stored but limit is reached (no
	// points)
	// } else {
	// return new GamificationNotificationResponse(
	// event, NotificationType.success); // Event is stored an points are given
	// }
	// }
	//
	// return new GamificationNotificationResponse(
	// event, NotificationType.error); // Event wasn't stored (something went wrong)
	//
	// }

	public GamificationNotificationResponse calculatePoints(String userID, int eventID, String messageID,
			String courseID) {
		return calculatePoints(userID, eventID, messageID, courseID, 0, null);

	}

	public GamificationNotificationResponse calculatePoints(String userID, int eventID, String messageID,
			String courseID, int fantasyPoints, String respType) {

		boolean dbUpdate = false;
		boolean noFantasyUpdate = false;
		boolean inDatabase = false;
		Date timestamp = new Date();
		int before = 0;

		GamificationMessage gamiMessage = new GamificationMessage(messageID, eventID, courseID, timestamp);
		GamificationEvent gamificationevent = eventCache.get(String.valueOf(eventID));
		UserGamification usergamification = usersCache.get(userID);

		if (gamificationevent != null) {
			// if user isn't in the database insert new Document
			// First event will be "Register" since it's the first event with Points to be
			// executed,
			// therefore Daily Goal Points won't be regarded
			if (usergamification == null) {

				GamificationPoint kp = gamificationevent.getKnowledgepoints();
				GamificationPoint sp = gamificationevent.getSocialpoints();
				GamificationPoint cp = gamificationevent.getCreativitypoints();

				usergamification = new UserGamification(userID, 1, kp.getValue(), sp.getValue(), cp.getValue(), 0);

				kp.setDate(timestamp);
				sp.setDate(timestamp);
				cp.setDate(timestamp);

				kp.setCourseid(courseID);
				sp.setCourseid(courseID);
				cp.setCourseid(courseID);

				usergamification.addEvent(gamiMessage);
				usergamification.setAllPoints(new LinkedList<>(Arrays.asList(kp, sp, cp)));

				usersCache.set(userID, usergamification);
				dbUpdate = true;

			} else {
				// if user is already in database check for events and increment the points if
				// no limit is
				// reached

				// get the needed user data from the database
				List<GamificationMessage> gms = usergamification.getEvents();
				before = usergamification.getTotalPoints();

				if (usergamification != null) {
					// which timelimit does the event has
					switch (gamificationevent.getTimeLimit()) {

					// points will only be given, if the executed event isn't already in the
					// EventList
					case ONCE: {

						if (gms.stream().anyMatch(g -> g.getEvent() != eventID)) {

							// add the Points and event as GamificationMessage
							usergamification.addEvent(gamiMessage);
							usergamification = addPoints(usergamification, gamificationevent, timestamp, courseID);

							usersCache.set(userID, usergamification);
							dbUpdate = true;
						}

						break;
					}

					// points will only be given, if the executed event hasn't been executed X times
					// within
					// 24 hours (X is the daily limit)
					case TIMES_A_DAY: {

						// compare with given limit, if given limit isn't reached:
						// -> write event in EventList
						// -> give points
						if (gms.stream().filter(g -> g.getEvent() == eventID).count() < gamificationevent.getLimit()) {

							// add the Points and event as GamificationMessage
							usergamification.addEvent(gamiMessage);
							usergamification = addPoints(usergamification, gamificationevent, timestamp, courseID);
							usersCache.set(userID, usergamification);
							dbUpdate = true;

						} else {
							// if given limit is reached:
							// -> get the oldest event/s and check if it/they is/are expired (24h)
							// if expired:
							// -> delete it/them
							// -> add the new event
							// the new date must be degraded by one day to compare it with the events in the
							// List, which expire after 24h
							Calendar cal = Calendar.getInstance();
							cal.setTime(timestamp);
							cal.add(Calendar.DATE, -1);
							Date newevent = cal.getTime();

							gms.stream().filter(g -> g.getEvent() == eventID)
									.filter(g -> g.getTimestamp().getTime() < newevent.getTime())
									// .forEach(System.out::println);
									.collect(Collectors.toList()).forEach(gms::remove);

							// after expired events are removed count the list again, if limit isn't reached
							// again
							// -> give points
							if (gms.stream().filter(g -> g.getEvent() == eventID).count() < gamificationevent
									.getLimit()) {

								// add the Points
								usergamification = addPoints(usergamification, gamificationevent, timestamp, courseID);

							}

							// add event as GamificationMessage
							usergamification.addEvent(gamiMessage);
							usersCache.set(userID, usergamification);
							dbUpdate = true;

						}

						break;
					}

					// just happens with courses
					// points will only be given, if the executed event hasn't been executed X times
					// within
					// 24 hours (X is the daily limit) and if the course hasn't already been taken
					// once
					case ONCE_AND_TIMES_A_DAY: {

						// if there is already a GamificationMessage in the EventList with the given
						// courseID
						// -> do nothing
						if (gms.stream().anyMatch(p -> p.getCourseid() != null && p.getCourseid().equals(courseID))) {
							break;
						}

						// compare with given limit, if given limit isn't reached:
						// -> write event in EventList
						// -> give points
						if (gms.stream().filter(g -> g.getEvent() == eventID).count() < gamificationevent.getLimit()) {

							// store GamificationMessage with ID of message as value (this ID is the
							// courseID)
							// usergamification = addEvent(usergamification, messageID, eventID,
							// timestamp);

							// add the Points and event as GamificationMessage
							usergamification.addEvent(gamiMessage);
							usergamification = addPoints(usergamification, gamificationevent, timestamp, courseID);

							usersCache.set(userID, usergamification);
							dbUpdate = true;

						} else {
							// if given limit is reached:
							// -> check if X messages aren't older than a day (X is the daily limit)
							// if X messages aren't older:
							// -> add the new event but without giving points
							// -> otherwise: add the new event and give points

							// the new date must be degraded by one day to compare it with the events in the
							// List to check if they're older than 24 hours
							Calendar cal = Calendar.getInstance();
							cal.setTime(timestamp);
							cal.add(Calendar.DATE, -1);
							Date newevent = cal.getTime();

							// get all the messages, with the given event and check if they are older than a
							// day
							// and then count them
							// if given limit isn't reached
							// -> give points
							if (gms.stream().filter(g -> g.getEvent() == eventID)
									.filter(g -> g.getTimestamp().getTime() < newevent.getTime())
									.count() < gamificationevent.getLimit()) {

								// add the Points
								usergamification = addPoints(usergamification, gamificationevent, timestamp, courseID);
							}

							// add the new Event as GamificationMessage
							usergamification.addEvent(gamiMessage);

							usersCache.set(userID, usergamification);
							dbUpdate = true;

						}

						break;
					}

					case NO_LIMIT: {
						// delete all Events that match this time limit
						gms.stream().filter(g -> g.getEvent() == eventID).collect(Collectors.toList())
								.forEach(gms::remove);

						// add the points
						usergamification = addPoints(usergamification, gamificationevent, timestamp, courseID);
						// add event as GamificationMessage for the Reponse -> gets deleted next time
						usergamification.addEvent(gamiMessage);

						usersCache.set(userID, usergamification);
						dbUpdate = true;

						break;
					}
					case FANTASY: {

						// look if there is already a fantasy stored
						// fantasyid is stored in the courseid
						if (gms.stream().anyMatch(p -> p.getCourseid() != null && p.getCourseid().equals(courseID))) {
							inDatabase = true;
							noFantasyUpdate = true;
							break;
						}

						if (respType.equals("get")) {
							inDatabase = false;
							noFantasyUpdate = true;
							break;
						} else if (respType.equals("set")) {
							GamificationPoint knowledgepoints = gamificationevent.getKnowledgepoints();
							int value = fantasyPoints / 10;
							knowledgepoints.setValue(value);
							gamificationevent.setKnowledgepoints(knowledgepoints);

							usergamification = addPoints(usergamification, gamificationevent, timestamp, courseID);
							usergamification.addEvent(gamiMessage);

							usersCache.set(userID, usergamification);

							dbUpdate = true;

							break;
						}
						break;

					}
					default: {
						break;
					}
					}
				}
			}
		}

		if (dbUpdate) {
			if (usergamification.getEvents().stream().anyMatch(m -> m.getMessageid().equals(messageID))) {
				// check if sum of total Points increased

				if (before == usergamification.getTotalPoints()) {
					return new GamificationNotificationResponse(gamificationevent, NotificationType.info); // Event is
																											// stored
																											// but limit
																											// is
																											// reached
																											// (no
																											// points)
				} else {
					return new GamificationNotificationResponse(gamificationevent, NotificationType.success); // Event
																												// is
																												// stored
																												// an
																												// points
																												// are
																												// given
				}
			}
		}

		// just ask the points from Database
		if (noFantasyUpdate) {
			if (inDatabase) {
				return new GamificationNotificationResponse(gamificationevent, NotificationType.fantasy_info); // no
																												// points
																												// are
																												// stored
																												// because
																												// the
																												// Fantasy
																												// is
				// already played
			} else {
				return new GamificationNotificationResponse(gamificationevent, NotificationType.question); // Ask if the
																											// earned
																											// fantasy
																											// points
																											// should be
																											// written
																											// in the
																											// database
			}
		}

		// Something went wrong with storing the user-data or with getting the executed
		// event data
		return new GamificationNotificationResponse(new GamificationEvent(null, null, null, null, "OOPS!",
				"Something went wrong", null, null, null, null, null, 0), NotificationType.error);

	}

	// add the points to UserGamification and check if the level changed
	// -> if level changed update the users level
	private UserGamification addPoints(UserGamification usergamification, GamificationEvent gamificationevent,
			Date messageDate, String courseID) {

		GamificationPoint kpPoint = gamificationevent.getKnowledgepoints();
		GamificationPoint spPoint = gamificationevent.getSocialpoints();
		GamificationPoint cpPoint = gamificationevent.getCreativitypoints();

		kpPoint.setDate(messageDate);
		spPoint.setDate(messageDate);
		cpPoint.setDate(messageDate);

		kpPoint.setCourseid(courseID);
		spPoint.setCourseid(courseID);
		cpPoint.setCourseid(courseID);

		int eventKp = kpPoint.getValue();
		int eventSp = spPoint.getValue();
		int eventCp = cpPoint.getValue();

		int kp = usergamification.getKnowledgePoints() + eventKp;
		int sp = usergamification.getSocialPoints() + eventSp;
		int cp = usergamification.getCreativityPoints() + eventCp;

		usergamification.setKnowledgePoints(kp);
		usergamification.setSocialPoints(sp);
		usergamification.setCreativityPoints(cp);

		usergamification.addPoints(new LinkedList<>(Arrays.asList(kpPoint, spPoint, cpPoint)));

		GamificationDailyGoal dailyGoal = usergamification.getDailyGoal();

		// daily goal is set
		if (dailyGoal != null) {

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			Date now = cal.getTime();

			int reachedKp = dailyGoal.getKp();
			int reachedSp = dailyGoal.getSp();
			int reachedCp = dailyGoal.getCp();

			int goalKp = dailyGoal.getGoal_kp();
			int goalSp = dailyGoal.getGoal_sp();
			int goalCp = dailyGoal.getGoal_cp();

			// check if daily Goal Day isn't older than 24 hours
			if (dailyGoal.getDate().getTime() > now.getTime()) {
				reachedKp += eventKp;
				reachedSp += eventSp;
				reachedCp += eventCp;

				dailyGoal.setKp(reachedKp);
				dailyGoal.setSp(reachedSp);
				dailyGoal.setCp(reachedCp);

				// check if goal is reached
				if (reachedKp >= goalKp && reachedSp >= goalSp && reachedCp >= goalCp) {

					// only event, that doesn't get stored
					// caching.publish("GamificationStream", null, new Message(
					// new ObjectId().toHexString(), ug.getUserid(),
					// GamificationEvents.COMPLETE_DAILY_GOAL));
					usergamification.setDailyGoalReached(true);
					usergamification.setDailyGoal(null);
				}
			} else { // if it is older -> delete Daily Goal
				usergamification.setDailyGoal(null);
				usergamification.setDailyGoalReached(false);
			}

			// set Daily goals for this week
			Map<Integer, GamificationDailyGoalProgress> goals = usergamification.getDailyGoalProgress();
			int totalGoalPoints = goalKp + goalSp + goalCp;
			int totalReachedPoints = reachedKp + reachedSp + reachedCp;

			cal.setTime(dailyGoal.getDate());
			int dailyGoalDay = cal.get(Calendar.DAY_OF_WEEK);

			goals.put(dailyGoalDay,
					new GamificationDailyGoalProgress(cal.getTime(), totalGoalPoints, totalReachedPoints));

		}

		// check if the User gets a Level Up
		GamificationLevel level = levelCache.get(String.valueOf(usergamification.getLevel()));

		if (kp > level.getKp_limit() && sp > level.getSp_limit() && cp > level.getCp_limit()) {
			int newlevel = usergamification.getLevel() + 1;

			usergamification.setLevel(newlevel);
			usergamification.setEnergyOrbs(usergamification.getEnergyOrbs() + newlevel * 1000);
		}

		return usergamification;
	}

	public boolean getNotification(String messageid, String userid) {

		UserGamification ug = usersCache.get(userid);
		if (ug.getEvents().stream().anyMatch(m -> m.getMessageid().equals(messageid))) {
			return true;
		}

		return false;
	}

	public int getTotalUserCoursePoints(List<String> userids, String courseid) {

		int totalPoints = 0;

		for (String id : userids) {
			UserGamification ug = usersCache.get(id);

			if (ug != null) {
				totalPoints += ug.getAllPoints().stream()
						.filter(p -> p.getCourseid() != null && p.getCourseid().equals(courseid))
						.mapToInt(p -> p.getValue()).sum();
			}

		}

		return totalPoints;
	}

}
