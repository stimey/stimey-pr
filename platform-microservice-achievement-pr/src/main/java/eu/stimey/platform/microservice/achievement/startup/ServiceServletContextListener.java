package eu.stimey.platform.microservice.achievement.startup;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import eu.stimey.platform.library.core.startup.MongoDBService;
import eu.stimey.platform.library.core.startup.RedisAMQPService;
import eu.stimey.platform.library.utils.StimeyLogger;
import eu.stimey.platform.microservice.achievement.databases.GamificationEventsDB;
import eu.stimey.platform.microservice.achievement.databases.GamificationLevelDB;

/**
 * Global web application settings
 * 
 */
@WebListener
public class ServiceServletContextListener implements ServletContextListener {
	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		Global.startup();
		StimeyLogger.setApplicationName(servletContextEvent.getServletContext().getContextPath());

		MongoDBService.start("ms_achievement");
		RedisAMQPService.start();

		// Fill Event and Level Databases
		GamificationEventsDB events = new GamificationEventsDB();
		events.fillDatabase();

		GamificationLevelDB levels = new GamificationLevelDB();
		levels.fillDatabase();

	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		RedisAMQPService.stop();
		MongoDBService.stop();
	}
}
