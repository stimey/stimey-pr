package eu.stimey.platform.microservice.achievement.rest.resources;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import eu.stimey.platform.microservice.achievement.databind.GamificationDailyGoal;
import eu.stimey.platform.microservice.achievement.databind.GamificationDailyGoalProgress;
import eu.stimey.platform.microservice.achievement.databind.GamificationEvent;
import eu.stimey.platform.microservice.achievement.databind.GamificationLeaderboard;
import eu.stimey.platform.microservice.achievement.databind.GamificationLevel;
import eu.stimey.platform.microservice.achievement.databind.GamificationNotificationResponse;
import eu.stimey.platform.microservice.achievement.databind.LeaderboardType;
import eu.stimey.platform.microservice.achievement.databind.NotificationType;
import eu.stimey.platform.microservice.achievement.databind.UserGamification;
import eu.stimey.platform.library.core.beans.UserSharedService;
import eu.stimey.platform.microservice.achievement.beans.GamificationService;
import eu.stimey.platform.microservice.achievement.databind.requests.DailyGoalRequest;
import eu.stimey.platform.microservice.achievement.databind.responses.GamificationDailyGoalResponse;
import eu.stimey.platform.microservice.achievement.databind.responses.GamificationLeaderboardListResponse;
import eu.stimey.platform.microservice.achievement.databind.responses.UserGamificationResponse;
import eu.stimey.platform.microservice.achievement.rest.utils.RESTUri;

@Path("/achievements")
public class AchievementResource {

	@Inject
	GamificationService gamificationService;
	@Inject
	UserSharedService userSharedService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserInfo(@Context ContainerRequestContext context) {

		// System.out.println("TESTTT");

		String userid = (String) context.getProperty("userid");

		UserGamification usergamification = gamificationService.getUserGamification(userid);
		UserGamificationResponse response = null;

		if (usergamification != null) {

			// update Daily Goals of this week
			Map<Integer, GamificationDailyGoalProgress> goals = usergamification.getDailyGoalProgress();

			for (int i = 1; i <= 7; i++) {
				Calendar cal = Calendar.getInstance();
				int today = cal.get(Calendar.DAY_OF_WEEK);
				// System.out.println("TODAY: " + today);
				// future days will be deleted (are old dates)
				if (i > today) {
					goals.put(i, null);
				} else { // old goals will be checked -> from last week or not
					if (goals.get(i) != null) {
						Date goalDate = goals.get(i).getDate();
						cal.add(Calendar.DATE, -(today - i + 1));

						// System.out.println("DATE: " + cal.getTime());
						// System.out.println("GOALDATE: " + goalDate);

						// goals older than a week will be replaced
						if (goalDate.getTime() < cal.getTime().getTime()) {
							goals.put(i, new GamificationDailyGoalProgress(cal.getTime(), 0, 0));
						}
					}
				}
			}

			usergamification.setDailyGoalProgress(goals);
			gamificationService.setUserGamification(userid, usergamification);

			GamificationLevel gamificationLevel = gamificationService
					.getGamificationLevel(String.valueOf(usergamification.getLevel()));

			response = new UserGamificationResponse(usergamification, gamificationLevel,
					usergamification.getDailyGoal());

		}

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();

	}

	@GET
	@Path("/mission/{missionid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCourseProgress(@Context ContainerRequestContext context,
			@PathParam("missionid") String missionid, @QueryParam("ids[]") List<String> list) {

		String userid = (String) context.getProperty("userid");

		/*
		 * for (String s : list) { System.out.println("ELEM: " + s); }
		 */

		// System.out.println("MISSIONID: " + missionid);
		// UserGamification usergamification =
		// gamificationService.getUserGamification(userid);

		int points = gamificationService.getTotalUserCoursePoints(list, missionid);
		// System.out.println("POINTS: " + points);

		return Response.ok().entity(points).build();
	}

	@GET
	@Path("/dailygoal/{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDailyGoal(@Context ContainerRequestContext context, @PathParam("userid") String userid) {

		UserGamification usergamification = gamificationService.getUserGamification(userid);

		if (usergamification != null) {

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			Date now = cal.getTime();

			GamificationDailyGoal dailyGoal = usergamification.getDailyGoal();

			// if no DailyGoal is set return null
			if (dailyGoal == null) {
				return null;
			} else { // if DailyGoal is set -> check if the 24h limit hasn't been reached
				if (dailyGoal.getDate().getTime() > now.getTime()) {
					// return DailyGoal till it's still on

					GamificationDailyGoalResponse response = new GamificationDailyGoalResponse(dailyGoal);
					return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context))
							.entity((response == null) ? dailyGoal : response).build();
				} else { // DailyGoal is expired -> delete the old DailyGoal
					usergamification.setDailyGoal(null);
					usergamification.setDailyGoalReached(false);
					gamificationService.setUserGamification(userid, usergamification);
					return null;
				}
			}
		}
		return null;

	}

	@POST
	@Path("dailygoal/{userid}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response setDailyGoal(@Context ContainerRequestContext context, DailyGoalRequest dailygoal,
			@PathParam("userid") String userid) {

		boolean firstGoalOver20 = false;

		int kpGoal = Integer.valueOf(dailygoal.knowledgeGoal);
		int spGoal = Integer.valueOf(dailygoal.socialGoal);
		int cpGoal = Integer.valueOf(dailygoal.creativityGoal);

		// check for first goal over 20
		if (kpGoal > 20 && spGoal > 20 && cpGoal > 20) {
			// firstGoalOver20 = gamificationService.publish(userid,
			// GamificationEvents.SET_FIRST_DAILY_GOAL_VALUES_OVER_20, new
			// ObjectId().toHexString());
		}

		UserGamification userGamification = gamificationService.getUserGamification(userid);
		GamificationDailyGoal oldGoal = userGamification.getDailyGoal();
		GamificationDailyGoal newGoal = new GamificationDailyGoal(kpGoal, spGoal, cpGoal);
		GamificationNotificationResponse response = null;

		// System.out.println("OLD: " + oldGoal);

		// if there is no DailyGoal set the new Goal
		if (oldGoal == null) {
			userGamification.setDailyGoal(newGoal);
			userGamification.setDailyGoalReached(false);
			gamificationService.setUserGamification(userid, userGamification);

			// over 20?
			if (firstGoalOver20) {
				// TODO: REMOVE
				// GamificationEvent event = gamificationService
				// .getGamificationEvent(String.valueOf(GamificationEvents.SET_FIRST_DAILY_GOAL_VALUES_OVER_20));
				GamificationEvent event = null;

				response = new GamificationNotificationResponse(event, NotificationType.success);

			} else { // no GamificationEvent is set -> just return that DailyGoal is set
				response = new GamificationNotificationResponse("Daily Goal is set!", "You have to reach " + kpGoal
						+ " Brain Points, " + spGoal + " Heart Points and " + cpGoal + " Spirit Points today!",
						NotificationType.success);
			}

		} else { // there is already a DailyGoal set

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			Date now = cal.getTime();
			// check if DailyGoal is expired
			if (oldGoal.getDate().getTime() < now.getTime()) {

				// expired -> set the new DailyGoal
				userGamification.setDailyGoal(newGoal);
				userGamification.setDailyGoalReached(false);
				// gamificationService.setUserGamification(userid, userGamification);

				// over 20?
				if (firstGoalOver20) {
					// TODO: REMOVE
					// GamificationEvent event = gamificationService.getGamificationEvent(
					// String.valueOf(GamificationEvents.SET_FIRST_DAILY_GOAL_VALUES_OVER_20));
					GamificationEvent event = null;
					response = new GamificationNotificationResponse(event, NotificationType.success);

				} else { // no GamificationEvent is set -> just return that DailyGoal is set
					response = new GamificationNotificationResponse(
							"Daily Goal is set!", "You have to reach " + kpGoal + " Brain Points, " + spGoal
									+ " Heart Points and " + cpGoal + " Spirit Points today!",
							NotificationType.success);
				}

			} else { // not expired -> return that current DialyGoal is not completed
				response = new GamificationNotificationResponse("No Daily Goal is set!",
						"Your current Daily Goal is not completed!", NotificationType.error);
			}
		}

		return Response.status(Status.OK).header("URI", RESTUri.parseAbsolutePath(context)).entity(response).build();

	}

	@GET
	@Path("/leaderboard")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLeaderboard(@Context ContainerRequestContext context) {
		List<GamificationLeaderboard> alltime = gamificationService.getLeaderboard(userSharedService,
				LeaderboardType.ALLTIME, gamificationService.getUserGamificationsLimited(0, 100));
		/*
		List<GamificationLeaderboard> month = gamificationService.getLeaderboard(userSharedService,
				LeaderboardType.MONTHLY);
		List<GamificationLeaderboard> week = gamificationService.getLeaderboard(userSharedService,
				LeaderboardType.WEEKLY);
		List<GamificationLeaderboard> day = gamificationService.getLeaderboard(userSharedService,
				LeaderboardType.DAILY);

		GamificationLeaderboardListResponse response = new GamificationLeaderboardListResponse(context, alltime, month,
				week, day);*/
		
		GamificationLeaderboardListResponse response = new GamificationLeaderboardListResponse(context, alltime, alltime,
				alltime, alltime);
		

		return Response.status(Status.OK).header("URI", RESTUri.parseCompletePath(context)).entity(response).build();

	}

	@GET
	@Path("/{id}/overAllStudents")
	@Produces(MediaType.APPLICATION_JSON)
	public Response overallStudentsOfTeacher(@PathParam("id") String teacherId) {
		// final Teacher teacher = this.teacherService.getTeacher(teacherId);
		//
		// if (!teacher.hasMissions())
		// return Response.status(Response.Status.NO_CONTENT).build();

		// final List<Mission> activeMissions =
		// this.missionsService.getMissionsByIds(teacher.activeMissions);
		Map<String, List<String>> missionToStudentsMap = new HashMap<>();
		// for(final Mission mission : activeMissions){
		// missionToStudentsMap.put(mission.get_id(), mission.students);
		// }

		List<String> first = new LinkedList<>();
		List<String> second = new LinkedList<>();

		first.add("5beadd4c11219c37a8a6853f");
		// first.add("Student2");
		second.add("5beadd4c11219c37a8a6853f");
		// second.add("Student3");

		missionToStudentsMap.put("EINS", first);
		missionToStudentsMap.put("ZWEI", second);
		return Response.ok().entity(missionToStudentsMap).build();
	}

	@GET
	@Path("/{missionId}/overAllGamificationPoints")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOverallGamificationPointsOfMission(@PathParam("missionId") String missionId) {
		// final Mission mission = this.missionsService.getMissionById(missionId);
		// int amountOfLearningObjects = mission.levels.stream().map(level ->
		// level.learningObjectIds).flatMap(List::stream).collect(Collectors.toList()).size();

		// System.out.println("MISSIONID: " + missionId);
		final GamificationPoints gamificationPoints = new GamificationPoints(5, 6, 7);
		return Response.status(Status.OK).entity(gamificationPoints).build();
	}

}

class GamificationPoints {
	private final int knowledge;
	private final int social;
	private final int creativity;

	public GamificationPoints(int knowledge, int social, int creativity) {
		this.knowledge = knowledge;
		this.social = social;
		this.creativity = creativity;
	}

	public int getKnowledge() {
		return knowledge;
	}

	public int getSocial() {
		return social;
	}

	public int getCreativity() {
		return creativity;
	}
}
