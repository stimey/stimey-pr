package eu.stimey.platform.microservice.achievement.databind;

public class GamificationLeaderboard {

	private String userid;
	private String name;
	private int level;
	private int total;
	private int knowledge;
	private int social;
	private int creativity;
	private String country;

	public GamificationLeaderboard() {
		this(null, null, 0, 0, 0, 0, 0, null);

	}

	public GamificationLeaderboard(String userid, String name, int level, int total, int knowledge, int social,
			int creativity, String country) {
		super();
		this.userid = userid;
		this.name = name;
		this.level = level;
		this.total = total;
		this.knowledge = knowledge;
		this.social = social;
		this.creativity = creativity;
		this.country = country;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getKnowledge() {
		return knowledge;
	}

	public void setKnowledge(int knowledge) {
		this.knowledge = knowledge;
	}

	public int getSocial() {
		return social;
	}

	public void setSocial(int social) {
		this.social = social;
	}

	public int getCreativity() {
		return creativity;
	}

	public void setCreativity(int creativity) {
		this.creativity = creativity;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "GamificationLeaderboard [userid=" + userid + ", name=" + name + ", level=" + level + ", total=" + total
				+ ", knowledge=" + knowledge + ", social=" + social + ", creativity=" + creativity + ", country="
				+ country + "]";
	}

}
