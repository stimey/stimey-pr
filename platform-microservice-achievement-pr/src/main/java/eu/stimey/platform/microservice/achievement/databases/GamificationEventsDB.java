package eu.stimey.platform.microservice.achievement.databases;

import java.util.List;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.achievement.databind.GamificationPoint;
import eu.stimey.platform.microservice.achievement.databind.Pointtype;
import eu.stimey.platform.microservice.achievement.beans.GlobalVariables;
import eu.stimey.platform.microservice.achievement.databind.GamificationEvent;

public class GamificationEventsDB {

	private Caching<String, GamificationEvent> eventCache;

	public GamificationEventsDB() {
		this.eventCache = new Caching<>("ms_achievement", "event_id", GamificationEvent.class, "gamificationevents",
				GlobalVariables.CACHE_CONFIG);
		this.eventCache.clear();
	}

	public void fillDatabase() {

		List<String> keys = eventCache.find(null, null, 0, 0, (ge) -> ge.getEvent_id(), true);
		for (String key : keys) {
			eventCache.del(key);
		}

		eventCache.set("1",
				new GamificationEvent("1", "REGISTER", "You are now registered!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 50, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 50, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 50, null), 1, 0));
		eventCache.set("2",
				new GamificationEvent("2", "DAILY_LOGIN", "Your daily Login!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 10, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 10, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 10, null), 2, 1));
		eventCache.set("3",
				new GamificationEvent("3", "ENROL_COURSE", "You enrolled for this course!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "No extra points!",
						"You already enrolled for this course once or reached your daily limit!", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 20, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 10, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 10, null), 3, 5));
		eventCache.set("4",
				new GamificationEvent("4", "COMPLETE_LEARNING_OBJECT_COURSE", "You completed a learning object course!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 5, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 0, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 0, null), 0, 0));
		eventCache.set("5",
				new GamificationEvent("5", "COMPLETE_25_PERCENT_COURSE", "You completed 25% of this course!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 30, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 10, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 10, null), 0, 0));
		eventCache.set("6",
				new GamificationEvent("6", "COMPLETE_50_PERCENT_COURSE", "You completed 50% of this course!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 30, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 10, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 10, null), 0, 0));
		eventCache.set("7",
				new GamificationEvent("7", "COMPLETE_75_PERCENT_COURSE", "You completed 75% of this course!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 30, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 10, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 10, null), 0, 0));
		eventCache.set("8",
				new GamificationEvent("8", "COMPLETE_100_PERCENT_COURSE", "You completed this course!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 50, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 20, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 20, null), 0, 0));
		eventCache.set("9",
				new GamificationEvent("9", "POST_QUESTION_COURSE_QA", "You posted a Question in a course!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 3, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 2, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 1, null), 2, 5));
		eventCache.set("10",
				new GamificationEvent("10", "COMMENT_POST_QA", "You commented to a post!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 2, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 2, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 2, null), 2, 5));
		eventCache.set("11",
				new GamificationEvent("11", "COMMENT_ABOUT_COURSE_OR_TEACHER", "You commented about this course!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 1, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 2, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 1, null), 2, 5));
		eventCache.set("12",
				new GamificationEvent("12", "COURSE_REVIEW", "You reviewed a course!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 3, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 3, null), 2, 5));
		eventCache.set("13",
				new GamificationEvent("13", "CREATE_COMMUNITY", "You created a community!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 2, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 3, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 1, null), 2, 5));
		eventCache.set("14",
				new GamificationEvent("14", "JOIN_COMMUNITY", "You joined a community!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 2, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 2, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 0, null), 2, 5));
		eventCache.set("15",
				new GamificationEvent("15", "CREATE_COMMUNITY_ANNOUNCEMENT", "You created a community announcement!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 3, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 0, null), 2, 5));
		eventCache.set("16",
				new GamificationEvent("16", "COMMUNITY_PUBLISH_UPDATE_ONLY_TEXT",
						"You published an update in this community!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 1, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 1, null), 2, 10));
		eventCache.set("17",
				new GamificationEvent("17", "COMMUNITY_PUBLISH_UPDATE_IMAGE_OR_VIDEO",
						"You published an update with an Image or Video in this community!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 2, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 2, null), 2, 10));
		eventCache.set("18",
				new GamificationEvent("18", "COMMUNITY_PUBLISH_UPDATE_ONLY_URL",
						"You published an update with a link in this community!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 2, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 2, null), 2, 10));
		eventCache.set("19",
				new GamificationEvent("19", "COMMUNITY_COMMENT_UPDATE", "You commented on an update in this community!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 2, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 1, null), 2, 5));
		eventCache.set("20",
				new GamificationEvent("20", "COMMUNITY_LIKE_SHARE_UPDATE", "You liked/shared this update!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 1, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 0, null), 2, 5));
		eventCache.set("21",
				new GamificationEvent("21", "USER_PROFILE_PUBLISH_UPDATE_ONLY_TEXT",
						"You published an update on your profile!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 1, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 1, null), 2, 10));
		eventCache.set("22",
				new GamificationEvent("22", "USER_PROFILE_PUBLISH_UPDATE_IMAGE_OR_VIDEO",
						"You published an update with an Image or Video on your profile!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 2, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 2, null), 2, 10));
		eventCache.set("23",
				new GamificationEvent("23", "USER_PROFILE_PUBLISH_UPDATE_ONLY_URL",
						"You published an update with a link on your profile!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 2, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 2, null), 2, 10));
		eventCache.set("24",
				new GamificationEvent("24", "USER_PROFILE_COMMENT_UPDATE",
						"You commented on an update on your profile!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 2, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 1, null), 2, 5));
		eventCache.set("25",
				new GamificationEvent("25", "USER_PROFILE_LIKE_SHARE_UPDATE", "You liked/shared this update!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 1, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 0, null), 2, 5));
		eventCache.set("26",
				new GamificationEvent("26", "ADD_PROFILE_PHOTO_FIRST_TIME",
						"You added a profile photo for the first time!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 2, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 1, null), 1, 0));
		eventCache.set("27",
				new GamificationEvent("27", "ADD_COVER_PHOTO_FIRST_TIME", "You added a cover photo for the first time!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 2, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 1, null), 1, 0));
		eventCache.set("28",
				new GamificationEvent("28", "POST_DISCUSSION", "You published a post in a discussion!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 2, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 1, null), 2, 5));
		eventCache.set("29",
				new GamificationEvent("29", "DISCUSSION_COMMENT_POST", "You commented a post in a discussion!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 2, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 1, null), 2, 5));
		eventCache.set("30",
				new GamificationEvent("30", "COMPLETE_LEVEL_1_ACCURACY_OVER_60",
						"You completed level 1 with an accuracy over 60% in Mental Calculation!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 5, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 0, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 3, null), 2, 2));
		eventCache.set("31",
				new GamificationEvent("31", "COMPLETE_LEVEL_2_ACCURACY_OVER_60",
						"You completed level 2 with an accuracy over 60% in Mental Calculation!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 10, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 0, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 6, null), 2, 2));
		eventCache.set("32",
				new GamificationEvent("32", "COMPLETE_LEVEL_3_ACCURACY_OVER_60",
						"You completed level 3 with an accuracy over 60% in Mental Calculation!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 15, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 0, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 10, null), 2, 2));
		eventCache.set("33",
				new GamificationEvent("33", "COMPLETE_LEVEL_4_ACCURACY_OVER_60",
						"You completed level 4 with an accuracy over 60% in Mental Calculation!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 20, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 0, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 15, null), 2, 2));
		eventCache.set("34",
				new GamificationEvent("34", "COMPLETE_LEVEL_5_ACCURACY_OVER_60",
						"You completed level 5 with an accuracy over 60% in Mental Calculation!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 25, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 0, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 20, null), 2, 2));
		eventCache.set("35",
				new GamificationEvent("35", "COMPLETE_LEVEL_6_ACCURACY_OVER_60",
						"You completed level 6 with an accuracy over 60% in Mental Calculation!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 30, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 0, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 25, null), 2, 2));
		eventCache.set("36",
				new GamificationEvent("36", "LISTEN_STIMEY_RADIO", "You listened to STIMEY Radio for half an hour!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "",
						"No more points for today!", "You reached your daily limit!",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 5, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 0, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 5, null), 2, 4));
		eventCache.set("37",
				new GamificationEvent("37", "SET_DAILY_GOAL_VALUES_OVER_20", "Your first daily goal is set!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 10, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 10, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 10, null), 1, 0));
		eventCache.set("38",
				new GamificationEvent("38", "COMPLETE_DAILY_GOAL", "You completed your daily goal!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 5, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 5, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 5, null), 2, 1));
		eventCache.set("39",
				new GamificationEvent("39", "START_MENTAL_CALCULATOR", "You started the Mental Calculator today!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 5, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 5, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 5, null), 2, 1));

		eventCache.set("40",
				new GamificationEvent("40", "START_PHYSICS_ENGINE", "You started the Physics Engine today!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 5, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 5, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 5, null), 2, 1));

		eventCache.set("41",
				new GamificationEvent("41", "START_CHEMISTRY_ENGINE", "You started the Chemistry Engine today!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 5, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 5, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 5, null), 2, 1));

		eventCache.set("42",
				new GamificationEvent("42", "START_SERIOUS_GAMES", "You started the Serious Games today!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 5, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 5, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 5, null), 2, 1));

		eventCache.set("43",
				new GamificationEvent("43", "COMPLETE_TRAINING_LEVEL_1", "You completed a training in level 1!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 2, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 1, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 1, null), 2, 5));

		eventCache.set("44",
				new GamificationEvent("44", "COMPLETE_TRAINING_LEVEL_2", "You completed a training in level 2!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 3, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 1, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 1, null), 2, 5));

		eventCache.set("45",
				new GamificationEvent("45", "COMPLETE_TRAINING_LEVEL_3", "You completed a training in level 3!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 4, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 1, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 2, null), 2, 5));

		eventCache.set("46",
				new GamificationEvent("46", "COMPLETE_TRAINING_LEVEL_4", "You completed a training in level 4!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 5, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 1, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 2, null), 2, 5));

		eventCache.set("47",
				new GamificationEvent("47", "COMPLETE_TRAINING_LEVEL_5", "You completed a training in level 5!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 6, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 1, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 3, null), 2, 5));

		eventCache.set("48",
				new GamificationEvent("48", "COMPLETE_TRAINING_LEVEL_6", "You completed a training in level 6!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 7, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 1, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 3, null), 2, 5));

		eventCache.set("49",
				new GamificationEvent("49", "COMPLETE_FANTASY",
						"Your score is %2d, would you like to try to improve it?",
						"Please note %2d will replace the new score.", "", "", "Already completed Fantasy",
						"You did great!", new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 0, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 0, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 0, null), 4, 0));
		eventCache.set("50",
				new GamificationEvent("50", "RADIO_PODCAST_PROPOSAL_ACCEPTED", "You proposed an accepted podcast!",
						"You get %2d Brain Points, %2d Heart Points and %2d Spirit Points!", "", "", "", "",
						new GamificationPoint(Pointtype.KNOWLEDGEPOINT, null, 20, null),
						new GamificationPoint(Pointtype.SOCIALPOINT, null, 20, null),
						new GamificationPoint(Pointtype.CREATIVITYPOINT, null, 20, null), 0, 0));
	}

}
