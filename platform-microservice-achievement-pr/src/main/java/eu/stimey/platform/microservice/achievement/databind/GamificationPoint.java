package eu.stimey.platform.microservice.achievement.databind;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import eu.stimey.platform.library.utils.databind.DateDeserializer;
import eu.stimey.platform.library.utils.databind.DateSerializer;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class GamificationPoint {

	// private String name;
	private Pointtype type;

	@JsonSerialize(using = DateSerializer.class)
	@JsonDeserialize(using = DateDeserializer.class)
	private Date date;
	private int value;
	private String courseid;

	public GamificationPoint() {
		this(null, null, 0, null);
	}

	public GamificationPoint(Pointtype type, Date date, int value, String courseid) {
		this.type = type;
		this.date = date;
		this.value = value;
		this.courseid = courseid;
	}

	public Pointtype getType() {
		return type;
	}

	public void setType(Pointtype type) {
		this.type = type;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getCourseid() {
		return courseid;
	}

	public void setCourseid(String courseid) {
		this.courseid = courseid;
	}

	@Override
	public String toString() {
		return "GamificationPoint [type=" + type + ", date=" + date + ", value=" + value + ", courseid=" + courseid
				+ "]";
	}

}
