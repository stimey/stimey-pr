package eu.stimey.platform.microservice.achievement.databind;

public enum LeaderboardType {
	ALLTIME, MONTHLY, WEEKLY, DAILY
}
