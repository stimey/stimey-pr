package eu.stimey.platform.microservice.achievement.databind.responses;

import eu.stimey.platform.microservice.achievement.databind.GamificationDailyGoal;

public class GamificationDailyGoalResponse {

	public int kp;
	public int sp;
	public int cp;
	public int kpGoal;
	public int spGoal;
	public int cpGoal;
	public int kpPercent;
	public int spPercent;
	public int cpPercent;

	public GamificationDailyGoalResponse(GamificationDailyGoal dailyGoal) {
		this.kp = dailyGoal.getKp();
		this.sp = dailyGoal.getSp();
		this.cp = dailyGoal.getCp();
		this.kpGoal = dailyGoal.getGoal_kp();
		this.spGoal = dailyGoal.getGoal_sp();
		this.cpGoal = dailyGoal.getGoal_cp();
		// System.out.println("GOAL: " + kpGoal);
		this.kpPercent = getPercent(kp, kpGoal);
		this.spPercent = getPercent(sp, spGoal);
		this.cpPercent = getPercent(cp, cpGoal);
	}

	private int getPercent(int a, int b) {
		return (int) Math.round((double) a / (double) b * 100);
	}

}
