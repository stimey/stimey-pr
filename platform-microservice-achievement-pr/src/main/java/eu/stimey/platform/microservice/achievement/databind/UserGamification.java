package eu.stimey.platform.microservice.achievement.databind;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class UserGamification {

	private String userid;
	private int level;
	private int knowledgePoints;
	private int socialPoints;
	private int creativityPoints;
	private int totalPoints;
	private int energyOrbs;
	private GamificationDailyGoal dailyGoal;
	private List<GamificationMessage> events;
	private List<GamificationPoint> allPoints;
	private Map<Integer, GamificationDailyGoalProgress> dailyGoalProgress;
	private boolean dailyGoalReached;
	// private List<GamificationBadge> badges;
	// private List<GamificationCertificate> certificates;

	public UserGamification() {
		this(null, 0, 0, 0, 0, 0);
	}

	public UserGamification(String userid, int level, int knowledgePoints, int socialPoints, int creativityPoints,
			int energyOrbs) {
		this.userid = userid;
		this.level = level;
		this.knowledgePoints = knowledgePoints;
		this.socialPoints = socialPoints;
		this.creativityPoints = creativityPoints;
		this.energyOrbs = energyOrbs;
		this.totalPoints = knowledgePoints + socialPoints + creativityPoints;
		this.events = new LinkedList<>();
		this.allPoints = new LinkedList<>();
		this.dailyGoalProgress = new HashMap<>();
		dailyGoalProgress.put(Calendar.SUNDAY, null);
		dailyGoalProgress.put(Calendar.MONDAY, null);
		dailyGoalProgress.put(Calendar.TUESDAY, null);
		dailyGoalProgress.put(Calendar.WEDNESDAY, null);
		dailyGoalProgress.put(Calendar.THURSDAY, null);
		dailyGoalProgress.put(Calendar.FRIDAY, null);
		dailyGoalProgress.put(Calendar.SATURDAY, null);
		this.dailyGoalReached = false;
		// badges = new LinkedList<>();
		// certificates = new LinkedList<>();

	}

	// public UserGamification(String userid, int level, int knowledgePoints, int
	// socialPoints, int creativityPoints,
	// List<GamificationMessage> events) {
	// super();
	// this.userid = userid;
	// this.level = level;
	// this.knowledgePoints = knowledgePoints;
	// this.socialPoints = socialPoints;
	// this.creativityPoints = creativityPoints;
	// this.events = events;
	// }

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getKnowledgePoints() {
		return knowledgePoints;
	}

	public void setKnowledgePoints(int knowledgePoints) {
		this.knowledgePoints = knowledgePoints;
		setTotalPoints();
	}

	public int getSocialPoints() {
		return socialPoints;
	}

	public void setSocialPoints(int socialPoints) {
		this.socialPoints = socialPoints;
		setTotalPoints();
	}

	public int getCreativityPoints() {
		return creativityPoints;
	}

	public void setCreativityPoints(int creativityPoints) {
		this.creativityPoints = creativityPoints;
		setTotalPoints();
	}

	public int getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints() {
		this.totalPoints = knowledgePoints + socialPoints + creativityPoints;
	}

	public int getEnergyOrbs() {
		return energyOrbs;
	}

	public void setEnergyOrbs(int energyOrbs) {
		this.energyOrbs = energyOrbs;
	}

	public List<GamificationMessage> getEvents() {
		return events;
	}

	public void setEvents(List<GamificationMessage> events) {
		this.events = events;
	}

	public void addEvent(GamificationMessage event) {
		this.events.add(event);
	}

	public GamificationDailyGoal getDailyGoal() {
		return dailyGoal;
	}

	public void setDailyGoal(GamificationDailyGoal dailyGoal) {
		this.dailyGoal = dailyGoal;
	}

	public List<GamificationPoint> getAllPoints() {
		return allPoints;
	}

	public void setAllPoints(List<GamificationPoint> allPoints) {
		this.allPoints = allPoints;
	}

	public void addPoints(List<GamificationPoint> points) {
		for (GamificationPoint g : points) {
			allPoints.add(g);
		}
	}

	public Map<Integer, GamificationDailyGoalProgress> getDailyGoalProgress() {
		return dailyGoalProgress;
	}

	public void setDailyGoalProgress(Map<Integer, GamificationDailyGoalProgress> dailyGoalProgress) {
		this.dailyGoalProgress = dailyGoalProgress;
	}

	public boolean isDailyGoalReached() {
		return dailyGoalReached;
	}

	public void setDailyGoalReached(boolean dailyGoalReached) {
		this.dailyGoalReached = dailyGoalReached;
	}

	@Override
	public String toString() {
		return "UserGamification [userid=" + userid + ", level=" + level + ", knowledgePoints=" + knowledgePoints
				+ ", socialPoints=" + socialPoints + ", creativityPoints=" + creativityPoints + ", totalPoints="
				+ totalPoints + ", energyOrbs=" + energyOrbs + ", dailyGoal=" + dailyGoal + ", events=" + events
				+ ", allPoints=" + allPoints + ", dailyGoalProgress=" + dailyGoalProgress + ", dailyGoalReached="
				+ dailyGoalReached + "]";
	}

	// public List<GamificationBadge> getBadges() {
	// return badges;
	// }
	//
	// public void setBadges(List<GamificationBadge> badges) {
	// this.badges = badges;
	// }
	//
	// public List<GamificationCertificate> getCertificates() {
	// return certificates;
	// }
	//
	// public void setCertificates(List<GamificationCertificate> certificates) {
	// this.certificates = certificates;
	// }

}
