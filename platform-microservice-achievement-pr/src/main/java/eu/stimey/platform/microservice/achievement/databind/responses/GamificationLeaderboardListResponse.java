package eu.stimey.platform.microservice.achievement.databind.responses;

import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;

import eu.stimey.platform.microservice.achievement.databind.GamificationLeaderboard;

public class GamificationLeaderboardListResponse {

	public List<Object> alltime;
	public List<Object> monthly;
	public List<Object> weekly;
	public List<Object> daily;

	public GamificationLeaderboardListResponse(ContainerRequestContext context, List<GamificationLeaderboard> all,
			List<GamificationLeaderboard> month, List<GamificationLeaderboard> week,
			List<GamificationLeaderboard> day) {

		this.alltime = new LinkedList<>();
		this.monthly = new LinkedList<>();
		this.weekly = new LinkedList<>();
		this.daily = new LinkedList<>();

		if (all != null) {
			for (GamificationLeaderboard userboard : all) {
				GamificationLeaderboardResponse response = new GamificationLeaderboardResponse(context, userboard);
				response.position = all.indexOf(userboard) + 1;
				this.alltime.add(response);
			}
		}

		if (month != null) {
			for (GamificationLeaderboard userboard : month) {
				GamificationLeaderboardResponse response = new GamificationLeaderboardResponse(context, userboard);
				response.position = month.indexOf(userboard) + 1;
				this.monthly.add(response);
			}
		}

		if (week != null) {
			for (GamificationLeaderboard userboard : week) {
				GamificationLeaderboardResponse response = new GamificationLeaderboardResponse(context, userboard);
				response.position = week.indexOf(userboard) + 1;
				this.weekly.add(response);
			}
		}

		if (day != null) {
			for (GamificationLeaderboard userboard : day) {
				GamificationLeaderboardResponse response = new GamificationLeaderboardResponse(context, userboard);
				response.position = day.indexOf(userboard) + 1;
				this.daily.add(response);
			}
		}

	}

}
