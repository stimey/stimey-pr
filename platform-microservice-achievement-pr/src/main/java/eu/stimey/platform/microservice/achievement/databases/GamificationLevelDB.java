package eu.stimey.platform.microservice.achievement.databases;

import java.util.List;

import eu.stimey.platform.library.data.access.Caching;
import eu.stimey.platform.microservice.achievement.beans.GlobalVariables;
import eu.stimey.platform.microservice.achievement.databind.GamificationLevel;

public class GamificationLevelDB {

	private Caching<String, GamificationLevel> levelCache;

	public GamificationLevelDB() {
		this.levelCache = new Caching<>("ms_achievement", "level", GamificationLevel.class, "gamificationlevel",
				GlobalVariables.CACHE_CONFIG);
		this.levelCache.clear();
	}

	public void fillDatabase() {

		List<String> keys = levelCache.find(null, null, 0, 0, (gl) -> gl.getLevel(), true);
		for (String key : keys) {
			levelCache.del(key);
		}

		GamificationLevel level1 = new GamificationLevel("1", 250, 200, 200,
				"You have a challenging, but fun, journey ahead to advance in the fleet. So start exploring!",
				"You're learning a lot on your journey, which is great, but you need to learn to work with others more, and learn 'outside of the box'!",
				"You're great at making friends, how nice! But make sure you stay focused on your learning journey, and learn 'outside of the box'!",
				"Your creative juices are flowing, very inspiring! But make sure you stay focused on your learning journey, and you need to learn to work with others more!");
		GamificationLevel level2 = new GamificationLevel("2", 500, 400, 400,
				"You're now capable of managing more responsibilities and bigger challenges. Keep going!",
				"You're learning a lot on your journey, which is great, but you need to learn to work with others more, and learn 'outside of the box'!",
				"You're great at making friends, how nice! But make sure you stay focused on your learning journey, and learn 'outside of the box'!",
				"Your creative juices are flowing, very inspiring! But make sure you stay focused on your learning journey, and you need to learn to work with others more!");
		GamificationLevel level3 = new GamificationLevel("3", 750, 600, 600,
				"As a Junior Lieutenant, your horizons are broadening with more planets to explore, and we know you‘re up to the challenge!",
				"You're learning a lot on your journey, which is great, but you need to learn to work with others more, and learn 'outside of the box'!",
				"You're great at making friends, how nice! But make sure you stay focused on your learning journey, and learn 'outside of the box'!",
				"Your creative juices are flowing, very inspiring! But make sure you stay focused on your learning journey, and you need to learn to work with others more!");
		GamificationLevel level4 = new GamificationLevel("4", 1000, 800, 800,
				"Your horizons are broadening with more planets to explore, and we know you're up to the challenge!",
				"You're learning a lot on your journey, which is great, but you need to learn to work with others more, and learn 'outside of the box'!",
				"You're great at making friends, how nice! But make sure you stay focused on your learning journey, and learn 'outside of the box'!",
				"Your creative juices are flowing, very inspiring! But make sure you stay focused on your learning journey, and you need to learn to work with others more!");
		GamificationLevel level5 = new GamificationLevel("5", 1500, 1200, 1200, "",
				"You're learning a lot on your journey, which is great, but you need to learn to work with others more, and learn 'outside of the box'!",
				"You're great at making friends, how nice! But make sure you stay focused on your learning journey, and learn 'outside of the box'!",
				"Your creative juices are flowing, very inspiring! But make sure you stay focused on your learning journey, and you need to learn to work with others more!");
		GamificationLevel level6 = new GamificationLevel("6", 2000, 1600, 1600, "",
				"You're learning a lot on your journey, which is great, but you need to learn to work with others more, and learn 'outside of the box'!",
				"You're great at making friends, how nice! But make sure you stay focused on your learning journey, and learn 'outside of the box'!",
				"Your creative juices are flowing, very inspiring! But make sure you stay focused on your learning journey, and you need to learn to work with others more!");
		GamificationLevel level7 = new GamificationLevel("7", 2800, 2200, 2200, "",
				"You're learning a lot on your journey, which is great, but you need to learn to work with others more, and learn 'outside of the box'!",
				"You're great at making friends, how nice! But make sure you stay focused on your learning journey, and learn 'outside of the box'!",
				"Your creative juices are flowing, very inspiring! But make sure you stay focused on your learning journey, and you need to learn to work with others more!");
		GamificationLevel level8 = new GamificationLevel("8", 3600, 2800, 2800, "",
				"You're learning a lot on your journey, which is great, but you need to learn to work with others more, and learn 'outside of the box'!",
				"You're great at making friends, how nice! But make sure you stay focused on your learning journey, and learn 'outside of the box'!",
				"Your creative juices are flowing, very inspiring! But make sure you stay focused on your learning journey, and you need to learn to work with others more!");
		GamificationLevel level9 = new GamificationLevel("9", 4600, 3800, 3800, "",
				"You're learning a lot on your journey, which is great, but you need to learn to work with others more, and learn 'outside of the box'!",
				"You're great at making friends, how nice! But make sure you stay focused on your learning journey, and learn 'outside of the box'!",
				"Your creative juices are flowing, very inspiring! But make sure you stay focused on your learning journey, and you need to learn to work with others more!");
		GamificationLevel level10 = new GamificationLevel("10", 5600, 4800, 4800, "",
				"You're learning a lot on your journey, which is great, but you need to learn to work with others more, and learn 'outside of the box'!",
				"You're great at making friends, how nice! But make sure you stay focused on your learning journey, and learn 'outside of the box'!",
				"Your creative juices are flowing, very inspiring! But make sure you stay focused on your learning journey, and you need to learn to work with others more!");

		levelCache.set("1", level1);
		levelCache.set("2", level2);
		levelCache.set("3", level3);
		levelCache.set("4", level4);
		levelCache.set("5", level5);
		levelCache.set("6", level6);
		levelCache.set("7", level7);
		levelCache.set("8", level8);
		levelCache.set("9", level9);
		levelCache.set("10", level10);

	}

}
