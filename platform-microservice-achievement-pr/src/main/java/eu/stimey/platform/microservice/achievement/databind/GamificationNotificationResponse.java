package eu.stimey.platform.microservice.achievement.databind;

public class GamificationNotificationResponse {

	public String title;
	public String message;
	public String knowledge;
	public String social;
	public String creativity;
	public String type;

	public GamificationNotificationResponse(GamificationEvent event, NotificationType type) {

		if (type == NotificationType.success) {
			this.title = event.getSuccessNotificationTitle();
			this.message = String.format(event.getSuccessNotification(), event.getKnowledgepoints().getValue(),
					event.getSocialpoints().getValue(), event.getCreativitypoints().getValue());
			;
			this.knowledge = String.valueOf(event.getKnowledgepoints().getValue());
			this.social = String.valueOf(event.getSocialpoints().getValue());
			this.creativity = String.valueOf(event.getCreativitypoints().getValue());
			this.type = "success";
		}
		if (type == NotificationType.error) {
			this.title = event.getErrorNotificationTitle();
			this.message = event.getErrorNotification();
			this.knowledge = "";
			this.social = "";
			this.creativity = "";
			this.type = "error";
		}
		if (type == NotificationType.info) {
			this.title = event.getInfoNotificationTitle();
			this.message = event.getInfoNotification();
			this.knowledge = "";
			this.social = "";
			this.creativity = "";
			this.type = "info";
		}
		if (type == NotificationType.question) {
			this.title = String.format(event.getSuccessNotificationTitle(), event.getKnowledgepoints().getValue());
			this.message = String.format(event.getSuccessNotification(), event.getKnowledgepoints().getValue());
			this.knowledge = String.valueOf(event.getKnowledgepoints().getValue());
			this.social = String.valueOf(event.getSocialpoints().getValue());
			this.creativity = String.valueOf(event.getCreativitypoints().getValue());
			this.type = "question";
		}

		if (type == NotificationType.fantasy_info) {
			this.title = event.getInfoNotificationTitle();
			this.message = event.getInfoNotification();
			this.knowledge = String.valueOf(event.getKnowledgepoints().getValue());
			this.social = String.valueOf(event.getSocialpoints().getValue());
			this.creativity = String.valueOf(event.getCreativitypoints().getValue());
			this.type = "fantasy_info";
		}

	}

	public GamificationNotificationResponse(String title, String message, NotificationType type) {
		this.title = title;
		this.message = message;
		this.type = type.toString();
	}

}
