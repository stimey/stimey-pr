package eu.stimey.platform.microservice.achievement.databind.responses;

import eu.stimey.platform.microservice.achievement.databind.GamificationDailyGoal;
import eu.stimey.platform.microservice.achievement.databind.GamificationLevel;
import eu.stimey.platform.microservice.achievement.databind.UserGamification;

public class UserGamificationResponse {

	public String userid;
	public int level;
	public int knowledgeLimit;
	public int socialLimit;
	public int creativityLimit;
	public int knowledgePoints;
	public int socialPoints;
	public int creativityPoints;
	public int totalPoints;
	public int knowledgePercent;
	public int socialPercent;
	public int creativityPercent;
	public int energyOrbs;
	public String feedback;
	public int dailyGoalPercent;
	public Object dailyGoalProgress;
	// public List<Message> events;
	// public GamificationDailyGoal dailyGoal;
	// public List<GamificationBadge> badges;
	// public List<GamificationCertificate> certificates;

	public UserGamificationResponse() {
		//empty
	}

	public UserGamificationResponse(UserGamification ug, GamificationLevel gl, GamificationDailyGoal dg) {
		// this.userid = ug.getUserid();
		this.level = ug.getLevel();
		this.knowledgeLimit = gl.getKp_limit();
		this.socialLimit = gl.getSp_limit();
		this.creativityLimit = gl.getCp_limit();
		this.knowledgePoints = ug.getKnowledgePoints();
		this.socialPoints = ug.getSocialPoints();
		this.creativityPoints = ug.getCreativityPoints();
		this.totalPoints = ug.getTotalPoints();
		this.knowledgePercent = getPercent(knowledgePoints, knowledgeLimit);
		this.socialPercent = getPercent(socialPoints, socialLimit);
		this.creativityPercent = getPercent(creativityPoints, creativityLimit);
		this.energyOrbs = ug.getEnergyOrbs();
		this.feedback = getFeedback(gl, knowledgePoints, socialPoints, creativityPoints);
		if (dg == null) {
			this.dailyGoalPercent = 0;
		} else {
			this.dailyGoalPercent = getPercent(dg.getKp() + dg.getSp() + dg.getCp(),
					dg.getGoal_kp() + dg.getGoal_sp() + dg.getGoal_cp());
		}
		dailyGoalProgress = new DailyGoalProgressResponse(ug.getDailyGoalProgress());

	}

	private String getFeedback(GamificationLevel gl, int kp, int sp, int cp) {
		// System.out.println(kp + " " + sp + " " + cp);
		if (kp == sp && sp == cp) {
			return gl.getFeedback_same();
		} else if ((sp > kp) && (sp > cp)) { // SocialPoints are the highest
			return gl.getFeedback_sp_high();
		} else if ((cp > kp) && (cp > sp)) { // CreativityPoints are the highest
			return gl.getFeedback_cp_high();
		} else { // Either KnowledgePoints are the highest or also the same number as SP or CP
			return gl.getFeedback_kp_high();
		}
	}

	private int getPercent(int a, int b) {
		return (int) Math.round((double) a / (double) b * 100);
	}

}
