package eu.stimey.platform.microservice.achievement.databind;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import eu.stimey.platform.library.utils.databind.DateDeserializer;
import eu.stimey.platform.library.utils.databind.DateSerializer;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class GamificationMessage {

	private String messageid;
	private int event;
	private String courseid;

	@JsonSerialize(using = DateSerializer.class)
	@JsonDeserialize(using = DateDeserializer.class)
	private Date timestamp;

	public GamificationMessage() {
		this(null, 0, null, null);
	}

	public GamificationMessage(String eventid, int event, String courseid, Date timestamp) {
		this.messageid = eventid;
		this.event = event;
		this.courseid = courseid;
		this.timestamp = timestamp;
	}

	public String getMessageid() {
		return messageid;
	}

	public void setMessageid(String messageid) {
		this.messageid = messageid;
	}

	public int getEvent() {
		return event;
	}

	public void setEvent(int event) {
		this.event = event;
	}

	public String getCourseid() {
		return courseid;
	}

	public void setCourseid(String courseid) {
		this.courseid = courseid;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "GamificationMessage [messageid=" + messageid + ", event=" + event + ", courseid=" + courseid
				+ ", timestamp=" + timestamp + "]";
	}

}
