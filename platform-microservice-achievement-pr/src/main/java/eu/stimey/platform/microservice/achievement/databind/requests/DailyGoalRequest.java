package eu.stimey.platform.microservice.achievement.databind.requests;

public class DailyGoalRequest {

	public String knowledgeGoal;
	public String socialGoal;
	public String creativityGoal;

	public DailyGoalRequest() {

	}

}
